﻿Imports eZeeCommonLib

Public Class clsEmployeeAddress_approval_tran
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeAddress_Approval_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mstrApprovalremark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintStatusunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mstrAddress1 As String = String.Empty
    Private mstrAddress2 As String = String.Empty
    Private mintCountryunkid As Integer
    Private mintStateunkid As Integer
    Private mintPost_Townunkid As Integer
    Private mintPostcodeunkid As Integer
    Private mstrProvicnce As String = String.Empty
    Private mstrRoad As String = String.Empty
    Private mstrEstate As String = String.Empty
    Private mintProvinceunkid As Integer
    Private mintRoadunkid As Integer
    Private mintChiefdomunkid As Integer
    Private mintVillageunkid As Integer
    Private mintTown1unkid As Integer
    Private mstrMobile As String = String.Empty
    Private mstrTel_No As String = String.Empty
    Private mstrPlotno As String = String.Empty
    Private mstrAlternateno As String = String.Empty
    Private mstrEmail As String = String.Empty
    Private mstrFax As String = String.Empty
    Private mintAdddresstype As Integer

    Private mintLoginemployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtAuditdatetime As Date
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHost As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsprocessed As Boolean = False
    Private mintOperationtypeid As Integer = 0

    'S.SANDEEP |26-APR-2019| -- START
    Private mstrnewattachdocumentid As String = String.Empty
    Private mstrdeleteattachdocumentid As String = String.Empty
    'S.SANDEEP |26-APR-2019| -- END

#End Region

#Region " Enum "

    Public Enum enAddressType
'Gajanan [17-April-2019] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.
        NONE = 0
'Gajanan [17-April-2019] -- End       
        PRESENT = 1
        DOMICILE = 2
        RECRUITMENT = 3
    End Enum

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvalremark
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Approvalremark() As String
        Get
            Return mstrApprovalremark
        End Get
        Set(ByVal value As String)
            mstrApprovalremark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Public Property _Employeeunkid() As Integer
    Public Property _Employeeunkid(Optional ByVal xDataOp As clsDataOperation = Nothing) As Integer
        'Gajanan [17-April-2019] -- End

        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
            GetData(xDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address1
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Address1() As String
        Get
            Return mstrAddress1
        End Get
        Set(ByVal value As String)
            mstrAddress1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address2
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Address2() As String
        Get
            Return mstrAddress2
        End Get
        Set(ByVal value As String)
            mstrAddress2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set post_townunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Post_Townunkid() As Integer
        Get
            Return mintPost_Townunkid
        End Get
        Set(ByVal value As Integer)
            mintPost_Townunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set postcodeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Postcodeunkid() As Integer
        Get
            Return mintPostcodeunkid
        End Get
        Set(ByVal value As Integer)
            mintPostcodeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set provicnce
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Provicnce() As String
        Get
            Return mstrProvicnce
        End Get
        Set(ByVal value As String)
            mstrProvicnce = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set road
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Road() As String
        Get
            Return mstrRoad
        End Get
        Set(ByVal value As String)
            mstrRoad = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set estate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Estate() As String
        Get
            Return mstrEstate
        End Get
        Set(ByVal value As String)
            mstrEstate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set provinceunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Provinceunkid() As Integer
        Get
            Return mintProvinceunkid
        End Get
        Set(ByVal value As Integer)
            mintProvinceunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set roadunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Roadunkid() As Integer
        Get
            Return mintRoadunkid
        End Get
        Set(ByVal value As Integer)
            mintRoadunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set chiefdomunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Chiefdomunkid() As Integer
        Get
            Return mintChiefdomunkid
        End Get
        Set(ByVal value As Integer)
            mintChiefdomunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set villageunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Villageunkid() As Integer
        Get
            Return mintVillageunkid
        End Get
        Set(ByVal value As Integer)
            mintVillageunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set town1unkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Town1unkid() As Integer
        Get
            Return mintTown1unkid
        End Get
        Set(ByVal value As Integer)
            mintTown1unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mobile
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Mobile() As String
        Get
            Return mstrMobile
        End Get
        Set(ByVal value As String)
            mstrMobile = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tel_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Tel_No() As String
        Get
            Return mstrTel_No
        End Get
        Set(ByVal value As String)
            mstrTel_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set plotNo
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Plotno() As String
        Get
            Return mstrPlotno
        End Get
        Set(ByVal value As String)
            mstrPlotno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set alternateno
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Alternateno() As String
        Get
            Return mstrAlternateno
        End Get
        Set(ByVal value As String)
            mstrAlternateno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fax
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Fax() As String
        Get
            Return mstrFax
        End Get
        Set(ByVal value As String)
            mstrFax = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set adddresstype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Adddresstype() As Integer
        Get
            Return mintAdddresstype
        End Get
        Set(ByVal value As Integer)
            mintAdddresstype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set host
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprocessed
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isprocessed() As Boolean
        Get
            Return mblnIsprocessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocessed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set operationtypeid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Operationtypeid() As Integer
        Get
            Return mintOperationtypeid
        End Get
        Set(ByVal value As Integer)
            mintOperationtypeid = value
        End Set
    End Property


    'S.SANDEEP |26-APR-2019| -- START
    Public Property _Newattachdocumentid() As String
        Get
            Return mstrnewattachdocumentid
        End Get
        Set(ByVal value As String)
            mstrnewattachdocumentid = value
        End Set
    End Property

    Public Property _Deleteattachdocumentid() As String
        Get
            Return mstrdeleteattachdocumentid
        End Get
        Set(ByVal value As String)
            mstrdeleteattachdocumentid = value
        End Set
    End Property
    'S.SANDEEP |26-APR-2019| -- END

#End Region


    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Public Sub GetData()
    Public Sub GetData(Optional ByVal xDataOp As clsDataOperation = Nothing)
        'Gajanan [17-April-2019] -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Gajanan [17-April-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        'objDataOperation = New clsDataOperation

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        'Gajanan [17-April-2019] -- End
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", approvalremark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", employeeunkid " & _
              ", address1 " & _
              ", address2 " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", post_townunkid " & _
              ", postcodeunkid " & _
              ", provicnce " & _
              ", road " & _
              ", estate " & _
              ", provinceunkid " & _
              ", roadunkid " & _
              ", chiefdomunkid " & _
              ", villageunkid " & _
              ", town1unkid " & _
              ", mobile " & _
              ", tel_no " & _
              ", plotNo " & _
              ", alternateno " & _
              ", email " & _
              ", fax " & _
              ", addresstype " & _
              ", loginemployeeunkid " & _
              ", isvoid " & _
              ", auditdatetime " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
              ", isweb " & _
              ", isprocessed " & _
              ", operationtypeid " & _
              ", ISNULL(newattachdocumentid,'') AS newattachdocumentid " & _
              ", ISNULL(deleteattachdocumentid,'') AS deleteattachdocumentid " & _
             "FROM hremployee_address_approval_tran " & _
             "WHERE employeeunkid = @employeeunkid and  addresstype = @addresstype and isfinal=0 and isprocessed = 0 and isvoid = 0"
            'S.SANDEEP |26-APR-2019| -- START {newattachdocumentid,deleteattachdocumentid} -- END

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@addresstype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAdddresstype)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            mstrTranguid = ""
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
            mstrnewattachdocumentid = ""
            mstrdeleteattachdocumentid = ""
               'Gajanan [17-April-2019] -- Start
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mstrTranguid = dtRow.Item("tranguid").ToString
                mdtTransactiondate = dtRow.Item("transactiondate")
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mstrApprovalremark = dtRow.Item("approvalremark").ToString
                mblnIsfinal = CBool(dtRow.Item("isfinal"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                'Gajanan [17-April-2019] -- End
                mstrAddress1 = dtRow.Item("address1").ToString
                mstrAddress2 = dtRow.Item("address2").ToString
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintStateunkid = CInt(dtRow.Item("stateunkid"))
                mintPost_Townunkid = CInt(dtRow.Item("post_townunkid"))
                mintPostcodeunkid = CInt(dtRow.Item("postcodeunkid"))
                mstrProvicnce = dtRow.Item("provicnce").ToString
                mstrRoad = dtRow.Item("road").ToString
                mstrEstate = dtRow.Item("estate").ToString
                mintProvinceunkid = CInt(dtRow.Item("provinceunkid"))
                mintRoadunkid = CInt(dtRow.Item("roadunkid"))
                mintChiefdomunkid = CInt(dtRow.Item("chiefdomunkid"))
                mintVillageunkid = CInt(dtRow.Item("villageunkid"))
                mintTown1unkid = CInt(dtRow.Item("town1unkid"))
                mstrMobile = dtRow.Item("mobile").ToString
                mstrTel_No = dtRow.Item("tel_no").ToString
                mstrPlotno = dtRow.Item("plotNo").ToString
                mstrAlternateno = dtRow.Item("alternateno").ToString
                mstrEmail = dtRow.Item("email").ToString
                mstrFax = dtRow.Item("fax").ToString
                mintAdddresstype = CInt(dtRow.Item("addresstype"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mdtAuditdatetime = dtRow.Item("auditdatetime")
                mintAudittype = CInt(dtRow.Item("audittype"))
                mintAudituserunkid = CInt(dtRow.Item("audituserunkid"))
                mstrIp = dtRow.Item("ip").ToString
                mstrHost = dtRow.Item("host").ToString
                mstrForm_Name = dtRow.Item("form_name").ToString
                mblnIsweb = CBool(dtRow.Item("isweb"))
                mblnIsprocessed = CBool(dtRow.Item("isprocessed"))
                mintOperationtypeid = CInt(dtRow.Item("operationtypeid"))
                'S.SANDEEP |26-APR-2019| -- START
                mstrnewattachdocumentid = dtRow.Item("newattachdocumentid").ToString()
                mstrdeleteattachdocumentid = dtRow.Item("deleteattachdocumentid").ToString()
                'S.SANDEEP |26-APR-2019| -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan [17-April-2019] -- End
        End Try
    End Sub

    'S.SANDEEP |26-APR-2019| -- START
    'Public Function Insert(ByVal xintAddressType As Integer, _
    '                       Optional ByVal xDataOpr As clsDataOperation = Nothing, _
    '                       Optional ByVal blnFromApproval As Boolean = False) As Boolean

    Public Function Insert(ByVal xintAddressType As Integer, _
                           ByVal intCompanyId As Integer, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal blnFromApproval As Boolean = False, _
                           Optional ByVal dtDocument As DataTable = Nothing) As Boolean
        'S.SANDEEP |26-APR-2019| -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()


        If blnFromApproval = False Then
            'S.SANDEEP |26-APR-2019| -- START
            'If xintAddressType = CInt(enAddressType.PRESENT) Then
            '    If isAddressExist(mintEmployeeunkid, xintAddressType, objDataOperation) Then
            '        Return False
            '    End If
            'End If

            'If xintAddressType = CInt(enAddressType.DOMICILE) Then
            '    If isAddressExist(mintEmployeeunkid, xintAddressType, objDataOperation) Then
            '        Return False
            '    End If
            'End If

            'If xintAddressType = CInt(enAddressType.RECRUITMENT) Then
            '    If isAddressExist(mintEmployeeunkid, xintAddressType, objDataOperation) Then
            '        Return False
            '    End If
            'End If
            Dim blnCheckIsExist As Boolean = True
            If dtDocument IsNot Nothing Then
                Dim oAttachType As enScanAttactRefId = Nothing
                Select Case mintAdddresstype
                    Case enAddressType.PRESENT
                        oAttachType = enScanAttactRefId.PERSONAL_ADDRESS_TYPE
                    Case enAddressType.DOMICILE
                        oAttachType = enScanAttactRefId.DOMICILE_ADDRESS_TYPE
                    Case enAddressType.RECRUITMENT
                        oAttachType = enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE
                End Select

                If dtDocument.AsEnumerable().AsEnumerable().Where(Function(x) x.Field(Of Integer)("scanattachrefid") = CInt(oAttachType) AndAlso x.Field(Of String)("AUD") <> "").Count > 0 Then
                    blnCheckIsExist = False
                End If
            End If

            If blnCheckIsExist Then
            If xintAddressType = CInt(enAddressType.PRESENT) Then
                If isAddressExist(mintEmployeeunkid, xintAddressType, objDataOperation) Then
                        'Gajanan [27-May-2019] -- Start              
                        mstrMessage = Language.getMessage(mstrModuleName, 50, "Selected present address is already assigned to the employee. Please assign new present address.")
                        'Gajanan [27-May-2019] -- End

                    Return False
                End If
            End If
            End If

            If blnCheckIsExist Then
            If xintAddressType = CInt(enAddressType.DOMICILE) Then
                If isAddressExist(mintEmployeeunkid, xintAddressType, objDataOperation) Then
                        'Gajanan [27-May-2019] -- Start              
                        mstrMessage = Language.getMessage(mstrModuleName, 51, "Selected domicile address is already assigned to the employee. Please assign new domicile address.")
                        'Gajanan [27-May-2019] -- End

                    Return False
                End If
            End If
            End If

            If blnCheckIsExist Then
            If xintAddressType = CInt(enAddressType.RECRUITMENT) Then
                If isAddressExist(mintEmployeeunkid, xintAddressType, objDataOperation) Then
                        'Gajanan [27-May-2019] -- Start              
                        mstrMessage = Language.getMessage(mstrModuleName, 52, "Selected recruitment address is already assigned to the employee. Please assign new recruitment address.")
                        'Gajanan [27-May-2019] -- End
                    Return False
                End If
            End If
            End If
            'S.SANDEEP |26-APR-2019| -- END

            If xintAddressType = CInt(enAddressType.PRESENT) Then
                If isAddressInApproval(mintEmployeeunkid, xintAddressType, objDataOperation) Then
                    'Gajanan [27-May-2019] -- Start              
                    mstrMessage = Language.getMessage(mstrModuleName, 53, "Sorry, you cannot add seleted information, Reason : Same present address is already present in approval process.")
                    'Gajanan [27-May-2019] -- End
                    Return False
                End If
            End If

            If xintAddressType = CInt(enAddressType.DOMICILE) Then
                If isAddressInApproval(mintEmployeeunkid, xintAddressType, objDataOperation) Then
                    'Gajanan [27-May-2019] -- Start       
                    mstrMessage = Language.getMessage(mstrModuleName, 54, "Sorry, you cannot add seleted information, Reason : Same domicile address is already present in approval process.")

                    'Gajanan [27-May-2019] -- End
                    Return False
                End If
            End If

            If xintAddressType = CInt(enAddressType.RECRUITMENT) Then
                If isAddressInApproval(mintEmployeeunkid, xintAddressType, objDataOperation) Then
                    'Gajanan [27-May-2019] -- Start              
                    mstrMessage = Language.getMessage(mstrModuleName, 55, "Sorry, you cannot add seleted information, Reason : Same recruitment address is already present in approval process.")
                    'Gajanan [27-May-2019] -- End
                    Return False
                End If
            End If
        End If





        Try
            'S.SANDEEP |26-APR-2019| -- START
            Dim eAttachType As enScanAttactRefId = Nothing
            Select Case mintAdddresstype
                Case enAddressType.PRESENT
                    eAttachType = enScanAttactRefId.PERSONAL_ADDRESS_TYPE
                Case enAddressType.DOMICILE
                    eAttachType = enScanAttactRefId.DOMICILE_ADDRESS_TYPE
                Case enAddressType.RECRUITMENT
                    eAttachType = enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE
            End Select
            Dim objDocument As New clsScan_Attach_Documents
            Dim strLocalPath As String = ""
            Dim dtTran As DataTable = Nothing
            Dim strFolderName As String = ""
            If dtDocument IsNot Nothing Then
                If dtDocument.Rows.Count > 0 Then
                    Dim objConfig As New clsConfigOptions
                    strLocalPath = objConfig.GetKeyValue(intCompanyId, "DocumentPath")
                    If strLocalPath Is Nothing Then strLocalPath = ""
                    objConfig = Nothing
                    strFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", eAttachType).Tables(0).Rows(0)("Name").ToString
                    If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                    dtTran = objDocument._Datatable.Clone()
                End If
            End If

            If dtDocument IsNot Nothing AndAlso dtDocument.Rows.Count > 0 Then
                Dim ftab As DataTable = New DataView(dtDocument, "scanattachrefid = '" & CInt(eAttachType) & "'", "", DataViewRowState.CurrentRows).ToTable
                For Each drow As DataRow In ftab.Rows
                    Dim dr As DataRow = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    dr("transactionunkid") = drow("transactionunkid")
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("orgfilepath")
                    dr("destfilepath") = strLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    dr("filesize") = drow("filesize")
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")

                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = ftab
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDocument._Datatable.Rows.Count > 0 AndAlso objDocument._Newattachdocumentids.Length <= 0 AndAlso objDocument._Deleteattachdocumentids.Length <= 0 AndAlso _
                  (objDocument._Datatable.Select("AUD = 'D'").Count > 0 Or objDocument._Datatable.Select("scanattachtranunkid = -1").Count > 0) Then
                    Return False
                End If

            End If
            'S.SANDEEP |26-APR-2019| -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
            End If
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@post_townunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPost_Townunkid.ToString)
            objDataOperation.AddParameter("@postcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostcodeunkid.ToString)
            objDataOperation.AddParameter("@provicnce", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrProvicnce.ToString)
            objDataOperation.AddParameter("@road", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRoad.ToString)
            objDataOperation.AddParameter("@estate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEstate.ToString)
            objDataOperation.AddParameter("@provinceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProvinceunkid.ToString)
            objDataOperation.AddParameter("@roadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoadunkid.ToString)
            objDataOperation.AddParameter("@chiefdomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChiefdomunkid.ToString)
            objDataOperation.AddParameter("@villageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVillageunkid.ToString)
            objDataOperation.AddParameter("@town1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTown1unkid.ToString)
            objDataOperation.AddParameter("@mobile", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobile.ToString)
            objDataOperation.AddParameter("@tel_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTel_No.ToString)
            objDataOperation.AddParameter("@plotNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPlotno.ToString)
            objDataOperation.AddParameter("@alternateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAlternateno.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFax.ToString)
            objDataOperation.AddParameter("@addresstype", SqlDbType.Int, eZeeDataType.INT_SIZE, xintAddressType)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationtypeid.ToString)
            'S.SANDEEP |26-APR-2019| -- START
            If objDocument._Newattachdocumentids.Length > 0 Then
                objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, objDocument._Newattachdocumentids.Length, objDocument._Newattachdocumentids)
            Else
                objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, mstrnewattachdocumentid.Length, mstrnewattachdocumentid.ToString())
            End If

            If objDocument._Deleteattachdocumentids.Length > 0 Then
                objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, objDocument._Deleteattachdocumentids.Length, objDocument._Deleteattachdocumentids)
            Else
                objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, mstrdeleteattachdocumentid.Length, mstrdeleteattachdocumentid.ToString())
            End If
            'S.SANDEEP |26-APR-2019| -- END

            strQ = "INSERT INTO hremployee_address_approval_tran ( " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", approvalremark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", employeeunkid " & _
              ", address1 " & _
              ", address2 " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", post_townunkid " & _
              ", postcodeunkid " & _
              ", provicnce " & _
              ", road " & _
              ", estate " & _
              ", provinceunkid " & _
              ", roadunkid " & _
              ", chiefdomunkid " & _
              ", villageunkid " & _
              ", town1unkid " & _
              ", mobile " & _
              ", tel_no " & _
              ", plotNo " & _
              ", alternateno " & _
              ", email " & _
              ", fax " & _
              ", addresstype " & _
              ", loginemployeeunkid " & _
              ", isvoid " & _
              ", auditdatetime " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
              ", isweb " & _
              ", isprocessed " & _
              ", operationtypeid" & _
              ", newattachdocumentid " & _
              ", deleteattachdocumentid " & _
            ") VALUES (" & _
              "  @tranguid " & _
              ", @transactiondate " & _
              ", @mappingunkid " & _
              ", @approvalremark " & _
              ", @isfinal " & _
              ", @statusunkid " & _
              ", @employeeunkid " & _
              ", @address1 " & _
              ", @address2 " & _
              ", @countryunkid " & _
              ", @stateunkid " & _
              ", @post_townunkid " & _
              ", @postcodeunkid " & _
              ", @provicnce " & _
              ", @road " & _
              ", @estate " & _
              ", @provinceunkid " & _
              ", @roadunkid " & _
              ", @chiefdomunkid " & _
              ", @villageunkid " & _
              ", @town1unkid " & _
              ", @mobile " & _
              ", @tel_no " & _
              ", @plotNo " & _
              ", @alternateno " & _
              ", @email " & _
              ", @fax " & _
              ", @addresstype " & _
              ", @loginemployeeunkid " & _
              ", @isvoid " & _
              ", GETDATE() " & _
              ", @audittype " & _
              ", @audituserunkid " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
              ", @isweb " & _
              ", @isprocessed " & _
              ", @operationtypeid" & _
              ", @newattachdocumentid " & _
              ", @deleteattachdocumentid " & _
            "); SELECT @@identity"
            'S.SANDEEP |26-APR-2019| -- START {newattachdocumentid,deleteattachdocumentid} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_address_approval_tran) </purpose>
    Public Function Update() As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@post_townunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPost_Townunkid.ToString)
            objDataOperation.AddParameter("@postcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostcodeunkid.ToString)
            objDataOperation.AddParameter("@provicnce", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrProvicnce.ToString)
            objDataOperation.AddParameter("@road", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRoad.ToString)
            objDataOperation.AddParameter("@estate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEstate.ToString)
            objDataOperation.AddParameter("@provinceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProvinceunkid.ToString)
            objDataOperation.AddParameter("@roadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoadunkid.ToString)
            objDataOperation.AddParameter("@chiefdomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChiefdomunkid.ToString)
            objDataOperation.AddParameter("@villageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVillageunkid.ToString)
            objDataOperation.AddParameter("@town1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTown1unkid.ToString)
            objDataOperation.AddParameter("@mobile", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobile.ToString)
            objDataOperation.AddParameter("@tel_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTel_No.ToString)
            objDataOperation.AddParameter("@plotNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPlotno.ToString)
            objDataOperation.AddParameter("@alternateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAlternateno.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFax.ToString)
            objDataOperation.AddParameter("@adddresstype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAdddresstype.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditdatetime.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationtypeid.ToString)
            'S.SANDEEP |26-APR-2019| -- START
            objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrnewattachdocumentid.ToString())
            objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrdeleteattachdocumentid.ToString())
            'S.SANDEEP |26-APR-2019| -- END

            strQ = "UPDATE hremployee_address_approval_tran SET " & _
              "  transactiondate = @transactiondate" & _
              ", mappingunkid = @mappingunkid" & _
              ", approvalremark = @approvalremark" & _
              ", isfinal = @isfinal" & _
              ", statusunkid = @statusunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", address1 = @address1" & _
              ", address2 = @address2" & _
              ", countryunkid = @countryunkid" & _
              ", stateunkid = @stateunkid" & _
              ", post_townunkid = @post_townunkid" & _
              ", postcodeunkid = @postcodeunkid" & _
              ", provicnce = @provicnce" & _
              ", road = @road" & _
              ", estate = @estate" & _
              ", provinceunkid = @provinceunkid" & _
              ", roadunkid = @roadunkid" & _
              ", chiefdomunkid = @chiefdomunkid" & _
              ", villageunkid = @villageunkid" & _
              ", town1unkid = @town1unkid" & _
              ", mobile = @mobile" & _
              ", tel_no = @tel_no" & _
              ", plotNo = @plotNo" & _
              ", alternateno = @alternateno" & _
              ", email = @email" & _
              ", fax = @fax" & _
              ", adddresstype = @adddresstype" & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", isvoid = @isvoid" & _
              ", auditdatetime = @auditdatetime" & _
              ", audittype = @audittype" & _
              ", audituserunkid = @audituserunkid" & _
              ", ip = @ip" & _
              ", host = @host" & _
              ", form_name = @form_name" & _
              ", isweb = @isweb" & _
              ", isprocessed = @isprocessed" & _
              ", operationtypeid = @operationtypeid " & _
              ", newattachdocumentid = @newattachdocumentid " & _
              ", deleteattachdocumentid = @deleteattachdocumentid " & _
            "WHERE tranguid = @tranguid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intAddressTranUnkid As Integer, ByVal intAddressType As Integer, ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
        End If

        Try
            strQ = "UPDATE hremployee_address_approval_tran SET " & _
                   " " & strColName & " = '" & intAddressTranUnkid & "' " & _
                   "WHERE isprocessed= 0 and employeeunkid = @employeeunkid AND addresstype = @addresstype and isprocessed = 0"

            'Gajanan [31-May-2020] -- Start
            objDataOperation.AddParameter("@addresstype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAddressType)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            'Gajanan [31-May-2020] -- End


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP |26-APR-2019| -- START
    Public Function UpdateDeleteDocument(ByVal xDataOperation As clsDataOperation, _
                                         ByVal intEmployeeId As Integer, _
                                         ByVal intTransactionId As Integer, _
                                         ByVal strScanRefIds As String, _
                                         ByVal strFormName As String, _
                                         ByVal strColName As String, _
                                         ByVal blnIsDelete As Boolean) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            If strScanRefIds.Trim.Length > 0 Then
                strQ = "UPDATE hrdocuments_tran SET " & strColName & " = '" & intTransactionId & "' ,isinapproval = 0 "
                If blnIsDelete Then
                    strQ &= ", isactive = 0 "
                Else
                    strQ &= ", isactive = 1 "
                End If
                strQ &= "WHERE employeeunkid = '" & intEmployeeId & "' AND scanattachtranunkid IN (" & strScanRefIds & ") "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If xDataOperation Is Nothing Then
                    objDataOperation.ReleaseTransaction(True)
                End If
            End If
            Return True
        Catch ex As Exception
            If xDataOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'S.SANDEEP |26-APR-2019| -- END


    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isAddressExist(ByVal intEmpUnkid As Integer, ByVal xaddressType As Integer, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing _
                           ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  employeeunkid " & _
              " FROM hremployee_master " & _
              " WHERE "


            If xaddressType = CInt(enAddressType.PRESENT) Then
                strQ &= " employeeunkid = @employeeunkid " & _
                        " and present_address1 =@address1" & _
                        " and present_address2=@address2 " & _
                        " and present_countryunkid =@countryunkid " & _
                        " and present_stateunkid=@stateunkid " & _
                        " and present_post_townunkid=@post_townunkid " & _
                        " and present_postcodeunkid=@postcodeunkid " & _
                        " and present_provicnce=@provicnce " & _
                        " and present_road=@road " & _
                        " and present_estate=@estate " & _
                        " and present_provinceunkid=@provinceunkid " & _
                        " and present_roadunkid=@roadunkid " & _
                        " and present_chiefdomunkid=@chiefdomunkid " & _
                        " and present_villageunkid=@villageunkid " & _
                        " and present_town1unkid=@town1unkid " & _
                        " and present_mobile=@mobile " & _
                        " and present_tel_no=@tel_no " & _
                        " and present_plotNo=@plotNo " & _
                        " and present_alternateno=@alternateno " & _
                        " and present_email=@email " & _
                        " and present_fax=@fax "

                objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmpUnkid)
                objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
                objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
                objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
                objDataOperation.AddParameter("@post_townunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPost_Townunkid.ToString)
                objDataOperation.AddParameter("@postcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostcodeunkid.ToString)
                objDataOperation.AddParameter("@provicnce", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrProvicnce.ToString)
                objDataOperation.AddParameter("@road", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRoad.ToString)
                objDataOperation.AddParameter("@estate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEstate.ToString)
                objDataOperation.AddParameter("@provinceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProvinceunkid.ToString)
                objDataOperation.AddParameter("@roadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoadunkid.ToString)
                objDataOperation.AddParameter("@chiefdomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChiefdomunkid.ToString)
                objDataOperation.AddParameter("@villageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVillageunkid.ToString)
                objDataOperation.AddParameter("@town1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTown1unkid.ToString)
                objDataOperation.AddParameter("@mobile", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobile.ToString)
                objDataOperation.AddParameter("@tel_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTel_No.ToString)
                objDataOperation.AddParameter("@plotNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPlotno.ToString)
                objDataOperation.AddParameter("@alternateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAlternateno.ToString)
                objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
                objDataOperation.AddParameter("@fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFax.ToString)

            ElseIf xaddressType = CInt(enAddressType.DOMICILE) Then
                strQ &= " employeeunkid = @employeeunkid " & _
                        " and domicile_address1 =@address1" & _
                        " and domicile_address2=@address2 " & _
                        " and domicile_countryunkid =@countryunkid " & _
                        " and domicile_stateunkid=@stateunkid " & _
                        " and domicile_post_townunkid=@post_townunkid " & _
                        " and domicile_postcodeunkid=@postcodeunkid " & _
                        " and domicile_provicnce=@provicnce " & _
                        " and domicile_road=@road " & _
                        " and domicile_estate=@estate " & _
                        " and domicile_provinceunkid=@provinceunkid " & _
                        " and domicile_roadunkid=@roadunkid " & _
                        " and domicile_chiefdomunkid=@chiefdomunkid " & _
                        " and domicile_villageunkid=@villageunkid " & _
                        " and domicile_town1unkid=@town1unkid " & _
                        " and domicile_mobile=@mobile " & _
                        " and domicile_tel_no=@tel_no " & _
                        " and domicile_plotNo=@plotNo " & _
                        " and domicile_alternateno=@alternateno " & _
                        " and domicile_email=@email " & _
                        " and domicile_fax=@fax "

                objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmpUnkid)
                objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
                objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
                objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
                objDataOperation.AddParameter("@post_townunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPost_Townunkid.ToString)
                objDataOperation.AddParameter("@postcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostcodeunkid.ToString)
                objDataOperation.AddParameter("@provicnce", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrProvicnce.ToString)
                objDataOperation.AddParameter("@road", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRoad.ToString)
                objDataOperation.AddParameter("@estate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEstate.ToString)
                objDataOperation.AddParameter("@provinceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProvinceunkid.ToString)
                objDataOperation.AddParameter("@roadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoadunkid.ToString)
                objDataOperation.AddParameter("@chiefdomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChiefdomunkid.ToString)
                objDataOperation.AddParameter("@villageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVillageunkid.ToString)
                objDataOperation.AddParameter("@town1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTown1unkid.ToString)
                objDataOperation.AddParameter("@mobile", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobile.ToString)
                objDataOperation.AddParameter("@tel_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTel_No.ToString)
                objDataOperation.AddParameter("@plotNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPlotno.ToString)
                objDataOperation.AddParameter("@alternateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAlternateno.ToString)
                objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
                objDataOperation.AddParameter("@fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFax.ToString)

            ElseIf xaddressType = CInt(enAddressType.RECRUITMENT) Then
                strQ &= " employeeunkid = @employeeunkid " & _
                        " and rc_address1 =@address1" & _
                        " and rc_address2=@address2 " & _
                        " and rc_countryunkid =@countryunkid " & _
                        " and rc_stateunkid=@stateunkid " & _
                        " and rc_post_townunkid=@post_townunkid " & _
                        " and rc_postcodeunkid=@postcodeunkid " & _
                        " and rc_province=@provicnce " & _
                        " and rc_road=@road " & _
                        " and rc_estate=@estate " & _
                        " and recruitment_provinceunkid=@provinceunkid " & _
                        " and recruitment_roadunkid=@roadunkid " & _
                        " and recruitment_chiefdomunkid=@chiefdomunkid " & _
                        " and recruitment_villageunkid=@villageunkid " & _
                        " and recruitment_town1unkid=@town1unkid " & _
                        " /* and present_mobile=@mobile */ " & _
                        " and rc_tel_no=@tel_no " & _
                        " and rc_plotno=@plotNo "
                'Gajanan [8-April-2019] -- [and present_mobile=@mobile --> /* and present_mobile=@mobile */]

                objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmpUnkid)
                objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress1.ToString)
                objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress2.ToString)
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
                objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
                objDataOperation.AddParameter("@post_townunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPost_Townunkid.ToString)
                objDataOperation.AddParameter("@postcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostcodeunkid.ToString)
                objDataOperation.AddParameter("@provicnce", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrProvicnce.ToString)
                objDataOperation.AddParameter("@road", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRoad.ToString)
                objDataOperation.AddParameter("@estate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEstate.ToString)
                objDataOperation.AddParameter("@provinceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProvinceunkid.ToString)
                objDataOperation.AddParameter("@roadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoadunkid.ToString)
                objDataOperation.AddParameter("@chiefdomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChiefdomunkid.ToString)
                objDataOperation.AddParameter("@villageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVillageunkid.ToString)
                objDataOperation.AddParameter("@town1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTown1unkid.ToString)
                'Gajanan [8-April-2019] -- Start
                'objDataOperation.AddParameter("@mobile", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobile.ToString)
                'Gajanan [8-April-2019] -- End
                objDataOperation.AddParameter("@tel_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTel_No.ToString)
                objDataOperation.AddParameter("@plotNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPlotno.ToString)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isAddressInApproval(ByVal intEmpUnkid As Integer, ByVal xaddressType As Integer, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing _
                           ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  employeeunkid " & _
              " FROM hremployee_address_approval_tran " & _
              " WHERE employeeunkid = @employeeunkid and addresstype=@addresstype and isprocessed=0 and isfinal=0"

            If xaddressType = CInt(enAddressType.PRESENT) Then
                objDataOperation.AddParameter("@addresstype", SqlDbType.Int, eZeeDataType.INT_SIZE, xaddressType)

            ElseIf xaddressType = CInt(enAddressType.DOMICILE) Then
                objDataOperation.AddParameter("@addresstype", SqlDbType.Int, eZeeDataType.INT_SIZE, xaddressType)

            ElseIf xaddressType = CInt(enAddressType.RECRUITMENT) Then
                objDataOperation.AddParameter("@addresstype", SqlDbType.Int, eZeeDataType.INT_SIZE, xaddressType)
            End If
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    Public Function isAddressNewlyAdded(ByVal intEmpUnkid As Integer, ByVal xaddressType As Integer, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing _
                           ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
              "  employeeunkid " & _
              " FROM hremployee_master " & _
              " WHERE "


            If xaddressType = CInt(enAddressType.PRESENT) Then
                strQ &= " employeeunkid = @employeeunkid " & _
                        " and present_address1 = ''" & _
                        " and present_address2=''" & _
                        " and present_countryunkid =0 " & _
                        " and present_stateunkid=0 " & _
                        " and present_post_townunkid=0 " & _
                        " and present_postcodeunkid=0 " & _
                        " and present_provicnce='' " & _
                        " and present_road='' " & _
                        " and present_estate='' " & _
                        " and present_provinceunkid=0 " & _
                        " and present_roadunkid=0 " & _
                        " and present_chiefdomunkid=0 " & _
                        " and present_villageunkid=0 " & _
                        " and present_town1unkid=0 " & _
                        " and present_mobile='' " & _
                        " and present_tel_no='' " & _
                        " and present_plotNo='' " & _
                        " and present_alternateno='' " & _
                        " and present_email='' " & _
                        " and present_fax='' "


            ElseIf xaddressType = CInt(enAddressType.DOMICILE) Then
                strQ &= " employeeunkid = @employeeunkid " & _
                        " and domicile_address1 =''" & _
                        " and domicile_address2='' " & _
                        " and domicile_countryunkid =0 " & _
                        " and domicile_stateunkid=0 " & _
                        " and domicile_post_townunkid=0 " & _
                        " and domicile_postcodeunkid=0 " & _
                        " and domicile_provicnce='' " & _
                        " and domicile_road='' " & _
                        " and domicile_estate='' " & _
                        " and domicile_provinceunkid=0 " & _
                        " and domicile_roadunkid=0 " & _
                        " and domicile_chiefdomunkid=0 " & _
                        " and domicile_villageunkid=0 " & _
                        " and domicile_town1unkid=0 " & _
                        " and domicile_mobile='' " & _
                        " and domicile_tel_no='' " & _
                        " and domicile_plotNo='' " & _
                        " and domicile_alternateno='' " & _
                        " and domicile_email='' " & _
                        " and domicile_fax='' "


            ElseIf xaddressType = CInt(enAddressType.RECRUITMENT) Then
                strQ &= " employeeunkid = @employeeunkid " & _
                        " and rc_address1 =''  " & _
                        " and rc_address2='' " & _
                        " and rc_countryunkid =0 " & _
                        " and rc_stateunkid=0 " & _
                        " and rc_post_townunkid=0 " & _
                        " and rc_postcodeunkid=0 " & _
                        " and rc_province='' " & _
                        " and rc_road='' " & _
                        " and rc_estate='' " & _
                        " and recruitment_provinceunkid=0 " & _
                        " and recruitment_roadunkid=0 " & _
                        " and recruitment_chiefdomunkid=0 " & _
                        " and recruitment_villageunkid=0 " & _
                        " and recruitment_town1unkid=0 " & _
                        " and present_mobile='' " & _
                        " and rc_tel_no='' " & _
                        " and rc_plotno='' "
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmpUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 50, "Selected present address is already assigned to the employee. Please assign new present address.")
			Language.setMessage(mstrModuleName, 51, "Selected domicile address is already assigned to the employee. Please assign new domicile address.")
			Language.setMessage(mstrModuleName, 52, "Selected recruitment address is already assigned to the employee. Please assign new recruitment address.")
			Language.setMessage(mstrModuleName, 53, "Sorry, you cannot add seleted information, Reason : Same present address is already present in approval process.")
			Language.setMessage(mstrModuleName, 54, "Sorry, you cannot add seleted information, Reason : Same domicile address is already present in approval process.")
			Language.setMessage(mstrModuleName, 55, "Sorry, you cannot add seleted information, Reason : Same recruitment address is already present in approval process.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

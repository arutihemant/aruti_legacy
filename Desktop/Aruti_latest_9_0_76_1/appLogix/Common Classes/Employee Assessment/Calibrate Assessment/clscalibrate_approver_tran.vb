﻿Imports eZeeCommonLib

Public Class clscalibrate_approver_tran
    Private Shared ReadOnly mstrModuleName As String = "clscalibrate_approver_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change
#Region "ENUM"
    Public Enum enCalibrationApproverVisibilityType
        VISIBLE = 1
        MIGRATE = 2
        SWAPPED = 3
    End Enum
#End Region
    'Gajanan [15-April-2020] -- End


#Region " Private variables "
    Private mintMappingtranunkid As Integer = -1
    Private mintMappingunkid As Integer = -1
    Private mintUserunkid As Integer = -1
    Private mdtTran As DataTable
    Private mintemployeeunkid As Integer = -1

    Private minAuditUserid As Integer
    Private minAuditDate As DateTime
    Private minClientIp As String
    Private minloginemployeeunkid As Integer
    Private mstrHostName As String
    Private mstrFormName As String
    Private blnIsFromWeb As Boolean

#End Region

#Region " Properties "

    Public Property _Mappingtranunkid() As Integer
        Get
            Return mintMappingtranunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingtranunkid = value
        End Set
    End Property

    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Employeeunkid() As Integer
        Get
            Return mintemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintemployeeunkid = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _AuditUserid() As Integer
        Get
            Return minAuditUserid
        End Get
        Set(ByVal value As Integer)
            minAuditUserid = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return minAuditDate
        End Get
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public Property _ClientIp() As String
        Get
            Return minClientIp
        End Get
        Set(ByVal value As String)
            minClientIp = value
        End Set
    End Property

    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

    Public Property _loginemployeeunkid() As Integer
        Get
            Return minloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            minloginemployeeunkid = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("ApproverTran")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("mappingtranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("mappingunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region


    Public Function Insert_CalibarteApproverData(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            End If
            objDataOperation.ClearParameters()

            If mdtTran Is Nothing Then Return True

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    strQ = "Select count(1) as 'Countemp' From hrscore_calibration_approver_tran " & _
                           " WHERE mappingunkid = @mappingunkid AND employeeunkid = @empunkid AND hrscore_calibration_approver_tran.isvoid = 0 " & _
                           "AND visibletypeid = 1 "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid)
                    objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)

                    Dim dscount As DataSet = objDataOperation.ExecQuery(strQ, "Count")
                    If Not dscount Is Nothing And CInt(dscount.Tables("Count").Rows(0)("Countemp")) > 0 Then Continue For

                    strQ = "INSERT INTO hrscore_calibration_approver_tran ( " & _
                                "  mappingunkid " & _
                                ", employeeunkid " & _
                                ", visibletypeid " & _
                                ", isvoid " & _
                                ", voiduserunkid " & _
                            ") VALUES (" & _
                                "  @mappingunkid " & _
                                ", @employeeunkid " & _
                                ", @visibletypeid " & _
                                ", @isvoid " & _
                                ", @voiduserunkid " & _
                                "); SELECT @@identity"

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                    objDataOperation.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enCalibrationApproverVisibilityType.VISIBLE))
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(0))
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintMappingtranunkid = dsList.Tables(0).Rows(0).Item(0)
                    mintemployeeunkid = CInt(.Item("employeeunkid").ToString())


                    If Insert_AtTranLog(objDataOperation, 1, mintMappingtranunkid, mintMappingunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End With
            Next

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If

            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete_GrievanceApproverData; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    Public Function Delete_CalibarteApproverData(ByVal mdicEmployee As Dictionary(Of Integer, Integer), ByVal xDataOpr As clsDataOperation) As Boolean
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            End If
            objDataOperation.ClearParameters()


            For i As Integer = 0 To mdicEmployee.Count - 1
                strQ = " Update hrscore_calibration_approver_tran set " & _
                            " isvoid = 1,voiddatetime=GETDATE(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                            " WHERE mappingtranunkid = @mappingtranunkid AND employeeunkid = @employeeunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@mappingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mdicEmployee.Keys(i))
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mdicEmployee.Values(i))
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 2, "UnAssign"))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintemployeeunkid = CInt(mdicEmployee.Values(i))
                mintMappingtranunkid = CInt(mdicEmployee.Keys(i))

                If Insert_AtTranLog(objDataOperation, 3, mintMappingtranunkid, mintMappingunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete_GrievanceApproverData; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer, ByVal intUnkid As Integer, ByVal masterUnkid As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO athrscore_calibration_approver_tran( " & _
             "  row_guid " & _
             ", mappingtranunkid " & _
             ", mappingunkid " & _
             ", employeeunkid " & _
             ", visibletypeid " & _
             ", audittype " & _
             ", auditdatetime " & _
             ", audituserunkid " & _
             ", loginemployeeunkid " & _
             ", ip " & _
             ", host " & _
             ", form_name " & _
             ", isweb " & _
           ") VALUES (" & _
             "  @row_guid " & _
             ", @mappingtranunkid " & _
             ", @mappingunkid " & _
             ", @employeeunkid " & _
             ", @visibletypeid " & _
             ", @audittype " & _
             ", GETDATE()  " & _
             ", @audituserunkid " & _
             ", @loginemployeeunkid " & _
             ", @ip " & _
             ", @host " & _
             ", @form_name " & _
             ", @isweb )"

            objDoOps.ClearParameters()
            objDoOps.AddParameter("@row_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDoOps.AddParameter("@mappingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDoOps.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, masterUnkid)
            objDoOps.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintemployeeunkid)
            objDoOps.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserid)
            objDoOps.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enCalibrationApproverVisibilityType.VISIBLE))
            objDoOps.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception

            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    Public Function GetApproverTran(ByVal mdtEmployeeAsonDate As Date, ByVal intApproverunkid As Integer, Optional ByVal xDataOperation As clsDataOperation = Nothing, Optional ByVal isblank As Boolean = False, Optional ByVal filterstring As String = "") As DataTable

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            If isblank Then

                strQ = "SELECT " & _
                      " mappingtranunkid  " & _
                      ", 0 as mappingunkid " & _
                      ", '' as approvername" & _
                      ", 0 as employeeunkid " & _
                      ", '' as name" & _
                      ", 0 as Alloc.departmentunkid " & _
                      ", '' as  departmentname " & _
                      ", 0 as Jobs.jobunkid " & _
                      ", '' as  jobname " & _
                      ", '' as AUD ,'' as GUID,CAST(0 as BIT) as IsCheck " & _
                      " FROM hrscore_calibration_approver_tran " & _
                      " Union All "
            End If

            strQ &= "SELECT " & _
              "  mappingtranunkid " & _
              ", hrscore_calibration_approver_tran.mappingunkid as approverunkid" & _
              ", '' as approvername" & _
              ", hrscore_calibration_approver_tran.employeeunkid " & _
              ", isnull(Emp2.employeecode,'')+ '-' +  isnull(Emp2.firstname,'') + ' ' + isnull(Emp2.surname,'') as name" & _
              ", Alloc.departmentunkid " & _
              ", hrdepartment_master.name as departmentname " & _
              ", Jobs.jobunkid " & _
              ", hrjob_master.job_name as jobname " & _
              ", '' as AUD ,'' as GUID,CAST(0 as BIT) as IsCheck " & _
              " FROM hrscore_calibration_approver_tran " & _
              " LEFT JOIN hrscore_calibration_approver_master on hrscore_calibration_approver_master.mappingunkid = hrscore_calibration_approver_tran.mappingunkid " & _
              " LEFT JOIN hremployee_master Emp2 on hrscore_calibration_approver_tran.employeeunkid = Emp2.employeeunkid " & _
              " LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = Emp2.employeeunkid AND Alloc.rno = 1 " & _
              " LEFT JOIN hrdepartment_master on hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
              " LEFT JOIN " & _
              " ( " & _
              "    SELECT " & _
              "         jobunkid " & _
              "        ,jobgroupunkid " & _
              "        ,employeeunkid " & _
              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
              "    FROM hremployee_categorization_tran " & _
              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
              " ) AS Jobs ON Jobs.employeeunkid = Emp2.employeeunkid AND Jobs.rno = 1 " & _
              " LEFT JOIN hrjob_master on hrjob_master.jobunkid = Jobs.jobunkid " & _
              " WHERE ISNULL(hrscore_calibration_approver_tran.isvoid,0) = 0"

            If intApproverunkid > 0 Then
                strQ &= " AND hrscore_calibration_approver_tran.mappingunkid = @approvermasterunkid"
                objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)
            End If


            If filterstring <> "" Then
                strQ &= " AND" + filterstring
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("List")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverTran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOperation Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

    Public Function GetAssignedEmpIds(ByVal blnIsCalibrator As Boolean) As String
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Dim strIds As String = ""
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT ISNULL(STUFF(( " & _
                       "SELECT DISTINCT ',' + " & _
                       "    CAST(CAT.employeeunkid AS NVARCHAR(50)) " & _
                       "FROM hrscore_calibration_approver_tran AS CAT " & _
                       "    JOIN hrscore_calibration_approver_master AS CAM ON CAM.mappingunkid = CAT.mappingunkid " & _
                       "WHERE CAT.isvoid = 0 AND CAT.visibletypeid = 1 AND CAM.isvoid = 0 AND CAM.visibletypeid = 1 " & _
                       "AND CAM.iscalibrator = " & IIf(blnIsCalibrator = True, 1, 0) & " FOR XML PATH('')),1,1,''),'') AS ids "
                Dim ds As New DataSet
                ds = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0)("ids").ToString().Trim.Length > 0 Then
                        strIds = ds.Tables(0).Rows(0)("ids").ToString().Trim
                    End If
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strIds
    End Function

    Public Function GetMappedEmployeecombolist(ByVal xDatabaseName As String, _
                                               ByVal intUserId As Integer, _
                                               ByVal blnIsCalibrator As Boolean, _
                                               ByVal dtEmpAsOnDate As String, _
                                               Optional ByVal strList As String = "List", _
                                               Optional ByVal blnAddSelect As Boolean = False, _
                                               Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet

        Dim objDataOperation As clsDataOperation = Nothing
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try
            Dim xDateJoinQry, xDateFilterQry As String : xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dtEmpAsOnDate), eZeeDate.convertDate(dtEmpAsOnDate), , , xDatabaseName, "EM")
            If blnAddSelect Then
                StrQ &= "SELECT 0 AS Id ,@Select AS Name,0 AS mappingtranunkid,'' AS ecode, '' AS ename UNION ALL "
            End If
            StrQ &= "SELECT " & _
                    "    EM.employeeunkid AS Id " & _
                    "   ,EM.employeecode + ' - ' + EM.firstname+ ' ' + EM.surname AS Name " & _
                    "   ,CAT.mappingtranunkid " & _
                    "   ,EM.employeecode ecode " & _
                    "   ,EM.firstname+ ' ' + EM.surname AS ename " & _
                    "FROM hrscore_calibration_approver_master AS CAM " & _
                    "   JOIN hrscore_calibration_approver_tran AS CAT ON CAM.mappingunkid = CAT.mappingunkid " & _
                    "   JOIN hremployee_master AS EM ON CAT.employeeunkid = EM.employeeunkid "
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            StrQ &= "WHERE CAM.isvoid = 0 AND CAM.isactive = 1 AND CAM.visibletypeid = 1 " & _
                    "AND CAT.isvoid = 0 AND CAT.visibletypeid = 1 AND CAM.mapuserunkid = @mapuserunkid " & _
                    "AND CAM.iscalibrator = @iscalibrator "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If

            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            objDataOperation.AddParameter("@iscalibrator", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsCalibrator)
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            If strList.Trim.Length <= 0 Then strList = "list"

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function


    'S.SANDEEP |01-JUN-2020| -- START
    'ISSUE/ENHANCEMENT : Import CALIBRATION CHANGES
    Public Function IsValidUserType(ByVal intUserId As Integer, ByVal blnIsCalibrator As Boolean) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Dim blnFlag As Boolean = False
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT 1 " & _
                       "FROM hrscore_calibration_approver_master AS CAM " & _
                       "WHERE CAM.isvoid = 0 AND CAM.isactive = 1 AND CAM.mapuserunkid = @mapuserunkid AND CAM.iscalibrator = @iscalibrator " & _
                       "AND CAM.visibletypeid = 1 "

                objDo.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
                objDo.AddParameter("@iscalibrator", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsCalibrator)

                If objDo.RecordCount(StrQ) > 0 Then
                    blnFlag = True
                End If

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidUserType; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function

    Public Function IsValidMapping(ByVal intUserId As Integer, ByVal intEmpId As Integer, ByVal blnIsCalibrator As Boolean) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Dim blnFlag As Boolean = False
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT 1 " & _
                       "FROM hrscore_calibration_approver_master AS CAM " & _
                       "JOIN hrscore_calibration_approver_tran AS CAT ON CAM.mappingunkid = CAT.mappingunkid " & _
                       "WHERE CAM.isvoid = 0 AND CAM.isactive = 1 AND CAM.mapuserunkid = @mapuserunkid AND CAM.iscalibrator = @iscalibrator " & _
                       "AND CAM.visibletypeid = 1 AND CAT.isvoid = 0 AND CAT.visibletypeid = 1 " & _
                       "AND CAT.employeeunkid = @employeeunkid "

                objDo.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
                objDo.AddParameter("@iscalibrator", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsCalibrator)
                objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)

                If objDo.RecordCount(StrQ) > 0 Then
                    blnFlag = True
                End If

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidMapping; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return blnFlag
    End Function
    'S.SANDEEP |01-JUN-2020| -- END


    'S.SANDEEP |19-JUN-2020| -- START
    'ISSUE/ENHANCEMENT : MIGRATION PROCESS
    Public Function MigrateEmployee(ByVal intFromUserId As Integer, _
                                    ByVal intToUserId As Integer, _
                                    ByVal strEmpIds As String(), _
                                    ByVal blnIsCalibrator As Boolean, _
                                    ByVal intUserId As Integer, _
                                    ByVal strIPAddr As String, _
                                    ByVal strHostName As String, _
                                    ByVal strScreenName As String, _
                                    ByVal blnIsWeb As Boolean, _
                                    ByVal eOprType As clsAssessor_tran.enOperationType, _
                                    ByVal strUserName As String, _
                                    ByVal strSenderAddress As String, _
                                    ByVal intCompanyId As Integer, _
                                    Optional ByVal xDataOperation As clsDataOperation = Nothing) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Dim intMapFrmId, intMapToId, intFrmTotalEmp As Integer
        Dim objDataOperation As clsDataOperation = Nothing
        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Dim objMigration As New clsMigration
        Dim blnFullMigrate As Boolean = False
        Try
            If strEmpIds.Length > 0 Then
                intMapFrmId = 0 : intMapToId = 0 : intFrmTotalEmp = 0

                For idx As Integer = 1 To 2
                    objDataOperation.ClearParameters()

                    StrQ = "SELECT @mappingunkid = mappingunkid FROM hrscore_calibration_approver_master WHERE mapuserunkid = @mapuserunkid AND isactive = 1 AND isvoid = 0 AND iscalibrator = @iscalibrator "

                    objDataOperation.AddParameter("@iscalibrator", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsCalibrator)
                    Select Case idx
                        Case 1

                            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMapFrmId, ParameterDirection.InputOutput)
                            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFromUserId)

                            objDataOperation.ExecNonQuery(StrQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            intMapFrmId = objDataOperation.GetParameterValue("@mappingunkid")

                            StrQ = "SELECT mappingunkid FROM hrscore_calibration_approver_tran WHERE mappingunkid = @mappingunkid AND isvoid = 0 AND visibletypeid = 1 "
                            objDataOperation.ClearParameters()

                            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMapFrmId)

                            intFrmTotalEmp = objDataOperation.RecordCount(StrQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If intFrmTotalEmp = strEmpIds.Length Then
                                blnFullMigrate = True
                            End If

                        Case 2

                            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMapToId, ParameterDirection.InputOutput)
                            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intToUserId)

                            objDataOperation.ExecNonQuery(StrQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            intMapToId = objDataOperation.GetParameterValue("@mappingunkid")

                    End Select
                Next

                If intMapFrmId > 0 AndAlso intMapToId > 0 Then
                    Dim iCSVEmpId As String = String.Join(",", strEmpIds)

                    StrQ = "IF OBJECT_ID(N'tempdb..#Emps') IS NOT NULL " & _
                           "DROP TABLE #Emps " & _
                           "CREATE TABLE #Emps (empcode nvarchar(max),empid int default(0)) " & _
                           "DECLARE @DATA NVARCHAR(MAX) = '" & iCSVEmpId & "' " & _
                           "DECLARE @CNT INT " & _
                           "DECLARE @SEP AS CHAR(1) " & _
                           "SET @SEP = ',' " & _
                           "SET @CNT = 1 " & _
                           "WHILE (CHARINDEX(@SEP,@DATA)>0) " & _
                           "    BEGIN " & _
                           "        INSERT INTO #Emps (empcode) " & _
                           "        SELECT DATA = LTRIM(RTRIM(SUBSTRING(@DATA,1,CHARINDEX(@SEP,@DATA)-1))) " & _
                           "        SET @DATA = SUBSTRING(@DATA,CHARINDEX(@SEP,@DATA)+1,LEN(@DATA)) " & _
                           "        SET @CNT = @CNT + 1 " & _
                           "    END " & _
                           "INSERT INTO #Emps (empcode) " & _
                           "SELECT DATA = LTRIM(RTRIM(@DATA)) " & _
                           " " & _
                           "UPDATE #Emps SET empid = employeeunkid FROM hremployee_master WHERE empcode = employeecode " & _
                           " " & _
                           " /*STEP 1 { MIGRATING THEM }*/ " & _
                           "UPDATE hrscore_calibration_approver_tran " & _
                           "SET visibletypeid = '" & CInt(clscalibrate_approver_tran.enCalibrationApproverVisibilityType.MIGRATE) & "' " & _
                           "FROM " & _
                           "( " & _
                           "    SELECT " & _
                           "        CT.mappingtranunkid " & _
                           "    FROM #Emps " & _
                           "        JOIN hrscore_calibration_approver_tran AS CT ON #Emps.empid = CT.employeeunkid " & _
                           "    WHERE CT.mappingunkid = '" & intMapFrmId & "' AND visibletypeid = 1 " & _
                           ") AS A WHERE A.mappingtranunkid = hrscore_calibration_approver_tran.mappingtranunkid " & _
                           "AND NOT EXISTS(SELECT * FROM hrscore_calibration_approver_tran AS CT WHERE CT.isvoid = 0 AND CT.visibletypeid = 1 AND CT.employeeunkid = hrscore_calibration_approver_tran.employeeunkid AND CT.mappingunkid = '" & intMapToId & "') " & _
                           " /*STEP 2 { INSERTING AUDIT TRAILS }*/ " & _
                           "INSERT INTO athrscore_calibration_approver_tran(row_guid,mappingtranunkid,mappingunkid,employeeunkid,visibletypeid,audittype,auditdatetime,audituserunkid,loginemployeeunkid,ip,host,form_name,isweb) " & _
                           "SELECT DISTINCT LOWER(NEWID()),mappingtranunkid,mappingunkid,employeeunkid,'" & CInt(clscalibrate_approver_tran.enCalibrationApproverVisibilityType.MIGRATE) & "',2,GETDATE(),'" & intUserId & "',0,'" & strIPAddr & "','" & strHostName & "','" & strScreenName & "',CAST('" & IIf(blnIsWeb = True, 1, 0) & "' AS BIT) " & _
                           "FROM #Emps " & _
                           "JOIN hrscore_calibration_approver_tran AS CT ON #Emps.empid = CT.employeeunkid " & _
                           "WHERE CT.mappingunkid = '" & intMapFrmId & "' " & _
                           " /*STEP 3 { TRANSFERING TO NEW APPROVER }*/ " & _
                           "INSERT INTO hrscore_calibration_approver_tran(mappingunkid,employeeunkid,isvoid,voiddatetime,voiduserunkid,voidreason,visibletypeid) " & _
                           "SELECT DISTINCT " & _
                           "'" & intMapToId & "',#Emps.empid,0,NULL,0,'','" & CInt(clscalibrate_approver_tran.enCalibrationApproverVisibilityType.VISIBLE) & "' " & _
                           "FROM #Emps " & _
                           "JOIN hrscore_calibration_approver_tran AS CT ON #Emps.empid = CT.employeeunkid " & _
                           "WHERE CT.mappingunkid = '" & intMapFrmId & "' AND visibletypeid = 2 " & _
                           "AND NOT EXISTS(SELECT * FROM hrscore_calibration_approver_tran AS CT WHERE CT.isvoid = 0 AND CT.visibletypeid = 1 AND CT.employeeunkid = #Emps.empid AND CT.mappingunkid = '" & intMapToId & "') " & _
                           " /*STEP 4 { INSERTING AUDIT TRAILS }*/ " & _
                           "INSERT INTO athrscore_calibration_approver_tran(row_guid,mappingtranunkid,mappingunkid,employeeunkid,visibletypeid,audittype,auditdatetime,audituserunkid,loginemployeeunkid,ip,host,form_name,isweb) " & _
                           "SELECT DISTINCT LOWER(NEWID()),mappingtranunkid,mappingunkid,employeeunkid,'" & CInt(clscalibrate_approver_tran.enCalibrationApproverVisibilityType.VISIBLE) & "',1,GETDATE(),'" & intUserId & "',0,'" & strIPAddr & "','" & strHostName & "','" & strScreenName & "',CAST('" & IIf(blnIsWeb = True, 1, 0) & "' AS BIT) " & _
                           "FROM #Emps " & _
                           "JOIN hrscore_calibration_approver_tran AS CT ON #Emps.empid = CT.employeeunkid " & _
                           "WHERE CT.mappingunkid = '" & intMapToId & "' " & _
                           " " & _
                           "IF OBJECT_ID(N'tempdb..#Emps') IS NOT NULL " & _
                           "DROP TABLE #Emps "

                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If blnFullMigrate Then
                        StrQ = "UPDATE hrscore_calibration_approver_master SET visibletypeid = '" & CInt(clscalibrate_approver_tran.enCalibrationApproverVisibilityType.MIGRATE) & "' WHERE mappingunkid = '" & intMapFrmId & "' " & _
                               "INSERT INTO athrscore_calibration_approver_master(tranguid,mappingunkid,levelunkid,mapuserunkid,isactive,audittype,audituserunkid,auditdatetime,ip,hostname,form_name,isweb,visibletypeid,iscalibrator) " & _
                               "SELECT LOWER(NEWID()),mappingunkid,levelunkid,mapuserunkid,isactive,2,'" & intUserId & "',GETDATE(),'" & strIPAddr & "','" & strHostName & "','" & strScreenName & "',CAST('" & IIf(blnIsFromWeb = True, 1, 0) & "' AS BIT),'" & CInt(clscalibrate_approver_tran.enCalibrationApproverVisibilityType.MIGRATE) & "',CAST('" & IIf(blnIsCalibrator = True, 1, 0) & "' AS BIT) " & _
                               "FROM hrscore_calibration_approver_master WHERE mappingunkid = '" & intMapFrmId & "' "

                        objDataOperation.ExecNonQuery(StrQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                    StrQ = "IF OBJECT_ID(N'tempdb..#Emps') IS NOT NULL " & _
                           "DROP TABLE #Emps " & _
                           "CREATE TABLE #Emps (empcode nvarchar(max),empid int default(0)) " & _
                           "DECLARE @DATA NVARCHAR(MAX) = '" & iCSVEmpId & "' " & _
                           "DECLARE @CNT INT " & _
                           "DECLARE @SEP AS CHAR(1) " & _
                           "SET @SEP = ',' " & _
                           "SET @CNT = 1 " & _
                           "WHILE (CHARINDEX(@SEP,@DATA)>0) " & _
                           "BEGIN " & _
                           "INSERT INTO #Emps (empcode) " & _
                           "SELECT DATA = LTRIM(RTRIM(SUBSTRING(@DATA,1,CHARINDEX(@SEP,@DATA)-1))) " & _
                           "SET @DATA = SUBSTRING(@DATA,CHARINDEX(@SEP,@DATA)+1,LEN(@DATA)) " & _
                           "SET @CNT = @CNT + 1 " & _
                           "END " & _
                           "INSERT INTO #Emps (empcode) " & _
                           "SELECT DATA = LTRIM(RTRIM(@DATA)) " & _
                           "UPDATE #Emps SET empid = employeeunkid FROM hremployee_master WHERE empcode = employeecode "

                    Select Case eOprType
                        Case clsAssessor_tran.enOperationType.Overwrite
                            'StrQ &= "UPDATE hrassess_computescore_approval_tran SET " & _
                            '        "mappingunkid = '" & intMapToId & "', audituserunkid = '" & intToUserId & "' " & _
                            '        "FROM " & _
                            '        "( " & _
                            '        "    SELECT " & _
                            '        "         CAT.tranguid " & _
                            '        "        ,CAT.calibratnounkid " & _
                            '        "        ,CAT.periodunkid " & _
                            '        "        ,CM.mapuserunkid " & _
                            '        "        ,CASE WHEN CM.iscalibrator = 1 THEN CAT.mappingunkid ELSE CM.mappingunkid END mappingunkid " & _
                            '        "        ,CT.employeeunkid " & _
                            '        "    FROM hrassess_computescore_approval_tran AS CAT " & _
                            '        "        JOIN #Emps ON #Emps.empid = CAT.employeeunkid " & _
                            '        "        JOIN hrscore_calibration_approver_tran AS CT ON CAT.employeeunkid = CT.employeeunkid " & _
                            '        "        JOIN hrscore_calibration_approver_master AS CM ON CM.mappingunkid = CT.mappingunkid " & _
                            '        "    WHERE CAT.isvoid = 0 AND CT.isvoid = 0 AND CT.visibletypeid = 1 " & _
                            '        "    AND CM.visibletypeid = 1 AND CM.iscalibrator = @iscalibrator AND CAT.audituserunkid = @audituserunkid " & _
                            '        "    AND CAT.isprocessed = 0 " & _
                            '        ") AS A WHERE A.tranguid = hrassess_computescore_approval_tran.tranguid AND A.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                            '        "AND A.periodunkid = hrassess_computescore_approval_tran.periodunkid AND A.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                            '        "AND hrassess_computescore_approval_tran.isvoid = 0 "

                        Case clsAssessor_tran.enOperationType.Void
                            StrQ &= "UPDATE hrassess_computescore_approval_tran SET " & _
                                    "isvoid = 1,voidreason = @voidreason " & _
                                    "FROM " & _
                                    "( " & _
                                    "    SELECT " & _
                                    "         CAT.tranguid " & _
                                    "        ,CAT.calibratnounkid " & _
                                    "        ,CAT.periodunkid " & _
                                    "        ,CT.employeeunkid " & _
                                    "    FROM hrassess_computescore_approval_tran AS CAT " & _
                                    "        JOIN #Emps ON #Emps.empid = CAT.employeeunkid " & _
                                    "        JOIN hrscore_calibration_approver_tran AS CT ON CAT.employeeunkid = CT.employeeunkid " & _
                                    "        JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = CAT.periodunkid " & _
                                    "    WHERE CAT.isvoid = 0 AND CT.isvoid = 0 AND CT.visibletypeid = 1 " & _
                                    "    /* AND CM.visibletypeid = 1 AND CM.iscalibrator = @iscalibrator AND CAT.audituserunkid = @audituserunkid */ " & _
                                    "    AND CAT.isprocessed = 0 AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.statusid = 1 " & _
                                    ") AS A WHERE A.tranguid = hrassess_computescore_approval_tran.tranguid AND A.calibratnounkid = hrassess_computescore_approval_tran.calibratnounkid " & _
                                    "AND A.periodunkid = hrassess_computescore_approval_tran.periodunkid AND A.employeeunkid = hrassess_computescore_approval_tran.employeeunkid " & _
                                    "AND hrassess_computescore_approval_tran.isvoid = 0; "
                            If blnIsCalibrator Then
                                StrQ &= "UPDATE hrassess_compute_score_master SET " & _
                                        "   issubmitted = 0, submit_date = NULL, calibrated_score=0, calibratnounkid=0, calibration_remark='', overall_remark = '' " & _
                                        "FROM " & _
                                        "( " & _
                                        "   SELECT " & _
                                        "       computescoremasterunkid " & _
                                        "   FROM hrassess_compute_score_master " & _
                                        "       JOIN #Emps ON #Emps.empid = hrassess_compute_score_master.employeeunkid " & _
                                        "       JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrassess_compute_score_master.periodunkid " & _
                                        "   WHERE isactive = 1 AND statusid = 1 AND isprocessed = 0 " & _
                                        ") AS A WHERE hrassess_compute_score_master.computescoremasterunkid = A.computescoremasterunkid " & _
                                        "AND isvoid = 0 AND isprocessed = 0 "
                            Else
                                StrQ &= "UPDATE hrassess_compute_score_master SET " & _
                                        "   issubmitted = 0,submit_date = NULL " & _
                                        "FROM " & _
                                        "( " & _
                                        "   SELECT " & _
                                        "       computescoremasterunkid " & _
                                        "   FROM hrassess_compute_score_master " & _
                                        "       JOIN #Emps ON #Emps.empid = hrassess_compute_score_master.employeeunkid " & _
                                        "       JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrassess_compute_score_master.periodunkid " & _
                                        "   WHERE isactive = 1 AND statusid = 1 AND isprocessed = 0 " & _
                                        ") AS A WHERE hrassess_compute_score_master.computescoremasterunkid = A.computescoremasterunkid " & _
                                        "AND isvoid = 0 AND isprocessed = 0 "
                            End If
                    End Select

                    StrQ &= "IF OBJECT_ID(N'tempdb..#Emps') IS NOT NULL " & _
                            "DROP TABLE #Emps "

                    objDataOperation.ClearParameters()
                    If blnIsCalibrator Then
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Migrate to another calibrator"))
                    Else
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Migrate to another approver"))
                    End If
                    objDataOperation.AddParameter("@iscalibrator", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsCalibrator)
                    objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFromUserId)

                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    With objMigration
                        ._Migratedemployees = iCSVEmpId
                        ._Migrationdate = Now
                        ._Migrationfromunkid = intMapFrmId
                        ._Migrationtounkid = intMapToId
                        ._Usertypeid = enUserType.Calibration_Approver
                        ._Userunkid = intUserId
                        If .Insert(objDataOperation, intUserId) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End With

                    If xDataOperation Is Nothing Then
                        objDataOperation.ReleaseTransaction(True)
                    End If

                    StrQ = "IF OBJECT_ID(N'tempdb..#Emps') IS NOT NULL " & _
                           "DROP TABLE #Emps " & _
                           "CREATE TABLE #Emps (empcode nvarchar(max),empid int default(0), ename nvarchar(max) default('')) " & _
                           "DECLARE @DATA NVARCHAR(MAX) = '" & iCSVEmpId & "' " & _
                           "DECLARE @CNT INT " & _
                           "DECLARE @SEP AS CHAR(1) " & _
                           "SET @SEP = ',' " & _
                           "SET @CNT = 1 " & _
                           "WHILE (CHARINDEX(@SEP,@DATA)>0) " & _
                           "BEGIN " & _
                           "    INSERT INTO #Emps (empcode) " & _
                           "    SELECT DATA = LTRIM(RTRIM(SUBSTRING(@DATA,1,CHARINDEX(@SEP,@DATA)-1))) " & _
                           "    SET @DATA = SUBSTRING(@DATA,CHARINDEX(@SEP,@DATA)+1,LEN(@DATA)) " & _
                           "    SET @CNT = @CNT + 1 " & _
                           "END " & _
                           "INSERT INTO #Emps (empcode) " & _
                           "SELECT DATA = LTRIM(RTRIM(@DATA)) " & _
                           "UPDATE #Emps SET empid = employeeunkid " & _
                           ",ename = empcode + ' - '+ hremployee_master.firstname + ' ' + hremployee_master.surname " & _
                           "FROM hremployee_master WHERE empcode = employeecode " & _
                           "SELECT " & _
                           "     #Emps.* " & _
                           "    ,CASE WHEN ISNULL(UR.firstname,'')+' '+ISNULL(UR.lastname,'') = ' ' THEN UR.username ELSE ISNULL(UR.firstname,'')+' '+ISNULL(UR.lastname,'') END AS Username " & _
                           "    ,ISNULL(UR.email,'') AS email " & _
                           "    ,UR.userunkid AS UsrId " & _
                           "FROM #Emps " & _
                           "    JOIN hrscore_calibration_approver_tran AS CT ON CT.employeeunkid = #Emps.empid " & _
                           "    JOIN hrscore_calibration_approver_master AS CM ON CM.mappingunkid = CT.mappingunkid " & _
                           "    JOIN hrmsConfiguration..cfuser_master UR ON UR.userunkid = CM.mapuserunkid AND UR.isactive = 1 " & _
                           "WHERE CT.isvoid = 0 AND CT.visibletypeid = 1 AND CM.isactive = 1 AND CM.isvoid = 0 AND CM.visibletypeid = 1 " & _
                           "AND CM.iscalibrator = 1 AND ISNULL(UR.email,'') <> '' " & _
                           "IF OBJECT_ID(N'tempdb..#Emps') IS NOT NULL " & _
                           "DROP TABLE #Emps "

                    Dim ds As New DataSet
                    ds = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If ds.Tables("List").Rows.Count > 0 Then
                        Dim xDictUser As New Dictionary(Of String, String)
                        Dim info1 As Globalization.TextInfo = Globalization.CultureInfo.InvariantCulture.TextInfo
                        Dim objMail As New clsSendMail
                        Dim strMsg As String = String.Empty
                        xDictUser = ds.Tables("List").AsEnumerable().Select(Function(row) New With {Key .uname = row.Field(Of String)("Username"), Key .email = row.Field(Of String)("email")}).Distinct().ToDictionary(Function(s) s.uname, Function(s) s.email)
                        For Each iKey As String In xDictUser.Keys
                            Dim strMessage As String = ""
                            If blnIsCalibrator Then
                                objMail._Subject = Language.getMessage(mstrModuleName, 3, "Notification Migrated Emplyoee And Re-Calibration.")
                            Else
                                objMail._Subject = Language.getMessage(mstrModuleName, 4, "Notification Migrated Emplyoee And Submit for Approval.")
                            End If
                            strMessage = "<HTML> <BODY>"
                            strMessage &= "<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>" & Language.getMessage(mstrModuleName, 5, "Dear") & " " & info1.ToTitleCase(iKey.ToLower()) & ",</b></span></p> <BR><BR>"
                            strMessage &= "<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>"
                            strMessage &= Language.getMessage(mstrModuleName, 6, "Please note that, Following employee(s) has been migrated under you,") & " "
                            If blnIsCalibrator Then
                                strMessage &= Language.getMessage(mstrModuleName, 7, "Please re-calibrated them and submit for approval for calibration.") & " "
                            Else
                                strMessage &= Language.getMessage(mstrModuleName, 8, "Please submit them for approval for calibration.") & " "
                            End If
                            strMessage &= "</span></p>"
                            strMessage &= "<BR>"

                            Dim ftab As New DataTable
                            ftab = New DataView(ds.Tables("List"), "Username = '" & iKey & "'", "", DataViewRowState.CurrentRows).ToTable
                            If ftab.Rows.Count > 0 Then
                                strMessage &= "<TABLE border = '1' WIDTH = '50%'>"
                                strMessage &= "<TR WIDTH = '50%' bgcolor= 'SteelBlue'>"
                                strMessage &= "<TD><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 9, "Employee(s)") & "</span></b></TD>"
                                strMessage &= "</TR>"
                                For idx As Integer = 0 To ftab.Rows.Count - 1
                                    strMessage &= "<TR WIDTH = '50%'><TD align = 'LEFT' WIDTH = '50%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & info1.ToTitleCase(ftab.Rows(idx)("ename").ToString().ToLower()) & "</span></TD></TR>"
                                Next
                                strMessage &= "</TABLE>"
                            End If
                            strMessage &= "<BR><BR>"
                            strMessage &= Language.getMessage(mstrModuleName, 9, "Regards,")
                            strMessage &= "<BR>"
                            strMessage &= info1.ToTitleCase((strUserName).ToLower()).ToString()
                            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                            strMessage &= "</BODY></HTML>"
                            objMail._Message = strMessage
                            objMail._ToEmail = xDictUser(iKey)
                            objMail._Form_Name = strScreenName
                            objMail._LogEmployeeUnkid = 0
                            objMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
                            objMail._UserUnkid = intUserId
                            objMail._SenderAddress = strSenderAddress
                            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                            strMsg = objMail.SendMail(intCompanyId)
                        Next
                    End If

                End If

                Return True
            End If
        Catch ex As Exception
            If xDataOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: MigrateEmployee; Module Name: " & mstrModuleName)
        Finally
            If xDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'S.SANDEEP |19-JUN-2020| -- END



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "UnAssign")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

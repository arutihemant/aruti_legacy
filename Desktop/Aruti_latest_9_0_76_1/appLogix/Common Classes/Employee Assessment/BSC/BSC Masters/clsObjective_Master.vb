﻿'************************************************************************************************************************************
'Class Name : clsObjective_Master.vb
'Purpose    :
'Date       :04/01/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsObjective_Master
    Private Shared ReadOnly mstrModuleName As String = "clsObjective_Master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintObjectiveunkid As Integer
    Private mintPerspectiveunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private msinWeight As Decimal
    Private mstrDescription As String = String.Empty
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mintResultgroupUnkid As Integer


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Private mintReferenceunkid As Integer = 0
    Private mintEmployeeunkid As Integer = 0
    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsfinal As Boolean = False
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintPeriodunkid As Integer = 0
    Private mintYearunkid As Integer = 0
    'S.SANDEEP [ 28 DEC 2012 ] -- END

    'S.SANDEEP [ 28 JAN 2014 ] -- START
    Private mstrWebFrmName As String = String.Empty
    'S.SANDEEP [ 28 JAN 2014 ] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set objectiveunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Objectiveunkid() As Integer
        Get
            Return mintObjectiveunkid
        End Get
        Set(ByVal value As Integer)
            mintObjectiveunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perspectiveunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Perspectiveunkid() As Integer
        Get
            Return mintPerspectiveunkid
        End Get
        Set(ByVal value As Integer)
            mintPerspectiveunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Weight() As Decimal
        Get
            Return msinWeight
        End Get
        Set(ByVal value As Decimal)
            msinWeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ResultGroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _ResultGroupunkid() As Integer
        Get
            Return mintResultgroupUnkid
        End Get
        Set(ByVal value As Integer)
            mintResultgroupUnkid = value
        End Set
    End Property


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set ResultGroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Referenceunkid() As Integer
        Get
            Return mintReferenceunkid
        End Get
        Set(ByVal value As Integer)
            mintReferenceunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Allocation Ids
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EmployeeId() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = Value
        End Set
    End Property
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = Value
        End Set
    End Property
    'S.SANDEEP [ 28 DEC 2012 ] -- END

    'S.SANDEEP [ 28 JAN 2014 ] -- START
    Public WriteOnly Property _WebFrmName() As String
        Set(ByVal value As String)
            mstrWebFrmName = value
        End Set
    End Property
    'S.SANDEEP [ 28 JAN 2014 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '  "  objectiveunkid " & _
            '  ", perspectiveunkid " & _
            '  ", code " & _
            '  ", name " & _
            '  ", weight " & _
            '  ", description " & _
            '  ", name1 " & _
            '  ", name2 " & _
            '  ", resultgroupunkid " & _
            '  ", isactive " & _
            ' "FROM hrobjective_master " & _
            ' "WHERE objectiveunkid = @objectiveunkid "

            strQ = "SELECT " & _
              "  objectiveunkid " & _
              ", perspectiveunkid " & _
              ", code " & _
              ", name " & _
              ", weight " & _
              ", description " & _
              ", name1 " & _
              ", name2 " & _
              ", resultgroupunkid " & _
             ", ISNULL(referenceunkid,0) as referenceunkid " & _
             ", ISNULL(employeeunkid,0) as employeeunkid " & _
              ", isactive " & _
              ", isfinal " & _
                      ", periodunkid " & _
                      ", yearunkid " & _
             "FROM hrobjective_master " & _
             "WHERE objectiveunkid = @objectiveunkid "
            'S.SANDEEP [ 28 DEC 2012 ] -- START {periodunkid,yearunkid} -- END

            'Pinkal (20-Jan-2012) -- End

            objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintObjectiveunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintObjectiveunkid = CInt(dtRow.Item("objectiveunkid"))
                mintPerspectiveunkid = CInt(dtRow.Item("perspectiveunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                msinWeight = dtRow.Item("weight")
                mstrDescription = dtRow.Item("description").ToString
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                mintResultgroupUnkid = CInt(dtRow.Item("resultgroupunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))

                'Pinkal (20-Jan-2012) -- Start
                'Enhancement : TRA Changes
                mintReferenceunkid = CInt(dtRow.Item("referenceunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                'Pinkal (20-Jan-2012) -- End

                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mblnIsfinal = CBool(dtRow.Item("isfinal"))
                'S.SANDEEP [ 05 MARCH 2012 ] -- END

                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                'S.SANDEEP [ 28 DEC 2012 ] -- END

                Exit For
            Next



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal strIncludeInactiveEmployee As String = "", _
                            Optional ByVal strEmployeeAsOnDate As String = "", _
                            Optional ByVal strUserAccessLevelFilterString As String = "", Optional ByVal iEmpId As Integer = 0, Optional ByVal iPeriodId As Integer = 0, Optional ByVal sDatabaseName As String = "") As DataSet 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet

        'S.SANDEEP [ 10 APR 2013 ] -- START {iEmpId,iPeriodId} -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            'strQ = "SELECT " & _
            '              "  objectiveunkid " & _
            '              ", perspectiveunkid " & _
            '              ", code " & _
            '              ", name " & _
            '              ", weight " & _
            '              ", description " & _
            '              ", name1 " & _
            '              ", name2 " & _
            '              ", resultgroupunkid " & _
            '              ", isactive " & _
            '              ", CASE WHEN perspectiveunkid = " & enBSCPerspective.FINANCIAL & " THEN @FINANCIAL " & _
            '              "           WHEN perspectiveunkid = " & enBSCPerspective.CUSTOMER & " THEN @CUSTOMER " & _
            '              "           WHEN perspectiveunkid = " & enBSCPerspective.BUSINESS_PROCESS & " THEN @BUSINESS_PROCESS " & _
            '              "           WHEN perspectiveunkid = " & enBSCPerspective.ORGANIZATION_CAPACITY & " THEN @ORGANIZATION_CAPACITY " & _
            '              " END AS  perspective " & _
            '          "FROM hrobjective_master "

            'S.SANDEEP [ 10 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If sDatabaseName.Trim.Length <= 0 Then sDatabaseName = FinancialYear._Object._DatabaseName
            'S.SANDEEP [ 10 APR 2013 ] -- END

            strQ = "SELECT " & _
                       " hrobjective_master.objectiveunkid " & _
                       ", hrobjective_master.perspectiveunkid " & _
                       ", hrobjective_master.code " & _
                       ", hrobjective_master.name " & _
                       ", hrobjective_master.weight " & _
                       ", hrobjective_master.description " & _
                       ", hrobjective_master.name1 " & _
                       ", hrobjective_master.name2 " & _
                       ", hrobjective_master.resultgroupunkid " & _
                       ", ISNULL(hrobjective_master.employeeunkid,0) employeeunkid " & _
                       ", ISNULL(hremployee_master.employeecode,'') employeecode " & _
                       ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') employee " & _
                       ", hrobjective_master.isactive " & _
                          ", CASE WHEN perspectiveunkid = " & enBSCPerspective.FINANCIAL & " THEN @FINANCIAL " & _
                          "           WHEN perspectiveunkid = " & enBSCPerspective.CUSTOMER & " THEN @CUSTOMER " & _
                          "           WHEN perspectiveunkid = " & enBSCPerspective.BUSINESS_PROCESS & " THEN @BUSINESS_PROCESS " & _
                          "           WHEN perspectiveunkid = " & enBSCPerspective.ORGANIZATION_CAPACITY & " THEN @ORGANIZATION_CAPACITY " & _
                          " END AS  perspective " & _
                       ", hrobjective_master.isfinal " & _
                           ", hrobjective_master.periodunkid " & _
                           ", hrobjective_master.yearunkid " & _
                           ", ISNULL(cfcommon_period_tran.period_name,'') AS Period " & _
                           ", ISNULL(cfcommon_period_tran.statusid,2) AS statusid " & _
                       ", ISNULL(STypId,0) AS STypId " & _
                       " FROM " & sDatabaseName & "..hrobjective_master " & _
                       " LEFT JOIN " & sDatabaseName & "..cfcommon_period_tran ON " & sDatabaseName & "..hrobjective_master.periodunkid = " & sDatabaseName & "..cfcommon_period_tran.periodunkid  " & _
                       " LEFT JOIN " & sDatabaseName & "..hremployee_master ON " & sDatabaseName & "..hremployee_master.employeeunkid = " & sDatabaseName & "..hrobjective_master.employeeunkid "
            'S.SANDEEP [ 28 DEC 2012 ] -- START {periodunkid,yearunkid} -- END

            'S.SANDEEP [ 10 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= "LEFT JOIN " & _
                    "( " & _
                         "SELECT EId,YId,PId,STypId FROM " & _
                         "( " & _
                              "SELECT " & _
                                    "employeeunkid AS EId " & _
                                   ",yearunkid AS YId " & _
                                   ",periodunkid AS PId " & _
                                   ",statustypeid AS STypId " & _
                                   ",CONVERT(CHAR(8),status_date,112) AS Status_Date " & _
                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid,yearunkid,periodunkid ORDER BY objectivestatustranunkid DESC) AS RNo " & _
                              "FROM " & sDatabaseName & ".. hrobjective_status_tran " & _
                         ")AS A WHERE RNo = 1 " & _
                    ") AS Obj_Status ON Obj_Status.EId = hrobjective_master.employeeunkid AND Obj_Status.PId = hrobjective_master.periodunkid AND Obj_Status.YId = hrobjective_master.yearunkid "
            'S.SANDEEP [ 10 APR 2013 ] -- END


            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            If strIncludeInactiveEmployee.Trim.Length <= 0 Then
                strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
            End If

            If CBool(strIncludeInactiveEmployee) = False Then
                strQ &= " AND CONVERT(CHAR(8)," & sDatabaseName & "..hremployee_master.appointeddate,112) <= @enddate " & _
                           " AND ISNULL(CONVERT(CHAR(8)," & sDatabaseName & "..hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8)," & sDatabaseName & "..hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8)," & sDatabaseName & "..hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
            End If

            If strUserAccessLevelFilterString = "" Then
                'Sohail (08 May 2015) -- Start
                'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                strQ &= UserAccessLevel._AccessLevelFilterString
                'strQ &= NewAccessLevelFilterString()
                'Sohail (08 May 2015) -- End
            Else
                strQ &= strUserAccessLevelFilterString
            End If
           

            strQ &= " WHERE 1 = 1 "
            'S.SANDEEP [ 27 APRIL 2012 ] -- END

            If blnOnlyActive Then
                strQ &= " AND " & sDatabaseName & "..hrobjective_master.isactive = 1 "
            End If


            'S.SANDEEP [ 10 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If iEmpId > 0 Then
                strQ &= " AND ISNULL(" & sDatabaseName & "..hrobjective_master.employeeunkid,0) = '" & iEmpId & "' "
            End If

            If iPeriodId > 0 Then
                strQ &= " AND " & sDatabaseName & "..hrobjective_master.periodunkid = '" & iPeriodId & "' "
            End If
            'S.SANDEEP [ 10 APR 2013 ] -- END


            objDataOperation.AddParameter("@FINANCIAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 315, "Financial"))
            objDataOperation.AddParameter("@CUSTOMER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 316, "Customer"))
            objDataOperation.AddParameter("@BUSINESS_PROCESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 317, "Business Process"))
            objDataOperation.AddParameter("@ORGANIZATION_CAPACITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 318, "Organization Capacity"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrobjective_master) </purpose>
    Public Function Insert(Optional ByVal intUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Function Insert() As Boolean

        'S.SANDEEP [ 28 DEC 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mstrCode, , mintEmployeeunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
        '    Return False
        'End If

        'If isExist(, mstrName, mintEmployeeunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
        '    Return False
        'End If

        If isExist(mstrCode, , mintEmployeeunkid, , mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
            Return False
        End If

        If isExist(, mstrName, mintEmployeeunkid, , mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
            Return False
        End If
        'S.SANDEEP [ 28 DEC 2012 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Pinkal (20-Jan-2012) -- Start
        'Enhancement : TRA Changes
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()
        'Pinkal (20-Jan-2012) -- End
        Try

            objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerspectiveunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinWeight.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultgroupUnkid.ToString)


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            'S.SANDEEP [ 28 DEC 2012 ] -- END



            'strQ = "INSERT INTO hrobjective_master ( " & _
            '  "  perspectiveunkid " & _
            '  ", code " & _
            '  ", name " & _
            '  ", weight " & _
            '  ", description " & _
            '  ", name1 " & _
            '  ", name2 " & _
            '  ", resultgroupunkid " & _
            '  ", isactive" & _
            '") VALUES (" & _
            '  "  @perspectiveunkid " & _
            '  ", @code " & _
            '  ", @name " & _
            '  ", @weight " & _
            '  ", @description " & _
            '  ", @name1 " & _
            '  ", @name2 " & _
            '  ", @resultgroupunkid " & _
            '  ", @isactive" & _
            '"); SELECT @@identity"

            strQ = "INSERT INTO hrobjective_master ( " & _
              "  perspectiveunkid " & _
              ", code " & _
              ", name " & _
              ", weight " & _
              ", description " & _
              ", name1 " & _
              ", name2 " & _
              ", resultgroupunkid " & _
              ", referenceunkid " & _
              ", employeeunkid " & _
              ", isactive" & _
              ", isfinal " & _
                          ", periodunkid " & _
                          ", yearunkid" & _
            ") VALUES (" & _
              "  @perspectiveunkid " & _
              ", @code " & _
              ", @name " & _
              ", @weight " & _
              ", @description " & _
              ", @name1 " & _
              ", @name2 " & _
              ", @resultgroupunkid " & _
              ", @referenceunkid " & _
              ", @employeeunkid " & _
              ", @isactive" & _
              ", @isfinal" & _
                          ", @periodunkid " & _
                          ", @yearunkid" & _
            "); SELECT @@identity"
            'S.SANDEEP [ 28 DEC 2012 ] -- START {periodunkid,yearunkid} -- END

            'Pinkal (20-Jan-2012) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintObjectiveunkid = dsList.Tables(0).Rows(0).Item(0)


            'S.SANDEEP [ 10 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objStatusTran As New clsobjective_status_tran
            objStatusTran._Assessoremployeeunkid = 0
            objStatusTran._Assessormasterunkid = 0
            objStatusTran._Commtents = ""
            objStatusTran._Employeeunkid = mintEmployeeunkid
            objStatusTran._Periodunkid = mintPeriodunkid
            objStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            objStatusTran._Statustypeid = enObjective_Status.NOT_SUBMIT
            objStatusTran._Yearunkid = mintYearunkid
            'S.SANDEEP [ 23 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objStatusTran._Userunkid = User._Object._Userunkid
            'S.SANDEEP [ 23 JULY 2013 ] -- END
            If objStatusTran.Insert(objDataOperation, intUserUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objStatusTran = Nothing
            'S.SANDEEP [ 10 APR 2013 ] -- END


            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrobjective_master", "objectiveunkid", mintObjectiveunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrobjective_master", "objectiveunkid", mintObjectiveunkid, , intUserUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END

            objDataOperation.ReleaseTransaction(True)

            'Pinkal (20-Jan-2012) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrobjective_master) </purpose>
    Public Function Update(Optional ByVal intUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Function Update() As Boolean

        'S.SANDEEP [ 28 DEC 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mstrCode, , mintEmployeeunkid, mintObjectiveunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
        '    Return False
        'End If

        'If isExist(, mstrName, mintEmployeeunkid, mintObjectiveunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
        '    Return False
        'End If

        If isExist(mstrCode, , mintEmployeeunkid, mintObjectiveunkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
            Return False
        End If

        If isExist(, mstrName, mintEmployeeunkid, mintObjectiveunkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
            Return False
        End If
        'S.SANDEEP [ 28 DEC 2012 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation


        'Pinkal (20-Jan-2012) -- Start
        'Enhancement : TRA Changes
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()
        'Pinkal (20-Jan-2012) -- End


        Try
            objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintObjectiveunkid.ToString)
            objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerspectiveunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinWeight.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultgroupUnkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            'S.SANDEEP [ 28 DEC 2012 ] -- END


            'strQ = "UPDATE hrobjective_master SET " & _
            '  "  perspectiveunkid = @perspectiveunkid" & _
            '  ", code = @code" & _
            '  ", name = @name" & _
            '  ", weight = @weight" & _
            '  ", description = @description" & _
            '  ", name1 = @name1" & _
            '  ", name2 = @name2" & _
            '  ", isactive = @isactive " & _
            '  ", resultgroupunkid = @resultgroupunkid " & _
            '"WHERE objectiveunkid = @objectiveunkid "

            strQ = "UPDATE hrobjective_master SET " & _
              "  perspectiveunkid = @perspectiveunkid" & _
              ", code = @code" & _
              ", name = @name" & _
              ", weight = @weight" & _
              ", description = @description" & _
              ", name1 = @name1" & _
              ", name2 = @name2" & _
              ", isactive = @isactive " & _
              ", resultgroupunkid = @resultgroupunkid " & _
           ", referenceunkid  = @referenceunkid " & _
           ", employeeunkid  = @employeeunkid " & _
                   ", isfinal = @isfinal " & _
                        ", periodunkid = @periodunkid " & _
                        ", yearunkid = @yearunkid " & _
            "WHERE objectiveunkid = @objectiveunkid "
            'S.SANDEEP [ 28 DEC 2012 ] -- START {periodunkid,yearunkid} -- END

            'Pinkal (20-Jan-2012) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrobjective_master", "objectiveunkid", mintObjectiveunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrobjective_master", "objectiveunkid", mintObjectiveunkid, , intUserUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrobjective_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal intReferenceUnkid As Integer, Optional ByVal intUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Function Delete(ByVal intUnkid As Integer, ByVal intReferenceUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation


        'Pinkal (20-Jan-2012) -- Start
        'Enhancement : TRA Changes
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Dim blnFlag As Boolean = False
        Try

            strQ = "UPDATE hrobjective_master " & _
                      " SET isactive = 0 " & _
                      "WHERE objectiveunkid = @objectiveunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrobjective_master", "objectiveunkid", intUnkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrobjective_master", "objectiveunkid", intUnkid, , intUserUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END
            

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        'S.SANDEEP [ 10 APR 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim dmList As New DataSet
        'S.SANDEEP [ 10 APR 2013 ] -- END
        Dim blnIsUsed As Boolean = False
        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT " & _
                      "TABLE_NAME AS TableName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='objectiveunkid' "


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("List").Rows

                If dtRow.Item("TableName") = "hrobjective_master" Or dtRow.Item("TableName") = "hrtnatraining_impact_objective" Then Continue For

                    objDataOperation.ClearParameters()
                    If dtRow.Item("TableName") = "hrbsc_analysis_tran" Then
                        strQ = "SELECT objectiveunkid FROM " & dtRow.Item("TableName").ToString & " WHERE objectiveunkid = @objectiveunkid  and isvoid = 0"
                        objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    dmList = objDataOperation.ExecQuery(strQ, "Used")
                    Else
                        strQ = "SELECT objectiveunkid FROM " & dtRow.Item("TableName").ToString & " WHERE objectiveunkid = @objectiveunkid  and isactive = 1 "
            objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    dmList = objDataOperation.ExecQuery(strQ, "Used")
                    End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dmList.Tables("Used").Rows.Count > 0 Then
                        blnIsUsed = True
                        Exit For
                    End If

            Next

            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", _
                                          Optional ByVal strName As String = "", _
                                          Optional ByVal intEmployeeunkid As Integer = 0, _
                                          Optional ByVal intUnkid As Integer = -1, _
                                          Optional ByVal intPeriodId As Integer = -1) As Boolean 'S.SANDEEP [ 28 DEC 2012 ] -- START {intPeriodId} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '  "  objectiveunkid " & _
            '  ", perspectiveunkid " & _
            '  ", code " & _
            '  ", name " & _
            '  ", weight " & _
            '  ", description " & _
            '  ", name1 " & _
            '  ", name2 " & _
            '  ", resultgroupunkid " & _
            '  ", isactive " & _
            ' "FROM hrobjective_master " & _
            ' "WHERE isactive = 1 "

            strQ = "SELECT " & _
              "  objectiveunkid " & _
              ", perspectiveunkid " & _
              ", code " & _
              ", name " & _
              ", weight " & _
              ", description " & _
              ", name1 " & _
              ", name2 " & _
              ", resultgroupunkid " & _
              ", referenceunkid " & _
              ", employeeunkid " & _
              ", isactive " & _
             "FROM hrobjective_master " & _
             "WHERE isactive = 1 "

            'Pinkal (20-Jan-2012) -- End

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
            End If

            If strName.Length > 0 Then
                strQ &= " AND name = @name "
            End If

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If intPeriodId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If intEmployeeunkid > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If
            'Pinkal (20-Jan-2012) -- End


            If intUnkid > 0 Then
                strQ &= " AND objectiveunkid <> @objectiveunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@objectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function



    'S.SANDEEP [ 23 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False, Optional ByVal intperspectiveunkid As Integer = -1) As DataSet
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", _
                                 Optional ByVal mblnFlag As Boolean = False, _
                                 Optional ByVal intperspectiveunkid As Integer = -1, _
                                 Optional ByVal intEmployeeId As Integer = 0, Optional ByVal intPeriodId As Integer = -1) As DataSet 'S.SANDEEP [ 28 DEC 2012 ] -- START {intPeriodId} -- END
        'S.SANDEEP [ 23 JAN 2012 ] -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As id , @ItemName As  name , '' as code UNION "
            End If
            strQ &= "SELECT objectiveunkid,name,code FROM hrobjective_master WHERE isactive =1 "

            If ConfigParameter._Object._IsBSC_ByEmployee Then
                If intEmployeeId >= 0 Then
                    strQ &= " AND referenceunkid = '" & enAllocation.EMPLOYEE & "' AND employeeunkid = '" & intEmployeeId & "' "
                End If
            Else
                strQ &= " AND referenceunkid = '" & enAllocation.GENERAL & "' "
            End If

            If intperspectiveunkid > 0 Then
                strQ &= " AND perspectiveunkid = '" & intperspectiveunkid & "'"
            End If


            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If intPeriodId > 0 Then
            strQ &= " AND periodunkid = '" & intPeriodId & "'"
            'End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END


            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                If intperspectiveunkid > 0 AndAlso intEmployeeId > 0 Then
                    strQ &= " AND isfinal = 1 "
                End If
            Else
                If intperspectiveunkid > 0 Then
                    strQ &= " AND isfinal = 1 "
                End If
            End If
            'S.SANDEEP [ 11 SEP 2012 ] -- END


            

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetObjectiveUnkid(ByVal mstOCode As String, _
                                      Optional ByVal mstObjective As String = "", _
                                      Optional ByVal iPeriodId As Integer = 0, _
                                      Optional ByVal iEmpId As Integer = 0) As Integer 'S.SANDEEP [ 20 JULY 2013 (mstOCode,iPeriodId,iEmpId) ] -- START -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'S.SANDEEP [ 20 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = " SELECT " & _
            '       "     objectiveunkid " & _
            '       " FROM hrobjective_master " & _
            '       " WHERE name = @name "

            strQ = " SELECT " & _
                      "     objectiveunkid " & _
                      " FROM hrobjective_master " & _
                   "WHERE isactive = 1 "

            If iPeriodId > 0 Then
                strQ &= " AND periodunkid = '" & iPeriodId & "' "
            End If

            If iEmpId > 0 Then
                strQ &= " AND employeeunkid = '" & iEmpId & "'"
            End If

            If mstObjective.Trim.Length > 0 Then
                If mstObjective.Contains("'") Then
                    mstObjective = mstObjective.Replace("'", "''")
                End If
                strQ &= " AND name = @name "
            End If

            If mstOCode.Trim.Length > 0 Then
                If mstOCode.Contains("'") Then
                    mstOCode = mstOCode.Replace("'", "''")
                End If
                strQ &= " AND code = @code "
            End If
            'S.SANDEEP [ 20 JULY 2013 ] -- END


            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstObjective)
            'S.SANDEEP [ 20 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstOCode)
            'S.SANDEEP [ 20 JULY 2013 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("objectiveunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetObjectiveUnkid; Module Name: " & mstrModuleName)
        End Try
        Return -1

    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub RemainingWeight(ByRef mdecRemainingWeight As Decimal, ByRef mdecAssigned As Decimal, ByVal intRefID As Integer, Optional ByVal intEmployeeunkid As Integer = 0, Optional ByVal intItemMasterId As Integer = -1)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            If intRefID = enAllocation.GENERAL Then
            strQ = "SELECT " & _
                       " (100 - ISNULL(SUM(weight),0)) AS Remaining " & _
                       ",ISNULL(SUM(weight),0) AS Assigned " & _
                       "FROM hrobjective_master " & _
                           "WHERE isactive = 1  AND referenceunkid = " & enAllocation.GENERAL
            Else
                strQ = "SELECT " & _
                           " (100 - ISNULL(SUM(weight),0)) AS Remaining " & _
                           ",ISNULL(SUM(weight),0) AS Assigned " & _
                           "FROM hrobjective_master " & _
                           "WHERE isactive = 1  AND referenceunkid = " & enAllocation.EMPLOYEE

                If intEmployeeunkid >= 0 Then
                    strQ &= " AND hrobjective_master.employeeunkid = '" & intEmployeeunkid & "' "
                End If

            End If


            If intItemMasterId > 0 Then
                strQ &= " AND hrobjective_master.objectiveunkid <> '" & intItemMasterId & "' "
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdecRemainingWeight = CDec(dsList.Tables(0).Rows(0)("Remaining"))
            mdecAssigned = CDec(dsList.Tables(0).Rows(0)("Assigned"))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: RemainingWeight; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- End


    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetResultGroupFromPerspective(ByVal intPerspectiveId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim mintResultGroupUnkid As Integer = 0
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()
            strQ = "SELECT top 1 ISNULL(resultgroupunkid,0) AS resultgroupunkid FROM hrobjective_master Where isactive = 1"
            objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPerspectiveId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mintResultGroupUnkid = CInt(dsList.Tables(0).Rows(0)("resultgroupunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetResultGroupFromPerspective; Module Name: " & mstrModuleName)
        End Try
        Return mintResultGroupUnkid
    End Function
    'Pinkal (06-Feb-2012) -- End

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Set_Final_Operation(ByVal blnOperation As Boolean, ByVal intEmpId As Integer, ByVal intPeriodId As Integer) As Boolean 'S.SANDEEP [ 28 DEC 2012 ] -- START-- END
        'Public Function Set_Final_Operation(ByVal blnOperation As Boolean, ByVal intEmpId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ &= "UPDATE hrobjective_master SET isfinal = @Final WHERE employeeunkid = '" & intEmpId & "' "
            StrQ &= "UPDATE hrobjective_master SET isfinal = @Final WHERE employeeunkid = '" & intEmpId & "' AND periodunkid = '" & intPeriodId & "' "
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            objDataOperation.AddParameter("@Final", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnOperation.ToString)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Set_Final_Operation; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 05 MARCH 2012 ] -- END


    'S.SANDEEP [ 10 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Sub Send_Notification_Assessor(ByVal iEmployeeId As Integer, _
                                          ByVal iPeriodId As Integer, _
                                          ByVal iYearId As Integer, _
                                          ByVal sYearName As String, _
                                          ByVal strDatabaseName As String, _
                                          Optional ByVal iCompanyId As Integer = 0, _
                                          Optional ByVal sArutiSSURL As String = "", _
                                          Optional ByVal iLoginTypeId As Integer = 0, _
                                          Optional ByVal iLoginEmployeeId As Integer = 0, _
                                          Optional ByVal iUserId As Integer = 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {iLoginTypeId,iLoginEmployeeId,iUserId} -- END
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Dim dsList As DataSet = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty

        objDataOperation = New clsDataOperation

        Try
            If iCompanyId <= 0 Then iCompanyId = Company._Object._Companyunkid
            If sArutiSSURL = "" Then sArutiSSURL = ConfigParameter._Object._ArutiSelfServiceURL
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

            StrQ = "SELECT " & _
                   "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
                   " ,ISNULL(email,'') AS Email " & _
                   " ,hrassessor_master.assessormasterunkid " & _
                   " ,hrapprover_usermapping.userunkid " & _
                   "FROM hrapprover_usermapping " & _
                   " JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                   " JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                   " JOIN hrassessor_tran ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                   "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrassessor_tran.employeeunkid = '" & iEmployeeId & "' AND isreviewer = 0 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isvoid = 0 "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
            End If

            Dim objEmp As New clsEmployee_Master
            Dim objMail As New clsSendMail
            Dim objPeriod As New clscommom_period_Tran

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEmp._Employeeunkid = iEmployeeId : objPeriod._Periodunkid = iPeriodId

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = iEmployeeId : objPeriod._Periodunkid(strDatabaseName) = iPeriodId
            objPeriod._Periodunkid(strDatabaseName) = iPeriodId
            objEmp._Employeeunkid(objPeriod._End_Date) = iEmployeeId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- End


            'S.SANDEEP [ 23 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim dsYear As New DataSet
            Dim objFY As New clsCompany_Master
            dsYear = objFY.GetFinancialYearList(iCompanyId, , "List", objPeriod._Yearunkid)
            If dsYear.Tables("List").Rows.Count > 0 Then
                sYearName = dsYear.Tables("List").Rows(0).Item("financialyear_name")
            End If
            objFY = Nothing
            'S.SANDEEP [ 23 JULY 2013 ] -- END

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("Email") = "" Then Continue For
                Dim strMessage As String = ""
                objMail._Subject = Language.getMessage(mstrModuleName, 4, "Notification for Approval/Rejection BSC Planning.")
                strMessage = "<HTML> <BODY>"
                strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & dtRow.Item("Ename").ToString() & ", <BR><BR>"

                'Pinkal (22-Mar-2019) -- Start
                'Enhancement - Working on PA Changes FOR NMB.
                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 6, "This is to inform you that i have completed my Balanced Score Card Planning for ") & _
                'objPeriod._Period_Name & ", " & sYearName & Language.getMessage(mstrModuleName, 7, ". Please click the link below to review it and thereafter approve/reject it.")

                strMessage &= Language.getMessage(mstrModuleName, 6, "This is to inform you that i have completed my Balanced Score Card Planning for ") & _
                                      " (<b> " & objPeriod._Period_Name & " </b>) , <b> " & sYearName & "</b> " & Language.getMessage(mstrModuleName, 7, ". Please click the link below to review it and thereafter approve/reject it.")
                'Pinkal (22-Mar-2019) -- End

                
                strMessage &= "<BR><BR>"
                strMessage &= objEmp._Firstname & " " & objEmp._Surname
                strLink = sArutiSSURL & "/Assessment/BSC Masters/wPgApprove_Reject_BSC.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iYearId.ToString & "|" & sYearName.ToString & "|" & iPeriodId.ToString))

                'Pinkal (22-Mar-2019) -- Start
                'Enhancement - Working on PA Changes FOR NMB.
                'strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
                strMessage &= "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>"
                'Pinkal (22-Mar-2019) -- End

                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                strMessage &= "</BODY></HTML>"
                objMail._Message = strMessage
                objMail._ToEmail = dtRow.Item("Email")
                'S.SANDEEP [ 28 JAN 2014 ] -- START
                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrWebFrmName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFrmName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                'S.SANDEEP [ 28 JAN 2014 ] -- END
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objMail.SendMail()
                objMail.SendMail(iCompanyId)
                'Sohail (30 Nov 2017) -- End
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_Assessor", mstrModuleName)
        Finally
        End Try
    End Sub

    'Public Function Get_Email_Notification(ByVal intEmpId As Integer, _
    '                                       ByVal sYear As String, _
    '                                       ByVal intPeriodId As Integer, _
    '                                       ByVal StrMessage As String, _
    '                                       ByVal blnIs_Submitted As Boolean, _
    '                                       ByVal blnFinalSaved As Boolean, _
    '                                       ByVal sAssessorName As String, _
    '                                       ByVal sEmployeeName As String, _
    '                                       ByVal sEmp_EmailAddress As String) As Dictionary(Of String, String)
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim dicEmail As New Dictionary(Of String, String)
    '    Try
    '        objDataOperation = New clsDataOperation
    '        If blnIs_Submitted = True Then
    '            StrQ = "SELECT " & _
    '               " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
    '               ",ISNULL(email,'') AS Email " & _
    '               "FROM hrapprover_usermapping " & _
    '               "  JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
    '               "  JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
    '               "  JOIN hrassessor_tran ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
    '               "WHERE usertypeid = 2 AND hrassessor_tran.employeeunkid = '" & intEmpId & "' AND isreviewer = 0 "

    '            dsList = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
    '            End If

    '            Dim objPeriod As New clscommom_period_Tran
    '            objPeriod._Periodunkid = intPeriodId

    '            For Each dtRow As DataRow In dsList.Tables(0).Rows
    '                If dtRow.Item("Email") = "" Then Continue For
    '                If dicEmail.ContainsKey(dtRow.Item("Email")) = False Then
    '                    Dim sMessage As New System.Text.StringBuilder
    '                    sMessage.Append("<HTML><BODY>")
    '                    sMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                    sMessage.Append("Dear <b>" & dtRow.Item("Ename") & "</b></span></p>")
    '                    sMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                    sMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that i have completed my Balanced Score Card Planning for period : " & objPeriod._Period_Name & ", For Year : " & sYear & ".</span></p>")
    '                    sMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                    sMessage.Append("<br></span></p>")
    '                    sMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & StrMessage.Replace("|", "<br>") & "</span></p>")
    '                    sMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
    '                    sMessage.Append("</span></p>")
    '                    sMessage.Append("</BODY></HTML>")
    '                    dicEmail.Add(dtRow.Item("Email"), sMessage.ToString)
    '                End If
    '            Next
    '            objPeriod = Nothing
    '        Else
    '            If blnFinalSaved = True Then
    '                If sEmp_EmailAddress.Trim.Length Then
    '                    If dicEmail.ContainsKey(sEmp_EmailAddress) = False Then
    '                        Dim sMessage As New System.Text.StringBuilder
    '                        sMessage.Append("<HTML><BODY>")
    '                        sMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                        sMessage.Append("Dear <b>" & sEmployeeName & "</b></span></p>")
    '                        sMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                        sMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that your Balanced Score Card Planning has been approved and final saved by  : " & sAssessorName & "</span></p>")
    '                        sMessage.Append("</span></p>")
    '                        sMessage.Append("</BODY></HTML>")
    '                        dicEmail.Add(sEmp_EmailAddress, sMessage.ToString)
    '                    End If
    '                End If
    '            Else
    '                If sEmp_EmailAddress.Trim.Length Then
    '                    If dicEmail.ContainsKey(sEmp_EmailAddress) = False Then
    '                        Dim sMessage As New System.Text.StringBuilder
    '                        sMessage.Append("<HTML><BODY>")
    '                        sMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                        sMessage.Append("Dear <b>" & sEmployeeName & "</b></span></p>")
    '                        sMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                        sMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that your Balanced Card Planning has been rejected by  : " & sAssessorName & " and it is now open for editing.</span></p>")
    '                        sMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                        sMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your advised to make the changes recommended by your assessor as per remarks below.</span></p>")
    '                        sMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                        sMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & StrMessage & "</span></p>")
    '                        sMessage.Append("</span></p>")
    '                        sMessage.Append("</BODY></HTML>")
    '                        dicEmail.Add(sEmp_EmailAddress, sMessage.ToString)
    '                    End If
    '                End If
    '            End If
    '        End If

    '        Return dicEmail

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Get_Email_Notification", mstrModuleName)
    '        Return dicEmail
    '    End Try
    'End Function

    Public Sub Send_Notification_Employee(ByVal iEmployeeId As Integer, _
                                          ByVal sAssessorName As String, _
                                          ByVal sComments As String, _
                                          ByVal isFinalOpr As Boolean, _
                                          ByVal isUnlock As Boolean, _
                                          ByVal intCompanyUnkId As Integer, _
                                          Optional ByVal iLoginTypeId As Integer = 0, _
                                          Optional ByVal iLoginEmployeeId As Integer = 0, _
                                          Optional ByVal iUserId As Integer = 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {iLoginTypeId,iLoginEmployeeId,iUserId} -- END
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objMail As New clsSendMail

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = iEmployeeId
            objEmp._Employeeunkid(Now.Date) = iEmployeeId
            'S.SANDEEP [04 JUN 2015] -- END


            If objEmp._Email.Trim.Length > 0 Then
                Dim strMessage As String = ""

                strMessage = "<HTML> <BODY>"
                strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & objEmp._Firstname & " " & objEmp._Surname & ", <BR><BR>"

                If isFinalOpr = True Then
                    objMail._Subject = Language.getMessage(mstrModuleName, 8, "Notification for Approval of BSC Planning")

                    'Pinkal (22-Mar-2019) -- Start
                    'Enhancement - Working on PA Changes FOR NMB.
                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 10, "This is to inform you that your Balanced Score Card Planning has been approved and final saved by ") & "<b>" & sAssessorName & "</b>" & "."
                    strMessage &= Language.getMessage(mstrModuleName, 10, "This is to inform you that your Balanced Score Card Planning has been approved and final saved by ") & " <b>" & sAssessorName & "</b> " & "."
                    'Pinkal (22-Mar-2019) -- End
                Else
                    If isUnlock = True Then
                        objMail._Subject = Language.getMessage(mstrModuleName, 15, "Notification for Unlocked of BSC Planning")

                        'Pinkal (22-Mar-2019) -- Start
                        'Enhancement - Working on PA Changes FOR NMB.
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 16, "This is to inform you that your Balanced Score Card Planning has been Unlocked by ") & "<b>" & sAssessorName & "</b>" & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>"
                        strMessage &= Language.getMessage(mstrModuleName, 16, "This is to inform you that your Balanced Score Card Planning has been Unlocked by ") & " <b>" & sAssessorName & "</b> " & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>"
                        'Pinkal (22-Mar-2019) -- End

                    Else
                    objMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification for Rejection of BSC Planning")

                        'Pinkal (22-Mar-2019) -- Start
                        'Enhancement - Working on PA Changes FOR NMB.
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 11, "This is to inform you that your Balanced Score Card Planning has been rejected by ") & "<b>" & sAssessorName & "</b>" & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>"
                        strMessage &= Language.getMessage(mstrModuleName, 11, "This is to inform you that your Balanced Score Card Planning has been rejected by ") & " <b>" & sAssessorName & "</b> " & Language.getMessage(mstrModuleName, 12, " and it is now open for editing.") & "<br>"
                        'Pinkal (22-Mar-2019) -- End


                    End If

                    'Pinkal (22-Mar-2019) -- Start
                    'Enhancement - Working on PA Changes FOR NMB.
                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 13, "Your advised to make the changes recommended by your assessor as per remarks below.") & "<br><br>" & _
                    '              "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>" & Language.getMessage(mstrModuleName, 14, "Comments :") & "<b>" & sComments

                    strMessage &= Language.getMessage(mstrModuleName, 13, "Your advised to make the changes recommended by your assessor as per remarks below.") & "<br><br>" & _
                                  " <b>" & Language.getMessage(mstrModuleName, 14, "Comments :") & "<b>" & sComments
                    'Pinkal (22-Mar-2019) -- End

                    

                End If
                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                strMessage &= "</BODY></HTML>"
                objMail._ToEmail = objEmp._Email
                objMail._Message = strMessage
                'S.SANDEEP [ 28 JAN 2014 ] -- START
                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrWebFrmName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFrmName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = iUserId
                objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                'S.SANDEEP [ 28 JAN 2014 ] -- END
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objMail.SendMail()
                objMail.SendMail(intCompanyUnkId)
                'Sohail (30 Nov 2017) -- End
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification_Employee", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 10 APR 2013 ] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
			Language.setMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Notification for Approval/Rejection BSC Planning.")
			Language.setMessage(mstrModuleName, 5, "Dear")
			Language.setMessage(mstrModuleName, 6, "This is to inform you that i have completed my Balanced Score Card Planning for")
			Language.setMessage(mstrModuleName, 7, ". Please click the link below to review it and thereafter approve/reject it.")
			Language.setMessage(mstrModuleName, 8, "Notification for Approval of BSC Planning")
			Language.setMessage(mstrModuleName, 9, "Notification for Rejection of BSC Planning")
			Language.setMessage(mstrModuleName, 10, "This is to inform you that your Balanced Score Card Planning has been approved and final saved by")
			Language.setMessage(mstrModuleName, 11, "This is to inform you that your Balanced Score Card Planning has been rejected by")
			Language.setMessage(mstrModuleName, 12, " and it is now open for editing.")
			Language.setMessage(mstrModuleName, 13, "Your advised to make the changes recommended by your assessor as per remarks below.")
			Language.setMessage(mstrModuleName, 14, "Comments :")
			Language.setMessage(mstrModuleName, 15, "Notification for Unlocked of BSC Planning")
			Language.setMessage(mstrModuleName, 16, "This is to inform you that your Balanced Score Card Planning has been Unlocked by")
			Language.setMessage("clsMasterData", 315, "Financial")
			Language.setMessage("clsMasterData", 316, "Customer")
			Language.setMessage("clsMasterData", 317, "Business Process")
			Language.setMessage("clsMasterData", 318, "Organization Capacity")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
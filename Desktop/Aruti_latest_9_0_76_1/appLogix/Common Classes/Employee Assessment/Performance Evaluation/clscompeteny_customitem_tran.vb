﻿'************************************************************************************************************************************
'Class Name :clscompeteny_customitem_tran.vb
'Purpose    :
'Date       :22-Aug-2014
'Written By :Sandeep Sharma
'Modified   :ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
''' Developer: Sandeep Sharma
''' </summary>
Public Class clscompeteny_customitem_tran
    Private Shared ReadOnly mstrModuleName As String = "clscompeteny_customitem_tran"
    Dim mstrMessage As String = ""
    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    Private objPDPFormTran As New clspdpform_tran
    Private objpdpItemMaster As New clspdpitem_master
    'S.SANDEEP |03-MAY-2021| -- END
    Dim objDataOperation As New clsDataOperation


#Region " Private Variables "

    Private mintAnalysisTranId As Integer = -1
    Private mintAnalysisUnkid As Integer = -1
    Private mdtTran As DataTable
    Private mintEmployeeId As Integer = -1
    'S.SANDEEP [29 DEC 2015] -- START
    Private mintPeriodId As Integer = 0
    'S.SANDEEP [29 DEC 2015] -- END

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Private mintHeaderUnkid As Integer = -1
    Private mblnIsCompanyNeedReviewer As Boolean = False
    'Shani (26-Sep-2016) -- End

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Private icustomanalysistranunkid As Integer = 0
    Private ianalysisunkid As Integer = 0
    Private icustomitemunkid As Integer = 0
    Private icustom_value As String = ""
    Private iisvoid As Boolean = False
    Private ivoiddatetime As DateTime = Nothing
    Private ivoiduserunkid As Integer = 0
    Private ivoidreason As String = ""
    Private iperiodunkid As Integer = 0
    Private icustomanalysistranguid As String = ""
    Private iemployeeunkid As Integer = 0
    Private iismanual As Boolean = False
    'S.SANDEEP |18-JAN-2020| -- END


    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mintloginemployeeunkid As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""
    'S.SANDEEP |03-MAY-2021| -- END 
#End Region

#Region " Properties "

    Public Property _AnalysisUnkid() As Integer
        Get
            Return mintAnalysisUnkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisUnkid = value
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property

    Public Property _EmployeeId() As Integer
        Get
            Return mintEmployeeId
        End Get
        Set(ByVal value As Integer)
            mintEmployeeId = value
            'S.SANDEEP [29 DEC 2015] -- START
            'Call GetAnalysisTran()
            'S.SANDEEP [29 DEC 2015] -- END
        End Set
    End Property

    'S.SANDEEP [29 DEC 2015] -- START

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
            'S.SANDEEP [29 DEC 2015] -- START
            Call GetAnalysisTran()
            'S.SANDEEP [29 DEC 2015] -- END
        End Set
    End Property
    'S.SANDEEP [29 DEC 2015] -- END

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Public WriteOnly Property _IsCompanyNeedReviewer() As Boolean
        Set(ByVal value As Boolean)
            mblnIsCompanyNeedReviewer = value
        End Set
    End Property
    'Shani (26-Sep-2016) -- End


    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public Property _icustomanalysistranunkid() As Integer
        Get
            Return icustomanalysistranunkid
        End Get
        Set(ByVal value As Integer)
            icustomanalysistranunkid = value
        End Set
    End Property
    Public Property _ianalysisunkid() As Integer
        Get
            Return ianalysisunkid
        End Get
        Set(ByVal value As Integer)
            ianalysisunkid = value
        End Set
    End Property
    Public Property _icustomitemunkid() As Integer
        Get
            Return icustomitemunkid
        End Get
        Set(ByVal value As Integer)
            icustomitemunkid = value
        End Set
    End Property
    Public Property _icustom_value() As String
        Get
            Return icustom_value
        End Get
        Set(ByVal value As String)
            icustom_value = value
        End Set
    End Property
    Public Property _iisvoid() As Boolean
        Get
            Return iisvoid
        End Get
        Set(ByVal value As Boolean)
            iisvoid = value
        End Set
    End Property
    Public Property _ivoiddatetime() As DateTime
        Get
            Return ivoiddatetime
        End Get
        Set(ByVal value As DateTime)
            ivoiddatetime = value
        End Set
    End Property
    Public Property _ivoiduserunkid() As Integer
        Get
            Return ivoiduserunkid
        End Get
        Set(ByVal value As Integer)
            ivoiduserunkid = value
        End Set
    End Property
    Public Property _ivoidreason() As String
        Get
            Return ivoidreason
        End Get
        Set(ByVal value As String)
            ivoidreason = value
        End Set
    End Property
    Public Property _iperiodunkid() As Integer
        Get
            Return iperiodunkid
        End Get
        Set(ByVal value As Integer)
            iperiodunkid = value
        End Set
    End Property
    Public Property _icustomanalysistranguid() As String
        Get
            Return icustomanalysistranguid
        End Get
        Set(ByVal value As String)
            icustomanalysistranguid = value
        End Set
    End Property
    Public Property _iemployeeunkid() As Integer
        Get
            Return iemployeeunkid
        End Get
        Set(ByVal value As Integer)
            iemployeeunkid = value
        End Set
    End Property
    Public Property _iismanual() As Boolean
        Get
            Return iismanual
        End Get
        Set(ByVal value As Boolean)
            iismanual = value
        End Set
    End Property
    'S.SANDEEP |18-JAN-2020| -- END


    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintloginemployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
#End Region

#Region " Contructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("custom")

            mdtTran.Columns.Add("customanalysistranguid", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("customitemunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("custom_value", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("custom_header", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("custom_item", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("dispaly_value", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("itemtypeid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("selectionmodeid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("customanalysistranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            mdtTran.Columns.Add("isdefaultentry", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("ismanual", GetType(System.Boolean)).DefaultValue = True
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP |25-MAR-2019| -- START
            mdtTran.Columns.Add("Header_Id", GetType(System.Int32)).DefaultValue = 0
            'S.SANDEEP |25-MAR-2019| -- END


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public Sub New(ByVal _analysisunkid As Integer, _
                   ByVal _customitemunkid As Integer, _
                   ByVal _custom_value As String, _
                   ByVal _isvoid As Boolean, _
                   ByVal _voiddatetime As DateTime, _
                   ByVal _voiduserunkid As Integer, _
                   ByVal _voidreason As String, _
                   ByVal _periodunkid As Integer, _
                   ByVal _customanalysistranguid As String, _
                   ByVal _employeeunkid As Integer, _
                   ByVal _ismanual As Boolean)
        ianalysisunkid = _analysisunkid
        icustomitemunkid = _customitemunkid
        icustom_value = _custom_value
        iisvoid = _isvoid
        ivoiddatetime = _voiddatetime
        ivoiduserunkid = _voiduserunkid
        ivoidreason = _voidreason
        iperiodunkid = _periodunkid
        icustomanalysistranguid = _customanalysistranguid
        iemployeeunkid = _employeeunkid
        iismanual = _ismanual
    End Sub
    'S.SANDEEP |18-JAN-2020| -- END

#End Region

#Region " Private/Public Methods "

    Private Sub GetAnalysisTran()
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception

        'Shani (26-Sep-2016) -- Start
        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
        Dim blnIsRecordExist As Boolean = True
        'Shani (26-Sep-2016) -- End


        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        Try
            StrQ = "SELECT " & _
                   "     hrcompetency_customitem_tran.customanalysistranguid " & _
                   "    ,hrcompetency_customitem_tran.analysisunkid " & _
                   "    ,hrcompetency_customitem_tran.customitemunkid " & _
                   "    ,hrcompetency_customitem_tran.periodunkid " & _
                   "    ,hrcompetency_customitem_tran.custom_value " & _
                   "    ,hrcompetency_customitem_tran.isvoid " & _
                   "    ,hrcompetency_customitem_tran.voiddatetime " & _
                   "    ,hrcompetency_customitem_tran.voiduserunkid " & _
                   "    ,hrcompetency_customitem_tran.voidreason " & _
                   "    ,hrcompetency_customitem_tran.customanalysistranunkid " & _
                   "    ,'' AS AUD " & _
                   "    ,hrassess_custom_items.custom_item AS custom_item " & _
                   "    ,hrassess_custom_headers.name AS custom_header " & _
                   "    ,hrassess_custom_items.itemtypeid " & _
                   "    ,hrassess_custom_items.selectionmodeid " & _
                   "    ,'' AS dispaly_value " & _
                   "    ,hrassess_custom_items.isdefaultentry AS isdefaultentry " & _
                   "    ,hrcompetency_customitem_tran.ismanual AS ismanual " & _
                   "    ,hrassess_custom_headers.customheaderunkid AS Header_Id " & _
                   "FROM hrcompetency_customitem_tran " & _
                   "    JOIN hrassess_custom_items ON hrassess_custom_items.customitemunkid = hrcompetency_customitem_tran.customitemunkid " & _
                   "    JOIN hrassess_custom_headers ON hrassess_custom_headers.customheaderunkid = hrassess_custom_items.customheaderunkid " & _
                   "WHERE hrcompetency_customitem_tran.isvoid = 0 "
            'S.SANDEEP |25-MAR-2019| -- START {Header_Id} -- END

            objDataOperation.ClearParameters()

            'S.SANDEEP [29 DEC 2015] -- START
            If mintPeriodId > 0 Then
                StrQ &= " AND hrcompetency_customitem_tran.periodunkid = @PeriodId  "
                objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId.ToString)
            End If
            'S.SANDEEP [29 DEC 2015] -- END

            If mintEmployeeId > 0 Then
                StrQ &= " AND hrcompetency_customitem_tran.employeeunkid = @employeeunkid  "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId.ToString)
            Else
                StrQ &= " AND hrcompetency_customitem_tran.analysisunkid = @analysisunkid  "
            End If


            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [29 DEC 2015] -- START

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso mblnAllowCustomItemInPlanning = True Then
            If dsList.Tables(0).Rows.Count <= 0 Then
                blnIsRecordExist = False
                'Shani (26-Sep-2016) -- End
                StrQ = "SELECT " & _
                       "     hrassess_plan_customitem_tran.plancustomguid AS customanalysistranguid " & _
                       "    ,0 AS analysisunkid " & _
                       "    ,hrassess_plan_customitem_tran.customitemunkid " & _
                       "    ,hrassess_plan_customitem_tran.periodunkid " & _
                       "    ,hrassess_plan_customitem_tran.employeeunkid " & _
                       "    ,hrassess_plan_customitem_tran.custom_value " & _
                       "    ,hrassess_plan_customitem_tran.isvoid " & _
                       "    ,hrassess_plan_customitem_tran.voiddatetime " & _
                       "    ,hrassess_plan_customitem_tran.voiduserunkid " & _
                       "    ,hrassess_plan_customitem_tran.voidreason " & _
                       "    ,'A' AS AUD " & _
                       "    ,hrassess_custom_items.custom_item AS custom_item " & _
                       "    ,hrassess_custom_headers.name AS custom_header " & _
                       "    ,hrassess_custom_items.itemtypeid " & _
                       "    ,hrassess_custom_items.selectionmodeid " & _
                       "    ,'' AS dispaly_value " & _
                       "    ,hrassess_custom_items.isdefaultentry AS isdefaultentry " & _
                       "    ,hrassess_plan_customitem_tran.ismanual AS ismanual " & _
                       "    ,hrassess_custom_headers.customheaderunkid AS Header_Id " & _
                       "FROM hrassess_plan_customitem_tran " & _
                       "    JOIN hrassess_custom_items ON hrassess_custom_items.customitemunkid = hrassess_plan_customitem_tran.customitemunkid " & _
                       "    JOIN hrassess_custom_headers ON hrassess_custom_headers.customheaderunkid = hrassess_custom_items.customheaderunkid " & _
                       "WHERE isfinal = 1 AND isvoid = 0 AND hrassess_custom_headers.isinclude_planning = 1 "
                'S.SANDEEP |25-MAR-2019| -- START {Header_Id} -- END

                'Shani (26-Sep-2016) -- [AND hrassess_custom_headers.isinclude_planning = 1]

                objDataOperation.ClearParameters()
                StrQ &= " AND hrassess_plan_customitem_tran.employeeunkid = @employeeunkid AND hrassess_plan_customitem_tran.periodunkid = @PeriodId "

                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId.ToString)
                objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId.ToString)

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'S.SANDEEP [29 DEC 2015] -- END

            mdtTran.Rows.Clear()
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Select Case CInt(dtRow.Item("itemtypeid"))
                    Case clsassess_custom_items.enCustomType.FREE_TEXT
                        dtRow.Item("dispaly_value") = dtRow.Item("custom_value")
                    Case clsassess_custom_items.enCustomType.SELECTION
                        If dtRow.Item("custom_value").ToString.Trim.Length > 0 Then
                            Select Case CInt(dtRow.Item("selectionmodeid"))
                                'S.SANDEEP [04 OCT 2016] -- START
                                'Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                    'S.SANDEEP [04 OCT 2016] -- END
                                    Dim objCMaster As New clsCommon_Master
                                    'S.SANDEEP |18-JAN-2019| -- START
                                    'objCMaster._Masterunkid = CInt(dtRow.Item("custom_value"))
                                    Try
                                    objCMaster._Masterunkid = CInt(dtRow.Item("custom_value"))
                                    Catch ex As Exception
                                        objCMaster._Masterunkid = 0
                                    End Try
                                    'S.SANDEEP |18-JAN-2019| -- END
                                    dtRow.Item("dispaly_value") = objCMaster._Name
                                    objCMaster = Nothing
                                Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                    Dim objCMaster As New clsassess_competencies_master
                                    objCMaster._Competenciesunkid = CInt(dtRow.Item("custom_value"))
                                    dtRow.Item("dispaly_value") = objCMaster._Name
                                    objCMaster = Nothing
                                Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                    Dim objEmpField1 As New clsassess_empfield1_master
                                    objEmpField1._Empfield1unkid = CInt(dtRow.Item("custom_value"))
                                    dtRow.Item("dispaly_value") = objEmpField1._Field_Data
                                    objEmpField1 = Nothing

                                    'S.SANDEEP |16-AUG-2019| -- START
                                    'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                    Dim objCMaster As New clsCommon_Master
                                    Try
                                        objCMaster._Masterunkid = CInt(dtRow.Item("custom_value"))
                                    Catch ex As Exception
                                        objCMaster._Masterunkid = 0
                                    End Try
                                    dtRow.Item("dispaly_value") = objCMaster._Name
                                    objCMaster = Nothing
                                    'S.SANDEEP |16-AUG-2019| -- END
                            End Select
                        End If
                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
                        If dtRow.Item("custom_value").ToString.Trim.Length > 0 Then
                            dtRow.Item("dispaly_value") = eZeeDate.convertDate(dtRow.Item("custom_value").ToString).ToShortDateString
                        End If
                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                        If dtRow.Item("custom_value").ToString.Trim.Length > 0 Then
                            If IsNumeric(dtRow.Item("custom_value")) Then
                                dtRow.Item("dispaly_value") = CDbl(dtRow.Item("custom_value")).ToString
                            End If
                        End If
                End Select
                mdtTran.ImportRow(dtRow)
            Next

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            If mintAnalysisUnkid <= 0 AndAlso blnIsRecordExist = False Then
                Dim dsCustomHeader As DataSet = (New clsassess_custom_header).GetList("List")
                Dim dsCItem As DataSet = (New clsassess_custom_items).GetList("List")

                Dim strFilter As String = "isallowaddpastperiod = 1 "
                If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                    Dim strHeaderCsv As String = String.Join(",", mdtTran.AsEnumerable().Select(Function(x) "'" & x.Field(Of String)("custom_header").ToString() & "'").Distinct().ToArray)
                    If strHeaderCsv.ToString.Trim.Length > 0 Then
                        strFilter &= " AND name NOT IN  (" & strHeaderCsv & ")  "
                    End If
                End If

                For Each dRow As DataRow In dsCustomHeader.Tables(0).Select(strFilter)
                    StrQ = "SELECT " & _
                            "    A.Value " & _
                            "   ,A.competenciesunkid " & _
                            "   ,A.rno " & _
                            "FROM ( " & _
                            "       SELECT " & _
                            "            COMP_CAT.name AS Value " & _
                            "           ,hrassess_competencies_master.competenciesunkid " & _
                            "           ,1 AS rno " & _
                            "       FROM hrevaluation_analysis_master " & _
                            "           LEFT JOIN hrcompetency_analysis_tran ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                            "           LEFT JOIN hrassess_competencies_master ON hrassess_competencies_master.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                            "           LEFT JOIN cfcommon_master AS COMP_CAT ON COMP_CAT.masterunkid = hrassess_competencies_master.competence_categoryunkid AND COMP_CAT.mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & "' " & _
                            "       WHERE hrassess_competencies_master.isactive = 1  AND hrcompetency_analysis_tran.isvoid = 0 " & _
                            "           AND hrevaluation_analysis_master.isvoid = 0 AND COMP_CAT.isactive = 1 " & _
                            "           AND hrevaluation_analysis_master.periodunkid = '" & dRow.Item("mappedperiodunkid") & "' " & _
                            "           AND hrevaluation_analysis_master.assessmodeid = " & IIf(mblnIsCompanyNeedReviewer, enAssessmentMode.REVIEWER_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT) & " " & _
                            "           AND hrevaluation_analysis_master.assessedemployeeunkid = '" & mintEmployeeId & "' " & _
                            "           AND hrcompetency_analysis_tran.result <= " & dRow.Item("min_score") & " " & _
                            "   UNION ALL " & _
                            "       SELECT " & _
                            "            hrassess_competencies_master.name AS Value " & _
                            "           ,hrassess_competencies_master.competenciesunkid " & _
                            "           ,2 AS rno " & _
                            "       FROM hrevaluation_analysis_master " & _
                            "           LEFT JOIN hrcompetency_analysis_tran ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                            "           LEFT JOIN hrassess_competencies_master ON hrassess_competencies_master.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                            "       WHERE hrassess_competencies_master.isactive = 1  AND hrcompetency_analysis_tran.isvoid = 0 " & _
                            "           AND hrevaluation_analysis_master.isvoid = 0 " & _
                            "           AND hrevaluation_analysis_master.periodunkid = '" & dRow.Item("mappedperiodunkid") & "' " & _
                            "           AND hrevaluation_analysis_master.assessmodeid = " & IIf(mblnIsCompanyNeedReviewer, enAssessmentMode.REVIEWER_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT) & " " & _
                            "           AND hrevaluation_analysis_master.assessedemployeeunkid = '" & mintEmployeeId & "' " & _
                            "           AND hrcompetency_analysis_tran.result <= " & dRow.Item("min_score") & " " & _
                            ") AS A ORDER BY A.competenciesunkid,A.rno "

                    objDataOperation.ClearParameters()
                    dsList = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso dsCItem IsNot Nothing AndAlso dsCItem.Tables(0).Rows.Count > 0 Then
                        If dsCItem.Tables(0).Select("periodunkid = '" & mintPeriodId & "' AND customheaderunkid = '" & dRow.Item("customheaderunkid").ToString & "' ").Count > 0 Then
                            Dim iCustomRow = dsCItem.Tables(0).Select("periodunkid = '" & mintPeriodId & "' AND customheaderunkid = '" & dRow.Item("customheaderunkid").ToString & "' AND isdefaultentry = 1 ")

                            If iCustomRow.Count = 2 Then
                                Dim iRow As DataRow
                                Dim strGuid As String = ""
                                For Each dtRow As DataRow In dsList.Tables(0).Rows
                                    iRow = mdtTran.NewRow
                                    If CInt(dtRow.Item("rno")) = 1 Then
                                        strGuid = System.Guid.NewGuid().ToString()
                                    End If
                                    iRow.Item("customanalysistranguid") = strGuid
                                    iRow.Item("analysisunkid") = 0

                                    If CInt(dtRow.Item("rno")) = 1 Then
                                        iRow.Item("customitemunkid") = iCustomRow(0).Item("customitemunkid").ToString
                                        iRow.Item("custom_item") = iCustomRow(0).Item("custom_item").ToString
                                        iRow.Item("itemtypeid") = iCustomRow(0).Item("itemtypeid").ToString
                                    Else
                                        iRow.Item("customitemunkid") = iCustomRow(1).Item("customitemunkid").ToString
                                        iRow.Item("custom_item") = iCustomRow(1).Item("custom_item").ToString
                                        iRow.Item("itemtypeid") = iCustomRow(1).Item("itemtypeid").ToString
                                    End If
                                    iRow.Item("periodunkid") = mintPeriodId
                                    iRow.Item("custom_value") = dtRow.Item("Value")
                                    iRow.Item("custom_header") = dRow.Item("name")
                                    iRow.Item("dispaly_value") = dtRow.Item("Value")
                                    iRow.Item("AUD") = "A"
                                    iRow.Item("isdefaultentry") = True
                                    iRow.Item("ismanual") = False
                                    mdtTran.Rows.Add(iRow)
                                Next
                            End If
                        End If
                    End If
                Next
            End If
            'Shani (26-Sep-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAnalysisTran", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_CustomAnalysisTran(Optional ByVal intUserUnkid As Integer = 0) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrcompetency_customitem_tran ( " & _
                                           "  analysisunkid " & _
                                           ", customitemunkid " & _
                                           ", custom_value " & _
                                           ", isvoid " & _
                                           ", voiddatetime " & _
                                           ", voiduserunkid " & _
                                           ", voidreason " & _
                                           ", periodunkid " & _
                                           ", customanalysistranguid " & _
                                           ", employeeunkid " & _
                                           ", ismanual " & _
                                       ") VALUES (" & _
                                           "  @analysisunkid " & _
                                           ", @customitemunkid " & _
                                           ", @custom_value " & _
                                           ", @isvoid " & _
                                           ", @voiddatetime " & _
                                           ", @voiduserunkid " & _
                                           ", @voidreason " & _
                                           ", @periodunkid " & _
                                           ", @customanalysistranguid " & _
                                           ", @employeeunkid " & _
                                           ", @ismanual " & _
                                       "); SELECT @@identity"

                                objDataOperation.AddParameter("@customanalysistranguid", SqlDbType.NVarChar, .Item("customanalysistranguid").ToString.Length, .Item("customanalysistranguid"))
                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)
                                objDataOperation.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("customitemunkid"))
                                objDataOperation.AddParameter("@custom_value", SqlDbType.NVarChar, .Item("custom_value").ToString.Length, .Item("custom_value"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid"))
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                                objDataOperation.AddParameter("@ismanual", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("ismanual"))

                                Dim dsList As New DataSet
                                dsList = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintAnalysisTranId = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("analysisunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrcompetency_customitem_tran", "customanalysistranunkid", mintAnalysisTranId, 2, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", mintAnalysisUnkid, "hrcompetency_customitem_tran", "customanalysistranunkid", mintAnalysisTranId, 1, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "U"

                                StrQ = "UPDATE hrcompetency_customitem_tran SET " & _
                                        "  analysisunkid = @analysisunkid" & _
                                        ", customitemunkid = @customitemunkid" & _
                                        ", custom_value = @custom_value" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voidreason = @voidreason " & _
                                        ", periodunkid  = @periodunkid " & _
                                        ", employeeunkid = @employeeunkid " & _
                                        "WHERE customanalysistranguid = @customanalysistranguid " & _
                                        " AND customitemunkid = @customitemunkid "

                                objDataOperation.AddParameter("@customanalysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("customanalysistranunkid"))
                                objDataOperation.AddParameter("@customanalysistranguid", SqlDbType.NVarChar, .Item("customanalysistranguid").ToString.Length, .Item("customanalysistranguid"))
                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysisunkid"))
                                objDataOperation.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("customitemunkid"))
                                objDataOperation.AddParameter("@custom_value", SqlDbType.NText, .Item("custom_value").ToString.Length, .Item("custom_value"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid"))
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrcompetency_customitem_tran", "customanalysistranunkid", .Item("customanalysistranunkid"), 2, 2, , intUserUnkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                If .Item("customanalysistranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrcompetency_customitem_tran", "customanalysistranunkid", .Item("customanalysistranunkid"), 2, 3, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                StrQ = "UPDATE hrcompetency_customitem_tran SET " & _
                                           "  isvoid = @isvoid" & _
                                           ", voiddatetime = @voiddatetime" & _
                                           ", voiduserunkid = @voiduserunkid" & _
                                           ", voidreason = @voidreason " & _
                                       "WHERE customanalysistranguid = @customanalysistranguid " & _
                                       " AND customitemunkid = @customitemunkid "

                                objDataOperation.AddParameter("@customanalysistranguid", SqlDbType.NVarChar, .Item("customanalysistranguid").ToString.Length, .Item("customanalysistranguid").ToString)
                                objDataOperation.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("customitemunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_CustomAnalysisTran", mstrModuleName)
        Finally
        End Try
    End Function

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : New Screen Training Need Form.
    Public Function getTrainingCourseComboList(ByVal intPeriodUnkId As Integer, Optional ByVal blnAddSelect As Boolean = True, Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        Dim strQ As String = ""
        Dim dsList As DataSet

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            If blnAddSelect = True Then

                strQ = "SELECT 0 AS masterunkid " & _
                            ",  ' ' + @Select AS name " & _
                            ", '' AS description " & _
                            ", 99 AS coursetypeid " & _
                       "UNION "

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            End If

            strQ &= "SELECT DISTINCT cfcommon_master.masterunkid  " & _
                                  ", cfcommon_master.name " & _
                                  ", cfcommon_master.description " & _
                                  ", cfcommon_master.coursetypeid " & _
                    "FROM hrcompetency_customitem_tran " & _
                        "JOIN hrassess_custom_items ON hrcompetency_customitem_tran.customitemunkid = hrassess_custom_items.customitemunkid " & _
                            "AND hrcompetency_customitem_tran.periodunkid = hrassess_custom_items.periodunkid " & _
                        "JOIN cfcommon_master ON hrcompetency_customitem_tran.custom_value = cfcommon_master.masterunkid " & _
                    "WHERE isvoid = 0 " & _
                        "AND custom_value <> '' " & _
                        "AND custom_value <> '0' " & _
                        "AND hrassess_custom_items.isactive = 1 " & _
                        "AND hrassess_custom_items.periodunkid = @periodunkid " & _
                        "AND itemtypeid = " & clsassess_custom_items.enCustomType.SELECTION & " " & _
                        "AND selectionmodeid IN (" & clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE & "," & clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES & "," & clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES & ") "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (14 Nov 2019) -- End


    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public Function VoidItems(ByVal intUserUnkid As Integer, _
                              ByVal strGUID As String, _
                              ByVal intEmployeeId As Integer, _
                              ByVal intPeriodId As Integer, _
                              ByVal iVoidReason As String) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet


        'S.SANDEEP |03-MAY-2021| -- START
        'ISSUE/ENHANCEMENT : PDP_PM_LINKING
        Dim intPDPFormId As Integer = 0
        Dim objpdpform As New clspdpform_master
        objpdpform.IsPDPFormExists(intEmployeeId, 0, intPDPFormId)
        objpdpform = Nothing
        'S.SANDEEP |03-MAY-2021| -- END

        Try
            Using objDataOperation As New clsDataOperation
                objDataOperation.BindTransaction()
                StrQ = "UPDATE hrcompetency_customitem_tran SET " & _
                       "    isvoid = 1,voiddatetime = GETDATE(),voiduserunkid = @voiduserunkid,voidreason = @voidreason " & _
                       "WHERE customanalysistranguid = @customanalysistranguid " & _
                       "AND periodunkid = @periodunkid AND employeeunkid = @employeeunkid "

                objDataOperation.AddParameter("@customanalysistranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strGUID)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, iVoidReason.Length, iVoidReason)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkid)

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objDataOperation.ClearParameters()

                StrQ = "SELECT customanalysistranunkid,analysisunkid " & _
                       "FROM hrcompetency_customitem_tran " & _
                       "WHERE customanalysistranguid = @customanalysistranguid " & _
                       "AND periodunkid = @periodunkid AND employeeunkid = @employeeunkid "

                objDataOperation.AddParameter("@customanalysistranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strGUID)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For Each row As DataRow In dsList.Tables(0).Rows
                    objDataOperation.ClearParameters()

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", CInt(row("analysisunkid")), "hrcompetency_customitem_tran", "customanalysistranunkid", CInt(row("customanalysistranunkid")), 2, 3, , intUserUnkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

                'S.SANDEEP |03-MAY-2021| -- START
                'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                StrQ = "SELECT customitemunkid " & _
                       "FROM hrcompetency_customitem_tran " & _
                       "WHERE customanalysistranguid = @customanalysistranguid " & _
                       "AND periodunkid = @periodunkid AND employeeunkid = @employeeunkid AND CAST(customitemunkid AS NVARCHAR(MAX)) LIKE '5000%' "

                objDataOperation.AddParameter("@customanalysistranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strGUID)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    If dsList.Tables(0).Rows(0)("customitemunkid").ToString().StartsWith("5000") AndAlso intPDPFormId > 0 Then
                        objpdpItemMaster._Itemunkid(objDataOperation) = CInt(dsList.Tables(0).Rows(0)("customitemunkid").ToString().Replace("5000", ""))

                        objPDPFormTran._Pdpformunkid = intPDPFormId
                        objPDPFormTran._Isvoid = True
                        objPDPFormTran._Voiddatetime = ivoiddatetime
                        objPDPFormTran._Voiduserunkid = intUserUnkid
                        objPDPFormTran._Voidreason = iVoidReason

                        objPDPFormTran._AuditUserId = intUserUnkid
                        objPDPFormTran._FormName = mstrFormName
                        objPDPFormTran._FromWeb = mblnIsWeb
                        objPDPFormTran._HostName = mstrHostName
                        objPDPFormTran._ClientIP = mstrClientIP
                        objPDPFormTran._DatabaseName = mstrDatabaseName
                        objPDPFormTran._LoginEmployeeUnkid = mintloginemployeeunkid
                        objPDPFormTran._PAnalysisunkid = mintAnalysisUnkid

                        If objPDPFormTran.Delete(-1, strGUID, objpdpItemMaster._Categoryunkid, objDataOperation) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                End If
                'S.SANDEEP |03-MAY-2021| -- END

                objDataOperation.ReleaseTransaction(True)
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidItems; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function InsertUpdate(ByVal objDataOperation As clsDataOperation, ByVal intUserUnkid As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation.ClearParameters()
        Try
            StrQ = "IF NOT EXISTS(SELECT * FROM hrcompetency_customitem_tran WITH (NOLOCK) WHERE isvoid = 0 AND customanalysistranguid = @customanalysistranguid AND analysisunkid = @analysisunkid AND customitemunkid = @customitemunkid AND periodunkid = @periodunkid AND employeeunkid = @employeeunkid) " & _
                   "BEGIN " & _
                   "    INSERT INTO hrcompetency_customitem_tran " & _
                   "    ( " & _
                   "         customanalysistranguid " & _
                   "        ,analysisunkid " & _
                   "        ,customitemunkid " & _
                   "        ,periodunkid " & _
                   "        ,custom_value " & _
                   "        ,isvoid " & _
                   "        ,voiddatetime " & _
                   "        ,voiduserunkid " & _
                   "        ,voidreason " & _
                   "        ,employeeunkid " & _
                   "        ,ismanual " & _
                   "    ) " & _
                   "    VALUES " & _
                   "    ( " & _
                   "         @customanalysistranguid " & _
                   "        ,@analysisunkid " & _
                   "        ,@customitemunkid " & _
                   "        ,@periodunkid " & _
                   "        ,@custom_value " & _
                   "        ,@isvoid " & _
                   "        ,@voiddatetime " & _
                   "        ,@voiduserunkid " & _
                   "        ,@voidreason " & _
                   "        ,@employeeunkid " & _
                   "        ,@ismanual " & _
                   "    ); SELECT @@identity,1 AS atype " & _
                   "END " & _
                   "ELSE " & _
                   "BEGIN " & _
                   "    DECLARE @customanalysistranunkid AS INT " & _
                   "    SET @customanalysistranunkid = (SELECT customanalysistranunkid FROM hrcompetency_customitem_tran WITH (NOLOCK) WHERE isvoid = 0 AND customanalysistranguid = @customanalysistranguid AND analysisunkid = @analysisunkid AND customitemunkid = @customitemunkid AND periodunkid = @periodunkid AND employeeunkid = @employeeunkid) " & _
                   "    UPDATE hrcompetency_customitem_tran SET " & _
                   "         analysisunkid = @analysisunkid " & _
                   "        ,customitemunkid = @customitemunkid " & _
                   "        ,custom_value = @custom_value " & _
                   "        ,isvoid = @isvoid " & _
                   "        ,voiddatetime = @voiddatetime " & _
                   "        ,voiduserunkid = @voiduserunkid " & _
                   "        ,voidreason = @voidreason " & _
                   "        ,periodunkid  = @periodunkid " & _
                   "        ,employeeunkid = @employeeunkid " & _
                   "     WHERE customanalysistranunkid = @customanalysistranunkid " & _
                   "    ;SELECT @customanalysistranunkid,2 AS atype " & _
                   "END "

            objDataOperation.AddParameter("@customanalysistranguid", SqlDbType.NVarChar, icustomanalysistranguid.ToString.Length, icustomanalysistranguid)
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ianalysisunkid)
            objDataOperation.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, icustomitemunkid)
            objDataOperation.AddParameter("@custom_value", SqlDbType.NText, icustom_value.ToString.Length, icustom_value)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, iisvoid)
            If ivoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ivoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ivoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, ivoidreason)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iperiodunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iemployeeunkid)
            objDataOperation.AddParameter("@ismanual", SqlDbType.Bit, eZeeDataType.BIT_SIZE, iismanual)

            Dim dsList As New DataSet
            Dim intAuditTypeId As Integer = 0

            If icustomanalysistranunkid <= 0 Then
                dsList = objDataOperation.ExecQuery(StrQ, "List")
            Else
                objDataOperation.ExecNonQuery(StrQ)
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If icustomanalysistranunkid <= 0 Then
                icustomanalysistranunkid = dsList.Tables(0).Rows(0).Item(0)
                intAuditTypeId = dsList.Tables(0).Rows(0).Item(1)
            End If

            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", ianalysisunkid, "hrcompetency_customitem_tran", "customanalysistranunkid", icustomanalysistranunkid, 2, intAuditTypeId, , intUserUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdate; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |18-JAN-2020| -- END

#End Region

End Class

﻿'************************************************************************************************************************************
'Class Name : clsAppraisal_ShortList_Master.vb
'Purpose    :
'Date       :08-Feb-12
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsAppraisal_ShortList_Master
    Private Shared ReadOnly mstrModuleName As String = "clsAppraisal_ShortList_Master"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintShortlistunkid As Integer
    Private mdtReferencedate As Date
    Private mstrReferenceno As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrRemark As String = String.Empty
    Private mintPeriodunkid As Integer
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shortlistunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Shortlistunkid() As Integer
        Get
            Return mintShortlistunkid
        End Get
        Set(ByVal value As Integer)
            mintShortlistunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referencedate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Referencedate() As Date
        Get
            Return mdtReferencedate
        End Get
        Set(ByVal value As Date)
            mdtReferencedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referenceno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Referenceno() As String
        Get
            Return mstrReferenceno
        End Get
        Set(ByVal value As String)
            mstrReferenceno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  shortlistunkid " & _
              ", referencedate " & _
              ", referenceno " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(remark,'') AS remark " & _
              ", periodunkid " & _
             "FROM hrapps_shortlist_master " & _
             "WHERE shortlistunkid = @shortlistunkid "

            'S.SANDEEP [ 05 MARCH 2012 ISNULL(remark,''),periodunkid ] -- START -- END

            objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShortlistunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintshortlistunkid = CInt(dtRow.Item("shortlistunkid"))
                mdtreferencedate = dtRow.Item("referencedate")
                mstrreferenceno = dtRow.Item("referenceno").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtvoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mstrRemark = dtRow.Item("remark").ToString
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                'S.SANDEEP [ 05 MARCH 2012 ] -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  hrapps_shortlist_master.shortlistunkid " & _
                   ", CONVERT(CHAR(8),hrapps_shortlist_master.referencedate,112) AS referencedate " & _
                   ", hrapps_shortlist_master.referenceno " & _
                   ", hrapps_shortlist_master.userunkid " & _
                   ", hrapps_shortlist_master.isvoid " & _
                   ", hrapps_shortlist_master.voiduserunkid " & _
                   ", hrapps_shortlist_master.voiddatetime " & _
                   ", hrapps_shortlist_master.voidreason " & _
                   ", ISNULL(hrapps_shortlist_master.remark,'') AS remark " & _
                   ", hrapps_shortlist_master.periodunkid " & _
                   ", ISNULL(cfcommon_period_tran.period_name,'') AS Period " & _
                   "FROM hrapps_shortlist_master " & _
                   " LEFT JOIN cfcommon_period_tran ON hrapps_shortlist_master.periodunkid = cfcommon_period_tran.periodunkid "

            'S.SANDEEP [ 05 MARCH 2012 ISNULL(remark,''),periodunkid ] -- START -- END

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrapps_shortlist_master) </purpose>
    Public Function Insert(ByVal mdtFilter As DataTable, ByVal dsEmployee As DataSet) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@referencedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReferencedate.ToString)
            objDataOperation.AddParameter("@referenceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReferenceno.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, 4000, mstrRemark.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            'S.SANDEEP [ 05 MARCH 2012 ] -- END



            strQ = "INSERT INTO hrapps_shortlist_master ( " & _
              "  referencedate " & _
              ", referenceno " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", remark " & _
              ", periodunkid " & _
            ") VALUES (" & _
              "  @referencedate " & _
              ", @referenceno " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @remark " & _
              ", @periodunkid " & _
            "); SELECT @@identity"

            'S.SANDEEP [ 05 MARCH 2012 remark,periodunkid ] -- START -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintShortlistunkid = dsList.Tables(0).Rows(0).Item(0)

            Dim objMFilter As New clsAppraisal_Filter

            objMFilter._Shortlistunkid = mintShortlistunkid
            objMFilter._DataTable = mdtFilter
            objMFilter._Userunkid = mintUserunkid

            If objMFilter._DataTable IsNot Nothing Then
                If objMFilter._DataTable.Rows.Count > 0 Then

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If objMFilter.Insert_Filter(objDataOperation, dsEmployee.Tables(0), mintUserunkid) = False Then
                        'If objMFilter.Insert_Filter(objDataOperation, dsEmployee.Tables(0)) = False Then
                        'S.SANDEEP [04 JUN 2015] -- END
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrapps_shortlist_master) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.AddParameter("@shortlistunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintshortlistunkid.ToString)
            objDataOperation.AddParameter("@referencedate", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtreferencedate.ToString)
            objDataOperation.AddParameter("@referenceno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrreferenceno.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, 4000, mstrRemark.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            strQ = "UPDATE hrapps_shortlist_master SET " & _
              "  referencedate = @referencedate" & _
              ", referenceno = @referenceno" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", remark = @remark " & _
              ", periodunkid = @periodunkid " & _
            "WHERE shortlistunkid = @shortlistunkid "

            'S.SANDEEP [ 05 MARCH 2012 remark,periodunkid ] -- START -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrapps_shortlist_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        'S.SANDEEP [ 19 JULY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim blnFlag As Boolean = False
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 19 JULY 2012 ] -- END
        Try
            strQ = "UPDATE hrapps_shortlist_master SET " & _
                   " isvoid = 1,voiduserunkid=@voiduserunkid,voiddatetime=@voiddatetime,voidreason = @voidreason " & _
                   "WHERE shortlistunkid = @shortlistunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim dsChild As New DataSet
            dsChild = clsCommonATLog.GetChildList(objDataOperation, "hrapps_filter", "shortlistunkid", intUnkid)
            If dsChild.Tables(0).Rows.Count > 0 Then
                strQ = "UPDATE hrapps_filter SET  isvoid = 1,voiduserunkid=@voiduserunkid,voiddatetime=@voiddatetime,voidreason = @voidreason " & _
                       "WHERE shortlistunkid = @shortlistunkid "
                For Each dRow As DataRow In dsChild.Tables(0).Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                    objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    objDataOperation.ExecNonQuery(strQ)

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrapps_shortlist_master", "shortlistunkid", intUnkid, "hrapps_filter", "shortlistunkid", dRow.Item("filterunkid"), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next
                blnFlag = True
            End If

            dsChild = clsCommonATLog.GetChildList(objDataOperation, "hrapps_finalemployee", "shortlistunkid", intUnkid)
            If dsChild.Tables(0).Rows.Count > 0 Then
                strQ = "UPDATE hrapps_finalemployee SET  isvoid = 1,voiduserunkid=@voiduserunkid,voiddatetime=@voiddatetime,voidreason = @voidreason " & _
                       "WHERE shortlistunkid = @shortlistunkid "
                For Each dRow As DataRow In dsChild.Tables(0).Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                    objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    objDataOperation.ExecNonQuery(strQ)

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrapps_shortlist_master", "shortlistunkid", intUnkid, "hrapps_finalemployee", "shortlistunkid", dRow.Item("finalemployeeunkid"), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next
                blnFlag = True
            End If

            If blnFlag = False Then
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrapps_shortlist_master", "shortlistunkid", intUnkid, "", "", 0, 3, 0) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 19 JULY 2012 ] -- END


            Return True
        Catch ex As Exception
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 19 JULY 2012 ] -- END
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ = "SELECT 1 FROM hrapps_finalemployee WHERE isvoid = 0 AND shortlistunkid = @shortlistunkid AND operationmodeid > 0 "
            'S.SANDEEP [ 14 AUG 2013 ] -- END
            objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function getComboList(ByVal strList As String, Optional ByVal blnFlag As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If blnFlag = True Then
            '    StrQ = " SELECT 0 AS shortlistunkid ,@Select AS referenceno ,'19000101' AS referencedate UNION "
            'End If
            'StrQ &= " SELECT shortlistunkid ,referenceno ,ISNULL(CONVERT(CHAR(8),referencedate,112),'19000101') AS referencedate FROM dbo.hrapps_shortlist_master WHERE isvoid = 0 "
            If blnFlag = True Then
                StrQ = " SELECT 0 AS shortlistunkid ,@Select AS referenceno ,'19000101' AS referencedate ,'' AS remark UNION "
            End If
            StrQ &= "SELECT " & _
                    "    shortlistunkid " & _
                    "   ,referenceno " & _
                    "   ,ISNULL(CONVERT(CHAR(8),referencedate,112),'19000101') AS referencedate " & _
                    "   ,ISNULL(remark,'') AS remark " & _
                    "FROM hrapps_shortlist_master " & _
                    "WHERE isvoid = 0 "
            'S.SANDEEP [ 14 AUG 2013 ] -- END

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            If strList.Trim.Length <= 0 Then strList = "List"

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

﻿'************************************************************************************************************************************
'Class Name :clsassess_plan_customitem_tran.vb
'Purpose    :
'Date       :24-Dec-2015
'Written By :Sandeep Sharma
'Modified   :ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_plan_customitem_tran
    Private Shared ReadOnly mstrModuleName As String = "clsassess_plan_customitem_tran"
    Dim mstrMessage As String = ""

#Region " Private Variables "

    Private mintEmployeeUnkid As Integer = 0
    Private mintplancustomunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mstrplancustomguid As String = ""
    Private mintcustomitemunkid As Integer = 0
    Private mstrcustom_value As String = ""
    Private mdtransactiondate As DateTime = Nothing
    Private mblnIsvoid As Boolean = False
    Private mdtvoiddatetime As DateTime = Nothing
    Private mintvoiduserunkid As Integer = 0
    Private mstrvoidreason As String = String.Empty
    Private mblnIsfinal As Boolean = False
    Private mintcustomheaderunkid As Integer = 0

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Private mstrFormName As String = String.Empty
    'Shani (26-Sep-2016) -- End


#End Region

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _EmployeeUnkid() As Integer
        Get
            Return mintEmployeeUnkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public Property _plancustomunkid() As Integer
        Get
            Return mintplancustomunkid
        End Get
        Set(ByVal value As Integer)
            mintplancustomunkid = value
        End Set
    End Property

    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    Public Property _plancustomguid() As String
        Get
            Return mstrplancustomguid
        End Get
        Set(ByVal value As String)
            mstrplancustomguid = value
        End Set
    End Property

    Public Property _customitemunkid() As Integer
        Get
            Return mintcustomitemunkid
        End Get
        Set(ByVal value As Integer)
            mintcustomitemunkid = value
        End Set
    End Property

    Public Property _custom_value() As String
        Get
            Return mstrcustom_value
        End Get
        Set(ByVal value As String)
            mstrcustom_value = value
        End Set
    End Property

    Public Property _transactiondate() As DateTime
        Get
            Return mdtransactiondate
        End Get
        Set(ByVal value As DateTime)
            mdtransactiondate = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _voiddatetime() As DateTime
        Get
            Return mdtvoiddatetime
        End Get
        Set(ByVal value As DateTime)
            mdtvoiddatetime = value
        End Set
    End Property

    Public Property _voiduserunkid() As Integer
        Get
            Return mintvoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintvoiduserunkid = value
        End Set
    End Property

    Public Property _voidreason() As String
        Get
            Return mstrvoidreason
        End Get
        Set(ByVal value As String)
            mstrvoidreason = value
        End Set
    End Property

    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    Public Property _customheaderunkid() As Integer
        Get
            Return mintcustomheaderunkid
        End Get
        Set(ByVal value As Integer)
            mintcustomheaderunkid = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

#End Region

#Region " List Methods "

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            ByVal intEmployeeId As Integer, _
                            ByVal intPeriodId As Integer, _
                            ByVal intCustomHeaderId As Integer, _
                            Optional ByVal strFilterString As String = "") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Using objDo As New clsDataOperation

                StrQ = "SELECT " & _
                       "     hrassess_plan_customitem_tran.plancustomunkid " & _
                       "    ,hrassess_plan_customitem_tran.plancustomguid " & _
                       "    ,hrassess_plan_customitem_tran.customitemunkid " & _
                       "    ,hrassess_plan_customitem_tran.custom_value " & _
                       "    ,hrassess_plan_customitem_tran.employeeunkid " & _
                       "    ,hrassess_plan_customitem_tran.periodunkid " & _
                       "    ,hrassess_plan_customitem_tran.transactiondate " & _
                       "    ,hrassess_plan_customitem_tran.isvoid " & _
                       "    ,hrassess_plan_customitem_tran.voiddatetime " & _
                       "    ,hrassess_plan_customitem_tran.voiduserunkid " & _
                       "    ,hrassess_plan_customitem_tran.voidreason " & _
                       "    ,hrassess_plan_customitem_tran.isfinal " & _
                       "    ,hrassess_plan_customitem_tran.customheaderunkid " & _
                       "    ,hrassess_custom_items.itemtypeid " & _
                       "    ,hrassess_custom_items.selectionmodeid " & _
                       "    ,'' AS display_value " & _
                       "FROM hrassess_plan_customitem_tran " & _
                       "    JOIN hrassess_custom_items ON hrassess_custom_items.customitemunkid = hrassess_plan_customitem_tran.customitemunkid  " & _
                       "WHERE hrassess_plan_customitem_tran.isvoid = 0 AND hrassess_plan_customitem_tran.employeeunkid = '" & intEmployeeId & "' " & _
                       "    AND hrassess_plan_customitem_tran.periodunkid = '" & intPeriodId & "' AND hrassess_plan_customitem_tran.customheaderunkid = '" & intCustomHeaderId & "' "

                If strFilterString.Trim.Length > 0 Then
                    StrQ &= " AND " & strFilterString & " "
                End If

                If strTableName.Trim.Length <= 0 Then strTableName = "List"

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetList(ByVal intEmployeeId As Integer, _
                            ByVal intPeriodId As Integer, _
                            ByVal intHeaderUnkid As Integer, _
                            ByVal strList As String, _
                            ByVal mblnIsCompanyNeedReviewer As Boolean, _
                            Optional ByVal blnOnlyFinal As Boolean = False) As DataTable

        'Shani (26-Sep-2016) -- [mblnIsCompanyNeedReviewer]

        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim mdtFinal As DataTable = Nothing
        Dim dCol As DataColumn
        Dim StrQ As String = ""

        'Shani (26-Sep-2016) -- Start
        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
        Dim objCustomHeader As New clsassess_custom_header
        Dim objCustomItems As New clsassess_custom_items
        'Shani (26-Sep-2016) -- End
        Try

            If strList.Trim.Length <= 0 Then strList = "List"

            mdtFinal = New DataTable(strList)

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            objCustomHeader._Customheaderunkid = intHeaderUnkid
            'Shani (26-Sep-2016) -- End


            dsList = objCustomItems.getComboList(intPeriodId, intHeaderUnkid, False, "List")

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'objCustomItems = Nothing
            'Shani (26-Sep-2016) -- End


            If dsList.Tables(0).Rows.Count > 0 Then

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                'For Each drow As DataRow In dsList.Tables(0).Rows
                For Each drow As DataRow In dsList.Tables(0).Select("", "isdefaultentry DESC")
                    'Shani (26-Sep-2016) -- End
                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "CItem_" & drow.Item("Id").ToString
                        .Caption = drow.Item("Name").ToString
                        .DataType = System.Type.GetType("System.String")
                        .DefaultValue = ""
                    End With
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "CItem_" & drow.Item("Id").ToString & "Id"
                        .Caption = ""
                        .DataType = System.Type.GetType("System.Int32")
                        .DefaultValue = -1
                        'Shani (26-Sep-2016) -- Start
                        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                        .ExtendedProperties.Add("IsSystemGen", drow.Item("isdefaultentry"))
                        'Shani (26-Sep-2016) -- End
                    End With
                    mdtFinal.Columns.Add(dCol)
                Next

                'S.SANDEEP [29 DEC 2015] -- START
                'dCol = New DataColumn
                'With dCol
                '    .ColumnName = "GUID"
                '    .Caption = ""
                '    .DataType = System.Type.GetType("System.String")
                '    .DefaultValue = ""
                'End With
                'mdtFinal.Columns.Add(dCol)
                'S.SANDEEP [29 DEC 2015] -- END

            End If

            'S.SANDEEP [29 DEC 2015] -- START
                dCol = New DataColumn
                With dCol
                    .ColumnName = "GUID"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.String")
                    .DefaultValue = ""
                End With
                mdtFinal.Columns.Add(dCol)
            'S.SANDEEP [29 DEC 2015] -- END

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            dCol = New DataColumn
            With dCol
                .ColumnName = "competenciesunkid"
                .Caption = ""
                .DataType = System.Type.GetType("System.Int32")
                .DefaultValue = -1
            End With
            mdtFinal.Columns.Add(dCol)
            'Shani (26-Sep-2016) -- End


            

            Using objDo As New clsDataOperation
                objDo.ClearParameters()

                StrQ = "SELECT " & _
                       "     hrassess_plan_customitem_tran.plancustomunkid " & _
                       "    ,hrassess_plan_customitem_tran.plancustomguid " & _
                       "    ,hrassess_plan_customitem_tran.customitemunkid " & _
                       "    ,hrassess_plan_customitem_tran.custom_value " & _
                       "    ,hrassess_plan_customitem_tran.employeeunkid " & _
                       "    ,hrassess_plan_customitem_tran.periodunkid " & _
                       "    ,hrassess_plan_customitem_tran.transactiondate " & _
                       "    ,hrassess_plan_customitem_tran.isvoid " & _
                       "    ,hrassess_plan_customitem_tran.voiddatetime " & _
                       "    ,hrassess_plan_customitem_tran.voiduserunkid " & _
                       "    ,hrassess_plan_customitem_tran.voidreason " & _
                       "    ,hrassess_plan_customitem_tran.isfinal " & _
                       "    ,hrassess_plan_customitem_tran.customheaderunkid " & _
                       "    ,hrassess_custom_items.itemtypeid " & _
                       "    ,hrassess_custom_items.selectionmodeid " & _
                       "FROM hrassess_plan_customitem_tran " & _
                       "    JOIN hrassess_custom_items ON hrassess_custom_items.customitemunkid = hrassess_plan_customitem_tran.customitemunkid " & _
                       "WHERE hrassess_plan_customitem_tran.isvoid = 0 AND employeeunkid = '" & intEmployeeId & "' " & _
                       "    AND hrassess_custom_items.customheaderunkid = '" & intHeaderUnkid & "' " & _
                       "    AND hrassess_custom_items.periodunkid = '" & intPeriodId & "' "

                If blnOnlyFinal Then
                    StrQ &= " AND isfinal = 1 "
                End If

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dFRow As DataRow = Nothing
                Dim iCustomId, iNewCustomId As Integer : iCustomId = 0 : iNewCustomId = 0
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If iCustomId <= 0 Then iCustomId = dRow.Item("customitemunkid")
                    iNewCustomId = dRow.Item("customitemunkid")
                    If iCustomId = iNewCustomId Then
                        dFRow = mdtFinal.NewRow
                        mdtFinal.Rows.Add(dFRow)
                    End If
                    Select Case CInt(dRow.Item("itemtypeid"))
                        Case clsassess_custom_items.enCustomType.FREE_TEXT
                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
                        Case clsassess_custom_items.enCustomType.SELECTION
                            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                Select Case CInt(dRow.Item("selectionmodeid"))
                                    'S.SANDEEP [04 OCT 2016] -- START
                                    'Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                        'S.SANDEEP [04 OCT 2016] -- END
                                        Dim objCMaster As New clsCommon_Master
                                        objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                        objCMaster = Nothing
                                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                        Dim objCMaster As New clsassess_competencies_master
                                        objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                        objCMaster = Nothing
                                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                        Dim objEmpField1 As New clsassess_empfield1_master
                                        objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
                                        objEmpField1 = Nothing
                                        'S.SANDEEP |16-AUG-2019| -- START
                                        'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                    Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                        Dim objCMaster As New clsCommon_Master
                                        objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                        objCMaster = Nothing
                                        'S.SANDEEP |16-AUG-2019| -- END
                                End Select
                            End If
                        Case clsassess_custom_items.enCustomType.DATE_SELECTION
                            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                            End If
                        Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                If IsNumeric(dRow.Item("custom_value")) Then
                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
                                End If
                            End If
                    End Select
                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
                    dFRow.Item("GUID") = dRow.Item("plancustomguid")
                    dFRow.Item("competenciesunkid") = -1
                Next

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            ElseIf objCustomHeader._IsMatch_With_Competency = True Then

                StrQ = "SELECT " & _
                       "     COMP_CAT.name AS comp_cate " & _
                       "    ,hrassess_competencies_master.name AS comp " & _
                       "    ,hrassess_competencies_master.competenciesunkid " & _
                       "    ,NewId() AS GUID " & _
                       "FROM hrevaluation_analysis_master " & _
                       "    LEFT JOIN hrcompetency_analysis_tran ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                       "    LEFT JOIN hrassess_competencies_master ON hrassess_competencies_master.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                       "    LEFT JOIN cfcommon_master AS COMP_CAT ON COMP_CAT.masterunkid = hrassess_competencies_master.competence_categoryunkid AND COMP_CAT.mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & "' " & _
                       "WHERE hrassess_competencies_master.isactive = 1  AND hrcompetency_analysis_tran.isvoid = 0 " & _
                       "    AND hrevaluation_analysis_master.isvoid = 0 AND COMP_CAT.isactive = 1 " & _
                       "    AND hrevaluation_analysis_master.periodunkid = '" & objCustomHeader._MappedPeriodUnkid & "' " & _
                       "    AND hrevaluation_analysis_master.assessmodeid = " & IIf(mblnIsCompanyNeedReviewer, enAssessmentMode.REVIEWER_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT) & " " & _
                       "    AND hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmployeeId & "' " & _
                       "    AND hrcompetency_analysis_tran.result <= " & objCustomHeader._Min_Score & " "

                Using objDo As New clsDataOperation
                    objDo.ClearParameters()
                    dsList = objDo.ExecQuery(StrQ, "List")

                    If objDo.ErrorMessage <> "" Then
                        exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                        Throw exForce
                    End If
                End Using
                Dim dsCItem As DataSet = objCustomItems.getComboList(intPeriodId, intHeaderUnkid, False, "List")

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso dsCItem IsNot Nothing AndAlso dsCItem.Tables(0).Rows.Count > 0 Then
                    Dim strGuId As String = System.Guid.NewGuid().ToString
                    Dim iCustomRow = dsCItem.Tables(0).Select("isdefaultentry = 1 ")
                    If iCustomRow.Count = 2 Then
                        Dim iRow As DataRow
                        For Each dtRow As DataRow In dsList.Tables(0).Rows
                            iRow = mdtFinal.NewRow
                            iRow.Item("GUID") = dtRow.Item("GUID")

                            iRow.Item("CItem_" & iCustomRow(0).Item("id").ToString) = dtRow.Item("comp_cate")
                            iRow.Item("CItem_" & iCustomRow(0).Item("id").ToString & "Id") = iCustomRow(0).Item("id").ToString

                            iRow.Item("CItem_" & iCustomRow(1).Item("id").ToString) = dtRow.Item("comp")
                            iRow.Item("CItem_" & iCustomRow(1).Item("id").ToString & "Id") = iCustomRow(1).Item("id").ToString
                            iRow.Item("competenciesunkid") = dtRow.Item("competenciesunkid")
                            mdtFinal.Rows.Add(iRow)
                        Next
                    End If
                Else
                    mdtFinal.Rows.Add(mdtFinal.NewRow)
                End If
                'Shani (26-Sep-2016) -- End
            Else
                mdtFinal.Rows.Add(mdtFinal.NewRow)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            objCustomHeader = Nothing
            objCustomItems = Nothing
            'Shani (26-Sep-2016) -- End
        End Try
        Return mdtFinal
    End Function

    Public Function Get_CItems_ForAddEdit(ByVal iPeriodId As Integer, ByVal iHeaderId As Integer) As DataTable
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Using objDo As New clsDataOperation

                StrQ = "SELECT " & _
                       "     custom_item " & _
                       "    ,'' AS custom_value " & _
                       "    ,itemtypeid " & _
                       "    ,selectionmodeid " & _
                       "    ,CAST(NULL AS DATETIME) AS ddate " & _
                       "    ,0 AS selectedid " & _
                       "    ,customitemunkid " & _
                       "    ,isdefaultentry " & _
                      "FROM hrassess_custom_items " & _
                       "WHERE customheaderunkid = '" & iHeaderId & "' AND periodunkid = '" & iPeriodId & "' AND isactive = 1 "

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_CItems_ForAddEdit; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList.Tables(0)
    End Function

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' <purpose> INSERT INTO Database Table (hrassess_plan_customitem_tran) </purpose>
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function Insert(ByVal xUserUnkid As Integer) As Boolean
        If isExist(mintEmployeeUnkid, mintPeriodunkid, mintcustomitemunkid, mintcustomheaderunkid, mstrplancustomguid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Custom Item is already defined for the employee for selected period.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            Using objDo As New clsDataOperation

                objDo.ClearParameters()
                objDo.BindTransaction()

                objDo.AddParameter("@plancustomguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrplancustomguid.ToString)
                objDo.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcustomitemunkid.ToString)

                'Shani(06-Feb-2016) -- Start
                'PA Changes Given By Glory for CCBRT
                'objDo.AddParameter("@custom_value", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrcustom_value.ToString)
                objDo.AddParameter("@custom_value", SqlDbType.NVarChar, mstrcustom_value.Length, mstrcustom_value.ToString)
                'Shani(06-Feb-2016) -- End

                objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)
                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                objDo.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtransactiondate)
                objDo.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                If mdtvoiddatetime <> Nothing Then
                    objDo.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
                Else
                    objDo.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDo.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
                objDo.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)
                objDo.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
                objDo.AddParameter("@customheaderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcustomheaderunkid.ToString)

                strQ = "INSERT INTO hrassess_plan_customitem_tran ( " & _
                           "  plancustomguid " & _
                           ", customitemunkid " & _
                           ", custom_value " & _
                           ", employeeunkid " & _
                           ", periodunkid " & _
                           ", transactiondate " & _
                           ", isvoid " & _
                           ", voiddatetime " & _
                           ", voiduserunkid " & _
                           ", voidreason " & _
                           ", isfinal " & _
                           ", customheaderunkid" & _
                       ") VALUES (" & _
                           "  @plancustomguid " & _
                           ", @customitemunkid " & _
                           ", @custom_value " & _
                           ", @employeeunkid " & _
                           ", @periodunkid " & _
                           ", @transactiondate " & _
                           ", @isvoid " & _
                           ", @voiddatetime " & _
                           ", @voiduserunkid " & _
                           ", @voidreason " & _
                           ", @isfinal " & _
                           ", @customheaderunkid" & _
                       "); SELECT @@identity"

                dsList = objDo.ExecQuery(strQ, "List")

                If objDo.ErrorMessage <> "" Then
                    objDo.ReleaseTransaction(False)
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                mintplancustomunkid = dsList.Tables(0).Rows(0).Item(0)

                If clsCommonATLog.Insert_AtLog(objDo, 1, "hrassess_plan_customitem_tran", "plancustomunkid", mintplancustomunkid, , xUserUnkid) = False Then
                    objDo.ReleaseTransaction(False)
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                objDo.ReleaseTransaction(True)

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' <purpose> Update Database Table (hrassess_plan_customitem_tran) </purpose>
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function Update(ByVal xUserUnkid As Integer) As Boolean
        If isExist(mintEmployeeUnkid, mintPeriodunkid, mintcustomitemunkid, mintcustomheaderunkid, mstrplancustomguid, mintplancustomunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Custom Item is already defined for the employee for selected period.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            Using objDo As New clsDataOperation

                objDo.ClearParameters()

                objDo.BindTransaction()

                objDo.AddParameter("@plancustomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintplancustomunkid.ToString)
                objDo.AddParameter("@plancustomguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrplancustomguid.ToString)
                objDo.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcustomitemunkid.ToString)

                'Shani(06-Feb-2016) -- Start
                'PA Changes Given By Glory for CCBRT
                'objDo.AddParameter("@custom_value", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrcustom_value.ToString)
                objDo.AddParameter("@custom_value", SqlDbType.NVarChar, mstrcustom_value.Length, mstrcustom_value.ToString)
                'Shani(06-Feb-2016) -- End

                objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)
                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                objDo.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtransactiondate)
                objDo.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                If mdtvoiddatetime <> Nothing Then
                    objDo.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
                Else
                    objDo.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDo.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
                objDo.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)
                objDo.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
                objDo.AddParameter("@customheaderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcustomheaderunkid.ToString)

                strQ = "UPDATE hrassess_plan_customitem_tran SET " & _
                       "  plancustomguid = @plancustomguid" & _
                       " ,customitemunkid = @customitemunkid" & _
                       " ,custom_value = @custom_value" & _
                       " ,employeeunkid = @employeeunkid" & _
                       " ,periodunkid = @periodunkid" & _
                       " ,transactiondate = @transactiondate" & _
                       " ,isvoid = @isvoid" & _
                       " ,voiddatetime = @voiddatetime" & _
                       " ,voiduserunkid = @voiduserunkid" & _
                       " ,voidreason = @voidreason" & _
                       " ,isfinal = @isfinal" & _
                       " ,customheaderunkid = @customheaderunkid " & _
                       "WHERE plancustomunkid = @plancustomunkid "

                Call objDo.ExecNonQuery(strQ)

                If objDo.ErrorMessage <> "" Then
                    objDo.ReleaseTransaction(False)
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.Insert_AtLog(objDo, 2, "hrassess_plan_customitem_tran", "plancustomunkid", mintplancustomunkid, , xUserUnkid) = False Then
                    objDo.ReleaseTransaction(False)
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                objDo.ReleaseTransaction(True)

            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' <purpose> Delete Database Table (hrassess_plan_customitem_tran) </purpose>
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function Delete() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            Using objDo As New clsDataOperation

                objDo.ClearParameters()

                objDo.BindTransaction()

                objDo.AddParameter("@plancustomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintplancustomunkid.ToString)
                objDo.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDo.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
                objDo.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
                objDo.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

                strQ = "UPDATE hrassess_plan_customitem_tran SET " & _
                       "  isvoid = @isvoid " & _
                       " ,voiddatetime = @voiddatetime " & _
                       " ,voiduserunkid = @voiduserunkid " & _
                       " ,voidreason = @voidreason " & _
                       "WHERE plancustomunkid = @plancustomunkid "

                Call objDo.ExecNonQuery(strQ)

                If objDo.ErrorMessage <> "" Then
                    objDo.ReleaseTransaction(False)
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.Insert_AtLog(objDo, 3, "hrassess_plan_customitem_tran", "plancustomunkid", mintplancustomunkid, , mintvoiduserunkid) = False Then
                    objDo.ReleaseTransaction(False)
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                objDo.ReleaseTransaction(True)

            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return True
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="strGUID"></param>
    ''' <param name="intEmployeeId"></param>
    ''' <param name="intPeriodId"></param>
    ''' <param name="intCustomHeaderId"></param>
    ''' <param name="intOperationMode">1= ADD, 2= EDIT, 3 = DELETE</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsFinalSaved(ByVal strGUID As String, ByVal intEmployeeId As Integer, _
                                 ByVal intPeriodId As Integer, ByVal intCustomHeaderId As Integer, ByVal intOperationMode As Integer) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim strMessage As String = String.Empty
        Dim iCnt As Integer = -1
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT 1 FROM hrassess_plan_customitem_tran WHERE isfinal = 1 "

                If strGUID.Trim.Length > 0 Then
                    StrQ &= " AND plancustomguid = '" & strGUID & "' "
                End If

                If intEmployeeId > 0 Then
                    StrQ &= " AND employeeunkid = '" & intEmployeeId & "' "
                End If

                If intPeriodId > 0 Then
                    StrQ &= " AND periodunkid = '" & intPeriodId & "' "
                End If

                If intCustomHeaderId > 0 Then
                    StrQ &= " AND customheaderunkid = '" & intCustomHeaderId & "' "
                End If

                iCnt = objDo.RecordCount(StrQ)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                If iCnt > 0 Then
                    'S.SANDEEP |12-MAR-2019| -- START
                    'ISSUE/ENHANCEMENT : {Action List Phase 2 - 61}
                    'Select Case intOperationMode
                    '    Case 1
                    '        strMessage = Language.getMessage(mstrModuleName, 2, "Sorry, you cannot add custom items planning. Reason, custom item(s) already final saved.")
                    '    Case 2
                    '        strMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot edit custom items planning. Reason, custom item(s) already final saved.")
                    '    Case 3
                    '        strMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot void custom items planning. Reason, custom item(s) already final saved.")
                    'End Select
                    Select Case intOperationMode
                        Case 1
                            strMessage = Language.getMessage(mstrModuleName, 200, "Sorry, you cannot add custom items planning. Reason, custom item(s) already final approved.")
                        Case 2
                            strMessage = Language.getMessage(mstrModuleName, 300, "Sorry, you cannot edit custom items planning. Reason, custom item(s) already final approved.")
                        Case 3
                            strMessage = Language.getMessage(mstrModuleName, 400, "Sorry, you cannot void custom items planning. Reason, custom item(s) already final approved.")
                    End Select
                    'S.SANDEEP |12-MAR-2019| -- END
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsFinalSaved; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strMessage
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpId As Integer, _
                            ByVal intPeriodId As Integer, _
                            ByVal intCustomItemUnkid As Integer, _
                            ByVal intCustomHeaderId As Integer, _
                            ByVal strCustomGuid As String, _
                            Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Using objDo As New clsDataOperation

                strQ = "SELECT " & _
                       "  plancustomunkid " & _
                       ", plancustomguid " & _
                       ", customitemunkid " & _
                       ", custom_value " & _
                       ", employeeunkid " & _
                       ", periodunkid " & _
                       ", transactiondate " & _
                       ", isvoid " & _
                       ", voiddatetime " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                       ", isfinal " & _
                       ", customheaderunkid " & _
                       "FROM hrassess_plan_customitem_tran " & _
                       "WHERE employeeunkid = '" & intEmpId & "' " & _
                       " AND periodunkid = '" & intPeriodId & "' AND customheaderunkid =  '" & intCustomHeaderId & "' " & _
                       " AND customitemunkid =  '" & intCustomItemUnkid & "' AND isvoid  = 0 AND plancustomguid = '" & strCustomGuid & "' "

                If intUnkid > 0 Then
                    strQ &= " AND plancustomunkid <> @plancustomunkid"
                    objDo.AddParameter("@plancustomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                End If


                dsList = objDo.ExecQuery(strQ, "List")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function GetCustomeDataHeaderWise(ByVal iEmpUnkId As Integer, ByVal IPeriodUnkId As Integer, ByVal IHeaderUnkId As Integer, ByVal iCompanyNeedReviewer As Boolean) As DataTable
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim blnIsRecordExist As Boolean = True
        Dim mdtTran As New DataTable
        Try
            mdtTran = New DataTable("custom")

            mdtTran.Columns.Add("plancustomunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("plancustomguid", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("customitemunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("custom_value", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("custom_header", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("custom_item", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("display_value", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("itemtypeid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("selectionmodeid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isdefaultentry", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("customheaderunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("isfinal", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("transactiondate", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("ismanual", GetType(System.Boolean)).DefaultValue = True

            StrQ = "SELECT " & _
                   "     hrcompetency_customitem_tran.customanalysistranguid AS plancustomguid " & _
                   "    ,-1 AS plancustomunkid " & _
                   "    ,hrcompetency_customitem_tran.customitemunkid " & _
                   "    ,hrcompetency_customitem_tran.periodunkid " & _
                   "    ,hrcompetency_customitem_tran.custom_value " & _
                   "    ,hrcompetency_customitem_tran.isvoid " & _
                   "    ,hrcompetency_customitem_tran.voiddatetime " & _
                   "    ,hrcompetency_customitem_tran.voiduserunkid " & _
                   "    ,hrcompetency_customitem_tran.voidreason " & _
                   "    ,'' AS AUD " & _
                   "    ,hrassess_custom_items.custom_item AS custom_item " & _
                   "    ,hrassess_custom_headers.name AS custom_header " & _
                   "    ,hrassess_custom_items.itemtypeid " & _
                   "    ,hrassess_custom_items.selectionmodeid " & _
                   "    ,'' AS display_value " & _
                   "    ,hrassess_custom_items.isdefaultentry AS isdefaultentry " & _
                   "    ,hrassess_custom_headers.customheaderunkid " & _
                   "    ,1 AS isfinal " & _
                   "    ,hrcompetency_customitem_tran.ismanual " & _
                   "FROM hrcompetency_customitem_tran " & _
                   "    JOIN hrassess_custom_items ON hrassess_custom_items.customitemunkid = hrcompetency_customitem_tran.customitemunkid " & _
                   "    JOIN hrassess_custom_headers ON hrassess_custom_headers.customheaderunkid = hrassess_custom_items.customheaderunkid " & _
                   "WHERE hrcompetency_customitem_tran.isvoid = 0 " & _
                   "    AND hrcompetency_customitem_tran.periodunkid = '" & IPeriodUnkId & "'  " & _
                   "    AND hrcompetency_customitem_tran.employeeunkid = '" & iEmpUnkId & "' " & _
                   "    AND hrassess_custom_headers.customheaderunkid = '" & IHeaderUnkId & "'  "

            Using objDataOperation As New clsDataOperation
                objDataOperation.ClearParameters()
                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End Using

            If dsList.Tables(0).Rows.Count <= 0 Then
                blnIsRecordExist = False

                StrQ = "SELECT " & _
                       "     hrassess_plan_customitem_tran.plancustomguid AS plancustomguid " & _
                       "    ,hrassess_plan_customitem_tran.plancustomunkid AS plancustomunkid " & _
                       "    ,hrassess_plan_customitem_tran.customitemunkid " & _
                       "    ,hrassess_plan_customitem_tran.periodunkid " & _
                       "    ,hrassess_plan_customitem_tran.employeeunkid " & _
                       "    ,hrassess_plan_customitem_tran.custom_value " & _
                       "    ,hrassess_plan_customitem_tran.isvoid " & _
                       "    ,hrassess_plan_customitem_tran.voiddatetime " & _
                       "    ,hrassess_plan_customitem_tran.voiduserunkid " & _
                       "    ,hrassess_plan_customitem_tran.voidreason " & _
                       "    ,'' AS AUD " & _
                       "    ,hrassess_custom_items.custom_item AS custom_item " & _
                       "    ,hrassess_custom_headers.name AS custom_header " & _
                       "    ,hrassess_custom_items.itemtypeid " & _
                       "    ,hrassess_custom_items.selectionmodeid " & _
                       "    ,'' AS display_value " & _
                       "    ,hrassess_custom_items.isdefaultentry AS isdefaultentry " & _
                       "    ,hrassess_custom_headers.customheaderunkid " & _
                       "    ,hrassess_plan_customitem_tran.isfinal " & _
                       "    ,hrassess_plan_customitem_tran.ismanual " & _
                       "FROM hrassess_plan_customitem_tran " & _
                       "    JOIN hrassess_custom_items ON hrassess_custom_items.customitemunkid = hrassess_plan_customitem_tran.customitemunkid " & _
                       "    JOIN hrassess_custom_headers ON hrassess_custom_headers.customheaderunkid = hrassess_custom_items.customheaderunkid " & _
                       "WHERE isvoid = 0 AND hrassess_custom_headers.isinclude_planning = 1 " & _
                       "    AND hrassess_plan_customitem_tran.employeeunkid = '" & iEmpUnkId & "' " & _
                       "    AND hrassess_plan_customitem_tran.periodunkid = '" & IPeriodUnkId & "' " & _
                       "    AND hrassess_custom_headers.customheaderunkid = '" & IHeaderUnkId & "'  "

                Using objDataOperation As New clsDataOperation
                    objDataOperation.ClearParameters()
                    dsList = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End Using

            End If

            mdtTran.Rows.Clear()
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Select Case CInt(dtRow.Item("itemtypeid"))
                    Case clsassess_custom_items.enCustomType.FREE_TEXT
                        dtRow.Item("display_value") = dtRow.Item("custom_value")
                    Case clsassess_custom_items.enCustomType.SELECTION
                        If dtRow.Item("custom_value").ToString.Trim.Length > 0 Then
                            Select Case CInt(dtRow.Item("selectionmodeid"))
                                'S.SANDEEP [04 OCT 2016] -- START
                                'Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                    'S.SANDEEP [04 OCT 2016] -- END
                                    Dim objCMaster As New clsCommon_Master
                                    objCMaster._Masterunkid = CInt(dtRow.Item("custom_value"))
                                    dtRow.Item("display_value") = objCMaster._Name
                                    objCMaster = Nothing
                                Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                    Dim objCMaster As New clsassess_competencies_master
                                    objCMaster._Competenciesunkid = CInt(dtRow.Item("custom_value"))
                                    dtRow.Item("display_value") = objCMaster._Name
                                    objCMaster = Nothing
                                Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                    Dim objEmpField1 As New clsassess_empfield1_master
                                    objEmpField1._Empfield1unkid = CInt(dtRow.Item("custom_value"))
                                    dtRow.Item("display_value") = objEmpField1._Field_Data
                                    objEmpField1 = Nothing

                                    'S.SANDEEP |16-AUG-2019| -- START
                                    'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                    Dim objCMaster As New clsCommon_Master
                                    objCMaster._Masterunkid = CInt(dtRow.Item("custom_value"))
                                    dtRow.Item("display_value") = objCMaster._Name
                                    objCMaster = Nothing
                                    'S.SANDEEP |16-AUG-2019| -- END

                            End Select
                        End If
                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
                        If dtRow.Item("custom_value").ToString.Trim.Length > 0 Then
                            dtRow.Item("display_value") = eZeeDate.convertDate(dtRow.Item("custom_value").ToString).ToShortDateString
                        End If
                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                        If dtRow.Item("custom_value").ToString.Trim.Length > 0 Then
                            If IsNumeric(dtRow.Item("custom_value")) Then
                                dtRow.Item("display_value") = CDbl(dtRow.Item("custom_value")).ToString
                            End If
                        End If
                End Select
                mdtTran.ImportRow(dtRow)
            Next

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            If mdtTran.Rows.Count <= 0 Then
                Dim dsCustomHeader As DataSet = (New clsassess_custom_header).GetList("List")
                Dim dsCItem As DataSet = (New clsassess_custom_items).GetList("List")

                Dim strFilter As String = "isallowaddpastperiod = 1 "

                If IHeaderUnkId > 0 Then
                    strFilter &= " AND customheaderunkid = '" & IHeaderUnkId & "'  "
                End If

                For Each dRow As DataRow In dsCustomHeader.Tables(0).Select(strFilter)
                    StrQ = "SELECT " & _
                            "    A.Value " & _
                            "   ,A.competenciesunkid " & _
                            "   ,A.rno " & _
                            "FROM ( " & _
                            "       SELECT " & _
                            "            COMP_CAT.name AS Value " & _
                            "           ,hrassess_competencies_master.competenciesunkid " & _
                            "           ,1 AS rno " & _
                            "       FROM hrevaluation_analysis_master " & _
                            "           LEFT JOIN hrcompetency_analysis_tran ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                            "           LEFT JOIN hrassess_competencies_master ON hrassess_competencies_master.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                            "           LEFT JOIN cfcommon_master AS COMP_CAT ON COMP_CAT.masterunkid = hrassess_competencies_master.competence_categoryunkid AND COMP_CAT.mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & "' " & _
                            "       WHERE hrassess_competencies_master.isactive = 1  AND hrcompetency_analysis_tran.isvoid = 0 " & _
                            "           AND hrevaluation_analysis_master.isvoid = 0 AND COMP_CAT.isactive = 1 " & _
                            "           AND hrevaluation_analysis_master.periodunkid = '" & dRow.Item("mappedperiodunkid") & "' " & _
                            "           AND hrevaluation_analysis_master.assessmodeid = " & IIf(iCompanyNeedReviewer, enAssessmentMode.REVIEWER_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT) & " " & _
                            "           AND hrevaluation_analysis_master.assessedemployeeunkid = '" & iEmpUnkId & "' " & _
                            "           AND hrcompetency_analysis_tran.result <= " & dRow.Item("min_score") & " " & _
                            "   UNION ALL " & _
                            "       SELECT " & _
                            "            hrassess_competencies_master.name AS Value " & _
                            "           ,hrassess_competencies_master.competenciesunkid " & _
                            "           ,2 AS rno " & _
                            "       FROM hrevaluation_analysis_master " & _
                            "           LEFT JOIN hrcompetency_analysis_tran ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                            "           LEFT JOIN hrassess_competencies_master ON hrassess_competencies_master.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                            "       WHERE hrassess_competencies_master.isactive = 1  AND hrcompetency_analysis_tran.isvoid = 0 " & _
                            "           AND hrevaluation_analysis_master.isvoid = 0 " & _
                            "           AND hrevaluation_analysis_master.periodunkid = '" & dRow.Item("mappedperiodunkid") & "' " & _
                            "           AND hrevaluation_analysis_master.assessmodeid = " & IIf(iCompanyNeedReviewer, enAssessmentMode.REVIEWER_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT) & " " & _
                            "           AND hrevaluation_analysis_master.assessedemployeeunkid = '" & iEmpUnkId & "' " & _
                            "           AND hrcompetency_analysis_tran.result <= " & dRow.Item("min_score") & " " & _
                            ") AS A ORDER BY A.competenciesunkid,A.rno "

                    Using objDataOperation As New clsDataOperation
                        objDataOperation.ClearParameters()
                        dsList = objDataOperation.ExecQuery(StrQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End Using

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso dsCItem IsNot Nothing AndAlso dsCItem.Tables(0).Rows.Count > 0 Then
                        If dsCItem.Tables(0).Select("periodunkid = '" & IPeriodUnkId & "' AND customheaderunkid = '" & dRow.Item("customheaderunkid").ToString & "' ").Count > 0 Then
                            Dim iCustomRow = dsCItem.Tables(0).Select("periodunkid = '" & IPeriodUnkId & "' AND customheaderunkid = '" & dRow.Item("customheaderunkid").ToString & "' AND isdefaultentry = 1 ")

                            If iCustomRow.Count = 2 Then
                                Dim iRow As DataRow
                                Dim strGuid As String = ""
                                For Each dtRow As DataRow In dsList.Tables(0).Rows
                                    If CInt(dtRow.Item("rno")) = 1 Then
                                        strGuid = System.Guid.NewGuid().ToString
                                    End If
                                    iRow = mdtTran.NewRow
                                    iRow.Item("plancustomguid") = strGuid
                                    iRow.Item("plancustomunkid") = 0

                                    If CInt(dtRow.Item("rno")) = 1 Then
                                        iRow.Item("customitemunkid") = iCustomRow(0).Item("customitemunkid").ToString
                                        iRow.Item("custom_item") = iCustomRow(0).Item("custom_item").ToString
                                        iRow.Item("itemtypeid") = iCustomRow(0).Item("itemtypeid").ToString
                                    Else
                                        iRow.Item("customitemunkid") = iCustomRow(1).Item("customitemunkid").ToString
                                        iRow.Item("custom_item") = iCustomRow(1).Item("custom_item").ToString
                                        iRow.Item("itemtypeid") = iCustomRow(1).Item("itemtypeid").ToString
                                    End If
                                    iRow.Item("periodunkid") = IPeriodUnkId
                                    iRow.Item("custom_value") = dtRow.Item("Value")
                                    iRow.Item("custom_header") = dRow.Item("name")
                                    iRow.Item("display_value") = dtRow.Item("Value")
                                    iRow.Item("AUD") = "A"
                                    iRow.Item("isdefaultentry") = True
                                    iRow.Item("customheaderunkid") = dRow.Item("customheaderunkid")
                                    iRow.Item("isfinal") = 0
                                    iRow.Item("ismanual") = False
                                    mdtTran.Rows.Add(iRow)
                                Next
                            End If
                        End If
                    End If
                Next
            End If
            'Shani (26-Sep-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAnalysisTran", mstrModuleName)
        Finally
        End Try
        Return mdtTran
    End Function

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Public Function InsertUpdateDelete_CustomAnalysisTran(ByVal mdtTran As DataTable, ByVal intUserUnkid As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Using objDataOperation As New clsDataOperation
                clsCommonATLog._WebFormName = mstrModuleName

                For Each dRow As DataRow In mdtTran.Rows
                    With dRow
                        objDataOperation.ClearParameters()
                        If Not IsDBNull(.Item("AUD")) Then
                            Select Case .Item("AUD")
                                Case "A"
                                    StrQ = "INSERT INTO hrassess_plan_customitem_tran ( " & _
                                           "     plancustomguid " & _
                                           "    ,customitemunkid " & _
                                           "    ,custom_value " & _
                                           "    ,employeeunkid " & _
                                           "    ,periodunkid " & _
                                           "    ,transactiondate " & _
                                           "    ,isvoid " & _
                                           "    ,voiddatetime " & _
                                           "    ,voiduserunkid " & _
                                           "    ,voidreason " & _
                                           "    ,isfinal " & _
                                           "    ,customheaderunkid " & _
                                           "    ,ismanual " & _
                                           ") VALUES (" & _
                                           "     @plancustomguid " & _
                                           "    ,@customitemunkid " & _
                                           "    ,@custom_value " & _
                                           "    ,@employeeunkid " & _
                                           "    ,@periodunkid " & _
                                           "    ,@transactiondate " & _
                                           "    ,@isvoid " & _
                                           "    ,@voiddatetime " & _
                                           "    ,@voiduserunkid " & _
                                           "    ,@voidreason " & _
                                           "    ,@isfinal " & _
                                           "    ,@customheaderunkid " & _
                                           "    ,@ismanual " & _
                                           "); SELECT @@identity"


                                    objDataOperation.AddParameter("@plancustomguid", SqlDbType.NVarChar, .Item("plancustomguid").ToString.Length, .Item("plancustomguid").ToString)
                                    objDataOperation.AddParameter("@customitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("customitemunkid").ToString)
                                    objDataOperation.AddParameter("@custom_value", SqlDbType.NVarChar, .Item("custom_value").ToString.Length, .Item("custom_value").ToString)
                                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)
                                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                                    objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("transactiondate"))
                                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                    objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isfinal").ToString)
                                    objDataOperation.AddParameter("@customheaderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("customheaderunkid").ToString)
                                    objDataOperation.AddParameter("@ismanual", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("ismanual").ToString)

                                    Dim dsList As New DataSet
                                    dsList = objDataOperation.ExecQuery(StrQ, "List")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    .Item("plancustomunkid") = dsList.Tables(0).Rows(0).Item(0)
                                    If intUserUnkid <= 0 Then
                                        clsCommonATLog._LoginEmployeeUnkid = mintEmployeeUnkid
                                    End If

                                    If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_plan_customitem_tran", "plancustomunkid", .Item("plancustomunkid"), , intUserUnkid) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Case "U"
                                    StrQ = "UPDATE hrassess_plan_customitem_tran SET " & _
                                           "    custom_value = @custom_value " & _
                                           "WHERE plancustomunkid = @plancustomunkid "

                                    objDataOperation.AddParameter("@plancustomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("plancustomunkid").ToString)
                                    objDataOperation.AddParameter("@custom_value", SqlDbType.NVarChar, .Item("custom_value").ToString.Length, .Item("custom_value").ToString)

                                    'Shani(06-MAR-2017) -- Start
                                    Dim dsList As New DataSet
                                    dsList = objDataOperation.ExecQuery(StrQ, "List")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                    'Shani(06-MAR-2017) -- End

                                    If intUserUnkid <= 0 Then
                                        clsCommonATLog._LoginEmployeeUnkid = mintEmployeeUnkid
                                    End If

                                    If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_plan_customitem_tran", "plancustomunkid", .Item("plancustomunkid"), , intUserUnkid) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                Case "D"

                                    StrQ = "UPDATE hrassess_plan_customitem_tran SET " & _
                                           "     isvoid = @isvoid " & _
                                           "    ,voiddatetime = @voiddatetime " & _
                                           "    ,voiduserunkid = @voiduserunkid " & _
                                           "    ,voidreason = @voidreason " & _
                                           "WHERE plancustomunkid = @plancustomunkid "

                                    objDataOperation.AddParameter("@plancustomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("plancustomunkid").ToString)
                                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)

                                    If IsDBNull(.Item("voiddatetime")) = True Then
                                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                    Else
                                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                    End If

                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)


                                    Call objDataOperation.ExecNonQuery(StrQ)

                                    If intUserUnkid <= 0 Then
                                        clsCommonATLog._LoginEmployeeUnkid = mintEmployeeUnkid
                                    End If

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_plan_customitem_tran", "plancustomunkid", .Item("plancustomunkid"), , mintvoiduserunkid) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                            End Select
                        End If
                    End With
                Next

            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function
    'Shani (26-Sep-2016) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Custom Item is already defined for the employee for selected period.")
            Language.setMessage(mstrModuleName, 200, "Sorry, you cannot add custom items planning. Reason, custom item(s) already final approved.")
            Language.setMessage(mstrModuleName, 300, "Sorry, you cannot edit custom items planning. Reason, custom item(s) already final approved.")
            Language.setMessage(mstrModuleName, 400, "Sorry, you cannot void custom items planning. Reason, custom item(s) already final approved.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

﻿Imports Aruti.Data.SSRef
Imports eZeeCommonLib
Imports System.IO
Imports System.Net

Public Class clsFileUploadDownload


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Shared Function UploadFile(ByVal strFilePath As String, ByVal strFolderName As String, ByVal strfilename As String, ByRef strErrorMessage As String) As Boolean
    Public Shared Function UploadFile(ByVal strFilePath As String, ByVal strFolderName As String, ByVal strfilename As String, ByRef strErrorMessage As String, ByVal strArutiSelfServiceURL As String) As Boolean
        'Shani(24-Aug-2015) -- End

        Try

            Dim HttpBinding As New System.ServiceModel.BasicHttpBinding
            With HttpBinding
                .MaxBufferSize = 2147483647
                'Sohail (04 Jul 2019) -- Start
                'PACT Enhancement - Support Issue Id # 3955 - 76.1 - The remote server returned an unexpected response: (413) Request Entity Too Large..
                .MaxBufferPoolSize = 2147483647
                .ReaderQuotas.MaxDepth = 2000000
                'Sohail (04 Jul 2019) -- End
                .MaxReceivedMessageSize = 2147483647
                .ReaderQuotas.MaxStringContentLength = 2147483647
                .ReaderQuotas.MaxArrayLength = 2147483647
                .ReaderQuotas.MaxBytesPerRead = 2147483647
                .ReaderQuotas.MaxNameTableCharCount = 2147483647
                .OpenTimeout = New TimeSpan(0, 20, 0)
                .ReceiveTimeout = New TimeSpan(0, 20, 0)
                .SendTimeout = New TimeSpan(0, 20, 0)
                'Sohail (05 Jul 2019) -- Start
                'NMB Issue - Support Issue Id # - 76.1 - The provided URI scheme 'https' is invalid; expected 'http'. Parameter name: via.
                '.Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport
                If strArutiSelfServiceURL.ToLower.Contains("https:") = True Then
                    .Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport
                Else
                    .Security.Mode = ServiceModel.BasicHttpSecurityMode.None
                    .Security.Transport.ClientCredentialType = ServiceModel.HttpClientCredentialType.None
                End If
                .MessageEncoding = ServiceModel.WSMessageEncoding.Text '[CCBRT Error: The content type text/html of the response message does not match the content type of the binding]
                'Sohail (05 Jul 2019) -- End
            End With

            Dim EndPointAdd As System.ServiceModel.EndpointAddress


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim strServicePath As String = ConfigParameter._Object._ArutiSelfServiceURL
            'If Strings.Right(strServicePath, 1) <> "/" OrElse Strings.Right(strServicePath, 1) <> "\" Then
            '    strServicePath += "/"
            'End If
            'strServicePath += "ArutiFileTransferService.asmx"
            'EndPointAdd = New System.ServiceModel.EndpointAddress(New Uri(strServicePath))
            If Strings.Right(strArutiSelfServiceURL, 1) <> "/" OrElse Strings.Right(strArutiSelfServiceURL, 1) <> "\" Then
                strArutiSelfServiceURL += "/"
            End If
            strArutiSelfServiceURL += "ArutiFileTransferService.asmx"

            EndPointAdd = New System.ServiceModel.EndpointAddress(New Uri(strArutiSelfServiceURL))
            'Shani(24-Aug-2015) -- End


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'Dim SSRef As New ArutiFileTransferServiceSoapClient(HttpBinding, EndPointAdd)
            'Dim UserCred As New UserCredentials

            Dim SSRef As New SSRef.ArutiFileTransferService
            Dim UserCred As New SSRef.UserCredentials
            SSRef.Url = strArutiSelfServiceURL
            SSRef.Timeout = 1200000 '20mins timeout

            'Gajanan [23-SEP-2019] -- End

            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            System.Net.ServicePointManager.Expect100Continue = False 'Sohail (25 Oct 2016) - [Issue : The remote server returned an error: (417) Expectation Failed]
            Try
                System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
            Catch ex As Exception

            End Try
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications
            'Sohail (20 Oct 2020) -- End

            UserCred.Password = clsSecurity.Decrypt("Qw0hgX7HJ0cge3U8zJbx44dZa+ZiNCzTLLNaxe5cIlv5vcoS7F/wiymPNWvVT+YK", "ezee")


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            SSRef.UserCredentialsValue = UserCred
            'Gajanan [23-SEP-2019] -- End


            Dim f As New System.IO.FileInfo(strFilePath)
            Dim filebytes As Byte() = File.ReadAllBytes(strFilePath)

            strErrorMessage = ""

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'If SSRef.UploadFile(UserCred, filebytes, strFolderName, strfilename, strErrorMessage) = False Then
            If SSRef.UploadFile(filebytes, strFolderName, strfilename, strErrorMessage) = False Then
                'Gajanan [23-SEP-2019] -- End
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            If ex.Message.Contains("TCP error code 10060") Then
                strErrorMessage = "Aruti Self Servivce not found"
            Else
                strErrorMessage = ex.Message & " Error In UploadFile"
            End If
            Return False
        End Try
    End Function


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Shared Function UpdateFile(ByVal strOldFilePath As String, ByVal strFilePath As String, ByVal strFolderName As String, ByRef strErrorMessage As String) As Boolean
    Public Shared Function UpdateFile(ByVal strOldFilePath As String, ByVal strFilePath As String, ByVal strFolderName As String, ByRef strErrorMessage As String, ByVal strArutiSelfServiceURL As String) As Boolean
        'Shani(24-Aug-2015) -- End

        Try

            Dim HttpBinding As New System.ServiceModel.BasicHttpBinding
            With HttpBinding
                .MaxBufferSize = 2147483647
                'Sohail (04 Jul 2019) -- Start
                'PACT Enhancement - Support Issue Id # 3955 - 76.1 - The remote server returned an unexpected response: (413) Request Entity Too Large..
                .MaxBufferPoolSize = 2147483647
                .ReaderQuotas.MaxDepth = 2000000
                'Sohail (04 Jul 2019) -- End
                .MaxReceivedMessageSize = 2147483647
                .ReaderQuotas.MaxStringContentLength = 2147483647
                .ReaderQuotas.MaxArrayLength = 2147483647
                .ReaderQuotas.MaxBytesPerRead = 2147483647
                .ReaderQuotas.MaxNameTableCharCount = 2147483647
                .OpenTimeout = New TimeSpan(0, 20, 0)
                .ReceiveTimeout = New TimeSpan(0, 20, 0)
                .SendTimeout = New TimeSpan(0, 20, 0)
                'Sohail (05 Jul 2019) -- Start
                'NMB Issue - Support Issue Id # - 76.1 - The provided URI scheme 'https' is invalid; expected 'http'. Parameter name: via.
                '.Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport
                If strArutiSelfServiceURL.ToLower.Contains("https:") = True Then
                    .Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport
                Else
                    .Security.Mode = ServiceModel.BasicHttpSecurityMode.None
                    .Security.Transport.ClientCredentialType = ServiceModel.HttpClientCredentialType.None
                End If
                .MessageEncoding = ServiceModel.WSMessageEncoding.Text '[CCBRT Error: The content type text/html of the response message does not match the content type of the binding]
                'Sohail (05 Jul 2019) -- End
            End With

            Dim EndPointAdd As System.ServiceModel.EndpointAddress

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim strServicePath As String = ConfigParameter._Object._ArutiSelfServiceURL
            'If strOldFilePath.Contains(strServicePath) = False Then
            '    strErrorMessage = "Cannot update record first delete record after insert record"
            'End If
            'If Strings.Right(strServicePath, 1) <> "/" OrElse Strings.Right(strServicePath, 1) <> "\" Then
            '    strServicePath += "/"
            'End If
            'strServicePath += "ArutiFileTransferService.asmx"
            ''strServicePath = "http://localhost:1764/ArutiFileTransferService.asmx"
            'EndPointAdd = New System.ServiceModel.EndpointAddress(New Uri(strServicePath))


            If strOldFilePath.Contains(strArutiSelfServiceURL) = False Then
                strErrorMessage = "Cannot update record first delete record after insert record"
            End If

            If Strings.Right(strArutiSelfServiceURL, 1) <> "/" OrElse Strings.Right(strArutiSelfServiceURL, 1) <> "\" Then
                strArutiSelfServiceURL += "/"
            End If
            strArutiSelfServiceURL += "ArutiFileTransferService.asmx"

            EndPointAdd = New System.ServiceModel.EndpointAddress(New Uri(strArutiSelfServiceURL))

            'Shani(24-Aug-2015) -- End


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'Dim SSRef As New ArutiFileTransferServiceSoapClient(HttpBinding, EndPointAdd)
            'Dim UserCred As New UserCredentials

            Dim SSRef As New SSRef.ArutiFileTransferService
            Dim UserCred As New SSRef.UserCredentials
            SSRef.Url = strArutiSelfServiceURL
            SSRef.Timeout = 1200000 '20mins timeout
            'Gajanan [23-SEP-2019] -- End

            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            System.Net.ServicePointManager.Expect100Continue = False 'Sohail (25 Oct 2016) - [Issue : The remote server returned an error: (417) Expectation Failed]
            Try
                System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
            Catch ex As Exception

            End Try
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications
            'Sohail (20 Oct 2020) -- End

            UserCred.Password = clsSecurity.Decrypt("Qw0hgX7HJ0cge3U8zJbx44dZa+ZiNCzTLLNaxe5cIlv5vcoS7F/wiymPNWvVT+YK", "ezee")

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            SSRef.UserCredentialsValue = UserCred
            'Gajanan [23-SEP-2019] -- End

            strErrorMessage = ""
            strOldFilePath = strOldFilePath.Substring(strOldFilePath.IndexOf("uploadimage"), (Len(strOldFilePath) - strOldFilePath.IndexOf("uploadimage")))
            strFilePath = strFilePath.Substring(strFilePath.IndexOf("uploadimage"), (Len(strFilePath) - strFilePath.IndexOf("uploadimage")))

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'If SSRef.UpdateFile(UserCred, strOldFilePath, strFilePath, strFolderName, strErrorMessage) = False Then
            If SSRef.UpdateFile(strOldFilePath, strFilePath, strFolderName, strErrorMessage) = False Then
                'Gajanan [23-SEP-2019] -- End
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            If ex.Message.Contains("TCP error code 10060") Then
                strErrorMessage = "Aruti Self Servivce not found"
            Else
                strErrorMessage = ex.Message & " Error In Update File"
            End If
            Return False
        End Try
    End Function


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Shared Function DownloadFile(ByVal strFilePath As String, ByVal strFileName As String, ByVal strFolderName As String, ByRef strErrorMessage As String) As Byte()
    Public Shared Function DownloadFile(ByVal strFilePath As String, ByVal strFileName As String, ByVal strFolderName As String, ByRef strErrorMessage As String, ByVal strArutiSelfServiceURL As String) As Byte()
        'Shani(24-Aug-2015) -- End

        Dim filebytes As Byte() = Nothing
        Try

            'Gajanan [11-NOV-2019] -- Start   
            'Dim HttpBinding As New System.ServiceModel.BasicHttpBinding
            'With HttpBinding
            '    .MaxBufferSize = 2147483647
            '    'Sohail (04 Jul 2019) -- Start
            '    'PACT Enhancement - Support Issue Id # 3955 - 76.1 - The remote server returned an unexpected response: (413) Request Entity Too Large..
            '    .MaxBufferPoolSize = 2147483647
            '    .ReaderQuotas.MaxDepth = 2000000
            '    'Sohail (04 Jul 2019) -- End
            '    .MaxReceivedMessageSize = 2147483647
            '    .ReaderQuotas.MaxStringContentLength = 2147483647
            '    .ReaderQuotas.MaxArrayLength = 2147483647
            '    .ReaderQuotas.MaxBytesPerRead = 2147483647
            '    .ReaderQuotas.MaxNameTableCharCount = 2147483647
            '    .OpenTimeout = New TimeSpan(0, 20, 0)
            '    .ReceiveTimeout = New TimeSpan(0, 20, 0)
            '    .SendTimeout = New TimeSpan(0, 20, 0)
            '    'Sohail (05 Jul 2019) -- Start
            '    'NMB Issue - Support Issue Id # - 76.1 - The provided URI scheme 'https' is invalid; expected 'http'. Parameter name: via.
            '    '.Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport
            '    If strArutiSelfServiceURL.ToLower.Contains("https:") = True Then
            '        .Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport
            '    Else
            '        .Security.Mode = ServiceModel.BasicHttpSecurityMode.None
            '        .Security.Transport.ClientCredentialType = ServiceModel.HttpClientCredentialType.None
            '    End If
            '    .MessageEncoding = ServiceModel.WSMessageEncoding.Text '[CCBRT Error: The content type text/html of the response message does not match the content type of the binding]
            '    'Sohail (05 Jul 2019) -- End
            'End With

            GC.Collect()
            'Gajanan [11-NOV-2019] -- End   


            Dim EndPointAdd As System.ServiceModel.EndpointAddress

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim strServicePath As String = ConfigParameter._Object._ArutiSelfServiceURL
            'If strFilePath.Contains(strServicePath) Then
            '    If Strings.Right(strServicePath, 1) <> "/" OrElse Strings.Right(strServicePath, 1) <> "\" Then
            '        strServicePath += "/"
            '    End If
            '    strServicePath += "ArutiFileTransferService.asmx"
            '    EndPointAdd = New System.ServiceModel.EndpointAddress(New Uri(strServicePath))

            If strFilePath.Contains(strArutiSelfServiceURL) Then
                If Strings.Right(strArutiSelfServiceURL, 1) <> "/" OrElse Strings.Right(strArutiSelfServiceURL, 1) <> "\" Then
                    strArutiSelfServiceURL += "/"
                End If

                strArutiSelfServiceURL += "ArutiFileTransferService.asmx"

                EndPointAdd = New System.ServiceModel.EndpointAddress(New Uri(strArutiSelfServiceURL))
                'Shani(24-Aug-2015) -- End


                'Gajanan [23-SEP-2019] -- Start    
                'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                'Dim SSRef As New ArutiFileTransferServiceSoapClient(HttpBinding, EndPointAdd)
                'Dim UserCred As New UserCredentials
                Dim SSRef As New SSRef.ArutiFileTransferService
                Dim UserCred As New SSRef.UserCredentials
                SSRef.Url = strArutiSelfServiceURL
                SSRef.Timeout = 1200000 '20mins timeout
                'Gajanan [23-SEP-2019] -- End

                'Sohail (20 Oct 2020) -- Start
                'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
                System.Net.ServicePointManager.Expect100Continue = False 'Sohail (25 Oct 2016) - [Issue : The remote server returned an error: (417) Expectation Failed]
                Try
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
                Catch ex As Exception

                End Try
                System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications
                'Sohail (20 Oct 2020) -- End

                UserCred.Password = clsSecurity.Decrypt("Qw0hgX7HJ0cge3U8zJbx44dZa+ZiNCzTLLNaxe5cIlv5vcoS7F/wiymPNWvVT+YK", "ezee")
                'Gajanan [23-SEP-2019] -- Start    
                'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                SSRef.UserCredentialsValue = UserCred
                'Gajanan [23-SEP-2019] -- End
                strErrorMessage = ""


                'Gajanan [23-SEP-2019] -- Start    
                'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                'filebytes = SSRef.DownloadFile(UserCred, strFolderName, strFileName, strErrorMessage)
                filebytes = SSRef.DownloadFile(strFolderName, strFileName, strErrorMessage)
                'Gajanan [23-SEP-2019] -- End


            ElseIf strFilePath.Contains("http://") Then
                Dim HttpReq As HttpWebRequest = DirectCast(WebRequest.Create(strFilePath), HttpWebRequest)

                Using HttpResponse As HttpWebResponse = DirectCast(HttpReq.GetResponse(), HttpWebResponse)
                    Using Reader As New BinaryReader(HttpResponse.GetResponseStream())
                        filebytes = Reader.ReadBytes(1 * 1024 * 1024 * 10)
                    End Using
                End Using
            ElseIf System.IO.File.Exists(strFilePath) Then
                'Call UploadFile(strFilePath, strFolderName, strFileName, strErrorMessage)
                'Dim f As New System.IO.FileInfo(strFilePath)
                'filebytes = File.ReadAllBytes(strFilePath)
            End If

        Catch ex As Exception
            filebytes = Nothing
            If ex.Message.Contains("TCP error code 10060") Then
                strErrorMessage = "Aruti Self Servivce not found"
            Else
                strErrorMessage = ex.Message & " Error In DownloadFile"
            End If
        End Try
        Return filebytes
    End Function


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Shared Function DeleteFile(ByVal strFilePath As String, ByVal strFileName As String, ByVal strFolderName As String, ByRef strErrorMessage As String) As Boolean
    Public Shared Function DeleteFile(ByVal strFilePath As String, ByVal strFileName As String, ByVal strFolderName As String, ByRef strErrorMessage As String, ByVal strArutiSelfServiceURL As String, ByVal intCompanyId As Integer) As Boolean 'Hemant [8-April-2019] -- Add intCompanyId
        'Shani(24-Aug-2015) -- End

        Dim bln As Boolean
        Try
            Dim HttpBinding As New System.ServiceModel.BasicHttpBinding
            With HttpBinding
                .MaxBufferSize = 2147483647
                'Sohail (04 Jul 2019) -- Start
                'PACT Enhancement - Support Issue Id # 3955 - 76.1 - The remote server returned an unexpected response: (413) Request Entity Too Large..
                .MaxBufferPoolSize = 2147483647
                .ReaderQuotas.MaxDepth = 2000000
                'Sohail (04 Jul 2019) -- End
                .MaxReceivedMessageSize = 2147483647
                .ReaderQuotas.MaxStringContentLength = 2147483647
                .ReaderQuotas.MaxArrayLength = 2147483647
                .ReaderQuotas.MaxBytesPerRead = 2147483647
                .ReaderQuotas.MaxNameTableCharCount = 2147483647
                .OpenTimeout = New TimeSpan(0, 20, 0)
                .ReceiveTimeout = New TimeSpan(0, 20, 0)
                .SendTimeout = New TimeSpan(0, 20, 0)
                'Sohail (05 Jul 2019) -- Start
                'NMB Issue - Support Issue Id # - 76.1 - The provided URI scheme 'https' is invalid; expected 'http'. Parameter name: via.
                '.Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport
                If strArutiSelfServiceURL.ToLower.Contains("https:") = True Then
                    .Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport
                Else
                    .Security.Mode = ServiceModel.BasicHttpSecurityMode.None
                    .Security.Transport.ClientCredentialType = ServiceModel.HttpClientCredentialType.None
                End If
                .MessageEncoding = ServiceModel.WSMessageEncoding.Text '[CCBRT Error: The content type text/html of the response message does not match the content type of the binding]
                'Sohail (05 Jul 2019) -- End
            End With

            Dim EndPointAdd As System.ServiceModel.EndpointAddress

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim strServicePath As String = ConfigParameter._Object._ArutiSelfServiceURL
            'If strFilePath.Contains(strServicePath) Then
            '    If Strings.Right(strServicePath, 1) <> "/" OrElse Strings.Right(strServicePath, 1) <> "\" Then
            '        strServicePath += "/"
            '    End If

            '    strServicePath += "ArutiFileTransferService.asmx"

            '    EndPointAdd = New System.ServiceModel.EndpointAddress(New Uri(strServicePath))

            If strFilePath.Contains(strArutiSelfServiceURL) Then
                If Strings.Right(strArutiSelfServiceURL, 1) <> "/" OrElse Strings.Right(strArutiSelfServiceURL, 1) <> "\" Then
                    strArutiSelfServiceURL += "/"
                End If

                strArutiSelfServiceURL += "ArutiFileTransferService.asmx"

                EndPointAdd = New System.ServiceModel.EndpointAddress(New Uri(strArutiSelfServiceURL))

                'Shani(24-Aug-2015) -- End

                'Gajanan [23-SEP-2019] -- Start    
                'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                'Dim SSRef As New ArutiFileTransferServiceSoapClient(HttpBinding, EndPointAdd)
                'Dim UserCred As New UserCredentials
                Dim SSRef As New SSRef.ArutiFileTransferService
                Dim UserCred As New SSRef.UserCredentials
                SSRef.Url = strArutiSelfServiceURL
                SSRef.Timeout = 1200000 '20mins timeout
                'Gajanan [23-SEP-2019] -- End

                'Sohail (20 Oct 2020) -- Start
                'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
                System.Net.ServicePointManager.Expect100Continue = False 'Sohail (25 Oct 2016) - [Issue : The remote server returned an error: (417) Expectation Failed]
                Try
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
                Catch ex As Exception

                End Try
                System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications
                'Sohail (20 Oct 2020) -- End

                UserCred.Password = clsSecurity.Decrypt("Qw0hgX7HJ0cge3U8zJbx44dZa+ZiNCzTLLNaxe5cIlv5vcoS7F/wiymPNWvVT+YK", "ezee")

                'Gajanan [23-SEP-2019] -- Start    
                'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                SSRef.UserCredentialsValue = UserCred
                'Gajanan [23-SEP-2019] -- End

                strErrorMessage = ""

                'Gajanan [23-SEP-2019] -- Start    
                'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                'bln = SSRef.DeleteFile(UserCred, strFolderName, strFileName, strErrorMessage)
                bln = SSRef.DeleteFile(strFolderName, strFileName, strErrorMessage)
                'Gajanan [23-SEP-2019] -- End
            Else

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'strServicePath = strFilePath.Substring(0, strFilePath.IndexOf("/uploadimage"))
                'If Strings.Right(strServicePath, 1) <> "/" OrElse Strings.Right(strServicePath, 1) <> "\" Then
                '    strServicePath += "/"
                'End If

                'strServicePath += "ArutiFileTransferService.asmx"

                'EndPointAdd = New System.ServiceModel.EndpointAddress(New Uri(strServicePath))


                'Hemant [8-April-2019] -- Start
                'strArutiSelfServiceURL = strFilePath.Substring(0, strFilePath.IndexOf("/uploadimage"))
                'If Strings.Right(strArutiSelfServiceURL, 1) <> "/" OrElse Strings.Right(strArutiSelfServiceURL, 1) <> "\" Then
                '    strArutiSelfServiceURL += "/"
                'End If

                'strArutiSelfServiceURL += "ArutiFileTransferService.asmx"

                'EndPointAdd = New System.ServiceModel.EndpointAddress(New Uri(strArutiSelfServiceURL))

                ''Shani(24-Aug-2015) -- End

                'Dim SSRef As New ArutiFileTransferServiceSoapClient(HttpBinding, EndPointAdd)
                'Dim UserCred As New UserCredentials

                'UserCred.Password = clsSecurity.Decrypt("Qw0hgX7HJ0cge3U8zJbx44dZa+ZiNCzTLLNaxe5cIlv5vcoS7F/wiymPNWvVT+YK", "ezee")

                'strErrorMessage = ""
                'bln = SSRef.DeleteFile(UserCred, strFolderName, strFileName, strErrorMessage)

                Dim objConfig As New clsConfigOptions
                Dim strDocPath As String = String.Empty
                strDocPath = objConfig.GetKeyValue(intCompanyId, "DocumentPath")
                If strDocPath IsNot Nothing AndAlso strDocPath.Trim.Length > 0 Then
                    If Directory.Exists(strDocPath) Then
                        Dim strDocLocalPath As String = strDocPath & "\" & strFolderName & "\" & strFileName
                        If File.Exists(strDocLocalPath) Then
                            File.Delete(strDocLocalPath)
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage("clsFileUploadDownload", 131, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                objConfig = Nothing
                'Hemant [8-April-2019] -- End

            End If
            
        Catch ex As Exception
            bln = False
            If ex.Message.Contains("TCP error code 10060") Then
                strErrorMessage = "Aruti Self Servivce not found"
            Else
                strErrorMessage = ex.Message & " Error In DeleteFile"
            End If

        End Try
        Return bln
    End Function

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Shared Function ByteImageUpload(ByVal imgbyte As Byte(), ByVal strFolderName As String, ByVal strfilename As String, ByRef strErrorMessage As String) As Boolean
    Public Shared Function ByteImageUpload(ByVal imgbyte As Byte(), ByVal strFolderName As String, ByVal strfilename As String, ByRef strErrorMessage As String, ByVal strArutiSelfServiceURL As String) As Boolean
        'Shani(24-Aug-2015) -- End
        Try

            Dim HttpBinding As New System.ServiceModel.BasicHttpBinding
            With HttpBinding
                .MaxBufferSize = 2147483647
                'Sohail (04 Jul 2019) -- Start
                'PACT Enhancement - Support Issue Id # 3955 - 76.1 - The remote server returned an unexpected response: (413) Request Entity Too Large..
                .MaxBufferPoolSize = 2147483647
                .ReaderQuotas.MaxDepth = 2000000
                'Sohail (04 Jul 2019) -- End
                .MaxReceivedMessageSize = 2147483647
                .ReaderQuotas.MaxStringContentLength = 2147483647
                .ReaderQuotas.MaxArrayLength = 2147483647
                .ReaderQuotas.MaxBytesPerRead = 2147483647
                .ReaderQuotas.MaxNameTableCharCount = 2147483647
                .OpenTimeout = New TimeSpan(0, 20, 0)
                .ReceiveTimeout = New TimeSpan(0, 20, 0)
                .SendTimeout = New TimeSpan(0, 20, 0)
                'Sohail (05 Jul 2019) -- Start
                'NMB Issue - Support Issue Id # - 76.1 - The provided URI scheme 'https' is invalid; expected 'http'. Parameter name: via.
                '.Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport
                If strArutiSelfServiceURL.ToLower.Contains("https:") = True Then
                    .Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport
                Else
                    .Security.Mode = ServiceModel.BasicHttpSecurityMode.None
                    .Security.Transport.ClientCredentialType = ServiceModel.HttpClientCredentialType.None
                End If
                .MessageEncoding = ServiceModel.WSMessageEncoding.Text '[CCBRT Error: The content type text/html of the response message does not match the content type of the binding]
                'Sohail (05 Jul 2019) -- End
            End With

            Dim EndPointAdd As System.ServiceModel.EndpointAddress

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim strServicePath As String = ConfigParameter._Object._ArutiSelfServiceURL
            'If Strings.Right(strServicePath, 1) <> "/" OrElse Strings.Right(strServicePath, 1) <> "\" Then
            '    strServicePath += "/"
            'End If
            'strServicePath += "ArutiFileTransferService.asmx"

            'EndPointAdd = New System.ServiceModel.EndpointAddress(New Uri(strServicePath))

            If Strings.Right(strArutiSelfServiceURL, 1) <> "/" OrElse Strings.Right(strArutiSelfServiceURL, 1) <> "\" Then
                strArutiSelfServiceURL += "/"
            End If
            strArutiSelfServiceURL += "ArutiFileTransferService.asmx"

            EndPointAdd = New System.ServiceModel.EndpointAddress(New Uri(strArutiSelfServiceURL))

            'Shani(24-Aug-2015) -- End


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'Dim SSRef As New ArutiFileTransferServiceSoapClient(HttpBinding, EndPointAdd)
            'Dim UserCred As New UserCredentials

            Dim SSRef As New SSRef.ArutiFileTransferService
            Dim UserCred As New SSRef.UserCredentials
            SSRef.Url = strArutiSelfServiceURL
            SSRef.Timeout = 1200000 '20mins timeout

            'Gajanan [23-SEP-2019] -- End


            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-191  : Pick data from employee bio-data for internal applicant in Applicant CV Report.
            System.Net.ServicePointManager.Expect100Continue = False 'Sohail (25 Oct 2016) - [Issue : The remote server returned an error: (417) Expectation Failed]
            Try
                System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
            Catch ex As Exception

            End Try
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications
            'Sohail (20 Oct 2020) -- End

            UserCred.Password = clsSecurity.Decrypt("Qw0hgX7HJ0cge3U8zJbx44dZa+ZiNCzTLLNaxe5cIlv5vcoS7F/wiymPNWvVT+YK", "ezee")

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            SSRef.UserCredentialsValue = UserCred
            'Gajanan [23-SEP-2019] -- End

            strErrorMessage = ""

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'If SSRef.UploadFile(UserCred, imgbyte, strFolderName, strfilename, strErrorMessage) = False Then
            If SSRef.UploadFile(imgbyte, strFolderName, strfilename, strErrorMessage) = False Then
                'Gajanan [23-SEP-2019] -- End
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            If ex.Message.Contains("TCP error code 10060") Then
                strErrorMessage = "Aruti Self Servivce not found"
            Else
                strErrorMessage = ex.Message & " Error In ByteImageUpload"
            End If
            Return False
        End Try
    End Function

End Class

﻿'************************************************************************************************************************************
'Class Name : clsView_AT_Logs.vb
'Purpose    :
'Date       :20-Jul-2012
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsView_AT_Logs
    Private Shared ReadOnly mstrModuleName As String = "clsView_AT_Logs"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " ENUMS "

    Public Enum enAT_View_Mode
        APPLICANT_EVENT_LOG = 1
        USER_AUTHENTICATION_LOG = 2
        USER_ATTEMPTS_LOG = 3
        'S.SANDEEP [ 28 JAN 2014 ] -- START
        EMAIL_ATVIEW_LOG = 4
        'S.SANDEEP [ 28 JAN 2014 ] -- END

        'S.SANDEEP [ 17 OCT 2014 ] -- START
        ACTIVITY_DELETE_LOG = 5
        'S.SANDEEP [ 17 OCT 2014 ] -- END

    End Enum

    Public Enum enAT_OperationId
        SEARCH = 1
        EXPORT = 2
        'S.SANDEEP [ 28 JAN 2014 ] -- START
        DELETE = 3
        'S.SANDEEP [ 28 JAN 2014 ] -- END
    End Enum

#End Region

#Region " Private variables "

    Private mintAtviewunkid As Integer
    Private mintUserunkid As Integer
    Private mdtViewdatetime As Date
    Private mintAtviewmodeid As Integer
    Private mobjValue_View As String
    Private mintViewoperationid As Integer

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set atviewunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Atviewunkid() As Integer
        Get
            Return mintAtviewunkid
        End Get
        Set(ByVal value As Integer)
            mintAtviewunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set viewdatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Viewdatetime() As Date
        Get
            Return mdtViewdatetime
        End Get
        Set(ByVal value As Date)
            mdtViewdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set atviewmodeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Atviewmodeid() As Integer
        Get
            Return mintAtviewmodeid
        End Get
        Set(ByVal value As Integer)
            mintAtviewmodeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set value_view
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Value_View() As String
        Get
            Return mobjValue_View
        End Get
        Set(ByVal value As String)
            mobjValue_View = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set viewoperationid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Viewoperationid() As Integer
        Get
            Return mintViewoperationid
        End Get
        Set(ByVal value As Integer)
            mintViewoperationid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function GetList(ByVal strTableName As String, ByVal intAT_View_Mode As Integer) As DataSet
    Public Function GetList(ByVal strTableName As String, ByVal intAT_View_Mode As Integer, ByVal intLanguageunkid As Integer) As DataSet
        'Shani(24-Aug-2015) -- End

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Select Case intAT_View_Mode
                Case enAT_View_Mode.APPLICANT_EVENT_LOG
                    'S.SHARMA [ 22 JAN 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'StrQ = "SELECT " & _
                    '       " ISNULL(hrmsConfiguration..cfuser_master.username, '') AS 'User' " & _
                    '       ",CONVERT(CHAR(8),viewdatetime,112) AS 'View Date' " & _
                    '       ",ISNULL(CASE WHEN value_view.value('(frmATLogView_New/cboViewBy)[1]','Int') = 1 THEN @Desktop " & _
                    '       "             WHEN value_view.value('(frmATLogView_New/cboViewBy)[1]','Int') = 2 THEN @ESS " & _
                    '       "             WHEN value_view.value('(frmATLogView_New/cboViewBy)[1]','Int') = 3 THEN @MSS " & _
                    '       " END,'') AS 'Operation Mode' " & _
                    '       ",CASE WHEN @LangId = 0 THEN REPLACE(cflanguage.LANGUAGE,'&','') " & _
                    '       "      WHEN @LangId = 1 THEN REPLACE(cflanguage.language1,'&','') " & _
                    '       "      WHEN @LangId = 2 THEN REPLACE(cflanguage.language2,'&','') " & _
                    '       " END AS 'Master Detail' " & _
                    '       ",ISNULL(value_view.value('(frmATLogView_New/cboChildData)[1]','NVARCHAR(MAX)'),'') AS 'Transaction Detail' " & _
                    '       ",CASE WHEN value_view.value('(frmATLogView_New/cboEventType)[1]','Int') = 1 THEN @Add " & _
                    '       "      WHEN value_view.value('(frmATLogView_New/cboEventType)[1]','Int') = 2 THEN @Edit " & _
                    '       "      WHEN value_view.value('(frmATLogView_New/cboEventType)[1]','Int') = 3 THEN @Delete " & _
                    '       " ELSE '' END AS 'Event Type' " & _
                    '       ",ISNULL(CASE WHEN value_view.value('(frmATLogView_New/cboViewBy)[1]','Int') IN (1,3) THEN 'User : '+ " & _
                    '       "                ISNULL(VUF.username,'') " & _
                    '       "             WHEN value_view.value('(frmATLogView_New/cboViewBy)[1]','Int') IN (2) THEN 'Employee : '+ " & _
                    '       "                ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
                    '       " END,'') AS 'Search For' " & _
                    '       ",ISNULL(CONVERT(CHAR(8),value_view.value('(frmATLogView_New/dtpFromDate)[1]','Datetime'),112),'') AS 'From Date' " & _
                    '       ",ISNULL(CONVERT(CHAR(8),value_view.value('(frmATLogView_New/dtpTodate)[1]','Datetime'),112),'') AS 'To Date' " & _
                    '       ",ISNULL(value_view.value('(frmATLogView_New/txtModuleName)[1]','NVARCHAR(MAX)'),'') AS 'Module Name' " & _
                    '       ",ISNULL(value_view.value('(frmATLogView_New/txtMachine)[1]','NVARCHAR(MAX)'),'') AS 'Machine Name' " & _
                    '       ",ISNULL(value_view.value('(frmATLogView_New/txtIPAddress)[1]','NVARCHAR(MAX)'),'') AS 'IP Address' " & _
                    '       ",CASE WHEN viewoperationid = 1 THEN @SEARCH " & _
                    '       "      WHEN viewoperationid = 2 THEN @EXPORT END AS 'Operation' " & _
                    '       ",trnviewat_log.viewoperationid " & _
                    '       "FROM trnviewat_log " & _
                    '       "JOIN cflanguage ON cflanguage.controlname = value_view.value('(frmATLogView_New/lvSummary)[1]','NVARCHAR(MAX)') " & _
                    '       "LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = trnviewat_log.userunkid " & _
                    '       "LEFT JOIN hrmsConfiguration..cfuser_master AS VUF ON VUF.userunkid = value_view.value('(frmATLogView_New/cboFilter)[1]','Int') " & _
                    '       "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = value_view.value('(frmATLogView_New/cboFilter)[1]','Int')"

                    StrQ = "SELECT " & _
                           " ISNULL(hrmsConfiguration..cfuser_master.username, '') AS 'User' " & _
                           ",CONVERT(CHAR(8),viewdatetime,112) AS 'View Date' " & _
                           ",ISNULL(CASE WHEN value_view.value('(frmATLogView_New/cboViewBy)[1]','Int') = 1 THEN @Desktop " & _
                           "             WHEN value_view.value('(frmATLogView_New/cboViewBy)[1]','Int') = 2 THEN @ESS " & _
                           "             WHEN value_view.value('(frmATLogView_New/cboViewBy)[1]','Int') = 3 THEN @MSS " & _
                           " END,'') AS 'Operation Mode' " & _
                           ",CASE WHEN @LangId = 0 THEN REPLACE(cflanguage.LANGUAGE,'&','') " & _
                           "      WHEN @LangId = 1 THEN REPLACE(cflanguage.language1,'&','') " & _
                           "      WHEN @LangId = 2 THEN REPLACE(cflanguage.language2,'&','') " & _
                           " END AS 'Master Detail' " & _
                           ",ISNULL(value_view.value('(frmATLogView_New/cboChildData)[1]','NVARCHAR(MAX)'),'') AS 'Transaction Detail' " & _
                           ",CASE WHEN value_view.value('(frmATLogView_New/cboEventType)[1]','Int') = 1 THEN @Add " & _
                           "      WHEN value_view.value('(frmATLogView_New/cboEventType)[1]','Int') = 2 THEN @Edit " & _
                           "      WHEN value_view.value('(frmATLogView_New/cboEventType)[1]','Int') = 3 THEN @Delete " & _
                           " ELSE '' END AS 'Event Type' "
                    Select Case gApplicationType
                        Case enArutiApplicatinType.Aruti_Payroll
                            StrQ &= ",ISNULL(CASE WHEN value_view.value('(frmATLogView_New/cboViewBy)[1]','Int') IN (1,3) THEN 'User : '+ " & _
                           "                ISNULL(VUF.username,'') " & _
                           "             WHEN value_view.value('(frmATLogView_New/cboViewBy)[1]','Int') IN (2) THEN 'Employee : '+ " & _
                           "                ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
                                    " END,'') AS 'Search For' "
                        Case enArutiApplicatinType.Aruti_Configuration
                            StrQ &= ",ISNULL(CASE WHEN value_view.value('(frmATLogView_New/cboViewBy)[1]','Int') IN (1,3) THEN 'User : '+ " & _
                                    "                ISNULL(VUF.username,'') " & _
                                    "             END,'') AS 'Search For' "
                    End Select
                    StrQ &= ",ISNULL(CONVERT(CHAR(8),value_view.value('(frmATLogView_New/dtpFromDate)[1]','Datetime'),112),'') AS 'From Date' " & _
                           ",ISNULL(CONVERT(CHAR(8),value_view.value('(frmATLogView_New/dtpTodate)[1]','Datetime'),112),'') AS 'To Date' " & _
                           ",ISNULL(value_view.value('(frmATLogView_New/txtModuleName)[1]','NVARCHAR(MAX)'),'') AS 'Module Name' " & _
                           ",ISNULL(value_view.value('(frmATLogView_New/txtMachine)[1]','NVARCHAR(MAX)'),'') AS 'Machine Name' " & _
                           ",ISNULL(value_view.value('(frmATLogView_New/txtIPAddress)[1]','NVARCHAR(MAX)'),'') AS 'IP Address' " & _
                           ",CASE WHEN viewoperationid = 1 THEN @SEARCH " & _
                           "      WHEN viewoperationid = 2 THEN @EXPORT END AS 'Operation' " & _
                           ",trnviewat_log.viewoperationid " & _
                           "FROM trnviewat_log " & _
                           "JOIN cflanguage ON cflanguage.controlname = value_view.value('(frmATLogView_New/lvSummary)[1]','NVARCHAR(MAX)') " & _
                           "LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = trnviewat_log.userunkid " & _
                            "LEFT JOIN hrmsConfiguration..cfuser_master AS VUF ON VUF.userunkid = value_view.value('(frmATLogView_New/cboFilter)[1]','Int') "
                    Select Case gApplicationType
                        Case enArutiApplicatinType.Aruti_Payroll
                            StrQ &= "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = value_view.value('(frmATLogView_New/cboFilter)[1]','Int')"                        
                    End Select
                    'S.SHARMA [ 22 JAN 2013 ] -- END

                    objDataOperation.AddParameter("@Add", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 255, "Add"))
                    objDataOperation.AddParameter("@Edit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 256, "Edit"))
                    objDataOperation.AddParameter("@Delete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 257, "Delete"))

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objDataOperation.AddParameter("@LangId", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Languageunkid)
                    objDataOperation.AddParameter("@LangId", SqlDbType.Int, eZeeDataType.INT_SIZE, intLanguageunkid)
                    'Shani(24-Aug-2015) -- End

                    objDataOperation.AddParameter("@Desktop", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Desktop"))
                    objDataOperation.AddParameter("@MSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Manager Self Service"))
                    objDataOperation.AddParameter("@ESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Employee Self Service"))

                Case enAT_View_Mode.USER_AUTHENTICATION_LOG
                    'S.SHARMA [ 22 JAN 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'StrQ = "SELECT " & _
                    '       " ISNULL(hrmsConfiguration..cfuser_master.username, '') AS 'User' " & _
                    '       ",CONVERT(CHAR(8),viewdatetime,112) AS 'View Date' " & _
                    '       ",value_view.value('(frmUserLogList/dtpLoginFromDate)[1]','CHAR(8)') AS 'Login From Date' " & _
                    '       ",value_view.value('(frmUserLogList/dtpLoginToDate)[1]','CHAR(8)') AS 'Login To Date' " & _
                    '       ",value_view.value('(frmUserLogList/dtpLogoutFromDate)[1]','CHAR(8)') AS 'Logout From Date' " & _
                    '       ",value_view.value('(frmUserLogList/dtpLogoutToDate)[1]','CHAR(8)') AS 'Logout To Date' " & _
                    '       ",CASE WHEN value_view.value('(frmUserLogList/cboGrouping)[1]','Int') = 0 THEN @UNAME " & _
                    '       "      WHEN value_view.value('(frmUserLogList/cboGrouping)[1]','Int') = 1 THEN @LOGIN_DATE " & _
                    '       "      WHEN value_view.value('(frmUserLogList/cboGrouping)[1]','Int') = 2 THEN @LOGOUT_DATE " & _
                    '       "      WHEN value_view.value('(frmUserLogList/cboGrouping)[1]','Int') = 3 THEN @LOGIN_FROM END AS 'Grouping' " & _
                    '       ",CASE WHEN value_view.value('(frmUserLogList/radDescending)[1]','NVARCHAR(20)') <> '' THEN @DESC " & _
                    '       "      WHEN value_view.value('(frmUserLogList/radAscending)[1]','NVARCHAR(20)') <> '' THEN @ASC  END AS 'Sort By' " & _
                    '       ",CASE WHEN value_view.value('(frmUserLogList/cboViewBy)[1]','Int') = 1 THEN @DESKTOP " & _
                    '       "      WHEN value_view.value('(frmUserLogList/cboViewBy)[1]','Int') = 2 THEN @ESS " & _
                    '       "      WHEN value_view.value('(frmUserLogList/cboViewBy)[1]','Int') = 3 THEN @MSS END AS 'Login From' " & _
                    '       ",CASE WHEN value_view.value('(frmUserLogList/cboViewBy)[1]','Int') = 2 THEN " & _
                    '       "        ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
                    '       "      WHEN value_view.value('(frmUserLogList/cboViewBy)[1]','Int') <> 2 THEN " & _
                    '       "        ISNULL(VUF.username, '') END AS 'Search For' " & _
                    '       ",CASE WHEN viewoperationid = 1 THEN @SEARCH " & _
                    '       "      WHEN viewoperationid = 2 THEN @EXPORT END AS 'Operation' " & _
                    '       ",trnviewat_log.viewoperationid " & _
                    '       "FROM trnviewat_log " & _
                    '       "    LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = trnviewat_log.userunkid " & _
                    '       "    LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = value_view.value('(frmUserLogList/cboFilter)[1]','Int') " & _
                    '       "    LEFT JOIN hrmsConfiguration..cfuser_master AS VUF ON VUF.userunkid = value_view.value('(frmUserLogList/cboFilter)[1]','Int') "

                    StrQ = "SELECT " & _
                             " ISNULL(hrmsConfiguration..cfuser_master.username, '') AS 'User' " & _
                             ",CONVERT(CHAR(8),viewdatetime,112) AS 'View Date' " & _
                             ",value_view.value('(frmUserLogList/dtpLoginFromDate)[1]','CHAR(8)') AS 'Login From Date' " & _
                             ",value_view.value('(frmUserLogList/dtpLoginToDate)[1]','CHAR(8)') AS 'Login To Date' " & _
                             ",value_view.value('(frmUserLogList/dtpLogoutFromDate)[1]','CHAR(8)') AS 'Logout From Date' " & _
                             ",value_view.value('(frmUserLogList/dtpLogoutToDate)[1]','CHAR(8)') AS 'Logout To Date' " & _
                             ",CASE WHEN value_view.value('(frmUserLogList/cboGrouping)[1]','Int') = 0 THEN @UNAME " & _
                                    "WHEN value_view.value('(frmUserLogList/cboGrouping)[1]','Int') = 1 THEN @LOGIN_DATE " & _
                                    "WHEN value_view.value('(frmUserLogList/cboGrouping)[1]','Int') = 2 THEN @LOGOUT_DATE " & _
                                    "WHEN value_view.value('(frmUserLogList/cboGrouping)[1]','Int') = 3 THEN @LOGIN_FROM END AS 'Grouping' " & _
                             ",CASE WHEN value_view.value('(frmUserLogList/radDescending)[1]','NVARCHAR(20)') <> '' THEN @DESC " & _
                                    "WHEN value_view.value('(frmUserLogList/radAscending)[1]','NVARCHAR(20)') <> '' THEN @ASC  END AS 'Sort By' " & _
                             ",CASE WHEN value_view.value('(frmUserLogList/cboViewBy)[1]','Int') = 1 THEN @DESKTOP " & _
                                    "WHEN value_view.value('(frmUserLogList/cboViewBy)[1]','Int') = 2 THEN @ESS " & _
                           "      WHEN value_view.value('(frmUserLogList/cboViewBy)[1]','Int') = 3 THEN @MSS END AS 'Login From' "
                    Select Case gApplicationType
                        Case enArutiApplicatinType.Aruti_Payroll
                            StrQ &= ",CASE WHEN value_view.value('(frmUserLogList/cboViewBy)[1]','Int') = 2 THEN " & _
                                       "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
                                    "WHEN value_view.value('(frmUserLogList/cboViewBy)[1]','Int') <> 2 THEN " & _
                                    "        ISNULL(VUF.username, '') END AS 'Search For' "
                        Case enArutiApplicatinType.Aruti_Configuration
                            StrQ &= ",CASE WHEN value_view.value('(frmUserLogList/cboViewBy)[1]','Int') <> 2 THEN " & _
                                    "        ISNULL(VUF.username, '') END AS 'Search For' "
                    End Select
                    StrQ &= ",CASE WHEN viewoperationid = 1 THEN @SEARCH " & _
                                    "WHEN viewoperationid = 2 THEN @EXPORT END AS 'Operation' " & _
                             ",trnviewat_log.viewoperationid " & _
                        "FROM trnviewat_log " & _
                            "    LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = trnviewat_log.userunkid "
                    Select Case gApplicationType
                        Case enArutiApplicatinType.Aruti_Payroll
                            StrQ &= "  LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = value_view.value('(frmUserLogList/cboFilter)[1]','Int') "
                    End Select
                    StrQ &= "    LEFT JOIN hrmsConfiguration..cfuser_master AS VUF ON VUF.userunkid = value_view.value('(frmUserLogList/cboFilter)[1]','Int') "
                    'S.SHARMA [ 22 JAN 2013 ] -- END

                    objDataOperation.AddParameter("@DESKTOP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserLogList", 105, "Desktop"))
                    objDataOperation.AddParameter("@ESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserLogList", 106, "Employee Self Service"))
                    objDataOperation.AddParameter("@MSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserLogList", 107, "Manager Self Service"))

                    objDataOperation.AddParameter("@UNAME", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserLogList", 101, "User Name"))
                    objDataOperation.AddParameter("@LOGIN_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserLogList", 103, "Login Date"))
                    objDataOperation.AddParameter("@LOGOUT_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserLogList", 104, "Logout Date"))
                    objDataOperation.AddParameter("@LOGIN_FROM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserLogList", 108, "Login From"))
                    objDataOperation.AddParameter("@ASC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Ascending"))
                    objDataOperation.AddParameter("@DESC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Descending"))

                Case enAT_View_Mode.USER_ATTEMPTS_LOG
                    'S.SHARMA [ 22 JAN 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    StrQ = "SELECT " & _
                                 " ISNULL(hrmsConfiguration..cfuser_master.username, '') AS 'User' " & _
                                 ",CONVERT(CHAR(8), viewdatetime, 112) AS 'View Date' " & _
                                 ",ISNULL(CASE WHEN value_view.value('(frmUserAttemptsLog/cboAttemptType)[1]', 'Int') = 1 THEN @Successful " & _
                                                 "WHEN value_view.value('(frmUserAttemptsLog/cboAttemptType)[1]', 'Int') = 1 THEN @Unsuccessful END,'') AS 'Attempt Type' " & _
                                 ",ISNULL(CASE WHEN value_view.value('(frmUserAttemptsLog/cboViewBy)[1]', 'Int') = 1 THEN @DESKTOP " & _
                                                 "WHEN value_view.value('(frmUserAttemptsLog/cboViewBy)[1]', 'Int') = 2 THEN @ESS " & _
                           "             WHEN value_view.value('(frmUserAttemptsLog/cboViewBy)[1]', 'Int') = 3 THEN @MSS END,'') AS 'View By'"
                    Select Case gApplicationType
                        Case enArutiApplicatinType.Aruti_Payroll
                            StrQ &= ",CASE WHEN value_view.value('(frmUserAttemptsLog/cboViewBy)[1]', 'Int') = 2 THEN " & _
                                           "ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
                                    "      WHEN value_view.value('(frmUserAttemptsLog/cboViewBy)[1]', 'Int') <> 2 THEN ISNULL(VUF.username, '') END AS 'Search For' "
                        Case enArutiApplicatinType.Aruti_Configuration
                            StrQ &= ",CASE WHEN value_view.value('(frmUserAttemptsLog/cboViewBy)[1]', 'Int') <> 2 THEN ISNULL(VUF.username, '') END AS 'Search For' "
                    End Select
                    StrQ &= ",CASE WHEN viewoperationid = 1 THEN @SEARCH " & _
                                        "WHEN viewoperationid = 2 THEN @EXPORT END AS 'Operation' " & _
                                 ",trnviewat_log.viewoperationid " & _
                            "FROM trnviewat_log " & _
                            "LEFT JOIN hrmsConfiguration..cfuser_master ON trnviewat_log.userunkid = hrmsConfiguration..cfuser_master.userunkid "
                    Select Case gApplicationType
                        Case enArutiApplicatinType.Aruti_Payroll
                            StrQ &= "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = value_view.value('(frmUserAttemptsLog/cboFilter)[1]','Int') "
                    End Select
                    StrQ &= "LEFT JOIN hrmsConfiguration..cfuser_master AS VUF ON VUF.userunkid = value_view.value('(frmUserAttemptsLog/cboFilter)[1]','Int') "
                    'S.SHARMA [ 22 JAN 2013 ] -- END
                    objDataOperation.AddParameter("@DESKTOP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 4, "Desktop"))
                    objDataOperation.AddParameter("@ESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 5, "Employee Self Service"))
                    objDataOperation.AddParameter("@MSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 6, "Manager Self Service"))

                    objDataOperation.AddParameter("@Successful", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 2, "Successful"))
                    objDataOperation.AddParameter("@Unsuccessful", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 3, "Unsuccessful"))

                    'S.SANDEEP [ 28 JAN 2014 ] -- START
                Case clsView_AT_Logs.enAT_View_Mode.EMAIL_ATVIEW_LOG
                    StrQ = "SELECT " & _
                           " ISNULL(hrmsConfiguration..cfuser_master.username, '') AS 'User' " & _
                           ",CASE WHEN value_view.value('(frmATEmail_LogView/cboViewType)[1]', 'Int') = 1 THEN @ASSESSMENT_MGT " & _
                           "      WHEN value_view.value('(frmATEmail_LogView/cboViewType)[1]', 'Int') = 2 THEN @LEAVE_MGT " & _
                           "      WHEN value_view.value('(frmATEmail_LogView/cboViewType)[1]', 'Int') = 3 THEN @EMPLOYEE_MGT " & _
                           "      WHEN value_view.value('(frmATEmail_LogView/cboViewType)[1]', 'Int') = 4 THEN @PAYROLL_MGT " & _
                           "      WHEN value_view.value('(frmATEmail_LogView/cboViewType)[1]', 'Int') = 5 THEN @RECRUITMENT_MGT END AS 'View Type' " & _
                           ",CONVERT(CHAR(8),viewdatetime,112) AS 'View Date' " & _
                           ",ISNULL(CASE WHEN value_view.value('(frmATEmail_LogView/cboViewBy)[1]', 'Int') = 1 THEN @DESKTOP " & _
                           "             WHEN value_view.value('(frmATEmail_LogView/cboViewBy)[1]', 'Int') = 2 THEN @ESS " & _
                           "             WHEN value_view.value('(frmATEmail_LogView/cboViewBy)[1]', 'Int') = 3 THEN @MSS END,'') AS 'View By' " & _
                           ",value_view.value('(frmATEmail_LogView/dtpFromDate)[1]', 'CHAR(8)') AS iFDate " & _
                           ",value_view.value('(frmATEmail_LogView/dtpTodate)[1]', 'CHAR(8)') AS iTDate " & _
                           ",'' AS 'Date Range' "
                    Select Case gApplicationType
                        Case enArutiApplicatinType.Aruti_Payroll
                            StrQ &= ",CASE WHEN value_view.value('(frmATEmail_LogView/cboViewBy)[1]', 'Int') = 2 THEN " & _
                                    "           ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
                                    "      WHEN value_view.value('(frmATEmail_LogView/cboViewBy)[1]', 'Int') <> 2 THEN ISNULL(VUF.username, '') END AS 'Search For' "
                        Case enArutiApplicatinType.Aruti_Configuration
                            StrQ &= ",CASE WHEN value_view.value('(frmATEmail_LogView/cboViewBy)[1]', 'Int') <> 2 THEN ISNULL(VUF.username, '') END AS 'Search For' "
                    End Select
                    StrQ &= ",CASE WHEN viewoperationid = 1 THEN @SEARCH " & _
                            "      WHEN viewoperationid = 2 THEN @EXPORT " & _
                            "      WHEN viewoperationid = 3 THEN @DELETE END AS 'Operation' " & _
                            ",trnviewat_log.viewoperationid " & _
                            "FROM trnviewat_log " & _
                            "   LEFT JOIN hrmsConfiguration..cfuser_master ON trnviewat_log.userunkid = hrmsConfiguration..cfuser_master.userunkid "
                    Select Case gApplicationType
                        Case enArutiApplicatinType.Aruti_Payroll
                            StrQ &= "   LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = value_view.value('(frmATEmail_LogView/cboFilter)[1]','Int') "
                    End Select
                    StrQ &= "   LEFT JOIN hrmsConfiguration..cfuser_master AS VUF ON VUF.userunkid = value_view.value('(frmATEmail_LogView/cboFilter)[1]','Int') "

                    objDataOperation.AddParameter("@DESKTOP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 4, "Desktop"))
                    objDataOperation.AddParameter("@ESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 5, "Employee Self Service"))
                    objDataOperation.AddParameter("@MSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 6, "Manager Self Service"))

                    objDataOperation.AddParameter("@ASSESSMENT_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 3, "Employee Assessment"))
                    objDataOperation.AddParameter("@LEAVE_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 4, "Leave Managment"))
                    objDataOperation.AddParameter("@EMPLOYEE_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 5, "Employee Managment"))
                    objDataOperation.AddParameter("@PAYROLL_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 6, "Payroll Managment"))
                    objDataOperation.AddParameter("@RECRUITMENT_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmUserAttemptsLog", 7, "Recruitment Managment"))
                    objDataOperation.AddParameter("@DELETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Delete"))
                    'S.SANDEEP [ 28 JAN 2014 ] -- END

                    'S.SANDEEP [ 17 OCT 2014 ] -- START
                Case clsView_AT_Logs.enAT_View_Mode.ACTIVITY_DELETE_LOG
                    StrQ = "SELECT " & _
                           "    CASE WHEN value_view.value('(frmPermanentlyDeletePPALogs/cboRemoveFrom)[1]', 'Int') = 0 THEN @PA " & _
                           "         WHEN value_view.value('(frmPermanentlyDeletePPALogs/cboRemoveFrom)[1]', 'Int') = 1 THEN @PR " & _
                           "    END AS 'View Type' " & _
                           "   ,CONVERT(CHAR(8),viewdatetime,112) AS 'Delete Date' " & _
                           "   ,value_view.value('(frmPermanentlyDeletePPALogs/dtpFromDate)[1]', 'CHAR(8)') AS iFDate " & _
                           "   ,value_view.value('(frmPermanentlyDeletePPALogs/dtpTodate)[1]', 'CHAR(8)') AS iTDate " & _
                           "   ,'' AS 'Date Range' " & _
                           "   ,CASE WHEN viewoperationid = 1 THEN @SEARCH " & _
                           "         WHEN viewoperationid = 2 THEN @EXPORT " & _
                           "         WHEN viewoperationid = 3 THEN @DELETE END AS 'Operation' " & _
                           "    ,trnviewat_log.viewoperationid " & _
                           "FROM trnviewat_log " & _
                           "   LEFT JOIN hrmsConfiguration..cfuser_master ON trnviewat_log.userunkid = hrmsConfiguration..cfuser_master.userunkid "

                    objDataOperation.AddParameter("@DELETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Delete"))
                    objDataOperation.AddParameter("@PA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmPermanentlyDeletePPALogs", 1, "Activity Postings"))
                    objDataOperation.AddParameter("@PR", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmPermanentlyDeletePPALogs", 2, "Activiy Rates"))
                    'S.SANDEEP [ 17 OCT 2014 ] -- END
            End Select

            If StrQ.Trim.Length > 0 Then
                StrQ &= "WHERE trnviewat_log.atviewmodeid = " & intAT_View_Mode
            End If

            objDataOperation.AddParameter("@SEARCH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Search"))
            objDataOperation.AddParameter("@EXPORT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Export"))

            If StrQ.Length > 0 Then
                dsList = objDataOperation.ExecQuery(StrQ, strTableName)
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Select Case intAT_View_Mode
                Case enAT_View_Mode.APPLICANT_EVENT_LOG
                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        If dRow.Item("View Date").ToString.Trim <> "" Then
                            dRow.Item("View Date") = eZeeDate.convertDate(dRow.Item("View Date").ToString).ToShortDateString
                        Else
                            dRow.Item("View Date") = ""
                        End If

                        If dRow.Item("From Date").ToString.Trim <> "19000101" Then
                            dRow.Item("From Date") = eZeeDate.convertDate(dRow.Item("From Date").ToString).ToShortDateString
                        Else
                            dRow.Item("From Date") = ""
                        End If

                        If dRow.Item("To Date").ToString.Trim <> "19000101" Then
                            dRow.Item("To Date") = eZeeDate.convertDate(dRow.Item("To Date").ToString).ToShortDateString
                        Else
                            dRow.Item("To Date") = ""
                        End If
                    Next
                Case enAT_View_Mode.USER_AUTHENTICATION_LOG
                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        If dRow.Item("View Date").ToString.Trim <> "" Then
                            dRow.Item("View Date") = eZeeDate.convertDate(dRow.Item("View Date").ToString).ToShortDateString
                        Else
                            dRow.Item("View Date") = ""
                        End If

                        If dRow.Item("Login From Date").ToString.Trim <> "" Then
                            dRow.Item("Login From Date") = eZeeDate.convertDate(dRow.Item("Login From Date").ToString).ToShortDateString
                        Else
                            dRow.Item("Login From Date") = ""
                        End If

                        If dRow.Item("Login To Date").ToString.Trim <> "" Then
                            dRow.Item("Login To Date") = eZeeDate.convertDate(dRow.Item("Login To Date").ToString).ToShortDateString
                        Else
                            dRow.Item("Login To Date") = ""
                        End If

                        If dRow.Item("Logout From Date").ToString.Trim <> "" Then
                            dRow.Item("Logout From Date") = eZeeDate.convertDate(dRow.Item("Logout From Date").ToString).ToShortDateString
                        Else
                            dRow.Item("Logout From Date") = ""
                        End If

                        If dRow.Item("Logout To Date").ToString.Trim <> "" Then
                            dRow.Item("Logout To Date") = eZeeDate.convertDate(dRow.Item("Logout To Date").ToString).ToShortDateString
                        Else
                            dRow.Item("Logout To Date") = ""
                        End If
                    Next
                Case enAT_View_Mode.USER_ATTEMPTS_LOG
                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        If dRow.Item("View Date").ToString.Trim <> "" Then
                            dRow.Item("View Date") = eZeeDate.convertDate(dRow.Item("View Date").ToString).ToShortDateString
                        Else
                            dRow.Item("View Date") = ""
                        End If
                    Next
                    'S.SANDEEP [ 28 JAN 2014 ] -- START
                Case clsView_AT_Logs.enAT_View_Mode.EMAIL_ATVIEW_LOG
                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        If dRow.Item("View Date").ToString.Trim <> "" Then
                            dRow.Item("View Date") = eZeeDate.convertDate(dRow.Item("View Date").ToString).ToShortDateString
                        Else
                            dRow.Item("View Date") = ""
                        End If
                        If dRow.Item("iFDate").ToString.Trim <> "" AndAlso dRow.Item("iTDate").ToString.Trim <> "" Then
                            dRow.Item("Date Range") = eZeeDate.convertDate(dRow.Item("iFDate").ToString.Trim).ToShortDateString & " - " & eZeeDate.convertDate(dRow.Item("iTDate").ToString.Trim).ToShortDateString
                        End If
                    Next
                    dsList.Tables(0).Columns.Remove("iTDate") : dsList.Tables(0).Columns.Remove("iFDate")
                    'S.SANDEEP [ 28 JAN 2014 ] -- END

                    'S.SANDEEP [ 17 OCT 2014 ] -- START
                Case clsView_AT_Logs.enAT_View_Mode.ACTIVITY_DELETE_LOG
                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        If dRow.Item("Delete Date").ToString.Trim <> "" Then
                            dRow.Item("Delete Date") = eZeeDate.convertDate(dRow.Item("Delete Date").ToString).ToShortDateString
                        Else
                            dRow.Item("Delete Date") = ""
                        End If
                        If dRow.Item("iFDate").ToString.Trim <> "" AndAlso dRow.Item("iTDate").ToString.Trim <> "" Then
                            dRow.Item("Date Range") = eZeeDate.convertDate(dRow.Item("iFDate").ToString.Trim).ToShortDateString & " - " & eZeeDate.convertDate(dRow.Item("iTDate").ToString.Trim).ToShortDateString
                        End If
                    Next
                    dsList.Tables(0).Columns.Remove("iTDate") : dsList.Tables(0).Columns.Remove("iFDate")
                    'S.SANDEEP [ 17 OCT 2014 ] -- END
            End Select

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trnviewat_log) </purpose>
    Public Function Insert() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@viewdatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtviewdatetime.ToString)
            objDataOperation.AddParameter("@atviewmodeid", SqlDbType.int, eZeeDataType.INT_SIZE, mintatviewmodeid.ToString)
            objDataOperation.AddParameter("@value_view", SqlDbType.Xml, mobjValue_View.Length, mobjValue_View.ToString)
            objDataOperation.AddParameter("@viewoperationid", SqlDbType.int, eZeeDataType.INT_SIZE, mintviewoperationid.ToString)

            strQ = "INSERT INTO trnviewat_log ( " & _
                   "  userunkid " & _
                   ", viewdatetime " & _
                   ", atviewmodeid " & _
                   ", value_view " & _
                   ", viewoperationid" & _
                   ") VALUES (" & _
                   "  @userunkid " & _
                   ", @viewdatetime " & _
                   ", @atviewmodeid " & _
                   ", @value_view " & _
                   ", @viewoperationid" & _
                   "); SELECT @@identity "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAtviewUnkId = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Export")
			Language.setMessage(mstrModuleName, 2, "Ascending")
			Language.setMessage(mstrModuleName, 3, "Descending")
			Language.setMessage("frmUserAttemptsLog", 4, "Desktop")
			Language.setMessage(mstrModuleName, 5, "Desktop")
			Language.setMessage(mstrModuleName, 6, "Manager Self Service")
			Language.setMessage(mstrModuleName, 7, "Employee Self Service")
			Language.setMessage(mstrModuleName, 8, "Search")
			Language.setMessage("frmUserLogList", 101, "User Name")
			Language.setMessage("frmUserLogList", 103, "Login Date")
			Language.setMessage("frmUserLogList", 104, "Logout Date")
			Language.setMessage("frmUserLogList", 105, "Desktop")
			Language.setMessage("frmUserLogList", 106, "Employee Self Service")
			Language.setMessage("frmUserLogList", 107, "Manager Self Service")
			Language.setMessage("frmUserLogList", 108, "Login From")
			Language.setMessage("clsMasterData", 255, "Add")
			Language.setMessage("clsMasterData", 256, "Edit")
			Language.setMessage("clsMasterData", 257, "Delete")
			Language.setMessage("frmUserAttemptsLog", 2, "Successful")
			Language.setMessage("frmUserAttemptsLog", 3, "Unsuccessful")
			Language.setMessage("frmUserAttemptsLog", 5, "Employee Self Service")
			Language.setMessage("frmUserAttemptsLog", 6, "Manager Self Service")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

﻿'************************************************************************************************************************************
'Class Name : clsinstitute_master.vb
'Purpose    : All Institute Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :30/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsinstitute_master
    Private Shared ReadOnly mstrModuleName As String = "clsinstitute_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintInstituteunkid As Integer
    Private mstrInstitute_Code As String = String.Empty
    Private mstrInstitute_Name As String = String.Empty
    Private mstrInstitute_Address As String = String.Empty
    Private mstrTelephoneno As String = String.Empty
    Private mstrInstitute_Fax As String = String.Empty
    Private mstrInstitute_Email As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mstrContact_Person As String = String.Empty
    Private mintStateunkid As Integer
    Private mintCityunkid As Integer
    Private mintCountryunkid As Integer
    Private mblnIshospital As Boolean
    Private mblnIsactive As Boolean = True

    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes
    Private mblnIsFormRequire As Boolean = False
    'Pinkal (12-Oct-2011) -- End

    'S.SANDEEP [ 03 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIssync_Recruit As Boolean = False
    'S.SANDEEP [ 03 MAY 2012 ] -- END


    'Pinkal (18-Dec-2012) -- Start
    'Enhancement : TRA Changes
    Private mblnlocalProvider As Boolean = True
    'Pinkal (18-Dec-2012) -- End


#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set instituteunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Instituteunkid() As Integer
        Get
            Return mintInstituteunkid
        End Get
        Set(ByVal value As Integer)
            mintInstituteunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set institute_code
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Institute_Code() As String
        Get
            Return mstrInstitute_Code
        End Get
        Set(ByVal value As String)
            mstrInstitute_Code = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set institute_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Institute_Name() As String
        Get
            Return mstrInstitute_Name
        End Get
        Set(ByVal value As String)
            mstrInstitute_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set institute_address
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Institute_Address() As String
        Get
            Return mstrInstitute_Address
        End Get
        Set(ByVal value As String)
            mstrInstitute_Address = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set telephoneno
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Telephoneno() As String
        Get
            Return mstrTelephoneno
        End Get
        Set(ByVal value As String)
            mstrTelephoneno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set institute_fax
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Institute_Fax() As String
        Get
            Return mstrInstitute_Fax
        End Get
        Set(ByVal value As String)
            mstrInstitute_Fax = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set institute_email
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Institute_Email() As String
        Get
            Return mstrInstitute_Email
        End Get
        Set(ByVal value As String)
            mstrInstitute_Email = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contact_person
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Contact_Person() As String
        Get
            Return mstrContact_Person
        End Get
        Set(ByVal value As String)
            mstrContact_Person = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cityunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ishospital
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ishospital() As Boolean
        Get
            Return mblnIshospital
        End Get
        Set(ByVal value As Boolean)
            mblnIshospital = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set isformrequire
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFormRequire() As Boolean
        Get
            Return mblnIsFormRequire
        End Get
        Set(ByVal value As Boolean)
            mblnIsFormRequire = value
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- End


    'S.SANDEEP [ 03 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set issync_recruit
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Issync_Recruit() As Boolean
        Get
            Return mblnIssync_Recruit
        End Get
        Set(ByVal value As Boolean)
            mblnIssync_Recruit = Value
        End Set
    End Property

    'S.SANDEEP [ 03 MAY 2012 ] -- END


    'Pinkal (18-Dec-2012) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set islocalProvider
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsLocalProvider() As Boolean
        Get
            Return mblnlocalProvider
        End Get
        Set(ByVal value As Boolean)
            mblnlocalProvider = value
        End Set
    End Property

    'Pinkal (18-Dec-2012) -- End



#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'S.SANDEEP [ 03 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '  "  instituteunkid " & _
            '  ", institute_code " & _
            '  ", institute_name " & _
            '  ", institute_address " & _
            '  ", telephoneno " & _
            '  ", institute_fax " & _
            '  ", institute_email " & _
            '  ", description " & _
            '  ", contact_person " & _
            '  ", stateunkid " & _
            '  ", cityunkid " & _
            '  ", countryunkid " & _
            '  ", ishospital " & _
            '  ", isactive " & _
            ' ", isnull(isformrequire,0) isformrequire " & _
            ' "FROM hrinstitute_master " & _
            ' "WHERE instituteunkid = @instituteunkid "


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '  "  instituteunkid " & _
            '  ", institute_code " & _
            '  ", institute_name " & _
            '  ", institute_address " & _
            '  ", telephoneno " & _
            '  ", institute_fax " & _
            '  ", institute_email " & _
            '  ", description " & _
            '  ", contact_person " & _
            '  ", stateunkid " & _
            '  ", cityunkid " & _
            '  ", countryunkid " & _
            '  ", ishospital " & _
            '  ", isactive " & _
            ' ", isnull(isformrequire,0) isformrequire " & _
            '       ", ISNULL(issync_recruit,0) AS issync_recruit " & _
            ' "FROM hrinstitute_master " & _
            ' "WHERE instituteunkid = @instituteunkid "


            strQ = "SELECT " & _
              "  instituteunkid " & _
              ", institute_code " & _
              ", institute_name " & _
              ", institute_address " & _
              ", telephoneno " & _
              ", institute_fax " & _
              ", institute_email " & _
              ", description " & _
              ", contact_person " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", countryunkid " & _
              ", ishospital " & _
              ", isactive " & _
             ", isnull(isformrequire,0) isformrequire " & _
                   ", ISNULL(issync_recruit,0) AS issync_recruit " & _
                      ", ISNULL(islocalprovider,0) AS islocalprovider " & _
             "FROM hrinstitute_master " & _
             "WHERE instituteunkid = @instituteunkid "


            'Pinkal (18-Dec-2012) -- End


            'S.SANDEEP [ 03 MAY 2012 ] -- END


            

            objDataOperation.AddParameter("@instituteunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintInstituteUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintInstituteunkid = CInt(dtRow.Item("instituteunkid"))
                mstrInstitute_Code = dtRow.Item("institute_code").ToString
                mstrInstitute_Name = dtRow.Item("institute_name").ToString
                mstrInstitute_Address = dtRow.Item("institute_address").ToString
                mstrTelephoneno = dtRow.Item("telephoneno").ToString
                mstrInstitute_Fax = dtRow.Item("institute_fax").ToString
                mstrInstitute_Email = dtRow.Item("institute_email").ToString
                mstrDescription = dtRow.Item("description").ToString
                mstrContact_Person = dtRow.Item("contact_person").ToString
                mintStateunkid = CInt(dtRow.Item("stateunkid"))
                mintCityunkid = CInt(dtRow.Item("cityunkid"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mblnIshospital = CBool(dtRow.Item("ishospital"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes
                mblnIsFormRequire = CBool(dtRow.Item("isformrequire"))
                'Pinkal (12-Oct-2011) -- End

                'S.SANDEEP [ 03 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mblnIssync_Recruit = CBool(dtRow.Item("issync_recruit"))
                'S.SANDEEP [ 03 MAY 2012 ] -- END


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
                mblnlocalProvider = CBool(dtRow.Item("islocalprovider"))
                'Pinkal (18-Dec-2012) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal mbitIshospital As Boolean, Optional ByVal isForImport As Boolean = False, Optional ByVal mintActive As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            strQ = "SELECT " & _
              "  instituteunkid " & _
              ", institute_code " & _
              ", institute_name " & _
              ", institute_address " & _
              ", telephoneno " & _
              ", institute_fax " & _
              ", institute_email " & _
              ", description " & _
              ", contact_person " & _
              ", hrinstitute_master.stateunkid " & _
              ", hrinstitute_master.cityunkid " & _
              ", hrinstitute_master.countryunkid " & _
              ", ishospital " & _
              ", hrinstitute_master.isactive "


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            strQ &= " , isnull(hrinstitute_master.isformrequire,0) isformrequire "
            'Pinkal (12-Oct-2011) -- End


            'S.SANDEEP [ 03 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= " , ISNULL(hrinstitute_master.issync_recruit,0) issync_recruit "
            'S.SANDEEP [ 03 MAY 2012 ] -- END



            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            strQ &= " , ISNULL(hrinstitute_master.islocalprovider,0) islocalprovider "
            'Pinkal (18-Dec-2012) -- End



            If isForImport Then
                strQ &= ", hrmsConfiguration..cfcity_master.name as city " & _
                            ",  hrmsConfiguration..cfstate_master.name as state" & _
                            ", hrmsConfiguration..cfcountry_master.country_name as country "
            End If

            strQ &= " FROM hrinstitute_master "

            If isForImport Then
                strQ &= " LEFT JOIN hrmsConfiguration..cfcountry_master on hrmsConfiguration..cfcountry_master.countryunkid = hrinstitute_master.countryunkid " & _
                            " LEFT JOIN hrmsConfiguration..cfstate_master on hrmsConfiguration..cfstate_master.stateunkid = hrinstitute_master.stateunkid " & _
                            " LEFT JOIN hrmsConfiguration..cfcity_master on hrmsConfiguration..cfcity_master.cityunkid = hrinstitute_master.cityunkid  "
            End If


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If mbitIshospital Then
                strQ &= " LEFT JOIN mdprovider_mapping ON mdprovider_mapping.providerunkid  = hrinstitute_master.instituteunkid AND (mdprovider_mapping.userunkid = " & User._Object._Userunkid & " OR mdprovider_mapping.userunkid = 1) "
            End If

            'Pinkal (20-Jan-2012) -- End

            strQ &= "  WHERE ishospital = @ishospital"


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'If blnOnlyActive Then
            '    strQ &= " AND hrinstitute_master.isactive = 1 "
            'End If

            If mintActive = 1 Then
                strQ &= " AND hrinstitute_master.isactive = 1 "
            ElseIf mintActive = 2 Then
                strQ &= " AND hrinstitute_master.isactive = 0 "
            End If

            'Pinkal (20-Jan-2012) -- End


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            strQ &= " ORDER BY institute_name "
            'Pinkal (18-Dec-2012) -- End


            objDataOperation.AddParameter("@ishospital", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mbitIshospital.ToString())
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrinstitute_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mblnIshospital, mstrInstitute_Code) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Institute Code is already defined. Please define new Institute Code.")
            Return False
        ElseIf isExist(mblnIshospital, "", mstrInstitute_Name) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Institute Name is already defined. Please define new Institute Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@institute_code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrinstitute_code.ToString)
            objDataOperation.AddParameter("@institute_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrinstitute_name.ToString)
            objDataOperation.AddParameter("@institute_address", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrinstitute_address.ToString)
            objDataOperation.AddParameter("@telephoneno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrtelephoneno.ToString)
            objDataOperation.AddParameter("@institute_fax", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrinstitute_fax.ToString)
            objDataOperation.AddParameter("@institute_email", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrinstitute_email.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@contact_person", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontact_person.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcityunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@ishospital", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnishospital.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isformrequire", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFormRequire.ToString())

            'S.SANDEEP [ 03 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@issync_recruit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssync_Recruit.ToString)
            'S.SANDEEP [ 03 MAY 2012 ] -- END


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            objDataOperation.AddParameter("@islocalprovider", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnlocalProvider.ToString)

            'strQ = "INSERT INTO hrinstitute_master ( " & _
            '  "  institute_code " & _
            '  ", institute_name " & _
            '  ", institute_address " & _
            '  ", telephoneno " & _
            '  ", institute_fax " & _
            '  ", institute_email " & _
            '  ", description " & _
            '  ", contact_person " & _
            '  ", stateunkid " & _
            '  ", cityunkid " & _
            '  ", countryunkid " & _
            '  ", ishospital " & _
            '  ", isactive" & _
            '  ", isformrequire " & _
            '  ", issync_recruit " & _
            '") VALUES (" & _
            '  "  @institute_code " & _
            '  ", @institute_name " & _
            '  ", @institute_address " & _
            '  ", @telephoneno " & _
            '  ", @institute_fax " & _
            '  ", @institute_email " & _
            '  ", @description " & _
            '  ", @contact_person " & _
            '  ", @stateunkid " & _
            '  ", @cityunkid " & _
            '  ", @countryunkid " & _
            '  ", @ishospital " & _
            '  ", @isactive" & _
            '  ", @isformrequire " & _
            '  ", @issync_recruit " & _
            '"); SELECT @@identity"

            strQ = "INSERT INTO hrinstitute_master ( " & _
              "  institute_code " & _
              ", institute_name " & _
              ", institute_address " & _
              ", telephoneno " & _
              ", institute_fax " & _
              ", institute_email " & _
              ", description " & _
              ", contact_person " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", countryunkid " & _
              ", ishospital " & _
              ", isactive" & _
              ", isformrequire " & _
              ", issync_recruit " & _
                        ", islocalprovider " & _
            ") VALUES (" & _
              "  @institute_code " & _
              ", @institute_name " & _
              ", @institute_address " & _
              ", @telephoneno " & _
              ", @institute_fax " & _
              ", @institute_email " & _
              ", @description " & _
              ", @contact_person " & _
              ", @stateunkid " & _
              ", @cityunkid " & _
              ", @countryunkid " & _
              ", @ishospital " & _
              ", @isactive" & _
              ", @isformrequire " & _
              ", @issync_recruit " & _
                        ", @islocalprovider " & _
            "); SELECT @@identity"


            'Pinkal (18-Dec-2012) -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintInstituteunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrinstitute_master", "instituteunkid", mintInstituteunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrinstitute_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mblnIshospital, mstrInstitute_Code, "", mintInstituteunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Institute Code is already defined. Please define new Institute Code.")
            Return False
        ElseIf isExist(mblnIshospital, "", mstrInstitute_Name, mintInstituteunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Institute Name is already defined. Please define new Institute Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintinstituteunkid.ToString)
            objDataOperation.AddParameter("@institute_code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrinstitute_code.ToString)
            objDataOperation.AddParameter("@institute_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrinstitute_name.ToString)
            objDataOperation.AddParameter("@institute_address", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrinstitute_address.ToString)
            objDataOperation.AddParameter("@telephoneno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrtelephoneno.ToString)
            objDataOperation.AddParameter("@institute_fax", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrinstitute_fax.ToString)
            objDataOperation.AddParameter("@institute_email", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrinstitute_email.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@contact_person", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontact_person.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcityunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@ishospital", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnishospital.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isformrequire", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFormRequire.ToString)

            'S.SANDEEP [ 03 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@issync_recruit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssync_Recruit.ToString)
            'S.SANDEEP [ 03 MAY 2012 ] -- END


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            objDataOperation.AddParameter("@islocalprovider", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnlocalProvider.ToString)

            'strQ = "UPDATE hrinstitute_master SET " & _
            '             "  institute_code = @institute_code" & _
            '             ", institute_name = @institute_name" & _
            '             ", institute_address = @institute_address" & _
            '             ", telephoneno = @telephoneno" & _
            '             ", institute_fax = @institute_fax" & _
            '             ", institute_email = @institute_email" & _
            '             ", description = @description" & _
            '             ", contact_person = @contact_person" & _
            '             ", stateunkid = @stateunkid" & _
            '             ", cityunkid = @cityunkid" & _
            '             ", countryunkid = @countryunkid" & _
            '             ", ishospital = @ishospital" & _
            '             ", isactive = @isactive " & _
            '             ", isformrequire = @isformrequire " & _
            '             ", Syncdatetime  = NULL " & _
            '         ", issync_recruit = @issync_recruit " & _
            '           "WHERE instituteunkid = @instituteunkid "

            strQ = "UPDATE hrinstitute_master SET " & _
              "  institute_code = @institute_code" & _
              ", institute_name = @institute_name" & _
              ", institute_address = @institute_address" & _
              ", telephoneno = @telephoneno" & _
              ", institute_fax = @institute_fax" & _
              ", institute_email = @institute_email" & _
              ", description = @description" & _
              ", contact_person = @contact_person" & _
              ", stateunkid = @stateunkid" & _
              ", cityunkid = @cityunkid" & _
              ", countryunkid = @countryunkid" & _
              ", ishospital = @ishospital" & _
              ", isactive = @isactive " & _
          ", isformrequire = @isformrequire " & _
              ", Syncdatetime  = NULL " & _
                         ", issync_recruit = @issync_recruit " & _
                     ", islocalprovider = @islocalprovider " & _
                       "WHERE instituteunkid = @instituteunkid "

            'Pinkal (18-Dec-2012) -- End




            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrinstitute_master", mintInstituteunkid, "instituteunkid", 2) Then

                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrinstitute_master", "instituteunkid", mintInstituteunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrinstitute_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Institute. Reason: This Institute is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            'strQ = "DELETE FROM hrinstitute_master " & _
            '"WHERE instituteunkid = @instituteunkid "
            strQ = " UPDATE hrinstitute_master SET " & _
                        "  isactive = 0 " & _
                    "WHERE instituteunkid = @instituteunkid "
            
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrinstitute_master", "instituteunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False
        Dim dtTran As New DataSet
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "TABLE_NAME AS TableName " & _
                      ",COLUMN_NAME " & _
                      "FROM INFORMATION_SCHEMA.COLUMNS " & _
                      "WHERE COLUMN_NAME IN ('instituteunkid','traininginstituteunkid') AND TABLE_NAME NOT LIKE 'at%' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""

            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "hrinstitute_master" Then Continue For
                If dtRow.Item("TableName") = "hremp_qualification_tran" Then
                    strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @instituteunkid AND isvoid = 0 "
                ElseIf dtRow.Item("TableName") = "hrtraining_scheduling" Then
                    strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @instituteunkid AND isvoid = 0 AND iscancel = 0 "
                ElseIf dtRow.Item("TableName") = "mdmedical_claim_master" Then
                    strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @instituteunkid AND isvoid = 0 "
                ElseIf dtRow.Item("TableName") = "mdmedical_sicksheet" Then
                    strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @instituteunkid AND isvoid = 0 "
                End If




                dtTran = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dtTran.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            'Hemant (07 Jun 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
            If blnIsUsed = False Then
                strQ = "select isnull(trainingproviderunkid,0) FROM trtrainingvenue_master WHERE trainingproviderunkid = @instituteunkid AND isactive = 1 "
                If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "select isnull(trainingproviderunkid,0) FROM trdepartmentaltrainingneed_master WHERE trainingproviderunkid = @instituteunkid AND isvoid = 0 "
                If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "select isnull(trainingproviderunkid,0) FROM trtraining_request_master WHERE trainingproviderunkid = @instituteunkid AND isvoid = 0 "
                If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Hemant (07 Jun 2021) -- End

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal mbIshospital As Boolean, Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '  "  instituteunkid " & _
            '  ", institute_code " & _
            '  ", institute_name " & _
            '  ", institute_address " & _
            '  ", telephoneno " & _
            '  ", institute_fax " & _
            '  ", institute_email " & _
            '  ", description " & _
            '  ", contact_person " & _
            '  ", stateunkid " & _
            '  ", cityunkid " & _
            '  ", countryunkid " & _
            '  ", ishospital " & _
            '  ", isactive " & _
            ' ", isformrequire " & _
            ' "FROM hrinstitute_master " & _
            ' "WHERE 1=1 and ishospital=@ishospital"

            strQ = "SELECT " & _
              "  instituteunkid " & _
              ", institute_code " & _
              ", institute_name " & _
              ", institute_address " & _
              ", telephoneno " & _
              ", institute_fax " & _
              ", institute_email " & _
              ", description " & _
              ", contact_person " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", countryunkid " & _
              ", ishospital " & _
              ", isactive " & _
            ", isformrequire " & _
                     ", islocalprovider " & _
             "FROM hrinstitute_master " & _
             "WHERE 1=1 and ishospital=@ishospital"


            'Pinkal (18-Dec-2012) -- End



            'Anjan (04 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            If mblnIshospital = False Then
                strQ &= " AND isactive = 1 "
            End If

            'Anjan (04 May 2012)-End 


            If strCode.Length > 0 Then
                strQ &= " AND institute_code = @code "
            End If

            If strName.Length > 0 Then
                strQ &= " AND institute_name = @name "
            End If

            If intUnkid > 0 Then
                strQ &= " AND instituteunkid <> @instituteunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@ishospital", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mbIshospital)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(ByVal blnIshospital As Boolean, _
                                    Optional ByVal strListName As String = "List", _
                                    Optional ByVal mblFlag As Boolean = False, _
                                    Optional ByVal mintActive As Integer = -1, _
                                    Optional ByVal blnFormRequire As Boolean = False, _
                                    Optional ByVal intUserUnkId As Integer = 0, _
                                    Optional ByVal blnShowSyncOnly As Boolean = False) As DataSet 'Pinkal (25-APR-2012) -- Start 'S.SANDEEP [ 03 MAY 2012 ] -- START -- END
        'Public Function getListForCombo(ByVal blnIshospital As Boolean, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal mintActive As Integer = -1, Optional ByVal blnFormRequire As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as instituteunkid, ' ' +  @name  as name UNION "
            End If

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'strQ &= "SELECT instituteunkid, institute_name as name FROM hrinstitute_master where ishospital = @ishospital and isactive = 1  ORDER BY name "

            strQ &= "SELECT instituteunkid, institute_name as name FROM hrinstitute_master "

            If blnIshospital Then

                'Pinkal (25-APR-2012) -- Start
                'Enhancement : TRA Changes

                'strQ &= " LEFT JOIN mdprovider_mapping ON mdprovider_mapping.providerunkid  = hrinstitute_master.instituteunkid AND (mdprovider_mapping.userunkid = " & User._Object._Userunkid & " OR  mdprovider_mapping.userunkid = 1) "
                strQ &= " LEFT JOIN mdprovider_mapping ON mdprovider_mapping.providerunkid  = hrinstitute_master.instituteunkid AND (mdprovider_mapping.userunkid = " & IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId) & " OR  mdprovider_mapping.userunkid = 1) "

                'Pinkal (25-APR-2012) -- End

            End If

            strQ &= " where ishospital = @ishospital  "

            If blnIshospital = True Then
                If mintActive = 1 Then
                    strQ &= " AND isactive = 1 "
                ElseIf mintActive = 2 Then
                    strQ &= " AND isactive = 0 "
                End If
            Else
                strQ &= " AND isactive = 1 "
                'S.SANDEEP [ 03 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If blnShowSyncOnly Then
                    strQ &= " AND issync_recruit = 1 "
                End If
                'S.SANDEEP [ 03 MAY 2012 ] -- END
            End If


            If blnFormRequire Then
                strQ &= " AND ISNULL(isformrequire,0) = @isformrequire"
                objDataOperation.AddParameter("@isformrequire", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnFormRequire)
            End If

            strQ &= " ORDER BY name "


            'Pinkal (20-Jan-2012) -- End



            objDataOperation.AddParameter("@ishospital", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIshospital)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))


            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    'Sandeep [ 25 APRIL 2011 ] -- Start

    Public Function GetInstituteUnkid(ByVal blnIsTrainingInstitute As Boolean, ByVal strName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                            " instituteunkid " & _
                       "FROM hrinstitute_master " & _
                       "WHERE isactive = 1 AND institute_name = @Iname "
            If blnIsTrainingInstitute = True Then
                strQ &= "AND ishospital <> @IsTraining "
            Else
                strQ &= "AND ishospital = @IsTraining "
            End If

            objDataOperation.AddParameter("@Iname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@IsTraining", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsTrainingInstitute)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("instituteunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetInstituteUnkid", mstrModuleName)
        End Try
    End Function

    'Sandeep [ 25 APRIL 2011 ] -- End 

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Institute Code is already defined. Please define new Institute Code.")
			Language.setMessage(mstrModuleName, 2, "This Institute Name is already defined. Please define new Institute Name.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
﻿'************************************************************************************************************************************
'Class Name : clsUserDef_ReportMode.vb
'Purpose    :
'Date       :16/06/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsUserDef_ReportMode
    Private Shared ReadOnly mstrModuleName As String = "clsUserDef_ReportMode"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintReportmodeunkid As Integer
    Private mintReportunkid As Integer
    Private mintReporttypeid As Integer
    Private mintReportmodeid As Integer = 1
    Private mintHeadtypeid As Integer
    Private mstrEarningTranHeadIds As String = String.Empty
    Private mstrDeductionTranHeadIds As String = String.Empty
    Private mstrNonEarningDeductionTranHeadIds As String = String.Empty 'Sohail (09 Mar 2012)

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reportmodeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reportmodeunkid() As Integer
        Get
            Return mintReportmodeunkid
        End Get
        Set(ByVal value As Integer)
            mintReportmodeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reportunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reportunkid() As Integer
        Get
            Return mintReportunkid
        End Get
        Set(ByVal value As Integer)
            mintReportunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reporttypeid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reporttypeid() As Integer
        Get
            Return mintReporttypeid
        End Get
        Set(ByVal value As Integer)
            mintReporttypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reportmodeid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reportmodeid() As Integer
        Get
            Return mintReportmodeid
        End Get
        Set(ByVal value As Integer)
            mintReportmodeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set headtypeid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Headtypeid() As Integer
        Get
            Return mintHeadtypeid
        End Get
        Set(ByVal value As Integer)
            mintHeadtypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactionheadid [ Earning ]
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _EarningTranHeadIds() As String
        Get
            Return mstrEarningTranHeadIds
        End Get
        Set(ByVal value As String)
            mstrEarningTranHeadIds = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactionheadid [ Deduction ]
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _DeductionTranHeadIds() As String
        Get
            Return mstrDeductionTranHeadIds
        End Get
        Set(ByVal value As String)
            mstrDeductionTranHeadIds = value
        End Set
    End Property

    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _NonEarningDeduction_HeadIds() As String
        Get
            Return mstrNonEarningDeductionTranHeadIds
        End Get
        Set(ByVal value As String)
            mstrNonEarningDeductionTranHeadIds = value
        End Set
    End Property
    'Sohail (09 Mar 2012) -- End
#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  reportmodeunkid " & _
                   ", reportunkid " & _
                   ", reporttypeid " & _
                   ", reportmodeid " & _
                   ", headtypeid " & _
                   ", transactionheadid " & _
                   "FROM repuserdefined_reports " & _
                   "WHERE reportunkid = @reportunkid AND reporttypeid = @reporttypeid AND reportmodeid = @reportmodeid "

            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportunkid.ToString)
            objDataOperation.AddParameter("@reporttypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReporttypeid.ToString)
            objDataOperation.AddParameter("@reportmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportmodeid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim intCnt As Integer = 0

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintReportmodeunkid = CInt(dtRow.Item("reportmodeunkid"))
                mintReportunkid = CInt(dtRow.Item("reportunkid"))
                mintReporttypeid = CInt(dtRow.Item("reporttypeid"))
                mintReportmodeid = CInt(dtRow.Item("reportmodeid"))
                mintHeadtypeid = CInt(dtRow.Item("headtypeid"))
                Select Case CInt(dtRow.Item("reportmodeid"))
                    Case enUserDef_RepMode.EARNINGS
                        mstrEarningTranHeadIds = dtRow.Item("transactionheadid").ToString
                    Case enUserDef_RepMode.DEDUCTIONS
                        mstrDeductionTranHeadIds = dtRow.Item("transactionheadid").ToString
                    Case enUserDef_RepMode.EARNINGANDDEDUCTION
                        Select Case intCnt
                            Case 0
                                mstrEarningTranHeadIds = dtRow.Item("transactionheadid").ToString
                                intCnt += 1
                            Case 1
                                mstrDeductionTranHeadIds = dtRow.Item("transactionheadid").ToString
                                'Sohail (09 Mar 2012) -- Start
                                'TRA - ENHANCEMENT
                                intCnt += 1
                            Case 2
                                mstrNonEarningDeductionTranHeadIds = dtRow.Item("transactionheadid").ToString
                                'Sohail (09 Mar 2012) -- End
                        End Select
                End Select
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intReportUnkId As Integer = 0, Optional ByVal intReportModeID As Integer = -1, Optional ByVal intReportTypeID As Integer = -1) As DataSet
        'Sohail (25 Mar 2015) - [intReportTypeID]
        'Sohail (09 Mar 2015) - [intReportModeID]
        'Sohail (10 Dec 2013) - [intReportUnkId]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  reportmodeunkid " & _
              ", reportunkid " & _
              ", reporttypeid " & _
              ", reportmodeid " & _
              ", headtypeid " & _
              ", transactionheadid " & _
             "FROM repuserdefined_reports " & _
             "WHERE 1 = 1 "

            'Sohail (10 Dec 2013) -- Start
            'Enhancement - End of Service report - OMAN
            If intReportUnkId > 0 Then
                strQ &= " AND reportunkid = @reportunkid "
                objDataOperation.AddParameter("reportunkid", SqlDbType.Int, enDataType.INT_SIZE, intReportUnkId)
            End If
            'Sohail (10 Dec 2013) -- End

            'Sohail (09 Mar 2015) -- Start
            'Enhancement - New EFT Report Custom CSV Report.
            If intReportModeID > -1 Then
                strQ &= " AND reportmodeid = @reportmodeid "
                objDataOperation.AddParameter("reportmodeid", SqlDbType.Int, enDataType.INT_SIZE, intReportModeID)
            End If
            'Sohail (09 Mar 2015) -- End

            'Sohail (25 Mar 2015) -- Start
            'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
            If intReportTypeID > -1 Then
                strQ &= " AND reporttypeid = @reporttypeid "
                objDataOperation.AddParameter("reporttypeid", SqlDbType.Int, enDataType.INT_SIZE, intReportTypeID)
            End If
            'Sohail (25 Mar 2015) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (repuserdefined_reports) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportunkid.ToString)
            objDataOperation.AddParameter("@reporttypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReporttypeid.ToString)
            objDataOperation.AddParameter("@reportmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportmodeid.ToString)
            objDataOperation.AddParameter("@headtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHeadtypeid.ToString)
            'Sohail (26 May 2014) -- Start
            'TRA Issue Column1 does not exist on ED spreadsheet while selecting all heads on Save selection and generating reort.
            'objDataOperation.AddParameter("@transactionheadid", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrEarningTranHeadIds.ToString)
            objDataOperation.AddParameter("@transactionheadid", SqlDbType.Text, eZeeDataType.DESC_SIZE, mstrEarningTranHeadIds.ToString)
            'Sohail (26 May 2014) -- End

            strQ = "INSERT INTO repuserdefined_reports ( " & _
                      "  reportunkid " & _
                      ", reporttypeid " & _
                      ", reportmodeid " & _
                      ", headtypeid " & _
                      ", transactionheadid" & _
                   ") VALUES (" & _
                      "  @reportunkid " & _
                      ", @reporttypeid " & _
                      ", @reportmodeid " & _
                      ", @headtypeid " & _
                      ", @transactionheadid" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintReportmodeunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (repuserdefined_reports) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@reportmodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportmodeunkid.ToString)
            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportunkid.ToString)
            objDataOperation.AddParameter("@reporttypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReporttypeid.ToString)
            objDataOperation.AddParameter("@reportmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportmodeid.ToString)
            objDataOperation.AddParameter("@headtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHeadtypeid.ToString)
            'Sohail (26 May 2014) -- Start
            'TRA Issue Column1 does not exist on ED spreadsheet while selecting all heads on Save selection and generating reort.
            'objDataOperation.AddParameter("@transactionheadid", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrEarningTranHeadIds.ToString)
            objDataOperation.AddParameter("@transactionheadid", SqlDbType.Text, eZeeDataType.DESC_SIZE, mstrEarningTranHeadIds.ToString)
            'Sohail (26 May 2014) -- End

            strQ = "UPDATE repuserdefined_reports SET " & _
                   "  reportunkid = @reportunkid" & _
                   ", reporttypeid = @reporttypeid" & _
                   ", reportmodeid = @reportmodeid" & _
                   ", headtypeid = @headtypeid" & _
                   ", transactionheadid = @transactionheadid " & _
                   "WHERE reportmodeunkid = @reportmodeunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (repuserdefined_reports) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM repuserdefined_reports " & _
            "WHERE reportmodeunkid = @reportmodeunkid "

            objDataOperation.AddParameter("@reportmodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intReportId As Integer, ByVal intReportTypeId As Integer, ByVal intModeId As Integer, ByVal intHeadTypeId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  reportmodeunkid " & _
                   " ,reportunkid " & _
                   " ,reporttypeid " & _
                   " ,reportmodeid " & _
                   " ,headtypeid " & _
                   " ,transactionheadid " & _
                   "FROM repuserdefined_reports " & _
                   "WHERE reportunkid = @Reportid " & _
                   " AND reporttypeid = @ReportTpeId " & _
                   " AND reportmodeid = @ModeId " & _
                   " AND headtypeid = @HeadtypeId "


            objDataOperation.AddParameter("@Reportid", SqlDbType.Int, eZeeDataType.INT_SIZE, intReportId)
            objDataOperation.AddParameter("@ReportTpeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intReportTypeId)
            objDataOperation.AddParameter("@ModeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intModeId)
            objDataOperation.AddParameter("@HeadtypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intHeadTypeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)("reportmodeunkid")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetModeReportList() As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 1 AS Id , @Earning AS Name " & _
                   " UNION SELECT 2 AS Id , @Deduction AS Name " & _
                   " UNION SELECT 3 AS Id , @EarningDeduction AS Name "

            objDataOperation.AddParameter("@Earning", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Earning"))
            objDataOperation.AddParameter("@Deduction", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Deduction"))
            objDataOperation.AddParameter("@EarningDeduction", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Earning And Deduction"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetModeReportList ; Module Name : " & mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Earning")
			Language.setMessage(mstrModuleName, 2, "Deduction")
			Language.setMessage(mstrModuleName, 3, "Earning And Deduction")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

﻿'************************************************************************************************************************************
'Class Name : clsAnnouncement_Tran.vb
'Purpose    :
'Date       :01/04/2021
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsAnnouncements_master
    Private Shared ReadOnly mstrModuleName As String = "clsAnnouncements_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private objdocument As New clsAnnouncement_document
    Private mintAnnouncementunkid As Integer
    Private mstrTitle As String = String.Empty
    Private mstrAnnouncement As String = String.Empty
    Private mimgPhoto As Byte()
    Private mdtVisiblefromdate As Date
    Private mdtVisibletodate As Date
    Private mblnRequireAcknowledgement As Boolean
    Private mblnIsmarkasclose As Boolean
    Private mdtDocumentAttachment As DataTable = Nothing
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mblnIsWeb As Boolean = False
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set announcementunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Announcementunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintAnnouncementunkid
        End Get
        Set(ByVal value As Integer)
            mintAnnouncementunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set title
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Title() As String
        Get
            Return mstrTitle
        End Get
        Set(ByVal value As String)
            mstrTitle = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set announcement
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Announcement() As String
        Get
            Return mstrAnnouncement
        End Get
        Set(ByVal value As String)
            mstrAnnouncement = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set photo
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Photo() As Byte()
        Get
            Return mimgPhoto
        End Get
        Set(ByVal value As Byte())
            mimgPhoto = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set visiblefromdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Visiblefromdate() As Date
        Get
            Return mdtVisiblefromdate
        End Get
        Set(ByVal value As Date)
            mdtVisiblefromdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set visibletodate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Visibletodate() As Date
        Get
            Return mdtVisibletodate
        End Get
        Set(ByVal value As Date)
            mdtVisibletodate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set RequireAcknowledgement
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _RequireAcknowledgement() As Boolean
        Get
            Return mblnRequireAcknowledgement
        End Get
        Set(ByVal value As Boolean)
            mblnRequireAcknowledgement = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ismarkasclose
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ismarkasclose() As Boolean
        Get
            Return mblnIsmarkasclose
        End Get
        Set(ByVal value As Boolean)
            mblnIsmarkasclose = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtDocumentAttachment
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtDocumentAttachment() As DataTable
        Get
            Return mdtDocumentAttachment
        End Get
        Set(ByVal value As DataTable)
            mdtDocumentAttachment = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  announcementunkid " & _
                      ", title " & _
                      ", announcement " & _
                      ", photo " & _
                      ", visiblefromdate " & _
                      ", visibletodate " & _
                      ", requireacknowledgement " & _
                      ", ismarkasclose " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM hrmsconfiguration..cfannouncement_master " & _
                      " WHERE announcementunkid = @announcementunkid "

            objDataOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnnouncementunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAnnouncementunkid = CInt(dtRow.Item("announcementunkid"))
                mstrTitle = dtRow.Item("title").ToString
                mstrAnnouncement = dtRow.Item("announcement").ToString

                If IsDBNull(dtRow.Item("photo")) = False Then
                    mimgPhoto = dtRow.Item("photo")
                End If

                If IsDBNull(dtRow.Item("visiblefromdate")) = False Then
                    mdtVisiblefromdate = dtRow.Item("visiblefromdate")
                End If

                If IsDBNull(dtRow.Item("visibletodate")) = False Then
                    mdtVisibletodate = dtRow.Item("visibletodate")
                End If

                mblnRequireAcknowledgement = CBool(dtRow.Item("requireacknowledgement"))

                mblnIsmarkasclose = CBool(dtRow.Item("ismarkasclose"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True _
                                   , Optional ByVal mintMarkAsClose As Integer = -1, Optional ByVal objDoOperation As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  announcementunkid " & _
                      ", title " & _
                      ", announcement " & _
                      ", photo " & _
                      ", visiblefromdate " & _
                      ", visibletodate " & _
                      ", requireacknowledgement " & _
                      ", ismarkasclose " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM hrmsconfiguration..cfannouncement_master  "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid  = 0 "
            End If

            If mintMarkAsClose <> -1 Then
                strQ &= " AND ismarkasclose = @ismarkasclose "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@ismarkasclose", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(mintMarkAsClose))
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrmsconfiguration..cfannouncement_master) </purpose>
    Public Function Insert() As Boolean

        If isExist(mstrTitle.Trim(), "", -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Title is already defined. Please define new Title.")
            Return False
        ElseIf isExist("", mstrAnnouncement.Trim(), -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Announcement is already defined. Please define new Announcement.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@title", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTitle.ToString)
            objDataOperation.AddParameter("@announcement", SqlDbType.NVarChar, mstrAnnouncement.Length, mstrAnnouncement.ToString)
            If mimgPhoto IsNot Nothing Then
                objDataOperation.AddParameter("@photo", SqlDbType.Image, mimgPhoto.Length, mimgPhoto)
            Else
                objDataOperation.AddParameter("@photo", SqlDbType.Image, eZeeDataType.IMAGE_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@visiblefromdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVisiblefromdate <> Nothing, mdtVisiblefromdate, DBNull.Value))
            objDataOperation.AddParameter("@visibletodate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVisibletodate <> Nothing, mdtVisibletodate, DBNull.Value))
            objDataOperation.AddParameter("@requireacknowledgement", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnRequireAcknowledgement.ToString)
            objDataOperation.AddParameter("@ismarkasclose", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmarkasclose.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO hrmsconfiguration..cfannouncement_master ( " & _
                      "  title " & _
                      ", announcement " & _
                      ", photo " & _
                      ", visiblefromdate " & _
                      ", visibletodate " & _
                      ", requireacknowledgement " & _
                      ", ismarkasclose " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @title " & _
                      ", @announcement " & _
                      ", @photo " & _
                      ", @visiblefromdate " & _
                      ", @visibletodate " & _
                      ", @requireacknowledgement " & _
                      ", @ismarkasclose " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAnnouncementunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAnnouncementAuditTrail(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If mdtDocumentAttachment IsNot Nothing AndAlso mdtDocumentAttachment.Rows.Count > 0 Then
                objdocument._Announcementunkid = mintAnnouncementunkid
                objdocument._dtDocumentList = mdtDocumentAttachment
                objdocument._Userunkid = mintUserunkid
                objdocument._WebClientIP = mstrWebClientIP
                objdocument._WebHostName = mstrWebHostName
                objdocument._WebFormName = mstrWebFormName
                objdocument._IsWeb = mblnIsWeb
                If objdocument.Insert_Update_DeleteDocument(mintAnnouncementunkid, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrmsconfiguration..cfannouncement_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrTitle.Trim(), "", mintAnnouncementunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Title is already defined. Please define new Title.")
            Return False
        ElseIf isExist("", mstrAnnouncement.Trim(), mintAnnouncementunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Announcement is already defined. Please define new Announcement.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnnouncementunkid.ToString)
            objDataOperation.AddParameter("@title", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTitle.ToString)
            objDataOperation.AddParameter("@announcement", SqlDbType.NVarChar, mstrAnnouncement.Length, mstrAnnouncement.ToString)

            If mimgPhoto IsNot Nothing Then
                objDataOperation.AddParameter("@photo", SqlDbType.Image, mimgPhoto.Length, mimgPhoto)
            Else
                objDataOperation.AddParameter("@photo", SqlDbType.Image, eZeeDataType.IMAGE_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@visiblefromdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVisiblefromdate <> Nothing, mdtVisiblefromdate, DBNull.Value))
            objDataOperation.AddParameter("@visibletodate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVisibletodate <> Nothing, mdtVisibletodate, DBNull.Value))
            objDataOperation.AddParameter("@requireacknowledgement", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnRequireAcknowledgement.ToString)
            objDataOperation.AddParameter("@ismarkasclose", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmarkasclose.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            strQ = " UPDATE hrmsconfiguration..cfannouncement_master SET " & _
                      "  title = @title" & _
                      ", announcement = @announcement" & _
                      ", photo = @photo" & _
                      ", visiblefromdate = @visiblefromdate" & _
                      ", visibletodate = @visibletodate" & _
                      ", requireacknowledgement = @requireacknowledgement " & _
                      ", ismarkasclose = @ismarkasclose" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      " WHERE announcementunkid = @announcementunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If InsertAnnouncementAuditTrail(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtDocumentAttachment IsNot Nothing AndAlso mdtDocumentAttachment.Rows.Count > 0 Then
                objdocument._Announcementunkid = mintAnnouncementunkid
                objdocument._dtDocumentList = mdtDocumentAttachment
                objdocument._Userunkid = mintUserunkid
                objdocument._WebClientIP = mstrWebClientIP
                objdocument._WebHostName = mstrWebHostName
                objdocument._WebFormName = mstrWebFormName
                objdocument._IsWeb = mblnIsWeb
                If objdocument.Insert_Update_DeleteDocument(mintAnnouncementunkid, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrmsconfiguration..cfannouncement_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            objdocument._Announcementunkid = intUnkid
            objdocument._Isvoid = True
            objdocument._Userunkid = mintUserunkid
            objdocument._Voiduserunkid = mintVoiduserunkid
            objdocument._Voiddatetime = mdtVoiddatetime
            objdocument._Voidreason = mstrVoidreason
            objdocument._WebClientIP = mstrWebClientIP
            objdocument._WebHostName = mstrWebHostName
            objdocument._WebFormName = mstrWebFormName
            objdocument._IsWeb = mblnIsWeb
            If objdocument.DeleteDocuments(objDataOperation, mintAnnouncementunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " UPDATE  hrmsconfiguration..cfannouncement_master SET " & _
                      " isvoid =1,voiddatetime= @voiddatetime, " & _
                      " voidreason = @voidreason,  " & _
                      " voiduserunkid = @voiduserunkid " & _
                      " WHERE announcementunkid = @announcementunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Announcementunkid(objDataOperation) = intUnkid

            If InsertAnnouncementAuditTrail(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal mstrTitle As String, ByVal mstrAnnouncement As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                      "  announcementunkid " & _
                      ", title " & _
                      ", announcement " & _
                      ", photo " & _
                      ", visiblefromdate " & _
                      ", visibletodate " & _
                      ", requireacknowledgement " & _
                      ", ismarkasclose " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM hrmsconfiguration..cfannouncement_master " & _
                      " WHERE 1 = 1 "

            If mstrTitle.Trim.Length > 0 Then
                strQ &= " AND  title = @title "
                objDataOperation.AddParameter("@title", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTitle)
            End If

            If mstrAnnouncement.Trim.Length > 0 Then
                strQ &= " AND  announcement = @announcement "
                objDataOperation.AddParameter("@announcement", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrAnnouncement)
            End If

            If intUnkid > 0 Then
                strQ &= " AND announcementunkid <> @announcementunkid"
                objDataOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAnnouncementAuditTrail(ByVal objDoOperation As clsDataOperation, ByVal xAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDoOperation.ClearParameters()
            objDoOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnnouncementunkid)
            objDoOperation.AddParameter("@title", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTitle.ToString)
            objDoOperation.AddParameter("@announcement", SqlDbType.NVarChar, mstrAnnouncement.Length, mstrAnnouncement.ToString)
            If mimgPhoto IsNot Nothing Then
                objDoOperation.AddParameter("@photo", SqlDbType.Image, mimgPhoto.Length, mimgPhoto)
            Else
                objDoOperation.AddParameter("@photo", SqlDbType.Image, eZeeDataType.IMAGE_SIZE, DBNull.Value)
            End If
            objDoOperation.AddParameter("@visiblefromdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVisiblefromdate <> Nothing, mdtVisiblefromdate, DBNull.Value))
            objDoOperation.AddParameter("@visibletodate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVisibletodate <> Nothing, mdtVisibletodate, DBNull.Value))
            objDoOperation.AddParameter("@requireacknowledgement", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnRequireAcknowledgement.ToString)
            objDoOperation.AddParameter("@ismarkasclose", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmarkasclose.ToString)
            objDoOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType.ToString)
            objDoOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            If mstrWebClientIP.Trim.Length > 0 Then
                objDoOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            Else
                objDoOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            End If

            If mstrWebHostName.Length > 0 Then
                objDoOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)
            Else
                objDoOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName())
            End If

            objDoOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
            objDoOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO hrmsconfiguration..atcfannouncement_master ( " & _
                      "  announcementunkid " & _
                      ",  title " & _
                      ", announcement " & _
                      ", photo " & _
                      ", visiblefromdate " & _
                      ", visibletodate " & _
                      ", requireacknowledgement " & _
                      ", ismarkasclose " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      "  @announcementunkid " & _
                      ", @title " & _
                      ", @announcement " & _
                      ", @photo " & _
                      ", @visiblefromdate " & _
                      ", @visibletodate " & _
                      ", @requireacknowledgement " & _
                      ", @ismarkasclose " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GetDate() " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDoOperation.ExecQuery(strQ, "List")

            If objDoOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAnnouncementAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Title is already defined. Please define new Title.")
            Language.setMessage(mstrModuleName, 2, "This Announcement is already defined. Please define new Announcement.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
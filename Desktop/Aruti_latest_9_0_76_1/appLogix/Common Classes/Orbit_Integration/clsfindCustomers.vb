﻿Imports System.ComponentModel

Public Class clsfindCustomers
    Private _referenceNo As String = String.Empty
    Private _credentials As New credentials

    Public Property referenceNo() As String
        Get
            Return _referenceNo
        End Get
        Set(ByVal value As String)
            _referenceNo = value
        End Set
    End Property

    Public Property credentials() As credentials
        Get
            Return _credentials
        End Get
        Set(ByVal value As credentials)
            _credentials = value
        End Set
    End Property
End Class

Public Class findCustomersInformation

    Private _referenceNo As String = ""
    Private _credentials As New credentials
    Private _customerNo As String = ""
    Private _firstName As String = ""
    Private _lastName As String = ""

    <DefaultValue("")> _
    Public Property referenceNo() As String
        Get
            Return _referenceNo
        End Get
        Set(ByVal value As String)
            _referenceNo = value
        End Set
    End Property

    Public Property credentials() As credentials
        Get
            Return _credentials
        End Get
        Set(ByVal value As credentials)
            _credentials = value
        End Set
    End Property

    <DefaultValue("")> _
    Public Property customerNo() As String
        Get
            Return _customerNo
        End Get
        Set(ByVal value As String)
            _customerNo = value
        End Set
    End Property

    <DefaultValue("")> _
    Public Property firstName() As String
        Get
            Return _firstName
        End Get
        Set(ByVal value As String)
            _firstName = value
        End Set
    End Property

    <DefaultValue("")> _
    Public Property lastName() As String
        Get
            Return _lastName
        End Get
        Set(ByVal value As String)
            _lastName = value
        End Set
    End Property

End Class
﻿Option Strict On
'************************************************************************************************************************************
'Class Name : clstraining_approver_master.vb
'Purpose    :
'Date       :10-Oct-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clstraining_approver_master
    Private Shared ReadOnly mstrModuleName As String = "clstraining_approver_master"
    Dim objTrainingApproverTrans As New clsTraining_Approver_Tran
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintMappingunkid As Integer
    Private mintCalendarunkid As Integer
    Private mintLevelunkid As Integer
    Private mintMapuserunkid As Integer
    Private mblnIsactive As Boolean = True
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing
    Private mintRoleunkid As Integer

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mappingunkid(Optional ByVal objOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
            Call GetData(objOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set calendarunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Calendarunkid() As Integer
        Get
            Return mintCalendarunkid
        End Get
        Set(ByVal value As Integer)
            mintCalendarunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mapuserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mapuserunkid() As Integer
        Get
            Return mintMapuserunkid
        End Get
        Set(ByVal value As Integer)
            mintMapuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set roleunkid
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Roleunkid() As Integer
        Get
            Return mintRoleunkid
        End Get
        Set(ByVal value As Integer)
            mintRoleunkid = value
        End Set
    End Property

    
#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  mappingunkid " & _
              ", calendarunkid " & _
              ", levelunkid " & _
              ", mapuserunkid " & _
              ", isactive " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", roleunkid " & _
             "FROM hrtraining_approver_master " & _
             "WHERE mappingunkid = @mappingunkid "

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintCalendarunkid = CInt(dtRow.Item("calendarunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintMapuserunkid = CInt(dtRow.Item("mapuserunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintRoleunkid = CInt(dtRow.Item("roleunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                          , Optional ByVal blnOnlyActive As Boolean = True _
                          , Optional ByVal mstrFilter As String = "" _
                          , Optional ByVal intApproverUserUnkId As Integer = 0 _
                            , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                            , Optional ByVal blnAddSelect As Boolean = False _
                            , Optional ByVal intRoleUnkId As Integer = 0) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            If blnAddSelect Then
                strQ = "SELECT " & _
                        "  0 AS mappingunkid " & _
                        " ,0 AS calendarunkid " & _
                        " ,0 AS levelunkid " & _
                        " ,'' AS LevelCode " & _
                        " ,'' AS  Level " & _
                        " ,0 AS mapuserunkid " & _
                        " ,@Select AS approver " & _
                        " ,0 AS isactive " & _
                        " ,'' As Status " & _
                        " ,0 AS isvoid " & _
                        " ,0 AS voiduserunkid " & _
                        " ,NULL AS voiddatetime " & _
                        " ,'' AS voidreason " & _
                        " ,0 AS priority " & _
                        " ,'' as Calendar " & _
                        " ,0 AS roleunkid " & _
                        " ,'' AS role " & _
                        "UNION ALL "
            End If
            strQ &= "SELECT " & _
                    "     TAM.mappingunkid " & _
                    "    ,TAM.calendarunkid " & _
                    "    ,TAM.levelunkid " & _
                    "    ,ISNULL(TLM.levelcode,'') as LevelCode " & _
                    "    ,ISNULL(TLM.levelname,'') as Level " & _
                    "    ,TAM.mapuserunkid " & _
                    "    ,ISNULL(UM.username,'') AS approver " & _
                    "    ,TAM.isactive " & _
                    "    ,CASE WHEN TAM.isactive = 0 THEN @InActive ELSE @Active END As Status " & _
                    "    ,TAM.isvoid " & _
                    "    ,TAM.voiduserunkid " & _
                    "    ,TAM.voiddatetime " & _
                    "    ,TAM.voidreason " & _
                    "    ,ISNULL(TLM.priority,0) AS priority " & _
                    "    ,ISNULL(TCM.calendar_name , '') AS calendar " & _
                    "    ,TAM.roleunkid " & _
                    "    ,ISNULL(RM.name,'') as role " & _
                    "FROM hrtraining_approver_master AS TAM " & _
                    "    JOIN hrtraining_approverlevel_master AS TLM ON TAM.levelunkid = TLM.levelunkid " & _
                    "    LEFT JOIN hrmsconfiguration..cfuser_master AS UM ON UM.userunkid = TAM.mapuserunkid " & _
                    "    LEFT JOIN trtraining_calendar_master as TCM ON TCM.calendarunkid = TAM.calendarunkid " & _
                    "    LEFT JOIN hrmsConfiguration..cfrole_master AS RM ON RM.roleunkid = TAM.roleunkid " & _
                    "WHERE TAM.isvoid = 0 AND TLM.isactive = 1 " & _
                    " AND TAM.roleunkid > 0 "

            If intApproverUserUnkId > 0 Then
                strQ &= " AND TAM.mapuserunkid = @mapuserunkid "
            End If

            If intRoleUnkId > 0 Then
                strQ &= " AND TAM.roleunkid = @roleunkid "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            objDataOperation.AddParameter("@InActive", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "In Active"))
            objDataOperation.AddParameter("@Active", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Active"))
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkId)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRoleUnkId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_approver_master) </purpose>
    Public Function Insert(Optional ByVal mdtran As DataTable = Nothing) As Boolean
        If isExist(mintCalendarunkid, mintMapuserunkid, mintLevelunkid, mintRoleunkid, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Training Approver already exists for selected level. Please define new Training Approver.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)

            strQ = "INSERT INTO hrtraining_approver_master ( " & _
                   "  calendarunkid " & _
                   ", levelunkid " & _
                   ", mapuserunkid " & _
                   ", isactive " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason" & _
                   ", roleunkid " & _
                   ") VALUES (" & _
                   "  @calendarunkid " & _
                   ", @levelunkid " & _
                   ", @mapuserunkid " & _
                   ", @isactive " & _
                   ", @isvoid " & _
                   ", @voiduserunkid " & _
                   ", @voiddatetime " & _
                   ", @voidreason" & _
                   ", @roleunkid " & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMappingunkid = CInt(dsList.Tables(0).Rows(0).Item(0))

            If InsertAuditTrailApproverMapping(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtran IsNot Nothing Then
            objTrainingApproverTrans._TrainingApproverunkid = mintMappingunkid
            objTrainingApproverTrans._TranDataTable = mdtran
            objTrainingApproverTrans._WebFormName = mstrFormName
            If objTrainingApproverTrans._TranDataTable IsNot Nothing AndAlso objTrainingApproverTrans._TranDataTable.Rows.Count > 0 Then

                If objTrainingApproverTrans.Insert_Update_Delete(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremp_appusermapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "INSERT INTO attrtraining_approver_tran ( " & _
                      "  tranguid" & _
                      ", trapprovertranunkid " & _
                      ", trapproverunkid " & _
                      ", employeeunkid " & _
                      ", audittypeid " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", form_name " & _
                      ", ip " & _
                      ", host " & _
                      ", isweb " & _
                     " )" & _
                     "SELECT " & _
                      "  LOWER(NEWID()) " & _
                      ", trapprovertranunkid " & _
                      ", trapproverunkid " & _
                      ", employeeunkid " & _
                      ", 3 " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @form_name " & _
                      ", @ip " & _
                      ", @host " & _
                      ", @isweb " & _
                 "FROM trtraining_approver_tran " & _
                      "WHERE isvoid = 0 " & _
                       "AND trapproverunkid = @trapproverunkid "
            
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE hrtraining_approver_master SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   " ,voiddatetime = @voiddatetime " & _
                   " ,voidreason = @voidreason " & _
                   "WHERE mappingunkid = @mappingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Mappingunkid(objDataOperation) = intUnkid

            If InsertAuditTrailApproverMapping(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xCalendarId As Integer, ByVal xMapUserId As Integer, ByVal xLevelId As Integer, _
                             ByVal xRoleId As Integer, _
                            Optional ByVal intunkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  mappingunkid " & _
                   " ,calendarunkid " & _
                   " ,levelunkid " & _
                   " ,mapuserunkid " & _
                   " ,isactive " & _
                   " ,isvoid " & _
                   " ,voiduserunkid " & _
                   " ,voiddatetime " & _
                   " ,voidreason " & _
                   " ,roleunkid " & _
                   "FROM hrtraining_approver_master " & _
                   "WHERE isvoid = 0  " & _
                   " AND calendarunkid = @calendarunkid " & _
                   " AND mapuserunkid = @mapuserunkid " & _
                   " AND levelunkid = @levelunkid " & _
                   " AND roleunkid = @roleunkid "

          

            objDataOperation.ClearParameters()

            If intunkid > 0 Then
                strQ &= " AND mappingunkid <> @mappingunkid"
                objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)
            End If

            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCalendarId)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMapUserId)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLevelId)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRoleId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InActiveApprover(ByVal xMappingId As Integer, ByVal intCompanyID As Integer, ByVal blnIsActive As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If blnIsActive Then
                strQ = " UPDATE hrtraining_approver_master set isactive = 1 where mappingunkid = @mappingunkid AND isvoid = 0  "
            Else
                strQ = " UPDATE hrtraining_approver_master set isactive = 0 where mappingunkid = @mappingunkid AND isvoid = 0 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMappingId)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Mappingunkid(objDataOperation) = xMappingId

            If InsertAuditTrailApproverMapping(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InActiveApprover; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Function GetEmployeeListByApprover(ByVal xDatabaseName As String, _
                                              ByVal xUserUnkid As Integer, _
                                              ByVal xYearUnkid As Integer, _
                                              ByVal xCompanyUnkid As Integer, _
                                              ByVal xEmpAsOnDate As DateTime, _
                                              ByVal xUserModeSetting As String, _
                                              ByVal xOnlyApproved As Boolean, _
                                              ByVal strListName As String, _
                                              ByVal mblnAddSelect As Boolean, _
                                              Optional ByVal strFilterQuery As String = "", _
                                              Optional ByVal blnReinstatementDate As Boolean = False, _
                                              Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                              Optional ByVal blnAddApprovalCondition As Boolean = True) As DataSet
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            dsList = objEmp.GetEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xEmpAsOnDate, xEmpAsOnDate, xUserModeSetting, xOnlyApproved, False, strListName, mblnAddSelect, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, strFilterQuery, blnReinstatementDate, blnIncludeAccessFilterQry, blnAddApprovalCondition)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeListByApprover; Module Name: " & mstrModuleName)
        Finally
            objEmp = Nothing
        End Try
        Return dsList
    End Function

    Public Function InsertAuditTrailApproverMapping(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO athrtraining_approver_master ( " & _
                   "  tranguid " & _
                   ", mappingunkid " & _
                   ", calendarunkid " & _
                   ", levelunkid " & _
                   ", mapuserunkid " & _
                   ", isactive " & _
                   ", audittype " & _
                   ", audituserunkid " & _
                   ", auditdatetime " & _
                   ", ip" & _
                   ", hostname" & _
                   ", form_name " & _
                   ", isweb " & _
                   ", roleunkid " & _
                   ") VALUES (" & _
                   "  @tranguid " & _
                   ", @mappingunkid " & _
                   ", @calendarunkid " & _
                   ", @levelunkid " & _
                   ", @mapuserunkid " & _
                   ", @isactive " & _
                   ", @audittype " & _
                   ", @audituserunkid " & _
                   ", @auditdatetime " & _
                   ", @ip" & _
                   ", @hostname" & _
                   ", @form_name " & _
                   ", @isweb " & _
                   ", @roleunkid " & _
                   ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid)
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtAuditDatetime <> Nothing, mdtAuditDatetime, DBNull.Value))
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailApproverMapping; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    Public Function GetApproverEmployeeId(ByVal CalendarID As Integer, ByVal ApproveID As Integer) As String
        Dim mstrEmployeeIds As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' " & _
                               "+ CAST(trtraining_approver_tran.employeeunkid AS VARCHAR(MAX)) " & _
                      "FROM     hrtraining_approver_master " & _
                                "JOIN trtraining_approver_tran ON hrtraining_approver_master.mappingunkid = trtraining_approver_tran.trapproverunkid AND trtraining_approver_tran.isvoid = 0" & _
                      "WHERE    hrtraining_approver_master.calendarunkid = " & CalendarID & " " & _
                                " AND hrtraining_approver_master.mapuserunkid = " & ApproveID & " " & _
                                " AND hrtraining_approver_master.isvoid = 0 " & _
                                " AND hrtraining_approver_master.isactive = 1 " & _
                      "ORDER BY trtraining_approver_tran.employeeunkid " & _
                    "FOR " & _
                      "XML PATH('') " & _
                    "), 1, 1, ''), '') EmployeeIds "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeIds = dsList.Tables(0).Rows(0)("EmployeeIds").ToString()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverEmployeeId", mstrModuleName)
        End Try
        Return mstrEmployeeIds
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Update Database Table hrtraining_approver_master </purpose>
    Public Function Update(Optional ByVal mdtran As DataTable = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If isExist(mintCalendarunkid, mintMapuserunkid, mintLevelunkid, mintRoleunkid, mintMappingunkid) = True Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Training Approver already exists. Please define new Training Approver.")
            Return False
        End If

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)


            strQ = "UPDATE hrtraining_approver_master  SET " & _
                    " calendarunkid = @calendarunkid " & _
                    " ,levelunkid = @levelunkid " & _
                    " ,mapuserunkid = @mapuserunkid " & _
                    " ,isactive = @isactive " & _
                    " ,isvoid = @isvoid " & _
                    " ,voiddatetime = @voiddatetime " & _
                    " ,voiduserunkid = @voiduserunkid " & _
                    " ,voidreason = @voidreason " & _
                    " ,roleunkid = @roleunkid " & _
                " WHERE mappingunkid = @mappingunkid"

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailApproverMapping(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtran IsNot Nothing Then
            objTrainingApproverTrans._TrainingApproverunkid = mintMappingunkid
            objTrainingApproverTrans._TranDataTable = mdtran
            objTrainingApproverTrans._UserId = mintAuditUserId
            objTrainingApproverTrans._WebClientIP = mstrClientIP
            objTrainingApproverTrans._WebFormName = mstrFormName
            objTrainingApproverTrans._WebHostName = mstrHostName

            If objTrainingApproverTrans.Insert_Update_Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    Public Function GetEmployeeApprover(ByVal xDatabaseName As String, _
                                        ByVal xUserUnkid As Integer, _
                                        ByVal xYearUnkid As Integer, _
                                        ByVal xCompanyUnkid As Integer, _
                                        ByVal intEmployeeID As Integer, _
                                        ByVal intMaxPriority As Integer, _
                                        Optional ByVal mstrFilter As String = "" _
                                        ) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            Dim strQFinal As String = String.Empty
            Dim strQCondition As String = String.Empty

            objDataOperation.ClearParameters()

            strQ &= " SELECT " & _
                      "  hrtraining_approver_master.mapuserunkid " & _
                      ", trtraining_approver_tran.trapproverunkid " & _
                      ", trtraining_approver_tran.employeeunkid " & _
                      ", hrtraining_approverlevel_master.levelunkid " & _
                      ", hrtraining_approverlevel_master.levelname " & _
                      ", hrtraining_approverlevel_master.priority " & _
                      ", hrtraining_approver_master.roleunkid " & _
                      " FROM trtraining_approver_tran " & _
                      " JOIN hrtraining_approver_master ON trtraining_approver_tran.trapproverunkid = hrtraining_approver_master.mappingunkid " & _
                      "     AND hrtraining_approver_master.isvoid = 0 AND hrtraining_approver_master.isactive = 1 " & _
                      " JOIN hrtraining_approverlevel_master ON hrtraining_approverlevel_master.levelunkid = hrtraining_approver_master.levelunkid "

            strQFinal = strQ

            strQCondition = " WHERE trtraining_approver_tran.isvoid = 0  AND trtraining_approver_tran.employeeunkid = " & intEmployeeID


            If mstrFilter.Trim.Length > 0 Then
                strQCondition &= " AND " & mstrFilter.Trim
            End If

            strQ &= strQCondition

            objDataOperation.ClearParameters()

            Dim blnTrainingRequireForForeignTravelling As Boolean = False
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = xCompanyUnkid
            blnTrainingRequireForForeignTravelling = objConfig._TrainingRequireForForeignTravelling
            objConfig = Nothing

            If blnTrainingRequireForForeignTravelling = True Then
                If intMaxPriority > 0 Then
                    strQ &= "  AND hrtraining_approverlevel_master.priority = @priority "
                    objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intMaxPriority.ToString)
                End If
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            'For Each dRow As DataRow In dsCompany.Tables("Company").Rows
            '    strQ = strQFinal

            '    strQ &= strQCondition
            '    strQ &= " AND cfuser_master.companyunkid = " & CInt(dRow.Item("companyunkid")) & ""

            '    dsExtList = objDataOperation.ExecQuery(strQ, "List")

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            '    If dsList.Tables.Count <= 0 Then
            '        dsList.Tables.Add(dsExtList.Tables("List").Copy)
            '    Else
            '        dsList.Tables("List").Merge(dsExtList.Tables("List"), True)
            '    End If
            'Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "priority", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeApprover", mstrMessage)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
        If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
            Return dsList.Tables("List")
        Else
            Return Nothing
        End If
    End Function

    Public Function GetNextEmployeeApprovers(ByVal strDatabaseName As String _
                                          , ByVal intCompanyId As Integer _
                                          , ByVal intYearId As Integer _
                                          , ByVal strUserAccessMode As String _
                                          , ByVal intPrivilegeId As Integer _
                                          , ByVal xEmployeeAsOnDate As String _
                                          , ByVal intUserId As Integer _
                                          , ByVal intEmployeeID As Integer _
                                          , ByVal decCostAmount As Decimal _
                                          , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                          , Optional ByVal mstrFilterString As String = "" _
                                          ) As DataTable
        Dim StrQ As String = String.Empty
        Dim dtList As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""

            StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                 "DROP TABLE #USR "
            StrQ &= "SELECT " & _
                    "* " & _
                    "INTO #USR " & _
                    "FROM " & _
                    "( "

            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Dim xStrJoinColName As String = ""
                Dim xIntAllocId As Integer = 0
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        xStrJoinColName = "stationunkid"
                        xIntAllocId = CInt(enAllocation.BRANCH)
                    Case enAllocation.DEPARTMENT_GROUP
                        xStrJoinColName = "deptgroupunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
                    Case enAllocation.DEPARTMENT
                        xStrJoinColName = "departmentunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT)
                    Case enAllocation.SECTION_GROUP
                        xStrJoinColName = "sectiongroupunkid"
                        xIntAllocId = CInt(enAllocation.SECTION_GROUP)
                    Case enAllocation.SECTION
                        xStrJoinColName = "sectionunkid"
                        xIntAllocId = CInt(enAllocation.SECTION)
                    Case enAllocation.UNIT_GROUP
                        xStrJoinColName = "unitgroupunkid"
                        xIntAllocId = CInt(enAllocation.UNIT_GROUP)
                    Case enAllocation.UNIT
                        xStrJoinColName = "unitunkid"
                        xIntAllocId = CInt(enAllocation.UNIT)
                    Case enAllocation.TEAM
                        xStrJoinColName = "teamunkid"
                        xIntAllocId = CInt(enAllocation.TEAM)
                    Case enAllocation.JOB_GROUP
                        xStrJoinColName = "jobgroupunkid"
                        xIntAllocId = CInt(enAllocation.JOB_GROUP)
                    Case enAllocation.JOBS
                        xStrJoinColName = "jobunkid"
                        xIntAllocId = CInt(enAllocation.JOBS)
                    Case enAllocation.CLASS_GROUP
                        xStrJoinColName = "classgroupunkid"
                        xIntAllocId = CInt(enAllocation.CLASS_GROUP)
                    Case enAllocation.CLASSES
                        xStrJoinColName = "classunkid"
                        xIntAllocId = CInt(enAllocation.CLASSES)
                End Select
                StrQ &= "SELECT DISTINCT " & _
                        "    B" & index.ToString() & ".userunkid AS mapuserunkid " & _
                        "   ,A.employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        AEM.employeeunkid " & _
                        "       ,ISNULL(T.departmentunkid, 0) AS departmentunkid " & _
                        "       ,ISNULL(J.jobunkid, 0) AS jobunkid " & _
                        "       ,ISNULL(T.classgroupunkid, 0) AS classgroupunkid " & _
                        "       ,ISNULL(T.classunkid, 0) AS classunkid " & _
                        "       ,ISNULL(T.stationunkid, 0) AS stationunkid " & _
                        "       ,ISNULL(T.deptgroupunkid, 0) AS deptgroupunkid " & _
                        "       ,ISNULL(T.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                        "       ,ISNULL(T.sectionunkid, 0) AS sectionunkid " & _
                        "       ,ISNULL(T.unitgroupunkid, 0) AS unitgroupunkid " & _
                        "       ,ISNULL(T.unitunkid, 0) AS unitunkid " & _
                        "       ,ISNULL(T.teamunkid, 0) AS teamunkid " & _
                        "       ,ISNULL(J.jobgroupunkid, 0) AS jobgroupunkid " & _
                        "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            stationunkid " & _
                        "           ,deptgroupunkid " & _
                        "           ,departmentunkid " & _
                        "           ,sectiongroupunkid " & _
                        "           ,sectionunkid " & _
                        "           ,unitgroupunkid " & _
                        "           ,unitunkid " & _
                        "           ,teamunkid " & _
                        "           ,classgroupunkid " & _
                        "           ,classunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM " & strDatabaseName & "..hremployee_transfer_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS T ON T.employeeunkid = AEM.employeeunkid " & _
                        "   AND T.Rno = 1 " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            jobgroupunkid " & _
                        "           ,jobunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS J ON J.employeeunkid = AEM.employeeunkid " & _
                        "   AND J.Rno = 1 " & _
                        ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        UPM.userunkid " & _
                        "       ,UPT.allocationunkid " & _
                        "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                        "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                        "   WHERE UPM.companyunkid = " & intCompanyId & " AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
                        ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "
                If index < strvalues.Length - 1 Then
                    StrQ &= " INTERSECT "
                End If
            Next

            StrQ &= ") AS Fl "

            StrQ &= "SELECT " & _
                        "#USR.mapuserunkid " & _
                       ",#USR.employeeunkid " & _
                       ",UM.roleunkid " & _
                       ",TAM.levelunkid " & _
                       ",TLM.priority " & _
                       ",TAM.mappingunkid trapproverunkid " & _
                       ",TAMX.costamountfrom " & _
                       ",TAMX.costamountto " & _
                    "FROM #USR " & _
                    "JOIN hrmsConfiguration..cfuser_master UM    ON UM.userunkid = #USR.mapuserunkid " & _
                    "JOIN hrtraining_approver_master TAM    ON TAM.roleunkid = UM.roleunkid " & _
                            "AND TAM.isvoid = 0 AND TAM.isactive = 1 " & _
                    "JOIN hrtraining_approverlevel_master TLM ON TLM.levelunkid = TAM.levelunkid AND TLM.isactive = 1 " & _
                    "JOIN trtraining_approval_matrix TAMX ON TAMX.levelunkid = TLM.levelunkid AND TAMX.isvoid = 0 "

            StrQ &= "    JOIN " & _
                   "    ( " & _
                   "        SELECT DISTINCT " & _
                   "             cfuser_master.userunkid " & _
                   "            ,cfuser_master.firstname + ' ' +cfuser_master.lastname as username " & _
                   "            ,cfuser_master.email " & _
                   "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                   "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                   "        FROM hrmsConfiguration..cfuser_master " & _
                   "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                   "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                   "        WHERE cfuser_master.isactive = 1 " & _
                    "            AND yearunkid = @Y AND privilegeunkid = @P  " & _
                    ") AS Fn ON #USR.mapuserunkid = Fn.userunkid  "

            StrQ &= "WHERE #USR.employeeunkid = " & intEmployeeID & " " & _
                    "AND UM.roleunkid > 0 "

            If decCostAmount >= 0 Then
                StrQ &= " AND TAMX.costamountfrom <= @costamount "
                objDataOperation.AddParameter("@costamount", SqlDbType.Int, eZeeDataType.DECIMAL_SIZE, decCostAmount)
            End If

            StrQ &= " DROP TABLE #USR "

            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)


            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            dtList = dsList.Tables("List").Copy()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextEmployeeApprovers; Module Name: " & mstrModuleName)
        Finally
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dtList
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This user is already map with another level or same level.please select new user to map with this level.")
			Language.setMessage(mstrModuleName, 2, "In Active")
			Language.setMessage(mstrModuleName, 3, "Active")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

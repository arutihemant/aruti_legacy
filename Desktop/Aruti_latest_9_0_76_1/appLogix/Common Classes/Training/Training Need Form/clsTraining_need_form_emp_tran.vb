﻿'************************************************************************************************************************************
'Class Name : clsTraining_need_form_emp_tran.vb
'Purpose    :
'Date       :22-11-2019
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsTraining_need_form_emp_tran
    Private Const mstrModuleName = "clsTraining_need_form_emp_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private xDataOp As clsDataOperation
    Private mintTrainingneedformemptranunkid As Integer
    Private mintTrainingneedformtranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingneedformemptranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Trainingneedformemptranunkid() As Integer
        Get
            Return mintTrainingneedformemptranunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingneedformemptranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingneedformtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Trainingneedformtranunkid() As Integer
        Get
            Return mintTrainingneedformtranunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingneedformtranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingneedformtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property





    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  trainingneedformemptranunkid " & _
              ", trainingneedformtranunkid " & _
              ", employeeunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrtraining_need_form_emp_tran " & _
             "WHERE trainingneedformemptranunkid = @trainingneedformemptranunkid "

            objDataOperation.AddParameter("@trainingneedformemptranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintTrainingneedformempTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                minttrainingneedformemptranunkid = CInt(dtRow.Item("trainingneedformemptranunkid"))
                mintTrainingneedformtranunkid = CInt(dtRow.Item("trainingneedformtranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mintloginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnisweb = CBool(dtRow.Item("isweb"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintvoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intTrainingneedformtranunkid As Integer = 0, Optional ByVal intTrainingneedformunkid As Integer = 0, Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                        "  hrtraining_need_form_emp_tran.trainingneedformemptranunkid " & _
                        ", hrtraining_need_form_emp_tran.trainingneedformtranunkid " & _
                        ", hrtraining_need_form_emp_tran.employeeunkid " & _
                        ", hremployee_master.employeecode " & _
                        ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename " & _
                        ", hrtraining_need_form_emp_tran.userunkid " & _
                        ", hrtraining_need_form_emp_tran.loginemployeeunkid " & _
                        ", hrtraining_need_form_emp_tran.isweb " & _
                        ", hrtraining_need_form_emp_tran.isvoid " & _
                        ", hrtraining_need_form_emp_tran.voiduserunkid " & _
                        ", hrtraining_need_form_emp_tran.voidloginemployeeunkid " & _
                        ", hrtraining_need_form_emp_tran.voiddatetime " & _
                        ", hrtraining_need_form_emp_tran.voidreason " & _
                        ", CAST(0 AS BIT) AS IsChecked " & _
                        ", '' AS AUD " & _
                    "FROM hrtraining_need_form_emp_tran " & _
                        "LEFT JOIN hrtraining_need_form_tran ON hrtraining_need_form_tran.trainingneedformtranunkid = hrtraining_need_form_emp_tran.trainingneedformtranunkid " & _
                        "JOIN hremployee_master ON hremployee_master.employeeunkid = hrtraining_need_form_emp_tran.employeeunkid " & _
                    "WHERE hrtraining_need_form_emp_tran.isvoid = 0 "

            If intTrainingneedformtranunkid > 0 Then
                strQ &= " AND hrtraining_need_form_emp_tran.trainingneedformtranunkid = @trainingneedformtranunkid "
                objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingneedformtranunkid)
            End If

            If intTrainingneedformunkid > 0 Then
                strQ &= " AND hrtraining_need_form_tran.trainingneedformunkid = @trainingneedformunkid "
                objDataOperation.AddParameter("@trainingneedformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingneedformunkid)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtraining_need_form_emp_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformtranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            strQ = "INSERT INTO hrtraining_need_form_emp_tran ( " & _
              "  trainingneedformtranunkid " & _
              ", employeeunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @trainingneedformtranunkid " & _
              ", @employeeunkid " & _
              ", @userunkid " & _
              ", @loginemployeeunkid " & _
              ", @isweb " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidloginemployeeunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrainingneedformempTranUnkId = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrail(objDataOperation, enAuditType.ADD) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAll(ByVal dtTable As DataTable) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            For Each dtRow As DataRow In dtTable.Rows

                If Not (dtRow.Item("AUD").ToString = "A" OrElse dtRow.Item("AUD").ToString = "D") Then Continue For

                If IsDBNull(dtRow.Item("trainingneedformemptranunkid")) = True Then
                    mintTrainingneedformemptranunkid = 0
                Else
                    mintTrainingneedformemptranunkid = CInt(dtRow.Item("trainingneedformemptranunkid"))
                End If
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))

                Me._xDataOp = objDataOperation

                If dtRow.Item("AUD").ToString = "A" Then
                    mblnIsvoid = False
                    mintVoiduserunkid = -1
                    mdtVoiddatetime = Nothing
                    mstrVoidreason = ""
                    mintVoidloginemployeeunkid = -1

                    If Insert() = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                ElseIf dtRow.Item("AUD").ToString = "D" Then
                    mblnIsvoid = True
                    mintVoiduserunkid = mintUserunkid
                    mdtVoiddatetime = mdtAuditDate
                    mstrVoidreason = Language.getMessage(mstrModuleName, 1, "Allocation Removed")
                    mintVoidloginemployeeunkid = mintLoginemployeeunkid
                    Dim intOldUserUnkId As Integer = mintUserunkid
                    Dim intOldLoginemployeeUnkId As Integer = mintLoginemployeeunkid

                    If Delete() = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                    mintUserunkid = intOldUserUnkId
                    mintLoginemployeeunkid = intOldLoginemployeeUnkId
                End If
            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtraining_need_form_emp_tran) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintTrainingneedformemptranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@trainingneedformemptranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttrainingneedformemptranunkid.ToString)
            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformtranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            strQ = "UPDATE hrtraining_need_form_emp_tran SET " & _
              "  trainingneedformtranunkid = @trainingneedformtranunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", userunkid = @userunkid" & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", isweb = @isweb" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE trainingneedformemptranunkid = @trainingneedformemptranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrail(objDataOperation, enAuditType.EDIT) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtraining_need_form_emp_tran) </purpose>
    Public Function Delete() As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnxDataOp As Boolean = False

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
            blnxDataOp = True
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hrtraining_need_form_emp_tran SET " & _
                     " isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid " & _
                     ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voidreason = @voidreason " & _
                   "WHERE trainingneedformemptranunkid = @trainingneedformemptranunkid "

            objDataOperation.AddParameter("@trainingneedformemptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformemptranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._xDataOp = objDataOperation
            Me._Trainingneedformemptranunkid = mintTrainingneedformemptranunkid

            If InsertAuditTrail(objDataOperation, enAuditType.DELETE) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If blnxDataOp = False Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If blnxDataOp = False Then objDataOperation = Nothing
        End Try
    End Function

    Public Function DeleteByTrainingNeedFormTranUnkId() As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnxDataOp As Boolean = False

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
            blnxDataOp = True
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                 " hrtraining_need_form_emp_tran.trainingneedformemptranunkid " & _
            "FROM hrtraining_need_form_emp_tran " & _
            "WHERE hrtraining_need_form_emp_tran.isvoid = 0 " & _
            "AND hrtraining_need_form_emp_tran.trainingneedformtranunkid = @trainingneedformtranunkid "

            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformtranunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dsRow As DataRow In dsList.Tables(0).Rows

                mintTrainingneedformemptranunkid = CInt(dsRow.Item("trainingneedformemptranunkid"))

                Me._xDataOp = objDataOperation
                If Delete() = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            Next

            If blnxDataOp = False Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteByTrainingNeedFormTranUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If blnxDataOp = False Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@trainingneedformemptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  trainingneedformemptranunkid " & _
              ", trainingneedformtranunkid " & _
              ", employeeunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrtraining_need_form_emp_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND trainingneedformemptranunkid <> @trainingneedformemptranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@trainingneedformemptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrail(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@trainingneedformemptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformemptranunkid.ToString)
            objDataOperation.AddParameter("@trainingneedformtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedformtranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)

            strQ = "INSERT INTO athrtraining_need_form_emp_tran ( " & _
                            "  trainingneedformemptranunkid " & _
                            ", trainingneedformtranunkid " & _
                            ", employeeunkid " & _
                            ", audituserunkid " & _
                            ", loginemployeeunkid " & _
                            ", audittype " & _
                            ", auditdatetime " & _
                            ", isweb " & _
                            ", ip " & _
                            ", host " & _
                            ", form_name " & _
                  ") VALUES (" & _
                            "  @trainingneedformemptranunkid " & _
                            ", @trainingneedformtranunkid " & _
                            ", @employeeunkid " & _
                            ", @audituserunkid " & _
                            ", @loginemployeeunkid " & _
                            ", @audittype " & _
                            ", @auditdatetime " & _
                            ", @isweb " & _
                            ", @ip " & _
                            ", @host " & _
                            ", @form_name " & _
                  "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

End Class
﻿'************************************************************************************************************************************
'Class Name : clsTraining_Approver_Tran
'Purpose    :
'Date       : 02-Feb-2021
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>

Public Class clsTraining_Approver_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsTraining_Approver_tran"
    Dim mstrMessage As String = ""

#Region "Private Variables"

    Private minttrApproverTranunkid As Integer = 0
    Private minttrApproverunkid As Integer = 0
    Private mintEmployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtTran As DataTable
    Private mintUserId As Integer = 0
    Private objDataOperation As clsDataOperation
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mblnIsWeb As Boolean = False
#End Region

#Region "Properties"

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    '' <summary>
    '' Purpose: Get or Set WebFormName
    '' Modify By: Hemant
    '' </summary>
    '' 

    Public Property _TrainingApproverTranunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        Get
            Return minttrApproverTranunkid
        End Get
        Set(ByVal value As Integer)
            minttrApproverTranunkid = value
            Call GetData(objDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingapproverunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TrainingApproverunkid() As Integer
        Get
            Return minttrApproverunkid
        End Get
        Set(ByVal value As Integer)
            minttrApproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TranDataTable
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TranDataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set UserId
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _UserId() As Integer
        Get
            Return mintUserId
        End Get
        Set(ByVal value As Integer)
            mintUserId = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

#End Region

#Region "Constructor"

    Public Sub New()
        mdtTran = New DataTable("List")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("ischeck")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trapprovertranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trapproverunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ecode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("edept")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ejob")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Public Methods"

    '' <summary>
    '' Modify By: Hemant
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    '' 
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        objDataOperation.ClearParameters()
        Try
            strQ = " SELECT " & _
                    "  CAST (0 AS BIT) AS ischeck " & _
                    " ,trtraining_approver_tran.trapprovertranunkid " & _
                    " ,trtraining_approver_tran.trapproverunkid " & _
                    " ,trtraining_approver_tran.employeeunkid " & _
                    " ,trtraining_approver_tran.isvoid " & _
                    " ,trtraining_approver_tran.voiddatetime " & _
                    " ,trtraining_approver_tran.voiduserunkid " & _
                    " ,trtraining_approver_tran.voidreason " & _
                    " ,'' AS AUD " & _
                    " ,hremployee_master.employeecode AS ecode " & _
                    " ,hremployee_master.firstname +' '+ hremployee_master.surname AS ename  " & _
                    " ,hrdepartment_master.name AS edept " & _
                    " ,hrjob_master.job_name AS ejob " & _
                " FROM trtraining_approver_tran " & _
                    " JOIN hremployee_master ON trtraining_approver_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                    " JOIN hrtraining_approver_master ON trtraining_approver_tran.trapproverunkid = hrtraining_approver_master.mappingunkid " & _
                " WHERE hrtraining_approver_master.mappingunkid = '" & minttrApproverunkid & "' AND trtraining_approver_tran.isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()
            For Each drRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(drRow)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetData", mstrModuleName)
        Finally
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try

    End Sub

    '' <summary>
    '' Modify By: Hemant
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    '' 
    Public Sub GetDataByUnkId(ByVal inttrApproverTranunkid As Integer, Optional ByVal objDataOp As clsDataOperation = Nothing)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        objDataOperation.ClearParameters()
        Try
            strQ = " SELECT " & _
                    " trapprovertranunkid " & _
                    " ,trapproverunkid " & _
                    " ,employeeunkid " & _
                    " ,isvoid " & _
                    " ,voiddatetime " & _
                    " ,voiduserunkid " & _
                    " ,voidreason " & _
                " FROM trtraining_approver_tran " & _
                " WHERE isvoid = 0 " & _
                "       AND trapprovertranunkid = @trapprovertranunkid "

            objDataOperation.AddParameter("@trapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, inttrApproverTranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                minttrApproverTranunkid = CInt(dtRow.Item("trapprovertranunkid"))
                minttrApproverunkid = CInt(dtRow.Item("trapproverunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetData", mstrModuleName)
        Finally
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try

    End Sub


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function Insert_Update_Delete(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean

        Dim i As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
            objDataOperation.ClearParameters()
        End If

        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)

                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = " INSERT INTO trtraining_approver_tran (" & _
                                            "  trapproverunkid " & _
                                            " ,employeeunkid " & _
                                            " ,isvoid " & _
                                            " ,voiddatetime " & _
                                            " ,voiduserunkid " & _
                                            " ,voidreason " & _
                                        ") VALUES (" & _
                                            "  @trapproverunkid " & _
                                            " ,@employeeunkid " & _
                                            " ,@isvoid " & _
                                            " ,@voiddatetime " & _
                                            " ,@voiduserunkid " & _
                                            " ,@voidreason " & _
                                        "); SELECT @@identity "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@trapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minttrApproverunkid)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                minttrApproverTranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If InsertATApprover_Tran(objDataOperation, 1, CInt(.Item("employeeunkid"))) = False Then
                                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                            Case "U"
                                strQ = "UPDATE trtraining_approver_tran SET " & _
                                            "  trapproverunkid = @trapproverunkid " & _
                                            " ,employeeunkid = @ employeeunkid " & _
                                            " ,isvoid = @ isvoid " & _
                                            " ,voiddatetime = @ voiddatetime " & _
                                            " ,voiduserunkid = @ voiduserunkid " & _
                                            " ,voidreason = @ voidreason " & _
                                        "WHERE trapprovertranunkid = @ trapprovertranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@trapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minttrApproverunkid)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@trapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trapprovertranunkid"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                minttrApproverTranunkid = CInt(.Item("trapprovertranunkid"))

                                If InsertATApprover_Tran(objDataOperation, 2, CInt(.Item("employeeunkid"))) = False Then
                                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                GetDataByUnkId(.Item("trapprovertranunkid"), objDataOperation)

                                strQ = "UPDATE trtraining_approver_tran SET " & _
                                            " isvoid = @isvoid " & _
                                            " ,voiddatetime = @voiddatetime " & _
                                            " ,voiduserunkid = @voiduserunkid " & _
                                            " ,voidreason = @voidreason " & _
                                        "WHERE trapprovertranunkid = @trapprovertranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@trapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trapprovertranunkid"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If InsertATApprover_Tran(objDataOperation, 3, CInt(.Item("employeeunkid"))) = False Then
                                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If

                End With
            Next
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert_Update_Delete , Module Name: " & mstrModuleName)
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function IsUserMapped(ByVal iUserId As Integer, ByRef sMsg As String) As Boolean
        Dim strQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Dim objData As New clsDataOperation

            strQ = "SELECT " & _
                   "	 hremployee_master.employeecode AS ECode " & _
                   "	,hremployee_master.firstname +' '+ hremployee_master.surname AS EName " & _
                   "FROM trtraining_approver_mapping " & _
                   "	JOIN hrtraining_approver_master ON trtraining_approver_mapping.approvertranunkid = hrtraining_approver_master.trapproverunkid " & _
                   "	JOIN hremployee_master ON hrtraining_approver_master.approverempunkid = hremployee_master.employeeunkid " & _
                   "WHERE  trtraining_approver_mapping.userunkid = '" & iUserId & "' "

            dsList = objData.ExecQuery(strQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                sMsg = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot map selected user with this employee." & vbCrLf & _
                                           "Reason user is already mapped with [ Code : ") & dsList.Tables(0).Rows(0).Item("ECode") & _
                Language.getMessage(mstrModuleName, 2, " Employee : ") & dsList.Tables(0).Rows(0).Item("EName") & Language.getMessage(mstrModuleName, 3, " ]. Please select new user to map.")
                Return True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsUserMapped", mstrModuleName)
            Return True
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function InsertATApprover_Tran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal intEmployeeId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minttrapproverTranunkid.ToString)
            objDataOperation.AddParameter("@trapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minttrapproverunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
           

            strQ = "INSERT INTO attrtraining_approver_tran (" & _
                        "  tranguid " & _
                        ",trapprovertranunkid " & _
                        ",trapproverunkid " & _
                        ",employeeunkid " & _
                        ",audittypeid " & _
                        ",audituserunkid " & _
                        ",auditdatetime " & _
                        ",ip " & _
                        ",host " & _
                        ",form_name " & _
                        ",isweb " & _
                    ") VALUES (" & _
                        "  LOWER(NEWID()) " & _
                        ", @trapprovertranunkid " & _
                        ", @trapproverunkid " & _
                        ", @employeeunkid " & _
                        ", @audittypeid " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @host " & _
                        ", @form_name " & _
                        ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATApprover_Tran , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    Public Function GetApproverTranIdFromEmployeeAndApprover(ByVal ApproverId As Integer, ByVal EmployeeId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = " SELECT ISNULL(trapprovertranunkid,0) as trapprovertranunkid FROM trtraining_approver_tran WHERE trapproverunkid = @approverunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ApproverId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, EmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables("List").Rows(0)("trapprovertranunkid").ToString())
            End If

            Return 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverTranIdFromEmployeeAndApprover; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function GetApproverData(ByVal EmployeeId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = " SELECT " & _
                       "  trtraining_approver_tran.trapprovertranunkid " & _
                       ", trtraining_approver_tran.trapproverunkid " & _
                       ", trtraining_approver_tran.employeeunkid " & _
                       ", hrtraining_approver_master.mappingunkid " & _
                       ", hrtraining_approver_master.levelunkid " & _
                       ", hrtraining_approver_master.mapuserunkid " & _
                       ", hrtraining_approverlevel_master.levelname " & _
                       ", hrtraining_approverlevel_master.priority " & _
                       ", ISNULL(hrmsConfiguration..cfuser_master.username,'') AS Approver " & _
                       ", ISNULL(hrmsConfiguration..cfuser_master.email,'') AS App_Email " & _
                    " FROM trtraining_approver_tran " & _
                    " JOIN hrtraining_approver_master " & _
                        " ON hrtraining_approver_master.mappingunkid = trtraining_approver_tran.trapproverunkid " & _
                            " AND hrtraining_approver_master.isvoid = 0 " & _
                            " AND hrtraining_approver_master.isactive = 1 " & _
                    " JOIN hrtraining_approverlevel_master " & _
                        " ON hrtraining_approverlevel_master.levelunkid = hrtraining_approver_master.levelunkid " & _
                            " AND hrtraining_approverlevel_master.isactive = 1 " & _
                    " LEFT JOIN hrmsConfiguration..cfuser_master on hrmsConfiguration..cfuser_master.userunkid = hrtraining_approver_master.mapuserunkid " & _
                    " WHERE trtraining_approver_tran.isvoid = 0 " & _
                    " AND trtraining_approver_tran.employeeunkid = @employeeunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, EmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    
#End Region
End Class

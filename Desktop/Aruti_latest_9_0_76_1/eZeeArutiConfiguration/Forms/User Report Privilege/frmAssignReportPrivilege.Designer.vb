﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssignReportPrivilege
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssignReportPrivilege))
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbReportPrivilege = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objChkAllReports = New System.Windows.Forms.CheckBox
        Me.objStline1 = New eZee.Common.eZeeStraightLine
        Me.txtUser = New eZee.TextBox.AlphanumericTextBox
        Me.lblUser = New System.Windows.Forms.Label
        Me.objStLine2 = New eZee.Common.eZeeStraightLine
        Me.lblSelectCompany = New System.Windows.Forms.Label
        Me.cboCompany = New System.Windows.Forms.ComboBox
        Me.objbtnMoveLeft = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnMoveRight = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvAssignedReports = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhACheck = New System.Windows.Forms.ColumnHeader
        Me.colhAssignedReports = New System.Windows.Forms.ColumnHeader
        Me.objcolhCompanyName = New System.Windows.Forms.ColumnHeader
        Me.objcolhCompanyId = New System.Windows.Forms.ColumnHeader
        Me.lvReports = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhReports = New System.Windows.Forms.ColumnHeader
        Me.objcolhCategory = New System.Windows.Forms.ColumnHeader
        Me.objcolhCatId = New System.Windows.Forms.ColumnHeader
        Me.objefFormFooter.SuspendLayout()
        Me.gbReportPrivilege.SuspendLayout()
        Me.SuspendLayout()
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnSave)
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 372)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(672, 55)
        Me.objefFormFooter.TabIndex = 4
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(474, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Final Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(570, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'gbReportPrivilege
        '
        Me.gbReportPrivilege.BorderColor = System.Drawing.Color.Black
        Me.gbReportPrivilege.Checked = False
        Me.gbReportPrivilege.CollapseAllExceptThis = False
        Me.gbReportPrivilege.CollapsedHoverImage = Nothing
        Me.gbReportPrivilege.CollapsedNormalImage = Nothing
        Me.gbReportPrivilege.CollapsedPressedImage = Nothing
        Me.gbReportPrivilege.CollapseOnLoad = False
        Me.gbReportPrivilege.Controls.Add(Me.objChkAllReports)
        Me.gbReportPrivilege.Controls.Add(Me.objStline1)
        Me.gbReportPrivilege.Controls.Add(Me.txtUser)
        Me.gbReportPrivilege.Controls.Add(Me.lblUser)
        Me.gbReportPrivilege.Controls.Add(Me.objStLine2)
        Me.gbReportPrivilege.Controls.Add(Me.lblSelectCompany)
        Me.gbReportPrivilege.Controls.Add(Me.cboCompany)
        Me.gbReportPrivilege.Controls.Add(Me.objbtnMoveLeft)
        Me.gbReportPrivilege.Controls.Add(Me.objbtnMoveRight)
        Me.gbReportPrivilege.Controls.Add(Me.lvAssignedReports)
        Me.gbReportPrivilege.Controls.Add(Me.lvReports)
        Me.gbReportPrivilege.ExpandedHoverImage = Nothing
        Me.gbReportPrivilege.ExpandedNormalImage = Nothing
        Me.gbReportPrivilege.ExpandedPressedImage = Nothing
        Me.gbReportPrivilege.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbReportPrivilege.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbReportPrivilege.HeaderHeight = 25
        Me.gbReportPrivilege.HeaderMessage = ""
        Me.gbReportPrivilege.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbReportPrivilege.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbReportPrivilege.HeightOnCollapse = 0
        Me.gbReportPrivilege.LeftTextSpace = 0
        Me.gbReportPrivilege.Location = New System.Drawing.Point(12, 12)
        Me.gbReportPrivilege.Name = "gbReportPrivilege"
        Me.gbReportPrivilege.OpenHeight = 300
        Me.gbReportPrivilege.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbReportPrivilege.ShowBorder = True
        Me.gbReportPrivilege.ShowCheckBox = False
        Me.gbReportPrivilege.ShowCollapseButton = False
        Me.gbReportPrivilege.ShowDefaultBorderColor = True
        Me.gbReportPrivilege.ShowDownButton = False
        Me.gbReportPrivilege.ShowHeader = True
        Me.gbReportPrivilege.Size = New System.Drawing.Size(649, 354)
        Me.gbReportPrivilege.TabIndex = 5
        Me.gbReportPrivilege.Temp = 0
        Me.gbReportPrivilege.Text = "Assign Privilege"
        Me.gbReportPrivilege.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objChkAllReports
        '
        Me.objChkAllReports.AutoSize = True
        Me.objChkAllReports.Location = New System.Drawing.Point(15, 74)
        Me.objChkAllReports.Name = "objChkAllReports"
        Me.objChkAllReports.Size = New System.Drawing.Size(15, 14)
        Me.objChkAllReports.TabIndex = 2
        Me.objChkAllReports.UseVisualStyleBackColor = True
        '
        'objStline1
        '
        Me.objStline1.BackColor = System.Drawing.Color.Transparent
        Me.objStline1.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.objStline1.Location = New System.Drawing.Point(8, 57)
        Me.objStline1.Name = "objStline1"
        Me.objStline1.Size = New System.Drawing.Size(282, 10)
        Me.objStline1.TabIndex = 22
        Me.objStline1.Text = "EZeeStraightLine2"
        '
        'txtUser
        '
        Me.txtUser.Flags = 0
        Me.txtUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUser.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtUser.Location = New System.Drawing.Point(84, 36)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.ReadOnly = True
        Me.txtUser.Size = New System.Drawing.Size(206, 21)
        Me.txtUser.TabIndex = 21
        '
        'lblUser
        '
        Me.lblUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.Location = New System.Drawing.Point(8, 38)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(70, 16)
        Me.lblUser.TabIndex = 20
        Me.lblUser.Text = "User"
        Me.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objStLine2
        '
        Me.objStLine2.BackColor = System.Drawing.Color.Transparent
        Me.objStLine2.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.objStLine2.Location = New System.Drawing.Point(359, 57)
        Me.objStLine2.Name = "objStLine2"
        Me.objStLine2.Size = New System.Drawing.Size(282, 10)
        Me.objStLine2.TabIndex = 19
        Me.objStLine2.Text = "EZeeStraightLine1"
        '
        'lblSelectCompany
        '
        Me.lblSelectCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectCompany.Location = New System.Drawing.Point(359, 38)
        Me.lblSelectCompany.Name = "lblSelectCompany"
        Me.lblSelectCompany.Size = New System.Drawing.Size(94, 16)
        Me.lblSelectCompany.TabIndex = 17
        Me.lblSelectCompany.Text = "Select Company "
        Me.lblSelectCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(459, 36)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(182, 21)
        Me.cboCompany.TabIndex = 18
        '
        'objbtnMoveLeft
        '
        Me.objbtnMoveLeft.BackColor = System.Drawing.Color.White
        Me.objbtnMoveLeft.BackgroundImage = CType(resources.GetObject("objbtnMoveLeft.BackgroundImage"), System.Drawing.Image)
        Me.objbtnMoveLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnMoveLeft.BorderColor = System.Drawing.Color.Empty
        Me.objbtnMoveLeft.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnMoveLeft.FlatAppearance.BorderSize = 0
        Me.objbtnMoveLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnMoveLeft.ForeColor = System.Drawing.Color.Black
        Me.objbtnMoveLeft.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnMoveLeft.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnMoveLeft.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnMoveLeft.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnMoveLeft.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Previous_24x24
        Me.objbtnMoveLeft.Location = New System.Drawing.Point(305, 191)
        Me.objbtnMoveLeft.Name = "objbtnMoveLeft"
        Me.objbtnMoveLeft.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnMoveLeft.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnMoveLeft.Size = New System.Drawing.Size(38, 36)
        Me.objbtnMoveLeft.TabIndex = 5
        Me.objbtnMoveLeft.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.objbtnMoveLeft.UseVisualStyleBackColor = True
        '
        'objbtnMoveRight
        '
        Me.objbtnMoveRight.BackColor = System.Drawing.Color.White
        Me.objbtnMoveRight.BackgroundImage = CType(resources.GetObject("objbtnMoveRight.BackgroundImage"), System.Drawing.Image)
        Me.objbtnMoveRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnMoveRight.BorderColor = System.Drawing.Color.Empty
        Me.objbtnMoveRight.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnMoveRight.FlatAppearance.BorderSize = 0
        Me.objbtnMoveRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnMoveRight.ForeColor = System.Drawing.Color.Black
        Me.objbtnMoveRight.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnMoveRight.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnMoveRight.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnMoveRight.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnMoveRight.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Next_24x24
        Me.objbtnMoveRight.Location = New System.Drawing.Point(305, 149)
        Me.objbtnMoveRight.Name = "objbtnMoveRight"
        Me.objbtnMoveRight.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnMoveRight.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnMoveRight.Size = New System.Drawing.Size(38, 36)
        Me.objbtnMoveRight.TabIndex = 3
        Me.objbtnMoveRight.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.objbtnMoveRight.UseVisualStyleBackColor = True
        '
        'lvAssignedReports
        '
        Me.lvAssignedReports.BackColorOnChecked = False
        Me.lvAssignedReports.CheckBoxes = True
        Me.lvAssignedReports.ColumnHeaders = Nothing
        Me.lvAssignedReports.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhACheck, Me.colhAssignedReports, Me.objcolhCompanyName, Me.objcolhCompanyId})
        Me.lvAssignedReports.CompulsoryColumns = ""
        Me.lvAssignedReports.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAssignedReports.FullRowSelect = True
        Me.lvAssignedReports.GridLines = True
        Me.lvAssignedReports.GroupingColumn = Nothing
        Me.lvAssignedReports.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAssignedReports.HideSelection = False
        Me.lvAssignedReports.Location = New System.Drawing.Point(359, 69)
        Me.lvAssignedReports.MinColumnWidth = 50
        Me.lvAssignedReports.MultiSelect = False
        Me.lvAssignedReports.Name = "lvAssignedReports"
        Me.lvAssignedReports.OptionalColumns = ""
        Me.lvAssignedReports.ShowMoreItem = False
        Me.lvAssignedReports.ShowSaveItem = False
        Me.lvAssignedReports.ShowSelectAll = True
        Me.lvAssignedReports.ShowSizeAllColumnsToFit = True
        Me.lvAssignedReports.Size = New System.Drawing.Size(282, 279)
        Me.lvAssignedReports.Sortable = True
        Me.lvAssignedReports.TabIndex = 2
        Me.lvAssignedReports.UseCompatibleStateImageBehavior = False
        Me.lvAssignedReports.View = System.Windows.Forms.View.Details
        '
        'objcolhACheck
        '
        Me.objcolhACheck.Tag = "objcolhCheck"
        Me.objcolhACheck.Text = ""
        Me.objcolhACheck.Width = 25
        '
        'colhAssignedReports
        '
        Me.colhAssignedReports.Tag = "colhAssignedReports"
        Me.colhAssignedReports.Text = "Reports"
        Me.colhAssignedReports.Width = 230
        '
        'objcolhCompanyName
        '
        Me.objcolhCompanyName.Tag = "objcolhCompanyName"
        Me.objcolhCompanyName.Text = "objcolhCompanyName"
        Me.objcolhCompanyName.Width = 0
        '
        'objcolhCompanyId
        '
        Me.objcolhCompanyId.Tag = "objcolhCompanyId"
        Me.objcolhCompanyId.Text = "objcolhCompanyId"
        Me.objcolhCompanyId.Width = 0
        '
        'lvReports
        '
        Me.lvReports.BackColorOnChecked = False
        Me.lvReports.CheckBoxes = True
        Me.lvReports.ColumnHeaders = Nothing
        Me.lvReports.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhReports, Me.objcolhCategory, Me.objcolhCatId})
        Me.lvReports.CompulsoryColumns = ""
        Me.lvReports.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvReports.FullRowSelect = True
        Me.lvReports.GridLines = True
        Me.lvReports.GroupingColumn = Nothing
        Me.lvReports.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvReports.HideSelection = False
        Me.lvReports.Location = New System.Drawing.Point(8, 69)
        Me.lvReports.MinColumnWidth = 50
        Me.lvReports.MultiSelect = False
        Me.lvReports.Name = "lvReports"
        Me.lvReports.OptionalColumns = ""
        Me.lvReports.ShowMoreItem = False
        Me.lvReports.ShowSaveItem = False
        Me.lvReports.ShowSelectAll = True
        Me.lvReports.ShowSizeAllColumnsToFit = True
        Me.lvReports.Size = New System.Drawing.Size(282, 279)
        Me.lvReports.Sortable = True
        Me.lvReports.TabIndex = 1
        Me.lvReports.UseCompatibleStateImageBehavior = False
        Me.lvReports.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 25
        '
        'colhReports
        '
        Me.colhReports.Tag = "colhReports"
        Me.colhReports.Text = "Reports"
        Me.colhReports.Width = 230
        '
        'objcolhCategory
        '
        Me.objcolhCategory.Tag = "objcolhCategory"
        Me.objcolhCategory.Text = ""
        Me.objcolhCategory.Width = 0
        '
        'objcolhCatId
        '
        Me.objcolhCatId.Tag = "objcolhCatId"
        Me.objcolhCatId.Text = "objcolhCatId"
        Me.objcolhCatId.Width = 0
        '
        'frmAssignReportPrivilege
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(672, 427)
        Me.Controls.Add(Me.gbReportPrivilege)
        Me.Controls.Add(Me.objefFormFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssignReportPrivilege"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Report Privilege"
        Me.objefFormFooter.ResumeLayout(False)
        Me.gbReportPrivilege.ResumeLayout(False)
        Me.gbReportPrivilege.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents gbReportPrivilege As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lvReports As eZee.Common.eZeeListView
    Friend WithEvents colhReports As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvAssignedReports As eZee.Common.eZeeListView
    Friend WithEvents colhAssignedReports As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnMoveRight As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnMoveLeft As eZee.Common.eZeeLightButton
    Friend WithEvents lblSelectCompany As System.Windows.Forms.Label
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents objStline1 As eZee.Common.eZeeStraightLine
    Friend WithEvents txtUser As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents objStLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objcolhCompanyName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhCompanyId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhCatId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhACheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents objChkAllReports As System.Windows.Forms.CheckBox
End Class

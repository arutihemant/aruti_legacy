﻿Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmCompanyOtherCaptions

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmCompanyOtherCaptions"
    Dim lblCaption1 As String = String.Empty
    Dim lblCaption2 As String = String.Empty
    Dim lblCaption3 As String = String.Empty
    Private mblnCancel As Boolean = True
#End Region

#Region " Display Dialog "
    Public Sub displayDialog(ByRef strCaption1 As String, ByRef strCaption2 As String, ByRef strCaption3 As String)
        Try
            lblCaption1 = strCaption1
            lblCaption2 = strCaption2
            lblCaption3 = strCaption3

            Me.ShowDialog()

            If Not mblnCancel Then
                strCaption1 = lblCaption1
                strCaption2 = lblCaption2
                strCaption3 = lblCaption3
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " From's Events "
    Private Sub frmCompanyOtherCaptions_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            ChkCaption1.Checked = True
            ChkCaption2.Checked = True
            ChkCaption3.Checked = True

            txtRegCaption1.Text = lblCaption1
            If txtRegCaption1.Text.Trim = "" Then ChkCaption1.Checked = False Else ChkCaption1.Checked = True

            txtRegCaption2.Text = lblCaption2
            If txtRegCaption2.Text.Trim = "" Then ChkCaption2.Checked = False Else ChkCaption2.Checked = True

            txtRegCaption3.Text = lblCaption3
            If txtRegCaption3.Text.Trim = "" Then ChkCaption3.Checked = False Else ChkCaption3.Checked = True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCompanyOtherCaptions_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Other Controls "
    Private Sub ChkCaption3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkCaption3.CheckedChanged
        If ChkCaption3.Checked = True Then
            txtRegCaption3.Enabled = True
        Else
            txtRegCaption3.Text = ""
            txtRegCaption3.Enabled = False
        End If
    End Sub

    Private Sub ChkCaption2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkCaption2.CheckedChanged
        If ChkCaption2.Checked = True Then
            txtRegCaption2.Enabled = True
        Else
            txtRegCaption2.Text = ""
            txtRegCaption2.Enabled = False
        End If
    End Sub

    Private Sub ChkCaption1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkCaption1.CheckedChanged
        If ChkCaption1.Checked = True Then
            txtRegCaption1.Enabled = True
        Else
            txtRegCaption1.Text = ""
            txtRegCaption1.Enabled = False
        End If
    End Sub
#End Region

#Region " Buttons "
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            lblCaption1 = txtRegCaption1.Text
            lblCaption2 = txtRegCaption2.Text
            lblCaption3 = txtRegCaption3.Text
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbOtherRegCaptions.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbOtherRegCaptions.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor

			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.gbOtherRegCaptions.Text = Language._Object.getCaption(Me.gbOtherRegCaptions.Name, Me.gbOtherRegCaptions.Text)
			Me.ChkCaption3.Text = Language._Object.getCaption(Me.ChkCaption3.Name, Me.ChkCaption3.Text)
			Me.ChkCaption2.Text = Language._Object.getCaption(Me.ChkCaption2.Name, Me.ChkCaption2.Text)
			Me.ChkCaption1.Text = Language._Object.getCaption(Me.ChkCaption1.Name, Me.ChkCaption1.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
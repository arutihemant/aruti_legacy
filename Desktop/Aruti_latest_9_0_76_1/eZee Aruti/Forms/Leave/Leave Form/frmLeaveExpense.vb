﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmLeaveExpense

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmLeaveExpense"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objLeaveExpense As clsleaveexpense
    Private mintFormunkid As Integer
    Private dtExpense As DataTable
    Private _Expense As DataTable


    'Pinkal (14-Dec-2012) -- Start
    'Enhancement : TRA Changes
    Private mintEmployeeID As Integer = 0
    'Pinkal (14-Dec-2012) -- End


    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    Dim mdtDate As Date = Nothing
    'Pinkal (21-Jul-2014) -- End



#End Region

#Region "Property"

    Public Property _dtExpense() As DataTable
        Get
            Return dtExpense
        End Get
        Set(ByVal value As DataTable)
            dtExpense = value
        End Set
    End Property

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal _Formunkid As Integer, ByVal _dtExpense As DataTable, ByVal _EmpId As Integer, ByVal dtDate As Date, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            mintFormunkid = _Formunkid
            _Expense = _dtExpense

            'Pinkal (14-Dec-2012) -- Start
            'Enhancement : TRA Changes
            mintEmployeeID = _EmpId
            'Pinkal (14-Dec-2012) -- End


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            mdtDate = dtDate
            'Pinkal (21-Jul-2014) -- End


            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub setColor()
        Try
            txtDescription.BackColor = GUI.ColorComp
            nudQty.BackColor = GUI.ColorComp
            txtUnitCost.BackColor = GUI.ColorOptional
            txtAmount.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If txtDescription.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Description cannot be blank.Description is required information."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtDescription.Select()
                Return False

            ElseIf txtAmount.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Amount cannot be blank or 0.Amount is required information."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtAmount.Select()
                Return False

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub ClearControl()
        Try
            txtDescription.Text = ""
            nudQty.Value = 1
            txtUnitCost.Text = ""
            txtAmount.Text = ""
            txtDescription.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearControl", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGrid()
        Try

            If dtExpense IsNot Nothing Then

                Dim dtFilter As DataTable = New DataView(dtExpense, "AUD<>'D'", "", DataViewRowState.CurrentRows).ToTable()

                dgExpense.AutoGenerateColumns = False

                dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency
                dgcolhUnitcost.DefaultCellStyle.Format = GUI.fmtCurrency
                dgcolhQty.DefaultCellStyle.Format = "#0.00"

                dgcolhQty.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                dgcolhUnitcost.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                dgcolhAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

                dgExpense.DataSource = dtFilter

                If dtFilter.Rows.Count > 0 Then
                    txtTotalAmount.Text = Format(dtFilter.Compute("SUM(amount)", "1=1"), GUI.fmtCurrency)
                Else
                    txtTotalAmount.Text = Format(0, GUI.fmtCurrency)
                End If

                dgcolhDescription.DataPropertyName = "description"
                dgcolhQty.DataPropertyName = "qty"
                dgcolhUnitcost.DataPropertyName = "unitcost"
                dgcolhAmount.DataPropertyName = "amount"
                objdgcolhExpenseId.DataPropertyName = "lvexpenseunkid"

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Private Sub frmLeaveExpense_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            objLeaveExpense = New clsleaveexpense

            txtUnitCost.Text = Format(0, GUI.fmtCurrency)
            setColor()


            'Pinkal (19-Nov-2012) -- Start
            'Enhancement : TRA Changes
            btnAdd.Enabled = User._Object.Privilege._AddLeaveExpense
            btnEdit.Enabled = User._Object.Privilege._EditLeaveExpense
            btnDelete.Enabled = User._Object.Privilege._DeleteLeaveExpense
            'Pinkal (19-Nov-2012) -- End


            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes

            If menAction = enAction.EDIT_ONE Then
                If _Expense IsNot Nothing Then

                    If _Expense.Rows.Count > 0 Then
                        dtExpense = _Expense
                    Else
                        dtExpense = objLeaveExpense.GetList("List", mintFormunkid, True).Tables(0)
                    End If

                Else
                dtExpense = objLeaveExpense.GetList("List", mintFormunkid, True).Tables(0)
                End If
            Else
                If mintFormunkid <= 0 Then
                    If _Expense IsNot Nothing Then
                        dtExpense = _Expense
            Else
                dtExpense = objLeaveExpense.GetList("List", mintFormunkid, True).Tables(0).Clone
            End If
                End If
            End If
            FillGrid()

            'Pinkal (05-Mar-2012) -- End


            'Pinkal (14-Dec-2012) -- Start
            'Enhancement : TRA Changes

            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = mintEmployeeID
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = mintEmployeeID
            'S.SANDEEP [04 JUN 2015] -- END


            Dim objState As New clsstate_master
            objState._Stateunkid = objEmployee._Domicile_Stateunkid

            Dim mstrCountry As String = ""
            Dim objCountry As New clsMasterData
            Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
            If dsCountry.Tables("List").Rows.Count > 0 Then
                mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
            End If

            Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                       IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                       IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                       IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                       mstrCountry

            txtDomicileAddress.Text = strAddress

            'Pinkal (14-Dec-2012) -- End


            txtDescription.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveExpense_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeaveExpense_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveExpense_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsleaveexpense.SetMessages()
            'objfrm._Other_ModuleNames = "clsleaveexpense"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try

            If Validation() Then

                Dim dr As DataRow() = dtExpense.Select("description='" & txtDescription.Text.Trim & "' AND qty = " & CDec(nudQty.Value) & " AND unitcost = " & CDec(txtUnitCost.Decimal) & " AND amount = " & CDec(txtAmount.Decimal) & _
                                                   " AND formunkid = " & mintFormunkid)

                If dr.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "This Expense is already exist for this leave form.Please define new leave expense."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

                Dim drRow As DataRow = dtExpense.NewRow()
                drRow("description") = txtDescription.Text.Trim
                drRow("qty") = nudQty.Value
                drRow("unitcost") = Format(CDec(txtUnitCost.Decimal), GUI.fmtCurrency)
                drRow("amount") = Format(CDec(txtAmount.Decimal), GUI.fmtCurrency)
                drRow("formunkid") = mintFormunkid
                drRow("lvexpenseunkid") = -1
                drRow("AUD") = "A"
                drRow("GUID") = Guid.NewGuid.ToString()
                dtExpense.Rows.Add(drRow)

                FillGrid()

                ClearControl()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try

            If Validation() Then

                If dtExpense Is Nothing Then Exit Sub

                Dim dr As DataRow() = dtExpense.Select("description='" & txtDescription.Text.Trim & "' AND qty = " & CDec(nudQty.Value) & " AND unitcost = " & CDec(txtUnitCost.Decimal) & " AND amount = " & CDec(txtAmount.Decimal) & _
                                                  " AND formunkid = " & mintFormunkid & " AND lvexpenseunkid <> " & CInt(txtDescription.Tag))

                If dr.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "This Expense is already exist for this leave form.Please define new leave expense."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

                Dim drRow As DataRow() = dtExpense.Select("lvexpenseunkid=" & CInt(txtDescription.Tag))

                If drRow.Length > 0 Then
                    drRow(0)("description") = txtDescription.Text.Trim
                    drRow(0)("qty") = nudQty.Value
                    drRow(0)("unitcost") = Format(txtUnitCost.Decimal, GUI.fmtCurrency)
                    drRow(0)("amount") = Format(txtAmount.Decimal, GUI.fmtCurrency)
                    drRow(0)("AUD") = "U"
                    drRow(0).AcceptChanges()

                    ClearControl()
                    FillGrid()

                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are You sure you want to delete this expense ?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                If dtExpense Is Nothing Then Exit Sub

                Dim drRow As DataRow() = dtExpense.Select("lvexpenseunkid=" & CInt(txtDescription.Tag))

                If drRow.Length > 0 Then
                    drRow(0)("AUD") = "D"
                    drRow(0).AcceptChanges()
                    FillGrid()
                    ClearControl()
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub BtnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOk.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "BtnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (14-Dec-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub btnDependentClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDependentClose.Click
        Try
            gbLeaveExpense.Enabled = True
            pnlExpense.Enabled = True
            objFooter.Enabled = True
            gbDependantsList.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDependentClose_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (14-Dec-2012) -- End


#End Region

#Region " DataGrid Event(s) "

    Private Sub dgExpense_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgExpense.CellClick
        Try

            If e.RowIndex < 0 Then Exit Sub

            txtDescription.Tag = CInt(dgExpense.Rows(e.RowIndex).Cells(objdgcolhExpenseId.Index).Value.ToString())
            txtDescription.Text = dgExpense.Rows(e.RowIndex).Cells(dgcolhDescription.Index).Value.ToString()
            nudQty.Value = CDec(dgExpense.Rows(e.RowIndex).Cells(dgcolhQty.Index).Value.ToString())
            txtUnitCost.Text = Format(dgExpense.Rows(e.RowIndex).Cells(dgcolhUnitcost.Index).Value, GUI.fmtCurrency)
            txtAmount.Text = Format(dgExpense.Rows(e.RowIndex).Cells(dgcolhAmount.Index).Value, GUI.fmtCurrency)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgExpense_CellClick", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (05-Mar-2012) -- Start
    'Enhancement : TRA Changes
#Region "TextBox Event"

    Private Sub nudQty_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudQty.ValueChanged, txtUnitCost.TextChanged
        Try
            Dim mdblAmount As Decimal = nudQty.Value * CDec(IIf(txtUnitCost.Text.Trim.Length = 0, 0, txtUnitCost.Text))
            txtAmount.Text = mdblAmount.ToString(GUI.fmtCurrency)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudQty_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Pinkal (05-Mar-2012) -- End

    'Pinkal (14-Dec-2012) -- Start
    'Enhancement : TRA Changes

#Region "Link Button Event"

    Private Sub LnkViewDependants_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LnkViewDependants.LinkClicked
        Try
            gbLeaveExpense.Enabled = False
            pnlExpense.Enabled = False
            objFooter.Enabled = False
            Dim objDependant As New clsDependants_Beneficiary_tran

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'Dim dsList As DataSet = objDependant.GetQualifiedDepedant(mintEmployeeID, False, True, mdtDate.Date)
            Dim dsList As DataSet = objDependant.GetQualifiedDepedant(mintEmployeeID, False, True, mdtDate.Date, dtAsOnDate:=mdtDate.Date)
            'Sohail (18 May 2019) -- End
            dgDepedent.AutoGenerateColumns = False
            dgDepedent.DataSource = dsList.Tables(0)
            dgcolhName.DataPropertyName = "dependants"
            dgcolhAge.DataPropertyName = "age"
            dgcolhGender.DataPropertyName = "gender"
            dgcolhRelation.DataPropertyName = "relation"

            'Pinkal (19-Jun-2014) -- Start
            'Enhancement : TRA Changes Leave Enhancement
            dgcolhMonth.DataPropertyName = "Months"
            'Pinkal (19-Jun-2014) -- End

            'Pinkal (21-Jul-2014) -- End

            gbDependantsList.Visible = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "LnkViewDependants_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (14-Dec-2012) -- End







	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbLeaveExpense.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeaveExpense.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbDependantsList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbDependantsList.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.BtnOk.GradientBackColor = GUI._ButttonBackColor
            Me.BtnOk.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

			Me.btnDependentClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDependentClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.BtnOk.Text = Language._Object.getCaption(Me.BtnOk.Name, Me.BtnOk.Text)
            Me.gbLeaveExpense.Text = Language._Object.getCaption(Me.gbLeaveExpense.Name, Me.gbLeaveExpense.Text)
            Me.lblQty.Text = Language._Object.getCaption(Me.lblQty.Name, Me.lblQty.Text)
            Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
            Me.lblUnitcost.Text = Language._Object.getCaption(Me.lblUnitcost.Name, Me.lblUnitcost.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.lblTotalAmount.Text = Language._Object.getCaption(Me.lblTotalAmount.Name, Me.lblTotalAmount.Text)
            Me.dgcolhDescription.HeaderText = Language._Object.getCaption(Me.dgcolhDescription.Name, Me.dgcolhDescription.HeaderText)
            Me.dgcolhQty.HeaderText = Language._Object.getCaption(Me.dgcolhQty.Name, Me.dgcolhQty.HeaderText)
            Me.dgcolhUnitcost.HeaderText = Language._Object.getCaption(Me.dgcolhUnitcost.Name, Me.dgcolhUnitcost.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
			Me.LblDomicileAdd.Text = Language._Object.getCaption(Me.LblDomicileAdd.Name, Me.LblDomicileAdd.Text)
			Me.LnkViewDependants.Text = Language._Object.getCaption(Me.LnkViewDependants.Name, Me.LnkViewDependants.Text)
			Me.gbDependantsList.Text = Language._Object.getCaption(Me.gbDependantsList.Name, Me.gbDependantsList.Text)
			Me.btnDependentClose.Text = Language._Object.getCaption(Me.btnDependentClose.Name, Me.btnDependentClose.Text)
			Me.dgcolhName.HeaderText = Language._Object.getCaption(Me.dgcolhName.Name, Me.dgcolhName.HeaderText)
			Me.dgcolhGender.HeaderText = Language._Object.getCaption(Me.dgcolhGender.Name, Me.dgcolhGender.HeaderText)
			Me.dgcolhAge.HeaderText = Language._Object.getCaption(Me.dgcolhAge.Name, Me.dgcolhAge.HeaderText)
			Me.dgcolhRelation.HeaderText = Language._Object.getCaption(Me.dgcolhRelation.Name, Me.dgcolhRelation.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Description cannot be blank.Description is required information.")
            Language.setMessage(mstrModuleName, 2, "Amount cannot be blank or 0.Amount is required information.")
            Language.setMessage(mstrModuleName, 3, "This Expense is already exist for this leave form.Please define new leave expense.")
            Language.setMessage(mstrModuleName, 4, "Are You sure you want to delete this expense ?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
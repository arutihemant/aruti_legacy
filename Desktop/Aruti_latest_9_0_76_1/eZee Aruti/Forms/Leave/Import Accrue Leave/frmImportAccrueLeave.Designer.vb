﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportAccrueLeave
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportAccrueLeave))
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.DgAccrueLeave = New System.Windows.Forms.DataGridView
        Me.gbFileInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radDonotIssueAsonDate = New System.Windows.Forms.RadioButton
        Me.radDonotIssue = New System.Windows.Forms.RadioButton
        Me.chkShortLeave = New System.Windows.Forms.CheckBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.radApplytoAll = New System.Windows.Forms.RadioButton
        Me.radApplytoChecked = New System.Windows.Forms.RadioButton
        Me.objLine2 = New eZee.Common.eZeeStraightLine
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.objbtnSet = New eZee.Common.eZeeLightButton(Me.components)
        Me.elMandatoryInfo = New eZee.Common.eZeeLine
        Me.radExceedBal = New System.Windows.Forms.RadioButton
        Me.radIssuebal = New System.Windows.Forms.RadioButton
        Me.objbtnOpenFile = New eZee.Common.eZeeGradientButton
        Me.txtFilePath = New System.Windows.Forms.TextBox
        Me.lblFileName = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objbtnImport = New eZee.Common.eZeeSplitButton
        Me.cmImportAccrueLeave = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnImportData = New System.Windows.Forms.ToolStripMenuItem
        Me.btnExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.btnGetFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.btnGetShortLeaveFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.objcolhIscheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhEmployeecode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhLeave = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAccrueSetting = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStartdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEnddate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAccrue_amount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMonthlyAccrue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhForwardAmt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhLeavetypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objColhAccrueSettingId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel1.SuspendLayout()
        CType(Me.DgAccrueLeave, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFileInfo.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.cmImportAccrueLeave.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.DgAccrueLeave)
        Me.Panel1.Controls.Add(Me.gbFileInfo)
        Me.Panel1.Controls.Add(Me.EZeeFooter1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(594, 566)
        Me.Panel1.TabIndex = 0
        '
        'DgAccrueLeave
        '
        Me.DgAccrueLeave.AllowUserToAddRows = False
        Me.DgAccrueLeave.AllowUserToDeleteRows = False
        Me.DgAccrueLeave.AllowUserToResizeColumns = False
        Me.DgAccrueLeave.AllowUserToResizeRows = False
        Me.DgAccrueLeave.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.DgAccrueLeave.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhImage, Me.objcolhIscheck, Me.colhEmployeecode, Me.colhEmployee, Me.colhLeave, Me.colhAccrueSetting, Me.colhStartdate, Me.colhEnddate, Me.colhAccrue_amount, Me.dgcolhMonthlyAccrue, Me.colhForwardAmt, Me.colhMessage, Me.objcolhEmployeeId, Me.objcolhLeavetypeId, Me.objColhAccrueSettingId})
        Me.DgAccrueLeave.Location = New System.Drawing.Point(12, 209)
        Me.DgAccrueLeave.Name = "DgAccrueLeave"
        Me.DgAccrueLeave.RowHeadersVisible = False
        Me.DgAccrueLeave.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DgAccrueLeave.Size = New System.Drawing.Size(570, 296)
        Me.DgAccrueLeave.TabIndex = 4
        '
        'gbFileInfo
        '
        Me.gbFileInfo.BorderColor = System.Drawing.Color.Black
        Me.gbFileInfo.Checked = False
        Me.gbFileInfo.CollapseAllExceptThis = False
        Me.gbFileInfo.CollapsedHoverImage = Nothing
        Me.gbFileInfo.CollapsedNormalImage = Nothing
        Me.gbFileInfo.CollapsedPressedImage = Nothing
        Me.gbFileInfo.CollapseOnLoad = False
        Me.gbFileInfo.Controls.Add(Me.radDonotIssueAsonDate)
        Me.gbFileInfo.Controls.Add(Me.radDonotIssue)
        Me.gbFileInfo.Controls.Add(Me.chkShortLeave)
        Me.gbFileInfo.Controls.Add(Me.Panel2)
        Me.gbFileInfo.Controls.Add(Me.objLine2)
        Me.gbFileInfo.Controls.Add(Me.objLine1)
        Me.gbFileInfo.Controls.Add(Me.objbtnSet)
        Me.gbFileInfo.Controls.Add(Me.elMandatoryInfo)
        Me.gbFileInfo.Controls.Add(Me.radExceedBal)
        Me.gbFileInfo.Controls.Add(Me.radIssuebal)
        Me.gbFileInfo.Controls.Add(Me.objbtnOpenFile)
        Me.gbFileInfo.Controls.Add(Me.txtFilePath)
        Me.gbFileInfo.Controls.Add(Me.lblFileName)
        Me.gbFileInfo.ExpandedHoverImage = Nothing
        Me.gbFileInfo.ExpandedNormalImage = Nothing
        Me.gbFileInfo.ExpandedPressedImage = Nothing
        Me.gbFileInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFileInfo.HeaderHeight = 25
        Me.gbFileInfo.HeaderMessage = ""
        Me.gbFileInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFileInfo.HeightOnCollapse = 0
        Me.gbFileInfo.LeftTextSpace = 0
        Me.gbFileInfo.Location = New System.Drawing.Point(12, 12)
        Me.gbFileInfo.Name = "gbFileInfo"
        Me.gbFileInfo.OpenHeight = 300
        Me.gbFileInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFileInfo.ShowBorder = True
        Me.gbFileInfo.ShowCheckBox = False
        Me.gbFileInfo.ShowCollapseButton = False
        Me.gbFileInfo.ShowDefaultBorderColor = True
        Me.gbFileInfo.ShowDownButton = False
        Me.gbFileInfo.ShowHeader = True
        Me.gbFileInfo.Size = New System.Drawing.Size(570, 190)
        Me.gbFileInfo.TabIndex = 3
        Me.gbFileInfo.Temp = 0
        Me.gbFileInfo.Text = "File Name"
        Me.gbFileInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radDonotIssueAsonDate
        '
        Me.radDonotIssueAsonDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDonotIssueAsonDate.Location = New System.Drawing.Point(24, 165)
        Me.radDonotIssueAsonDate.Name = "radDonotIssueAsonDate"
        Me.radDonotIssueAsonDate.Size = New System.Drawing.Size(304, 17)
        Me.radDonotIssueAsonDate.TabIndex = 233
        Me.radDonotIssueAsonDate.Text = "Don't Issue on Exceeding Leave Balance As On Date"
        Me.radDonotIssueAsonDate.UseVisualStyleBackColor = True
        '
        'radDonotIssue
        '
        Me.radDonotIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDonotIssue.Location = New System.Drawing.Point(24, 142)
        Me.radDonotIssue.Name = "radDonotIssue"
        Me.radDonotIssue.Size = New System.Drawing.Size(236, 17)
        Me.radDonotIssue.TabIndex = 239
        Me.radDonotIssue.Text = "Don't Issue on Exceeding Leave Balance"
        Me.radDonotIssue.UseVisualStyleBackColor = True
        '
        'chkShortLeave
        '
        Me.chkShortLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShortLeave.Location = New System.Drawing.Point(24, 78)
        Me.chkShortLeave.Name = "chkShortLeave"
        Me.chkShortLeave.Size = New System.Drawing.Size(205, 17)
        Me.chkShortLeave.TabIndex = 237
        Me.chkShortLeave.Text = "Import Short Leave"
        Me.chkShortLeave.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.radApplytoAll)
        Me.Panel2.Controls.Add(Me.radApplytoChecked)
        Me.Panel2.Location = New System.Drawing.Point(272, 75)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(152, 73)
        Me.Panel2.TabIndex = 5
        '
        'radApplytoAll
        '
        Me.radApplytoAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoAll.Location = New System.Drawing.Point(11, 9)
        Me.radApplytoAll.Name = "radApplytoAll"
        Me.radApplytoAll.Size = New System.Drawing.Size(133, 18)
        Me.radApplytoAll.TabIndex = 232
        Me.radApplytoAll.TabStop = True
        Me.radApplytoAll.Text = "Apply to All"
        Me.radApplytoAll.UseVisualStyleBackColor = True
        '
        'radApplytoChecked
        '
        Me.radApplytoChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoChecked.Location = New System.Drawing.Point(11, 33)
        Me.radApplytoChecked.Name = "radApplytoChecked"
        Me.radApplytoChecked.Size = New System.Drawing.Size(133, 18)
        Me.radApplytoChecked.TabIndex = 233
        Me.radApplytoChecked.TabStop = True
        Me.radApplytoChecked.Text = "Apply To Checked"
        Me.radApplytoChecked.UseVisualStyleBackColor = True
        '
        'objLine2
        '
        Me.objLine2.BackColor = System.Drawing.Color.Transparent
        Me.objLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine2.Location = New System.Drawing.Point(423, 71)
        Me.objLine2.Name = "objLine2"
        Me.objLine2.Size = New System.Drawing.Size(11, 79)
        Me.objLine2.TabIndex = 235
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(263, 71)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(11, 79)
        Me.objLine1.TabIndex = 234
        '
        'objbtnSet
        '
        Me.objbtnSet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSet.BackColor = System.Drawing.Color.White
        Me.objbtnSet.BackgroundImage = CType(resources.GetObject("objbtnSet.BackgroundImage"), System.Drawing.Image)
        Me.objbtnSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnSet.BorderColor = System.Drawing.Color.Empty
        Me.objbtnSet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnSet.FlatAppearance.BorderSize = 0
        Me.objbtnSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSet.ForeColor = System.Drawing.Color.Black
        Me.objbtnSet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnSet.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnSet.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnSet.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnSet.Location = New System.Drawing.Point(454, 116)
        Me.objbtnSet.Name = "objbtnSet"
        Me.objbtnSet.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnSet.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnSet.Size = New System.Drawing.Size(98, 30)
        Me.objbtnSet.TabIndex = 19
        Me.objbtnSet.Text = "&Set"
        Me.objbtnSet.UseVisualStyleBackColor = False
        '
        'elMandatoryInfo
        '
        Me.elMandatoryInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elMandatoryInfo.Location = New System.Drawing.Point(5, 57)
        Me.elMandatoryInfo.Name = "elMandatoryInfo"
        Me.elMandatoryInfo.Size = New System.Drawing.Size(547, 16)
        Me.elMandatoryInfo.TabIndex = 7
        Me.elMandatoryInfo.Text = "Mandatory Information"
        Me.elMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radExceedBal
        '
        Me.radExceedBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExceedBal.Location = New System.Drawing.Point(24, 119)
        Me.radExceedBal.Name = "radExceedBal"
        Me.radExceedBal.Size = New System.Drawing.Size(236, 17)
        Me.radExceedBal.TabIndex = 231
        Me.radExceedBal.Text = "Exceeding Leave Balance with unpaid"
        Me.radExceedBal.UseVisualStyleBackColor = True
        '
        'radIssuebal
        '
        Me.radIssuebal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radIssuebal.Location = New System.Drawing.Point(24, 98)
        Me.radIssuebal.Name = "radIssuebal"
        Me.radIssuebal.Size = New System.Drawing.Size(236, 16)
        Me.radIssuebal.TabIndex = 230
        Me.radIssuebal.Text = "Issue Balance with Paid"
        Me.radIssuebal.UseVisualStyleBackColor = True
        '
        'objbtnOpenFile
        '
        Me.objbtnOpenFile.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOpenFile.BorderSelected = False
        Me.objbtnOpenFile.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnOpenFile.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnOpenFile.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOpenFile.Location = New System.Drawing.Point(485, 31)
        Me.objbtnOpenFile.Name = "objbtnOpenFile"
        Me.objbtnOpenFile.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOpenFile.TabIndex = 5
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.Location = New System.Drawing.Point(105, 31)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(374, 21)
        Me.txtFilePath.TabIndex = 3
        '
        'lblFileName
        '
        Me.lblFileName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileName.Location = New System.Drawing.Point(26, 33)
        Me.lblFileName.Name = "lblFileName"
        Me.lblFileName.Size = New System.Drawing.Size(76, 17)
        Me.lblFileName.TabIndex = 3
        Me.lblFileName.Text = "File Name"
        Me.lblFileName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objbtnImport)
        Me.EZeeFooter1.Controls.Add(Me.btnCancel)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 511)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(594, 55)
        Me.EZeeFooter1.TabIndex = 0
        '
        'objbtnImport
        '
        Me.objbtnImport.BorderColor = System.Drawing.Color.Black
        Me.objbtnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnImport.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.objbtnImport.Location = New System.Drawing.Point(369, 13)
        Me.objbtnImport.Name = "objbtnImport"
        Me.objbtnImport.ShowDefaultBorderColor = True
        Me.objbtnImport.Size = New System.Drawing.Size(110, 30)
        Me.objbtnImport.SplitButtonMenu = Me.cmImportAccrueLeave
        Me.objbtnImport.TabIndex = 4
        Me.objbtnImport.Text = "Import"
        '
        'cmImportAccrueLeave
        '
        Me.cmImportAccrueLeave.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnImportData, Me.btnExportError, Me.btnGetFileFormat, Me.btnGetShortLeaveFileFormat})
        Me.cmImportAccrueLeave.Name = "cmImportAccrueLeave"
        Me.cmImportAccrueLeave.Size = New System.Drawing.Size(219, 92)
        '
        'btnImportData
        '
        Me.btnImportData.Name = "btnImportData"
        Me.btnImportData.Size = New System.Drawing.Size(218, 22)
        Me.btnImportData.Text = "&Import"
        '
        'btnExportError
        '
        Me.btnExportError.Name = "btnExportError"
        Me.btnExportError.Size = New System.Drawing.Size(218, 22)
        Me.btnExportError.Text = "Show Error"
        '
        'btnGetFileFormat
        '
        Me.btnGetFileFormat.Name = "btnGetFileFormat"
        Me.btnGetFileFormat.Size = New System.Drawing.Size(218, 22)
        Me.btnGetFileFormat.Text = "&Get File Format"
        '
        'btnGetShortLeaveFileFormat
        '
        Me.btnGetShortLeaveFileFormat.Name = "btnGetShortLeaveFileFormat"
        Me.btnGetShortLeaveFileFormat.Size = New System.Drawing.Size(218, 22)
        Me.btnGetShortLeaveFileFormat.Text = "Get Short Leave File Format"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(485, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'DataGridViewTextBoxColumn1
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 75
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle9
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 125
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle10
        Me.DataGridViewTextBoxColumn3.HeaderText = "Leave"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle11
        Me.DataGridViewTextBoxColumn4.HeaderText = "Start Date"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 250
        '
        'DataGridViewTextBoxColumn5
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle12
        Me.DataGridViewTextBoxColumn5.HeaderText = "End Date"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle13
        Me.DataGridViewTextBoxColumn6.HeaderText = "Accrue Amt"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle14
        Me.DataGridViewTextBoxColumn7.HeaderText = "Forward Amt"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle15
        Me.DataGridViewTextBoxColumn8.HeaderText = "EmployeeId"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "LeaveTypeId"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Visible = False
        Me.DataGridViewTextBoxColumn9.Width = 250
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "LeaveTypeId"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "AccrueSetting Id"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "AccrueSetting Id"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'colhImage
        '
        Me.colhImage.HeaderText = ""
        Me.colhImage.Name = "colhImage"
        Me.colhImage.ReadOnly = True
        Me.colhImage.Width = 30
        '
        'objcolhIscheck
        '
        Me.objcolhIscheck.HeaderText = ""
        Me.objcolhIscheck.Name = "objcolhIscheck"
        Me.objcolhIscheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhIscheck.Width = 25
        '
        'colhEmployeecode
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colhEmployeecode.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhEmployeecode.HeaderText = "Code"
        Me.colhEmployeecode.Name = "colhEmployeecode"
        Me.colhEmployeecode.ReadOnly = True
        Me.colhEmployeecode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhEmployeecode.Width = 75
        '
        'colhEmployee
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colhEmployee.DefaultCellStyle = DataGridViewCellStyle2
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        Me.colhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhEmployee.Width = 125
        '
        'colhLeave
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colhLeave.DefaultCellStyle = DataGridViewCellStyle3
        Me.colhLeave.HeaderText = "Leave"
        Me.colhLeave.Name = "colhLeave"
        Me.colhLeave.ReadOnly = True
        Me.colhLeave.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhAccrueSetting
        '
        Me.colhAccrueSetting.HeaderText = "Accrue Setting"
        Me.colhAccrueSetting.Name = "colhAccrueSetting"
        Me.colhAccrueSetting.ReadOnly = True
        Me.colhAccrueSetting.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhAccrueSetting.Width = 200
        '
        'colhStartdate
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colhStartdate.DefaultCellStyle = DataGridViewCellStyle4
        Me.colhStartdate.HeaderText = "Start Date"
        Me.colhStartdate.Name = "colhStartdate"
        Me.colhStartdate.ReadOnly = True
        Me.colhStartdate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhEnddate
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.colhEnddate.DefaultCellStyle = DataGridViewCellStyle5
        Me.colhEnddate.HeaderText = "End Date"
        Me.colhEnddate.Name = "colhEnddate"
        Me.colhEnddate.ReadOnly = True
        Me.colhEnddate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhAccrue_amount
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhAccrue_amount.DefaultCellStyle = DataGridViewCellStyle6
        Me.colhAccrue_amount.HeaderText = "Accrue Amt"
        Me.colhAccrue_amount.Name = "colhAccrue_amount"
        Me.colhAccrue_amount.ReadOnly = True
        Me.colhAccrue_amount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhMonthlyAccrue
        '
        Me.dgcolhMonthlyAccrue.HeaderText = "Monthly Accrue"
        Me.dgcolhMonthlyAccrue.Name = "dgcolhMonthlyAccrue"
        Me.dgcolhMonthlyAccrue.ReadOnly = True
        Me.dgcolhMonthlyAccrue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhForwardAmt
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhForwardAmt.DefaultCellStyle = DataGridViewCellStyle7
        Me.colhForwardAmt.HeaderText = "Forward Amt"
        Me.colhForwardAmt.Name = "colhForwardAmt"
        Me.colhForwardAmt.ReadOnly = True
        Me.colhForwardAmt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 250
        '
        'objcolhEmployeeId
        '
        Me.objcolhEmployeeId.HeaderText = "EmployeeId"
        Me.objcolhEmployeeId.Name = "objcolhEmployeeId"
        Me.objcolhEmployeeId.ReadOnly = True
        Me.objcolhEmployeeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhEmployeeId.Visible = False
        '
        'objcolhLeavetypeId
        '
        Me.objcolhLeavetypeId.HeaderText = "LeaveTypeId"
        Me.objcolhLeavetypeId.Name = "objcolhLeavetypeId"
        Me.objcolhLeavetypeId.ReadOnly = True
        Me.objcolhLeavetypeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhLeavetypeId.Visible = False
        '
        'objColhAccrueSettingId
        '
        Me.objColhAccrueSettingId.HeaderText = "AccrueSetting Id"
        Me.objColhAccrueSettingId.Name = "objColhAccrueSettingId"
        Me.objColhAccrueSettingId.ReadOnly = True
        Me.objColhAccrueSettingId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objColhAccrueSettingId.Visible = False
        '
        'frmImportAccrueLeave
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(594, 566)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportAccrueLeave"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Import Accrue Leave"
        Me.Panel1.ResumeLayout(False)
        CType(Me.DgAccrueLeave, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFileInfo.ResumeLayout(False)
        Me.gbFileInfo.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.cmImportAccrueLeave.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents gbFileInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnOpenFile As eZee.Common.eZeeGradientButton
    Friend WithEvents txtFilePath As System.Windows.Forms.TextBox
    Friend WithEvents lblFileName As System.Windows.Forms.Label
    Friend WithEvents DgAccrueLeave As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnImport As eZee.Common.eZeeSplitButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents cmImportAccrueLeave As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents btnImportData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnGetFileFormat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents radExceedBal As System.Windows.Forms.RadioButton
    Friend WithEvents radIssuebal As System.Windows.Forms.RadioButton
    Friend WithEvents elMandatoryInfo As eZee.Common.eZeeLine
    Friend WithEvents objbtnSet As eZee.Common.eZeeLightButton
    Friend WithEvents radApplytoAll As System.Windows.Forms.RadioButton
    Friend WithEvents radApplytoChecked As System.Windows.Forms.RadioButton
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkShortLeave As System.Windows.Forms.CheckBox
    Friend WithEvents btnExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents radDonotIssue As System.Windows.Forms.RadioButton
    Friend WithEvents btnGetShortLeaveFileFormat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents radDonotIssueAsonDate As System.Windows.Forms.RadioButton
    Friend WithEvents colhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objcolhIscheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhEmployeecode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhLeave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAccrueSetting As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStartdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEnddate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAccrue_amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMonthlyAccrue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhForwardAmt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhLeavetypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objColhAccrueSettingId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

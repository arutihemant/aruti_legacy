﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 5

Public Class frmDeleteAccruedLeave

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmDeleteAccruedLeave"

    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintView_Idx As Integer = -1
    Dim objBatch As clsbatch_master
    Dim objAccrue As clsleavebalance_tran
    Private mintEmployeeunkid As Integer = -1

    'Sandeep [ 14 DEC 2010 ] -- Start
    Private mintAccruedId As Integer = 0
    'Sandeep [ 14 DEC 2010 ] -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intemployeeunkid As Integer, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            mintEmployeeunkid = intemployeeunkid
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Property"

    Public WriteOnly Property _Index() As Integer
        Set(ByVal value As Integer)
            mintView_Idx = value
        End Set
    End Property
    'Sandeep [ 14 DEC 2010 ] -- Start
    Public WriteOnly Property _LeaveBalanceId() As Integer
        Set(ByVal value As Integer)
            mintAccruedId = value
        End Set
    End Property
    'Sandeep [ 14 DEC 2010 ] -- End 

#End Region

#Region "Form's Event"

    

    Private Sub frmDeleteAccruedLeave_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            If mintView_Idx = 1 Then
                objBatch = New clsbatch_master
            ElseIf mintView_Idx = 2 Then
                objAccrue = New clsleavebalance_tran
            End If
            Select Case mintView_Idx
                Case 1
                    pnlDeleteByLeave.Enabled = False
                Case 2
                    pnlDeleteByBatch.Enabled = False
            End Select
            FillCombo()

            'Sandeep [ 14 DEC 2010 ] -- Start
            If mintAccruedId > 0 Then
                cboLeaveCode.SelectedValue = mintAccruedId
                cboLeaveCode.Enabled = False
            End If
            'Sandeep [ 14 DEC 2010 ] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDeleteAccruedLeave_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDeleteAccruedLeave_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDeleteAccruedLeave_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDeleteAccruedLeave_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        If mintView_Idx = 2 Then
            objBatch = Nothing
        End If
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleavebalance_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsleavebalance_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Anjan (02 Sep 2011)-End 



#End Region

#Region "Button's Event"

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim blnFlag As Boolean = False
        Try
            If mintView_Idx = 1 Then
                If CInt(cboBatchNo.SelectedValue) = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Batch No is compulsory information.Please Select Batch No."), enMsgBoxStyle.Information)
                    cboBatchNo.Select()
                    Exit Sub
                End If

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Batch No?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                    'Sandeep [ 16 Oct 2010 ] -- Start
                    'objBatch._Voidreason = "VOID REASON"
                    Dim frm As New frmReasonSelection
                    Dim mstrVoidReason As String = String.Empty
                    frm.displayDialog(enVoidCategoryType.LEAVE, mstrVoidReason)
                    If mstrVoidReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objBatch._Voidreason = mstrVoidReason
                    End If
                    frm = Nothing
                    objBatch._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objBatch._Voiduserunkid = User._Object._Userunkid
                    'Sandeep [ 16 Oct 2010 ] -- End 
                    blnFlag = objBatch.Delete(CInt(cboBatchNo.SelectedValue))

                    FillCombo()
                End If
                If blnFlag = False And objBatch._Message <> "" Then
                    eZeeMsgBox.Show(objBatch._Message, enMsgBoxStyle.Information)
                End If

            ElseIf mintView_Idx = 2 Then

                If CInt(cboLeaveCode.SelectedValue) = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Leave Type is compulsory information.Please Select Leave Type."), enMsgBoxStyle.Information)
                    cboLeaveCode.Select()
                    Exit Sub
                End If

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete Accrue Amount for this Leave?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                    'Sandeep [ 16 Oct 2010 ] -- Start
                    'objAccrue._Voidreason = "VOID REASON"
                    'blnFlag = objAccrue.Delete(-1, CInt(cboLeaveCode.SelectedValue))
                    Dim frm As New frmReasonSelection
                    Dim mstrVoidReason As String = String.Empty
                    frm.displayDialog(enVoidCategoryType.LEAVE, mstrVoidReason)
                    If mstrVoidReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objAccrue._Voidreason = mstrVoidReason
                    End If
                    frm = Nothing
                    objAccrue._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objAccrue._Voiduserunkid = User._Object._Userunkid
                    'Sandeep [ 16 Oct 2010 ] -- End 

                    'Sandeep [ 14 DEC 2010 ] -- Start
                    'blnFlag = objAccrue.Delete(-1, CInt(cboLeaveCode.SelectedValue))
                    blnFlag = objAccrue.Delete(mintAccruedId, CInt(cboLeaveCode.SelectedValue))
                    'Sandeep [ 14 DEC 2010 ] -- End 
                    FillCombo()
                End If
                If blnFlag = False And objAccrue._Message <> "" Then
                    eZeeMsgBox.Show(objAccrue._Message, enMsgBoxStyle.Information)
                End If
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_ONE Then
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub



#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try

            If mintView_Idx = 1 Then
                dsFill = objBatch.getListForCombo("Batch", True)
                cboBatchNo.ValueMember = "batchunkid"
                cboBatchNo.DisplayMember = "name"
                cboBatchNo.DataSource = dsFill.Tables("Batch")

                cboBatchNo.Select()
            ElseIf mintView_Idx = 2 Then
                Dim objLeaveType As New clsleavetype_master
                Dim dtFill As DataTable
                dsFill = objLeaveType.GetList("LeaveType")
                dtFill = New DataView(dsFill.Tables("LeaveType"), "1=1", "", DataViewRowState.CurrentRows).ToTable

                Dim drRow As DataRow = dtFill.NewRow
                drRow("leavetypeunkid") = 0
                drRow("leavename") = "Select"
                dtFill.Rows.InsertAt(drRow, 0)

                cboLeaveCode.ValueMember = "leavetypeunkid"
                cboLeaveCode.DisplayMember = "leavename"
                cboLeaveCode.DataSource = dtFill
                cboLeaveCode.Select()
            End If
            

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.lblLeaveCode.Text = Language._Object.getCaption(Me.lblLeaveCode.Name, Me.lblLeaveCode.Text)
			Me.lblBatchNo.Text = Language._Object.getCaption(Me.lblBatchNo.Name, Me.lblBatchNo.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Batch No is compulsory information.Please Select Batch No.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Batch No?")
			Language.setMessage(mstrModuleName, 3, "Leave Type is compulsory information.Please Select Leave Type.")
			Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete Accrue Amount for this Leave?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
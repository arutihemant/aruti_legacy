﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveIssue_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeaveIssue_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.dgLeaveIssue = New System.Windows.Forms.DataGridView
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.lblLastYearAccrued = New System.Windows.Forms.Label
        Me.objlblLastYearAccruedValue = New System.Windows.Forms.Label
        Me.objlblLastYearIssuedValue = New System.Windows.Forms.Label
        Me.lblLastYearIssued = New System.Windows.Forms.Label
        Me.pnlLeaveIssueInfo = New System.Windows.Forms.Panel
        Me.gbLeaveIssue = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkPreviewDocument = New System.Windows.Forms.LinkLabel
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.txtEmployee = New System.Windows.Forms.TextBox
        Me.objTotalLeaveDays = New System.Windows.Forms.Label
        Me.lblTotalLeaveDays = New System.Windows.Forms.Label
        Me.objlblDateRange = New System.Windows.Forms.Label
        Me.objTlbLeaveBalance = New System.Windows.Forms.TableLayoutPanel
        Me.objlblBalanceValue = New System.Windows.Forms.Label
        Me.lblBalance = New System.Windows.Forms.Label
        Me.objlblAccruedToDateValue = New System.Windows.Forms.Label
        Me.lblToDateAccrued = New System.Windows.Forms.Label
        Me.lblAmount = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblTotalIssuedToDate = New System.Windows.Forms.Label
        Me.objlblIssuedToDateValue = New System.Windows.Forms.Label
        Me.LblLeaveBF = New System.Windows.Forms.Label
        Me.objlblLeaveBFvalue = New System.Windows.Forms.Label
        Me.LblTotalAdjustment = New System.Windows.Forms.Label
        Me.objlblTotalAdjustment = New System.Windows.Forms.Label
        Me.txtAsDate = New System.Windows.Forms.TextBox
        Me.lblLeaveEndDate = New System.Windows.Forms.Label
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.lblFormNo = New System.Windows.Forms.Label
        Me.cboFormNo = New System.Windows.Forms.ComboBox
        Me.cboLeaveYear = New System.Windows.Forms.ComboBox
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.lblLeaveYear = New System.Windows.Forms.Label
        Me.objlblcolor = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlLeaveViewer = New System.Windows.Forms.Panel
        Me.gbLeaveViewer = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.txtEmpviewer = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.LblEmpViewer = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblToDate = New System.Windows.Forms.Label
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.pnlLeaveLegends = New System.Windows.Forms.Panel
        Me.lvLeaveCodes = New eZee.Common.eZeeListView(Me.components)
        Me.colhLeavetype = New System.Windows.Forms.ColumnHeader
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgLeaveIssue, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlLeaveIssueInfo.SuspendLayout()
        Me.gbLeaveIssue.SuspendLayout()
        Me.objTlbLeaveBalance.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.pnlLeaveViewer.SuspendLayout()
        Me.gbLeaveViewer.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlLeaveLegends.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.dgLeaveIssue)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.lblLastYearAccrued)
        Me.pnlMainInfo.Controls.Add(Me.objlblLastYearAccruedValue)
        Me.pnlMainInfo.Controls.Add(Me.objlblLastYearIssuedValue)
        Me.pnlMainInfo.Controls.Add(Me.lblLastYearIssued)
        Me.pnlMainInfo.Controls.Add(Me.pnlLeaveIssueInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.pnlLeaveViewer)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(832, 558)
        Me.pnlMainInfo.TabIndex = 0
        '
        'dgLeaveIssue
        '
        Me.dgLeaveIssue.AllowUserToAddRows = False
        Me.dgLeaveIssue.AllowUserToDeleteRows = False
        Me.dgLeaveIssue.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgLeaveIssue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgLeaveIssue.Location = New System.Drawing.Point(10, 239)
        Me.dgLeaveIssue.Name = "dgLeaveIssue"
        Me.dgLeaveIssue.RowHeadersVisible = False
        Me.dgLeaveIssue.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.White
        Me.dgLeaveIssue.Size = New System.Drawing.Size(810, 257)
        Me.dgLeaveIssue.TabIndex = 6
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(832, 58)
        Me.eZeeHeader.TabIndex = 6
        Me.eZeeHeader.Title = "Leave Issue"
        '
        'lblLastYearAccrued
        '
        Me.lblLastYearAccrued.Location = New System.Drawing.Point(893, 93)
        Me.lblLastYearAccrued.Name = "lblLastYearAccrued"
        Me.lblLastYearAccrued.Size = New System.Drawing.Size(160, 20)
        Me.lblLastYearAccrued.TabIndex = 2
        Me.lblLastYearAccrued.Text = "Total Accrued Upto Last Year"
        Me.lblLastYearAccrued.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblLastYearAccrued.Visible = False
        '
        'objlblLastYearAccruedValue
        '
        Me.objlblLastYearAccruedValue.Location = New System.Drawing.Point(893, 120)
        Me.objlblLastYearAccruedValue.Name = "objlblLastYearAccruedValue"
        Me.objlblLastYearAccruedValue.Size = New System.Drawing.Size(100, 20)
        Me.objlblLastYearAccruedValue.TabIndex = 3
        Me.objlblLastYearAccruedValue.Text = "0.00"
        Me.objlblLastYearAccruedValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.objlblLastYearAccruedValue.Visible = False
        '
        'objlblLastYearIssuedValue
        '
        Me.objlblLastYearIssuedValue.Location = New System.Drawing.Point(896, 182)
        Me.objlblLastYearIssuedValue.Name = "objlblLastYearIssuedValue"
        Me.objlblLastYearIssuedValue.Size = New System.Drawing.Size(100, 20)
        Me.objlblLastYearIssuedValue.TabIndex = 7
        Me.objlblLastYearIssuedValue.Text = "0"
        Me.objlblLastYearIssuedValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLastYearIssued
        '
        Me.lblLastYearIssued.Location = New System.Drawing.Point(896, 148)
        Me.lblLastYearIssued.Name = "lblLastYearIssued"
        Me.lblLastYearIssued.Size = New System.Drawing.Size(153, 20)
        Me.lblLastYearIssued.TabIndex = 6
        Me.lblLastYearIssued.Text = "Total Issued Upto Last Year"
        Me.lblLastYearIssued.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLeaveIssueInfo
        '
        Me.pnlLeaveIssueInfo.Controls.Add(Me.gbLeaveIssue)
        Me.pnlLeaveIssueInfo.Location = New System.Drawing.Point(10, 64)
        Me.pnlLeaveIssueInfo.Name = "pnlLeaveIssueInfo"
        Me.pnlLeaveIssueInfo.Size = New System.Drawing.Size(810, 169)
        Me.pnlLeaveIssueInfo.TabIndex = 9
        '
        'gbLeaveIssue
        '
        Me.gbLeaveIssue.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveIssue.Checked = False
        Me.gbLeaveIssue.CollapseAllExceptThis = False
        Me.gbLeaveIssue.CollapsedHoverImage = Nothing
        Me.gbLeaveIssue.CollapsedNormalImage = Nothing
        Me.gbLeaveIssue.CollapsedPressedImage = Nothing
        Me.gbLeaveIssue.CollapseOnLoad = False
        Me.gbLeaveIssue.Controls.Add(Me.lnkPreviewDocument)
        Me.gbLeaveIssue.Controls.Add(Me.lblEmployee)
        Me.gbLeaveIssue.Controls.Add(Me.txtEmployee)
        Me.gbLeaveIssue.Controls.Add(Me.objTotalLeaveDays)
        Me.gbLeaveIssue.Controls.Add(Me.lblTotalLeaveDays)
        Me.gbLeaveIssue.Controls.Add(Me.objlblDateRange)
        Me.gbLeaveIssue.Controls.Add(Me.objTlbLeaveBalance)
        Me.gbLeaveIssue.Controls.Add(Me.txtAsDate)
        Me.gbLeaveIssue.Controls.Add(Me.lblLeaveEndDate)
        Me.gbLeaveIssue.Controls.Add(Me.cboLeaveType)
        Me.gbLeaveIssue.Controls.Add(Me.lblFormNo)
        Me.gbLeaveIssue.Controls.Add(Me.cboFormNo)
        Me.gbLeaveIssue.Controls.Add(Me.cboLeaveYear)
        Me.gbLeaveIssue.Controls.Add(Me.lblLeaveType)
        Me.gbLeaveIssue.Controls.Add(Me.lblLeaveYear)
        Me.gbLeaveIssue.Controls.Add(Me.objlblcolor)
        Me.gbLeaveIssue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbLeaveIssue.ExpandedHoverImage = Nothing
        Me.gbLeaveIssue.ExpandedNormalImage = Nothing
        Me.gbLeaveIssue.ExpandedPressedImage = Nothing
        Me.gbLeaveIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveIssue.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveIssue.HeaderHeight = 25
        Me.gbLeaveIssue.HeaderMessage = ""
        Me.gbLeaveIssue.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveIssue.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveIssue.HeightOnCollapse = 0
        Me.gbLeaveIssue.LeftTextSpace = 0
        Me.gbLeaveIssue.Location = New System.Drawing.Point(0, 0)
        Me.gbLeaveIssue.Name = "gbLeaveIssue"
        Me.gbLeaveIssue.OpenHeight = 300
        Me.gbLeaveIssue.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveIssue.ShowBorder = True
        Me.gbLeaveIssue.ShowCheckBox = False
        Me.gbLeaveIssue.ShowCollapseButton = False
        Me.gbLeaveIssue.ShowDefaultBorderColor = True
        Me.gbLeaveIssue.ShowDownButton = False
        Me.gbLeaveIssue.ShowHeader = True
        Me.gbLeaveIssue.Size = New System.Drawing.Size(810, 169)
        Me.gbLeaveIssue.TabIndex = 7
        Me.gbLeaveIssue.Temp = 0
        Me.gbLeaveIssue.Text = "Leave Issue Information"
        Me.gbLeaveIssue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkPreviewDocument
        '
        Me.lnkPreviewDocument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkPreviewDocument.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkPreviewDocument.Location = New System.Drawing.Point(218, 143)
        Me.lnkPreviewDocument.Name = "lnkPreviewDocument"
        Me.lnkPreviewDocument.Size = New System.Drawing.Size(183, 17)
        Me.lnkPreviewDocument.TabIndex = 260
        Me.lnkPreviewDocument.TabStop = True
        Me.lnkPreviewDocument.Text = "Preview Scanned Document"
        Me.lnkPreviewDocument.Visible = False
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(6, 37)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(70, 15)
        Me.lblEmployee.TabIndex = 246
        Me.lblEmployee.Text = "Employee"
        '
        'txtEmployee
        '
        Me.txtEmployee.BackColor = System.Drawing.SystemColors.Window
        Me.txtEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployee.Location = New System.Drawing.Point(81, 33)
        Me.txtEmployee.Name = "txtEmployee"
        Me.txtEmployee.ReadOnly = True
        Me.txtEmployee.Size = New System.Drawing.Size(365, 21)
        Me.txtEmployee.TabIndex = 2
        '
        'objTotalLeaveDays
        '
        Me.objTotalLeaveDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objTotalLeaveDays.Location = New System.Drawing.Point(333, 114)
        Me.objTotalLeaveDays.Name = "objTotalLeaveDays"
        Me.objTotalLeaveDays.Size = New System.Drawing.Size(61, 16)
        Me.objTotalLeaveDays.TabIndex = 258
        Me.objTotalLeaveDays.Text = "0"
        Me.objTotalLeaveDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalLeaveDays
        '
        Me.lblTotalLeaveDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalLeaveDays.Location = New System.Drawing.Point(218, 113)
        Me.lblTotalLeaveDays.Name = "lblTotalLeaveDays"
        Me.lblTotalLeaveDays.Size = New System.Drawing.Size(107, 17)
        Me.lblTotalLeaveDays.TabIndex = 257
        Me.lblTotalLeaveDays.Text = "Total Leave Days"
        Me.lblTotalLeaveDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblDateRange
        '
        Me.objlblDateRange.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblDateRange.Location = New System.Drawing.Point(218, 63)
        Me.objlblDateRange.Name = "objlblDateRange"
        Me.objlblDateRange.Size = New System.Drawing.Size(204, 13)
        Me.objlblDateRange.TabIndex = 254
        Me.objlblDateRange.Text = "01/01/2010 To 03/01/2010"
        '
        'objTlbLeaveBalance
        '
        Me.objTlbLeaveBalance.BackColor = System.Drawing.SystemColors.Control
        Me.objTlbLeaveBalance.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.objTlbLeaveBalance.ColumnCount = 2
        Me.objTlbLeaveBalance.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.51219!))
        Me.objTlbLeaveBalance.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.4878!))
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblBalanceValue, 1, 5)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblBalance, 0, 5)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblAccruedToDateValue, 1, 2)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblToDateAccrued, 0, 2)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblAmount, 1, 0)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblDescription, 0, 0)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblTotalIssuedToDate, 0, 3)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblIssuedToDateValue, 1, 3)
        Me.objTlbLeaveBalance.Controls.Add(Me.LblLeaveBF, 0, 1)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblLeaveBFvalue, 1, 1)
        Me.objTlbLeaveBalance.Controls.Add(Me.LblTotalAdjustment, 0, 4)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblTotalAdjustment, 1, 4)
        Me.objTlbLeaveBalance.Location = New System.Drawing.Point(452, 33)
        Me.objTlbLeaveBalance.Name = "objTlbLeaveBalance"
        Me.objTlbLeaveBalance.RowCount = 6
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.Size = New System.Drawing.Size(350, 128)
        Me.objTlbLeaveBalance.TabIndex = 3
        '
        'objlblBalanceValue
        '
        Me.objlblBalanceValue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblBalanceValue.Location = New System.Drawing.Point(246, 106)
        Me.objlblBalanceValue.Name = "objlblBalanceValue"
        Me.objlblBalanceValue.Size = New System.Drawing.Size(100, 21)
        Me.objlblBalanceValue.TabIndex = 11
        Me.objlblBalanceValue.Text = "0.00"
        Me.objlblBalanceValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBalance
        '
        Me.lblBalance.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblBalance.Location = New System.Drawing.Point(4, 106)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(235, 21)
        Me.lblBalance.TabIndex = 10
        Me.lblBalance.Text = "Balance"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblAccruedToDateValue
        '
        Me.objlblAccruedToDateValue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblAccruedToDateValue.Location = New System.Drawing.Point(246, 43)
        Me.objlblAccruedToDateValue.Name = "objlblAccruedToDateValue"
        Me.objlblAccruedToDateValue.Size = New System.Drawing.Size(100, 20)
        Me.objlblAccruedToDateValue.TabIndex = 5
        Me.objlblAccruedToDateValue.Text = "0.00"
        Me.objlblAccruedToDateValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblToDateAccrued
        '
        Me.lblToDateAccrued.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblToDateAccrued.Location = New System.Drawing.Point(4, 43)
        Me.lblToDateAccrued.Name = "lblToDateAccrued"
        Me.lblToDateAccrued.Size = New System.Drawing.Size(235, 20)
        Me.lblToDateAccrued.TabIndex = 4
        Me.lblToDateAccrued.Text = "Total Accrued To Date"
        Me.lblToDateAccrued.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAmount
        '
        Me.lblAmount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblAmount.Location = New System.Drawing.Point(246, 1)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(100, 20)
        Me.lblAmount.TabIndex = 1
        Me.lblAmount.Text = "Amount"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDescription
        '
        Me.lblDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblDescription.Location = New System.Drawing.Point(4, 1)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(235, 20)
        Me.lblDescription.TabIndex = 0
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalIssuedToDate
        '
        Me.lblTotalIssuedToDate.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblTotalIssuedToDate.Location = New System.Drawing.Point(4, 64)
        Me.lblTotalIssuedToDate.Name = "lblTotalIssuedToDate"
        Me.lblTotalIssuedToDate.Size = New System.Drawing.Size(235, 20)
        Me.lblTotalIssuedToDate.TabIndex = 8
        Me.lblTotalIssuedToDate.Text = "Total Issued To Date"
        Me.lblTotalIssuedToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblIssuedToDateValue
        '
        Me.objlblIssuedToDateValue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblIssuedToDateValue.Location = New System.Drawing.Point(246, 64)
        Me.objlblIssuedToDateValue.Name = "objlblIssuedToDateValue"
        Me.objlblIssuedToDateValue.Size = New System.Drawing.Size(100, 20)
        Me.objlblIssuedToDateValue.TabIndex = 9
        Me.objlblIssuedToDateValue.Text = "0"
        Me.objlblIssuedToDateValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblLeaveBF
        '
        Me.LblLeaveBF.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LblLeaveBF.Location = New System.Drawing.Point(4, 22)
        Me.LblLeaveBF.Name = "LblLeaveBF"
        Me.LblLeaveBF.Size = New System.Drawing.Size(235, 20)
        Me.LblLeaveBF.TabIndex = 12
        Me.LblLeaveBF.Text = "Leave BF"
        Me.LblLeaveBF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblLeaveBFvalue
        '
        Me.objlblLeaveBFvalue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblLeaveBFvalue.Location = New System.Drawing.Point(246, 22)
        Me.objlblLeaveBFvalue.Name = "objlblLeaveBFvalue"
        Me.objlblLeaveBFvalue.Size = New System.Drawing.Size(100, 20)
        Me.objlblLeaveBFvalue.TabIndex = 13
        Me.objlblLeaveBFvalue.Text = "0.00"
        Me.objlblLeaveBFvalue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblTotalAdjustment
        '
        Me.LblTotalAdjustment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LblTotalAdjustment.Location = New System.Drawing.Point(4, 85)
        Me.LblTotalAdjustment.Name = "LblTotalAdjustment"
        Me.LblTotalAdjustment.Size = New System.Drawing.Size(235, 20)
        Me.LblTotalAdjustment.TabIndex = 14
        Me.LblTotalAdjustment.Text = "Total Adjustment"
        Me.LblTotalAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblTotalAdjustment
        '
        Me.objlblTotalAdjustment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblTotalAdjustment.Location = New System.Drawing.Point(246, 85)
        Me.objlblTotalAdjustment.Name = "objlblTotalAdjustment"
        Me.objlblTotalAdjustment.Size = New System.Drawing.Size(100, 20)
        Me.objlblTotalAdjustment.TabIndex = 15
        Me.objlblTotalAdjustment.Text = "0.00"
        Me.objlblTotalAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAsDate
        '
        Me.txtAsDate.BackColor = System.Drawing.SystemColors.Window
        Me.txtAsDate.Location = New System.Drawing.Point(81, 111)
        Me.txtAsDate.Name = "txtAsDate"
        Me.txtAsDate.ReadOnly = True
        Me.txtAsDate.Size = New System.Drawing.Size(131, 21)
        Me.txtAsDate.TabIndex = 251
        '
        'lblLeaveEndDate
        '
        Me.lblLeaveEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveEndDate.Location = New System.Drawing.Point(6, 117)
        Me.lblLeaveEndDate.Name = "lblLeaveEndDate"
        Me.lblLeaveEndDate.Size = New System.Drawing.Size(70, 13)
        Me.lblLeaveEndDate.TabIndex = 250
        Me.lblLeaveEndDate.Text = "As Date"
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(81, 85)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(131, 21)
        Me.cboLeaveType.TabIndex = 4
        '
        'lblFormNo
        '
        Me.lblFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormNo.Location = New System.Drawing.Point(6, 62)
        Me.lblFormNo.Name = "lblFormNo"
        Me.lblFormNo.Size = New System.Drawing.Size(70, 15)
        Me.lblFormNo.TabIndex = 224
        Me.lblFormNo.Text = "Form No"
        '
        'cboFormNo
        '
        Me.cboFormNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFormNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFormNo.FormattingEnabled = True
        Me.cboFormNo.Location = New System.Drawing.Point(81, 59)
        Me.cboFormNo.Name = "cboFormNo"
        Me.cboFormNo.Size = New System.Drawing.Size(131, 21)
        Me.cboFormNo.TabIndex = 3
        '
        'cboLeaveYear
        '
        Me.cboLeaveYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveYear.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveYear.FormattingEnabled = True
        Me.cboLeaveYear.Location = New System.Drawing.Point(81, 33)
        Me.cboLeaveYear.Name = "cboLeaveYear"
        Me.cboLeaveYear.Size = New System.Drawing.Size(131, 21)
        Me.cboLeaveYear.TabIndex = 1
        Me.cboLeaveYear.Visible = False
        '
        'lblLeaveType
        '
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(6, 89)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(70, 15)
        Me.lblLeaveType.TabIndex = 220
        Me.lblLeaveType.Text = "Leave Type"
        '
        'lblLeaveYear
        '
        Me.lblLeaveYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveYear.Location = New System.Drawing.Point(8, 36)
        Me.lblLeaveYear.Name = "lblLeaveYear"
        Me.lblLeaveYear.Size = New System.Drawing.Size(67, 15)
        Me.lblLeaveYear.TabIndex = 217
        Me.lblLeaveYear.Text = "Leave Year"
        Me.lblLeaveYear.Visible = False
        '
        'objlblcolor
        '
        Me.objlblcolor.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objlblcolor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objlblcolor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblcolor.Location = New System.Drawing.Point(218, 86)
        Me.objlblcolor.Name = "objlblcolor"
        Me.objlblcolor.Size = New System.Drawing.Size(39, 21)
        Me.objlblcolor.TabIndex = 255
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 503)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(832, 55)
        Me.objFooter.TabIndex = 15
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(620, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(723, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 8
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'pnlLeaveViewer
        '
        Me.pnlLeaveViewer.Controls.Add(Me.gbLeaveViewer)
        Me.pnlLeaveViewer.Location = New System.Drawing.Point(10, 64)
        Me.pnlLeaveViewer.Name = "pnlLeaveViewer"
        Me.pnlLeaveViewer.Size = New System.Drawing.Size(808, 169)
        Me.pnlLeaveViewer.TabIndex = 10
        '
        'gbLeaveViewer
        '
        Me.gbLeaveViewer.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveViewer.Checked = False
        Me.gbLeaveViewer.CollapseAllExceptThis = False
        Me.gbLeaveViewer.CollapsedHoverImage = Nothing
        Me.gbLeaveViewer.CollapsedNormalImage = Nothing
        Me.gbLeaveViewer.CollapsedPressedImage = Nothing
        Me.gbLeaveViewer.CollapseOnLoad = False
        Me.gbLeaveViewer.Controls.Add(Me.lnkAllocation)
        Me.gbLeaveViewer.Controls.Add(Me.objbtnReset)
        Me.gbLeaveViewer.Controls.Add(Me.objbtnSearch)
        Me.gbLeaveViewer.Controls.Add(Me.txtEmpviewer)
        Me.gbLeaveViewer.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbLeaveViewer.Controls.Add(Me.LblEmpViewer)
        Me.gbLeaveViewer.Controls.Add(Me.dtpToDate)
        Me.gbLeaveViewer.Controls.Add(Me.dtpFromDate)
        Me.gbLeaveViewer.Controls.Add(Me.lblToDate)
        Me.gbLeaveViewer.Controls.Add(Me.lblFromDate)
        Me.gbLeaveViewer.Controls.Add(Me.pnlLeaveLegends)
        Me.gbLeaveViewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbLeaveViewer.ExpandedHoverImage = Nothing
        Me.gbLeaveViewer.ExpandedNormalImage = Nothing
        Me.gbLeaveViewer.ExpandedPressedImage = Nothing
        Me.gbLeaveViewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveViewer.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveViewer.HeaderHeight = 25
        Me.gbLeaveViewer.HeaderMessage = ""
        Me.gbLeaveViewer.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveViewer.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveViewer.HeightOnCollapse = 0
        Me.gbLeaveViewer.LeftTextSpace = 0
        Me.gbLeaveViewer.Location = New System.Drawing.Point(0, 0)
        Me.gbLeaveViewer.Name = "gbLeaveViewer"
        Me.gbLeaveViewer.OpenHeight = 300
        Me.gbLeaveViewer.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveViewer.ShowBorder = True
        Me.gbLeaveViewer.ShowCheckBox = False
        Me.gbLeaveViewer.ShowCollapseButton = False
        Me.gbLeaveViewer.ShowDefaultBorderColor = True
        Me.gbLeaveViewer.ShowDownButton = False
        Me.gbLeaveViewer.ShowHeader = True
        Me.gbLeaveViewer.Size = New System.Drawing.Size(808, 169)
        Me.gbLeaveViewer.TabIndex = 7
        Me.gbLeaveViewer.Temp = 0
        Me.gbLeaveViewer.Text = "Leave Viewer"
        Me.gbLeaveViewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(665, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 235
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(782, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 259
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(759, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 258
        Me.objbtnSearch.TabStop = False
        '
        'txtEmpviewer
        '
        Me.txtEmpviewer.BackColor = System.Drawing.Color.White
        Me.txtEmpviewer.Flags = 0
        Me.txtEmpviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpviewer.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmpviewer.Location = New System.Drawing.Point(87, 32)
        Me.txtEmpviewer.Name = "txtEmpviewer"
        Me.txtEmpviewer.ReadOnly = True
        Me.txtEmpviewer.Size = New System.Drawing.Size(231, 21)
        Me.txtEmpviewer.TabIndex = 262
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(324, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 263
        '
        'LblEmpViewer
        '
        Me.LblEmpViewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmpViewer.Location = New System.Drawing.Point(3, 36)
        Me.LblEmpViewer.Name = "LblEmpViewer"
        Me.LblEmpViewer.Size = New System.Drawing.Size(78, 15)
        Me.LblEmpViewer.TabIndex = 261
        Me.LblEmpViewer.Text = "Employee"
        '
        'dtpToDate
        '
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(87, 86)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.ShowCheckBox = True
        Me.dtpToDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpToDate.TabIndex = 256
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(87, 59)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.ShowCheckBox = True
        Me.dtpFromDate.Size = New System.Drawing.Size(119, 21)
        Me.dtpFromDate.TabIndex = 255
        '
        'lblToDate
        '
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(3, 90)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(78, 15)
        Me.lblToDate.TabIndex = 254
        Me.lblToDate.Text = "To Date"
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(3, 63)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(78, 15)
        Me.lblFromDate.TabIndex = 253
        Me.lblFromDate.Text = "From Date"
        '
        'pnlLeaveLegends
        '
        Me.pnlLeaveLegends.AutoScroll = True
        Me.pnlLeaveLegends.Controls.Add(Me.lvLeaveCodes)
        Me.pnlLeaveLegends.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlLeaveLegends.Location = New System.Drawing.Point(349, 29)
        Me.pnlLeaveLegends.Name = "pnlLeaveLegends"
        Me.pnlLeaveLegends.Size = New System.Drawing.Size(454, 135)
        Me.pnlLeaveLegends.TabIndex = 251
        '
        'lvLeaveCodes
        '
        Me.lvLeaveCodes.BackColor = System.Drawing.SystemColors.Window
        Me.lvLeaveCodes.BackColorOnChecked = False
        Me.lvLeaveCodes.ColumnHeaders = Nothing
        Me.lvLeaveCodes.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLeavetype})
        Me.lvLeaveCodes.CompulsoryColumns = ""
        Me.lvLeaveCodes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvLeaveCodes.FullRowSelect = True
        Me.lvLeaveCodes.GridLines = True
        Me.lvLeaveCodes.GroupingColumn = Nothing
        Me.lvLeaveCodes.HideSelection = False
        Me.lvLeaveCodes.Location = New System.Drawing.Point(0, 0)
        Me.lvLeaveCodes.MinColumnWidth = 50
        Me.lvLeaveCodes.MultiSelect = False
        Me.lvLeaveCodes.Name = "lvLeaveCodes"
        Me.lvLeaveCodes.OptionalColumns = ""
        Me.lvLeaveCodes.ShowMoreItem = False
        Me.lvLeaveCodes.ShowSaveItem = False
        Me.lvLeaveCodes.ShowSelectAll = True
        Me.lvLeaveCodes.ShowSizeAllColumnsToFit = True
        Me.lvLeaveCodes.Size = New System.Drawing.Size(454, 135)
        Me.lvLeaveCodes.Sortable = True
        Me.lvLeaveCodes.TabIndex = 0
        Me.lvLeaveCodes.UseCompatibleStateImageBehavior = False
        Me.lvLeaveCodes.View = System.Windows.Forms.View.Details
        '
        'colhLeavetype
        '
        Me.colhLeavetype.Tag = "colhLeavetype"
        Me.colhLeavetype.Text = "Leave Type"
        Me.colhLeavetype.Width = 450
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Sun"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 30
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = "Mon"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 30
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.Frozen = True
        Me.DataGridViewTextBoxColumn4.HeaderText = "Tue"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 30
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.Frozen = True
        Me.DataGridViewTextBoxColumn5.HeaderText = "Wed"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 30
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.Frozen = True
        Me.DataGridViewTextBoxColumn6.HeaderText = "Thu"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 30
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.Frozen = True
        Me.DataGridViewTextBoxColumn7.HeaderText = "Fri"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 30
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.Frozen = True
        Me.DataGridViewTextBoxColumn8.HeaderText = "Sat"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 30
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.Frozen = True
        Me.DataGridViewTextBoxColumn9.HeaderText = "Sun"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 30
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.Frozen = True
        Me.DataGridViewTextBoxColumn10.HeaderText = "Mon"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Width = 30
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.Frozen = True
        Me.DataGridViewTextBoxColumn11.HeaderText = "Tue"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Width = 30
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.Frozen = True
        Me.DataGridViewTextBoxColumn12.HeaderText = "Wed"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Width = 30
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.Frozen = True
        Me.DataGridViewTextBoxColumn13.HeaderText = "Thu"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.Width = 30
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.Frozen = True
        Me.DataGridViewTextBoxColumn14.HeaderText = "Fri"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.Width = 30
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.Frozen = True
        Me.DataGridViewTextBoxColumn15.HeaderText = "Sat"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.Width = 30
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.Frozen = True
        Me.DataGridViewTextBoxColumn16.HeaderText = "Sun"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.Width = 30
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.Frozen = True
        Me.DataGridViewTextBoxColumn17.HeaderText = "Mon"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.Width = 30
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.Frozen = True
        Me.DataGridViewTextBoxColumn18.HeaderText = "Tue"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.Width = 30
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.Frozen = True
        Me.DataGridViewTextBoxColumn19.HeaderText = "Wed"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.Width = 30
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.Frozen = True
        Me.DataGridViewTextBoxColumn20.HeaderText = "Thu"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.Width = 30
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.Frozen = True
        Me.DataGridViewTextBoxColumn21.HeaderText = "Fri"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.Width = 30
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.Frozen = True
        Me.DataGridViewTextBoxColumn22.HeaderText = "Sat"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.Width = 30
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.Frozen = True
        Me.DataGridViewTextBoxColumn23.HeaderText = "Sun"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.Width = 30
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.Frozen = True
        Me.DataGridViewTextBoxColumn24.HeaderText = "Mon"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.Width = 30
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.Frozen = True
        Me.DataGridViewTextBoxColumn25.HeaderText = "Tue"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.Width = 30
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.Frozen = True
        Me.DataGridViewTextBoxColumn26.HeaderText = "Wed"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.Width = 30
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.Frozen = True
        Me.DataGridViewTextBoxColumn27.HeaderText = "Thu"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.Width = 30
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.Frozen = True
        Me.DataGridViewTextBoxColumn28.HeaderText = "Fri"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.Width = 30
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.Frozen = True
        Me.DataGridViewTextBoxColumn29.HeaderText = "Sat"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.Width = 30
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.Frozen = True
        Me.DataGridViewTextBoxColumn30.HeaderText = "Sun"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.Width = 30
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.Frozen = True
        Me.DataGridViewTextBoxColumn31.HeaderText = "Mon"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.Width = 30
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.Frozen = True
        Me.DataGridViewTextBoxColumn32.HeaderText = "Tue"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.Width = 30
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.HeaderText = "Wed"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.Width = 30
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.HeaderText = "Thu"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.Width = 30
        '
        'frmLeaveIssue_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(832, 558)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeaveIssue_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Leave Issue"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.dgLeaveIssue, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlLeaveIssueInfo.ResumeLayout(False)
        Me.gbLeaveIssue.ResumeLayout(False)
        Me.gbLeaveIssue.PerformLayout()
        Me.objTlbLeaveBalance.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.pnlLeaveViewer.ResumeLayout(False)
        Me.gbLeaveViewer.ResumeLayout(False)
        Me.gbLeaveViewer.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlLeaveLegends.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbLeaveIssue As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboFormNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboLeaveYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveType As System.Windows.Forms.Label
    Friend WithEvents lblLeaveYear As System.Windows.Forms.Label
    Friend WithEvents lblFormNo As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveEndDate As System.Windows.Forms.Label
    Friend WithEvents txtAsDate As System.Windows.Forms.TextBox
    Friend WithEvents dgLeaveIssue As System.Windows.Forms.DataGridView
    Friend WithEvents objTlbLeaveBalance As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents objlblBalanceValue As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents objlblIssuedToDateValue As System.Windows.Forms.Label
    Friend WithEvents lblTotalIssuedToDate As System.Windows.Forms.Label
    Friend WithEvents objlblLastYearIssuedValue As System.Windows.Forms.Label
    Friend WithEvents lblLastYearIssued As System.Windows.Forms.Label
    Friend WithEvents objlblAccruedToDateValue As System.Windows.Forms.Label
    Friend WithEvents lblToDateAccrued As System.Windows.Forms.Label
    Friend WithEvents objlblLastYearAccruedValue As System.Windows.Forms.Label
    Friend WithEvents lblLastYearAccrued As System.Windows.Forms.Label
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents objlblDateRange As System.Windows.Forms.Label
    Friend WithEvents objlblcolor As System.Windows.Forms.Label
    Friend WithEvents pnlLeaveIssueInfo As System.Windows.Forms.Panel
    Friend WithEvents pnlLeaveViewer As System.Windows.Forms.Panel
    Friend WithEvents gbLeaveViewer As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlLeaveLegends As System.Windows.Forms.Panel
    Friend WithEvents lvLeaveCodes As eZee.Common.eZeeListView
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtEmployee As System.Windows.Forms.TextBox
    Friend WithEvents colhLeavetype As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblTotalLeaveDays As System.Windows.Forms.Label
    Friend WithEvents objTotalLeaveDays As System.Windows.Forms.Label
    Friend WithEvents LblEmpViewer As System.Windows.Forms.Label
    Friend WithEvents txtEmpviewer As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkPreviewDocument As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents LblLeaveBF As System.Windows.Forms.Label
    Friend WithEvents objlblLeaveBFvalue As System.Windows.Forms.Label
    Friend WithEvents LblTotalAdjustment As System.Windows.Forms.Label
    Friend WithEvents objlblTotalAdjustment As System.Windows.Forms.Label
End Class

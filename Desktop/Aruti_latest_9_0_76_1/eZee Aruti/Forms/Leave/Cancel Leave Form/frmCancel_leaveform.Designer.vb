﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCancel_leaveform
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCancel_leaveform))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbLeaveForm = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkCancelExpense = New System.Windows.Forms.LinkLabel
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvLeavedate = New eZee.Common.eZeeListView(Me.components)
        Me.colhLeavedate = New System.Windows.Forms.ColumnHeader
        Me.EZeeGradientButton1 = New eZee.Common.eZeeGradientButton
        Me.cboFormNo = New System.Windows.Forms.ComboBox
        Me.lblLeaveFormDate = New System.Windows.Forms.Label
        Me.objbtnSearchLeaveType = New eZee.Common.eZeeGradientButton
        Me.txtCancelFormNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblCancelFormNo = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.txtReason = New eZee.TextBox.AlphanumericTextBox
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.lblFormNo = New System.Windows.Forms.Label
        Me.lblReason = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnAttachDocument = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objTlbLeaveBalance = New System.Windows.Forms.TableLayoutPanel
        Me.objlblBalanceAsonDateValue = New System.Windows.Forms.Label
        Me.LblBalanceAsonDate = New System.Windows.Forms.Label
        Me.lblAmount = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblBalance = New System.Windows.Forms.Label
        Me.LblTotalAdjustment = New System.Windows.Forms.Label
        Me.lblTotalIssuedToDate = New System.Windows.Forms.Label
        Me.lblToDateAccrued = New System.Windows.Forms.Label
        Me.LblLeaveBF = New System.Windows.Forms.Label
        Me.objlblBalanceValue = New System.Windows.Forms.Label
        Me.objlblTotalAdjustment = New System.Windows.Forms.Label
        Me.objlblIssuedToDateValue = New System.Windows.Forms.Label
        Me.objlblAccruedToDateValue = New System.Windows.Forms.Label
        Me.objlblLeaveBFvalue = New System.Windows.Forms.Label
        Me.objlblLeaveDate = New System.Windows.Forms.Label
        Me.lblAs = New System.Windows.Forms.Label
        Me.lnLeaveBalance = New eZee.Common.eZeeLine
        Me.gbLeaveForm.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.objTlbLeaveBalance.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(891, 60)
        Me.eZeeHeader.TabIndex = 7
        Me.eZeeHeader.Title = "Cancel Leave Form"
        '
        'gbLeaveForm
        '
        Me.gbLeaveForm.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveForm.Checked = False
        Me.gbLeaveForm.CollapseAllExceptThis = False
        Me.gbLeaveForm.CollapsedHoverImage = Nothing
        Me.gbLeaveForm.CollapsedNormalImage = Nothing
        Me.gbLeaveForm.CollapsedPressedImage = Nothing
        Me.gbLeaveForm.CollapseOnLoad = False
        Me.gbLeaveForm.Controls.Add(Me.objlblLeaveDate)
        Me.gbLeaveForm.Controls.Add(Me.lblAs)
        Me.gbLeaveForm.Controls.Add(Me.lnLeaveBalance)
        Me.gbLeaveForm.Controls.Add(Me.objTlbLeaveBalance)
        Me.gbLeaveForm.Controls.Add(Me.lnkCancelExpense)
        Me.gbLeaveForm.Controls.Add(Me.chkSelectAll)
        Me.gbLeaveForm.Controls.Add(Me.lvLeavedate)
        Me.gbLeaveForm.Controls.Add(Me.EZeeGradientButton1)
        Me.gbLeaveForm.Controls.Add(Me.cboFormNo)
        Me.gbLeaveForm.Controls.Add(Me.lblLeaveFormDate)
        Me.gbLeaveForm.Controls.Add(Me.objbtnSearchLeaveType)
        Me.gbLeaveForm.Controls.Add(Me.txtCancelFormNo)
        Me.gbLeaveForm.Controls.Add(Me.lblCancelFormNo)
        Me.gbLeaveForm.Controls.Add(Me.cboEmployee)
        Me.gbLeaveForm.Controls.Add(Me.cboLeaveType)
        Me.gbLeaveForm.Controls.Add(Me.txtReason)
        Me.gbLeaveForm.Controls.Add(Me.lblLeaveType)
        Me.gbLeaveForm.Controls.Add(Me.lblFormNo)
        Me.gbLeaveForm.Controls.Add(Me.lblReason)
        Me.gbLeaveForm.Controls.Add(Me.lblEmployee)
        Me.gbLeaveForm.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbLeaveForm.ExpandedHoverImage = Nothing
        Me.gbLeaveForm.ExpandedNormalImage = Nothing
        Me.gbLeaveForm.ExpandedPressedImage = Nothing
        Me.gbLeaveForm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveForm.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveForm.HeaderHeight = 25
        Me.gbLeaveForm.HeaderMessage = ""
        Me.gbLeaveForm.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveForm.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveForm.HeightOnCollapse = 0
        Me.gbLeaveForm.LeftTextSpace = 0
        Me.gbLeaveForm.Location = New System.Drawing.Point(7, 65)
        Me.gbLeaveForm.Name = "gbLeaveForm"
        Me.gbLeaveForm.OpenHeight = 300
        Me.gbLeaveForm.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveForm.ShowBorder = True
        Me.gbLeaveForm.ShowCheckBox = False
        Me.gbLeaveForm.ShowCollapseButton = False
        Me.gbLeaveForm.ShowDefaultBorderColor = True
        Me.gbLeaveForm.ShowDownButton = False
        Me.gbLeaveForm.ShowHeader = True
        Me.gbLeaveForm.Size = New System.Drawing.Size(880, 223)
        Me.gbLeaveForm.TabIndex = 8
        Me.gbLeaveForm.Temp = 0
        Me.gbLeaveForm.Text = "Cancel Leave Form Information"
        Me.gbLeaveForm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkCancelExpense
        '
        Me.lnkCancelExpense.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkCancelExpense.Location = New System.Drawing.Point(250, 114)
        Me.lnkCancelExpense.Name = "lnkCancelExpense"
        Me.lnkCancelExpense.Size = New System.Drawing.Size(164, 18)
        Me.lnkCancelExpense.TabIndex = 270
        Me.lnkCancelExpense.TabStop = True
        Me.lnkCancelExpense.Text = "Cancel Expense"
        Me.lnkCancelExpense.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkCancelExpense.Visible = False
        '
        'chkSelectAll
        '
        Me.chkSelectAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelectAll.Location = New System.Drawing.Point(431, 41)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(13, 13)
        Me.chkSelectAll.TabIndex = 268
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'lvLeavedate
        '
        Me.lvLeavedate.BackColorOnChecked = True
        Me.lvLeavedate.CheckBoxes = True
        Me.lvLeavedate.ColumnHeaders = Nothing
        Me.lvLeavedate.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLeavedate})
        Me.lvLeavedate.CompulsoryColumns = ""
        Me.lvLeavedate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvLeavedate.FullRowSelect = True
        Me.lvLeavedate.GridLines = True
        Me.lvLeavedate.GroupingColumn = Nothing
        Me.lvLeavedate.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvLeavedate.HideSelection = False
        Me.lvLeavedate.Location = New System.Drawing.Point(423, 36)
        Me.lvLeavedate.MinColumnWidth = 50
        Me.lvLeavedate.MultiSelect = False
        Me.lvLeavedate.Name = "lvLeavedate"
        Me.lvLeavedate.OptionalColumns = ""
        Me.lvLeavedate.ShowMoreItem = False
        Me.lvLeavedate.ShowSaveItem = False
        Me.lvLeavedate.ShowSelectAll = True
        Me.lvLeavedate.ShowSizeAllColumnsToFit = True
        Me.lvLeavedate.Size = New System.Drawing.Size(125, 180)
        Me.lvLeavedate.Sortable = True
        Me.lvLeavedate.TabIndex = 10
        Me.lvLeavedate.UseCompatibleStateImageBehavior = False
        Me.lvLeavedate.View = System.Windows.Forms.View.Details
        '
        'colhLeavedate
        '
        Me.colhLeavedate.Tag = "colhLeavedate"
        Me.colhLeavedate.Text = ""
        Me.colhLeavedate.Width = 120
        '
        'EZeeGradientButton1
        '
        Me.EZeeGradientButton1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeGradientButton1.BackColor1 = System.Drawing.Color.Transparent
        Me.EZeeGradientButton1.BackColor2 = System.Drawing.Color.Transparent
        Me.EZeeGradientButton1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.EZeeGradientButton1.BorderSelected = False
        Me.EZeeGradientButton1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.EZeeGradientButton1.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.EZeeGradientButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.EZeeGradientButton1.Location = New System.Drawing.Point(296, 60)
        Me.EZeeGradientButton1.Name = "EZeeGradientButton1"
        Me.EZeeGradientButton1.Size = New System.Drawing.Size(21, 21)
        Me.EZeeGradientButton1.TabIndex = 266
        Me.EZeeGradientButton1.Visible = False
        '
        'cboFormNo
        '
        Me.cboFormNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFormNo.FormattingEnabled = True
        Me.cboFormNo.Location = New System.Drawing.Point(107, 60)
        Me.cboFormNo.Name = "cboFormNo"
        Me.cboFormNo.Size = New System.Drawing.Size(183, 21)
        Me.cboFormNo.TabIndex = 2
        '
        'lblLeaveFormDate
        '
        Me.lblLeaveFormDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveFormDate.Location = New System.Drawing.Point(322, 36)
        Me.lblLeaveFormDate.Name = "lblLeaveFormDate"
        Me.lblLeaveFormDate.Size = New System.Drawing.Size(101, 15)
        Me.lblLeaveFormDate.TabIndex = 262
        Me.lblLeaveFormDate.Text = "Leave Form Date"
        '
        'objbtnSearchLeaveType
        '
        Me.objbtnSearchLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeaveType.BorderSelected = False
        Me.objbtnSearchLeaveType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeaveType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeaveType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeaveType.Location = New System.Drawing.Point(296, 86)
        Me.objbtnSearchLeaveType.Name = "objbtnSearchLeaveType"
        Me.objbtnSearchLeaveType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeaveType.TabIndex = 261
        Me.objbtnSearchLeaveType.Visible = False
        '
        'txtCancelFormNo
        '
        Me.txtCancelFormNo.Flags = 0
        Me.txtCancelFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCancelFormNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCancelFormNo.Location = New System.Drawing.Point(107, 113)
        Me.txtCancelFormNo.Name = "txtCancelFormNo"
        Me.txtCancelFormNo.Size = New System.Drawing.Size(131, 21)
        Me.txtCancelFormNo.TabIndex = 4
        '
        'lblCancelFormNo
        '
        Me.lblCancelFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCancelFormNo.Location = New System.Drawing.Point(8, 116)
        Me.lblCancelFormNo.Name = "lblCancelFormNo"
        Me.lblCancelFormNo.Size = New System.Drawing.Size(92, 15)
        Me.lblCancelFormNo.TabIndex = 260
        Me.lblCancelFormNo.Text = "Cancel Form No"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(107, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(183, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(107, 86)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(183, 21)
        Me.cboLeaveType.TabIndex = 3
        '
        'txtReason
        '
        Me.txtReason.Flags = 0
        Me.txtReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReason.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReason.Location = New System.Drawing.Point(107, 141)
        Me.txtReason.Multiline = True
        Me.txtReason.Name = "txtReason"
        Me.txtReason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtReason.Size = New System.Drawing.Size(307, 75)
        Me.txtReason.TabIndex = 6
        '
        'lblLeaveType
        '
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(8, 89)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(92, 15)
        Me.lblLeaveType.TabIndex = 246
        Me.lblLeaveType.Text = "Leave Type"
        '
        'lblFormNo
        '
        Me.lblFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormNo.Location = New System.Drawing.Point(8, 63)
        Me.lblFormNo.Name = "lblFormNo"
        Me.lblFormNo.Size = New System.Drawing.Size(92, 15)
        Me.lblFormNo.TabIndex = 224
        Me.lblFormNo.Text = "Form No"
        '
        'lblReason
        '
        Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReason.Location = New System.Drawing.Point(8, 141)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(92, 15)
        Me.lblReason.TabIndex = 244
        Me.lblReason.Text = "Reason"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(92, 15)
        Me.lblEmployee.TabIndex = 217
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(295, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        Me.objbtnSearchEmployee.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnAttachDocument)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 292)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(891, 55)
        Me.objFooter.TabIndex = 9
        '
        'btnAttachDocument
        '
        Me.btnAttachDocument.BackColor = System.Drawing.Color.White
        Me.btnAttachDocument.BackgroundImage = CType(resources.GetObject("btnAttachDocument.BackgroundImage"), System.Drawing.Image)
        Me.btnAttachDocument.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAttachDocument.BorderColor = System.Drawing.Color.Empty
        Me.btnAttachDocument.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAttachDocument.FlatAppearance.BorderSize = 0
        Me.btnAttachDocument.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAttachDocument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAttachDocument.ForeColor = System.Drawing.Color.Black
        Me.btnAttachDocument.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAttachDocument.GradientForeColor = System.Drawing.Color.Black
        Me.btnAttachDocument.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAttachDocument.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAttachDocument.Location = New System.Drawing.Point(7, 13)
        Me.btnAttachDocument.Name = "btnAttachDocument"
        Me.btnAttachDocument.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAttachDocument.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAttachDocument.Size = New System.Drawing.Size(131, 30)
        Me.btnAttachDocument.TabIndex = 9
        Me.btnAttachDocument.Text = "&Browse"
        Me.btnAttachDocument.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(679, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(782, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 8
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objTlbLeaveBalance
        '
        Me.objTlbLeaveBalance.BackColor = System.Drawing.SystemColors.Control
        Me.objTlbLeaveBalance.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.objTlbLeaveBalance.ColumnCount = 2
        Me.objTlbLeaveBalance.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.22581!))
        Me.objTlbLeaveBalance.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.77419!))
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblBalanceAsonDateValue, 1, 5)
        Me.objTlbLeaveBalance.Controls.Add(Me.LblBalanceAsonDate, 0, 5)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblAmount, 1, 0)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblDescription, 0, 0)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblBalance, 0, 6)
        Me.objTlbLeaveBalance.Controls.Add(Me.LblTotalAdjustment, 0, 4)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblTotalIssuedToDate, 0, 3)
        Me.objTlbLeaveBalance.Controls.Add(Me.lblToDateAccrued, 0, 2)
        Me.objTlbLeaveBalance.Controls.Add(Me.LblLeaveBF, 0, 1)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblBalanceValue, 1, 6)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblTotalAdjustment, 1, 4)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblIssuedToDateValue, 1, 3)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblAccruedToDateValue, 1, 2)
        Me.objTlbLeaveBalance.Controls.Add(Me.objlblLeaveBFvalue, 1, 1)
        Me.objTlbLeaveBalance.Location = New System.Drawing.Point(554, 65)
        Me.objTlbLeaveBalance.Name = "objTlbLeaveBalance"
        Me.objTlbLeaveBalance.RowCount = 7
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objTlbLeaveBalance.Size = New System.Drawing.Size(319, 149)
        Me.objTlbLeaveBalance.TabIndex = 272
        '
        'objlblBalanceAsonDateValue
        '
        Me.objlblBalanceAsonDateValue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblBalanceAsonDateValue.Location = New System.Drawing.Point(236, 106)
        Me.objlblBalanceAsonDateValue.Name = "objlblBalanceAsonDateValue"
        Me.objlblBalanceAsonDateValue.Size = New System.Drawing.Size(79, 20)
        Me.objlblBalanceAsonDateValue.TabIndex = 17
        Me.objlblBalanceAsonDateValue.Text = "0"
        Me.objlblBalanceAsonDateValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblBalanceAsonDate
        '
        Me.LblBalanceAsonDate.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LblBalanceAsonDate.Location = New System.Drawing.Point(4, 106)
        Me.LblBalanceAsonDate.Name = "LblBalanceAsonDate"
        Me.LblBalanceAsonDate.Size = New System.Drawing.Size(225, 20)
        Me.LblBalanceAsonDate.TabIndex = 16
        Me.LblBalanceAsonDate.Text = "Balance As on Date"
        Me.LblBalanceAsonDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAmount
        '
        Me.lblAmount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblAmount.Location = New System.Drawing.Point(236, 1)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(79, 20)
        Me.lblAmount.TabIndex = 1
        Me.lblAmount.Text = "Amount"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDescription
        '
        Me.lblDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblDescription.Location = New System.Drawing.Point(4, 1)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(225, 20)
        Me.lblDescription.TabIndex = 0
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBalance
        '
        Me.lblBalance.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblBalance.Location = New System.Drawing.Point(4, 127)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(225, 21)
        Me.lblBalance.TabIndex = 10
        Me.lblBalance.Text = "Total Balance"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblTotalAdjustment
        '
        Me.LblTotalAdjustment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LblTotalAdjustment.Location = New System.Drawing.Point(4, 85)
        Me.LblTotalAdjustment.Name = "LblTotalAdjustment"
        Me.LblTotalAdjustment.Size = New System.Drawing.Size(225, 20)
        Me.LblTotalAdjustment.TabIndex = 12
        Me.LblTotalAdjustment.Text = "Total Adjustment"
        Me.LblTotalAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalIssuedToDate
        '
        Me.lblTotalIssuedToDate.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblTotalIssuedToDate.Location = New System.Drawing.Point(4, 64)
        Me.lblTotalIssuedToDate.Name = "lblTotalIssuedToDate"
        Me.lblTotalIssuedToDate.Size = New System.Drawing.Size(225, 20)
        Me.lblTotalIssuedToDate.TabIndex = 8
        Me.lblTotalIssuedToDate.Text = "Total Issued "
        Me.lblTotalIssuedToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblToDateAccrued
        '
        Me.lblToDateAccrued.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblToDateAccrued.Location = New System.Drawing.Point(4, 43)
        Me.lblToDateAccrued.Name = "lblToDateAccrued"
        Me.lblToDateAccrued.Size = New System.Drawing.Size(225, 20)
        Me.lblToDateAccrued.TabIndex = 4
        Me.lblToDateAccrued.Text = "Total Accrued To Date"
        Me.lblToDateAccrued.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblLeaveBF
        '
        Me.LblLeaveBF.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LblLeaveBF.Location = New System.Drawing.Point(4, 22)
        Me.LblLeaveBF.Name = "LblLeaveBF"
        Me.LblLeaveBF.Size = New System.Drawing.Size(225, 20)
        Me.LblLeaveBF.TabIndex = 14
        Me.LblLeaveBF.Text = "Leave BF"
        Me.LblLeaveBF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblBalanceValue
        '
        Me.objlblBalanceValue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblBalanceValue.Location = New System.Drawing.Point(236, 127)
        Me.objlblBalanceValue.Name = "objlblBalanceValue"
        Me.objlblBalanceValue.Size = New System.Drawing.Size(79, 21)
        Me.objlblBalanceValue.TabIndex = 11
        Me.objlblBalanceValue.Text = "0"
        Me.objlblBalanceValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblTotalAdjustment
        '
        Me.objlblTotalAdjustment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblTotalAdjustment.Location = New System.Drawing.Point(236, 85)
        Me.objlblTotalAdjustment.Name = "objlblTotalAdjustment"
        Me.objlblTotalAdjustment.Size = New System.Drawing.Size(79, 20)
        Me.objlblTotalAdjustment.TabIndex = 13
        Me.objlblTotalAdjustment.Text = "0"
        Me.objlblTotalAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblIssuedToDateValue
        '
        Me.objlblIssuedToDateValue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblIssuedToDateValue.Location = New System.Drawing.Point(236, 64)
        Me.objlblIssuedToDateValue.Name = "objlblIssuedToDateValue"
        Me.objlblIssuedToDateValue.Size = New System.Drawing.Size(79, 20)
        Me.objlblIssuedToDateValue.TabIndex = 9
        Me.objlblIssuedToDateValue.Text = "0"
        Me.objlblIssuedToDateValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblAccruedToDateValue
        '
        Me.objlblAccruedToDateValue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblAccruedToDateValue.Location = New System.Drawing.Point(236, 43)
        Me.objlblAccruedToDateValue.Name = "objlblAccruedToDateValue"
        Me.objlblAccruedToDateValue.Size = New System.Drawing.Size(79, 20)
        Me.objlblAccruedToDateValue.TabIndex = 5
        Me.objlblAccruedToDateValue.Text = "0"
        Me.objlblAccruedToDateValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblLeaveBFvalue
        '
        Me.objlblLeaveBFvalue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblLeaveBFvalue.Location = New System.Drawing.Point(236, 22)
        Me.objlblLeaveBFvalue.Name = "objlblLeaveBFvalue"
        Me.objlblLeaveBFvalue.Size = New System.Drawing.Size(79, 20)
        Me.objlblLeaveBFvalue.TabIndex = 15
        Me.objlblLeaveBFvalue.Text = "0"
        Me.objlblLeaveBFvalue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblLeaveDate
        '
        Me.objlblLeaveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblLeaveDate.Location = New System.Drawing.Point(788, 40)
        Me.objlblLeaveDate.Name = "objlblLeaveDate"
        Me.objlblLeaveDate.Size = New System.Drawing.Size(79, 13)
        Me.objlblLeaveDate.TabIndex = 275
        Me.objlblLeaveDate.Text = "31/12/2010"
        '
        'lblAs
        '
        Me.lblAs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAs.Location = New System.Drawing.Point(745, 39)
        Me.lblAs.Name = "lblAs"
        Me.lblAs.Size = New System.Drawing.Size(34, 15)
        Me.lblAs.TabIndex = 274
        Me.lblAs.Text = "As"
        Me.lblAs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnLeaveBalance
        '
        Me.lnLeaveBalance.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnLeaveBalance.Location = New System.Drawing.Point(554, 38)
        Me.lnLeaveBalance.Name = "lnLeaveBalance"
        Me.lnLeaveBalance.Size = New System.Drawing.Size(183, 17)
        Me.lnLeaveBalance.TabIndex = 273
        Me.lnLeaveBalance.Text = "Leave Balance"
        '
        'frmCancel_leaveform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(891, 347)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbLeaveForm)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCancel_leaveform"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Cancel Leave Form"
        Me.gbLeaveForm.ResumeLayout(False)
        Me.gbLeaveForm.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.objTlbLeaveBalance.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbLeaveForm As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtCancelFormNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCancelFormNo As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveType As System.Windows.Forms.Label
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents lblFormNo As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblLeaveFormDate As System.Windows.Forms.Label
    Friend WithEvents txtReason As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchLeaveType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents EZeeGradientButton1 As eZee.Common.eZeeGradientButton
    Friend WithEvents cboFormNo As System.Windows.Forms.ComboBox
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvLeavedate As eZee.Common.eZeeListView
    Friend WithEvents colhLeavedate As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkCancelExpense As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAttachDocument As eZee.Common.eZeeLightButton
    Friend WithEvents objTlbLeaveBalance As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents objlblBalanceAsonDateValue As System.Windows.Forms.Label
    Friend WithEvents LblBalanceAsonDate As System.Windows.Forms.Label
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents LblTotalAdjustment As System.Windows.Forms.Label
    Friend WithEvents lblTotalIssuedToDate As System.Windows.Forms.Label
    Friend WithEvents lblToDateAccrued As System.Windows.Forms.Label
    Friend WithEvents LblLeaveBF As System.Windows.Forms.Label
    Friend WithEvents objlblBalanceValue As System.Windows.Forms.Label
    Friend WithEvents objlblTotalAdjustment As System.Windows.Forms.Label
    Friend WithEvents objlblIssuedToDateValue As System.Windows.Forms.Label
    Friend WithEvents objlblAccruedToDateValue As System.Windows.Forms.Label
    Friend WithEvents objlblLeaveBFvalue As System.Windows.Forms.Label
    Friend WithEvents objlblLeaveDate As System.Windows.Forms.Label
    Friend WithEvents lblAs As System.Windows.Forms.Label
    Friend WithEvents lnLeaveBalance As eZee.Common.eZeeLine
End Class

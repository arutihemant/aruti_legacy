﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmCancel_leaveform

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmCancel_leaveform"
    Private mblnCancel As Boolean = True
    Private objCancelForm As clsCancel_Leaveform
    Private menAction As enAction = enAction.ADD_ONE
    Private mintCancelFormunkid As Integer = -1
    Private mintEmployeeunkid As Integer = 0
    Private mintFormunkid As Integer = -1
    Private mintLeaveTypeunkid As Integer = -1
    Private mdtStartdate As DateTime
    Private mdtEnddate As DateTime
    Private mstrLeavedates As String = ""
    Private mintTotalIssuedDays As Integer = 0
    Private mintTotalCanceldays As Integer = 0
    Private dsList As DataSet = Nothing
    Dim objClaimReqestMaster As clsclaim_request_master = Nothing
    Private mdtCancelExpense As DataTable = Nothing
    Private mstrCancelExpenseRemark As String = ""

    'Pinkal (28-Nov-2017) -- Start
    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
    Dim mdtAttachement As DataTable = Nothing
    'Pinkal (28-Nov-2017) -- End


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnSkipForApproverFlow As Boolean = False
    'Pinkal (01-Oct-2018) -- End


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintCancelFormunkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Property"

    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public Property _Formunkid() As Integer
        Get
            Return mintFormunkid
        End Get
        Set(ByVal value As Integer)
            mintFormunkid = value
        End Set
    End Property

    Public Property _LeaveTypeunkid() As Integer
        Get
            Return mintLeaveTypeunkid
        End Get
        Set(ByVal value As Integer)
            mintLeaveTypeunkid = value
        End Set
    End Property

    Public Property _DataList() As DataSet
        Get
            Return dsList
        End Get
        Set(ByVal value As DataSet)
            dsList = value
        End Set
    End Property


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Public Property _SkipApproverFlow() As Boolean
        Get
            Return mblnSkipForApproverFlow
        End Get
        Set(ByVal value As Boolean)
            mblnSkipForApproverFlow = value
        End Set
    End Property
    'Pinkal (01-Oct-2018) -- End


#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub setColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            cboFormNo.BackColor = GUI.ColorComp
            cboLeaveType.BackColor = GUI.ColorComp
            txtCancelFormNo.BackColor = GUI.ColorComp
            txtReason.BackColor = GUI.ColorComp
            lvLeavedate.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim dtFill As DataTable = Nothing
        Try

            'FOR EMPLOYEE
            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If mintEmployeeunkid > 0 Then

            '    'Anjan (17 Apr 2012)-Start
            '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '    'dsFill = objEmployee.GetEmployeeList("Employee", False, Not ConfigParameter._Object._IsIncludeInactiveEmp, mintEmployeeunkid)
            '    If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '        dsFill = objEmployee.GetEmployeeList("Employee", False, , mintEmployeeunkid, , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            '    Else
            '        dsFill = objEmployee.GetEmployeeList("Employee", False, , mintEmployeeunkid, , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            '    End If
            '    'Anjan (17 Apr 2012)-End 
            'Else
            '    'Anjan (17 Apr 2012)-Start
            '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '    'dsFill = objEmployee.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            '    If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '        dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            '    Else
            '        dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            '    End If
            '    'Anjan (17 Apr 2012)-End 
            'End If

            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", CBool(IIf(mintEmployeeunkid > 0, False, True)), mintEmployeeunkid)
            'S.SANDEEP [04 JUN 2015] -- END

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsFill.Tables("Employee")


            'FOR LEAVE FORM
            dsFill = Nothing
            Dim objLeaveForm As New clsleaveform
            dsFill = objLeaveForm.getListForCombo("LeaveForm", 7, True)
            If mintFormunkid > 0 Then
                dtFill = New DataView(dsFill.Tables("LeaveForm"), "formunkid = " & mintFormunkid, "", DataViewRowState.CurrentRows).ToTable()
            Else
                dtFill = dsFill.Tables("LeaveForm")
            End If

            cboFormNo.ValueMember = "formunkid"
            cboFormNo.DisplayMember = "name"
            cboFormNo.DataSource = dtFill


            'FOR LEAVE TYPE
            dsFill = Nothing
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.GetList("LeaveType", True)

            If mintLeaveTypeunkid > 0 Then
                dtFill = New DataView(dsFill.Tables("LeaveType"), "leavetypeunkid = " & mintLeaveTypeunkid, "", DataViewRowState.CurrentRows).ToTable()
            Else
                dtFill = dsFill.Tables("LeaveType")
            End If

            cboLeaveType.ValueMember = "leavetypeunkid"
            cboLeaveType.DisplayMember = "leavename"
            cboLeaveType.DataSource = dtFill

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboEmployee.SelectedValue = mintEmployeeunkid
            cboFormNo.SelectedValue = mintFormunkid
            cboLeaveType.SelectedValue = mintLeaveTypeunkid
            GetLeaveDate()
            txtReason.Text = objCancelForm._Cancel_Reason

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            objlblLeaveDate.Text = ConfigParameter._Object._CurrentDateAndTime.Date.ToShortDateString()
            GetBalanceInfo()
            'Pinkal (10-Jun-2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetValue() As Boolean
        Try
            objCancelForm._Formunkid = CInt(cboFormNo.SelectedValue)
            objCancelForm._Cancelformno = txtCancelFormNo.Text.Trim
            objCancelForm._Cancel_Reason = txtReason.Text.Trim
            objCancelForm._Cancel_Trandate = ConfigParameter._Object._CurrentDateAndTime
            objCancelForm._Canceluserunkid = User._Object._Userunkid

            mstrLeavedates = ""
            'For i As Integer = 0 To chkLeaveDate.CheckedItems.Count - 1
            '    mstrLeavedates &= eZeeDate.convertDate(CDate(chkLeaveDate.CheckedItems(i)).Date) & ","
            'Next

            For i As Integer = 0 To lvLeavedate.CheckedItems.Count - 1
                mstrLeavedates &= eZeeDate.convertDate(CDate(lvLeavedate.CheckedItems(i).Text).Date) & ","
            Next


            If mstrLeavedates.Trim.Length > 0 Then
                mstrLeavedates = mstrLeavedates.Trim.Substring(0, mstrLeavedates.Trim.Length - 1)
            End If

            If objClaimReqestMaster._Crmasterunkid > 0 Then
                objCancelForm._CancelExpenseRemark = mstrCancelExpenseRemark
                objCancelForm._dtCancelExpense = mdtCancelExpense
                objCancelForm._ClaimMstId = objClaimReqestMaster._Crmasterunkid
                objCancelForm._IsPaymentApprovalwithLeaveApproval = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
            End If


            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            objCancelForm._dtAttachment = mdtAttachement
            'Pinkal (28-Nov-2017) -- End

            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objCancelForm._SkipForApproverFlow = mblnSkipForApproverFlow
            'Pinkal (01-Oct-2018) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub GetLeaveDate()
        Try
            'Dim objIssue As New clsleaveissue_Tran
            'dsList = objIssue.GetList("List", False, CInt(cboFormNo.SelectedValue))

            If dsList IsNot Nothing And dsList.Tables(0).Rows.Count > 0 Then

                'START FOR GET TOTAL ISSUE DAYS
                mintTotalIssuedDays = dsList.Tables(0).Rows.Count
                'END FOR GET TOTAL ISSUE DAYS

                Dim dsCancelList As DataSet = objCancelForm.GetList("List", CInt(cboFormNo.SelectedValue))

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    Dim drRow As DataRow() = dsCancelList.Tables(0).Select("cancel_leavedate = '" & dsList.Tables(0).Rows(i)("leavedate").ToString() & "'")
                    If drRow.Length = 0 Then


                        'Pinkal (28-MAY-2012) -- Start
                        'Enhancement : TRA Changes

                        If User._Object.Privilege._AllowToCancelPreviousDateLeave = True And User._Object.Privilege._AllowToCancelLeave = False Then

                            If eZeeDate.convertDate(dsList.Tables(0).Rows(i)("leavedate").ToString()).Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                                Dim lvItem As New ListViewItem
                                lvItem.Text = eZeeDate.convertDate(dsList.Tables(0).Rows(i)("leavedate").ToString()).Date.ToShortDateString()
                                lvLeavedate.Items.Add(lvItem)
                            End If

                        ElseIf User._Object.Privilege._AllowToCancelPreviousDateLeave = False And User._Object.Privilege._AllowToCancelLeave = True Then

                            If eZeeDate.convertDate(dsList.Tables(0).Rows(i)("leavedate").ToString()).Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then

                                'Pinkal (06-Feb-2012) -- Start
                                'Enhancement : TRA Changes

                                Dim lvItem As New ListViewItem
                                lvItem.Text = eZeeDate.convertDate(dsList.Tables(0).Rows(i)("leavedate").ToString()).Date.ToShortDateString()
                                lvLeavedate.Items.Add(lvItem)

                                'Pinkal (06-Feb-2012) -- End

                            End If

                        ElseIf User._Object.Privilege._AllowToCancelPreviousDateLeave = True And User._Object.Privilege._AllowToCancelLeave = True Then

                            Dim lvItem As New ListViewItem
                            lvItem.Text = eZeeDate.convertDate(dsList.Tables(0).Rows(i)("leavedate").ToString()).Date.ToShortDateString()
                            lvLeavedate.Items.Add(lvItem)

                        End If

                        'Pinkal (28-MAY-2012) -- End

                    Else
                        mintTotalCanceldays += 1
                    End If

                Next
                dsCancelList.Dispose()
                If lvLeavedate.Items.Count > 0 Then lvLeavedate.Items(0).Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLeaveDate", mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Sub


    'Pinkal (10-Jun-2020) -- Start
    'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.

    Private Sub GetBalanceInfo()
        Try
            Dim objLeaveAccrue As New clsleavebalance_tran
            Dim dsList As DataSet = Nothing


            Dim mstrDeductedTypeIds As String = ""
            Dim drRow As DataRowView = CType(cboLeaveType.SelectedItem, DataRowView)
            If drRow IsNot Nothing Then
                If CInt(drRow("deductfromlvtypeunkid")) > 0 Then
                    mstrDeductedTypeIds = mintLeaveTypeunkid.ToString() & "," & CInt(drRow("deductfromlvtypeunkid")).ToString()
                Else
                    mstrDeductedTypeIds = mintLeaveTypeunkid.ToString()
                End If
            End If

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                dsList = objLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, mstrDeductedTypeIds, CInt(cboEmployee.SelectedValue).ToString(), ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth)

            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                objLeaveAccrue._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
                objLeaveAccrue._DBEnddate = FinancialYear._Object._Database_End_Date.Date
                dsList = objLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, mstrDeductedTypeIds, CInt(cboEmployee.SelectedValue).ToString(), ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, Nothing, Nothing, True, True)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables("Balanceinfo").Rows.Count > 0 Then
                objlblLeaveBFvalue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveBF")).ToString("#0.00")
                objlblAccruedToDateValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")).ToString("#0.00")
                objlblIssuedToDateValue.Text = CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveEncashment"))).ToString("#0.00")
                objlblTotalAdjustment.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")).ToString("#0.00")
                objlblBalanceAsonDateValue.Text = CDec(CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveBF")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveAdjustment"))) - CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveEncashment")))).ToString("#0.00")
                objlblBalanceValue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Balance")).ToString("#0.00")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpAsOnDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (10-Jun-2020) -- End





#End Region

#Region " Form's Event(s) "

    Private Sub frmCancel_leaveform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Set_Logo(Me, gApplicationType)


            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(Me.Name)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End


            objCancelForm = New clsCancel_Leaveform

            Dim p As New Point(CInt(cboEmployee.Size.Width), 21)

            cboEmployee.DropDownStyle = ComboBoxStyle.Simple
            cboFormNo.DropDownStyle = ComboBoxStyle.Simple
            cboLeaveType.DropDownStyle = ComboBoxStyle.Simple

            cboEmployee.Size = CType(p, Drawing.Size)
            cboFormNo.Size = CType(p, Drawing.Size)
            cboLeaveType.Size = CType(p, Drawing.Size)


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            If ConfigParameter._Object._LeaveCancelFormNotype = 1 Then
                txtCancelFormNo.Enabled = False
            Else
                txtCancelFormNo.Enabled = True
            End If
            'Pinkal (03-May-2019) -- End


            FillCombo()
            GetValue()


            objClaimReqestMaster = New clsclaim_request_master

            'Pinkal (30-Mar-2020) -- Start
            'Enhancement - Changes Related to Retain Leave Expense when leave is cancel.
            If ConfigParameter._Object._AllowToCancelLeaveButRetainExpense = False Then
            objClaimReqestMaster._Crmasterunkid = objClaimReqestMaster.GetClaimRequestMstIDFromLeaveForm(CInt(cboFormNo.SelectedValue))
            If objClaimReqestMaster._Crmasterunkid > 0 Then lnkCancelExpense.Visible = True
            End If
            'Pinkal (30-Mar-2020) -- End

            lvLeavedate.GridLines = False
            txtCancelFormNo.Select()

            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            If mdtAttachement Is Nothing Then
                Dim objAttchement As New clsScan_Attach_Documents
                'S.SANDEEP |04-SEP-2021| -- START
                'Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
                Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), , , , , CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
                'S.SANDEEP |04-SEP-2021| -- END
                Dim mdtTran As DataTable = clsleaveform.GetLeaveForms(CInt(cboEmployee.SelectedValue), False, mintCancelFormunkid).Tables(0)
                Dim strTranIds As String = String.Join(",", mdtTran.AsEnumerable().Select(Function(x) x.Field(Of String)("cancelunkid").ToString()).ToArray())
                If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
                mdtAttachement = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.LEAVEFORMS & " AND transactionunkid IN (" & strTranIds & ")  AND form_name = '" & Me.Name & "'", "", DataViewRowState.CurrentRows).ToTable
                objAttchement = Nothing
            End If
            'Pinkal (28-Nov-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCancel_leaveform_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCancel_leaveform_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
            If cboEmployee.Focused Or cboFormNo.Focused Or cboLeaveType.Focused Then
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCancel_leaveform_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCancel_leaveform_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed

    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsCancel_Leaveform.SetMessages()
            objfrm._Other_ModuleNames = "clsCancel_Leaveform"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            'If txtCancelFormNo.Text.Trim.Length = 0 Then
            If ConfigParameter._Object._LeaveCancelFormNotype = 0 AndAlso txtCancelFormNo.Text.Trim.Length = 0 Then
                'Pinkal (03-May-2019) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Cancel Form No cannot be blank. Cancel Form No is required information."), enMsgBoxStyle.Information)
                txtCancelFormNo.Focus()
                Exit Sub

            ElseIf lvLeavedate.CheckedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Leave Date is compulsory information.Please Select atleast One Leave Date."), enMsgBoxStyle.Information)
                lvLeavedate.Select()
                Exit Sub

            ElseIf txtReason.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Reason cannot be blank. Reason is required information."), enMsgBoxStyle.Information)
                txtReason.Select()
                Exit Sub

            End If


            'Pinkal (15-Sep-2013) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._ClosePayrollPeriodIfLeaveIssue Then
                Dim objleaveform As New clsleaveform
                objleaveform._Formunkid = mintFormunkid
                Dim objMaster As New clsMasterData

                'Pinkal (20-Jan-2014) -- Start
                'Enhancement : Oman Changes
                'Dim mintPeriodId As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, objleaveform._Startdate.Date, , , True)
                Dim mintPeriodId As Integer = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, objleaveform._Startdate.Date)
                'Pinkal (20-Jan-2014) -- End


                If mintPeriodId > 0 Then
                    Dim objPeriod As New clscommom_period_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = mintPeriodId
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodId
                    'Sohail (21 Aug 2015) -- End
                    If objPeriod._Statusid = enStatusType.Close Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You can't cancel this leave for this employee.Reason: Period is already closed."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    ElseIf objPeriod._Statusid = enStatusType.Open Then

                        If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo = True Then
                            Dim objTnALeaveTran As New clsTnALeaveTran

                            'Pinkal (03-Jan-2014) -- Start
                            'Enhancement : Oman Changes

                            'If objTnALeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid, CStr(cboEmployee.SelectedValue), objPeriod._End_Date) Then
                            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't cancel this leave for this employee.Reason: Process Payroll is already done for last date of period in which leave dates are falling."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            '    Exit Sub
                            'End If

                            Dim mdtTnADate As DateTime = Nothing
                            If objPeriod._TnA_EndDate.Date < objleaveform._Startdate.Date Then
                                mdtTnADate = objleaveform._Startdate.Date
                            Else
                                mdtTnADate = objPeriod._TnA_EndDate.Date
                            End If

                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'If objTnALeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid, CStr(cboEmployee.SelectedValue), mdtTnADate.Date, enModuleReference.TnA) Then
                            If objTnALeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName), CStr(cboEmployee.SelectedValue), mdtTnADate.Date, enModuleReference.TnA) Then
                                'Sohail (21 Aug 2015) -- End
                                'Sohail (19 Apr 2019) -- Start
                                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't cancel this leave for this employee.Reason: Process Payroll is already done for last date of period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't cancel this leave for this employee.Reason: Process Payroll is already done for last date of period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 13, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                    Dim objFrm As New frmProcessPayroll
                                    objFrm.ShowDialog()
                                End If
                                'Sohail (19 Apr 2019) -- End
                                Exit Sub
                            End If


                            'Pinkal (03-Jan-2014) -- End

                        End If
                    End If
                End If
            End If


            'Pinkal (15-Sep-2013) -- End

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes
            If lnkCancelExpense.Visible Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "There are some leave expenses attached with this leave form. Do you want to cancel leave expenses. If you want to cancel those expenses then please click on Leave Expense link from this screen."), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    lnkCancelExpense.Focus()
                    Exit Sub
                End If
            End If
            'Pinkal (06-Mar-2014) -- End


            'Pinkal (13-Jan-2015) -- Start
            'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                Dim mdtTnAStartdate As Date = Nothing
                Dim objLvForm As New clsleaveform
                objLvForm._Formunkid = CInt(cboFormNo.SelectedValue)
                Dim objPaymentTran As New clsPayment_tran
                Dim objPeriod As New clsMasterData
                Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, objLvForm._Startdate.Date)

                If intPeriodId > 0 Then
                    Dim objTnAPeriod As New clscommom_period_Tran
                    mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                End If

                If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, objLvForm._Employeeunkid, mdtTnAStartdate, intPeriodId, objLvForm._Startdate.Date) > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to cancel this leave ?  "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                End If
                objLvForm = Nothing
            End If
            'Pinkal (13-Jan-2015) -- End


            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            Dim objLvType As New clsleavetype_master
            objLvType._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
            If objLvType._CancelLvFormDocMandatory Then
                Dim mstrInfoMsg As String = ""
                If mdtAttachement IsNot Nothing Then
                    Dim drRow() As DataRow = mdtAttachement.Select("AUD <> 'D' AND AUD <> ''")
                    If drRow.Length <= 0 Then
                        mstrInfoMsg = Language.getMessage(mstrModuleName, 10, "It is mandatory to attach document for Cancel Leave Form.Please attach document in order to complete this transaction.")
                    End If
                ElseIf mdtAttachement Is Nothing Then
                    mstrInfoMsg = Language.getMessage(mstrModuleName, 14, "Cancel Leave Form has mandatory document attachment. please attach document.")
                End If
                If mstrInfoMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(mstrInfoMsg, CType(enMsgBoxStyle.Information, enMsgBoxStyle))
                    Exit Sub
                End If
            End If


            'Pinkal (16-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.

            If objLvType._IsBlockLeave AndAlso objLvType._LVDaysMinLimit > 0 Then
                Dim mdecCancelDays As Decimal = 0
                Dim mdecTotalIssueDays As Decimal = 0
                Dim objDayFraction As New clsleaveday_fraction
                For i As Integer = 0 To lvLeavedate.CheckedItems.Count - 1
                    mdecCancelDays += objDayFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(CDate(lvLeavedate.CheckedItems(i).Text).Date), CInt(cboFormNo.SelectedValue) _
                                                                        , CInt(cboEmployee.SelectedValue), ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString(), -1, Nothing)
                Next
                objDayFraction = Nothing

                Dim objIssue As New clsleaveissue_master
                Dim dsIssueDays As DataSet = objIssue.GetEmployeeIssueData(CInt(cboEmployee.SelectedValue), objLvType._Leavetypeunkid)
                objIssue = Nothing

                If dsIssueDays IsNot Nothing AndAlso dsIssueDays.Tables(0).Rows.Count > 0 Then
                    mdecTotalIssueDays = dsIssueDays.Tables(0).AsEnumerable.Sum(Function(x) x.Field(Of Decimal)("dayfraction"))
                End If

                If (mdecTotalIssueDays - mdecCancelDays) > 0 AndAlso (mdecTotalIssueDays - mdecCancelDays) < objLvType._LVDaysMinLimit Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot cancel leave days as it is going below as compared to") & " " & objLvType._LVDaysMinLimit & " " & Language.getMessage(mstrModuleName, 16, "days configured on Leave Application Leave days limit setting for this leave type."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

            End If

            'Pinkal (16-May-2019) -- End

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if all issued days are cancelled then system will void employee exemption transaction for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            If objLvType._IsPaid = False AndAlso lvLeavedate.Items.Count = lvLeavedate.CheckedItems.Count Then
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                    Dim objADate As New clsDates_Approval_Tran
                    Dim intID As Integer = objADate.GetUnkIdByLeaveIssueUnkId(Nothing, 0, "")
                    Dim Flag As Boolean = objADate.isExist(Nothing, Nothing, Nothing, enEmp_Dates_Transaction.DT_EXEMPTION, CInt(cboEmployee.SelectedValue), clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, intID)

                    If Flag = False Then
                        Flag = objADate.isExist(Nothing, Nothing, Nothing, enEmp_Dates_Transaction.DT_EXEMPTION, CInt(cboEmployee.SelectedValue), clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True, intID)

                        If Flag = False Then
                            Flag = objADate.isExist(Nothing, Nothing, Nothing, enEmp_Dates_Transaction.DT_EXEMPTION, CInt(cboEmployee.SelectedValue), clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True, intID)
                        End If
                    End If
                    If Flag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If
            'Sohail (21 Oct 2019) -- End


            objLvType = Nothing
            'Pinkal (28-Nov-2017) -- End


            If SetValue() Then


                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                Dim mintRelieverEmployeeId As Integer = 0
                'Pinkal (25-May-2019) -- End

                'Pinkal (22-Jun-2015) -- Start
                'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.

                'Pinkal (15-Jul-2013) -- Start
                'Enhancement : TRA Changes
                'Dim mintApproverID As Integer = -1
                'Dim objPending As New clspendingleave_Tran
                'Dim dtApprover As DataTable = objPending.GetEmployeeHighestApprover(CInt(cboFormNo.SelectedValue), CInt(cboEmployee.SelectedValue))
                'If dtApprover.Rows.Count > 0 Then
                '    mintApproverID = CInt(dtApprover.Rows(0)("ApproverId"))
                'End If
                'Pinkal (15-Jul-2013) -- End

                Dim mintApproverID As Integer = -1
                Dim objPending As New clspendingleave_Tran
                Dim dsApprover As DataSet = objPending.GetEmployeeApproverListWithPriority(CInt(cboEmployee.SelectedValue), CInt(cboFormNo.SelectedValue))
                If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then
                    Dim drRow() As DataRow = dsApprover.Tables(0).Select("priority = Max(priority)")
                    If drRow.Length > 0 Then
                        Dim objLeaveDayFraction As New clsleaveday_fraction
                        For i As Integer = 0 To drRow.Length - 1
                            Dim dsDayFraction As DataSet = objLeaveDayFraction.GetList("List", CInt(cboFormNo.SelectedValue), True, CInt(cboEmployee.SelectedValue), CInt(drRow(i)("leaveapproverunkid")), "")
                            If dsDayFraction IsNot Nothing AndAlso dsDayFraction.Tables(0).Rows.Count > 0 Then
                                mintApproverID = CInt(dsDayFraction.Tables(0).Rows(0)("approverunkid"))
                                Exit For
                            End If
                        Next
                    End If


                    'Pinkal (25-May-2019) -- Start
                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                    If ConfigParameter._Object._SetRelieverAsMandatoryForApproval Then
                        Dim mintMaxPriority As Integer = dsApprover.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("statusunkid") = 7).Select(Function(x) x.Field(Of Integer)("priority")).Max()
                        Dim drReliever = dsApprover.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("statusunkid") = 7 And x.Field(Of Integer)("priority") = mintMaxPriority)
                        If drReliever.Count > 0 Then
                            mintRelieverEmployeeId = CInt(drReliever(0)("relieverempunkid"))
                End If
                    End If
                    'Pinkal (25-May-2019) -- End

                End If


                'Pinkal (28-Nov-2017) -- Start
                'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                Me.Cursor = Cursors.WaitCursor

                Dim mstrFolderName As String = ""
                Dim strError As String = ""
                Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                Dim strFileName As String
                Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")

                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.LEAVEFORMS) Select (p.Item("Name").ToString)).FirstOrDefault
                If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                    strPath += "/"
                End If

                For Each dRow As DataRow In mdtAttachement.Rows

                    If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                        strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & System.IO.Path.GetExtension(CStr(dRow("orgfilepath")))

                        If blnIsIISInstalled Then
                            If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            Else
                                dRow("fileuniquename") = strFileName
                                dRow("filepath") = strPath & "uploadimage/" & mstrFolderName & "/" + strFileName
                                dRow.AcceptChanges()
                            End If
                        Else
                            If System.IO.Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If System.IO.Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                    System.IO.Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                                End If
                                System.IO.File.Copy(CStr(dRow("orgfilepath")), strDocLocalPath, True)
                                dRow("fileuniquename") = strFileName
                                dRow("filepath") = strDocLocalPath
                                dRow.AcceptChanges()
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        strFileName = dRow("fileuniquename").ToString

                        If blnIsIISInstalled Then
                            'Gajanan [8-April-2019] -- Start
                            'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                                'Gajanan [8-April-2019] -- End
                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        Else
                            If System.IO.Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If System.IO.File.Exists(strDocLocalPath) Then
                                    System.IO.File.Delete(strDocLocalPath)
                                End If
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    End If
                Next

                'Pinkal (28-Nov-2017) -- End




                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                'blnFlag = objCancelForm.Insert(mstrLeavedates, CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), mintTotalIssuedDays, mintTotalCanceldays, ConfigParameter._Object._CurrentDateAndTime, FinancialYear._Object._YearUnkid, "", ConfigParameter._Object._LeaveBalanceSetting, mintApproverID)
                blnFlag = objCancelForm.Insert(Company._Object._Companyunkid, mstrLeavedates, CInt(cboEmployee.SelectedValue) _
                                                            , CInt(cboLeaveType.SelectedValue), mintTotalIssuedDays, mintTotalCanceldays, ConfigParameter._Object._CurrentDateAndTime _
                                                            , FinancialYear._Object._YearUnkid, "", ConfigParameter._Object._LeaveBalanceSetting, mintApproverID _
                                                            , ConfigParameter._Object._SkipEmployeeMovementApprovalFlow, ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), enLogin_Mode.DESKTOP, User._Object._Username)
                'Sohail (21 Oct 2019) -- [blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst, xDatabaseName, xUserAccessModeSetting, dtEmployeeAsOnDate, eMode, xUsername]
                'Pinkal (03-May-2019) -- End


                If blnFlag = False And objCancelForm._Message <> "" Then
                    eZeeMsgBox.Show(objCancelForm._Message, enMsgBoxStyle.Information)
                Else

                    Dim intStatusID As Integer = 0
                    Dim arStr As String() = mstrLeavedates.Split(CChar(","))


                    'Pinkal (03-May-2019) -- Start
                    'Enhancement - Working on Leave UAT Changes for NMB.
                    'If arStr.Length = lvLeavedate.Items.Count Then
                    '    intStatusID = 6
                    'Else
                    '    intStatusID = 7
                    'End If
                    If arStr.Length > 0 Then
                        intStatusID = 6
                    End If
                    'Pinkal (03-May-2019) -- End


                    Dim objLeaveForm As New clsleaveform
                    objLeaveForm._Formunkid = objCancelForm._Formunkid

                    Dim objEmp As New clsEmployee_Master
                    objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLeaveForm._Employeeunkid
                    objLeaveForm._EmployeeCode = objEmp._Employeecode
                    objLeaveForm._EmployeeFirstName = objEmp._Firstname
                    objLeaveForm._EmployeeMiddleName = objEmp._Othername
                    objLeaveForm._EmployeeSurName = objEmp._Surname
                    objLeaveForm._EmpMail = objEmp._Email


                    'Pinkal (01-Apr-2019) -- Start
                    'Enhancement - Working on Leave Changes for NMB.

                    'objLeaveForm.SendMailToEmployee(CInt(cboEmployee.SelectedValue), _
                    '                                cboLeaveType.Text, _
                    '                                objLeaveForm._Startdate.Date, _
                    '                                objLeaveForm._Returndate.Date, _
                    '                                intStatusID, Company._Object._Companyunkid, mstrLeavedates, _
                    '                                "", _
                    '                                "", enLogin_Mode.DESKTOP, 0, User._Object._Userunkid)

                    objLeaveForm.SendMailToEmployee(CInt(cboEmployee.SelectedValue), _
                                                    cboLeaveType.Text, _
                                                    objLeaveForm._Startdate.Date, _
                                                    objLeaveForm._Returndate.Date, _
                                                    intStatusID, Company._Object._Companyunkid, mstrLeavedates, _
                                                    "", _
                                                  "", enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, "", _
                                                  0, objLeaveForm._Formno)

                    'Pinkal (01-Apr-2019) -- End



                    'Pinkal (25-May-2019) -- Start
                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                    If ConfigParameter._Object._SetRelieverAsMandatoryForApproval AndAlso mintRelieverEmployeeId > 0 Then
                        objLeaveForm.SendMailToReliever(Company._Object._Companyunkid, mintRelieverEmployeeId, objLeaveForm._Leavetypeunkid, objLeaveForm._Formunkid _
                                                                        , objLeaveForm._Formno, cboEmployee.Text, objLeaveForm._Startdate.Date, objLeaveForm._Returndate.Date _
                                                                        , User._Object._Userunkid, 6, mstrLeavedates, enLogin_Mode.DESKTOP)
                    End If
                    'Pinkal (25-May-2019) -- End



                    mblnCancel = False
                    Me.Close()
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (28-Nov-2017) -- Start
    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

    Private Sub btnAttachDocument_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAttachDocument.Click
        Dim frm As New frmScanOrAttachmentInfo
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If


            If mdtAttachement IsNot Nothing Then frm._dtAttachment = mdtAttachement.Copy()
            frm._TransactionID = -1
            frm._TransactionGuidString = Guid.NewGuid.ToString()
            frm._TransactionMstUnkId = CInt(cboFormNo.SelectedValue)

            'S.SANDEEP |26-APR-2019| -- START
            'If frm.displayDialog(Language.getMessage(mstrModuleName, 11, "Select Employee"), enImg_Email_RefId.Leave_Module _
            '                                                                        , menAction, "-1", mstrModuleName _
            '                                                                        , CInt(cboEmployee.SelectedValue), enScanAttactRefId.LEAVEFORMS, True) Then
            If frm.displayDialog(Language.getMessage(mstrModuleName, 11, "Select Employee"), enImg_Email_RefId.Leave_Module _
                                                                    , menAction, "-1", mstrModuleName, True _
                                                                        , CInt(cboEmployee.SelectedValue), enScanAttactRefId.LEAVEFORMS, True) Then
                'S.SANDEEP |26-APR-2019| -- END

                mdtAttachement = frm._dtAttachment
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAttachDocument_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (28-Nov-2017) -- End


#End Region

#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try

            For i As Integer = 0 To lvLeavedate.Items.Count - 1
                RemoveHandler lvLeavedate.ItemChecked, AddressOf lvLeavedate_ItemChecked
                lvLeavedate.Items(i).Checked = chkSelectAll.Checked
                AddHandler lvLeavedate.ItemChecked, AddressOf lvLeavedate_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvLeavedate_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvLeavedate.ItemChecked
        Try
            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            If lvLeavedate.CheckedItems.Count <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked

            ElseIf lvLeavedate.CheckedItems.Count < lvLeavedate.Items.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate

            ElseIf lvLeavedate.CheckedItems.Count = lvLeavedate.Items.Count Then
                chkSelectAll.CheckState = CheckState.Checked

            End If
            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "LinkButton Events"

    Private Sub lnkCancelExpense_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCancelExpense.LinkClicked
        Try

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You are about to cancel leave expense form, are you sure you want to cancel leave expense form ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim dtApprover As DataTable = Nothing
                Dim objApproverTran As New clsclaim_request_approval_tran

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'dtApprover = objApproverTran.GetMaxApproverForClaimForm(objClaimReqestMaster._Employeeunkid, objClaimReqestMaster._Expensetypeid _
                '                                                                                        , objClaimReqestMaster._Crmasterunkid, ConfigParameter._Object._PaymentApprovalwithLeaveApproval _
                '                                                                                        , objClaimReqestMaster._Statusunkid)

                dtApprover = objApproverTran.GetMaxApproverForClaimForm(objClaimReqestMaster._Employeeunkid, objClaimReqestMaster._Expensetypeid _
                                                                                                        , objClaimReqestMaster._Crmasterunkid, ConfigParameter._Object._PaymentApprovalwithLeaveApproval _
                                                                                                        , objClaimReqestMaster._Statusunkid, CInt(cboFormNo.SelectedValue))
                'Pinkal (04-Feb-2019) -- End

                

                If dtApprover IsNot Nothing AndAlso dtApprover.Rows.Count > 0 Then
                    Dim objCancelExpense As New frmCancelExpenseForm
                    objCancelExpense.displayDialog(objClaimReqestMaster._Crmasterunkid, CInt(dtApprover.Rows(0)("crapproverunkid")) _
                                                                     , CInt(dtApprover.Rows(0)("approveremployeeunkid")), CInt(dtApprover.Rows(0)("crpriority")), True)

                    mdtCancelExpense = objCancelExpense.mdtTran
                    mstrCancelExpenseRemark = objCancelExpense.txtCancelRemark.Text.ToString
                    objApproverTran = Nothing
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "There is no Expense Data to Cancel the Expense Form."), enMsgBoxStyle.Information) '?1
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCancelExpense_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbLeaveForm.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeaveForm.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAttachDocument.GradientBackColor = GUI._ButttonBackColor
            Me.btnAttachDocument.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.gbLeaveForm.Text = Language._Object.getCaption(Me.gbLeaveForm.Name, Me.gbLeaveForm.Text)
            Me.lblCancelFormNo.Text = Language._Object.getCaption(Me.lblCancelFormNo.Name, Me.lblCancelFormNo.Text)
            Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
            Me.lblFormNo.Text = Language._Object.getCaption(Me.lblFormNo.Name, Me.lblFormNo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblLeaveFormDate.Text = Language._Object.getCaption(Me.lblLeaveFormDate.Name, Me.lblLeaveFormDate.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.EZeeGradientButton1.Text = Language._Object.getCaption(Me.EZeeGradientButton1.Name, Me.EZeeGradientButton1.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.colhLeavedate.Text = Language._Object.getCaption(CStr(Me.colhLeavedate.Tag), Me.colhLeavedate.Text)
            Me.lnkCancelExpense.Text = Language._Object.getCaption(Me.lnkCancelExpense.Name, Me.lnkCancelExpense.Text)
            Me.btnAttachDocument.Text = Language._Object.getCaption(Me.btnAttachDocument.Name, Me.btnAttachDocument.Text)
			Me.LblBalanceAsonDate.Text = Language._Object.getCaption(Me.LblBalanceAsonDate.Name, Me.LblBalanceAsonDate.Text)
			Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.Name, Me.lblBalance.Text)
			Me.LblTotalAdjustment.Text = Language._Object.getCaption(Me.LblTotalAdjustment.Name, Me.LblTotalAdjustment.Text)
			Me.lblTotalIssuedToDate.Text = Language._Object.getCaption(Me.lblTotalIssuedToDate.Name, Me.lblTotalIssuedToDate.Text)
			Me.lblToDateAccrued.Text = Language._Object.getCaption(Me.lblToDateAccrued.Name, Me.lblToDateAccrued.Text)
			Me.LblLeaveBF.Text = Language._Object.getCaption(Me.LblLeaveBF.Name, Me.LblLeaveBF.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Cancel Form No cannot be blank. Cancel Form No is required information.")
            Language.setMessage(mstrModuleName, 2, "Leave Date is compulsory information.Please Select atleast One Leave Date.")
            Language.setMessage(mstrModuleName, 3, "Reason cannot be blank. Reason is required information.")
            Language.setMessage(mstrModuleName, 4, "You can't cancel this leave for this employee.Reason: Period is already closed.")
            Language.setMessage(mstrModuleName, 5, "You can't cancel this leave for this employee.Reason: Process Payroll is already done for last date of period.")
            Language.setMessage(mstrModuleName, 6, "There is no Expense Data to Cancel the Expense Form.")
            Language.setMessage(mstrModuleName, 7, "You are about to cancel leave expense form, are you sure you want to cancel leave expense form ?")
            Language.setMessage(mstrModuleName, 8, "There are some leave expenses attached with this leave form. Do you want to cancel leave expenses. If you want to cancel those expenses then please click on Leave Expense link from this screen.")
            Language.setMessage(mstrModuleName, 9, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to cancel this leave ?")
            Language.setMessage(mstrModuleName, 10, "It is mandatory to attach document for Cancel Leave Form.Please attach document in order to complete this transaction.")
            Language.setMessage(mstrModuleName, 11, "Select Employee")
            Language.setMessage(mstrModuleName, 12, "Configuration Path does not Exist.")
			Language.setMessage(mstrModuleName, 13, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 14, "Cancel Leave Form has mandatory document attachment. please attach document.")
			Language.setMessage(mstrModuleName, 15, "Sorry, you cannot cancel leave days as it is going below as compared to")
			Language.setMessage(mstrModuleName, 16, "days configured on Leave Application Leave days limit setting for this leave type.")
			Language.setMessage(mstrModuleName, 17, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmdenomination_AddEdit

    'Hemant (19 Mar 2019) -- Start
    'ENHANCEMENT : Add Missing Column fields of AT Tables & Show its on "Audit & Trails" Module.
    'Private ReadOnly mstrModuleName As String = "frmDenomation_AddEdit"
    Private ReadOnly mstrModuleName As String = "frmDenomination_AddEdit"
    'Hemant (19 Mar 2019) -- End

    Private mblnCancel As Boolean = True

    Private objDenomination As clsDenomination

    Private menAction As enAction = enAction.ADD_ONE
    Private mintDenominationUnkid As Integer = -1
    Private mintCountryUnkid As Integer = -1

    'S.SANDEEP [ 23 JUNE 2011 ] -- START
    'ISSUE : EXCHANGE RATE CHANGES
    Private mintCurrencyId As Integer = 0
    Dim objExRate As New clsExchangeRate
    'S.SANDEEP [ 23 JUNE 2011 ] -- END 


#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintDenominationUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintDenominationUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            cboCurrency.BackColor = GUI.ColorComp
            txtCode.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objDenomination._Denom_Code
        'Sandeep [ 29 NOV 2010 ] -- Start
        'txtName.Text = objDenomination._Denomination
        txtName.Decimal = CDec(Format(objDenomination._Denomination, GUI.fmtCurrency)) 'Sohail (11 May 2011)
        'Sandeep [ 29 NOV 2010 ] -- End 


        'S.SANDEEP [ 23 JUNE 2011 ] -- START
        'ISSUE : EXCHANGE RATE CHANGES
        'cboCurrency.Text = objDenomination._Currency_Sign

        'Sohail (03 Sep 2012) -- Start
        'TRA - ENHANCEMENT
        'cboCurrency.SelectedValue = IIf(objDenomination._Currency_Sign = Nothing, 1, objDenomination._Currency_Sign)
        mintCurrencyId = CInt(objDenomination._Currency_Sign)
        cboCurrency.SelectedValue = objDenomination._Countryunkid
        'Sohail (03 Sep 2012) -- End

        
        'S.SANDEEP [ 23 JUNE 2011 ] -- END 

    End Sub

    Private Sub SetValue()
        objDenomination._Denom_Code = txtCode.Text
        'Sandeep [ 29 NOV 2010 ] -- Start
        'objDenomination._Denomination = txtName.Text
        objDenomination._Denomination = txtName.Decimal 'Sohail (11 May 2011)
        'Sandeep [ 29 NOV 2010 ] -- End 


        'S.SANDEEP [ 23 JUNE 2011 ] -- START
        'ISSUE : EXCHANGE RATE CHANGES
        'objDenomination._Currency_Sign = cboCurrency.Text

        'Sohail (03 Sep 2012) -- Start
        'TRA - ENHANCEMENT
        'objDenomination._Currency_Sign = CStr(cboCurrency.SelectedValue)
        objDenomination._Currency_Sign = mintCurrencyId.ToString
        objDenomination._Countryunkid = CInt(cboCurrency.SelectedValue)
        'Sohail (03 Sep 2012) -- End

        'S.SANDEEP [ 23 JUNE 2011 ] -- END 

    End Sub

    'S.SANDEEP [ 23 JUNE 2011 ] -- START
    'ISSUE : EXCHANGE RATE CHANGES

    'Private Sub FillCombos()
    '    Dim objDataMaster As New clsDataOperation
    '    Dim StrQ As String
    '    Dim dsCurrency As DataSet = Nothing
    '    Try

    '        StrQ = "SELECT distinct currency FROM hrmsConfiguration..cfcountry_master WHERE currency <> '' "

    '        dsCurrency = objDataMaster.ExecQuery(StrQ, "List")
    '        cboCurrency.DisplayMember = "currency"
    '        cboCurrency.DataSource = dsCurrency.Tables(0)
    '        cboCurrency.SelectedIndex = 0
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillCombos", mstrModuleName)
    '    Finally
    '        objDataMaster.Dispose()
    '        dsCurrency.Dispose()
    '    End Try
    'End Sub

    Private Sub FillCombos()
        Dim dsList As New DataSet
        Try
            dsList = objExRate.getComboList()
            With cboCurrency
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                '.ValueMember = "exchangerateunkid"
                .ValueMember = "countryunkid"
                'Sohail (03 Sep 2012) -- End
                .DisplayMember = "currency_name"
                .DataSource = dsList.Tables(0)
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                '.SelectedValue = 1
                .SelectedIndex = 0
                'Sohail (03 Sep 2012) -- End
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombos", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 23 JUNE 2011 ] -- END 

#End Region

#Region " Form's Events "
    Private Sub frmDenomination_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objDenomination = Nothing
    End Sub

    Private Sub frmDenomination_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmDenomination_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmDenomination_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDenomination = New clsDenomination
        'Call Language.setLanguage(Me.Name)
        'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

        Try
            Call Set_Logo(Me, gApplicationType)


            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Call OtherSettings()
            Call setColor()

            If menAction = enAction.EDIT_ONE Then
                objDenomination._Denomunkid = mintDenominationUnkid
            End If
            Call FillCombos()
            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDenomination_AddEdit_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDenomination.SetMessages()
            objfrm._Other_ModuleNames = "clsDenomination"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region


#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Denomination Code cannot be blank. Denomination Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If


            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Denomination Name cannot be blank. Denomination Name is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            If cboCurrency.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select currency sign from drop down."), enMsgBoxStyle.Information) '?3
                cboCurrency.Select()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objDenomination.Update()
            Else
                blnFlag = objDenomination.Insert()
            End If

            If blnFlag = False And objDenomination._Message <> "" Then
                eZeeMsgBox.Show(objDenomination._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objDenomination = Nothing
                    objDenomination = New clsDenomination
                    Call GetValue()
                    cboCurrency.SelectedValue = mintCurrencyId
                    txtCode.Focus()
                Else
                    mintDenominationUnkid = objDenomination._Denomunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    Private Sub cboCurrency_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectionChangeCommitted
        Try
            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'mintCurrencyId = CInt(cboCurrency.SelectedValue)
            Dim dsList As New DataSet
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), , , True)
            If dsList.Tables(0).Rows.Count > 0 Then
                mintCurrencyId = CInt(dsList.Tables(0).Rows(0)("exchangerateunkid"))
            Else
                mintCurrencyId = 0
            End If
            'Sohail (03 Sep 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectionChangeCommitted", mstrModuleName)
        Finally
        End Try
    End Sub
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbCurrencyDenomination.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCurrencyDenomination.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbCurrencyDenomination.Text = Language._Object.getCaption(Me.gbCurrencyDenomination.Name, Me.gbCurrencyDenomination.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Denomination Code cannot be blank. Denomination Code is required information.")
			Language.setMessage(mstrModuleName, 2, "Denomination Name cannot be blank. Denomination Name is required information.")
			Language.setMessage(mstrModuleName, 3, "Please select currency sign from drop down.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
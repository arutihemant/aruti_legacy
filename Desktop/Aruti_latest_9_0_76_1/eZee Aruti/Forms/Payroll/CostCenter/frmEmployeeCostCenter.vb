﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 7

Public Class frmEmployeeCostCenter

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeCostCenter"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objEmployeeCostcenter As clsemployee_costcenter_Tran
    Private mintEmpcostcenterUnkid As Integer = -1
    Dim blnIslistclick As Boolean = False
    'Sohail (11 Sep 2010) -- Start
    Private mintEmployeeID As Integer = 0
    Private mintTranHeadID As Integer = 0
    Private mintCostCenterID As Integer = 0
    'Sohail (11 Sep 2010) -- End
    Private mstrSalaryHeadIDs As String 'Sohail (13 Sep 2011)
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmpID As Integer = 0, Optional ByVal intTranHeadID As Integer = 0, Optional ByVal intCostCenterID As Integer = 0) As Boolean
        Try
            mintEmpcostcenterUnkid = intUnkId
            menAction = eAction

            'Sohail (11 Sep 2010) -- Start
            mintEmployeeID = intEmpID
            mintTranHeadID = intTranHeadID
            mintCostCenterID = intCostCenterID
            'Sohail (11 Sep 2010) -- End

            Me.ShowDialog()

            intUnkId = mintEmpcostcenterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            cboED.BackColor = GUI.ColorComp
            cboCostcenter.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If mintEmpcostcenterUnkid > 0 Then
                cboEmployee.SelectedIndex = 0
                cboED.SelectedIndex = 0
                cboCostcenter.SelectedIndex = 0
                mintEmpcostcenterUnkid = -1
                cboEmployee.Select()
            End If
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEmployeeCostcenter._Costcentertranunkid = mintEmpcostcenterUnkid
            objEmployeeCostcenter._Costcentertranunkid(FinancialYear._Object._DatabaseName) = mintEmpcostcenterUnkid
            'Sohail (21 Aug 2015) -- End
            objEmployeeCostcenter._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmployeeCostcenter._Tranheadunkid = CInt(cboED.SelectedValue)
            objEmployeeCostcenter._Costcenterunkid = CInt(cboCostcenter.SelectedValue)
            objEmployeeCostcenter._Userunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()

        Dim dsFill As DataSet = Nothing
        Try

            'FOR EMPLOYEE
            Dim objEmployee As New clsEmployee_Master

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim EmpDate As Date = Nothing

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsFill = objEmployee.GetEmployeeList("Employee", True, True)
            'If menAction = enAction.EDIT_ONE Then
            '    'dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            '    EmpDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)  'Anjan [10 June 2015] -- Start
            'Else
            '    ' dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            '    EmpDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)  'Anjan [10 June 2015] -- Start
            'End If
            'Sohail (06 Jan 2012) -- End


            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "Employee", True)
            'Anjan [10 June 2015] -- End

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsFill.Tables("Employee")

            'Sohail (11 Sep 2010) -- Start
            ''FOR TRANSACTION HEAD
            'dsFill = Nothing
            'Dim objTranHead As New clsTransactionHead
            'dsFill = objTranHead.getComboList("TranHead", True)
            'cboED.ValueMember = "tranheadunkid"
            'cboED.DisplayMember = "name"
            'cboED.DataSource = dsFill.Tables("TranHead")
            'Sohail (11 Sep 2010) -- End

            'FOR COST CENTER
            dsFill = Nothing
            Dim objCostcenter As New clscostcenter_master
            dsFill = objCostcenter.getComboList("CostCenter", True)
            cboCostcenter.ValueMember = "costcenterunkid"
            cboCostcenter.DisplayMember = "costcentername"
            cboCostcenter.DataSource = dsFill.Tables("CostCenter")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            If CInt(cboEmployee.SelectedValue) > 0 Then

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objEmployeeCostcenter.GetList("EmpCostCenter", True)
                dsList = objEmployeeCostcenter.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmpCostCenter", True, CInt(cboEmployee.SelectedValue), , StrSearching)
                'Sohail (21 Aug 2015) -- End

                StrSearching = "AND employeeunkid=" & CInt(cboEmployee.SelectedValue) & " "
                StrSearching &= " AND tranheadunkid NOT IN (" & mstrSalaryHeadIDs & ") " 'Sohail (13 Sep 2011)

                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables("EmpCostCenter"), StrSearching, "", DataViewRowState.CurrentRows).ToTable

                lvEmpCostCenter.Items.Clear()
                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    'Sohail (28 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'lvItem.Text = dtRow("employeename").ToString
                    lvItem.Text = dtRow("employeecode").ToString
                    lvItem.SubItems.Add(dtRow("employeename").ToString())
                    'Sohail (28 Jan 2012) -- End
                    lvItem.Tag = CInt(dtRow("costcentertranunkid").ToString)
                    lvItem.SubItems.Add(dtRow("trnheadname").ToString())
                    lvItem.SubItems.Add(dtRow("costcentername").ToString())
                    lvItem.SubItems.Add(dtRow("employeeunkid").ToString())
                    lvItem.SubItems.Add(dtRow("tranheadunkid").ToString())
                    lvItem.SubItems.Add(dtRow("costcenterunkid").ToString())
                    lvEmpCostCenter.Items.Add(lvItem)
                Next

                If lvEmpCostCenter.Items.Count > 8 Then 'Sohail (19 Nov 2010)
                    colhCcName.Width = 210 - 18
                Else
                    colhCcName.Width = 210
                End If

                If lvEmpCostCenter.SelectedItems.Count > 0 Then
                    lvEmpCostCenter.SelectedItems(0).Selected = True
                End If
            Else
                lvEmpCostCenter.Items.Clear()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnSave.Enabled = User._Object.Privilege._AddEmployeeCostCenter
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeCostCenter


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeCostCenter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmployeeCostcenter = New clsemployee_costcenter_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End


            Call SetVisibility()
            SetColor()
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'mstrSalaryHeadIDs = (New clsTransactionHead).GetSalaryHeadsIDs(False) 'Sohail (13 Sep 2011)
            mstrSalaryHeadIDs = (New clsTransactionHead).GetSalaryHeadsIDs(FinancialYear._Object._DatabaseName, False)
            'Sohail (21 Aug 2015) -- End

            FillCombo()
            GetValue()
            'Sohail (11 Sep 2010) -- Start
            cboEmployee.SelectedValue = mintEmployeeID
            cboED.SelectedValue = mintTranHeadID
            cboCostcenter.SelectedValue = mintCostCenterID
            'Sohail (11 Sep 2010) -- End
            cboEmployee.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeCostCenter_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeCostCenter_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeCostCenter_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeCostCenter_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete And lvEmpCostCenter.Focused = True Then
                'Sohail (24 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (24 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeCostCenter_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeCostCenter_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objEmployeeCostcenter = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_costcenter_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_costcenter_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim strName As String = String.Empty
        Try

            If CInt(cboEmployee.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub
            ElseIf CInt(cboED.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Transaction Head is compulsory information.Please Select Transaction Head."), enMsgBoxStyle.Information)
                cboED.Select()
                Exit Sub
            ElseIf CInt(cboCostcenter.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "CostCenter is compulsory information.Please Select CostCenter."), enMsgBoxStyle.Information)
                cboCostcenter.Select()
                Exit Sub
            End If

            SetValue()

            If mintEmpcostcenterUnkid > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to update this Employee Cost Center?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnFlag = objEmployeeCostcenter.Update()
                    blnFlag = objEmployeeCostcenter.Update(FinancialYear._Object._DatabaseName, Nothing, ConfigParameter._Object._CurrentDateAndTime)
                    'Sohail (21 Aug 2015) -- End
                    If blnFlag Then mintEmpcostcenterUnkid = -1
                Else
                    Exit Sub
                End If
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objEmployeeCostcenter.Insert()
                blnFlag = objEmployeeCostcenter.Insert(FinancialYear._Object._DatabaseName, Nothing, ConfigParameter._Object._CurrentDateAndTime)
                'Sohail (21 Aug 2015) -- End
            End If


            If blnFlag = False And objEmployeeCostcenter._Message <> "" Then
                eZeeMsgBox.Show(objEmployeeCostcenter._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objEmployeeCostcenter = Nothing
                    objEmployeeCostcenter = New clsemployee_costcenter_Tran
                    FillList()
                    cboED.SelectedIndex = 0
                    cboCostcenter.SelectedIndex = 0
                    If lvEmpCostCenter.SelectedItems.Count > 0 Then lvEmpCostCenter.SelectedItems(0).Selected = False
                    cboEmployee.Select()
                Else
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'mintEmpcostcenterUnkid = objEmployeeCostcenter._Costcentertranunkid
                    mintEmpcostcenterUnkid = objEmployeeCostcenter._Costcentertranunkid(FinancialYear._Object._DatabaseName)
                    'Sohail (21 Aug 2015) -- End
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvEmpCostCenter.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Cost Center from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvEmpCostCenter.Select()
            Exit Sub
        End If
        'If objEmployeeCostcenter.isUsed(CInt(lvEmpCostCenter.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot delete this Employee CostCenter. Reason: This Employee CostCenter is in use."), enMsgBoxStyle.Information) '?2
        '    objEmployeeCostcenter.Select()
        '    Exit Sub
        'End If
        'Sohail (19 Nov 2010) -- Start
        If objEmployeeCostcenter.isUsed(CInt(lvEmpCostCenter.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot delete this Employee CostCenter. Reason: This Employee CostCenter is in use."), enMsgBoxStyle.Information) '?2
            lvEmpCostCenter.Select()
            Exit Sub
        End If
        'Sohail (19 Nov 2010) -- End
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvEmpCostCenter.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to delete this Employee Costcenter?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objEmployeeCostcenter._Voidreason = "VOID REASON"
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objEmployeeCostcenter._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                'Sandeep [ 16 Oct 2010 ] -- End 
                objEmployeeCostcenter._Voiduserunkid = User._Object._Userunkid 'Sohail (14 Oct 2010)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objEmployeeCostcenter.Delete(CInt(lvEmpCostCenter.SelectedItems(0).Tag), ConfigParameter._Object._CurrentDateAndTime)
                objEmployeeCostcenter.Delete(FinancialYear._Object._DatabaseName, CInt(lvEmpCostCenter.SelectedItems(0).Tag), ConfigParameter._Object._CurrentDateAndTime)
                'Sohail (21 Aug 2015) -- End
                lvEmpCostCenter.SelectedItems(0).Remove()
                blnIslistclick = False

                If lvEmpCostCenter.Items.Count <= 0 Then
                    GetValue()
                    Exit Try
                End If

                If lvEmpCostCenter.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvEmpCostCenter.Items.Count - 1
                    lvEmpCostCenter.Items(intSelectedIndex).Selected = True
                    lvEmpCostCenter.EnsureVisible(intSelectedIndex)
                ElseIf lvEmpCostCenter.Items.Count <> 0 Then
                    lvEmpCostCenter.Items(intSelectedIndex).Selected = True
                    lvEmpCostCenter.EnsureVisible(intSelectedIndex)
                End If
                lvEmpCostCenter_SelectedIndexChanged(sender, e)
            End If
            lvEmpCostCenter.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchEmployee.Click
        Dim objfrmCommonsearch As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrmCommonsearch.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmCommonsearch.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmCommonsearch)
            End If
            'Anjan (02 Sep 2011)-End 

            objfrmCommonsearch.DataSource = CType(cboEmployee.DataSource, DataTable)
            objfrmCommonsearch.ValueMember = cboEmployee.ValueMember
            objfrmCommonsearch.DisplayMember = cboEmployee.DisplayMember
            objfrmCommonsearch.CodeMember = "employeecode"
            If objfrmCommonsearch.DisplayDialog() Then
                cboEmployee.SelectedValue = objfrmCommonsearch.SelectedValue
            End If
            cboEmployee.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objSearchED_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchED.Click
        Dim objfrmCommonsearch As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrmCommonsearch.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmCommonsearch.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmCommonsearch)
            End If
            'Anjan (02 Sep 2011)-End 
            objfrmCommonsearch.DataSource = CType(cboED.DataSource, DataTable)
            objfrmCommonsearch.ValueMember = cboED.ValueMember
            objfrmCommonsearch.DisplayMember = cboED.DisplayMember
            objfrmCommonsearch.CodeMember = "code"
            If objfrmCommonsearch.DisplayDialog() Then
                cboED.SelectedValue = objfrmCommonsearch.SelectedValue
            End If
            cboED.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchED_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objSearchCostCenter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchCostCenter.Click
        Dim objfrmCommonsearch As New frmCommonSearch
        Try
            objfrmCommonsearch.DataSource = CType(cboCostcenter.DataSource, DataTable)
            objfrmCommonsearch.ValueMember = cboCostcenter.ValueMember
            objfrmCommonsearch.DisplayMember = cboCostcenter.DisplayMember
            objfrmCommonsearch.CodeMember = "costcentercode"
            If objfrmCommonsearch.DisplayDialog() Then
                cboCostcenter.SelectedValue = objfrmCommonsearch.SelectedValue
            End If
            cboCostcenter.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchCostCenter_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Sohail (13 Sep 2011) -- Start
    Private Sub btnSalaryCC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalaryCC.Click
        Dim objFrm As New frmEmpDistributedCostCenterList
        Try
            objFrm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSalaryCC_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (13 Sep 2011) -- End

#End Region

#Region "Listview's Event"

    Private Sub lvEmpCostCenter_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvEmpCostCenter.SelectedIndexChanged
        Try
            If lvEmpCostCenter.SelectedItems.Count > 0 AndAlso User._Object.Privilege._EditEmployeeCostCenter = True Then 'Anjan (12 Feb 2011) Issue : Included privilege for editing employee cost center.
                blnIslistclick = True
                cboED.SelectedValue = CInt(lvEmpCostCenter.SelectedItems(0).SubItems(colhTranheadunkid.Index).Text)
                cboCostcenter.SelectedValue = CInt(lvEmpCostCenter.SelectedItems(0).SubItems(colhCostcenterunkid.Index).Text)
                mintEmpcostcenterUnkid = CInt(lvEmpCostCenter.SelectedItems(0).Tag)
                cboEmployee.SelectedValue = CInt(lvEmpCostCenter.SelectedItems(0).SubItems(colhEmployeeunkid.Index).Text)
                blnIslistclick = False
                cboEmployee.Enabled = False
                'Sohail (31 Aug 2010) -- Start
                objSearchEmployee.Enabled = False
                'Sohail (31 Aug 2010) -- End
            Else
                cboEmployee.Enabled = True
                mintEmpcostcenterUnkid = 0
                'Sohail (31 Aug 2010) -- Start
                objSearchEmployee.Enabled = True
                'Sohail (31 Aug 2010) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmpCostCenter_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim objEmployee As New clsEmployee_Master
        Dim objCC As New clscostcenter_master
        Dim objED As New clsEarningDeduction
        Dim dsList As DataSet
        Dim dtTable As DataTable 'Sohail (13 Sep 2011)
        Try
            If CInt(cboEmployee.SelectedValue) > 0 And blnIslistclick = False Then
                FillList()
            Else
                lvEmpCostCenter.Items.Clear()
            End If

            'Sohail (11 Sep 2010) -- Start
            lblDefCC.Text = ""
            If CInt(cboEmployee.SelectedValue) > 0 Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END

                objCC._Costcenterunkid = objEmployee._Costcenterunkid
                lblDefCC.Text &= Language.getMessage(mstrModuleName, 8, "Default Cost Center :") & " " & objCC._Costcentername
            End If
            dsList = objED.getListForCombo_TransactionHead("TranHead", CInt(cboEmployee.SelectedValue))
            'Sohail (13 Sep 2011) -- Start
            dtTable = New DataView(dsList.Tables("TranHead"), "tranheadunkid NOT IN (" & mstrSalaryHeadIDs & ") ", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (13 Sep 2011) -- End
            With cboED
                .ValueMember = "tranheadunkid"
                .DisplayMember = "trnheadname"
                'Sohail (13 Sep 2011) -- Start
                '.DataSource = dsList.Tables("TranHead")
                .DataSource = dtTable
                'Sohail (13 Sep 2011) -- End
                .SelectedValue = 0
            End With
            'Sohail (11 Sep 2010) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
            objEmployee = Nothing
            objCC = Nothing
            objED = Nothing
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbCostCenterInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCostCenterInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnSalaryCC.GradientBackColor = GUI._ButttonBackColor
            Me.btnSalaryCC.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbCostCenterInfo.Text = Language._Object.getCaption(Me.gbCostCenterInfo.Name, Me.gbCostCenterInfo.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lblTrnHead.Text = Language._Object.getCaption(Me.lblTrnHead.Name, Me.lblTrnHead.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhEmpName.Text = Language._Object.getCaption(CStr(Me.colhEmpName.Tag), Me.colhEmpName.Text)
            Me.colhTrnHead.Text = Language._Object.getCaption(CStr(Me.colhTrnHead.Tag), Me.colhTrnHead.Text)
            Me.colhCcName.Text = Language._Object.getCaption(CStr(Me.colhCcName.Tag), Me.colhCcName.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.colhEmployeeunkid.Text = Language._Object.getCaption(CStr(Me.colhEmployeeunkid.Tag), Me.colhEmployeeunkid.Text)
            Me.colhTranheadunkid.Text = Language._Object.getCaption(CStr(Me.colhTranheadunkid.Tag), Me.colhTranheadunkid.Text)
            Me.colhCostcenterunkid.Text = Language._Object.getCaption(CStr(Me.colhCostcenterunkid.Tag), Me.colhCostcenterunkid.Text)
            Me.btnSalaryCC.Text = Language._Object.getCaption(Me.btnSalaryCC.Name, Me.btnSalaryCC.Text)
            Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)
            Me.lblDefCC.Text = Language._Object.getCaption(Me.lblDefCC.Name, Me.lblDefCC.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee.")
            Language.setMessage(mstrModuleName, 2, "Transaction Head is compulsory information.Please Select Transaction Head.")
            Language.setMessage(mstrModuleName, 3, "CostCenter is compulsory information.Please Select CostCenter.")
            Language.setMessage(mstrModuleName, 4, "Please select Cost Center from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 5, "Sorry, You cannot delete this Employee CostCenter. Reason: This Employee CostCenter is in use.")
            Language.setMessage(mstrModuleName, 6, "Are you sure you want to delete this Employee Costcenter?")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to update this Employee Cost Center?")
            Language.setMessage(mstrModuleName, 8, "Default Cost Center :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class




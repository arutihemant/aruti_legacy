﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBatchPostingList

#Region " Private Varaibles "
    Private objBatchPosting As clsBatchPosting
    Private ReadOnly mstrModuleName As String = "frmBatchPostingList"
    Private mstrAdvanceFilter As String = ""
    Private mintFirstOpenPeriod As Integer = 0
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mdtPeriod_Start As Date
    Private mdtPeriod_End As Date
    'Sohail (21 Aug 2015) -- End
#End Region

#Region " Private Functions "
    Private Sub SetColor()
        Try
            cboPeriod.BackColor = GUI.ColorOptional
            'cboEmployee.BackColor = GUI.ColorOptional
            'cboTranHead.BackColor = GUI.ColorOptional
            cboStatus.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        'Dim objEmployee As New clsEmployee_Master 'Sohail (24 Feb 2016) 
        Dim objPeriod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Dim dsCombos As DataSet
        'Dim dtTable As DataTable
        Try


            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)

            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , , , , False)
            'End If
            'Anjan [10 June 2015] -- End


            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombos.Tables("Employee")
            '    .SelectedValue = 0
            'End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open, FinancialYear._Object._YearUnkid)
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open, , True)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = mintFirstOpenPeriod
            End With

            'dsCombos = objTranHead.getComboList("TranHead", True, , enCalcType.FlatRate_Others)
            'dtTable = New DataView(dsCombos.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable
            'With cboTranHead
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dtTable
            '    .SelectedValue = 0
            'End With

            dsCombos = objMaster.getComboListEDBatchPostingStatus("Status", True, "" & enEDBatchPostingStatus.Posted_to_ED & ", " & enEDBatchPostingStatus.Not_Posted & "")
            With cboStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Status")
                .SelectedValue = 2
            End With

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            dsCombos = objBatchPosting.getComboList("Batch", True)
            With cboBatch
                .ValueMember = "empbatchpostingunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Batch")
                .SelectedIndex = -1
            End With
            'Sohail (24 Feb 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            'objEmployee = Nothing 'Sohail (24 Feb 2016) 
            objPeriod = Nothing
            objTranHead = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As New DataTable
        Try

            If User._Object.Privilege._AllowToViewBatchPostingList = False Then Exit Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objBatchPosting.GetBatchList("BatchPosting", txtBatchPostingNo.Text.Trim, CInt(cboPeriod.SelectedValue), CInt(cboStatus.SelectedValue), mstrAdvanceFilter, "premployee_batchposting_master.batchno")
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'dsList = objBatchPosting.GetBatchList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_Start, mdtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "BatchPosting", True, txtBatchPostingNo.Text.Trim, CInt(cboPeriod.SelectedValue), CInt(cboStatus.SelectedValue), mstrAdvanceFilter, "premployee_batchposting_master.batchno")
            dsList = objBatchPosting.GetBatchList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_Start, mdtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "BatchPosting", True, CInt(cboBatch.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboStatus.SelectedValue), mstrAdvanceFilter, "ISNULL(premployee_batchposting_master.batchcode, '')")
            'Sohail (24 Feb 2016) -- End
            'Sohail (21 Aug 2015) -- End

            Dim lvItem As ListViewItem

            lvEmpBatchPosting.Items.Clear()
            Dim lvArray As New List(Of ListViewItem)
            lvEmpBatchPosting.BeginUpdate()

            For Each dtRow As DataRow In dsList.Tables("BatchPosting").Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("empbatchpostingunkid").ToString
                lvItem.Tag = CInt(dtRow.Item("empbatchpostingunkid"))

                lvItem.SubItems.Add(dtRow.Item("BatchNo").ToString)

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                lvItem.SubItems.Add(dtRow.Item("BatchCode").ToString)

                lvItem.SubItems.Add(dtRow.Item("BatchName").ToString)
                'Sohail (24 Feb 2016) -- End

                lvItem.SubItems.Add(dtRow.Item("PeriodName").ToString)
                lvItem.SubItems(colhPeriod.Index).Tag = CInt(dtRow.Item("periodunkid"))

                'lvItem.SubItems.Add(dtRow.Item("EmpCode").ToString)
                'lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)
                'lvItem.SubItems(colhEmployeeName.Index).Tag = CInt(dtRow.Item("employeeunkid"))

                'lvItem.SubItems.Add(dtRow.Item("TranHeadName").ToString)
                'lvItem.SubItems(colhTranHeadName.Index).Tag = CInt(dtRow.Item("tranheadunkid"))

                lvItem.SubItems.Add(Format(dtRow.Item("TotalAmount"), GUI.fmtCurrency))

                If CBool(dtRow.Item("IsPosted")) = True Then
                    lvItem.SubItems.Add(Language.getMessage(mstrModuleName, 9, "YES"))
                Else
                    lvItem.SubItems.Add(Language.getMessage(mstrModuleName, 10, "NO"))
                End If
                lvItem.SubItems(colhPosted.Index).Tag = CBool(dtRow.Item("IsPosted"))

                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                lvItem.SubItems.Add(dtRow.Item("user_name").ToString)

                lvItem.SubItems.Add(dtRow.Item("description").ToString)
                'Sohail (24 Feb 2016) -- End

                lvArray.Add(lvItem)
            Next
            lvEmpBatchPosting.Items.AddRange(lvArray.ToArray)
            lvEmpBatchPosting.EndUpdate()

            'lvEmpBatchPosting.GroupingColumn = colhBatchNo
            'lvEmpBatchPosting.DisplayGroups(True)

            'lvEmpBatchPosting.GridLines = False

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'If lvEmpBatchPosting.Items.Count > 16 Then
            '    colhPeriod.Width = 200 - 18
            'Else
            '    colhPeriod.Width = 200
            'End If
            'Sohail (24 Feb 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
        Finally
            objbtnSearch.ShowResult(lvEmpBatchPosting.Items.Count.ToString)
            'Sohail (24 Feb 2016) -- End
        End Try
    End Sub

    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvEmpBatchPosting.Items
                If CBool(lvItem.SubItems(colhPosted.Index).Tag) = True Then Continue For
                RemoveHandler lvEmpBatchPosting.ItemCheck, AddressOf lvEmpBatchPosting_ItemCheck
                RemoveHandler lvEmpBatchPosting.ItemChecked, AddressOf lvEmpBatchPosting_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvEmpBatchPosting.ItemCheck, AddressOf lvEmpBatchPosting_ItemCheck
                AddHandler lvEmpBatchPosting.ItemChecked, AddressOf lvEmpBatchPosting_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEdHeads", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddBatchPosting
            btnEdit.Enabled = User._Object.Privilege._AllowToEditBatchPosting
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteBatchPosting

            mnuPostToED.Enabled = User._Object.Privilege._AllowToPostBatchPostingToED
            mnuVoidBatchPosting.Enabled = User._Object.Privilege._AllowToVoidBatchPostingToED 'Sohail (24 Feb 2016) 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try


    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmBatchPostingList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objBatchPosting = Nothing
    End Sub

    Private Sub frmBatchPostingList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                btnClose.PerformClick()
            ElseIf e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatchPostingList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBatchPostingList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBatchPosting = New clsBatchPosting
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Call SetVisibility()

            Call SetColor()
            Call FillCombo()
            Call FillList()


            If lvEmpBatchPosting.Items.Count > 0 Then lvEmpBatchPosting.Items(0).Selected = True
            lvEmpBatchPosting.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBatchPostingList_Load", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBatchPosting.SetMessages()
            'clsBatchPostingTran.SetMessages()
            objfrm._Other_ModuleNames = "clsBatchPosting,clsBatchPostingTran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (06 Apr 2013) -- End

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmBatchPosting_AddEdit
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
            If frm.displayDialog(-1, enAction.ADD_CONTINUE, False) Then
                'Sohail (24 Feb 2016) -- End
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvEmpBatchPosting.DoubleClick
        'Sohail (24 Feb 2016) - [lvEmpBatchPosting.DoubleClick]
        Try
            If lvEmpBatchPosting.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information)
                lvEmpBatchPosting.Select()
                Exit Sub
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                'ElseIf CBool(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPosted.Index).Tag) = True Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry! You can not edit the selected transaction. Reason: Selected transaction is already Posted to Earning Deduction."), enMsgBoxStyle.Information)
                '    lvEmpBatchPosting.Select()
                '    Exit Sub
                'Sohail (24 Feb 2016) -- End
            Else
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
                'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry! You can not edit the selected transaction. Reason: Period is closed."), enMsgBoxStyle.Information)
                    lvEmpBatchPosting.Select()
                    Exit Sub
                End If
            End If

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvEmpBatchPosting.SelectedItems(0).Index

            Dim frm As New frmBatchPosting_AddEdit
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'If frm.displayDialog(CInt(lvEmpBatchPosting.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
            If frm.displayDialog(CInt(lvEmpBatchPosting.SelectedItems(0).Tag), enAction.EDIT_ONE, CBool(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPosted.Index).Tag)) Then
                'Sohail (24 Feb 2016) -- End
                Call FillList()
            End If

            frm = Nothing
            lvEmpBatchPosting.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objPeriod As New clscommom_period_Tran
        Try
            If lvEmpBatchPosting.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information)
                lvEmpBatchPosting.Select()
                Exit Sub
            End If

            'For Each lvItem As ListViewItem In lvEmpBatchPosting.CheckedItems
            '    Dim objPeriod As New clscommom_period_Tran
            '    objPeriod._Periodunkid = CInt(lvItem.SubItems(colhPeriod.Index).Tag)
            '    If objPeriod._Statusid = enStatusType.Close Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry! You can not delete the selected transactions. Reason: Period is closed for some of the ticked transaction."), enMsgBoxStyle.Information)
            '        lvEmpBatchPosting.Select()
            '        Exit Sub
            '    End If
            'Next

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
            'Sohail (21 Aug 2015) -- End
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry! You can not delete the selected transactions. Reason: Period is closed for selected transaction."), enMsgBoxStyle.Information)
                lvEmpBatchPosting.Select()
                Exit Try
            End If
            If CBool(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPosted.Index).Tag) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry! You can not delete the selected transactions. Reason: selected batch is already posted to ED."), enMsgBoxStyle.Information)
                lvEmpBatchPosting.Select()
                Exit Try
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete selected transactions?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objBatchPosting._Isvoid = True
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objBatchPosting._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objBatchPosting._Voiduserunkid = User._Object._Userunkid
                objBatchPosting._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime

                'For Each lvItem As ListViewItem In lvEmpBatchPosting.CheckedItems
                '    If objBatchPosting.Void(CInt(lvItem.Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, True, True) = True Then
                '        lvItem.Remove()
                '    ElseIf objBatchPosting._Message <> "" Then
                '        eZeeMsgBox.Show(objBatchPosting._Message, enMsgBoxStyle.Information)
                '        Exit Try
                '    End If
                'Next
                If objBatchPosting.Void(CInt(lvEmpBatchPosting.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, True, True) = True Then
                    lvEmpBatchPosting.SelectedItems(0).Remove()
                ElseIf objBatchPosting._Message <> "" Then
                    eZeeMsgBox.Show(objBatchPosting._Message, enMsgBoxStyle.Information)
                    Exit Try
                End If

                If lvEmpBatchPosting.Items.Count <= 0 Then
                    Exit Try
                End If

            End If
            lvEmpBatchPosting.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
#End Region

#Region " Combobox's Events "

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                mdtPeriod_Start = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
                mdtPeriod_End = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
            Else
                mdtPeriod_Start = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriod_End = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    Private Sub cboPeriod_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPeriod.KeyPress, cboBatch.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboPeriod.ValueMember
                    .DisplayMember = cboPeriod.DisplayMember
                    .DataSource = CType(cboPeriod.DataSource, DataTable)
                    .CodeMember = "code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboPeriod.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboPeriod.Text = ""
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboBatch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboBatch.ValueMember
                    .DisplayMember = cboBatch.DisplayMember
                    .DataSource = CType(cboBatch.DataSource, DataTable)
                    .CodeMember = "code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboBatch.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboBatch.Text = ""
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBatch_KeyPress", mstrModuleName)
        End Try
    End Sub
    'Sohail (24 Feb 2016) -- End

#End Region

#Region " ListView's Events "

    Private Sub lvEmpBatchPosting_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lvEmpBatchPosting.ColumnClick
        Try
            Select Case e.Column
                Case colhBatchNo.Index
                    lvEmpBatchPosting.SortingType(eZee.Common.ListViewColumnSorter.ComparerType.NumericComparer)
                Case Else
                    lvEmpBatchPosting.SortingType(eZee.Common.ListViewColumnSorter.ComparerType.StringComparer)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmpBatchPosting_ColumnClick", mstrModuleName)
        End Try
    End Sub

    Private Sub lvEmpBatchPosting_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lvEmpBatchPosting.ItemCheck
        Try
            If CBool(lvEmpBatchPosting.Items(e.Index).SubItems(colhPosted.Index).Tag) = True AndAlso e.NewValue = CheckState.Checked Then
                e.NewValue = CheckState.Unchecked
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmpBatchPosting_ItemCheck", mstrModuleName)
        End Try
    End Sub

    Private Sub lvEmpBatchPosting_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmpBatchPosting.ItemChecked
        Try
            If lvEmpBatchPosting.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            ElseIf lvEmpBatchPosting.CheckedItems.Count < lvEmpBatchPosting.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            ElseIf lvEmpBatchPosting.CheckedItems.Count = lvEmpBatchPosting.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmpBatchPosting_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "

    Private Sub chkCopyPreviousEDSlab_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCopyPreviousEDSlab.CheckedChanged
        Try
            If chkCopyPreviousEDSlab.Checked = False Then chkOverwritePrevEDSlabHeads.Checked = False
            chkOverwritePrevEDSlabHeads.Enabled = chkCopyPreviousEDSlab.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCopyPreviousEDSlab_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAll(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    Private Sub mnuPostToED_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPostToED.Click
        Dim objPeriod As New clscommom_period_Tran
        Dim objED As New clsEarningDeduction
        Dim objTnA As New clsTnALeaveTran
        Dim objBatchPostingTran As New clsBatchPostingTran
        Dim dtTable As DataTable
        Try
            If lvEmpBatchPosting.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information)
                lvEmpBatchPosting.Select()
                Exit Try
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
            'Sohail (21 Aug 2015) -- End
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry! You can not post the selected batch. Reason: Period is closed for selected batch."), enMsgBoxStyle.Information)
                lvEmpBatchPosting.Select()
                Exit Try
            End If

            If CBool(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPosted.Index).Tag) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry! You can not post the selected batch to ED. Reason: selected batch is already posted to ED."), enMsgBoxStyle.Information)
                lvEmpBatchPosting.Select()
                Exit Try
            End If


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objBatchPostingTran._Empbatchpostingunkid = CInt(lvEmpBatchPosting.SelectedItems(0).Tag)
            objBatchPostingTran.GetBatchPosting_Tran(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_Start, mdtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(lvEmpBatchPosting.SelectedItems(0).Tag), False, "")
            'Sohail (21 Aug 2015) -- End
            dtTable = objBatchPostingTran._DataTable
            For Each dtRow As DataRow In dtTable.Rows
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTnA.IsPayrollProcessDone(objPeriod._Periodunkid, dtRow.Item("employeeunkid").ToString, objPeriod._End_Date) = True Then
                If objTnA.IsPayrollProcessDone(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName), dtRow.Item("employeeunkid").ToString, objPeriod._End_Date) = True Then
                    'Sohail (21 Aug 2015) -- End
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to post earning deduction."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to post earning deduction.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 20, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    Exit Try
                End If
            Next
            'For Each lvItem As ListViewItem In lvEmpBatchPosting.CheckedItems
            '    If objTnA.IsPayrollProcessDone(objPeriod._Periodunkid, lvItem.SubItems(colhEmployeeName.Index).Tag.ToString, objPeriod._End_Date) = True Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to post earning deduction."), enMsgBoxStyle.Information)
            '        Exit Try
            '    End If
            'Next




            If chkCopyPreviousEDSlab.Checked = True Then
                If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to Post selected transactions to earning deduction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Try
            End If


            Dim mdtED As DataTable = objED._DataSource
            Dim dRow As DataRow
            mdtED.Rows.Clear()

            Cursor.Current = Cursors.WaitCursor

            For Each dtRow As DataRow In dtTable.Rows
                dRow = mdtED.NewRow

                dRow.Item("edunkid") = -1
                'dRow.Item("employeeunkid") = CInt(lvItem.SubItems(colhEmployeeName.Index).Tag)
                'dRow.Item("tranheadunkid") = CInt(lvItem.SubItems(colhTranHeadName.Index).Tag)
                'dRow.Item("trnheadname") = ""
                'dRow.Item("batchtransactionunkid") = -1
                'dRow.Item("amount") = CDec(lvItem.SubItems(colhAmount.Index).Text)
                'dRow.Item("currencyid") = 0
                'dRow.Item("vendorid") = 0
                'dRow.Item("userunkid") = User._Object._Userunkid
                'dRow.Item("isvoid") = False
                'dRow.Item("voiduserunkid") = -1
                'dRow.Item("voiddatetime") = DBNull.Value
                'dRow.Item("voidreason") = ""
                'dRow.Item("membership_categoryunkid") = 0
                'If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                '    dRow.Item("isapproved") = True
                '    dRow.Item("approveruserunkid") = User._Object._Userunkid
                'Else
                '    dRow.Item("isapproved") = False
                '    dRow.Item("approveruserunkid") = -1
                'End If

                'objPeriod = New clscommom_period_Tran
                'objPeriod._Periodunkid = CInt(lvItem.SubItems(colhPeriod.Index).Tag)
                'dRow.Item("periodunkid") = CInt(lvItem.SubItems(colhPeriod.Index).Tag)
                'dRow.Item("start_date") = eZeeDate.convertDate(objPeriod._Start_Date)
                'dRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)
                dRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                dRow.Item("tranheadunkid") = CInt(dtRow.Item("tranheadunkid"))
                'Sohail (31 Oct 2019) -- Start
                'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
                'dRow.Item("trnheadname") = ""
                dRow.Item("trnheadname") = dtRow.Item("TranHeadName").ToString
                dRow.Item("calctype_id") = CInt(dtRow.Item("calctype_id"))
                dRow.Item("employeecode") = dtRow.Item("employeecode").ToString
                dRow.Item("employeename") = dtRow.Item("EmpName").ToString
                'Sohail (31 Oct 2019) -- End
                dRow.Item("batchtransactionunkid") = -1
                dRow.Item("amount") = CDec(dtRow.Item("amount"))
                dRow.Item("currencyid") = 0
                dRow.Item("vendorid") = 0
                dRow.Item("userunkid") = User._Object._Userunkid
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("membership_categoryunkid") = 0
                If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                    dRow.Item("isapproved") = True
                    dRow.Item("approveruserunkid") = User._Object._Userunkid
                Else
                    dRow.Item("isapproved") = False
                    dRow.Item("approveruserunkid") = -1
                End If

                objPeriod = New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(dtRow.Item("periodunkid"))
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dtRow.Item("periodunkid"))
                'Sohail (21 Aug 2015) -- End
                dRow.Item("periodunkid") = CInt(dtRow.Item("periodunkid"))
                dRow.Item("start_date") = eZeeDate.convertDate(objPeriod._Start_Date)
                dRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)

                dRow.Item("costcenterunkid") = 0
                dRow.Item("medicalrefno") = ""

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                dRow.Item("membershiptranunkid") = 0
                dRow.Item("disciplinefileunkid") = 0
                dRow.Item("cumulative_startdate") = DBNull.Value
                dRow.Item("stop_date") = DBNull.Value
                'Sohail (09 Nov 2013) -- End
                'Sohail (24 Aug 2019) -- Start
                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                dRow.Item("edstart_date") = DBNull.Value
                'Sohail (24 Aug 2019) -- End
                dRow.Item("empbatchpostingunkid") = CInt(lvEmpBatchPosting.SelectedItems(0).Tag) 'Sohail (24 Feb 2016)

                dRow.Item("AUD") = "A"

                mdtED.Rows.Add(dRow)
            Next

            If mdtED.Rows.Count > 0 Then
                Dim blnFlag As Boolean = False

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objED.InsertAllByDataTable(mdtED, chkOverwriteIfExist.Checked, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, User._Object._Userunkid)
                'Sohail (15 Dec 2018) -- Start
                'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
                'blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, mdtED, chkOverwriteIfExist.Checked, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                'Sohail (25 Jun 2020) -- Start
                'NMB Enhancement # : Performance enhancement on Import earning deduction heads.
                'blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, mdtED, chkOverwriteIfExist.Checked, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, Nothing, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                blnFlag = objED.InsertAllByDataTable(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, mdtED, chkOverwriteIfExist.Checked, User._Object._Userunkid, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, Nothing, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "", True)
                'Sohail (25 Jun 2020) -- End
                'Sohail (15 Dec 2018) -- End
                'Sohail (21 Aug 2015) -- End

                Cursor.Current = Cursors.Default

                If blnFlag = False And objED._Message <> "" Then
                    eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
                    Exit Try
                End If

                If blnFlag Then
                    blnFlag = False

                    Cursor.Current = Cursors.WaitCursor
                    'For Each lvItem As ListViewItem In lvEmpBatchPosting.CheckedItems
                    '    blnFlag = objBatchPosting.UpdateStatus(CInt(lvItem.Text), True, True, True)

                    '    If blnFlag = False And objBatchPosting._Message <> "" Then
                    '        Cursor.Current = Cursors.Default
                    '        eZeeMsgBox.Show(objBatchPosting._Message, enMsgBoxStyle.Information)
                    '        Exit Try
                    '    End If
                    'Next
                    blnFlag = objBatchPosting.UpdateStatus(CInt(lvEmpBatchPosting.SelectedItems(0).Tag), True, True, True)

                    If blnFlag = False And objBatchPosting._Message <> "" Then
                        Cursor.Current = Cursors.Default
                        eZeeMsgBox.Show(objBatchPosting._Message, enMsgBoxStyle.Information)
                        Exit Try
                    End If

                    'Sohail (31 Oct 2019) -- Start
                    'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
                    If blnFlag = True Then
                        If User._Object.Privilege._AllowToApproveEarningDeduction = False AndAlso chkOverwriteIfExist.Checked = True Then
                            If mdtED.Columns.Contains("trnheadname") = True Then
                                mdtED.Columns("trnheadname").ColumnName = "tranheadname"
                            End If
                            Call objED.SendMailToApprover(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, User._Object._Userunkid, mdtPeriod_Start, mdtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, False, User._Object.Privilege._AllowToApproveEarningDeduction, mdtED, CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPeriod.Index).Tag), GUI.fmtCurrency, enLogin_Mode.DESKTOP, mstrModuleName, 0)
                        End If
                    End If
                    'Sohail (31 Oct 2019) -- End

                    Cursor.Current = Cursors.Default
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Posting to ED Process completed successfully."), enMsgBoxStyle.Information)
                        Call FillList()
                    End If
                End If
            End If

            Cursor.Current = Cursors.Default

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPostToED_Click", mstrModuleName)
        Finally
            objPeriod = Nothing
            objED = Nothing
            objTnA = Nothing
            objBatchPostingTran = Nothing
        End Try
    End Sub

    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    Private Sub mnuVoidBatchPosting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVoidBatchPosting.Click
        Dim objPeriod As New clscommom_period_Tran
        Dim objBatchPostingTran As New clsBatchPostingTran
        Dim objTnA As New clsTnALeaveTran
        Dim objED As New clsEarningDeduction
        Dim dtTable As DataTable
        Try
            If lvEmpBatchPosting.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information)
                lvEmpBatchPosting.Select()
                Exit Try
            End If

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPeriod.Index).Tag)
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry! You can not void posting of the selected batch to ED. Reason: Period is closed for selected batch."), enMsgBoxStyle.Information)
                lvEmpBatchPosting.Select()
                Exit Try
            End If

            If CBool(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPosted.Index).Tag) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry! You cannot void posting of the selected batch to ED. Reason: selected batch is not posted to ED."), enMsgBoxStyle.Information)
                lvEmpBatchPosting.Select()
                Exit Try
            End If

            objBatchPostingTran.GetBatchPosting_Tran(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(lvEmpBatchPosting.SelectedItems(0).Tag), False, "")
            dtTable = objBatchPostingTran._DataTable

            For Each dtRow As DataRow In dtTable.Rows
                If objTnA.IsPayrollProcessDone(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName), dtRow.Item("employeeunkid").ToString, objPeriod._End_Date) = True Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to void posting of batch."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to void posting of batch.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 20, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    Exit Try
                End If
            Next

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Are you sure you want to Void Posting of selected batch to ED?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Try
            End If

            Dim frm As New frmReasonSelection
            Dim mstrVoidReason As String = String.Empty
            frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
            If mstrVoidReason.Length <= 0 Then
                Exit Sub
            End If
            frm = Nothing

            Cursor.Current = Cursors.WaitCursor
            Dim blnFlag As Boolean = False
            blnFlag = objED.VoidEDbyBatchPostingUnkID(FinancialYear._Object._DatabaseName, CInt(lvEmpBatchPosting.SelectedItems(0).Tag), CInt(lvEmpBatchPosting.SelectedItems(0).SubItems(colhPeriod.Index).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, "", "", -1, "", "", "", "", "", "")

            Cursor.Current = Cursors.Default

            If blnFlag = False And objED._Message <> "" Then
                eZeeMsgBox.Show(objED._Message, enMsgBoxStyle.Information)
                Exit Try
            End If

            If blnFlag Then
                blnFlag = False

                Cursor.Current = Cursors.WaitCursor
                blnFlag = objBatchPosting.UpdateStatus(CInt(lvEmpBatchPosting.SelectedItems(0).Tag), False, False, True, True)

                If blnFlag = False And objBatchPosting._Message <> "" Then
                    Cursor.Current = Cursors.Default
                    eZeeMsgBox.Show(objBatchPosting._Message, enMsgBoxStyle.Information)
                    Exit Try
                End If

                Cursor.Current = Cursors.Default
                If blnFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Void Batch Posting to ED Process completed successfully."), enMsgBoxStyle.Information)
                    Call FillList()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuVoidBatchPosting", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
    'Sohail (24 Feb 2016) -- End

    'Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim objfrm As New frmCommonSearch
    '    Try
    '        If User._Object._Isrighttoleft = True Then
    '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objfrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objfrm)
    '        End If

    '        With objfrm
    '            .ValueMember = cboEmployee.ValueMember
    '            .DisplayMember = cboEmployee.DisplayMember
    '            .DataSource = CType(cboEmployee.DataSource, DataTable)
    '            .CodeMember = "employeecode"
    '        End With
    '        If objfrm.DisplayDialog Then
    '            cboEmployee.SelectedValue = objfrm.SelectedValue
    '            cboEmployee.Focus()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub objbtnSearchTranHead_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim objfrm As New frmCommonSearch
    '    Try
    '        If User._Object._Isrighttoleft = True Then
    '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objfrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objfrm)
    '        End If

    '        With objfrm
    '            .ValueMember = cboTranHead.ValueMember
    '            .DisplayMember = cboTranHead.DisplayMember
    '            .DataSource = CType(cboTranHead.DataSource, DataTable)
    '            .CodeMember = "code"
    '        End With
    '        If objfrm.DisplayDialog Then
    '            cboTranHead.SelectedValue = objfrm.SelectedValue
    '            cboTranHead.Focus()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'txtBatchPostingNo.Text = ""
            cboBatch.SelectedIndex = -1
            'Sohail (24 Feb 2016) -- End
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            'cboEmployee.SelectedValue = 0
            'cboTranHead.SelectedValue = 0
            cboStatus.SelectedValue = 2
            mstrAdvanceFilter = ""

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
#End Region




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.colhID.Text = Language._Object.getCaption(CStr(Me.colhID.Tag), Me.colhID.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.chkOverwritePrevEDSlabHeads.Text = Language._Object.getCaption(Me.chkOverwritePrevEDSlabHeads.Name, Me.chkOverwritePrevEDSlabHeads.Text)
            Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
            Me.colhPosted.Text = Language._Object.getCaption(CStr(Me.colhPosted.Tag), Me.colhPosted.Text)
            Me.chkOverwriteIfExist.Text = Language._Object.getCaption(Me.chkOverwriteIfExist.Name, Me.chkOverwriteIfExist.Text)
            Me.mnuPostToED.Text = Language._Object.getCaption(Me.mnuPostToED.Name, Me.mnuPostToED.Text)
            Me.colhBatchNo.Text = Language._Object.getCaption(CStr(Me.colhBatchNo.Tag), Me.colhBatchNo.Text)
            Me.lblBatch.Text = Language._Object.getCaption(Me.lblBatch.Name, Me.lblBatch.Text)
            Me.colhBatchCode.Text = Language._Object.getCaption(CStr(Me.colhBatchCode.Tag), Me.colhBatchCode.Text)
            Me.colhBatchName.Text = Language._Object.getCaption(CStr(Me.colhBatchName.Tag), Me.colhBatchName.Text)
            Me.colhUser.Text = Language._Object.getCaption(CStr(Me.colhUser.Tag), Me.colhUser.Text)
            Me.colhDesc.Text = Language._Object.getCaption(CStr(Me.colhDesc.Tag), Me.colhDesc.Text)
            Me.mnuVoidBatchPosting.Text = Language._Object.getCaption(Me.mnuVoidBatchPosting.Name, Me.mnuVoidBatchPosting.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to Post selected transactions to earning deduction?")
            Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete selected transactions?")
            Language.setMessage(mstrModuleName, 5, "Sorry! You can not edit the selected transaction. Reason: Period is closed.")
            Language.setMessage(mstrModuleName, 6, "Sorry! You can not delete the selected transactions. Reason: Period is closed for selected transaction.")
            Language.setMessage(mstrModuleName, 7, "Sorry! You can not post the selected batch. Reason: Period is closed for selected batch.")
            Language.setMessage(mstrModuleName, 8, "Posting to ED Process completed successfully.")
            Language.setMessage(mstrModuleName, 9, "YES")
            Language.setMessage(mstrModuleName, 10, "NO")
            Language.setMessage(mstrModuleName, 11, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to post earning deduction.")
            Language.setMessage(mstrModuleName, 12, "Sorry! You can not delete the selected transactions. Reason: selected batch is already posted to ED.")
            Language.setMessage(mstrModuleName, 13, "Sorry! You can not post the selected batch to ED. Reason: selected batch is already posted to ED.")
            Language.setMessage(mstrModuleName, 14, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?")
            Language.setMessage(mstrModuleName, 15, "Sorry! You can not void posting of the selected batch to ED. Reason: Period is closed for selected batch.")
            Language.setMessage(mstrModuleName, 16, "Sorry! You cannot void posting of the selected batch to ED. Reason: selected batch is not posted to ED.")
            Language.setMessage(mstrModuleName, 17, "Sorry, Process Payroll is already done for last date of selected Period for some of the employees. Please Void Process Payroll first for selected employees to void posting of batch.")
            Language.setMessage(mstrModuleName, 18, "Are you sure you want to Void Posting of selected batch to ED?")
            Language.setMessage(mstrModuleName, 19, "Void Batch Posting to ED Process completed successfully.")
			Language.setMessage(mstrModuleName, 20, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
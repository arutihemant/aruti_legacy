﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmStatutoryPayment

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmStatutoryPayment"

    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_CONTINUE
    Private mintStatutorypaymentmasterunkid As Integer = 0

    Private objStatutoryPaymentMaster As clsStatutorypayment_master
    Private objStatutoryPaymentTran As clsStatutoryPayment_Tran

    Private mdtPayPeriodStartDate As DateTime
    Private mdtPayPeriodEndDate As DateTime
    Private mdtTable As DataTable = Nothing
    Private lstIDs As New List(Of Integer)

    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Display Dialogue "
    Public Function DisplayDialog(ByRef intUnkId As Integer, ByVal Action As enAction) As Boolean
        Try
            menAction = Action
            mintStatutorypaymentmasterunkid = intUnkId

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DisplayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods Functions "

    Private Sub SetColor()
        Try
            cboFromPeriod.BackColor = GUI.ColorComp
            cboToPeriod.BackColor = GUI.ColorComp
            cboMembership.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorOptional
            txtReceiptNo.BackColor = GUI.ColorComp
            txtReceiptAmount.BackColor = GUI.ColorComp

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboFromPeriod.SelectedValue = objStatutoryPaymentMaster._Fromperiodunkid
            cboToPeriod.SelectedValue = objStatutoryPaymentMaster._Toperiodunkid
            cboMembership.SelectedValue = objStatutoryPaymentMaster._Membershipunkid
            txtReceiptNo.Text = objStatutoryPaymentMaster._Receiptno
            If menAction = enAction.EDIT_ONE Then
                dtpReceiptDate.Value = objStatutoryPaymentMaster._Receiptdate
            End If
            txtTotalPaymentAmount.Text = Format(objStatutoryPaymentMaster._Total_Payableamount, GUI.fmtCurrency)
            txtReceiptAmount.Text = Format(objStatutoryPaymentMaster._Total_Receiptamount, GUI.fmtCurrency)
            txtRemark.Text = objStatutoryPaymentMaster._Remark
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objStatutoryPaymentMaster._Fromperiodunkid = CInt(cboFromPeriod.SelectedValue)
            objStatutoryPaymentMaster._Toperiodunkid = CInt(cboToPeriod.SelectedValue)
            objStatutoryPaymentMaster._Membershipunkid = CInt(cboMembership.SelectedValue)
            objStatutoryPaymentMaster._Receiptno = txtReceiptNo.Text.Trim
            objStatutoryPaymentMaster._Receiptdate = dtpReceiptDate.Value
            objStatutoryPaymentMaster._Total_Payableamount = txtTotalPaymentAmount.Decimal
            objStatutoryPaymentMaster._Total_Receiptamount = txtReceiptAmount.Decimal
            objStatutoryPaymentMaster._Remark = txtRemark.Text.Trim
            objStatutoryPaymentMaster._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMember As New clsmembership_master
        Dim dsCombo As DataSet

        Try
            'Sohail (05 Sep 2014) -- Start
            'Enhancement - Allow Previous Closed Years on Statutory Payment screen.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "PayPeriod", True, enStatusType.Close)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "PayPeriod", True, enStatusType.Close)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True, enStatusType.Close)
            'Sohail (21 Aug 2015) -- End
            'Sohail (05 Sep 2014) -- End

            With cboFromPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                If .Items.Count > 0 Then .SelectedValue = 0
                .EndUpdate()
            End With

            With cboToPeriod
                .BeginUpdate()
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("PayPeriod").Copy
                If .Items.Count > 0 Then .SelectedValue = 0
                .EndUpdate()
            End With

            dsCombo = objMember.getListForCombo("Membership", True, , 1)
            With cboMembership
                .BeginUpdate()
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Membership")
                .SelectedValue = 0
                .EndUpdate()
            End With



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objMember = Nothing
        End Try
    End Sub

    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate)
            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPayPeriodStartDate, _
                                           mdtPayPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "Employee", True)
            'Anjan [10 June 2015] -- End

            With cboEmployee
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Employee")
                .SelectedValue = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objPayroll As New clsPayrollProcessTran
        Dim dsList As DataSet
        Dim strEmpFilter As String = ""

        Try
            lvEmployeeList.Items.Clear()
            objchkSelectAll.Checked = False
            If CInt(cboFromPeriod.SelectedValue) <= 0 OrElse CInt(cboToPeriod.SelectedValue) <= 0 OrElse CInt(cboMembership.SelectedValue) <= 0 Then Exit Try
            Cursor.Current = Cursors.WaitCursor

            Dim lvItem As ListViewItem
            lvEmployeeList.BeginUpdate()

            strEmpFilter = mstrAdvanceFilter
            If CInt(cboEmployee.SelectedValue) > 0 Then
                If strEmpFilter.Trim <> "" Then
                    strEmpFilter &= " AND hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                Else
                    strEmpFilter &= " hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If
            End If

            'Sohail (05 Sep 2014) -- Start
            'Enhancement - Allow Previous Closed Years on Statutory Payment screen.
            'If menAction = enAction.EDIT_ONE Then
            '    dsList = objPayroll.GetMembershipDetail("Membership", mdtPayPeriodStartDate, mdtPayPeriodEndDate, CInt(cboMembership.SelectedValue), mintStatutorypaymentmasterunkid, , strEmpFilter, True, True, True)
            'Else
            '    dsList = objPayroll.GetMembershipDetail("Membership", mdtPayPeriodStartDate, mdtPayPeriodEndDate, CInt(cboMembership.SelectedValue), , , strEmpFilter, True, True, True)
            'End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Dim objPeriod As New clscommom_period_Tran
            'Dim srrDBIds As String = objPeriod.GetDISTINCTDatabaseNAMEs(enModuleReference.Payroll, mdtPayPeriodStartDate, mdtPayPeriodEndDate)
            Dim objMaster As New clsMasterData
            Dim dsYear As DataSet = objMaster.Get_Database_Year_List("List", True, Company._Object._Companyunkid, , )
            Dim dicYear As Dictionary(Of Integer, String) = (From p In dsYear.Tables(0).AsEnumerable Select New With {.id = CInt(p.Item("yearunkid")), .name = p.Item("database_name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            'Sohail (21 Aug 2015) -- End

            If menAction = enAction.EDIT_ONE Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPayroll.GetMembershipDetail("Membership", srrDBIds, mdtPayPeriodStartDate, mdtPayPeriodEndDate, CInt(cboMembership.SelectedValue), mintStatutorypaymentmasterunkid, , strEmpFilter, True, True, True)
                dsList = objPayroll.GetMembershipDetail(dicYear, User._Object._Userunkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, ConfigParameter._Object._Base_CurrencyId, "Membership", CInt(cboMembership.SelectedValue), ConfigParameter._Object._FirstNamethenSurname, mintStatutorypaymentmasterunkid, , True, True, True, True, strEmpFilter)
                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPayroll.GetMembershipDetail("Membership", srrDBIds, mdtPayPeriodStartDate, mdtPayPeriodEndDate, CInt(cboMembership.SelectedValue), , , strEmpFilter, True, True, True)
                dsList = objPayroll.GetMembershipDetail(dicYear, User._Object._Userunkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, True, ConfigParameter._Object._Base_CurrencyId, "Membership", CInt(cboMembership.SelectedValue), ConfigParameter._Object._FirstNamethenSurname, , , True, True, True, True, strEmpFilter)
                'Sohail (21 Aug 2015) -- End
            End If
            'Sohail (05 Sep 2014) -- End

            For Each dtRow As DataRow In dsList.Tables("Membership").Rows
                lvItem = New ListViewItem

                With lvItem
                    .Text = ""
                    .Tag = CInt(dtRow.Item("statutorypaymenttranunkid"))
                    If lstIDs.Contains(CInt(dtRow.Item("statutorypaymenttranunkid"))) = True Then
                        .Checked = True
                    End If

                    .SubItems.Add(dtRow.Item("employeecode").ToString)
                    .SubItems.Add(dtRow.Item("employeename").ToString)
                    .SubItems(colhName.Index).Tag = CInt(dtRow.Item("employeeunkid"))

                    .SubItems.Add(Format(dtRow.Item("EmpContribution"), GUI.fmtCurrency))
                    .SubItems(colhEmpContribution.Index).Tag = CDec(dtRow.Item("EmpContribution"))

                    .SubItems.Add(Format(dtRow.Item("EmployerContribution"), GUI.fmtCurrency))
                    .SubItems(colhEmplContribution.Index).Tag = CDec(dtRow.Item("EmployerContribution"))

                    .SubItems.Add(Format(dtRow.Item("TotalContribution"), GUI.fmtCurrency))
                    .SubItems(colhTotContribution.Index).Tag = CDec(dtRow.Item("TotalContribution"))

                    .SubItems.Add(dtRow.Item("membershiptranunkid").ToString)

                    'Comment these 2 lines if do not want grouping on period
                    .SubItems.Add(dtRow.Item("PeriodName").ToString)
                    .SubItems(colhPeriod.Index).Tag = CInt(dtRow.Item("periodunkid").ToString)

                    RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
                    lvEmployeeList.Items.Add(lvItem)
                    AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked


                End With
            Next

            'Comment these 2 lines if do not want grouping on period
            lvEmployeeList.GroupingColumn = colhPeriod
            lvEmployeeList.DisplayGroups(True)

            If lvEmployeeList.Items.Count > 8 Then
                colhName.Width = 150 - 18
            Else
                colhName.Width = 150
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objPayroll = Nothing
            objlblEmpCount.Text = "( " & lvEmployeeList.Items.Count.ToString & " )"
            lvEmployeeList.EndUpdate()
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvEmployeeList.Items
                RemoveHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvEmployeeList.ItemChecked, AddressOf lvEmployeeList_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub CalculateTotalAmount()
        Dim decTotAmt As Decimal = 0
        Try
            decTotAmt = (From p In lvEmployeeList.CheckedItems.Cast(Of ListViewItem)() Select CDec(p.SubItems(colhTotContribution.Index).Text)).DefaultIfEmpty.Sum()
            txtTotalPaymentAmount.Text = Format(decTotAmt, GUI.fmtCurrency)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CalculateTotalAmount", mstrModuleName)
        End Try
    End Sub

    Private Function CreateDatatable() As Boolean
        Try
            mdtTable = New DataTable
            With mdtTable

                .Columns.Add("statutorypaymenttranunkid", Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("periodunkid", Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("employeeunkid", Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("membershiptranunkid", Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("emp_contribution", Type.GetType("System.Decimal")).DefaultValue = 0
                .Columns.Add("empl_contribution", Type.GetType("System.Decimal")).DefaultValue = 0
                .Columns.Add("total_contribution", Type.GetType("System.Decimal")).DefaultValue = 0
                .Columns.Add("remark", Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("userunkid", Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("isvoid", Type.GetType("System.Boolean")).DefaultValue = False
                .Columns.Add("voiduserunkid", Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("voiddatetime", Type.GetType("System.DateTime"))
                .Columns.Add("voidreason", Type.GetType("System.String")).DefaultValue = ""

            End With

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDatatable", mstrModuleName)
        End Try
    End Function

    Private Function IsValid() As Boolean
        Try
            If CInt(cboFromPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select From Period. From Period is mandatory information."), enMsgBoxStyle.Information)
                cboFromPeriod.Focus()
                Return False
            ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select To Period. To Period is mandatory information."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            ElseIf cboFromPeriod.SelectedIndex > cboToPeriod.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            ElseIf CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Membership. Membership is mandatory information."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            ElseIf txtReceiptNo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please enter Receipt No.. Receipt No. is mandatory information."), enMsgBoxStyle.Information)
                txtReceiptNo.Focus()
                Return False
                'Sohail (05 Sep 2014) -- Start
                'Enhancement - Show Checkbox on Date control on Statutory Payment screen.
            ElseIf dtpReceiptDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please set Receipt Date."), enMsgBoxStyle.Information)
                dtpReceiptDate.Focus()
                Return False
                'Sohail (05 Sep 2014) -- End
            ElseIf dtpReceiptDate.Value.Date < mdtPayPeriodStartDate OrElse dtpReceiptDate.Value.Date > FinancialYear._Object._Database_End_Date.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Receipt Date should be between ") & mdtPayPeriodStartDate.ToShortDateString & Language.getMessage(mstrModuleName, 7, " And ") & FinancialYear._Object._Database_End_Date.ToShortDateString & ".", enMsgBoxStyle.Information)
                dtpReceiptDate.Focus()
                Return False
            ElseIf lvEmployeeList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please check atleast one employee."), enMsgBoxStyle.Information)
                lvEmployeeList.Focus()
                Return False
            ElseIf txtTotalPaymentAmount.Decimal <> txtReceiptAmount.Decimal Then
                'Sohail (05 Sep 2014) -- Start
                'Enhancement - Receipt amount should be same as Total Payable amount on Statutory Payment screen.
                'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Total Payable Amount is different from Paid Amount. Do you want to proceed anyway?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                '    lvEmployeeList.Focus()
                '    Return False
                'End If
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Receipt Amount should be same as Total Payable Amount"))
                txtReceiptAmount.Focus()
                Return False
                'Sohail (05 Sep 2014) -- End
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmStatutoryPayment_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objStatutoryPaymentMaster = Nothing
            objStatutoryPaymentTran = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStatutoryPayment_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStatutoryPayment_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                    e.Handled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStatutoryPayment_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStatutoryPayment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objStatutoryPaymentMaster = New clsStatutorypayment_master
        objStatutoryPaymentTran = New clsStatutoryPayment_Tran
        lstIDs = New List(Of Integer)
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objStatutoryPaymentMaster._Statutorypaymentmasterunkid = mintStatutorypaymentmasterunkid
                objStatutoryPaymentTran._Statutorypaymentmasterunkid = mintStatutorypaymentmasterunkid
                mdtTable = objStatutoryPaymentTran._Datatable
                If mdtTable IsNot Nothing Then
                    'cboFromPeriod.SelectedValue = CInt((From p In mdtTable Select (p)).OrderBy(Function(x) x.Item("end_date").ToString).First().Item("periodunkid"))
                    'cboToPeriod.SelectedValue = CInt((From p In mdtTable Select (p)).OrderByDescending(Function(x) x.Item("end_date").ToString).l().Item("periodunkid"))
                    'cboFromPeriod.SelectedValue = (From p In mdtTable Select (CInt(p.Item("periodunkid")))).FirstOrDefault()
                    'cboToPeriod.SelectedValue = (From p In mdtTable Select (CInt(p.Item("periodunkid")))).LastOrDefault()
                    cboFromPeriod.Enabled = False
                    cboToPeriod.Enabled = False
                    cboMembership.Enabled = False
                    cboEmployee.Enabled = False
                    lnkAllocation.Enabled = False
                    objbtnSearch.Enabled = False
                    objbtnReset.Enabled = False
                    lstIDs = (From p In mdtTable Select CInt(p.Item("statutorypaymenttranunkid"))).ToList
                End If
            End If
            Call GetValue()

            If menAction = enAction.EDIT_ONE Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStatutoryPayment_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsStatutorypayment_master.SetMessages()
            clsStatutoryPayment_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsStatutorypayment_master, clsStatutoryPayment_Tran"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Private Sub cboFromPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFromPeriod.SelectedIndexChanged, cboToPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try
            If CType(sender, ComboBox).Name = cboFromPeriod.Name Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboFromPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboFromPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPayPeriodStartDate = objPeriod._Start_Date
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboToPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                mdtPayPeriodEndDate = objPeriod._End_Date
            End If

            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillEmployeeCombo()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFromPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub cboMembership_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMembership.SelectedIndexChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim blnFlag As Boolean = False
        Dim dtRow As DataRow

        Try
            If IsValid() = False Then Exit Sub

            If CreateDatatable() = False Then Exit Sub

            Call SetValue()


            For Each lvItem As ListViewItem In lvEmployeeList.CheckedItems
                With mdtTable
                    dtRow = .NewRow()

                    dtRow.Item("statutorypaymenttranunkid") = CInt(lvItem.Tag)
                    dtRow.Item("periodunkid") = CInt(lvItem.SubItems(colhPeriod.Index).Tag)
                    dtRow.Item("employeeunkid") = CInt(lvItem.SubItems(colhName.Index).Tag)
                    dtRow.Item("membershiptranunkid") = CInt(lvItem.SubItems(objcolhMembertranunkid.Index).Text)
                    dtRow.Item("emp_contribution") = CDec(lvItem.SubItems(colhEmpContribution.Index).Text)
                    dtRow.Item("empl_contribution") = CDec(lvItem.SubItems(colhEmplContribution.Index).Text)
                    dtRow.Item("total_contribution") = CDec(lvItem.SubItems(colhTotContribution.Index).Text)

                    dtRow.Item("userunkid") = User._Object._Userunkid
                    dtRow.Item("isvoid") = False
                    dtRow.Item("voiduserunkid") = 0
                    dtRow.Item("voiddatetime") = DBNull.Value
                    dtRow.Item("voidreason") = ""

                    .Rows.Add(dtRow)
                End With
            Next

            If menAction = enAction.EDIT_ONE Then
                Dim lstDelIDs As List(Of String) = lstIDs.Where(Function(x) mdtTable.AsEnumerable.Any(Function(y) CInt(y.Item("statutorypaymenttranunkid")) <> x)).[Select](Function(x) x.ToString).ToList
                Dim strDelIDs As String = String.Join(",", lstDelIDs.ToArray)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objStatutoryPaymentMaster.Update(mdtTable, strDelIDs)
                blnFlag = objStatutoryPaymentMaster.Update(mdtTable, strDelIDs, ConfigParameter._Object._CurrentDateAndTime)
                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objStatutoryPaymentMaster.Insert(mdtTable)
                blnFlag = objStatutoryPaymentMaster.Insert(mdtTable, ConfigParameter._Object._CurrentDateAndTime)
                'Sohail (21 Aug 2015) -- End
            End If

            If blnFlag = False And objStatutoryPaymentMaster._Message <> "" Then
                eZeeMsgBox.Show(objStatutoryPaymentMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objStatutoryPaymentMaster = Nothing
                    objStatutoryPaymentMaster = New clsStatutorypayment_master
                    cboFromPeriod.SelectedValue = 0
                    cboToPeriod.SelectedValue = 0
                    cboMembership.SelectedValue = 0
                    txtReceiptNo.Text = ""
                    txtReceiptAmount.Decimal = 0
                    txtRemark.Text = ""
                    objchkSelectAll.CheckState = CheckState.Unchecked
                    txtTotalPaymentAmount.Text = Format(0, GUI.fmtCurrency)
                    cboFromPeriod.Focus()
                Else
                    Me.Close()
                End If
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Statutory Payment Process completed successfully."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnProcess_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Listview's Events "

    Private Sub lvEmployeeList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvEmployeeList.ItemChecked
        Try
            If lvEmployeeList.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvEmployeeList.CheckedItems.Count < lvEmployeeList.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvEmployeeList.CheckedItems.Count = lvEmployeeList.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If

            Call CalculateTotalAmount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvEmployeeList_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)

            Call CalculateTotalAmount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Textbox's Events "
    Private Sub txtPaidAmount_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtReceiptAmount.LostFocus
        Try
            txtReceiptAmount.Text = Format(txtReceiptAmount.Decimal, GUI.fmtCurrency)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtPaidAmount_LostFocus", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With cboEmployee
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = 0
            mstrAdvanceFilter = ""

            lvEmployeeList.Items.Clear()
            If objchkSelectAll.Checked = True Then objchkSelectAll.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbStatutoryPaymentInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStatutoryPaymentInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbAdvanceAmountInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAdvanceAmountInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnProcess.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbStatutoryPaymentInfo.Text = Language._Object.getCaption(Me.gbStatutoryPaymentInfo.Name, Me.gbStatutoryPaymentInfo.Text)
            Me.lblReceiptDate.Text = Language._Object.getCaption(Me.lblReceiptDate.Name, Me.lblReceiptDate.Text)
            Me.lblFromPeriod.Text = Language._Object.getCaption(Me.lblFromPeriod.Name, Me.lblFromPeriod.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblSearchEmp.Text = Language._Object.getCaption(Me.lblSearchEmp.Name, Me.lblSearchEmp.Text)
            Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.Name, Me.btnProcess.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.Name, Me.lblToPeriod.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
            Me.lblReceiptNo.Text = Language._Object.getCaption(Me.lblReceiptNo.Name, Me.lblReceiptNo.Text)
            Me.lblReceiptAmount.Text = Language._Object.getCaption(Me.lblReceiptAmount.Name, Me.lblReceiptAmount.Text)
            Me.gbAdvanceAmountInfo.Text = Language._Object.getCaption(Me.gbAdvanceAmountInfo.Name, Me.gbAdvanceAmountInfo.Text)
            Me.lblTotalPaymentAmount.Text = Language._Object.getCaption(Me.lblTotalPaymentAmount.Name, Me.lblTotalPaymentAmount.Text)
            Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
            Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhTotContribution.Text = Language._Object.getCaption(CStr(Me.colhTotContribution.Tag), Me.colhTotContribution.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.colhEmpContribution.Text = Language._Object.getCaption(CStr(Me.colhEmpContribution.Tag), Me.colhEmpContribution.Text)
            Me.colhEmplContribution.Text = Language._Object.getCaption(CStr(Me.colhEmplContribution.Tag), Me.colhEmplContribution.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select From Period. From Period is mandatory information.")
            Language.setMessage(mstrModuleName, 2, "Please select To Period. To Period is mandatory information.")
            Language.setMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period.")
            Language.setMessage(mstrModuleName, 4, "Please select Membership. Membership is mandatory information.")
            Language.setMessage(mstrModuleName, 5, "Please enter Receipt No.. Receipt No. is mandatory information.")
            Language.setMessage(mstrModuleName, 6, "Receipt Date should be between")
            Language.setMessage(mstrModuleName, 7, " And")
            Language.setMessage(mstrModuleName, 8, "Please check atleast one employee.")
            Language.setMessage(mstrModuleName, 9, "Sorry, Receipt Amount should be same as Total Payable Amount")
            Language.setMessage(mstrModuleName, 10, "Statutory Payment Process completed successfully.")
            Language.setMessage(mstrModuleName, 11, "Please set Receipt Date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
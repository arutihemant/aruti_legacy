﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBudgetPreparation_Wizard

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmBudgetPreparation_Wizard"

    Private menAllocation As enAnalysisRef
    Private mstrAllocation As String = "" 'Sohail (28 Dec 2010)

    Dim objBudget_Master As clsBudget_Master
    Dim objBudget_Tran As clsBudget_Tran
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mdtPeriod_Start As Date
    Private mdtPeriod_End As Date
    'Sohail (21 Aug 2015) -- End
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            lblStep1.ForeColor = SystemColors.WindowText
            lblStep2.ForeColor = SystemColors.ControlDark
            lblStep4.ForeColor = SystemColors.ControlDark
            lblStep5.ForeColor = SystemColors.ControlDark
            lblStep3.ForeColor = SystemColors.ControlDark
            lblStep6.ForeColor = SystemColors.ControlDark
            lblStep7.ForeColor = SystemColors.ControlDark

            picSide_Step1_Check.Visible = False
            picSide_Step2_Check.Visible = False
            picSide_Step4_Check.Visible = False
            picSide_Step5_Check.Visible = False
            picSide_Step3_Check.Visible = False
            picSide_Step6_Check.Visible = False

            pnlStep1.BackColor = SystemColors.GradientInactiveCaption
            pnlStep2.BackColor = Color.WhiteSmoke
            pnlStep4.BackColor = Color.WhiteSmoke
            pnlStep5.BackColor = Color.WhiteSmoke
            pnlStep3.BackColor = Color.WhiteSmoke
            pnlStep6.BackColor = Color.WhiteSmoke
            pnlStep7.BackColor = Color.WhiteSmoke
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objMaster.getComboListPAYYEAR("Year", True)
            dsCombo = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboStep2_PayYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Year")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListForHeadType("TranHeadType")
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("TranHeadType")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            dsCombo = Nothing
        End Try
    End Sub

    Private Sub FillTranHeadList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Dim lvItem As ListViewItem
        Try

            lvStep4_TrnHead.Items.Clear()
            dsList = objTranHead.GetList("TranHead")
            For Each dsRow As DataRow In dsList.Tables("TranHead").Rows
                With dsRow
                    lvItem = New ListViewItem

                    lvItem.Text = ""

                    lvItem.SubItems.Add(.Item("trnheadname").ToString)
                    lvItem.SubItems(colhTrnHeadName.Index).Tag = .Item("tranheadunkid").ToString

                    cboTrnHeadType.SelectedValue = CInt(.Item("trnheadtype_id").ToString)
                    lvItem.SubItems.Add(cboTrnHeadType.Text)
                    lvItem.SubItems(colhTrnHeadType.Index).Tag = .Item("trnheadtype_id").ToString

                    lvStep4_TrnHead.Items.Add(lvItem)
                End With
            Next

            If lvStep4_TrnHead.Items.Count > 20 Then
                colhTrnHeadType.Width = 250 - 18
            Else
                colhTrnHeadType.Width = 250
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillTranHeadList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillPreviousPeriod()
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            lvStep5_PayPeriod.Items.Clear()

            dsList = objBudget_Master.GetList("Period", enModuleReference.Payroll, CInt(menAllocation)) 'Sohail (28 Dec 2010)
            dtTable = New DataView(dsList.Tables("Period"), "payperiodunkid <> " & CInt(cboStep2_PayPeriod.SelectedValue) & "", "", DataViewRowState.CurrentRows).ToTable


            For Each dtRow As DataRow In dtTable.Rows
                With dtRow
                    lvItem = New ListViewItem
                    lvItem.Text = ""

                    lvItem.SubItems.Add(.Item("payyearunkid").ToString)
                    lvItem.SubItems(colh_Period_PayYear.Index).Tag = .Item("payyearunkid").ToString

                    lvItem.SubItems.Add(.Item("period_name").ToString)
                    lvItem.SubItems(colh_Period_PayPeriod.Index).Tag = .Item("payperiodunkid").ToString

                    lvItem.SubItems.Add(eZeeDate.convertDate(.Item("start_date").ToString).ToShortDateString)
                    lvItem.SubItems.Add(eZeeDate.convertDate(.Item("end_date").ToString).ToShortDateString)

                    lvStep5_PayPeriod.Items.Add(lvItem)
                End With
            Next

            If lvStep5_PayPeriod.Items.Count > 20 Then
                colh_Period_PayYear.Width = 150 - 18
            Else
                colh_Period_PayYear.Width = 150
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPreviousPeriod", mstrModuleName)
        Finally
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub

    Private Sub CheckAllTranHead(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvStep4_TrnHead.Items
                lvItem.Checked = blnCheckAll
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllTranHead", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAllPeriod(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvStep5_PayPeriod.Items
                lvItem.Checked = blnCheckAll
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllPeriod", mstrModuleName)
        End Try
    End Sub

    Private Sub GridSetup()
        Dim objTranHead As New clsTransactionHead
        'Dim strAllocation As String = "" 'Sohail (28 Dec 2010)
        Dim strPrevPeriodCol As String = ""
        Dim dCol As DataGridViewTextBoxColumn
        Dim dsAlloc As DataSet
        Dim dsList As New DataSet
        Dim strTranHeadIDList As String = ""

        Dim dtTemp As New DataTable
        Dim dtcol As DataColumn
        Dim intPrevAllocID As Integer = -1
        Dim intBgtID As Integer = -1

        Try
            Cursor.Current = Cursors.WaitCursor 'Sohail (01 Dec 2010)
            'Sohail (28 Dec 2010) -- Start
            'If radGrade.Checked = True Then
            '    strAllocation = "Grade"
            '    menAllocation = enAnalysisRef.Grade
            'ElseIf radAccess.Checked = True Then
            '    strAllocation = "Access"
            '    menAllocation = enAnalysisRef.Access
            'ElseIf radSection.Checked = True Then
            '    strAllocation = "Section"
            '    menAllocation = enAnalysisRef.Section
            'ElseIf radCostCenter.Checked = True Then
            '    strAllocation = "Cost Center"
            '    menAllocation = enAnalysisRef.CostCenter
            'ElseIf radStation.Checked = True Then
            '    strAllocation = "Station"
            '    menAllocation = enAnalysisRef.Station
            'ElseIf radPayPoint.Checked = True Then
            '    strAllocation = "Pay Point"
            '    menAllocation = enAnalysisRef.PayPoint
            'ElseIf radDepInDepGroup.Checked = True Then
            '    strAllocation = "Department in Department Group"
            '    menAllocation = enAnalysisRef.DeptInDeptGroup
            'ElseIf radSectionDept.Checked = True Then
            '    strAllocation = "Section in Department"
            '    menAllocation = enAnalysisRef.SectionInDepartment
            'ElseIf radJobnJobGrp.Checked = True Then
            '    strAllocation = "Job in Job Group"
            '    menAllocation = enAnalysisRef.JobInJobGroup
            'ElseIf radClassnClassGrp.Checked = True Then
            '    strAllocation = "Class in Class Group"
            '    menAllocation = enAnalysisRef.ClassInClassGroup
            'End If
            'Sohail (28 Dec 2010) -- End
            With dgvStep6_Budget
                If Not .DataSource Is Nothing Then .DataSource = Nothing
                .Columns.Clear()
                .Rows.Clear()

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhCollaps"
                dCol.HeaderText = ""
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 30
                dCol.Frozen = True
                dCol.Visible = True
                dCol.ReadOnly = True
                .Columns.Add(dCol)

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhAllocationID"
                dCol.HeaderText = mstrAllocation 'Sohail (28 Dec 2010)
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 100
                dCol.Frozen = True
                dCol.Visible = False
                dCol.ReadOnly = True
                dCol.DataPropertyName = dCol.Name
                .Columns.Add(dCol)
                'Adding DataTable Columns
                dtcol = New DataColumn("colhAllocationID")
                dtcol.DataType = System.Type.GetType("System.Int32")
                dtTemp.Columns.Add(dtcol)

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhAllocation"
                dCol.HeaderText = mstrAllocation 'Sohail (28 Dec 2010)
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 180
                dCol.Frozen = True
                dCol.ReadOnly = True
                dCol.DataPropertyName = dCol.Name
                .Columns.Add(dCol)
                'Adding DataTable Columns
                dtcol = New DataColumn("colhAllocation")
                dtcol.DataType = System.Type.GetType("System.String")
                dtTemp.Columns.Add(dtcol)

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhTranHeadID"
                dCol.HeaderText = Language.getMessage(mstrModuleName, 7, "Transaction Head")
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 0
                dCol.Visible = False
                dCol.Frozen = True
                dCol.ReadOnly = True
                dCol.DataPropertyName = dCol.Name
                .Columns.Add(dCol)
                'Adding DataTable Columns
                dtcol = New DataColumn("colhTranHeadID")
                dtcol.DataType = System.Type.GetType("System.Int32")
                dtTemp.Columns.Add(dtcol)

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhTranHead"
                dCol.HeaderText = Language.getMessage(mstrModuleName, 7, "Transaction Head")
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 150
                dCol.Visible = False
                dCol.Frozen = True
                dCol.ReadOnly = True
                dCol.DataPropertyName = dCol.Name
                .Columns.Add(dCol)
                'Adding DataTable Columns
                dtcol = New DataColumn("colhTranHead")
                dtcol.DataType = System.Type.GetType("System.String")
                dtTemp.Columns.Add(dtcol)

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colhTranHeadType"
                dCol.HeaderText = Language.getMessage(mstrModuleName, 8, "Transaction Head Type")
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 130
                dCol.Frozen = True
                dCol.ReadOnly = True
                dCol.DataPropertyName = dCol.Name
                .Columns.Add(dCol)
                'Adding DataTable Columns
                dtcol = New DataColumn("colhTranHeadType")
                dtcol.DataType = System.Type.GetType("System.String")
                dtTemp.Columns.Add(dtcol)

                For Each lvItem As ListViewItem In lvStep5_PayPeriod.CheckedItems
                    'Adding DataGrid Columns
                    dCol = New DataGridViewTextBoxColumn
                    dCol.Name = "colh" & lvItem.SubItems(colh_Period_PayPeriod.Index).Tag.ToString
                    dCol.HeaderText = lvItem.SubItems(colh_Period_PayPeriod.Index).Text
                    dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dCol.Width = 150 'Sohail (11 May 2011)
                    dCol.Frozen = False
                    dCol.ReadOnly = True
                    dCol.DataPropertyName = dCol.Name
                    dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dCol.DefaultCellStyle.Format = GUI.fmtCurrency 'Sohail (01 Dec 2010)
                    .Columns.Add(dCol)
                    'Adding DataTable Columns
                    dtcol = New DataColumn(dCol.Name)
                    dtcol.DataType = System.Type.GetType("System.Decimal") 'Sohail (11 May 2011)
                    dtTemp.Columns.Add(dtcol)

                    strPrevPeriodCol &= ", " & "0.00 AS " & dCol.Name
                Next

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "colh" & cboStep2_PayPeriod.SelectedValue.ToString
                dCol.HeaderText = cboStep2_PayPeriod.Text
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 200 'Sohail (11 May 2011)
                dCol.Frozen = False
                dCol.ReadOnly = False
                If .ColumnCount < 7 Then 'Sohail (11 May 2011)
                    dCol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    '.Columns("colh" & cboStep2_PayPeriod.SelectedValue.ToString).AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                End If
                dCol.DataPropertyName = dCol.Name
                dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dCol.DefaultCellStyle.BackColor = GUI.ColorComp
                'dCol.DefaultCellStyle.Format = GUI.fmtCurrency 'Sohail (11 May 2011)
                dCol.Resizable = DataGridViewTriState.True 'Sohail (01 Dec 2010)
                dCol.MaxInputLength = 30 'Sohail (11 May 2011)
                .Columns.Add(dCol)
                'Adding DataTable Columns
                dtcol = New DataColumn(dCol.Name)
                dtcol.DataType = System.Type.GetType("System.Decimal") 'Sohail (11 May 2011)
                dtTemp.Columns.Add(dtcol)

                strPrevPeriodCol &= ", " & "0.00 AS  " & dCol.Name

                'Adding DataGrid Columns
                dCol = New DataGridViewTextBoxColumn
                dCol.Name = "objisgroup"
                dCol.HeaderText = "isgroup"
                dCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dCol.Width = 50
                dCol.Frozen = False
                dCol.ReadOnly = True
                dCol.Visible = False
                dCol.DataPropertyName = dCol.Name
                dCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns.Add(dCol)
                'Adding DataTable Columns
                dtcol = New DataColumn("objisgroup")
                dtcol.DataType = System.Type.GetType("System.Boolean")
                dtTemp.Columns.Add(dtcol)

                'Get List
                For Each lvItem As ListViewItem In lvStep4_TrnHead.CheckedItems
                    If strTranHeadIDList.Length = 0 Then
                        strTranHeadIDList = lvItem.SubItems(colhTrnHeadName.Index).Tag.ToString
                    Else
                        strTranHeadIDList &= "," & lvItem.SubItems(colhTrnHeadName.Index).Tag.ToString
                    End If
                Next
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsAlloc = objBudget_Tran.GetDataGridList(menAllocation, strPrevPeriodCol, strTranHeadIDList, "List")
                dsAlloc = objBudget_Tran.GetDataGridList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_Start, mdtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, False, menAllocation, strPrevPeriodCol, strTranHeadIDList, "List", False, "")
                'Sohail (21 Aug 2015) -- End

                'Fill DataTable
                Dim drTemp As DataRow
                For Each dsRow As DataRow In dsAlloc.Tables("List").Rows
                    If CInt(dsRow.Item("colhAllocationID").ToString) <> intPrevAllocID Then
                        drTemp = dtTemp.NewRow()
                        drTemp.Item("colhAllocationID") = CInt(dsRow.Item("colhAllocationID").ToString)
                        drTemp.Item("colhAllocation") = dsRow.Item("colhAllocation").ToString
                        drTemp.Item("colhTranHeadID") = CInt(dsRow.Item("colhTranHeadID").ToString)
                        drTemp.Item("colhTranHead") = "" 'dsRow.Item("colhTranHead").ToString
                        drTemp.Item("colhTranHeadType") = "" 'dsRow.Item("colhTranHeadType").ToString
                        For i = 5 To dsAlloc.Tables("List").Columns.Count - 1
                            drTemp.Item(i) = DBNull.Value ' cdec(dsRow.Item(i).ToString)
                        Next
                        'drTemp.Item("") = dsRow.Item("").ToString
                        drTemp.Item("objisgroup") = True
                        dtTemp.Rows.Add(drTemp)
                        intPrevAllocID = CInt(dsRow.Item("colhAllocationID").ToString)
                    End If
                    drTemp = dtTemp.NewRow()
                    drTemp.Item("colhAllocationID") = CInt(dsRow.Item("colhAllocationID").ToString)
                    drTemp.Item("colhAllocation") = Space(5) & dsRow.Item("colhTranHead").ToString 'dsRow.Item("colhAllocation").ToString
                    drTemp.Item("colhTranHeadID") = CInt(dsRow.Item("colhTranHeadID").ToString)
                    drTemp.Item("colhTranHead") = dsRow.Item("colhTranHead").ToString
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objTranHead._Tranheadunkid = CInt(dsRow.Item("colhTranHeadID").ToString)
                    objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(dsRow.Item("colhTranHeadID").ToString)
                    'Sohail (21 Aug 2015) -- End
                    cboTrnHeadType.SelectedValue = objTranHead._Trnheadtype_Id
                    drTemp.Item("colhTranHeadType") = cboTrnHeadType.Text
                    cboTrnHeadType.SelectedValue = 0
                    For i = 5 To dsAlloc.Tables("List").Columns.Count - 1
                        intBgtID = objBudget_Master.GetBudgetID(CInt(Mid(dsAlloc.Tables("List").Columns(i).Caption, 5)), menAllocation)
                        If intBgtID > 0 Then
                            Dim dsTemp As DataSet = objBudget_Tran.GetList("Budget", intBgtID, CInt(dsRow.Item("colhAllocationID").ToString), CInt(dsRow.Item("colhTranHeadID").ToString))
                            If dsTemp.Tables("Budget").Rows.Count > 0 Then
                                drTemp.Item(i) = CDec(dsTemp.Tables("Budget").Rows(0).Item("amount").ToString) 'Sohail (11 May 2011)
                            Else
                                drTemp.Item(i) = CDec(dsRow.Item(i).ToString) 'Sohail (11 May 2011)
                            End If
                            dsTemp = Nothing
                        Else
                            drTemp.Item(i) = CDec(dsRow.Item(i).ToString) 'Sohail (11 May 2011)
                        End If
                    Next
                    'drTemp.Item("") = dsRow.Item("").ToString
                    drTemp.Item("objisgroup") = False
                    dtTemp.Rows.Add(drTemp)
                Next
                dsList.Tables.Clear()
                dsList.Tables.Add(dtTemp)
                .AutoGenerateColumns = False
                .DataSource = dsList.Tables(0)
                Cursor.Current = Cursors.Default 'Sohail (01 Dec 2010)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GridSetup", mstrModuleName)
        End Try
    End Sub

    Private Sub setColorTemplate()
        Dim intRow As Integer

        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.BackColor = Color.Gray
            dgvcsHeader.Font = New Font(dgvStep6_Budget.Font.FontFamily, 8, FontStyle.Bold)

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.SelectionBackColor = Color.LightGray
            dgvcsChild.BackColor = Color.LightGray
            dgvcsChild.Font = New Font(dgvStep6_Budget.Font.FontFamily, 8, FontStyle.Regular)

            For intRow = 0 To dgvStep6_Budget.RowCount - 1
                If CBool(dgvStep6_Budget.Rows(intRow).Cells("objisgroup").Value) = True Then
                    dgvStep6_Budget.Rows(intRow).DefaultCellStyle = dgvcsHeader
                    dgvStep6_Budget.Rows(intRow).Cells(0).Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    dgvStep6_Budget.Rows(intRow).Cells("colh" & cboStep2_PayPeriod.SelectedValue.ToString).ReadOnly = True
                Else
                    dgvStep6_Budget.Rows(intRow).DefaultCellStyle = dgvcsChild
                    For i = 6 To dgvStep6_Budget.Columns.Count - 2
                        dgvStep6_Budget.Rows(intRow).Cells(i).Style.BackColor = GUI.ColorOptional
                    Next
                    dgvStep6_Budget.Rows(intRow).Cells("colh" & cboStep2_PayPeriod.SelectedValue.ToString).Style.BackColor = GUI.ColorComp
                End If
            Next

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "setColorTemplate", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBudgetPreparation_Wizardt_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBudget_Master = Nothing
            objBudget_Tran = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetPreparation_Wizardt_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmBudgetPreparation_Wizardt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBudget_Master = New clsBudget_Master
        objBudget_Tran = New clsBudget_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetColor()
            Call FillCombo()
            Call FillTranHeadList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBudgetPreparation_Wizardt_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBudget_Master.SetMessages()
            clsBudget_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsBudget_Master, clsBudget_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " ComboBox's Events "
    Private Sub cboStep2_PayYear_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStep2_PayYear.SelectedValueChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet = Nothing
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboStep2_PayYear.SelectedValue), "Period", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(cboStep2_PayYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboStep2_PayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStep2_PayYear_SelectedValueChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private Sub cboStep2_PayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStep2_PayPeriod.SelectedIndexChanged
        Try
            If CInt(cboStep2_PayPeriod.SelectedValue) > 0 Then
                mdtPeriod_Start = eZeeDate.convertDate(CType(cboStep2_PayPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
                mdtPeriod_End = eZeeDate.convertDate(CType(cboStep2_PayPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
            Else
                mdtPeriod_Start = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriod_End = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStep2_PayPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

#End Region

#Region " Button's Events "

#End Region

#Region " Listview's Events "
    Private Sub lvStep4_TrnHead_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvStep4_TrnHead.ItemChecked
        Try
            If lvStep4_TrnHead.CheckedItems.Count = lvStep4_TrnHead.Items.Count Then
                objchkSelectAll.Checked = True
            ElseIf lvStep4_TrnHead.CheckedItems.Count = 0 Then
                objchkSelectAll.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvStep4_TrnHead_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvStep5_PayPeriod_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvStep5_PayPeriod.ItemChecked
        Try
            If lvStep5_PayPeriod.CheckedItems.Count = lvStep5_PayPeriod.Items.Count Then
                objchkSelectAll_Period.Checked = True
            ElseIf lvStep5_PayPeriod.CheckedItems.Count = 0 Then
                objchkSelectAll_Period.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvStep5_PayPeriod_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllTranHead(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkSelectAll_Period_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkSelectAll_Period.CheckedChanged
        Try
            Call CheckAllPeriod(objchkSelectAll_Period.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_Period_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " DataGrid's Events "

    Private Sub dgvStep6_Budget_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStep6_Budget.CellClick
        Try
            If e.RowIndex = -1 Then Exit Sub

            If CBool(dgvStep6_Budget.Rows(e.RowIndex).Cells("objisgroup").Value) = True Then
                If dgvStep6_Budget.Rows(e.RowIndex).Cells(0).Value.ToString = "-" Then
                    dgvStep6_Budget.Rows(e.RowIndex).Cells(0).Value = "+"
                Else
                    dgvStep6_Budget.Rows(e.RowIndex).Cells(0).Value = "-"
                End If
                For i = e.RowIndex + 1 To dgvStep6_Budget.RowCount - 1
                    If CInt(dgvStep6_Budget.Rows(e.RowIndex + 1).Cells("colhAllocationID").Value) = CInt(dgvStep6_Budget.Rows(i).Cells("colhAllocationID").Value) Then
                        If dgvStep6_Budget.Rows(i).Visible = False Then
                            dgvStep6_Budget.Rows(i).Visible = True
                        Else
                            dgvStep6_Budget.Rows(i).Visible = False
                        End If
                    Else
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvStep6_Budget_CellClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvStep6_Budget_CellMouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvStep6_Budget.CellMouseMove
        Try
            If e.RowIndex = -1 Then Exit Sub

            If CBool(dgvStep6_Budget.Rows(e.RowIndex).Cells("objisgroup").Value) = True Then
                dgvStep6_Budget.Cursor = Cursors.Hand
            Else
                dgvStep6_Budget.Cursor = Cursors.Default
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvStep6_Budget_CellMouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvStep6_Budget_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvStep6_Budget.DataBindingComplete
        Try

            For i = 0 To dgvStep6_Budget.RowCount - 1
                If CBool(dgvStep6_Budget.Rows(i).Cells("objisgroup").Value) = True Then
                    dgvStep6_Budget.Rows(i).Cells(0).Value = "-"
                Else
                    dgvStep6_Budget.Rows(i).Cells(0).Value = ""
                End If
            Next

            Call setColorTemplate()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvStep6_Budget_DataBindingComplete", mstrModuleName)
        End Try

    End Sub

    Private Sub dgvStep6_Budget_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvStep6_Budget.DataError
        Try
            If e.ColumnIndex = dgvStep6_Budget.Columns("colh" & cboStep2_PayPeriod.SelectedValue.ToString).Index Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please Enter correct Amount."), enMsgBoxStyle.Information)
                e.Cancel = True
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvStep6_Budget_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvStep6_Budget_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvStep6_Budget.EditingControlShowing
        Dim tb As TextBox
        Try
            If TypeOf e.Control Is TextBox AndAlso dgvStep6_Budget.CurrentCell.ColumnIndex = dgvStep6_Budget.Columns("colh" & cboStep2_PayPeriod.SelectedValue.ToString).Index Then 'Sohail (11 May 2011)
                tb = CType(e.Control, TextBox)
                AddHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvStep6_Budget_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    'Sohail (11 May 2011) -- Start
    'Private Sub tb_keypress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
    '    Dim isBackSpace As Boolean = False
    '    Try
    '        Dim isKey As Boolean = [Char].IsDigit(e.KeyChar)

    '        If Asc(e.KeyChar) = Keys.Back Then
    '            isBackSpace = True
    '        End If

    '        If Not isKey AndAlso Not isBackSpace AndAlso Not Asc(e.KeyChar) = Keys.Delete Then
    '            e.Handled = True
    '        End If

    '        Dim t As TextBox
    '        t = CType(sender, TextBox)
    '        If t.Text.Trim.Length = 0 Then Exit Sub
    '        If InStr(t.Text.Trim, ".") > 0 AndAlso Asc(e.KeyChar) = Keys.Delete Then
    '            e.Handled = True
    '            Exit Sub
    '        End If

    '        Dim d As Decimal = Convert.ToDecimal(t.Text)
    '        If d.ToString.Length > 20 AndAlso InStr(t.Text.Trim, ".") = 0 Then
    '            If d.ToString.Length = 21 AndAlso e.KeyChar <> "." AndAlso isBackSpace = False AndAlso t.SelectionLength <> t.Text.Length Then
    '                e.Handled = True
    '                Exit Sub
    '            End If
    '        ElseIf InStr(t.Text.Trim, ".") > 0 AndAlso t.Text.Substring(InStr(t.Text.Trim, ".") - 1).Trim.Length > 6 AndAlso isBackSpace = False AndAlso t.SelectionLength <> t.Text.Length Then
    '            e.Handled = True
    '            Exit Sub
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "tb_keypress", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (11 May 2011) -- End
#End Region

    'Sohail (28 Dec 2010) -- Start
#Region " Radio Buttons Events "
    Private Sub radCostCenter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radCostCenter.CheckedChanged, radStation.CheckedChanged
        Try
            If radGrade.Checked = True Then
                mstrAllocation = Language.getMessage(mstrModuleName, 9, "Grade")
                menAllocation = enAnalysisRef.Grade
            ElseIf radAccess.Checked = True Then
                mstrAllocation = Language.getMessage(mstrModuleName, 10, "Access")
                menAllocation = enAnalysisRef.Access
            ElseIf radSection.Checked = True Then
                mstrAllocation = Language.getMessage(mstrModuleName, 11, "Section")
                menAllocation = enAnalysisRef.Section
            ElseIf radCostCenter.Checked = True Then
                mstrAllocation = Language.getMessage(mstrModuleName, 12, "Cost Center")
                menAllocation = enAnalysisRef.CostCenter
            ElseIf radStation.Checked = True Then
                mstrAllocation = Language.getMessage(mstrModuleName, 13, "Station")
                menAllocation = enAnalysisRef.Station
            ElseIf radPayPoint.Checked = True Then
                mstrAllocation = Language.getMessage(mstrModuleName, 14, "Pay Point")
                menAllocation = enAnalysisRef.PayPoint
            ElseIf radDepInDepGroup.Checked = True Then
                mstrAllocation = Language.getMessage(mstrModuleName, 15, "Department in Department Group")
                menAllocation = enAnalysisRef.DeptInDeptGroup
            ElseIf radSectionDept.Checked = True Then
                mstrAllocation = Language.getMessage(mstrModuleName, 16, "Section in Department")
                menAllocation = enAnalysisRef.SectionInDepartment
            ElseIf radJobnJobGrp.Checked = True Then
                mstrAllocation = Language.getMessage(mstrModuleName, 17, "Job in Job Group")
                menAllocation = enAnalysisRef.JobInJobGroup
            ElseIf radClassnClassGrp.Checked = True Then
                mstrAllocation = Language.getMessage(mstrModuleName, 18, "Class in Class Group")
                menAllocation = enAnalysisRef.ClassInClassGroup
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radCostCenter_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (28 Dec 2010) -- End

#Region " Other Control's Events "
    Private Sub wizBudget_BeforeSwitchPages(ByVal sender As System.Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles wizBudget.BeforeSwitchPages
        Dim blnFlag As Boolean = False
        Dim blnValid As Boolean = False
        Try
            pnlStep1.BackColor = Color.WhiteSmoke
            pnlStep2.BackColor = Color.WhiteSmoke
            pnlStep4.BackColor = Color.WhiteSmoke
            pnlStep5.BackColor = Color.WhiteSmoke
            pnlStep3.BackColor = Color.WhiteSmoke
            pnlStep6.BackColor = Color.WhiteSmoke
            pnlStep7.BackColor = Color.WhiteSmoke

            Select Case e.OldIndex
                Case wizBudget.Pages.IndexOf(wizpWelcome)
                    If e.NewIndex > e.OldIndex Then
                        lblStep2.ForeColor = SystemColors.WindowText
                        pnlStep2.BackColor = SystemColors.GradientInactiveCaption

                        picSide_Step1_Check.Visible = True
                    End If
                Case wizBudget.Pages.IndexOf(wizpSelectPeriod)
                    If e.NewIndex > e.OldIndex Then
                        If CInt(cboStep2_PayYear.SelectedValue) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Pay Year. Pay Year is mandatory information."), enMsgBoxStyle.Information)
                            e.Cancel = True
                            If cboStep2_PayYear.Visible Then cboStep2_PayYear.Focus()
                            Exit Sub
                        ElseIf CInt(cboStep2_PayPeriod.SelectedValue) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Pay Period. Pay Period is mandatory information."), enMsgBoxStyle.Information)
                            e.Cancel = True
                            If cboStep2_PayPeriod.Visible Then cboStep2_PayPeriod.Focus()
                            Exit Sub
                        End If

                        lblStep3.ForeColor = SystemColors.WindowText
                        pnlStep3.BackColor = SystemColors.GradientInactiveCaption

                        picSide_Step2_Check.Visible = True
                    Else
                        lblStep2.ForeColor = SystemColors.ControlDark
                        pnlStep1.BackColor = SystemColors.GradientInactiveCaption

                        picSide_Step1_Check.Visible = False
                    End If
                Case wizBudget.Pages.IndexOf(wizpAllocationBy)
                    If e.NewIndex > e.OldIndex Then
                        lblStep4.ForeColor = SystemColors.WindowText
                        pnlStep4.BackColor = SystemColors.GradientInactiveCaption

                        picSide_Step3_Check.Visible = True
                    Else
                        lblStep3.ForeColor = SystemColors.ControlDark
                        pnlStep2.BackColor = SystemColors.GradientInactiveCaption

                        picSide_Step2_Check.Visible = False
                    End If
                Case wizBudget.Pages.IndexOf(wizpSelectTrnHead)
                    If e.NewIndex > e.OldIndex Then
                        If lvStep4_TrnHead.CheckedItems.Count = 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select atleast one Transaction Head."), enMsgBoxStyle.Information)
                            e.Cancel = True
                            If lvStep4_TrnHead.Visible Then lvStep4_TrnHead.Select()
                            Exit Sub
                        End If

                        Call FillPreviousPeriod()

                        lblStep5.ForeColor = SystemColors.WindowText
                        pnlStep5.BackColor = SystemColors.GradientInactiveCaption

                        picSide_Step4_Check.Visible = True
                    Else
                        lblStep4.ForeColor = SystemColors.ControlDark
                        pnlStep3.BackColor = SystemColors.GradientInactiveCaption

                        picSide_Step3_Check.Visible = False
                    End If
                Case wizBudget.Pages.IndexOf(wizpSelectPreviusPeriod)
                    If e.NewIndex > e.OldIndex Then
                        Call GridSetup()

                        lblStep6.ForeColor = SystemColors.WindowText
                        pnlStep6.BackColor = SystemColors.GradientInactiveCaption

                        picSide_Step5_Check.Visible = True
                    Else
                        lblStep5.ForeColor = SystemColors.ControlDark
                        pnlStep4.BackColor = SystemColors.GradientInactiveCaption

                        picSide_Step4_Check.Visible = False
                    End If
                Case wizBudget.Pages.IndexOf(wizpPrepareBudget)
                    If e.NewIndex > e.OldIndex Then
                        For i = 0 To dgvStep6_Budget.Rows.Count - 1
                            If Not IsDBNull(dgvStep6_Budget.Rows(i).Cells("colh" & cboStep2_PayPeriod.SelectedValue.ToString).Value) = True Then
                                If CDec(dgvStep6_Budget.Rows(i).Cells("colh" & cboStep2_PayPeriod.SelectedValue.ToString).Value) > 0 Then 'Sohail (11 May 2011)
                                    blnValid = True
                                    Exit For
                                End If
                            End If
                        Next
                        If blnValid = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please enter amount to atleast one Transaction Head for the " & cboStep2_PayPeriod.Text & " Period."))
                            e.Cancel = True
                            Exit Sub
                        End If
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure you want to Save this Budget Alloaction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            e.Cancel = True
                            Exit Sub
                        End If

                        Cursor.Current = Cursors.WaitCursor 'Sohail (01 Dec 2010)
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'blnFlag = objBudget_Master.InsertData(CInt(cboStep2_PayYear.SelectedValue), CInt(cboStep2_PayPeriod.SelectedValue), menAllocation, CType(dgvStep6_Budget.DataSource, DataTable))
                        blnFlag = objBudget_Master.InsertData(CInt(cboStep2_PayYear.SelectedValue), CInt(cboStep2_PayPeriod.SelectedValue), menAllocation, CType(dgvStep6_Budget.DataSource, DataTable), User._Object._Userunkid)
                        'Sohail (21 Aug 2015) -- End
                        Cursor.Current = Cursors.Default 'Sohail (01 Dec 2010)

                        If blnFlag = True Then
                            lblStep7.ForeColor = SystemColors.WindowText
                            pnlStep7.BackColor = SystemColors.GradientInactiveCaption

                            picSide_Step6_Check.Visible = True
                        End If
                    Else
                        lblStep6.ForeColor = SystemColors.ControlDark
                        pnlStep5.BackColor = SystemColors.GradientInactiveCaption

                        picSide_Step5_Check.Visible = False
                    End If

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "wizBudget_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Message List "
    '1, "Please select Pay Year. Pay Year is mandatory information."
    '2, "Please select Pay Period. Pay Period is mandatory information."
    '3, "Please select atleast one Transaction Head."
    '4, "Please enter amount to atleast one Transaction Head for the " & cboStep2_PayPeriod.Text & " Period."
    '5, "Are you sure you want to Save this Budget Alloaction?"
    '6, "Please Enter Proper Amount."
#End Region

    'Private Sub dgvStep6_Budget_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles dgvStep6_Budget.Paint
    '    Dim rowStart As Integer = 0
    '    Dim rowEnd As Integer = 0
    '    Dim fnt As New Font("Arial", 10, FontStyle.Bold, GraphicsUnit.Point)
    '    For Each dgRow As DataGridViewRow In dgvStep6_Budget.Rows
    '        If dgRow.Index = 0 Then
    '            rowStart = 0

    '        ElseIf dgRow.Cells(1).Value.ToString <> dgvStep6_Budget.Rows(dgRow.Index - 1).Cells(1).Value.ToString Then
    '            rowEnd = dgRow.Index

    '            Dim rct1 As New Rectangle((dgvStep6_Budget.GetColumnDisplayRectangle(1, True).X), _
    '                                  (dgvStep6_Budget.GetRowDisplayRectangle(rowStart, True).Y + dgvStep6_Budget.Columns(1).HeaderCell.ContentBounds.Height + 8), _
    '                                  dgvStep6_Budget.GetColumnDisplayRectangle(1, True).Width - 1, _
    '                                  (dgvStep6_Budget.GetRowDisplayRectangle(rowEnd, True).Top - dgvStep6_Budget.GetRowDisplayRectangle(rowEnd, True).Height - dgvStep6_Budget.GetRowDisplayRectangle(rowStart, True).Top))
    '            e.Graphics.FillRectangle(Brushes.White, rct1)

    '            rowStart = dgRow.Index
    '        ElseIf dgRow.Index = dgvStep6_Budget.Rows.Count - 1 Then
    '            rowEnd = dgRow.Index


    '            Dim rct1 As New Rectangle((dgvStep6_Budget.GetColumnDisplayRectangle(1, True).X), _
    '                                  (dgvStep6_Budget.GetRowDisplayRectangle(rowStart, True).Y + dgvStep6_Budget.Columns(1).HeaderCell.ContentBounds.Height + 8), _
    '                                  dgvStep6_Budget.GetColumnDisplayRectangle(1, True).Width - 1, _
    '                                  (dgvStep6_Budget.GetRowDisplayRectangle(rowEnd, True).Top - dgvStep6_Budget.GetRowDisplayRectangle(rowStart, True).Top))
    '            e.Graphics.FillRectangle(Brushes.White, rct1)
    '        End If
    '    Next
    'End Sub

    'Private Sub dgvStep6_Budget_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvStep6_Budget.CellFormatting
    '    Dim objTranHead As New clsTransactionHead
    '    Try
    '        If e.ColumnIndex = 4 Then 'Transaction Head Type
    '            objTranHead._Tranheadunkid = CInt(dgvStep6_Budget.Rows(e.RowIndex).Cells(2).Value) '2=tranheadunkid
    '            cboTrnHeadType.SelectedValue = objTranHead._Trnheadtype_Id
    '            e.Value = cboTrnHeadType.Text
    '            cboTrnHeadType.SelectedValue = 0
    '        End If
    '        With dgvStep6_Budget
    '            If e.RowIndex > 0 Then
    '                If e.ColumnIndex = 1 Then 'Allocation Name
    '                    If CInt(.Rows(e.RowIndex).Cells(0).Value) = CInt(.Rows(e.RowIndex - 1).Cells(0).Value) Then
    '                        e.Value = ""
    '                        'e.CellStyle.BackColor = Color.White
    '                        'Dim mystyle As New DataGridViewAdvancedBorderStyle
    '                        'mystyle.All = DataGridViewAdvancedCellBorderStyle.None
    '                        'Dim myplaceholder As New DataGridViewAdvancedBorderStyle
    '                        '.Rows(e.RowIndex).Cells(e.ColumnIndex).AdjustCellBorderStyle(mystyle, myplaceholder, False, False, False, False)
    '                    Else
    '                        'e.CellStyle.BackColor = Color.Aqua


    '                        'Dim mystyle As New DataGridViewAdvancedBorderStyle
    '                        'mystyle.All = DataGridViewAdvancedCellBorderStyle.None
    '                        'Dim myplaceholder As New DataGridViewAdvancedBorderStyle
    '                        '.Rows(e.RowIndex).Cells(e.ColumnIndex).AdjustCellBorderStyle(mystyle, myplaceholder, False, False, False, False)

    '                    End If
    '                End If
    '            Else
    '                If e.ColumnIndex = 1 Then 'Allocation Name
    '                    'e.CellStyle.BackColor = Color.Aqua
    '                End If
    '            End If
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvStep6_Budget_CellFormatting", mstrModuleName)
    '    Finally
    '        objTranHead = Nothing
    '    End Try
    'End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbStep1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep1.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbBudgetSteps.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBudgetSteps.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep2.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep2.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep4.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep4.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep5.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep5.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep6.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep6.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep7.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep7.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep3.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep3.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbStep3_AllocationBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbStep3_AllocationBy.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.EZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.wizBudget.CancelText = Language._Object.getCaption(Me.wizBudget.Name & "_CancelText", Me.wizBudget.CancelText)
            Me.wizBudget.NextText = Language._Object.getCaption(Me.wizBudget.Name & "_NextText", Me.wizBudget.NextText)
            Me.wizBudget.BackText = Language._Object.getCaption(Me.wizBudget.Name & "_BackText", Me.wizBudget.BackText)
            Me.wizBudget.FinishText = Language._Object.getCaption(Me.wizBudget.Name & "_FinishText", Me.wizBudget.FinishText)
            Me.EZeeHeader.Title = Language._Object.getCaption(Me.EZeeHeader.Name & "_Title", Me.EZeeHeader.Title)
            Me.EZeeHeader.Message = Language._Object.getCaption(Me.EZeeHeader.Name & "_Message", Me.EZeeHeader.Message)
            Me.gbStep1.Text = Language._Object.getCaption(Me.gbStep1.Name, Me.gbStep1.Text)
            Me.gbBudgetSteps.Text = Language._Object.getCaption(Me.gbBudgetSteps.Name, Me.gbBudgetSteps.Text)
            Me.gbStep2.Text = Language._Object.getCaption(Me.gbStep2.Name, Me.gbStep2.Text)
            Me.lblStep2_PayPeriod.Text = Language._Object.getCaption(Me.lblStep2_PayPeriod.Name, Me.lblStep2_PayPeriod.Text)
            Me.lblStep2_PayYear.Text = Language._Object.getCaption(Me.lblStep2_PayYear.Name, Me.lblStep2_PayYear.Text)
            Me.lblStep2_Desc.Text = Language._Object.getCaption(Me.lblStep2_Desc.Name, Me.lblStep2_Desc.Text)
            Me.lbl_Step1_Welcome.Text = Language._Object.getCaption(Me.lbl_Step1_Welcome.Name, Me.lbl_Step1_Welcome.Text)
            Me.gbStep4.Text = Language._Object.getCaption(Me.gbStep4.Name, Me.gbStep4.Text)
            Me.lblStep4_Desc.Text = Language._Object.getCaption(Me.lblStep4_Desc.Name, Me.lblStep4_Desc.Text)
            Me.gbStep5.Text = Language._Object.getCaption(Me.gbStep5.Name, Me.gbStep5.Text)
            Me.lblStep5_Desc.Text = Language._Object.getCaption(Me.lblStep5_Desc.Name, Me.lblStep5_Desc.Text)
            Me.gbStep6.Text = Language._Object.getCaption(Me.gbStep6.Name, Me.gbStep6.Text)
            Me.lblStep6_Desc.Text = Language._Object.getCaption(Me.lblStep6_Desc.Name, Me.lblStep6_Desc.Text)
            Me.gbStep7.Text = Language._Object.getCaption(Me.gbStep7.Name, Me.gbStep7.Text)
            Me.lblStep7_Finish.Text = Language._Object.getCaption(Me.lblStep7_Finish.Name, Me.lblStep7_Finish.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.lblStep1.Text = Language._Object.getCaption(Me.lblStep1.Name, Me.lblStep1.Text)
            Me.lblStep2.Text = Language._Object.getCaption(Me.lblStep2.Name, Me.lblStep2.Text)
            Me.lblStep3.Text = Language._Object.getCaption(Me.lblStep3.Name, Me.lblStep3.Text)
            Me.lblStep5.Text = Language._Object.getCaption(Me.lblStep5.Name, Me.lblStep5.Text)
            Me.lblStep4.Text = Language._Object.getCaption(Me.lblStep4.Name, Me.lblStep4.Text)
            Me.lblStep7.Text = Language._Object.getCaption(Me.lblStep7.Name, Me.lblStep7.Text)
            Me.gbStep3.Text = Language._Object.getCaption(Me.gbStep3.Name, Me.gbStep3.Text)
            Me.lblStep3_Desc.Text = Language._Object.getCaption(Me.lblStep3_Desc.Name, Me.lblStep3_Desc.Text)
            Me.gbStep3_AllocationBy.Text = Language._Object.getCaption(Me.gbStep3_AllocationBy.Name, Me.gbStep3_AllocationBy.Text)
            Me.radPayPoint.Text = Language._Object.getCaption(Me.radPayPoint.Name, Me.radPayPoint.Text)
            Me.radCostCenter.Text = Language._Object.getCaption(Me.radCostCenter.Name, Me.radCostCenter.Text)
            Me.radSection.Text = Language._Object.getCaption(Me.radSection.Name, Me.radSection.Text)
            Me.radClassnClassGrp.Text = Language._Object.getCaption(Me.radClassnClassGrp.Name, Me.radClassnClassGrp.Text)
            Me.radAccess.Text = Language._Object.getCaption(Me.radAccess.Name, Me.radAccess.Text)
            Me.radGrade.Text = Language._Object.getCaption(Me.radGrade.Name, Me.radGrade.Text)
            Me.radJobnJobGrp.Text = Language._Object.getCaption(Me.radJobnJobGrp.Name, Me.radJobnJobGrp.Text)
            Me.radDepInDepGroup.Text = Language._Object.getCaption(Me.radDepInDepGroup.Name, Me.radDepInDepGroup.Text)
            Me.radSectionDept.Text = Language._Object.getCaption(Me.radSectionDept.Name, Me.radSectionDept.Text)
            Me.lblStep6.Text = Language._Object.getCaption(Me.lblStep6.Name, Me.lblStep6.Text)
            Me.radStation.Text = Language._Object.getCaption(Me.radStation.Name, Me.radStation.Text)
            Me.colhTrnHeadName.Text = Language._Object.getCaption(CStr(Me.colhTrnHeadName.Tag), Me.colhTrnHeadName.Text)
            Me.colhTrnHeadType.Text = Language._Object.getCaption(CStr(Me.colhTrnHeadType.Tag), Me.colhTrnHeadType.Text)
            Me.colh_Period_PayYear.Text = Language._Object.getCaption(CStr(Me.colh_Period_PayYear.Tag), Me.colh_Period_PayYear.Text)
            Me.colh_Period_PayPeriod.Text = Language._Object.getCaption(CStr(Me.colh_Period_PayPeriod.Tag), Me.colh_Period_PayPeriod.Text)
            Me.colh_Period_StartDate.Text = Language._Object.getCaption(CStr(Me.colh_Period_StartDate.Tag), Me.colh_Period_StartDate.Text)
            Me.colh_Period_EndDate.Text = Language._Object.getCaption(CStr(Me.colh_Period_EndDate.Tag), Me.colh_Period_EndDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Pay Year. Pay Year is mandatory information.")
            Language.setMessage(mstrModuleName, 2, "Please select Pay Period. Pay Period is mandatory information.")
            Language.setMessage(mstrModuleName, 3, "Please select atleast one Transaction Head.")
            Language.setMessage(mstrModuleName, 4, "Please enter amount to atleast one Transaction Head for the " & cboStep2_PayPeriod.Text & " Period.")
            Language.setMessage(mstrModuleName, 5, "Are you sure you want to Save this Budget Alloaction?")
            Language.setMessage(mstrModuleName, 6, "Please Enter correct Amount.")
            Language.setMessage(mstrModuleName, 7, "Transaction Head")
            Language.setMessage(mstrModuleName, 8, "Transaction Head Type")
            Language.setMessage(mstrModuleName, 9, "Grade")
            Language.setMessage(mstrModuleName, 10, "Access")
            Language.setMessage(mstrModuleName, 11, "Section")
            Language.setMessage(mstrModuleName, 12, "Cost Center")
            Language.setMessage(mstrModuleName, 13, "Station")
            Language.setMessage(mstrModuleName, 14, "Pay Point")
            Language.setMessage(mstrModuleName, 15, "Department in Department Group")
            Language.setMessage(mstrModuleName, 16, "Section in Department")
            Language.setMessage(mstrModuleName, 17, "Job in Job Group")
            Language.setMessage(mstrModuleName, 18, "Class in Class Group")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
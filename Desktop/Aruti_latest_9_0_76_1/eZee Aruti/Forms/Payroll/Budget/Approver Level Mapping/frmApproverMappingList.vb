﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmApproverMappingList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmApproverMappingList"
    Private objApprover As clsbudget_approver_mapping

#End Region

#Region " Private Methods "

    Private Sub Fill_Combo()
        Dim objUsr As New clsUserAddEdit
        Dim objLevel As New clsbudgetapproverlevel_master
        Dim dsList As New DataSet
        Dim objOption As New clsPassowdOptions
        Try
            dsList = objLevel.getListForCombo("List", True, True)
            With cboLevel
                .ValueMember = "levelunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            Dim intPrivilegeId As Integer = 1034 'Allow To Map Level To Approver
            dsList = objUsr.getNewComboList("List", , True, Company._Object._Companyunkid, intPrivilegeId.ToString(), FinancialYear._Object._YearUnkid, False)

            With cboApproverUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsList.Dispose() : objUsr = Nothing : objLevel = Nothing : objOption = Nothing
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim StrSearch As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            If User._Object.Privilege._AllowToViewBudgetApproverLevelMapping = False Then Exit Sub

            dsList = objApprover.GetList("List", , "")

            If CInt(cboLevel.SelectedValue) > 0 Then
                StrSearch &= "AND levelunkid = " & CInt(cboLevel.SelectedValue)
            End If

            If CInt(cboApproverUser.SelectedValue) > 0 Then
                StrSearch &= "AND userapproverunkid = " & CInt(cboApproverUser.SelectedValue)
            End If

            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
                dtTable = New DataView(dsList.Tables("List"), StrSearch, "levelname", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables("List"), "", "levelname", DataViewRowState.CurrentRows).ToTable
            End If

            lvMappingList.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = dtRow.Item("levelname").ToString
                lvItem.SubItems.Add(dtRow.Item("approver").ToString)
                lvItem.SubItems(colhPaymentApprover.Index).Tag = CInt(dtRow.Item("userapproverunkid"))
                lvItem.Tag = dtRow.Item("budgetapproverunkid")
                lvMappingList.Items.Add(lvItem)
            Next

            lvMappingList.GridLines = False
            lvMappingList.GroupingColumn = objcolhLevel
            lvMappingList.DisplayGroups(True)

            If lvMappingList.Items.Count > 3 Then
                colhPaymentApprover.Width = 590 - 18
            Else
                colhPaymentApprover.Width = 590
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToMapLevelToApprover
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteBudgetApproverLevelMapping
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub Common_Search(ByVal cbo As ComboBox)
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cbo.DisplayMember
                .ValueMember = cbo.ValueMember
                .CodeMember = ""
                .DataSource = CType(cbo.DataSource, DataTable)
            End With
            If frm.DisplayDialog = True Then
                cbo.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Common_Search", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmApproverMappingList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objApprover = New clsbudget_approver_mapping
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            Call Fill_Combo()

            If lvMappingList.Items.Count > 0 Then lvMappingList.Items(0).Selected = True
            lvMappingList.Select()
            lvMappingList.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproverMappingList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproverMappingList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproverMappingList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproverMappingList_Closed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Close()
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsbudget_approver_mapping.SetMessages()
            objfrm._Other_ModuleNames = "clsbudget_approver_mapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim frm As New frmApproverMapping
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvMappingList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvMappingList.Select()
            Exit Sub
        End If
        Dim frm As New frmApproverMapping
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvMappingList.SelectedItems(0).Index
            If frm.displayDialog(CInt(lvMappingList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If
            frm = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvMappingList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvMappingList.Select()
            Exit Sub
        End If
        'If objApprover.isUsed(CInt(lvMappingList.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver. Reason: This Approver is in use."), enMsgBoxStyle.Information) '?2
        '    lvMappingList.Select()
        '    Exit Sub
        'End If
        Dim objMaster As New clsMasterData
        Try
           
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvMappingList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objApprover._Budgetapproverunkid = CInt(lvMappingList.SelectedItems(0).Tag)

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If
                objApprover._Isvoid = True
                objApprover._Voiduserunkid = User._Object._Userunkid
                objApprover._Voidreason = mstrVoidReason

                objApprover.Delete(CInt(lvMappingList.SelectedItems(0).Tag))
                lvMappingList.SelectedItems(0).Remove()

                If lvMappingList.Items.Count <= 0 Then
                    Exit Try
                End If
            End If
            lvMappingList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            objMaster = Nothing
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            lvMappingList.Items.Clear()
            cboApproverUser.SelectedValue = 0
            cboLevel.SelectedValue = 0
            lvMappingList.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click, objbtnSearchLevel.Click
        Try
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case "OBJBTNSEARCHLEVEL"
                    Call Common_Search(cboLevel)
                Case "OBJBTNSEARCHUSER"
                    Call Common_Search(cboApproverUser)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        Finally

        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblApprUser.Text = Language._Object.getCaption(Me.lblApprUser.Name, Me.lblApprUser.Text)
			Me.lblApprLevel.Text = Language._Object.getCaption(Me.lblApprLevel.Name, Me.lblApprLevel.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhPaymentApprover.Text = Language._Object.getCaption(CStr(Me.colhPaymentApprover.Tag), Me.colhPaymentApprover.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFundAdjustment_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFundAdjustment_AddEdit))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbProjectCodeAdjustments = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchFundName = New eZee.Common.eZeeGradientButton
        Me.txtNewBalance = New eZee.TextBox.NumericTextBox
        Me.lblNewBalance = New System.Windows.Forms.Label
        Me.txtCurrentBal = New eZee.TextBox.NumericTextBox
        Me.lblCurrentBal = New System.Windows.Forms.Label
        Me.cboFundProjectName = New System.Windows.Forms.ComboBox
        Me.txtIncrDecrAmount = New eZee.TextBox.NumericTextBox
        Me.dtTransactionDate = New System.Windows.Forms.DateTimePicker
        Me.lblTransactionDate = New System.Windows.Forms.Label
        Me.lblFundProjectName = New System.Windows.Forms.Label
        Me.lblIncDecAmount = New System.Windows.Forms.Label
        Me.objFooter.SuspendLayout()
        Me.gbProjectCodeAdjustments.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 248)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(441, 50)
        Me.objFooter.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(336, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(236, 9)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbProjectCodeAdjustments
        '
        Me.gbProjectCodeAdjustments.BorderColor = System.Drawing.Color.Black
        Me.gbProjectCodeAdjustments.Checked = False
        Me.gbProjectCodeAdjustments.CollapseAllExceptThis = False
        Me.gbProjectCodeAdjustments.CollapsedHoverImage = Nothing
        Me.gbProjectCodeAdjustments.CollapsedNormalImage = Nothing
        Me.gbProjectCodeAdjustments.CollapsedPressedImage = Nothing
        Me.gbProjectCodeAdjustments.CollapseOnLoad = False
        Me.gbProjectCodeAdjustments.Controls.Add(Me.lblRemark)
        Me.gbProjectCodeAdjustments.Controls.Add(Me.txtRemark)
        Me.gbProjectCodeAdjustments.Controls.Add(Me.objbtnSearchFundName)
        Me.gbProjectCodeAdjustments.Controls.Add(Me.txtNewBalance)
        Me.gbProjectCodeAdjustments.Controls.Add(Me.lblNewBalance)
        Me.gbProjectCodeAdjustments.Controls.Add(Me.txtCurrentBal)
        Me.gbProjectCodeAdjustments.Controls.Add(Me.lblCurrentBal)
        Me.gbProjectCodeAdjustments.Controls.Add(Me.cboFundProjectName)
        Me.gbProjectCodeAdjustments.Controls.Add(Me.txtIncrDecrAmount)
        Me.gbProjectCodeAdjustments.Controls.Add(Me.dtTransactionDate)
        Me.gbProjectCodeAdjustments.Controls.Add(Me.lblTransactionDate)
        Me.gbProjectCodeAdjustments.Controls.Add(Me.lblFundProjectName)
        Me.gbProjectCodeAdjustments.Controls.Add(Me.lblIncDecAmount)
        Me.gbProjectCodeAdjustments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbProjectCodeAdjustments.ExpandedHoverImage = Nothing
        Me.gbProjectCodeAdjustments.ExpandedNormalImage = Nothing
        Me.gbProjectCodeAdjustments.ExpandedPressedImage = Nothing
        Me.gbProjectCodeAdjustments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbProjectCodeAdjustments.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbProjectCodeAdjustments.HeaderHeight = 25
        Me.gbProjectCodeAdjustments.HeaderMessage = ""
        Me.gbProjectCodeAdjustments.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbProjectCodeAdjustments.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbProjectCodeAdjustments.HeightOnCollapse = 0
        Me.gbProjectCodeAdjustments.LeftTextSpace = 0
        Me.gbProjectCodeAdjustments.Location = New System.Drawing.Point(0, 0)
        Me.gbProjectCodeAdjustments.Name = "gbProjectCodeAdjustments"
        Me.gbProjectCodeAdjustments.OpenHeight = 300
        Me.gbProjectCodeAdjustments.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbProjectCodeAdjustments.ShowBorder = True
        Me.gbProjectCodeAdjustments.ShowCheckBox = False
        Me.gbProjectCodeAdjustments.ShowCollapseButton = False
        Me.gbProjectCodeAdjustments.ShowDefaultBorderColor = True
        Me.gbProjectCodeAdjustments.ShowDownButton = False
        Me.gbProjectCodeAdjustments.ShowHeader = True
        Me.gbProjectCodeAdjustments.Size = New System.Drawing.Size(441, 248)
        Me.gbProjectCodeAdjustments.TabIndex = 0
        Me.gbProjectCodeAdjustments.Temp = 0
        Me.gbProjectCodeAdjustments.Text = "Project Code Adjustments Information"
        Me.gbProjectCodeAdjustments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(12, 145)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(165, 16)
        Me.lblRemark.TabIndex = 92
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(182, 143)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(224, 69)
        Me.txtRemark.TabIndex = 4
        '
        'objbtnSearchFundName
        '
        Me.objbtnSearchFundName.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFundName.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFundName.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFundName.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFundName.BorderSelected = False
        Me.objbtnSearchFundName.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFundName.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFundName.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFundName.Location = New System.Drawing.Point(413, 35)
        Me.objbtnSearchFundName.Name = "objbtnSearchFundName"
        Me.objbtnSearchFundName.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFundName.TabIndex = 89
        '
        'txtNewBalance
        '
        Me.txtNewBalance.AllowNegative = True
        Me.txtNewBalance.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNewBalance.DigitsInGroup = 0
        Me.txtNewBalance.Flags = 0
        Me.txtNewBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewBalance.Location = New System.Drawing.Point(181, 218)
        Me.txtNewBalance.MaxDecimalPlaces = 6
        Me.txtNewBalance.MaxWholeDigits = 21
        Me.txtNewBalance.Name = "txtNewBalance"
        Me.txtNewBalance.Prefix = ""
        Me.txtNewBalance.RangeMax = 1.7976931348623157E+308
        Me.txtNewBalance.RangeMin = -1.7976931348623157E+308
        Me.txtNewBalance.ReadOnly = True
        Me.txtNewBalance.Size = New System.Drawing.Size(225, 21)
        Me.txtNewBalance.TabIndex = 5
        Me.txtNewBalance.Text = "0"
        Me.txtNewBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNewBalance
        '
        Me.lblNewBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewBalance.Location = New System.Drawing.Point(11, 220)
        Me.lblNewBalance.Name = "lblNewBalance"
        Me.lblNewBalance.Size = New System.Drawing.Size(165, 16)
        Me.lblNewBalance.TabIndex = 79
        Me.lblNewBalance.Text = "New Balance"
        Me.lblNewBalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCurrentBal
        '
        Me.txtCurrentBal.AllowNegative = True
        Me.txtCurrentBal.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCurrentBal.DigitsInGroup = 0
        Me.txtCurrentBal.Flags = 0
        Me.txtCurrentBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrentBal.Location = New System.Drawing.Point(182, 89)
        Me.txtCurrentBal.MaxDecimalPlaces = 6
        Me.txtCurrentBal.MaxWholeDigits = 21
        Me.txtCurrentBal.Name = "txtCurrentBal"
        Me.txtCurrentBal.Prefix = ""
        Me.txtCurrentBal.RangeMax = 1.7976931348623157E+308
        Me.txtCurrentBal.RangeMin = -1.7976931348623157E+308
        Me.txtCurrentBal.ReadOnly = True
        Me.txtCurrentBal.Size = New System.Drawing.Size(225, 21)
        Me.txtCurrentBal.TabIndex = 2
        Me.txtCurrentBal.Text = "0"
        Me.txtCurrentBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCurrentBal
        '
        Me.lblCurrentBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentBal.Location = New System.Drawing.Point(12, 91)
        Me.lblCurrentBal.Name = "lblCurrentBal"
        Me.lblCurrentBal.Size = New System.Drawing.Size(165, 16)
        Me.lblCurrentBal.TabIndex = 77
        Me.lblCurrentBal.Text = "Current Balance"
        Me.lblCurrentBal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFundProjectName
        '
        Me.cboFundProjectName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFundProjectName.DropDownWidth = 150
        Me.cboFundProjectName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFundProjectName.FormattingEnabled = True
        Me.cboFundProjectName.Location = New System.Drawing.Point(182, 35)
        Me.cboFundProjectName.Name = "cboFundProjectName"
        Me.cboFundProjectName.Size = New System.Drawing.Size(225, 21)
        Me.cboFundProjectName.TabIndex = 0
        '
        'txtIncrDecrAmount
        '
        Me.txtIncrDecrAmount.AllowNegative = True
        Me.txtIncrDecrAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtIncrDecrAmount.DigitsInGroup = 0
        Me.txtIncrDecrAmount.Flags = 0
        Me.txtIncrDecrAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIncrDecrAmount.Location = New System.Drawing.Point(182, 116)
        Me.txtIncrDecrAmount.MaxDecimalPlaces = 6
        Me.txtIncrDecrAmount.MaxWholeDigits = 21
        Me.txtIncrDecrAmount.Name = "txtIncrDecrAmount"
        Me.txtIncrDecrAmount.Prefix = ""
        Me.txtIncrDecrAmount.RangeMax = 1.7976931348623157E+308
        Me.txtIncrDecrAmount.RangeMin = -1.7976931348623157E+308
        Me.txtIncrDecrAmount.Size = New System.Drawing.Size(225, 21)
        Me.txtIncrDecrAmount.TabIndex = 3
        Me.txtIncrDecrAmount.Text = "0"
        Me.txtIncrDecrAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dtTransactionDate
        '
        Me.dtTransactionDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtTransactionDate.Checked = False
        Me.dtTransactionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtTransactionDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtTransactionDate.Location = New System.Drawing.Point(182, 62)
        Me.dtTransactionDate.Name = "dtTransactionDate"
        Me.dtTransactionDate.Size = New System.Drawing.Size(101, 21)
        Me.dtTransactionDate.TabIndex = 1
        '
        'lblTransactionDate
        '
        Me.lblTransactionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionDate.Location = New System.Drawing.Point(12, 64)
        Me.lblTransactionDate.Name = "lblTransactionDate"
        Me.lblTransactionDate.Size = New System.Drawing.Size(165, 16)
        Me.lblTransactionDate.TabIndex = 18
        Me.lblTransactionDate.Text = "Transaction Date"
        Me.lblTransactionDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFundProjectName
        '
        Me.lblFundProjectName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFundProjectName.Location = New System.Drawing.Point(12, 37)
        Me.lblFundProjectName.Name = "lblFundProjectName"
        Me.lblFundProjectName.Size = New System.Drawing.Size(165, 16)
        Me.lblFundProjectName.TabIndex = 12
        Me.lblFundProjectName.Text = "Fund Project Name"
        Me.lblFundProjectName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblIncDecAmount
        '
        Me.lblIncDecAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncDecAmount.Location = New System.Drawing.Point(12, 118)
        Me.lblIncDecAmount.Name = "lblIncDecAmount"
        Me.lblIncDecAmount.Size = New System.Drawing.Size(165, 16)
        Me.lblIncDecAmount.TabIndex = 16
        Me.lblIncDecAmount.Text = "Increment / Decrement Amount"
        Me.lblIncDecAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmFundAdjustment_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(441, 298)
        Me.Controls.Add(Me.gbProjectCodeAdjustments)
        Me.Controls.Add(Me.objFooter)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFundAdjustment_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Project Code Adjustments Add/Edit"
        Me.objFooter.ResumeLayout(False)
        Me.gbProjectCodeAdjustments.ResumeLayout(False)
        Me.gbProjectCodeAdjustments.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbProjectCodeAdjustments As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtNewBalance As eZee.TextBox.NumericTextBox
    Friend WithEvents lblNewBalance As System.Windows.Forms.Label
    Friend WithEvents txtCurrentBal As eZee.TextBox.NumericTextBox
    Friend WithEvents lblCurrentBal As System.Windows.Forms.Label
    Friend WithEvents cboFundProjectName As System.Windows.Forms.ComboBox
    Friend WithEvents txtIncrDecrAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents dtTransactionDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTransactionDate As System.Windows.Forms.Label
    Friend WithEvents lblFundProjectName As System.Windows.Forms.Label
    Friend WithEvents lblIncDecAmount As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchFundName As eZee.Common.eZeeGradientButton
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
End Class

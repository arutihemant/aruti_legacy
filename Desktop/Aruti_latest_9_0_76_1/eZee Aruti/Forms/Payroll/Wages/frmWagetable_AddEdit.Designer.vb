﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWagetable_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWagetable_AddEdit))
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlWageTable = New System.Windows.Forms.Panel
        Me.gbWagetable = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblGrade = New System.Windows.Forms.Label
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblGradegroup = New System.Windows.Forms.Label
        Me.cboGradegroup = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lnkCopyEffectivePeriodSlab = New System.Windows.Forms.LinkLabel
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportAll = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportListed = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImport = New System.Windows.Forms.ToolStripMenuItem
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgWagetable = New System.Windows.Forms.DataGridView
        Me.objcolhPeriodunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhwagetranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhwageunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhGradeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGrade = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhLevelID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGradeLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMinimum = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMidpoint = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMaximum = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIncrement = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPeriodStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.dlgSaveFile = New System.Windows.Forms.SaveFileDialog
        Me.pnlWageTable.SuspendLayout()
        Me.gbWagetable.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        CType(Me.dgWagetable, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlWageTable
        '
        Me.pnlWageTable.Controls.Add(Me.gbWagetable)
        Me.pnlWageTable.Controls.Add(Me.objFooter)
        Me.pnlWageTable.Controls.Add(Me.dgWagetable)
        Me.pnlWageTable.Controls.Add(Me.EZeeHeader1)
        Me.pnlWageTable.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlWageTable.Location = New System.Drawing.Point(0, 0)
        Me.pnlWageTable.Name = "pnlWageTable"
        Me.pnlWageTable.Size = New System.Drawing.Size(814, 503)
        Me.pnlWageTable.TabIndex = 0
        '
        'gbWagetable
        '
        Me.gbWagetable.BorderColor = System.Drawing.Color.Black
        Me.gbWagetable.Checked = False
        Me.gbWagetable.CollapseAllExceptThis = False
        Me.gbWagetable.CollapsedHoverImage = Nothing
        Me.gbWagetable.CollapsedNormalImage = Nothing
        Me.gbWagetable.CollapsedPressedImage = Nothing
        Me.gbWagetable.CollapseOnLoad = False
        Me.gbWagetable.Controls.Add(Me.lblPeriod)
        Me.gbWagetable.Controls.Add(Me.lblGrade)
        Me.gbWagetable.Controls.Add(Me.cboGrade)
        Me.gbWagetable.Controls.Add(Me.cboPeriod)
        Me.gbWagetable.Controls.Add(Me.lblGradegroup)
        Me.gbWagetable.Controls.Add(Me.cboGradegroup)
        Me.gbWagetable.ExpandedHoverImage = Nothing
        Me.gbWagetable.ExpandedNormalImage = Nothing
        Me.gbWagetable.ExpandedPressedImage = Nothing
        Me.gbWagetable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbWagetable.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbWagetable.HeaderHeight = 25
        Me.gbWagetable.HeaderMessage = ""
        Me.gbWagetable.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbWagetable.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbWagetable.HeightOnCollapse = 0
        Me.gbWagetable.LeftTextSpace = 0
        Me.gbWagetable.Location = New System.Drawing.Point(11, 67)
        Me.gbWagetable.Name = "gbWagetable"
        Me.gbWagetable.OpenHeight = 300
        Me.gbWagetable.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbWagetable.ShowBorder = True
        Me.gbWagetable.ShowCheckBox = False
        Me.gbWagetable.ShowCollapseButton = False
        Me.gbWagetable.ShowDefaultBorderColor = True
        Me.gbWagetable.ShowDownButton = False
        Me.gbWagetable.ShowHeader = True
        Me.gbWagetable.Size = New System.Drawing.Size(791, 67)
        Me.gbWagetable.TabIndex = 0
        Me.gbWagetable.Temp = 0
        Me.gbWagetable.Text = "Wages Table"
        Me.gbWagetable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(529, 37)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(80, 16)
        Me.lblPeriod.TabIndex = 112
        Me.lblPeriod.Text = "As On Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(299, 36)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(54, 16)
        Me.lblGrade.TabIndex = 109
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(359, 34)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(143, 21)
        Me.cboGrade.TabIndex = 108
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(615, 35)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(115, 21)
        Me.cboPeriod.TabIndex = 111
        '
        'lblGradegroup
        '
        Me.lblGradegroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradegroup.Location = New System.Drawing.Point(8, 36)
        Me.lblGradegroup.Name = "lblGradegroup"
        Me.lblGradegroup.Size = New System.Drawing.Size(87, 16)
        Me.lblGradegroup.TabIndex = 106
        Me.lblGradegroup.Text = "Grade Group"
        Me.lblGradegroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGradegroup
        '
        Me.cboGradegroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradegroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradegroup.FormattingEnabled = True
        Me.cboGradegroup.Location = New System.Drawing.Point(101, 34)
        Me.cboGradegroup.Name = "cboGradegroup"
        Me.cboGradegroup.Size = New System.Drawing.Size(145, 21)
        Me.cboGradegroup.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lnkCopyEffectivePeriodSlab)
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 448)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(814, 55)
        Me.objFooter.TabIndex = 2
        '
        'lnkCopyEffectivePeriodSlab
        '
        Me.lnkCopyEffectivePeriodSlab.Location = New System.Drawing.Point(421, 22)
        Me.lnkCopyEffectivePeriodSlab.Name = "lnkCopyEffectivePeriodSlab"
        Me.lnkCopyEffectivePeriodSlab.Size = New System.Drawing.Size(159, 16)
        Me.lnkCopyEffectivePeriodSlab.TabIndex = 1
        Me.lnkCopyEffectivePeriodSlab.TabStop = True
        Me.lnkCopyEffectivePeriodSlab.Text = "Copy Effective Period Slab"
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(10, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(108, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 8
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExport, Me.mnuImport})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(153, 70)
        '
        'mnuExport
        '
        Me.mnuExport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExportAll, Me.mnuExportListed})
        Me.mnuExport.Name = "mnuExport"
        Me.mnuExport.Size = New System.Drawing.Size(152, 22)
        Me.mnuExport.Text = "E&xport"
        '
        'mnuExportAll
        '
        Me.mnuExportAll.Name = "mnuExportAll"
        Me.mnuExportAll.Size = New System.Drawing.Size(105, 22)
        Me.mnuExportAll.Text = "&All"
        '
        'mnuExportListed
        '
        Me.mnuExportListed.Name = "mnuExportListed"
        Me.mnuExportListed.Size = New System.Drawing.Size(105, 22)
        Me.mnuExportListed.Text = "&Listed"
        '
        'mnuImport
        '
        Me.mnuImport.Name = "mnuImport"
        Me.mnuImport.Size = New System.Drawing.Size(152, 22)
        Me.mnuImport.Text = "&Import"
        Me.mnuImport.Visible = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(706, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(602, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'dgWagetable
        '
        Me.dgWagetable.AllowUserToAddRows = False
        Me.dgWagetable.AllowUserToDeleteRows = False
        Me.dgWagetable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgWagetable.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhPeriodunkid, Me.colhPeriod, Me.objcolhwagetranunkid, Me.objcolhwageunkid, Me.objcolhGradeID, Me.colhGrade, Me.objcolhLevelID, Me.colhGradeLevel, Me.colhMinimum, Me.colhMidpoint, Me.colhMaximum, Me.colhIncrement, Me.objcolhPeriodStatus})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgWagetable.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgWagetable.Location = New System.Drawing.Point(10, 140)
        Me.dgWagetable.Name = "dgWagetable"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgWagetable.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgWagetable.RowHeadersWidth = 4
        Me.dgWagetable.Size = New System.Drawing.Size(792, 302)
        Me.dgWagetable.TabIndex = 1
        '
        'objcolhPeriodunkid
        '
        Me.objcolhPeriodunkid.HeaderText = "Periodunkid"
        Me.objcolhPeriodunkid.Name = "objcolhPeriodunkid"
        Me.objcolhPeriodunkid.ReadOnly = True
        Me.objcolhPeriodunkid.Visible = False
        '
        'colhPeriod
        '
        Me.colhPeriod.HeaderText = "Effective Period"
        Me.colhPeriod.Name = "colhPeriod"
        Me.colhPeriod.ReadOnly = True
        '
        'objcolhwagetranunkid
        '
        Me.objcolhwagetranunkid.HeaderText = "wagetranunkid"
        Me.objcolhwagetranunkid.Name = "objcolhwagetranunkid"
        Me.objcolhwagetranunkid.ReadOnly = True
        Me.objcolhwagetranunkid.Visible = False
        '
        'objcolhwageunkid
        '
        Me.objcolhwageunkid.HeaderText = "wageunkid"
        Me.objcolhwageunkid.Name = "objcolhwageunkid"
        Me.objcolhwageunkid.ReadOnly = True
        Me.objcolhwageunkid.Visible = False
        '
        'objcolhGradeID
        '
        Me.objcolhGradeID.HeaderText = "GradeID"
        Me.objcolhGradeID.Name = "objcolhGradeID"
        Me.objcolhGradeID.ReadOnly = True
        Me.objcolhGradeID.Visible = False
        '
        'colhGrade
        '
        Me.colhGrade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhGrade.HeaderText = "Grade"
        Me.colhGrade.Name = "colhGrade"
        Me.colhGrade.ReadOnly = True
        Me.colhGrade.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'objcolhLevelID
        '
        Me.objcolhLevelID.HeaderText = "LevelID"
        Me.objcolhLevelID.Name = "objcolhLevelID"
        Me.objcolhLevelID.ReadOnly = True
        Me.objcolhLevelID.Visible = False
        '
        'colhGradeLevel
        '
        Me.colhGradeLevel.HeaderText = "Level"
        Me.colhGradeLevel.Name = "colhGradeLevel"
        Me.colhGradeLevel.ReadOnly = True
        Me.colhGradeLevel.Width = 140
        '
        'colhMinimum
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F2"
        Me.colhMinimum.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhMinimum.HeaderText = "Minimum"
        Me.colhMinimum.Name = "colhMinimum"
        Me.colhMinimum.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhMidpoint
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "F2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.colhMidpoint.DefaultCellStyle = DataGridViewCellStyle2
        Me.colhMidpoint.HeaderText = "Mid point"
        Me.colhMidpoint.Name = "colhMidpoint"
        Me.colhMidpoint.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhMaximum
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "F2"
        Me.colhMaximum.DefaultCellStyle = DataGridViewCellStyle3
        Me.colhMaximum.HeaderText = "Maximum"
        Me.colhMaximum.Name = "colhMaximum"
        Me.colhMaximum.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'colhIncrement
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "F2"
        Me.colhIncrement.DefaultCellStyle = DataGridViewCellStyle4
        Me.colhIncrement.HeaderText = "Increment"
        Me.colhIncrement.Name = "colhIncrement"
        Me.colhIncrement.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'objcolhPeriodStatus
        '
        Me.objcolhPeriodStatus.HeaderText = "Status"
        Me.objcolhPeriodStatus.Name = "objcolhPeriodStatus"
        Me.objcolhPeriodStatus.ReadOnly = True
        Me.objcolhPeriodStatus.Visible = False
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(814, 60)
        Me.EZeeHeader1.TabIndex = 3
        Me.EZeeHeader1.Title = "Wages Table Information"
        '
        'frmWagetable_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(814, 503)
        Me.Controls.Add(Me.pnlWageTable)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmWagetable_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Wages Table"
        Me.pnlWageTable.ResumeLayout(False)
        Me.gbWagetable.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        CType(Me.dgWagetable, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlWageTable As System.Windows.Forms.Panel
    Friend WithEvents gbWagetable As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents cboGradegroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradegroup As System.Windows.Forms.Label
    Friend WithEvents dgWagetable As System.Windows.Forms.DataGridView
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportListed As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dlgSaveFile As System.Windows.Forms.SaveFileDialog
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objcolhPeriodunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhwagetranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhwageunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhGradeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGrade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhLevelID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGradeLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMinimum As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMidpoint As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMaximum As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIncrement As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPeriodStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkCopyEffectivePeriodSlab As System.Windows.Forms.LinkLabel
End Class

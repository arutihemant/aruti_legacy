﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPeriod_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPeriod_AddEdit))
        Me.pnlPeriod = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbPeriod = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objpnlAssessmentDays = New System.Windows.Forms.Panel
        Me.txtDate2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtDate1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblDaysBefore = New System.Windows.Forms.Label
        Me.lblDaysAfter = New System.Windows.Forms.Label
        Me.nudDaysAfter = New System.Windows.Forms.NumericUpDown
        Me.nudDaysBefore = New System.Windows.Forms.NumericUpDown
        Me.lblFlexCubeNMBJV_BatchCode = New System.Windows.Forms.Label
        Me.txtFlexCubeNMBJV_BatchCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtCustomCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCostomCode = New System.Windows.Forms.Label
        Me.lblPrescribedIntRate = New System.Windows.Forms.Label
        Me.txtPrescribedIntRate = New eZee.TextBox.NumericTextBox
        Me.dtpTnAEnddate = New System.Windows.Forms.DateTimePicker
        Me.lblTnAEnddate = New System.Windows.Forms.Label
        Me.lblConstantDays = New System.Windows.Forms.Label
        Me.txtConstantDays = New eZee.TextBox.NumericTextBox
        Me.cboPayyear = New System.Windows.Forms.ComboBox
        Me.lblYear = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.dtpEnddate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartdate = New System.Windows.Forms.DateTimePicker
        Me.lblStartdate = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblEnddate = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.chkIsCmptPeriod = New System.Windows.Forms.CheckBox
        Me.pnlPeriod.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbPeriod.SuspendLayout()
        Me.objpnlAssessmentDays.SuspendLayout()
        CType(Me.nudDaysAfter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDaysBefore, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlPeriod
        '
        Me.pnlPeriod.Controls.Add(Me.objFooter)
        Me.pnlPeriod.Controls.Add(Me.gbPeriod)
        Me.pnlPeriod.Controls.Add(Me.EZeeHeader1)
        Me.pnlPeriod.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPeriod.Location = New System.Drawing.Point(0, 0)
        Me.pnlPeriod.Name = "pnlPeriod"
        Me.pnlPeriod.Size = New System.Drawing.Size(512, 441)
        Me.pnlPeriod.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 386)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(512, 55)
        Me.objFooter.TabIndex = 15
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(413, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 10
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(317, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbPeriod
        '
        Me.gbPeriod.BorderColor = System.Drawing.Color.Black
        Me.gbPeriod.Checked = False
        Me.gbPeriod.CollapseAllExceptThis = False
        Me.gbPeriod.CollapsedHoverImage = Nothing
        Me.gbPeriod.CollapsedNormalImage = Nothing
        Me.gbPeriod.CollapsedPressedImage = Nothing
        Me.gbPeriod.CollapseOnLoad = False
        Me.gbPeriod.Controls.Add(Me.objpnlAssessmentDays)
        Me.gbPeriod.Controls.Add(Me.lblFlexCubeNMBJV_BatchCode)
        Me.gbPeriod.Controls.Add(Me.txtFlexCubeNMBJV_BatchCode)
        Me.gbPeriod.Controls.Add(Me.txtCustomCode)
        Me.gbPeriod.Controls.Add(Me.lblCostomCode)
        Me.gbPeriod.Controls.Add(Me.lblPrescribedIntRate)
        Me.gbPeriod.Controls.Add(Me.txtPrescribedIntRate)
        Me.gbPeriod.Controls.Add(Me.dtpTnAEnddate)
        Me.gbPeriod.Controls.Add(Me.lblTnAEnddate)
        Me.gbPeriod.Controls.Add(Me.lblConstantDays)
        Me.gbPeriod.Controls.Add(Me.txtConstantDays)
        Me.gbPeriod.Controls.Add(Me.cboPayyear)
        Me.gbPeriod.Controls.Add(Me.lblYear)
        Me.gbPeriod.Controls.Add(Me.lblStatus)
        Me.gbPeriod.Controls.Add(Me.cboStatus)
        Me.gbPeriod.Controls.Add(Me.dtpEnddate)
        Me.gbPeriod.Controls.Add(Me.dtpStartdate)
        Me.gbPeriod.Controls.Add(Me.lblStartdate)
        Me.gbPeriod.Controls.Add(Me.lblDescription)
        Me.gbPeriod.Controls.Add(Me.lblEnddate)
        Me.gbPeriod.Controls.Add(Me.txtDescription)
        Me.gbPeriod.Controls.Add(Me.lblName)
        Me.gbPeriod.Controls.Add(Me.txtName)
        Me.gbPeriod.Controls.Add(Me.lblCode)
        Me.gbPeriod.Controls.Add(Me.txtCode)
        Me.gbPeriod.Controls.Add(Me.chkIsCmptPeriod)
        Me.gbPeriod.ExpandedHoverImage = Nothing
        Me.gbPeriod.ExpandedNormalImage = Nothing
        Me.gbPeriod.ExpandedPressedImage = Nothing
        Me.gbPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPeriod.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPeriod.HeaderHeight = 25
        Me.gbPeriod.HeaderMessage = ""
        Me.gbPeriod.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPeriod.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPeriod.HeightOnCollapse = 0
        Me.gbPeriod.LeftTextSpace = 0
        Me.gbPeriod.Location = New System.Drawing.Point(11, 64)
        Me.gbPeriod.Name = "gbPeriod"
        Me.gbPeriod.OpenHeight = 300
        Me.gbPeriod.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPeriod.ShowBorder = True
        Me.gbPeriod.ShowCheckBox = False
        Me.gbPeriod.ShowCollapseButton = False
        Me.gbPeriod.ShowDefaultBorderColor = True
        Me.gbPeriod.ShowDownButton = False
        Me.gbPeriod.ShowHeader = True
        Me.gbPeriod.Size = New System.Drawing.Size(488, 316)
        Me.gbPeriod.TabIndex = 0
        Me.gbPeriod.Temp = 0
        Me.gbPeriod.Text = "Period"
        Me.gbPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnlAssessmentDays
        '
        Me.objpnlAssessmentDays.Controls.Add(Me.txtDate2)
        Me.objpnlAssessmentDays.Controls.Add(Me.txtDate1)
        Me.objpnlAssessmentDays.Controls.Add(Me.lblDaysBefore)
        Me.objpnlAssessmentDays.Controls.Add(Me.lblDaysAfter)
        Me.objpnlAssessmentDays.Controls.Add(Me.nudDaysAfter)
        Me.objpnlAssessmentDays.Controls.Add(Me.nudDaysBefore)
        Me.objpnlAssessmentDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlAssessmentDays.Location = New System.Drawing.Point(4, 254)
        Me.objpnlAssessmentDays.Name = "objpnlAssessmentDays"
        Me.objpnlAssessmentDays.Size = New System.Drawing.Size(481, 54)
        Me.objpnlAssessmentDays.TabIndex = 22
        Me.objpnlAssessmentDays.Visible = False
        '
        'txtDate2
        '
        Me.txtDate2.BackColor = System.Drawing.SystemColors.Info
        Me.txtDate2.Flags = 0
        Me.txtDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDate2.Location = New System.Drawing.Point(367, 30)
        Me.txtDate2.Name = "txtDate2"
        Me.txtDate2.ReadOnly = True
        Me.txtDate2.Size = New System.Drawing.Size(105, 21)
        Me.txtDate2.TabIndex = 130
        '
        'txtDate1
        '
        Me.txtDate1.BackColor = System.Drawing.SystemColors.Info
        Me.txtDate1.Flags = 0
        Me.txtDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDate1.Location = New System.Drawing.Point(367, 3)
        Me.txtDate1.Name = "txtDate1"
        Me.txtDate1.ReadOnly = True
        Me.txtDate1.Size = New System.Drawing.Size(105, 21)
        Me.txtDate1.TabIndex = 129
        '
        'lblDaysBefore
        '
        Me.lblDaysBefore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDaysBefore.Location = New System.Drawing.Point(4, 5)
        Me.lblDaysBefore.Name = "lblDaysBefore"
        Me.lblDaysBefore.Size = New System.Drawing.Size(287, 16)
        Me.lblDaysBefore.TabIndex = 128
        Me.lblDaysBefore.Text = "Lock Assessment Planning for Employee from Start Date "
        Me.lblDaysBefore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDaysAfter
        '
        Me.lblDaysAfter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDaysAfter.Location = New System.Drawing.Point(4, 32)
        Me.lblDaysAfter.Name = "lblDaysAfter"
        Me.lblDaysAfter.Size = New System.Drawing.Size(287, 16)
        Me.lblDaysAfter.TabIndex = 127
        Me.lblDaysAfter.Text = "Lock Assessment Evaluation for Employee from End Date"
        Me.lblDaysAfter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudDaysAfter
        '
        Me.nudDaysAfter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDaysAfter.Location = New System.Drawing.Point(297, 3)
        Me.nudDaysAfter.Name = "nudDaysAfter"
        Me.nudDaysAfter.Size = New System.Drawing.Size(64, 21)
        Me.nudDaysAfter.TabIndex = 125
        Me.nudDaysAfter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'nudDaysBefore
        '
        Me.nudDaysBefore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDaysBefore.Location = New System.Drawing.Point(297, 30)
        Me.nudDaysBefore.Name = "nudDaysBefore"
        Me.nudDaysBefore.Size = New System.Drawing.Size(64, 21)
        Me.nudDaysBefore.TabIndex = 126
        Me.nudDaysBefore.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblFlexCubeNMBJV_BatchCode
        '
        Me.lblFlexCubeNMBJV_BatchCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFlexCubeNMBJV_BatchCode.Location = New System.Drawing.Point(198, 234)
        Me.lblFlexCubeNMBJV_BatchCode.Name = "lblFlexCubeNMBJV_BatchCode"
        Me.lblFlexCubeNMBJV_BatchCode.Size = New System.Drawing.Size(142, 16)
        Me.lblFlexCubeNMBJV_BatchCode.TabIndex = 123
        Me.lblFlexCubeNMBJV_BatchCode.Text = "Flex Cube JV Batch Code"
        Me.lblFlexCubeNMBJV_BatchCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFlexCubeNMBJV_BatchCode
        '
        Me.txtFlexCubeNMBJV_BatchCode.Flags = 0
        Me.txtFlexCubeNMBJV_BatchCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFlexCubeNMBJV_BatchCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFlexCubeNMBJV_BatchCode.Location = New System.Drawing.Point(346, 232)
        Me.txtFlexCubeNMBJV_BatchCode.Name = "txtFlexCubeNMBJV_BatchCode"
        Me.txtFlexCubeNMBJV_BatchCode.ReadOnly = True
        Me.txtFlexCubeNMBJV_BatchCode.Size = New System.Drawing.Size(130, 21)
        Me.txtFlexCubeNMBJV_BatchCode.TabIndex = 122
        '
        'txtCustomCode
        '
        Me.txtCustomCode.Flags = 0
        Me.txtCustomCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCustomCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCustomCode.Location = New System.Drawing.Point(346, 205)
        Me.txtCustomCode.Name = "txtCustomCode"
        Me.txtCustomCode.Size = New System.Drawing.Size(130, 21)
        Me.txtCustomCode.TabIndex = 120
        '
        'lblCostomCode
        '
        Me.lblCostomCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostomCode.Location = New System.Drawing.Point(250, 207)
        Me.lblCostomCode.Name = "lblCostomCode"
        Me.lblCostomCode.Size = New System.Drawing.Size(90, 16)
        Me.lblCostomCode.TabIndex = 119
        Me.lblCostomCode.Text = "Custom Code"
        Me.lblCostomCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPrescribedIntRate
        '
        Me.lblPrescribedIntRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrescribedIntRate.Location = New System.Drawing.Point(224, 36)
        Me.lblPrescribedIntRate.Name = "lblPrescribedIntRate"
        Me.lblPrescribedIntRate.Size = New System.Drawing.Size(170, 16)
        Me.lblPrescribedIntRate.TabIndex = 117
        Me.lblPrescribedIntRate.Text = "Quart. Prescribed Interest Rate"
        Me.lblPrescribedIntRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPrescribedIntRate
        '
        Me.txtPrescribedIntRate.AllowNegative = False
        Me.txtPrescribedIntRate.Decimal = New Decimal(New Integer() {2, 0, 0, 0})
        Me.txtPrescribedIntRate.DigitsInGroup = 0
        Me.txtPrescribedIntRate.Flags = 65536
        Me.txtPrescribedIntRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrescribedIntRate.Location = New System.Drawing.Point(400, 34)
        Me.txtPrescribedIntRate.MaxDecimalPlaces = 6
        Me.txtPrescribedIntRate.MaxLength = 100
        Me.txtPrescribedIntRate.MaxWholeDigits = 21
        Me.txtPrescribedIntRate.Name = "txtPrescribedIntRate"
        Me.txtPrescribedIntRate.Prefix = ""
        Me.txtPrescribedIntRate.RangeMax = 1.7976931348623157E+308
        Me.txtPrescribedIntRate.RangeMin = -1.7976931348623157E+308
        Me.txtPrescribedIntRate.Size = New System.Drawing.Size(76, 21)
        Me.txtPrescribedIntRate.TabIndex = 5
        Me.txtPrescribedIntRate.Text = "2"
        Me.txtPrescribedIntRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dtpTnAEnddate
        '
        Me.dtpTnAEnddate.Checked = False
        Me.dtpTnAEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTnAEnddate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTnAEnddate.Location = New System.Drawing.Point(107, 205)
        Me.dtpTnAEnddate.Name = "dtpTnAEnddate"
        Me.dtpTnAEnddate.Size = New System.Drawing.Size(137, 21)
        Me.dtpTnAEnddate.TabIndex = 113
        '
        'lblTnAEnddate
        '
        Me.lblTnAEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTnAEnddate.Location = New System.Drawing.Point(8, 207)
        Me.lblTnAEnddate.Name = "lblTnAEnddate"
        Me.lblTnAEnddate.Size = New System.Drawing.Size(93, 16)
        Me.lblTnAEnddate.TabIndex = 114
        Me.lblTnAEnddate.Text = "TnA End Date"
        Me.lblTnAEnddate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblConstantDays
        '
        Me.lblConstantDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConstantDays.Location = New System.Drawing.Point(224, 63)
        Me.lblConstantDays.Name = "lblConstantDays"
        Me.lblConstantDays.Size = New System.Drawing.Size(170, 16)
        Me.lblConstantDays.TabIndex = 111
        Me.lblConstantDays.Text = "Constant Days"
        Me.lblConstantDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtConstantDays
        '
        Me.txtConstantDays.AllowNegative = True
        Me.txtConstantDays.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtConstantDays.DigitsInGroup = 0
        Me.txtConstantDays.Flags = 0
        Me.txtConstantDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConstantDays.Location = New System.Drawing.Point(400, 61)
        Me.txtConstantDays.MaxDecimalPlaces = 6
        Me.txtConstantDays.MaxWholeDigits = 21
        Me.txtConstantDays.Name = "txtConstantDays"
        Me.txtConstantDays.Prefix = ""
        Me.txtConstantDays.RangeMax = 1.7976931348623157E+308
        Me.txtConstantDays.RangeMin = -1.7976931348623157E+308
        Me.txtConstantDays.Size = New System.Drawing.Size(76, 21)
        Me.txtConstantDays.TabIndex = 2
        Me.txtConstantDays.Text = "0"
        Me.txtConstantDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboPayyear
        '
        Me.cboPayyear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayyear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayyear.FormattingEnabled = True
        Me.cboPayyear.Location = New System.Drawing.Point(107, 34)
        Me.cboPayyear.Name = "cboPayyear"
        Me.cboPayyear.Size = New System.Drawing.Size(111, 21)
        Me.cboPayyear.TabIndex = 0
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(8, 36)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(93, 16)
        Me.lblYear.TabIndex = 106
        Me.lblYear.Text = "Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(8, 398)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(93, 16)
        Me.lblStatus.TabIndex = 102
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblStatus.Visible = False
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(107, 396)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(111, 21)
        Me.cboStatus.TabIndex = 8
        Me.cboStatus.Visible = False
        '
        'dtpEnddate
        '
        Me.dtpEnddate.Checked = False
        Me.dtpEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnddate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEnddate.Location = New System.Drawing.Point(346, 178)
        Me.dtpEnddate.Name = "dtpEnddate"
        Me.dtpEnddate.Size = New System.Drawing.Size(130, 21)
        Me.dtpEnddate.TabIndex = 7
        '
        'dtpStartdate
        '
        Me.dtpStartdate.Checked = False
        Me.dtpStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartdate.Location = New System.Drawing.Point(107, 178)
        Me.dtpStartdate.Name = "dtpStartdate"
        Me.dtpStartdate.Size = New System.Drawing.Size(137, 21)
        Me.dtpStartdate.TabIndex = 6
        '
        'lblStartdate
        '
        Me.lblStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartdate.Location = New System.Drawing.Point(8, 180)
        Me.lblStartdate.Name = "lblStartdate"
        Me.lblStartdate.Size = New System.Drawing.Size(93, 16)
        Me.lblStartdate.TabIndex = 97
        Me.lblStartdate.Text = "Start Date"
        Me.lblStartdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(8, 116)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(93, 16)
        Me.lblDescription.TabIndex = 96
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEnddate
        '
        Me.lblEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnddate.Location = New System.Drawing.Point(250, 180)
        Me.lblEnddate.Name = "lblEnddate"
        Me.lblEnddate.Size = New System.Drawing.Size(90, 16)
        Me.lblEnddate.TabIndex = 98
        Me.lblEnddate.Text = "End Date"
        Me.lblEnddate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(107, 115)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(369, 57)
        Me.txtDescription.TabIndex = 4
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 90)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(93, 16)
        Me.lblName.TabIndex = 92
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(107, 88)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(369, 21)
        Me.txtName.TabIndex = 3
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 63)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(93, 16)
        Me.lblCode.TabIndex = 3
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(107, 61)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(111, 21)
        Me.txtCode.TabIndex = 1
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(512, 58)
        Me.EZeeHeader1.TabIndex = 21
        Me.EZeeHeader1.Title = "Period Information"
        '
        'chkIsCmptPeriod
        '
        Me.chkIsCmptPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsCmptPeriod.Location = New System.Drawing.Point(11, 207)
        Me.chkIsCmptPeriod.Name = "chkIsCmptPeriod"
        Me.chkIsCmptPeriod.Size = New System.Drawing.Size(233, 17)
        Me.chkIsCmptPeriod.TabIndex = 125
        Me.chkIsCmptPeriod.Text = "Mark As Competencies Period"
        Me.chkIsCmptPeriod.UseVisualStyleBackColor = True
        Me.chkIsCmptPeriod.Visible = False
        '
        'frmPeriod_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(512, 441)
        Me.Controls.Add(Me.pnlPeriod)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPeriod_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Period"
        Me.pnlPeriod.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbPeriod.ResumeLayout(False)
        Me.gbPeriod.PerformLayout()
        Me.objpnlAssessmentDays.ResumeLayout(False)
        Me.objpnlAssessmentDays.PerformLayout()
        CType(Me.nudDaysAfter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDaysBefore, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlPeriod As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbPeriod As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEnddate As System.Windows.Forms.Label
    Friend WithEvents lblStartdate As System.Windows.Forms.Label
    Friend WithEvents dtpStartdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpEnddate As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboPayyear As System.Windows.Forms.ComboBox
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents lblConstantDays As System.Windows.Forms.Label
    Friend WithEvents txtConstantDays As eZee.TextBox.NumericTextBox
    Friend WithEvents dtpTnAEnddate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTnAEnddate As System.Windows.Forms.Label
    Friend WithEvents lblPrescribedIntRate As System.Windows.Forms.Label
    Friend WithEvents txtPrescribedIntRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblCostomCode As System.Windows.Forms.Label
    Friend WithEvents txtCustomCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFlexCubeNMBJV_BatchCode As System.Windows.Forms.Label
    Friend WithEvents txtFlexCubeNMBJV_BatchCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents nudDaysBefore As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudDaysAfter As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblDaysBefore As System.Windows.Forms.Label
    Friend WithEvents lblDaysAfter As System.Windows.Forms.Label
    Friend WithEvents objpnlAssessmentDays As System.Windows.Forms.Panel
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents txtDate2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDate1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkIsCmptPeriod As System.Windows.Forms.CheckBox
End Class

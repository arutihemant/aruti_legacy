﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportTranHeads
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportTranHeads))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.dgvImportInfo = New System.Windows.Forms.DataGridView
        Me.gbFileInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSet = New eZee.Common.eZeeLightButton(Me.components)
        Me.objLine2 = New eZee.Common.eZeeStraightLine
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.radApplytoChecked = New System.Windows.Forms.RadioButton
        Me.radApplySelected = New System.Windows.Forms.RadioButton
        Me.radApplytoAll = New System.Windows.Forms.RadioButton
        Me.lblCalcType = New System.Windows.Forms.Label
        Me.cboCalcTypeId = New System.Windows.Forms.ComboBox
        Me.cboTypeOfId = New System.Windows.Forms.ComboBox
        Me.lblTypeOfId = New System.Windows.Forms.Label
        Me.cboHeadTypeId = New System.Windows.Forms.ComboBox
        Me.lblHeadTypeId = New System.Windows.Forms.Label
        Me.elMandatoryInfo = New eZee.Common.eZeeLine
        Me.objbtnOpenFile = New eZee.Common.eZeeGradientButton
        Me.txtFilePath = New System.Windows.Forms.TextBox
        Me.lblFileName = New System.Windows.Forms.Label
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.ofdlgOpen = New System.Windows.Forms.OpenFileDialog
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhTranCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTranName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHeadType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTypeOF = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCalctype = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgvImportInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFileInfo.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.dgvImportInfo)
        Me.pnlMainInfo.Controls.Add(Me.gbFileInfo)
        Me.pnlMainInfo.Controls.Add(Me.objefFormFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(720, 541)
        Me.pnlMainInfo.TabIndex = 0
        '
        'dgvImportInfo
        '
        Me.dgvImportInfo.AllowUserToAddRows = False
        Me.dgvImportInfo.AllowUserToDeleteRows = False
        Me.dgvImportInfo.AllowUserToResizeColumns = False
        Me.dgvImportInfo.AllowUserToResizeRows = False
        Me.dgvImportInfo.BackgroundColor = System.Drawing.Color.White
        Me.dgvImportInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvImportInfo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhTranCode, Me.dgcolhTranName, Me.dgcolhHeadType, Me.dgcolhTypeOF, Me.dgcolhCalctype})
        Me.dgvImportInfo.Location = New System.Drawing.Point(12, 184)
        Me.dgvImportInfo.MultiSelect = False
        Me.dgvImportInfo.Name = "dgvImportInfo"
        Me.dgvImportInfo.RowHeadersVisible = False
        Me.dgvImportInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvImportInfo.Size = New System.Drawing.Size(696, 296)
        Me.dgvImportInfo.TabIndex = 5
        '
        'gbFileInfo
        '
        Me.gbFileInfo.BorderColor = System.Drawing.Color.Black
        Me.gbFileInfo.Checked = False
        Me.gbFileInfo.CollapseAllExceptThis = False
        Me.gbFileInfo.CollapsedHoverImage = Nothing
        Me.gbFileInfo.CollapsedNormalImage = Nothing
        Me.gbFileInfo.CollapsedPressedImage = Nothing
        Me.gbFileInfo.CollapseOnLoad = False
        Me.gbFileInfo.Controls.Add(Me.objbtnSet)
        Me.gbFileInfo.Controls.Add(Me.objLine2)
        Me.gbFileInfo.Controls.Add(Me.objLine1)
        Me.gbFileInfo.Controls.Add(Me.radApplytoChecked)
        Me.gbFileInfo.Controls.Add(Me.radApplySelected)
        Me.gbFileInfo.Controls.Add(Me.radApplytoAll)
        Me.gbFileInfo.Controls.Add(Me.lblCalcType)
        Me.gbFileInfo.Controls.Add(Me.cboCalcTypeId)
        Me.gbFileInfo.Controls.Add(Me.cboTypeOfId)
        Me.gbFileInfo.Controls.Add(Me.lblTypeOfId)
        Me.gbFileInfo.Controls.Add(Me.cboHeadTypeId)
        Me.gbFileInfo.Controls.Add(Me.lblHeadTypeId)
        Me.gbFileInfo.Controls.Add(Me.elMandatoryInfo)
        Me.gbFileInfo.Controls.Add(Me.objbtnOpenFile)
        Me.gbFileInfo.Controls.Add(Me.txtFilePath)
        Me.gbFileInfo.Controls.Add(Me.lblFileName)
        Me.gbFileInfo.ExpandedHoverImage = Nothing
        Me.gbFileInfo.ExpandedNormalImage = Nothing
        Me.gbFileInfo.ExpandedPressedImage = Nothing
        Me.gbFileInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFileInfo.HeaderHeight = 25
        Me.gbFileInfo.HeaderMessage = ""
        Me.gbFileInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFileInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFileInfo.HeightOnCollapse = 0
        Me.gbFileInfo.LeftTextSpace = 0
        Me.gbFileInfo.Location = New System.Drawing.Point(12, 12)
        Me.gbFileInfo.Name = "gbFileInfo"
        Me.gbFileInfo.OpenHeight = 300
        Me.gbFileInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFileInfo.ShowBorder = True
        Me.gbFileInfo.ShowCheckBox = False
        Me.gbFileInfo.ShowCollapseButton = False
        Me.gbFileInfo.ShowDefaultBorderColor = True
        Me.gbFileInfo.ShowDownButton = False
        Me.gbFileInfo.ShowHeader = True
        Me.gbFileInfo.Size = New System.Drawing.Size(696, 165)
        Me.gbFileInfo.TabIndex = 2
        Me.gbFileInfo.Temp = 0
        Me.gbFileInfo.Text = "File Name"
        Me.gbFileInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSet
        '
        Me.objbtnSet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSet.BackColor = System.Drawing.Color.White
        Me.objbtnSet.BackgroundImage = CType(resources.GetObject("objbtnSet.BackgroundImage"), System.Drawing.Image)
        Me.objbtnSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnSet.BorderColor = System.Drawing.Color.Empty
        Me.objbtnSet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnSet.FlatAppearance.BorderSize = 0
        Me.objbtnSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSet.ForeColor = System.Drawing.Color.Black
        Me.objbtnSet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnSet.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnSet.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnSet.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnSet.Location = New System.Drawing.Point(541, 121)
        Me.objbtnSet.Name = "objbtnSet"
        Me.objbtnSet.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnSet.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnSet.Size = New System.Drawing.Size(114, 30)
        Me.objbtnSet.TabIndex = 18
        Me.objbtnSet.Text = "&Set"
        Me.objbtnSet.UseVisualStyleBackColor = False
        '
        'objLine2
        '
        Me.objLine2.BackColor = System.Drawing.Color.Transparent
        Me.objLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine2.Location = New System.Drawing.Point(516, 78)
        Me.objLine2.Name = "objLine2"
        Me.objLine2.Size = New System.Drawing.Size(19, 75)
        Me.objLine2.TabIndex = 17
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(352, 78)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(19, 75)
        Me.objLine1.TabIndex = 16
        '
        'radApplytoChecked
        '
        Me.radApplytoChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoChecked.Location = New System.Drawing.Point(377, 134)
        Me.radApplytoChecked.Name = "radApplytoChecked"
        Me.radApplytoChecked.Size = New System.Drawing.Size(133, 17)
        Me.radApplytoChecked.TabIndex = 15
        Me.radApplytoChecked.TabStop = True
        Me.radApplytoChecked.Text = "Apply To Checked"
        Me.radApplytoChecked.UseVisualStyleBackColor = True
        '
        'radApplySelected
        '
        Me.radApplySelected.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplySelected.Location = New System.Drawing.Point(377, 107)
        Me.radApplySelected.Name = "radApplySelected"
        Me.radApplySelected.Size = New System.Drawing.Size(133, 17)
        Me.radApplySelected.TabIndex = 13
        Me.radApplySelected.TabStop = True
        Me.radApplySelected.Text = "Apply to Selected"
        Me.radApplySelected.UseVisualStyleBackColor = True
        '
        'radApplytoAll
        '
        Me.radApplytoAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoAll.Location = New System.Drawing.Point(377, 80)
        Me.radApplytoAll.Name = "radApplytoAll"
        Me.radApplytoAll.Size = New System.Drawing.Size(133, 17)
        Me.radApplytoAll.TabIndex = 14
        Me.radApplytoAll.TabStop = True
        Me.radApplytoAll.Text = "Apply to All"
        Me.radApplytoAll.UseVisualStyleBackColor = True
        '
        'lblCalcType
        '
        Me.lblCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalcType.Location = New System.Drawing.Point(45, 134)
        Me.lblCalcType.Name = "lblCalcType"
        Me.lblCalcType.Size = New System.Drawing.Size(88, 17)
        Me.lblCalcType.TabIndex = 12
        Me.lblCalcType.Text = "Calculation Type"
        Me.lblCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCalcTypeId
        '
        Me.cboCalcTypeId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCalcTypeId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCalcTypeId.FormattingEnabled = True
        Me.cboCalcTypeId.Location = New System.Drawing.Point(139, 132)
        Me.cboCalcTypeId.Name = "cboCalcTypeId"
        Me.cboCalcTypeId.Size = New System.Drawing.Size(207, 21)
        Me.cboCalcTypeId.TabIndex = 11
        '
        'cboTypeOfId
        '
        Me.cboTypeOfId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTypeOfId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTypeOfId.FormattingEnabled = True
        Me.cboTypeOfId.Location = New System.Drawing.Point(139, 105)
        Me.cboTypeOfId.Name = "cboTypeOfId"
        Me.cboTypeOfId.Size = New System.Drawing.Size(207, 21)
        Me.cboTypeOfId.TabIndex = 10
        '
        'lblTypeOfId
        '
        Me.lblTypeOfId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTypeOfId.Location = New System.Drawing.Point(45, 107)
        Me.lblTypeOfId.Name = "lblTypeOfId"
        Me.lblTypeOfId.Size = New System.Drawing.Size(88, 17)
        Me.lblTypeOfId.TabIndex = 9
        Me.lblTypeOfId.Text = "Type Of Id"
        Me.lblTypeOfId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboHeadTypeId
        '
        Me.cboHeadTypeId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHeadTypeId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboHeadTypeId.FormattingEnabled = True
        Me.cboHeadTypeId.Location = New System.Drawing.Point(139, 78)
        Me.cboHeadTypeId.Name = "cboHeadTypeId"
        Me.cboHeadTypeId.Size = New System.Drawing.Size(207, 21)
        Me.cboHeadTypeId.TabIndex = 8
        '
        'lblHeadTypeId
        '
        Me.lblHeadTypeId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeadTypeId.Location = New System.Drawing.Point(45, 80)
        Me.lblHeadTypeId.Name = "lblHeadTypeId"
        Me.lblHeadTypeId.Size = New System.Drawing.Size(88, 17)
        Me.lblHeadTypeId.TabIndex = 7
        Me.lblHeadTypeId.Text = "Head Type Id"
        Me.lblHeadTypeId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elMandatoryInfo
        '
        Me.elMandatoryInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elMandatoryInfo.Location = New System.Drawing.Point(11, 58)
        Me.elMandatoryInfo.Name = "elMandatoryInfo"
        Me.elMandatoryInfo.Size = New System.Drawing.Size(644, 17)
        Me.elMandatoryInfo.TabIndex = 6
        Me.elMandatoryInfo.Text = "Mandatory Information"
        Me.elMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOpenFile
        '
        Me.objbtnOpenFile.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOpenFile.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOpenFile.BorderSelected = False
        Me.objbtnOpenFile.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnOpenFile.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnOpenFile.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOpenFile.Location = New System.Drawing.Point(661, 34)
        Me.objbtnOpenFile.Name = "objbtnOpenFile"
        Me.objbtnOpenFile.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOpenFile.TabIndex = 5
        '
        'txtFilePath
        '
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.Location = New System.Drawing.Point(139, 34)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(516, 21)
        Me.txtFilePath.TabIndex = 3
        '
        'lblFileName
        '
        Me.lblFileName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileName.Location = New System.Drawing.Point(45, 36)
        Me.lblFileName.Name = "lblFileName"
        Me.lblFileName.Size = New System.Drawing.Size(88, 17)
        Me.lblFileName.TabIndex = 3
        Me.lblFileName.Text = "File Name"
        Me.lblFileName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnImport)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 486)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(720, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(611, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.BackColor = System.Drawing.Color.White
        Me.btnImport.BackgroundImage = CType(resources.GetObject("btnImport.BackgroundImage"), System.Drawing.Image)
        Me.btnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnImport.BorderColor = System.Drawing.Color.Empty
        Me.btnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.Color.Black
        Me.btnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnImport.GradientForeColor = System.Drawing.Color.Black
        Me.btnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Location = New System.Drawing.Point(508, 13)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Size = New System.Drawing.Size(97, 30)
        Me.btnImport.TabIndex = 0
        Me.btnImport.Text = "&Import"
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'ofdlgOpen
        '
        Me.ofdlgOpen.FileName = "OpenFileDialog1"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Trans. Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn3.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Head Type"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 135
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Type Of"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 135
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhTranCode
        '
        Me.dgcolhTranCode.HeaderText = "Trans. Code"
        Me.dgcolhTranCode.Name = "dgcolhTranCode"
        Me.dgcolhTranCode.ReadOnly = True
        Me.dgcolhTranCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTranCode.Width = 130
        '
        'dgcolhTranName
        '
        Me.dgcolhTranName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhTranName.HeaderText = "Tran. Head Name"
        Me.dgcolhTranName.Name = "dgcolhTranName"
        Me.dgcolhTranName.ReadOnly = True
        Me.dgcolhTranName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhHeadType
        '
        Me.dgcolhHeadType.HeaderText = "Head Type"
        Me.dgcolhHeadType.Name = "dgcolhHeadType"
        Me.dgcolhHeadType.ReadOnly = True
        Me.dgcolhHeadType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhHeadType.Width = 135
        '
        'dgcolhTypeOF
        '
        Me.dgcolhTypeOF.HeaderText = "Type Of"
        Me.dgcolhTypeOF.Name = "dgcolhTypeOF"
        Me.dgcolhTypeOF.ReadOnly = True
        Me.dgcolhTypeOF.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTypeOF.Width = 135
        '
        'dgcolhCalctype
        '
        Me.dgcolhCalctype.HeaderText = "Calculation Type"
        Me.dgcolhCalctype.Name = "dgcolhCalctype"
        Me.dgcolhCalctype.ReadOnly = True
        Me.dgcolhCalctype.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'frmImportTranHeads
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(720, 541)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportTranHeads"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Transaction Head"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.dgvImportInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFileInfo.ResumeLayout(False)
        Me.gbFileInfo.PerformLayout()
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnImport As eZee.Common.eZeeLightButton
    Friend WithEvents gbFileInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblFileName As System.Windows.Forms.Label
    Friend WithEvents txtFilePath As System.Windows.Forms.TextBox
    Friend WithEvents elMandatoryInfo As eZee.Common.eZeeLine
    Friend WithEvents objbtnOpenFile As eZee.Common.eZeeGradientButton
    Friend WithEvents radApplytoChecked As System.Windows.Forms.RadioButton
    Friend WithEvents radApplytoAll As System.Windows.Forms.RadioButton
    Friend WithEvents lblCalcType As System.Windows.Forms.Label
    Friend WithEvents radApplySelected As System.Windows.Forms.RadioButton
    Friend WithEvents cboCalcTypeId As System.Windows.Forms.ComboBox
    Friend WithEvents cboTypeOfId As System.Windows.Forms.ComboBox
    Friend WithEvents lblTypeOfId As System.Windows.Forms.Label
    Friend WithEvents cboHeadTypeId As System.Windows.Forms.ComboBox
    Friend WithEvents lblHeadTypeId As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnSet As eZee.Common.eZeeLightButton
    Friend WithEvents ofdlgOpen As System.Windows.Forms.OpenFileDialog
    Friend WithEvents dgvImportInfo As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhTranCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTranName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHeadType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTypeOF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCalctype As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAppraisalSetups
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAppraisalSetups))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbAppraisalSetup = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnBSCSearch = New eZee.Common.eZeeGradientButton
        Me.objbtnGEActionSearch = New eZee.Common.eZeeGradientButton
        Me.cboBSCAppraisalAction = New System.Windows.Forms.ComboBox
        Me.cboGEAppraisalAction = New System.Windows.Forms.ComboBox
        Me.lblBSCAppraisalAction = New System.Windows.Forms.Label
        Me.lblGEAppraisalAction = New System.Windows.Forms.Label
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvRatings = New eZee.Common.eZeeListView(Me.components)
        Me.colhScoreFrom = New System.Windows.Forms.ColumnHeader
        Me.colhScoreTo = New System.Windows.Forms.ColumnHeader
        Me.colhGrade = New System.Windows.Forms.ColumnHeader
        Me.colhAction = New System.Windows.Forms.ColumnHeader
        Me.colhBSCAction = New System.Windows.Forms.ColumnHeader
        Me.colhGEAction = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.objColhActionId = New System.Windows.Forms.ColumnHeader
        Me.objColhBSCActionId = New System.Windows.Forms.ColumnHeader
        Me.objColhGEActionId = New System.Windows.Forms.ColumnHeader
        Me.elLine = New eZee.Common.eZeeLine
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblAppraisalAction = New System.Windows.Forms.Label
        Me.objbtnSearch = New eZee.Common.eZeeGradientButton
        Me.lblGrade = New System.Windows.Forms.Label
        Me.lblTotalScoreT = New System.Windows.Forms.Label
        Me.lblTotalScoreF = New System.Windows.Forms.Label
        Me.cboAppraisalAction = New System.Windows.Forms.ComboBox
        Me.txtGrade_Award = New eZee.TextBox.AlphanumericTextBox
        Me.txtScoreTo = New eZee.TextBox.NumericTextBox
        Me.txtScoreFrom = New eZee.TextBox.NumericTextBox
        Me.txtPercentageDistribution = New eZee.TextBox.NumericTextBox
        Me.lblPercentageDistribution = New System.Windows.Forms.Label
        Me.dgcolhPctDistribution = New System.Windows.Forms.ColumnHeader
        Me.objFooter.SuspendLayout()
        Me.gbAppraisalSetup.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 363)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(792, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(580, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(683, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(11, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 440
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'gbAppraisalSetup
        '
        Me.gbAppraisalSetup.BorderColor = System.Drawing.Color.Black
        Me.gbAppraisalSetup.Checked = False
        Me.gbAppraisalSetup.CollapseAllExceptThis = False
        Me.gbAppraisalSetup.CollapsedHoverImage = Nothing
        Me.gbAppraisalSetup.CollapsedNormalImage = Nothing
        Me.gbAppraisalSetup.CollapsedPressedImage = Nothing
        Me.gbAppraisalSetup.CollapseOnLoad = False
        Me.gbAppraisalSetup.Controls.Add(Me.txtPercentageDistribution)
        Me.gbAppraisalSetup.Controls.Add(Me.objbtnBSCSearch)
        Me.gbAppraisalSetup.Controls.Add(Me.objbtnGEActionSearch)
        Me.gbAppraisalSetup.Controls.Add(Me.cboBSCAppraisalAction)
        Me.gbAppraisalSetup.Controls.Add(Me.cboGEAppraisalAction)
        Me.gbAppraisalSetup.Controls.Add(Me.lblBSCAppraisalAction)
        Me.gbAppraisalSetup.Controls.Add(Me.lblGEAppraisalAction)
        Me.gbAppraisalSetup.Controls.Add(Me.btnEdit)
        Me.gbAppraisalSetup.Controls.Add(Me.lvRatings)
        Me.gbAppraisalSetup.Controls.Add(Me.elLine)
        Me.gbAppraisalSetup.Controls.Add(Me.btnAdd)
        Me.gbAppraisalSetup.Controls.Add(Me.lblAppraisalAction)
        Me.gbAppraisalSetup.Controls.Add(Me.objbtnSearch)
        Me.gbAppraisalSetup.Controls.Add(Me.lblPercentageDistribution)
        Me.gbAppraisalSetup.Controls.Add(Me.lblGrade)
        Me.gbAppraisalSetup.Controls.Add(Me.lblTotalScoreT)
        Me.gbAppraisalSetup.Controls.Add(Me.lblTotalScoreF)
        Me.gbAppraisalSetup.Controls.Add(Me.cboAppraisalAction)
        Me.gbAppraisalSetup.Controls.Add(Me.txtGrade_Award)
        Me.gbAppraisalSetup.Controls.Add(Me.txtScoreTo)
        Me.gbAppraisalSetup.Controls.Add(Me.txtScoreFrom)
        Me.gbAppraisalSetup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAppraisalSetup.ExpandedHoverImage = Nothing
        Me.gbAppraisalSetup.ExpandedNormalImage = Nothing
        Me.gbAppraisalSetup.ExpandedPressedImage = Nothing
        Me.gbAppraisalSetup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAppraisalSetup.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAppraisalSetup.HeaderHeight = 25
        Me.gbAppraisalSetup.HeaderMessage = ""
        Me.gbAppraisalSetup.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAppraisalSetup.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAppraisalSetup.HeightOnCollapse = 0
        Me.gbAppraisalSetup.LeftTextSpace = 0
        Me.gbAppraisalSetup.Location = New System.Drawing.Point(0, 0)
        Me.gbAppraisalSetup.Name = "gbAppraisalSetup"
        Me.gbAppraisalSetup.OpenHeight = 300
        Me.gbAppraisalSetup.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAppraisalSetup.ShowBorder = True
        Me.gbAppraisalSetup.ShowCheckBox = False
        Me.gbAppraisalSetup.ShowCollapseButton = False
        Me.gbAppraisalSetup.ShowDefaultBorderColor = True
        Me.gbAppraisalSetup.ShowDownButton = False
        Me.gbAppraisalSetup.ShowHeader = True
        Me.gbAppraisalSetup.Size = New System.Drawing.Size(792, 363)
        Me.gbAppraisalSetup.TabIndex = 4
        Me.gbAppraisalSetup.Temp = 0
        Me.gbAppraisalSetup.Text = "Appraisal Ratings"
        Me.gbAppraisalSetup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnBSCSearch
        '
        Me.objbtnBSCSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnBSCSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnBSCSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnBSCSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnBSCSearch.BorderSelected = False
        Me.objbtnBSCSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnBSCSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnBSCSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnBSCSearch.Location = New System.Drawing.Point(761, 88)
        Me.objbtnBSCSearch.Name = "objbtnBSCSearch"
        Me.objbtnBSCSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnBSCSearch.TabIndex = 451
        '
        'objbtnGEActionSearch
        '
        Me.objbtnGEActionSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnGEActionSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnGEActionSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnGEActionSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnGEActionSearch.BorderSelected = False
        Me.objbtnGEActionSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnGEActionSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnGEActionSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnGEActionSearch.Location = New System.Drawing.Point(761, 61)
        Me.objbtnGEActionSearch.Name = "objbtnGEActionSearch"
        Me.objbtnGEActionSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnGEActionSearch.TabIndex = 450
        '
        'cboBSCAppraisalAction
        '
        Me.cboBSCAppraisalAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBSCAppraisalAction.DropDownWidth = 250
        Me.cboBSCAppraisalAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBSCAppraisalAction.FormattingEnabled = True
        Me.cboBSCAppraisalAction.Location = New System.Drawing.Point(483, 88)
        Me.cboBSCAppraisalAction.Name = "cboBSCAppraisalAction"
        Me.cboBSCAppraisalAction.Size = New System.Drawing.Size(272, 21)
        Me.cboBSCAppraisalAction.TabIndex = 449
        '
        'cboGEAppraisalAction
        '
        Me.cboGEAppraisalAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGEAppraisalAction.DropDownWidth = 250
        Me.cboGEAppraisalAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGEAppraisalAction.FormattingEnabled = True
        Me.cboGEAppraisalAction.Location = New System.Drawing.Point(483, 61)
        Me.cboGEAppraisalAction.Name = "cboGEAppraisalAction"
        Me.cboGEAppraisalAction.Size = New System.Drawing.Size(272, 21)
        Me.cboGEAppraisalAction.TabIndex = 448
        '
        'lblBSCAppraisalAction
        '
        Me.lblBSCAppraisalAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBSCAppraisalAction.Location = New System.Drawing.Point(312, 90)
        Me.lblBSCAppraisalAction.Name = "lblBSCAppraisalAction"
        Me.lblBSCAppraisalAction.Size = New System.Drawing.Size(165, 17)
        Me.lblBSCAppraisalAction.TabIndex = 447
        Me.lblBSCAppraisalAction.Text = "BSC Appraisal Actions"
        Me.lblBSCAppraisalAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGEAppraisalAction
        '
        Me.lblGEAppraisalAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGEAppraisalAction.Location = New System.Drawing.Point(312, 63)
        Me.lblGEAppraisalAction.Name = "lblGEAppraisalAction"
        Me.lblGEAppraisalAction.Size = New System.Drawing.Size(165, 17)
        Me.lblGEAppraisalAction.TabIndex = 446
        Me.lblGEAppraisalAction.Text = "Competency Appraisal Actions"
        Me.lblGEAppraisalAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(683, 117)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 444
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'lvRatings
        '
        Me.lvRatings.BackColorOnChecked = True
        Me.lvRatings.ColumnHeaders = Nothing
        Me.lvRatings.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhScoreFrom, Me.colhScoreTo, Me.colhGrade, Me.colhAction, Me.colhBSCAction, Me.colhGEAction, Me.objcolhGUID, Me.objColhActionId, Me.objColhBSCActionId, Me.objColhGEActionId, Me.dgcolhPctDistribution})
        Me.lvRatings.CompulsoryColumns = ""
        Me.lvRatings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvRatings.FullRowSelect = True
        Me.lvRatings.GridLines = True
        Me.lvRatings.GroupingColumn = Nothing
        Me.lvRatings.HideSelection = False
        Me.lvRatings.Location = New System.Drawing.Point(11, 170)
        Me.lvRatings.MinColumnWidth = 50
        Me.lvRatings.MultiSelect = False
        Me.lvRatings.Name = "lvRatings"
        Me.lvRatings.OptionalColumns = ""
        Me.lvRatings.ShowMoreItem = False
        Me.lvRatings.ShowSaveItem = False
        Me.lvRatings.ShowSelectAll = True
        Me.lvRatings.ShowSizeAllColumnsToFit = True
        Me.lvRatings.Size = New System.Drawing.Size(771, 180)
        Me.lvRatings.Sortable = True
        Me.lvRatings.TabIndex = 5
        Me.lvRatings.UseCompatibleStateImageBehavior = False
        Me.lvRatings.View = System.Windows.Forms.View.Details
        '
        'colhScoreFrom
        '
        Me.colhScoreFrom.Tag = "colhScoreFrom"
        Me.colhScoreFrom.Text = "Score From"
        Me.colhScoreFrom.Width = 80
        '
        'colhScoreTo
        '
        Me.colhScoreTo.Tag = "colhScoreTo"
        Me.colhScoreTo.Text = "Score To"
        Me.colhScoreTo.Width = 80
        '
        'colhGrade
        '
        Me.colhGrade.Tag = "colhGrade"
        Me.colhGrade.Text = "Grade/Award"
        Me.colhGrade.Width = 150
        '
        'colhAction
        '
        Me.colhAction.Tag = "colhAction"
        Me.colhAction.Text = "Overall Appraisal Action"
        Me.colhAction.Width = 130
        '
        'colhBSCAction
        '
        Me.colhBSCAction.DisplayIndex = 7
        Me.colhBSCAction.Text = "BSC Appraisal Action"
        Me.colhBSCAction.Width = 115
        '
        'colhGEAction
        '
        Me.colhGEAction.DisplayIndex = 6
        Me.colhGEAction.Text = "GE Appraisal Action"
        Me.colhGEAction.Width = 105
        '
        'objcolhGUID
        '
        Me.objcolhGUID.DisplayIndex = 4
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Width = 0
        '
        'objColhActionId
        '
        Me.objColhActionId.DisplayIndex = 5
        Me.objColhActionId.Tag = "objColhActionId"
        Me.objColhActionId.Text = ""
        Me.objColhActionId.Width = 0
        '
        'objColhBSCActionId
        '
        Me.objColhBSCActionId.Tag = "objColhBSCActionId"
        Me.objColhBSCActionId.Text = ""
        Me.objColhBSCActionId.Width = 0
        '
        'objColhGEActionId
        '
        Me.objColhGEActionId.Tag = "objColhGEActionId"
        Me.objColhGEActionId.Text = ""
        Me.objColhGEActionId.Width = 0
        '
        'elLine
        '
        Me.elLine.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elLine.Location = New System.Drawing.Point(13, 150)
        Me.elLine.Name = "elLine"
        Me.elLine.Size = New System.Drawing.Size(769, 17)
        Me.elLine.TabIndex = 442
        Me.elLine.Text = "Score(s)"
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(580, 117)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 441
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'lblAppraisalAction
        '
        Me.lblAppraisalAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppraisalAction.Location = New System.Drawing.Point(312, 36)
        Me.lblAppraisalAction.Name = "lblAppraisalAction"
        Me.lblAppraisalAction.Size = New System.Drawing.Size(165, 17)
        Me.lblAppraisalAction.TabIndex = 438
        Me.lblAppraisalAction.Text = "Overall Appraisal Actions"
        Me.lblAppraisalAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch.BorderSelected = False
        Me.objbtnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch.Location = New System.Drawing.Point(761, 34)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch.TabIndex = 437
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(8, 63)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(105, 17)
        Me.lblGrade.TabIndex = 7
        Me.lblGrade.Text = "Grade/Award"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalScoreT
        '
        Me.lblTotalScoreT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalScoreT.Location = New System.Drawing.Point(195, 36)
        Me.lblTotalScoreT.Name = "lblTotalScoreT"
        Me.lblTotalScoreT.Size = New System.Drawing.Size(42, 17)
        Me.lblTotalScoreT.TabIndex = 6
        Me.lblTotalScoreT.Text = "To"
        Me.lblTotalScoreT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalScoreF
        '
        Me.lblTotalScoreF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalScoreF.Location = New System.Drawing.Point(8, 36)
        Me.lblTotalScoreF.Name = "lblTotalScoreF"
        Me.lblTotalScoreF.Size = New System.Drawing.Size(105, 17)
        Me.lblTotalScoreF.TabIndex = 5
        Me.lblTotalScoreF.Text = "Overall Score From"
        Me.lblTotalScoreF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAppraisalAction
        '
        Me.cboAppraisalAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAppraisalAction.DropDownWidth = 250
        Me.cboAppraisalAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAppraisalAction.FormattingEnabled = True
        Me.cboAppraisalAction.Location = New System.Drawing.Point(483, 34)
        Me.cboAppraisalAction.Name = "cboAppraisalAction"
        Me.cboAppraisalAction.Size = New System.Drawing.Size(272, 21)
        Me.cboAppraisalAction.TabIndex = 4
        '
        'txtGrade_Award
        '
        Me.txtGrade_Award.Flags = 0
        Me.txtGrade_Award.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGrade_Award.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtGrade_Award.Location = New System.Drawing.Point(119, 61)
        Me.txtGrade_Award.Multiline = True
        Me.txtGrade_Award.Name = "txtGrade_Award"
        Me.txtGrade_Award.Size = New System.Drawing.Size(187, 48)
        Me.txtGrade_Award.TabIndex = 3
        '
        'txtScoreTo
        '
        Me.txtScoreTo.AllowNegative = True
        Me.txtScoreTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtScoreTo.DigitsInGroup = 0
        Me.txtScoreTo.Flags = 0
        Me.txtScoreTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScoreTo.Location = New System.Drawing.Point(243, 34)
        Me.txtScoreTo.MaxDecimalPlaces = 4
        Me.txtScoreTo.MaxWholeDigits = 9
        Me.txtScoreTo.Name = "txtScoreTo"
        Me.txtScoreTo.Prefix = ""
        Me.txtScoreTo.RangeMax = 1.7976931348623157E+308
        Me.txtScoreTo.RangeMin = -1.7976931348623157E+308
        Me.txtScoreTo.Size = New System.Drawing.Size(63, 21)
        Me.txtScoreTo.TabIndex = 2
        Me.txtScoreTo.Text = "0"
        Me.txtScoreTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtScoreFrom
        '
        Me.txtScoreFrom.AllowNegative = True
        Me.txtScoreFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtScoreFrom.DigitsInGroup = 0
        Me.txtScoreFrom.Flags = 0
        Me.txtScoreFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScoreFrom.Location = New System.Drawing.Point(119, 34)
        Me.txtScoreFrom.MaxDecimalPlaces = 4
        Me.txtScoreFrom.MaxWholeDigits = 9
        Me.txtScoreFrom.Name = "txtScoreFrom"
        Me.txtScoreFrom.Prefix = ""
        Me.txtScoreFrom.RangeMax = 1.7976931348623157E+308
        Me.txtScoreFrom.RangeMin = -1.7976931348623157E+308
        Me.txtScoreFrom.Size = New System.Drawing.Size(63, 21)
        Me.txtScoreFrom.TabIndex = 1
        Me.txtScoreFrom.Text = "0"
        Me.txtScoreFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPercentageDistribution
        '
        Me.txtPercentageDistribution.AllowNegative = True
        Me.txtPercentageDistribution.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPercentageDistribution.DigitsInGroup = 0
        Me.txtPercentageDistribution.Flags = 0
        Me.txtPercentageDistribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercentageDistribution.Location = New System.Drawing.Point(119, 115)
        Me.txtPercentageDistribution.MaxDecimalPlaces = 4
        Me.txtPercentageDistribution.MaxWholeDigits = 9
        Me.txtPercentageDistribution.Name = "txtPercentageDistribution"
        Me.txtPercentageDistribution.Prefix = ""
        Me.txtPercentageDistribution.RangeMax = 1.7976931348623157E+308
        Me.txtPercentageDistribution.RangeMin = -1.7976931348623157E+308
        Me.txtPercentageDistribution.Size = New System.Drawing.Size(63, 21)
        Me.txtPercentageDistribution.TabIndex = 453
        Me.txtPercentageDistribution.Text = "0"
        Me.txtPercentageDistribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPercentageDistribution
        '
        Me.lblPercentageDistribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentageDistribution.Location = New System.Drawing.Point(8, 117)
        Me.lblPercentageDistribution.Name = "lblPercentageDistribution"
        Me.lblPercentageDistribution.Size = New System.Drawing.Size(105, 17)
        Me.lblPercentageDistribution.TabIndex = 7
        Me.lblPercentageDistribution.Text = "% Distribution"
        Me.lblPercentageDistribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgcolhPctDistribution
        '
        Me.dgcolhPctDistribution.DisplayIndex = 3
        Me.dgcolhPctDistribution.Tag = "dgcolhPctDistribution"
        Me.dgcolhPctDistribution.Text = "% Distribution"
        Me.dgcolhPctDistribution.Width = 80
        '
        'frmAppraisalSetups
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 418)
        Me.Controls.Add(Me.gbAppraisalSetup)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAppraisalSetups"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Appraisal Setup"
        Me.objFooter.ResumeLayout(False)
        Me.gbAppraisalSetup.ResumeLayout(False)
        Me.gbAppraisalSetup.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbAppraisalSetup As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboAppraisalAction As System.Windows.Forms.ComboBox
    Friend WithEvents txtGrade_Award As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtScoreTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtScoreFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTotalScoreT As System.Windows.Forms.Label
    Friend WithEvents lblTotalScoreF As System.Windows.Forms.Label
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents objbtnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents lblAppraisalAction As System.Windows.Forms.Label
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents elLine As eZee.Common.eZeeLine
    Friend WithEvents lvRatings As eZee.Common.eZeeListView
    Friend WithEvents colhScoreFrom As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhScoreTo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGrade As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAction As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents objColhActionId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnBSCSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnGEActionSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboBSCAppraisalAction As System.Windows.Forms.ComboBox
    Friend WithEvents cboGEAppraisalAction As System.Windows.Forms.ComboBox
    Friend WithEvents lblBSCAppraisalAction As System.Windows.Forms.Label
    Friend WithEvents lblGEAppraisalAction As System.Windows.Forms.Label
    Friend WithEvents colhGEAction As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBSCAction As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhBSCActionId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhGEActionId As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtPercentageDistribution As eZee.TextBox.NumericTextBox
    Friend WithEvents lblPercentageDistribution As System.Windows.Forms.Label
    Friend WithEvents dgcolhPctDistribution As System.Windows.Forms.ColumnHeader
End Class

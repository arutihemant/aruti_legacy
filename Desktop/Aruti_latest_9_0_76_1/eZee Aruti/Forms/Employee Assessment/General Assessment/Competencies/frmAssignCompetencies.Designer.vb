﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssignCompetencies
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssignCompetencies))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbAssignCompetencies = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkZeroCompetenceWeight = New System.Windows.Forms.CheckBox
        Me.chkAssignByJobCompetencies = New System.Windows.Forms.CheckBox
        Me.txtWeight = New eZee.TextBox.NumericTextBox
        Me.lblWeight = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.objpnlData = New System.Windows.Forms.Panel
        Me.gbDescription = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.lnkApplyAction = New System.Windows.Forms.LinkLabel
        Me.picStayView = New System.Windows.Forms.PictureBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhTree = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhCompetencies = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhScale = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhWeight = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhLink = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAssignMasterId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAssignTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsPGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhcompetenciesunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboGroup = New System.Windows.Forms.ComboBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblGroupCode = New System.Windows.Forms.Label
        Me.objbtnSearchGroup = New eZee.Common.eZeeGradientButton
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.EZeeGradientButton1 = New eZee.Common.eZeeGradientButton
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblComptenceGroup = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboComptenceGroup = New System.Windows.Forms.ComboBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.gbAssignCompetencies.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objpnlData.SuspendLayout()
        Me.gbDescription.SuspendLayout()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbAssignCompetencies)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(913, 566)
        Me.pnlMain.TabIndex = 0
        '
        'gbAssignCompetencies
        '
        Me.gbAssignCompetencies.BorderColor = System.Drawing.Color.Black
        Me.gbAssignCompetencies.Checked = False
        Me.gbAssignCompetencies.CollapseAllExceptThis = False
        Me.gbAssignCompetencies.CollapsedHoverImage = Nothing
        Me.gbAssignCompetencies.CollapsedNormalImage = Nothing
        Me.gbAssignCompetencies.CollapsedPressedImage = Nothing
        Me.gbAssignCompetencies.CollapseOnLoad = False
        Me.gbAssignCompetencies.Controls.Add(Me.chkZeroCompetenceWeight)
        Me.gbAssignCompetencies.Controls.Add(Me.chkAssignByJobCompetencies)
        Me.gbAssignCompetencies.Controls.Add(Me.txtWeight)
        Me.gbAssignCompetencies.Controls.Add(Me.lblWeight)
        Me.gbAssignCompetencies.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbAssignCompetencies.Controls.Add(Me.objbtnReset)
        Me.gbAssignCompetencies.Controls.Add(Me.cboEmployee)
        Me.gbAssignCompetencies.Controls.Add(Me.lblEmployee)
        Me.gbAssignCompetencies.Controls.Add(Me.objbtnSearch)
        Me.gbAssignCompetencies.Controls.Add(Me.objLine1)
        Me.gbAssignCompetencies.Controls.Add(Me.objpnlData)
        Me.gbAssignCompetencies.Controls.Add(Me.lblPeriod)
        Me.gbAssignCompetencies.Controls.Add(Me.cboGroup)
        Me.gbAssignCompetencies.Controls.Add(Me.cboPeriod)
        Me.gbAssignCompetencies.Controls.Add(Me.lblGroupCode)
        Me.gbAssignCompetencies.Controls.Add(Me.objbtnSearchGroup)
        Me.gbAssignCompetencies.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAssignCompetencies.ExpandedHoverImage = Nothing
        Me.gbAssignCompetencies.ExpandedNormalImage = Nothing
        Me.gbAssignCompetencies.ExpandedPressedImage = Nothing
        Me.gbAssignCompetencies.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssignCompetencies.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssignCompetencies.HeaderHeight = 25
        Me.gbAssignCompetencies.HeaderMessage = ""
        Me.gbAssignCompetencies.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAssignCompetencies.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssignCompetencies.HeightOnCollapse = 0
        Me.gbAssignCompetencies.LeftTextSpace = 0
        Me.gbAssignCompetencies.Location = New System.Drawing.Point(0, 0)
        Me.gbAssignCompetencies.Name = "gbAssignCompetencies"
        Me.gbAssignCompetencies.OpenHeight = 300
        Me.gbAssignCompetencies.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssignCompetencies.ShowBorder = True
        Me.gbAssignCompetencies.ShowCheckBox = False
        Me.gbAssignCompetencies.ShowCollapseButton = False
        Me.gbAssignCompetencies.ShowDefaultBorderColor = True
        Me.gbAssignCompetencies.ShowDownButton = False
        Me.gbAssignCompetencies.ShowHeader = True
        Me.gbAssignCompetencies.Size = New System.Drawing.Size(913, 511)
        Me.gbAssignCompetencies.TabIndex = 242
        Me.gbAssignCompetencies.Temp = 0
        Me.gbAssignCompetencies.Text = "Competence Info."
        Me.gbAssignCompetencies.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkZeroCompetenceWeight
        '
        Me.chkZeroCompetenceWeight.BackColor = System.Drawing.Color.Transparent
        Me.chkZeroCompetenceWeight.Checked = True
        Me.chkZeroCompetenceWeight.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkZeroCompetenceWeight.Location = New System.Drawing.Point(539, 4)
        Me.chkZeroCompetenceWeight.Name = "chkZeroCompetenceWeight"
        Me.chkZeroCompetenceWeight.Size = New System.Drawing.Size(318, 17)
        Me.chkZeroCompetenceWeight.TabIndex = 371
        Me.chkZeroCompetenceWeight.Text = "Allow Competence Weight to be Zero"
        Me.chkZeroCompetenceWeight.UseVisualStyleBackColor = False
        '
        'chkAssignByJobCompetencies
        '
        Me.chkAssignByJobCompetencies.BackColor = System.Drawing.Color.Transparent
        Me.chkAssignByJobCompetencies.Checked = True
        Me.chkAssignByJobCompetencies.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAssignByJobCompetencies.Location = New System.Drawing.Point(619, 4)
        Me.chkAssignByJobCompetencies.Name = "chkAssignByJobCompetencies"
        Me.chkAssignByJobCompetencies.Size = New System.Drawing.Size(238, 17)
        Me.chkAssignByJobCompetencies.TabIndex = 369
        Me.chkAssignByJobCompetencies.Text = "Assign By Job Competencies"
        Me.chkAssignByJobCompetencies.UseVisualStyleBackColor = False
        Me.chkAssignByJobCompetencies.Visible = False
        '
        'txtWeight
        '
        Me.txtWeight.AllowNegative = False
        Me.txtWeight.BackColor = System.Drawing.Color.White
        Me.txtWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtWeight.DigitsInGroup = 0
        Me.txtWeight.Flags = 65536
        Me.txtWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeight.Location = New System.Drawing.Point(850, 35)
        Me.txtWeight.MaxDecimalPlaces = 2
        Me.txtWeight.MaxWholeDigits = 9
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Prefix = ""
        Me.txtWeight.RangeMax = 1.7976931348623157E+308
        Me.txtWeight.RangeMin = -1.7976931348623157E+308
        Me.txtWeight.ReadOnly = True
        Me.txtWeight.Size = New System.Drawing.Size(51, 21)
        Me.txtWeight.TabIndex = 241
        Me.txtWeight.Text = "0.00"
        Me.txtWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(798, 38)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(46, 15)
        Me.lblWeight.TabIndex = 240
        Me.lblWeight.Text = "Weight"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(258, 35)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 365
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(886, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 246
        Me.objbtnReset.TabStop = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(80, 35)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(172, 21)
        Me.cboEmployee.TabIndex = 367
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(12, 37)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(62, 17)
        Me.lblEmployee.TabIndex = 366
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(863, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 245
        Me.objbtnSearch.TabStop = False
        '
        'objLine1
        '
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(12, 62)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(889, 3)
        Me.objLine1.TabIndex = 243
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'objpnlData
        '
        Me.objpnlData.Controls.Add(Me.gbDescription)
        Me.objpnlData.Controls.Add(Me.picStayView)
        Me.objpnlData.Controls.Add(Me.dgvData)
        Me.objpnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlData.Location = New System.Drawing.Point(12, 68)
        Me.objpnlData.Name = "objpnlData"
        Me.objpnlData.Size = New System.Drawing.Size(889, 437)
        Me.objpnlData.TabIndex = 242
        '
        'gbDescription
        '
        Me.gbDescription.BorderColor = System.Drawing.Color.Black
        Me.gbDescription.Checked = False
        Me.gbDescription.CollapseAllExceptThis = False
        Me.gbDescription.CollapsedHoverImage = Nothing
        Me.gbDescription.CollapsedNormalImage = Nothing
        Me.gbDescription.CollapsedPressedImage = Nothing
        Me.gbDescription.CollapseOnLoad = False
        Me.gbDescription.Controls.Add(Me.txtRemark)
        Me.gbDescription.Controls.Add(Me.lnkApplyAction)
        Me.gbDescription.ExpandedHoverImage = Nothing
        Me.gbDescription.ExpandedNormalImage = Nothing
        Me.gbDescription.ExpandedPressedImage = Nothing
        Me.gbDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDescription.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDescription.HeaderHeight = 25
        Me.gbDescription.HeaderMessage = ""
        Me.gbDescription.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbDescription.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDescription.HeightOnCollapse = 0
        Me.gbDescription.LeftTextSpace = 0
        Me.gbDescription.Location = New System.Drawing.Point(275, 97)
        Me.gbDescription.Name = "gbDescription"
        Me.gbDescription.OpenHeight = 300
        Me.gbDescription.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDescription.ShowBorder = True
        Me.gbDescription.ShowCheckBox = False
        Me.gbDescription.ShowCollapseButton = False
        Me.gbDescription.ShowDefaultBorderColor = True
        Me.gbDescription.ShowDownButton = False
        Me.gbDescription.ShowHeader = True
        Me.gbDescription.Size = New System.Drawing.Size(359, 181)
        Me.gbDescription.TabIndex = 455
        Me.gbDescription.Temp = 0
        Me.gbDescription.Text = "Description"
        Me.gbDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbDescription.Visible = False
        '
        'txtRemark
        '
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.Location = New System.Drawing.Point(1, 25)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(357, 155)
        Me.txtRemark.TabIndex = 460
        '
        'lnkApplyAction
        '
        Me.lnkApplyAction.BackColor = System.Drawing.Color.Transparent
        Me.lnkApplyAction.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkApplyAction.Location = New System.Drawing.Point(333, 4)
        Me.lnkApplyAction.Name = "lnkApplyAction"
        Me.lnkApplyAction.Size = New System.Drawing.Size(21, 17)
        Me.lnkApplyAction.TabIndex = 453
        Me.lnkApplyAction.TabStop = True
        Me.lnkApplyAction.Text = "X"
        Me.lnkApplyAction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picStayView
        '
        Me.picStayView.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.picStayView.Location = New System.Drawing.Point(863, 411)
        Me.picStayView.Name = "picStayView"
        Me.picStayView.Size = New System.Drawing.Size(23, 23)
        Me.picStayView.TabIndex = 237
        Me.picStayView.TabStop = False
        Me.picStayView.Visible = False
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhTree, Me.objdgcolhCheck, Me.dgcolhCompetencies, Me.dgcolhScale, Me.dgcolhWeight, Me.dgcolhLink, Me.objdgcolhIsGrp, Me.objdgcolhGrpId, Me.objdgcolhAssignMasterId, Me.objdgcolhAssignTranId, Me.objdgcolhGUID, Me.objdgcolhPGrpId, Me.objdgcolhIsPGrp, Me.objdgcolhcompetenciesunkid})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.Size = New System.Drawing.Size(889, 437)
        Me.dgvData.TabIndex = 236
        '
        'objdgcolhTree
        '
        Me.objdgcolhTree.HeaderText = ""
        Me.objdgcolhTree.Name = "objdgcolhTree"
        Me.objdgcolhTree.ReadOnly = True
        Me.objdgcolhTree.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhTree.Width = 25
        '
        'objdgcolhCheck
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.NullValue = "False"
        Me.objdgcolhCheck.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhCompetencies
        '
        Me.dgcolhCompetencies.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhCompetencies.HeaderText = "Competencies"
        Me.dgcolhCompetencies.Name = "dgcolhCompetencies"
        Me.dgcolhCompetencies.ReadOnly = True
        Me.dgcolhCompetencies.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhScale
        '
        Me.dgcolhScale.HeaderText = "Scale/Score Group"
        Me.dgcolhScale.Name = "dgcolhScale"
        Me.dgcolhScale.ReadOnly = True
        Me.dgcolhScale.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhScale.Visible = False
        Me.dgcolhScale.Width = 120
        '
        'dgcolhWeight
        '
        Me.dgcolhWeight.AllowNegative = False
        Me.dgcolhWeight.DecimalLength = 2
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "F2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.dgcolhWeight.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhWeight.HeaderText = "Weight"
        Me.dgcolhWeight.Name = "dgcolhWeight"
        Me.dgcolhWeight.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhWeight.ShowNullWhenZero = True
        Me.dgcolhWeight.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhWeight.Width = 80
        '
        'dgcolhLink
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dgcolhLink.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhLink.HeaderText = "Info."
        Me.dgcolhLink.Name = "dgcolhLink"
        Me.dgcolhLink.ReadOnly = True
        Me.dgcolhLink.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhLink.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.ReadOnly = True
        Me.objdgcolhGrpId.Visible = False
        '
        'objdgcolhAssignMasterId
        '
        Me.objdgcolhAssignMasterId.HeaderText = "dgcolhAssignMasterId"
        Me.objdgcolhAssignMasterId.Name = "objdgcolhAssignMasterId"
        Me.objdgcolhAssignMasterId.Visible = False
        '
        'objdgcolhAssignTranId
        '
        Me.objdgcolhAssignTranId.HeaderText = "dgcolhAssignTranId"
        Me.objdgcolhAssignTranId.Name = "objdgcolhAssignTranId"
        Me.objdgcolhAssignTranId.Visible = False
        '
        'objdgcolhGUID
        '
        Me.objdgcolhGUID.HeaderText = "objdgcolhGUID"
        Me.objdgcolhGUID.Name = "objdgcolhGUID"
        Me.objdgcolhGUID.Visible = False
        '
        'objdgcolhPGrpId
        '
        Me.objdgcolhPGrpId.HeaderText = "objdgcolhPGrpId"
        Me.objdgcolhPGrpId.Name = "objdgcolhPGrpId"
        Me.objdgcolhPGrpId.ReadOnly = True
        Me.objdgcolhPGrpId.Visible = False
        '
        'objdgcolhIsPGrp
        '
        Me.objdgcolhIsPGrp.HeaderText = "objdgcolhIsPGrp"
        Me.objdgcolhIsPGrp.Name = "objdgcolhIsPGrp"
        Me.objdgcolhIsPGrp.ReadOnly = True
        Me.objdgcolhIsPGrp.Visible = False
        '
        'objdgcolhcompetenciesunkid
        '
        Me.objdgcolhcompetenciesunkid.HeaderText = "objdgcolhcompetenciesunkid"
        Me.objdgcolhcompetenciesunkid.Name = "objdgcolhcompetenciesunkid"
        Me.objdgcolhcompetenciesunkid.Visible = False
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(594, 38)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(47, 15)
        Me.lblPeriod.TabIndex = 24
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGroup
        '
        Me.cboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGroup.DropDownWidth = 300
        Me.cboGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGroup.FormattingEnabled = True
        Me.cboGroup.Location = New System.Drawing.Point(370, 35)
        Me.cboGroup.Name = "cboGroup"
        Me.cboGroup.Size = New System.Drawing.Size(191, 21)
        Me.cboGroup.TabIndex = 27
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(647, 35)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(142, 21)
        Me.cboPeriod.TabIndex = 25
        '
        'lblGroupCode
        '
        Me.lblGroupCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroupCode.Location = New System.Drawing.Point(285, 38)
        Me.lblGroupCode.Name = "lblGroupCode"
        Me.lblGroupCode.Size = New System.Drawing.Size(79, 15)
        Me.lblGroupCode.TabIndex = 26
        Me.lblGroupCode.Text = "Assess. Group"
        Me.lblGroupCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchGroup
        '
        Me.objbtnSearchGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGroup.BorderSelected = False
        Me.objbtnSearchGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGroup.Location = New System.Drawing.Point(567, 35)
        Me.objbtnSearchGroup.Name = "objbtnSearchGroup"
        Me.objbtnSearchGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGroup.TabIndex = 235
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.EZeeGradientButton1)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.lblComptenceGroup)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.cboComptenceGroup)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 511)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(913, 55)
        Me.objFooter.TabIndex = 3
        '
        'EZeeGradientButton1
        '
        Me.EZeeGradientButton1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeGradientButton1.BackColor1 = System.Drawing.Color.Transparent
        Me.EZeeGradientButton1.BackColor2 = System.Drawing.Color.Transparent
        Me.EZeeGradientButton1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.EZeeGradientButton1.BorderSelected = False
        Me.EZeeGradientButton1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.EZeeGradientButton1.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.EZeeGradientButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.EZeeGradientButton1.Location = New System.Drawing.Point(395, 19)
        Me.EZeeGradientButton1.Name = "EZeeGradientButton1"
        Me.EZeeGradientButton1.Size = New System.Drawing.Size(21, 21)
        Me.EZeeGradientButton1.TabIndex = 371
        Me.EZeeGradientButton1.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(701, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblComptenceGroup
        '
        Me.lblComptenceGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComptenceGroup.Location = New System.Drawing.Point(12, 21)
        Me.lblComptenceGroup.Name = "lblComptenceGroup"
        Me.lblComptenceGroup.Size = New System.Drawing.Size(105, 17)
        Me.lblComptenceGroup.TabIndex = 370
        Me.lblComptenceGroup.Text = "Competence Group"
        Me.lblComptenceGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblComptenceGroup.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(804, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'cboComptenceGroup
        '
        Me.cboComptenceGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboComptenceGroup.DropDownWidth = 200
        Me.cboComptenceGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboComptenceGroup.FormattingEnabled = True
        Me.cboComptenceGroup.Location = New System.Drawing.Point(120, 19)
        Me.cboComptenceGroup.Name = "cboComptenceGroup"
        Me.cboComptenceGroup.Size = New System.Drawing.Size(269, 21)
        Me.cboComptenceGroup.TabIndex = 369
        Me.cboComptenceGroup.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 25
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Competencies"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Scale/Score Group"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "objdgcolhGrpId"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "dgcolhAssignMasterId"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "dgcolhAssignTranId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhGUID"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhGUID"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhPGrpId"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhIsPGrp"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'frmAssignCompetencies
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(913, 566)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssignCompetencies"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assign Competencies"
        Me.pnlMain.ResumeLayout(False)
        Me.gbAssignCompetencies.ResumeLayout(False)
        Me.gbAssignCompetencies.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objpnlData.ResumeLayout(False)
        Me.gbDescription.ResumeLayout(False)
        Me.gbDescription.PerformLayout()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroupCode As System.Windows.Forms.Label
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnSearchGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents txtWeight As eZee.TextBox.NumericTextBox
    Friend WithEvents gbAssignCompetencies As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objpnlData As System.Windows.Forms.Panel
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents picStayView As System.Windows.Forms.PictureBox
    Friend WithEvents gbDescription As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents lnkApplyAction As System.Windows.Forms.LinkLabel
    Friend WithEvents EZeeGradientButton1 As eZee.Common.eZeeGradientButton
    Friend WithEvents lblComptenceGroup As System.Windows.Forms.Label
    Friend WithEvents cboComptenceGroup As System.Windows.Forms.ComboBox
    Friend WithEvents chkAssignByJobCompetencies As System.Windows.Forms.CheckBox
    Friend WithEvents objdgcolhTree As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhCompetencies As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhScale As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhWeight As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhLink As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAssignMasterId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAssignTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsPGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhcompetenciesunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkZeroCompetenceWeight As System.Windows.Forms.CheckBox
End Class

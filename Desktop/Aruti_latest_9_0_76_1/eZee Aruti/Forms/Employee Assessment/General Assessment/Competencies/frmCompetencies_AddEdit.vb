﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCompetencies_AddEdit

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmCompetencies_AddEdit"
    Private mblnCancel As Boolean = True
    Private objCompetencies As clsassess_competencies_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintCompetenciesUnkid As Integer = -1
    Private mintOldPeriodUnkid As Integer = -1

    'S.SANDEEP |29-MAR-2021| -- START
    'ISSUE/ENHANCEMENT : Competencies Training
    Private objCmptTraining As clsassess_competencies_trainingcourses_tran
    Private mdtCourses As DataTable
    'S.SANDEEP |29-MAR-2021| -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintCompetenciesUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintCompetenciesUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboCategory.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
            cboScore.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            'S.SANDEEP [21 NOV 2015] -- START
            cboComptenceGroup.BackColor = GUI.ColorOptional
            'S.SANDEEP [21 NOV 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objCompetencies._Periodunkid = CInt(cboPeriod.SelectedValue)
            objCompetencies._Competence_Categoryunkid = CInt(cboCategory.SelectedValue)
            objCompetencies._Description = txtDescription.Text
            objCompetencies._Isactive = True
            objCompetencies._Name = txtName.Text
            objCompetencies._Scalemasterunkid = CInt(cboScore.SelectedValue)
            'S.SANDEEP [21 NOV 2015] -- START
            objCompetencies._CompetenceGroupUnkid = CInt(cboComptenceGroup.SelectedValue)
            'S.SANDEEP [21 NOV 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboPeriod.SelectedValue = objCompetencies._Periodunkid
            mintOldPeriodUnkid = objCompetencies._Periodunkid
            cboCategory.SelectedValue = objCompetencies._Competence_Categoryunkid
            txtDescription.Text = objCompetencies._Description
            txtName.Text = objCompetencies._Name
            cboScore.SelectedValue = objCompetencies._Scalemasterunkid
            'S.SANDEEP [21 NOV 2015] -- START
            cboComptenceGroup.SelectedValue = objCompetencies._CompetenceGroupUnkid
            'S.SANDEEP [21 NOV 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objCMaster As New clsCommon_Master
        Dim objCPeriod As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES, True, "List")
            With cboCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)

            'S.SANDEEP |02-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            Dim blnIsCompetence As Boolean = False
            If ConfigParameter._Object._RunCompetenceAssessmentSeparately Then
                blnIsCompetence = True
            End If
            dsCombo = objCPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open, False, blnIsCompetence, CBool(IIf(blnIsCompetence = True, True, False)))        
            'S.SANDEEP |02-MAR-2021| -- END

            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

            'S.SANDEEP [21 NOV 2015] -- START
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_GROUP, True, "List")
            With cboComptenceGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [21 NOV 2015] -- END

            'S.SANDEEP |29-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Training
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingCourse
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP |29-MAR-2021| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objCMaster = Nothing : objCPeriod = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Competence Category is mandatory information. Please select Competence Category to continue."), enMsgBoxStyle.Information)
                cboCategory.Focus()
                Return False
            End If

            If txtName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Competence is mandatory information. Please provide Competence to continue."), enMsgBoxStyle.Information)
                txtName.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP |29-MAR-2021| -- START
    'ISSUE/ENHANCEMENT : Competencies Training
    Private Sub FillGrid()
        Dim xView As DataView
        Try
            xView = New DataView(mdtCourses, "AUD <> 'D'", "", DataViewRowState.CurrentRows)

            dgvData.AutoGenerateColumns = False

            dgcolhTrainingCourse.DataPropertyName = "cname"
            objdgColhTranId.DataPropertyName = "trainingcoursetranunkid"
            objdgcolhguid.DataPropertyName = "GUID"

            dgvData.DataSource = xView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub RowOperation(ByVal eType As enAuditType, ByVal iRow As DataRow, ByVal strVoidReason As String)
        Try
            Select Case eType
                Case enAuditType.ADD
                    Dim xRow As DataRow = mdtCourses.NewRow

                    xRow("trainingcoursetranunkid") = -1
                    xRow("competenciesunkid") = mintCompetenciesUnkid
                    xRow("masterunkid") = CInt(cboTrainingCourse.SelectedValue)
                    xRow("isvoid") = False
                    xRow("voiduserunkid") = -1
                    xRow("voiddatetime") = DBNull.Value
                    xRow("voidreason") = ""
                    xRow("AUD") = "A"
                    xRow("GUID") = Guid.NewGuid.ToString()
                    xRow("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                    xRow("audituserunkid") = User._Object._Userunkid
                    xRow("audittype") = eType
                    xRow("ip") = getIP()
                    xRow("host") = getHostName()
                    xRow("formname") = mstrModuleName
                    xRow("isweb") = False
                    xRow("cname") = cboTrainingCourse.Text

                    mdtCourses.Rows.Add(xRow)

                Case enAuditType.DELETE

                    iRow("trainingcoursetranunkid") = iRow("trainingcoursetranunkid")
                    iRow("isvoid") = True
                    iRow("voiduserunkid") = User._Object._Userunkid
                    iRow("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    iRow("voidreason") = strVoidReason
                    iRow("AUD") = "D"
                    iRow("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                    iRow("audituserunkid") = User._Object._Userunkid
                    iRow("audittype") = eType
                    iRow("ip") = getIP()
                    iRow("host") = getHostName()
                    iRow("formname") = mstrModuleName
                    iRow("isweb") = False

            End Select
            mdtCourses.AcceptChanges()
            FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "RowOperation", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |29-MAR-2021| -- END

#End Region

#Region " Form's Event "

    Private Sub frmCompetencies_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCompetencies = New clsassess_competencies_master
        'S.SANDEEP |29-MAR-2021| -- START
        'ISSUE/ENHANCEMENT : Competencies Training
        objCmptTraining = New clsassess_competencies_trainingcourses_tran
        mdtCourses = New DataTable
        'S.SANDEEP |29-MAR-2021| -- END
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            If menAction = enAction.EDIT_ONE Then
                objCompetencies._Competenciesunkid = mintCompetenciesUnkid
                cboCategory.Enabled = False : objbtnAddCategory.Enabled = False : objbtnSearchCategory.Enabled = False
                'S.SANDEEP [21 NOV 2015] -- START
                cboComptenceGroup.Enabled = False : objbtnAddGroup.Enabled = False : objbtnSearchGroup.Enabled = False
                'S.SANDEEP [21 NOV 2015] -- END
            End If
            Call FillCombo()
            Call GetValue()
            'S.SANDEEP |29-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Training
            objCmptTraining._Competenciesunkid = mintCompetenciesUnkid
            mdtCourses = objCmptTraining._DataTable
            FillGrid()
            'S.SANDEEP |29-MAR-2021| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCompetencies_AddEdit_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmCompetencies_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCompetencies_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCompetencies_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCompetencies_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCompetencies_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCompetencies = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_competencies_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_competencies_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub
            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                'IF PERIOD IS CHANGED IT SHOULD COPY TO SELECTED PERIOD -- BASED ON ANDREW'S REQUEST
                If mintOldPeriodUnkid <> CInt(cboPeriod.SelectedValue) Then
                    'S.SANDEEP |29-MAR-2021| -- START
                    'ISSUE/ENHANCEMENT : Competencies Training
                    '        blnFlag = objCompetencies.Insert()
                    '    Else
                    '        blnFlag = objCompetencies.Update()
                    '    End If
                    'Else
                    '    blnFlag = objCompetencies.Insert()
                    blnFlag = objCompetencies.Insert(mdtCourses)
                Else
                    blnFlag = objCompetencies.Update(mdtCourses)
                End If
            Else
                blnFlag = objCompetencies.Insert(mdtCourses)
                'S.SANDEEP |29-MAR-2021| -- END                    
            End If

            If blnFlag = False And objCompetencies._Message <> "" Then
                eZeeMsgBox.Show(objCompetencies._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objCompetencies = Nothing
                    objCompetencies = New clsassess_competencies_master
                    cboCategory.Tag = CInt(cboCategory.SelectedValue)
                    cboPeriod.Tag = CInt(cboPeriod.SelectedValue)
                    cboScore.Tag = CInt(cboScore.SelectedValue)
                    'S.SANDEEP [21 NOV 2015] -- START
                    cboComptenceGroup.Tag = CInt(cboComptenceGroup.SelectedValue)
                    'S.SANDEEP [21 NOV 2015] -- END
                    Call GetValue()
                    cboPeriod.SelectedValue = CInt(cboPeriod.Tag)
                    cboCategory.SelectedValue = CInt(cboCategory.Tag)
                    cboScore.SelectedValue = CInt(cboScore.Tag)
                    'S.SANDEEP [21 NOV 2015] -- START
                    cboComptenceGroup.SelectedValue = CInt(cboComptenceGroup.Tag)
                    'S.SANDEEP [21 NOV 2015] -- END

                    'S.SANDEEP |29-MAR-2021| -- START
                    'ISSUE/ENHANCEMENT : Competencies Training
                    objCmptTraining._Competenciesunkid = -1
                    mdtCourses = objCmptTraining._DataTable.Clone
                    FillGrid()
                    'S.SANDEEP |29-MAR-2021| -- END

                    txtName.Focus()
                Else
                    mintCompetenciesUnkid = objCompetencies._Competenciesunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            Call objFrmLangPopup.displayDialog(txtName.Text, objCompetencies._Name1, objCompetencies._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCategory.Click
        Dim frm As New frmCommonMaster
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim dsCombos As New DataSet
            Dim iRefId As Integer = -1
            frm.displayDialog(iRefId, clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES, enAction.ADD_ONE)
            If iRefId > 0 Then
                Dim objCMaster As New clsCommon_Master
                dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES, True, "List")
                With cboCategory
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                    .SelectedValue = iRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboCategory.ValueMember
                .DisplayMember = cboCategory.DisplayMember
                .DataSource = CType(cboCategory.DataSource, DataTable)
                If .DisplayDialog Then
                    cboCategory.SelectedValue = .SelectedValue
                    cboCategory.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchScore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchScore.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboScore.ValueMember
                .DisplayMember = cboScore.DisplayMember
                .DataSource = CType(cboScore.DataSource, DataTable)
                If .DisplayDialog Then
                    cboScore.SelectedValue = .SelectedValue
                    cboScore.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchScore_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'S.SANDEEP [21 NOV 2015] -- START
    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Dim frm As New frmCommonMaster
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim dsCombos As New DataSet
            Dim iRefId As Integer = -1
            frm.displayDialog(iRefId, clsCommon_Master.enCommonMaster.COMPETENCE_GROUP, enAction.ADD_ONE)
            If iRefId > 0 Then
                Dim objCMaster As New clsCommon_Master
                dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_GROUP, True, "List")
                With cboComptenceGroup
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                    .SelectedValue = iRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGroup.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboComptenceGroup.ValueMember
                .DisplayMember = cboComptenceGroup.DisplayMember
                .DataSource = CType(cboComptenceGroup.DataSource, DataTable)
                If .DisplayDialog Then
                    cboComptenceGroup.SelectedValue = .SelectedValue
                    cboComptenceGroup.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [21 NOV 2015] -- END

    'S.SANDEEP |29-MAR-2021| -- START
    'ISSUE/ENHANCEMENT : Competencies Training
    Private Sub objbtnAddTrainingCourse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddTrainingCourse.Click
        Dim frm As New frmCommonMaster
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Dim dsCombos As New DataSet
            Dim iRefId As Integer = -1
            frm.displayDialog(iRefId, clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, enAction.ADD_ONE)
            If iRefId > 0 Then
                Dim objCMaster As New clsCommon_Master
                dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                With cboTrainingCourse
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                    .SelectedValue = iRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddTrainingCourse_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchTrainingCourse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTrainingCourse.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboTrainingCourse.ValueMember
                .DisplayMember = cboTrainingCourse.DisplayMember
                .DataSource = CType(cboTrainingCourse.DataSource, DataTable)
                If .DisplayDialog Then
                    cboTrainingCourse.SelectedValue = .SelectedValue
                    cboTrainingCourse.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTrainingCourse_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdd.Click
        Try
            If mdtCourses IsNot Nothing AndAlso mdtCourses.Rows.Count > 0 Then
                If mdtCourses.AsEnumerable().Where(Function(x) x.Field(Of Integer)("masterunkid") = CInt(cboTrainingCourse.SelectedValue) AndAlso x.Field(Of String)("AUD") <> "D").Count() > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot add same training course again. Please add new course to continue."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            RowOperation(enAuditType.ADD, Nothing, "")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |29-MAR-2021| -- END

#End Region

#Region " Controls Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet : Dim objAScale As New clsAssessment_Scale
            dsList = objAScale.getComboList_ScaleGroup(CInt(cboPeriod.SelectedValue), "List", True)
            With cboScore
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhRemove.Index Then
                Dim iVoidReason As String = "" : Dim iRow() As DataRow = Nothing
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure you want to delete this added training course?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    If CInt(dgvData.Rows(e.RowIndex).Cells(objdgColhTranId.Index).Value) > 0 Then
                        Dim frm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        iRow = mdtCourses.Select("trainingcoursetranunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgColhTranId.Index).Value) & "'")
                    Else
                        iRow = mdtCourses.Select("GUID = '" & dgvData.Rows(e.RowIndex).Cells(objdgcolhguid.Index).Value.ToString & "'")
                    End If
                    RowOperation(enAuditType.DELETE, iRow(0), iVoidReason)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.tabpCompetenciesInfo.Text = Language._Object.getCaption(Me.tabpCompetenciesInfo.Name, Me.tabpCompetenciesInfo.Text)
            Me.tabpTrainingInfo.Text = Language._Object.getCaption(Me.tabpTrainingInfo.Name, Me.tabpTrainingInfo.Text)
            Me.lblComptenceGroup.Text = Language._Object.getCaption(Me.lblComptenceGroup.Name, Me.lblComptenceGroup.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.tabpName.Text = Language._Object.getCaption(Me.tabpName.Name, Me.tabpName.Text)
            Me.tabpDescription.Text = Language._Object.getCaption(Me.tabpDescription.Name, Me.tabpDescription.Text)
			Me.lblScore.Text = Language._Object.getCaption(Me.lblScore.Name, Me.lblScore.Text)
			Me.lblCategory.Text = Language._Object.getCaption(Me.lblCategory.Name, Me.lblCategory.Text)
            Me.lblTrainingCourse.Text = Language._Object.getCaption(Me.lblTrainingCourse.Name, Me.lblTrainingCourse.Text)
            Me.dgcolhTrainingCourse.HeaderText = Language._Object.getCaption(Me.dgcolhTrainingCourse.Name, Me.dgcolhTrainingCourse.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Competence Category is mandatory information. Please select Competence Category to continue.")
			Language.setMessage(mstrModuleName, 2, "Competence is mandatory information. Please provide Competence to continue.")
            Language.setMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 4, "Sorry, you cannot add same training course again. Please add new course to continue.")
            Language.setMessage(mstrModuleName, 5, "Are you sure you want to delete this added training course?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGroupList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGroupList))
        Me.pnlAssessmentGroupList = New System.Windows.Forms.Panel
        Me.lvAssessmentGroup = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhAllocation = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.objcolhTranId = New System.Windows.Forms.ColumnHeader
        Me.colhWeight = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblValue = New System.Windows.Forms.Label
        Me.cboMode = New System.Windows.Forms.ComboBox
        Me.lblFilterMode = New System.Windows.Forms.Label
        Me.radEmployee = New System.Windows.Forms.RadioButton
        Me.radAllocation = New System.Windows.Forms.RadioButton
        Me.objbtnSearchAllocation = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.cboAllocation = New System.Windows.Forms.ComboBox
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.gbAllocation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.radTeam = New System.Windows.Forms.RadioButton
        Me.radUnitGroup = New System.Windows.Forms.RadioButton
        Me.radJobs = New System.Windows.Forms.RadioButton
        Me.radJobGroup = New System.Windows.Forms.RadioButton
        Me.radUnit = New System.Windows.Forms.RadioButton
        Me.radSectionGroup = New System.Windows.Forms.RadioButton
        Me.radDepartmentGroup = New System.Windows.Forms.RadioButton
        Me.radSection = New System.Windows.Forms.RadioButton
        Me.radDepartment = New System.Windows.Forms.RadioButton
        Me.radBranch = New System.Windows.Forms.RadioButton
        Me.chkDelete = New System.Windows.Forms.CheckBox
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlAssessmentGroupList.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbAllocation.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlAssessmentGroupList
        '
        Me.pnlAssessmentGroupList.Controls.Add(Me.lvAssessmentGroup)
        Me.pnlAssessmentGroupList.Controls.Add(Me.gbFilterCriteria)
        Me.pnlAssessmentGroupList.Controls.Add(Me.objFooter)
        Me.pnlAssessmentGroupList.Controls.Add(Me.eZeeHeader)
        Me.pnlAssessmentGroupList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlAssessmentGroupList.Location = New System.Drawing.Point(0, 0)
        Me.pnlAssessmentGroupList.Name = "pnlAssessmentGroupList"
        Me.pnlAssessmentGroupList.Size = New System.Drawing.Size(768, 426)
        Me.pnlAssessmentGroupList.TabIndex = 2
        '
        'lvAssessmentGroup
        '
        Me.lvAssessmentGroup.BackColorOnChecked = False
        Me.lvAssessmentGroup.ColumnHeaders = Nothing
        Me.lvAssessmentGroup.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhAllocation, Me.colhCode, Me.colhName, Me.colhDescription, Me.objcolhTranId, Me.colhWeight})
        Me.lvAssessmentGroup.CompulsoryColumns = ""
        Me.lvAssessmentGroup.FullRowSelect = True
        Me.lvAssessmentGroup.GridLines = True
        Me.lvAssessmentGroup.GroupingColumn = Nothing
        Me.lvAssessmentGroup.HideSelection = False
        Me.lvAssessmentGroup.Location = New System.Drawing.Point(12, 138)
        Me.lvAssessmentGroup.MinColumnWidth = 50
        Me.lvAssessmentGroup.MultiSelect = False
        Me.lvAssessmentGroup.Name = "lvAssessmentGroup"
        Me.lvAssessmentGroup.OptionalColumns = ""
        Me.lvAssessmentGroup.ShowMoreItem = False
        Me.lvAssessmentGroup.ShowSaveItem = False
        Me.lvAssessmentGroup.ShowSelectAll = True
        Me.lvAssessmentGroup.ShowSizeAllColumnsToFit = True
        Me.lvAssessmentGroup.Size = New System.Drawing.Size(744, 227)
        Me.lvAssessmentGroup.Sortable = True
        Me.lvAssessmentGroup.TabIndex = 11
        Me.lvAssessmentGroup.UseCompatibleStateImageBehavior = False
        Me.lvAssessmentGroup.View = System.Windows.Forms.View.Details
        '
        'objcolhAllocation
        '
        Me.objcolhAllocation.Tag = "objcolhAllocation"
        Me.objcolhAllocation.Text = ""
        Me.objcolhAllocation.Width = 0
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 150
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 250
        '
        'colhDescription
        '
        Me.colhDescription.DisplayIndex = 4
        Me.colhDescription.Tag = "colhDescription"
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 250
        '
        'objcolhTranId
        '
        Me.objcolhTranId.DisplayIndex = 5
        Me.objcolhTranId.Tag = "objcolhTranId"
        Me.objcolhTranId.Text = ""
        Me.objcolhTranId.Width = 0
        '
        'colhWeight
        '
        Me.colhWeight.DisplayIndex = 3
        Me.colhWeight.Tag = "colhWeight"
        Me.colhWeight.Text = "Weight"
        Me.colhWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhWeight.Width = 90
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objlblValue)
        Me.gbFilterCriteria.Controls.Add(Me.cboMode)
        Me.gbFilterCriteria.Controls.Add(Me.lblFilterMode)
        Me.gbFilterCriteria.Controls.Add(Me.radEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.radAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.txtName)
        Me.gbFilterCriteria.Controls.Add(Me.lblName)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objlblCaption)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 73
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(744, 68)
        Me.gbFilterCriteria.TabIndex = 5
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblValue
        '
        Me.objlblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblValue.Location = New System.Drawing.Point(11, 35)
        Me.objlblValue.Name = "objlblValue"
        Me.objlblValue.Size = New System.Drawing.Size(68, 16)
        Me.objlblValue.TabIndex = 225
        Me.objlblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMode
        '
        Me.cboMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMode.FormattingEnabled = True
        Me.cboMode.Location = New System.Drawing.Point(85, 33)
        Me.cboMode.Name = "cboMode"
        Me.cboMode.Size = New System.Drawing.Size(128, 21)
        Me.cboMode.TabIndex = 224
        '
        'lblFilterMode
        '
        Me.lblFilterMode.BackColor = System.Drawing.Color.Transparent
        Me.lblFilterMode.Location = New System.Drawing.Point(439, 4)
        Me.lblFilterMode.Name = "lblFilterMode"
        Me.lblFilterMode.Size = New System.Drawing.Size(49, 17)
        Me.lblFilterMode.TabIndex = 223
        Me.lblFilterMode.Text = "Mode"
        Me.lblFilterMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radEmployee
        '
        Me.radEmployee.BackColor = System.Drawing.Color.Transparent
        Me.radEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEmployee.Location = New System.Drawing.Point(593, 4)
        Me.radEmployee.Name = "radEmployee"
        Me.radEmployee.Size = New System.Drawing.Size(93, 17)
        Me.radEmployee.TabIndex = 222
        Me.radEmployee.TabStop = True
        Me.radEmployee.Text = "Employee"
        Me.radEmployee.UseVisualStyleBackColor = False
        '
        'radAllocation
        '
        Me.radAllocation.BackColor = System.Drawing.Color.Transparent
        Me.radAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAllocation.Location = New System.Drawing.Point(494, 4)
        Me.radAllocation.Name = "radAllocation"
        Me.radAllocation.Size = New System.Drawing.Size(93, 17)
        Me.radAllocation.TabIndex = 221
        Me.radAllocation.TabStop = True
        Me.radAllocation.Text = "Allocation"
        Me.radAllocation.UseVisualStyleBackColor = False
        '
        'objbtnSearchAllocation
        '
        Me.objbtnSearchAllocation.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAllocation.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAllocation.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAllocation.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAllocation.BorderSelected = False
        Me.objbtnSearchAllocation.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAllocation.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAllocation.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAllocation.Location = New System.Drawing.Point(473, 33)
        Me.objbtnSearchAllocation.Name = "objbtnSearchAllocation"
        Me.objbtnSearchAllocation.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAllocation.TabIndex = 219
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(717, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 6
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(692, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 5
        Me.objbtnSearch.TabStop = False
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(556, 33)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(178, 21)
        Me.txtName.TabIndex = 11
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(500, 35)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(50, 16)
        Me.lblName.TabIndex = 10
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocation
        '
        Me.cboAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation.DropDownWidth = 300
        Me.cboAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation.FormattingEnabled = True
        Me.cboAllocation.Location = New System.Drawing.Point(322, 33)
        Me.cboAllocation.Name = "cboAllocation"
        Me.cboAllocation.Size = New System.Drawing.Size(145, 21)
        Me.cboAllocation.TabIndex = 9
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(219, 36)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(97, 15)
        Me.objlblCaption.TabIndex = 8
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.gbAllocation)
        Me.objFooter.Controls.Add(Me.chkDelete)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 371)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(768, 55)
        Me.objFooter.TabIndex = 3
        '
        'gbAllocation
        '
        Me.gbAllocation.BorderColor = System.Drawing.Color.Black
        Me.gbAllocation.Checked = False
        Me.gbAllocation.CollapseAllExceptThis = False
        Me.gbAllocation.CollapsedHoverImage = Nothing
        Me.gbAllocation.CollapsedNormalImage = Nothing
        Me.gbAllocation.CollapsedPressedImage = Nothing
        Me.gbAllocation.CollapseOnLoad = False
        Me.gbAllocation.Controls.Add(Me.Panel1)
        Me.gbAllocation.ExpandedHoverImage = Nothing
        Me.gbAllocation.ExpandedNormalImage = Nothing
        Me.gbAllocation.ExpandedPressedImage = Nothing
        Me.gbAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAllocation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAllocation.HeaderHeight = 25
        Me.gbAllocation.HeaderMessage = ""
        Me.gbAllocation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAllocation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAllocation.HeightOnCollapse = 0
        Me.gbAllocation.LeftTextSpace = 0
        Me.gbAllocation.Location = New System.Drawing.Point(12, 36)
        Me.gbAllocation.Name = "gbAllocation"
        Me.gbAllocation.OpenHeight = 300
        Me.gbAllocation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAllocation.ShowBorder = True
        Me.gbAllocation.ShowCheckBox = False
        Me.gbAllocation.ShowCollapseButton = False
        Me.gbAllocation.ShowDefaultBorderColor = True
        Me.gbAllocation.ShowDownButton = False
        Me.gbAllocation.ShowHeader = True
        Me.gbAllocation.Size = New System.Drawing.Size(71, 10)
        Me.gbAllocation.TabIndex = 12
        Me.gbAllocation.Temp = 0
        Me.gbAllocation.Text = "Allocation"
        Me.gbAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbAllocation.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.radTeam)
        Me.Panel1.Controls.Add(Me.radUnitGroup)
        Me.Panel1.Controls.Add(Me.radJobs)
        Me.Panel1.Controls.Add(Me.radJobGroup)
        Me.Panel1.Controls.Add(Me.radUnit)
        Me.Panel1.Controls.Add(Me.radSectionGroup)
        Me.Panel1.Controls.Add(Me.radDepartmentGroup)
        Me.Panel1.Controls.Add(Me.radSection)
        Me.Panel1.Controls.Add(Me.radDepartment)
        Me.Panel1.Controls.Add(Me.radBranch)
        Me.Panel1.Location = New System.Drawing.Point(2, 26)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(180, 10)
        Me.Panel1.TabIndex = 1
        '
        'radTeam
        '
        Me.radTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radTeam.Location = New System.Drawing.Point(12, 172)
        Me.radTeam.Name = "radTeam"
        Me.radTeam.Size = New System.Drawing.Size(156, 17)
        Me.radTeam.TabIndex = 16
        Me.radTeam.TabStop = True
        Me.radTeam.Text = "Team"
        Me.radTeam.UseVisualStyleBackColor = True
        '
        'radUnitGroup
        '
        Me.radUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radUnitGroup.Location = New System.Drawing.Point(12, 126)
        Me.radUnitGroup.Name = "radUnitGroup"
        Me.radUnitGroup.Size = New System.Drawing.Size(156, 17)
        Me.radUnitGroup.TabIndex = 15
        Me.radUnitGroup.TabStop = True
        Me.radUnitGroup.Text = "Unit Group"
        Me.radUnitGroup.UseVisualStyleBackColor = True
        '
        'radJobs
        '
        Me.radJobs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radJobs.Location = New System.Drawing.Point(12, 218)
        Me.radJobs.Name = "radJobs"
        Me.radJobs.Size = New System.Drawing.Size(156, 17)
        Me.radJobs.TabIndex = 14
        Me.radJobs.TabStop = True
        Me.radJobs.Text = "Job"
        Me.radJobs.UseVisualStyleBackColor = True
        '
        'radJobGroup
        '
        Me.radJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radJobGroup.Location = New System.Drawing.Point(12, 195)
        Me.radJobGroup.Name = "radJobGroup"
        Me.radJobGroup.Size = New System.Drawing.Size(156, 17)
        Me.radJobGroup.TabIndex = 13
        Me.radJobGroup.TabStop = True
        Me.radJobGroup.Text = "Job Group"
        Me.radJobGroup.UseVisualStyleBackColor = True
        '
        'radUnit
        '
        Me.radUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radUnit.Location = New System.Drawing.Point(12, 149)
        Me.radUnit.Name = "radUnit"
        Me.radUnit.Size = New System.Drawing.Size(156, 17)
        Me.radUnit.TabIndex = 12
        Me.radUnit.TabStop = True
        Me.radUnit.Text = "Unit"
        Me.radUnit.UseVisualStyleBackColor = True
        '
        'radSectionGroup
        '
        Me.radSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSectionGroup.Location = New System.Drawing.Point(12, 80)
        Me.radSectionGroup.Name = "radSectionGroup"
        Me.radSectionGroup.Size = New System.Drawing.Size(156, 17)
        Me.radSectionGroup.TabIndex = 11
        Me.radSectionGroup.TabStop = True
        Me.radSectionGroup.Text = "Section Group"
        Me.radSectionGroup.UseVisualStyleBackColor = True
        '
        'radDepartmentGroup
        '
        Me.radDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDepartmentGroup.Location = New System.Drawing.Point(12, 34)
        Me.radDepartmentGroup.Name = "radDepartmentGroup"
        Me.radDepartmentGroup.Size = New System.Drawing.Size(156, 17)
        Me.radDepartmentGroup.TabIndex = 10
        Me.radDepartmentGroup.TabStop = True
        Me.radDepartmentGroup.Text = "Department Group"
        Me.radDepartmentGroup.UseVisualStyleBackColor = True
        '
        'radSection
        '
        Me.radSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSection.Location = New System.Drawing.Point(12, 103)
        Me.radSection.Name = "radSection"
        Me.radSection.Size = New System.Drawing.Size(156, 17)
        Me.radSection.TabIndex = 9
        Me.radSection.TabStop = True
        Me.radSection.Text = "Section"
        Me.radSection.UseVisualStyleBackColor = True
        '
        'radDepartment
        '
        Me.radDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDepartment.Location = New System.Drawing.Point(12, 57)
        Me.radDepartment.Name = "radDepartment"
        Me.radDepartment.Size = New System.Drawing.Size(156, 17)
        Me.radDepartment.TabIndex = 8
        Me.radDepartment.TabStop = True
        Me.radDepartment.Text = "Department"
        Me.radDepartment.UseVisualStyleBackColor = True
        '
        'radBranch
        '
        Me.radBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radBranch.Location = New System.Drawing.Point(12, 11)
        Me.radBranch.Name = "radBranch"
        Me.radBranch.Size = New System.Drawing.Size(156, 17)
        Me.radBranch.TabIndex = 7
        Me.radBranch.TabStop = True
        Me.radBranch.Text = "Branch"
        Me.radBranch.UseVisualStyleBackColor = True
        '
        'chkDelete
        '
        Me.chkDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDelete.Location = New System.Drawing.Point(12, 13)
        Me.chkDelete.Name = "chkDelete"
        Me.chkDelete.Size = New System.Drawing.Size(71, 17)
        Me.chkDelete.TabIndex = 3
        Me.chkDelete.Text = "Delete Assessment group with selected allocation."
        Me.chkDelete.UseVisualStyleBackColor = True
        Me.chkDelete.Visible = False
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(350, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 130
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(453, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 129
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(556, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 128
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(659, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(768, 58)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Assessment Group List"
        '
        'frmGroupList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(768, 426)
        Me.Controls.Add(Me.pnlAssessmentGroupList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGroupList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assessment Group List"
        Me.pnlAssessmentGroupList.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbAllocation.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlAssessmentGroupList As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lvAssessmentGroup As eZee.Common.eZeeListView
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhAllocation As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchAllocation As eZee.Common.eZeeGradientButton
    Friend WithEvents gbAllocation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents radSectionGroup As System.Windows.Forms.RadioButton
    Friend WithEvents radDepartmentGroup As System.Windows.Forms.RadioButton
    Friend WithEvents radSection As System.Windows.Forms.RadioButton
    Friend WithEvents radDepartment As System.Windows.Forms.RadioButton
    Friend WithEvents radBranch As System.Windows.Forms.RadioButton
    Friend WithEvents radTeam As System.Windows.Forms.RadioButton
    Friend WithEvents radUnitGroup As System.Windows.Forms.RadioButton
    Friend WithEvents radJobs As System.Windows.Forms.RadioButton
    Friend WithEvents radJobGroup As System.Windows.Forms.RadioButton
    Friend WithEvents radUnit As System.Windows.Forms.RadioButton
    Friend WithEvents chkDelete As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhTranId As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhWeight As System.Windows.Forms.ColumnHeader
    Friend WithEvents radEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents radAllocation As System.Windows.Forms.RadioButton
    Friend WithEvents lblFilterMode As System.Windows.Forms.Label
    Friend WithEvents objlblValue As System.Windows.Forms.Label
    Friend WithEvents cboMode As System.Windows.Forms.ComboBox
End Class

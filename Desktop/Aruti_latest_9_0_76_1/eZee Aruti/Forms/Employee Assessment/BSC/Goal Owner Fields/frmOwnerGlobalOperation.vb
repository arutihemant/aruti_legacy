﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmOwnerGlobalOperation

#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmOwnerGlobalOperation"
    Private objassess_owrfield1Mst As clsassess_owrfield1_master
    Private mintTotalCount As Integer = 0
    Dim drRow() As DataRow = Nothing
    Dim iUnlockComments As String = ""
    Dim mstrAllocation As String = ""
#End Region

#Region "Private Method(S)"

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

            cboOperation.Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
            cboOperation.Items.Add(Language.getMessage(mstrModuleName, 2, "Commit"))
            cboOperation.Items.Add(Language.getMessage(mstrModuleName, 3, "Uncommit"))
            cboOperation.SelectedIndex = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = CType(dgvData.DataSource, DataTable).Select("ischeck = true")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgvData.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgvData.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub FillGrid()
        Dim dsList As DataSet = Nothing
        Try
            dgvData.AutoGenerateColumns = False
            dsList = objassess_owrfield1Mst.GetGlobalOwnerList(CInt(cboPeriod.SelectedValue), IIf(CInt(cboOperation.SelectedIndex) = 1, enObjective_Status.OPEN_CHANGES, enObjective_Status.FINAL_COMMITTED))

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Columns.Contains("Message") = False Then
                dsList.Tables(0).Columns.Add("Message", Type.GetType("System.String"))
            End If

            objcolhCheck.DataPropertyName = "ischeck"
            colhAllocation.DataPropertyName = "Owner"
            colhWeight.DataPropertyName = "Weight"
            dgcolhError.DataPropertyName = "Message"
            dgvData.DataSource = dsList.Tables(0)


            If dsList.Tables(0).Rows.Count > 0 Then
                colhAllocation.HeaderText = dsList.Tables(0).Rows(0)("OwnerCategory").ToString()
            ElseIf dsList.Tables(0).Rows.Count <= 0 Then
                colhAllocation.HeaderText = mstrAllocation
            End If


            objbtnSearch.ShowResult(dgvData.RowCount)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmOwrFieldsList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objassess_owrfield1Mst = New clsassess_owrfield1_master

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()
            objlblValue.Text = ""
            mstrAllocation = colhAllocation.HeaderText
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOwrFieldsList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_owrfield1_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_owrfield1_master"
            objfrm.displayDialog(Me)

            SetMessages()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub


#End Region

#Region "Button's Events"

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Period is compulsory information.Please select period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboPeriod.Focus()
                Exit Sub
            End If

            If CInt(cboOperation.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Operation is compulsory information.Please select Operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboOperation.Focus()
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            cboOperation.SelectedIndex = 0
            chkSelectAll.Checked = False
            dgvData.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Try
            If dgvData.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, there is no goal defined in order to") & " " & cboOperation.Text & ".", enMsgBoxStyle.Information)
                Exit Sub
            Else
                Dim dtTable As DataTable = Nothing

                If CType(dgvData.DataSource, DataTable) IsNot Nothing AndAlso CType(dgvData.DataSource, DataTable).Rows.Count > 0 Then
                    drRow = CType(dgvData.DataSource, DataTable).Select("ischeck = True")
                    If drRow.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry in order to do commit operation please select following things Owner."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    mintTotalCount = drRow.Length
                    Dim iMsgText As String = ""

                    Select Case ConfigParameter._Object._CascadingTypeId
                        Case enPACascading.STRICT_CASCADING, enPACascading.LOOSE_CASCADING
                            If CInt(cboOperation.SelectedIndex) = 1 Then 'Commit
                                iMsgText = Language.getMessage(mstrModuleName, 7, "You are about to commit the owner goals for the selected period. This will tranfer all goals to employee goals.") & vbCrLf & _
                                             Language.getMessage(mstrModuleName, 8, "Due to this you will not be allowed to do any operation(s) on owner goals.") & vbCrLf & _
                                             Language.getMessage(mstrModuleName, 14, "Do you wish to continue?")

                            ElseIf CInt(cboOperation.SelectedIndex) = 2 Then 'UnCommit
                                iMsgText = Language.getMessage(mstrModuleName, 10, "You are about to unlock the owner goals. This will result in opening of all operation on owner goals.") & vbCrLf & _
                                         Language.getMessage(mstrModuleName, 14, "Do you wish to continue?")

                            End If

                        Case Else
                            If CInt(cboOperation.SelectedIndex) = 1 Then 'Commit
                                iMsgText = Language.getMessage(mstrModuleName, 11, "You are about to commit the owner goals for the selected period.") & vbCrLf & _
                                             Language.getMessage(mstrModuleName, 8, "Due to this you will not be allowed to do any operation(s) on owner goals.") & vbCrLf & _
                                            Language.getMessage(mstrModuleName, 14, "Do you wish to continue?")

                            ElseIf CInt(cboOperation.SelectedIndex) = 2 Then 'UnCommit
                                iMsgText = Language.getMessage(mstrModuleName, 10, "You are about to unlock the owner goals. This will result in opening of all operation on owner goals.") & vbCrLf & _
                                       Language.getMessage(mstrModuleName, 14, "Do you wish to continue?")

                            End If

                    End Select


                    If eZeeMsgBox.Show(iMsgText, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

                    If CInt(cboOperation.SelectedIndex) = 2 Then 'UnCommit
                        iUnlockComments = InputBox(Language.getMessage(mstrModuleName, 17, "Enter reason to uncommit"), Me.Text)

                        If iUnlockComments.Trim.Length <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, Unlock reason is mandaroty information."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If

                    Me.Cursor = Cursors.WaitCursor

                    Application.DoEvents()
                    objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & "0/" & drRow.Length.ToString

                    Me.SuspendLayout()
                    RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

                    objbgWorker.RunWorkerAsync()

                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnProcess_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Checkbox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If dgvData.DataSource Is Nothing Then Exit Sub
            Dim drRow() As DataRow = CType(dgvData.DataSource, DataTable).Select("1=1")
            If drRow.Length > 0 Then
                For Each dr In drRow
                    dr("ischeck") = chkSelectAll.Checked
                    dr.AcceptChanges()
                Next
            End If
            CType(dgvData.DataSource, DataTable).AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Datagrid Event"

    Private Sub dgvData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objcolhCheck.Index Then

                If dgvData.IsCurrentCellDirty Then
                    dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                CType(dgvData.DataSource, DataTable).AcceptChanges()
                SetCheckBoxValue()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboOperation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOperation.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            chkSelectAll.Checked = False
            dgvData.DataSource = Nothing
            btnProcess.Enabled = True
            objlblValue.Text = ""
            objbtnSearch.ShowResult(dgvData.RowCount)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOperation_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Background Work Event"

    Private Sub bgWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgWorker.DoWork
        Try
            Me.ControlBox = False
            Me.Enabled = False

            Dim xCount As Integer = 0
            Dim iMsgText As String = ""
            Dim objFMapping As New clsAssess_Field_Mapping


            For Each dr As DataRow In drRow
                Try
                    dgvData.FirstDisplayedScrollingRowIndex = CType(dgvData.DataSource, DataTable).Rows.IndexOf(dr) - 5
                    Application.DoEvents()
                Catch ex As Exception
                End Try

                iMsgText = ""
                iMsgText = objFMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, CInt(cboPeriod.SelectedValue), CInt(dr("ownerunkid")), 0)
                If iMsgText.Trim.Length > 0 Then
                    dr("Message") = iMsgText
                    xCount += 1
                    objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                    Continue For
                End If

                Select Case ConfigParameter._Object._CascadingTypeId

                    Case enPACascading.STRICT_CASCADING, enPACascading.LOOSE_CASCADING

                        If objassess_owrfield1Mst.Transfer_Goals_ToEmployee(FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, CInt(dr("ownerunkid")), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, User._Object._Userunkid, True, Nothing) Then
                            If objassess_owrfield1Mst.Update_Status(IIf(CInt(cboOperation.SelectedIndex) = 1, enObjective_Status.FINAL_COMMITTED, enObjective_Status.OPEN_CHANGES), iUnlockComments, User._Object._Userunkid, CInt(cboPeriod.SelectedValue), CInt(dr("ownerunkid"))) Then
                                dr("Message") = cboOperation.Text & " " & Language.getMessage(mstrModuleName, 12, "Succesfully.")
                                xCount += 1
                                objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                            Else
                                dr("Message") = objassess_owrfield1Mst._Message
                                xCount += 1
                                objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                                Continue For
                            End If
                        Else
                            dr("Message") = objassess_owrfield1Mst._Message
                            xCount += 1
                            objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                            Continue For
                        End If

                    Case Else
                        If objassess_owrfield1Mst.Update_Status(IIf(CInt(cboOperation.SelectedIndex) = 1, enObjective_Status.FINAL_COMMITTED, enObjective_Status.OPEN_CHANGES), iUnlockComments, User._Object._Userunkid, CInt(cboPeriod.SelectedValue), CInt(dr("ownerunkid"))) Then
                            dr("Message") = cboOperation.Text & " " & Language.getMessage(mstrModuleName, 12, "Succesfully.")
                            xCount += 1
                            objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                        Else
                            dr("Message") = objassess_owrfield1Mst._Message
                            xCount += 1
                            objlblValue.Text = Language.getMessage(mstrModuleName, 15, "Processing : ") & (xCount) & "/" & drRow.Length.ToString
                            Continue For
                        End If
                End Select
            Next

            If objFMapping IsNot Nothing Then objFMapping = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgWorker_DoWork", mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Sub bgWorker_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgWorker.ProgressChanged
    '    Try
    '        objlblValue.Text = "[ " & e.ProgressPercentage.ToString & " / " & mintTotalCount & " ]"
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "bgWorker_ProgressChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub bgWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgWorker.RunWorkerCompleted
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            Else
                If objassess_owrfield1Mst._Message <> "" Then
                    eZeeMsgBox.Show(objassess_owrfield1Mst._Message, enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(cboOperation.Text & " " & Language.getMessage(mstrModuleName, 16, "Operation completed successfully."), enMsgBoxStyle.Information)
                    btnProcess.Enabled = False
                End If
                objlblValue.Text = ""
            End If
            Me.ControlBox = True
            Me.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgWorker_RunWorkerCompleted", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnProcess.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblOperation.Text = Language._Object.getCaption(Me.lblOperation.Name, Me.lblOperation.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.Name, Me.btnProcess.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.colhAllocation.HeaderText = Language._Object.getCaption(Me.colhAllocation.Name, Me.colhAllocation.HeaderText)
            Me.colhWeight.HeaderText = Language._Object.getCaption(Me.colhWeight.Name, Me.colhWeight.HeaderText)
            Me.dgcolhError.HeaderText = Language._Object.getCaption(Me.dgcolhError.Name, Me.dgcolhError.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Commit")
            Language.setMessage(mstrModuleName, 3, "Uncommit")
            Language.setMessage(mstrModuleName, 4, "Operation is compulsory information.Please select Operation.")
            Language.setMessage(mstrModuleName, 5, "Sorry, there is no goal defined in order to")
            Language.setMessage(mstrModuleName, 6, "Sorry in order to do commit operation please select following things Owner.")
            Language.setMessage(mstrModuleName, 7, "You are about to commit the owner goals for the selected period. This will tranfer all goals to employee goals.")
            Language.setMessage(mstrModuleName, 8, "Due to this you will not be allowed to do any operation(s) on owner goals.")
            Language.setMessage(mstrModuleName, 10, "You are about to unlock the owner goals. This will result in opening of all operation on owner goals.")
            Language.setMessage(mstrModuleName, 11, "You are about to commit the owner goals for the selected period.")
            Language.setMessage(mstrModuleName, 12, "Succesfully.")
            Language.setMessage(mstrModuleName, 13, "Period is compulsory information.Please select period.")
            Language.setMessage(mstrModuleName, 14, "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 15, "Processing :")
            Language.setMessage(mstrModuleName, 16, "Operation completed successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmBSCAssessorAssessmentList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmBSCAssessorAssessmentList"
    Private objBSCAssessor As New clsBSC_Analysis_Master
    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mStrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 05 MARCH 2012 ] -- END
#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        'S.SANDEEP [ 13 NOV 2013 ] -- START
        'Dim objAssessor As New clsAssessor
        Dim objAssessor As New clsBSC_Analysis_Master
        'S.SANDEEP [ 13 NOV 2013 ] -- END
        Dim objPeriod As New clscommom_period_Tran
        Dim objYear As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Dim mstrAssessorIds As String = String.Empty
        'Dim dtTable As DataTable
        Try
            'S.SANDEEP [ 13 NOV 2013 ] -- START
            'dsCombo = objAssessor.GetList("Assessor", False, True)
            'With cboAssessor
            '    .ValueMember = "assessormasterunkid"
            '    .DisplayMember = "assessorname"
            '    .DataSource = dsCombo.Tables("Assessor")
            '    .SelectedValue = 0
            'End With
            dsCombo = objAssessor.getAssessorComboList("Assessor", True, False, User._Object._Userunkid)
            With cboAssessor
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Assessor")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 13 NOV 2013 ] -- END

            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes

            'For Each dtRow As DataRow In dsCombo.Tables("Assessor").Rows
            '    If CInt(dtRow.Item("employeeunkid")) = 0 Then Continue For
            '    mstrAssessorIds &= "," & dtRow.Item("employeeunkid").ToString
            'Next
            'If mstrAssessorIds.Length > 0 Then
            '    mstrAssessorIds = Mid(mstrAssessorIds, 2)
            '    If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '        dsCombo = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            '    Else
            '        dsCombo = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            '    End If

            '    dtTable = New DataView(dsCombo.Tables("Emp"), "employeeunkid NOT IN( " & mstrAssessorIds & " )", "", DataViewRowState.CurrentRows).ToTable
            '    With cboEmployee
            '        .ValueMember = "employeeunkid"
            '        .DisplayMember = "employeename"
            '        .DataSource = dtTable
            '        .SelectedValue = 0
            '    End With
            'End If

            'Pinkal (5-MAY-2012) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objYear.getComboListPAYYEAR("Year", True, , , , True)
            dsCombo = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            cboYear.ValueMember = "Id"
            cboYear.DisplayMember = "name"
            cboYear.DataSource = dsCombo.Tables("Year")

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "Period", True, 0)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            'Sohail (21 Aug 2015) -- End
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DisplayMember = "name"
            cboPeriod.DataSource = dsCombo.Tables("Period")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objAssessor = Nothing
            objPeriod = Nothing
            objYear = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim lvItem As ListViewItem
        Dim dtTable As DataTable
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'If User._Object.Privilege._AllowToViewAssessorAssessedBSCList = True Then                'Pinkal (02-Jul-2012) -- Start
            'S.SANDEEP [28 MAY 2015] -- END



            dsList = objBSCAssessor.GetList("List", enAssessmentMode.APPRAISER_ASSESSMENT)


            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim dsPercent As New DataSet
            Dim blnFlag As Boolean = False
            dsPercent = objBSCAssessor.Get_Percentage(enAssessmentMode.APPRAISER_ASSESSMENT)
            If dsPercent.Tables(0).Rows.Count > 0 Then
                blnFlag = True
            End If
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND assessedemployeeunkid = " & CInt(cboEmployee.SelectedValue)
            End If

            'S.SANDEEP [ 14 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If CInt(cboAssessor.SelectedValue) > 0 Then
            '    StrSearching &= "AND assessoremployeeunkid = " & CInt(cboAssessor.SelectedValue)
            'End If
            If CInt(cboAssessor.SelectedValue) > 0 Then
                StrSearching &= "AND assessormasterunkid = " & CInt(cboAssessor.SelectedValue)
            End If
            'S.SANDEEP [ 14 JUNE 2012 ] -- END

            If CInt(cboYear.SelectedValue) > 0 Then
                StrSearching &= "AND yearunkid = " & CInt(cboYear.SelectedValue)
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND periodunkid = " & CInt(cboPeriod.SelectedValue)
            End If

            If dtpAssessmentdate.Checked = True Then
                StrSearching &= "AND assessmentdate = '" & eZeeDate.convertDate(dtpAssessmentdate.Value) & "'"
            End If

            If chkShowCommited.CheckState = CheckState.Checked And chkShowUncommited.CheckState = CheckState.Unchecked Then
                StrSearching &= "AND iscommitted = " & True
            ElseIf chkShowCommited.CheckState = CheckState.Unchecked And chkShowUncommited.CheckState = CheckState.Checked Then
                StrSearching &= "AND iscommitted = " & False
            End If

            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mStrAdvanceFilter.Trim.Length > 0 Then
                StrSearching &= "AND " & mStrAdvanceFilter
            End If
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables(0), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            lvBSCAssessorList.Items.Clear()

            For Each dRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dRow.Item("EmpName").ToString
                lvItem.SubItems.Add(eZeeDate.convertDate(dRow.Item("assessmentdate").ToString).ToShortDateString)
                lvItem.SubItems.Add(dRow.Item("PName").ToString)
                lvItem.SubItems(colhAssessmentPeriod.Index).Tag = dRow.Item("Sid")
                lvItem.SubItems.Add(dRow.Item("YearName").ToString)
                lvItem.SubItems.Add(dRow.Item("Assessor").ToString)
                lvItem.Tag = dRow.Item("analysisunkid")

                If CBool(dRow.Item("iscommitted")) = True Then
                    lvItem.ForeColor = Color.Blue
                End If

                'S.SANDEEP [ 04 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If blnFlag Then
                        'S.SANDEEP [ 08 APR 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'Dim dTemp() As DataRow = dsPercent.Tables(0).Select("EmpId = '" & CInt(dRow.Item("assessedemployeeunkid")) & "' AND Pid = '" & CInt(dRow.Item("periodunkid")) & "'")
                        Dim dTemp() As DataRow = dsPercent.Tables(0).Select("EmpId = '" & CInt(dRow.Item("assessedemployeeunkid")) & "' AND Pid = '" & CInt(dRow.Item("periodunkid")) & "' AND analysisunkid = '" & CInt(dRow.Item("analysisunkid")) & "'")
                        'S.SANDEEP [ 08 APR 2013 ] -- END
                        If dTemp.Length > 0 Then
                            lvItem.SubItems.Add(dTemp(0)("TotalPercent").ToString)
                            'S.SANDEEP [ 28 JAN 2014 ] -- START
                        Else
                            lvItem.SubItems.Add("")
                            'S.SANDEEP [ 28 JAN 2014 ] -- END
                        End If
                    End If
                    'S.SANDEEP [ 04 FEB 2012 ] -- END

                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvItem.SubItems.Add(dRow.Item("periodunkid").ToString)
                'S.SANDEEP [ 05 MARCH 2012 ] -- END

                    'S.SANDEEP [ 03 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(dRow.Item("assessedemployeeunkid").ToString)
                    lvItem.SubItems.Add(dRow.Item("yearunkid").ToString)
                    'S.SANDEEP [ 03 AUG 2013 ] -- END

                lvBSCAssessorList.Items.Add(lvItem)
            Next

            lvBSCAssessorList.GridLines = False

            lvBSCAssessorList.GroupingColumn = colhAssessor
            lvBSCAssessorList.DisplayGroups(True)

            If lvBSCAssessorList.Items.Count > 4 Then
                    colhEmployee.Width = 265 - 20
            Else
                colhEmployee.Width = 265
            End If

            ' End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'btnNew.Enabled = User._Object.Privilege._AddAssessmentAnalysis
            'btnEdit.Enabled = User._Object.Privilege._EditAssessmentAnalysis
            'btnDelete.Enabled = User._Object.Privilege._DeleteAssessmentAnalysis


            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)

            'btnNew.Enabled = User._Object.Privilege._AllowToAddAssessorBSCAssessment
            'btnEdit.Enabled = User._Object.Privilege._AllowToEditAssessorBSCAssessment
            'btnDelete.Enabled = User._Object.Privilege._AllowToDeleteAssessorBSCAssessment
            ''S.SANDEEP [ 05 MARCH 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedBSCAssessment
            ''S.SANDEEP [ 05 MARCH 2012 ] -- END

            'S.SANDEEP [28 MAY 2015] -- END



            'S.SANDEEP [ 16 MAY 2012 ] -- END

            
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBSCAssessorAssessmentList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBSCAssessor = New clsBSC_Analysis_Master
        Try
            chkShowCommited.CheckState = CheckState.Checked
            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()
            Call SetVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSCAssessorAssessmentList_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'S.SANDEEP [ 24 APR 2014 ] -- START
        'Dim frm As New frmBSC_Evaluation
        Dim frm As New frmBSC_TabularEvaluation
        'S.SANDEEP [ 24 APR 2014 ] -- END
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE, enAssessmentMode.APPRAISER_ASSESSMENT) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        'S.SANDEEP [ 24 APR 2014 ] -- START
        'Dim frm As New frmBSC_Evaluation
        Dim frm As New frmBSC_TabularEvaluation
        'S.SANDEEP [ 24 APR 2014 ] -- END
        Try

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If lvBSCAssessorList.SelectedItems.Count > 0 Then
                If CInt(lvBSCAssessorList.SelectedItems(0).SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, You cannot edit this information. Reason : Period is already closed."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(CInt(lvBSCAssessorList.SelectedItems(0).Tag), enAction.EDIT_ONE, enAssessmentMode.APPRAISER_ASSESSMENT) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
        'S.SANDEEP [ 29 DEC 2011 ] -- END
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvBSCAssessorList.SelectedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select atleast one Assessment to perform Operation on it."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CInt(lvBSCAssessorList.SelectedItems(0).SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Close Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot delete this information. Reason : Period is closed."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Try

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Assessment?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objBSCAssessor._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objBSCAssessor._Isvoid = True
                objBSCAssessor._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objBSCAssessor._Voiduserunkid = User._Object._Userunkid

                'S.SANDEEP [ 01 JUL 2014 ] -- START
                'objBSCAssessor.Delete(CInt(lvBSCAssessorList.SelectedItems(0).Tag), enAssessmentMode.REVIEWER_ASSESSMENT)
                objBSCAssessor.Delete(CInt(lvBSCAssessorList.SelectedItems(0).Tag), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(lvBSCAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text))
                'S.SANDEEP [ 01 JUL 2014 ] -- END

                If objBSCAssessor._Message <> "" Then
                    eZeeMsgBox.Show(objBSCAssessor._Message, enMsgBoxStyle.Information)
                Else
                    lvBSCAssessorList.SelectedItems(0).Remove()
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'frm.CodeMember = "employeecode"
            frm.CodeMember = "Code"
            'S.SANDEEP [ 16 MAY 2012 ] -- END
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAssessor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAssessor.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboAssessor.ValueMember
            frm.DisplayMember = cboAssessor.DisplayMember
            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'frm.CodeMember = ""

            'S.SANDEEP [ 13 NOV 2013 ] -- START
            'frm.CodeMember = "employeecode"
            frm.CodeMember = "Code"
            'S.SANDEEP [ 13 NOV 2013 ] -- END

            'S.SANDEEP [ 16 MAY 2012 ] -- END
            frm.DataSource = CType(cboAssessor.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboAssessor.SelectedValue = frm.SelectedValue
                cboAssessor.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAssessor_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboAssessor.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboYear.SelectedValue = 0
            dtpAssessmentdate.Checked = False
            chkShowCommited.CheckState = CheckState.Checked
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mStrAdvanceFilter = String.Empty
            'S.SANDEEP [ 05 MARCH 2012 ] -- END
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub btnMakeCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMakeCommit.Click
    '    Try
    '        If lvBSCAssessorList.CheckedItems.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please check atleast one information to set as commit."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        For Each LVI As ListViewItem In lvBSCAssessorList.CheckedItems
    '            objBSCAssessor._Analysisunkid = CInt(LVI.Tag)
    '            objBSCAssessor._Iscommitted = True
    '            objBSCAssessor.Update()
    '        Next
    '        Call FillList()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnMakeCommit_Click", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Private Sub btnUnlockCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnlockCommit.Click
        Try
            If lvBSCAssessorList.SelectedItems.Count > 0 Then

                'S.SANDEEP [ 03 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Dim objAnalysis_Master As New clsBSC_Analysis_Master
                If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(lvBSCAssessorList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text), CInt(lvBSCAssessorList.SelectedItems(0).SubItems(objcolhYearId.Index).Text), CInt(lvBSCAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                objAnalysis_Master = Nothing
                'S.SANDEEP [ 03 AUG 2013 ] -- END

                If CInt(lvBSCAssessorList.SelectedItems(0).SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Open Then
                    'S.SANDEEP [ 10 SEPT 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If objBSCAssessor.Unlock_Commit(CInt(lvBSCAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text)) = True Then
                    If objBSCAssessor.Unlock_Commit(CInt(lvBSCAssessorList.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text), _
                                                    CInt(lvBSCAssessorList.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)) = True Then
                        'S.SANDEEP [ 10 SEPT 2013 ] -- END
                        objBSCAssessor._Analysisunkid = CInt(lvBSCAssessorList.SelectedItems(0).Tag)
                        objBSCAssessor._Iscommitted = False
                        'S.SANDEEP [ 14 JUNE 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        objBSCAssessor._Committeddatetime = Nothing
                        'S.SANDEEP [ 14 JUNE 2012 ] -- END
                        objBSCAssessor.Update()
                        If objBSCAssessor._Message <> "" Then
                            eZeeMsgBox.Show(objBSCAssessor._Message)
                        End If
                        Call FillList()
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal."), enMsgBoxStyle.Information)
                Exit Sub
            End If
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot unlock this commited information. Reason : Period is already Closed."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnUnlockCommit_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 05 MARCH 2012 ] -- END


#End Region

#Region " Controls "

    Private Sub lvAssessorList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvBSCAssessorList.ItemChecked
        Try
            If lvBSCAssessorList.CheckedItems.Count <= 0 Then Exit Sub
            If e.Item.ForeColor = Color.Blue Then e.Item.Checked = False
            If CInt(e.Item.SubItems(colhAssessmentPeriod.Index).Tag) = enStatusType.Close Then e.Item.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAssessorList_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvAssessorList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvBSCAssessorList.SelectedIndexChanged
        Try
            If lvBSCAssessorList.SelectedItems.Count <= 0 Then Exit Sub
            If lvBSCAssessorList.SelectedItems(0).ForeColor = Color.Blue Then
                btnEdit.Enabled = False : btnDelete.Enabled = False : btnUnlockCommit.Enabled = True 'S.SANDEEP [ 05 MARCH 2012 btnMakeCommit.Enabled = False ] -- START -- END
            Else
                btnEdit.Enabled = True : btnDelete.Enabled = True : btnUnlockCommit.Enabled = False 'S.SANDEEP [ 05 MARCH 2012 btnMakeCommit.Enabled = True ] -- START -- END
            End If

            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'btnUnlockCommit.Enabled = User._Object.Privilege._Allow_UnlockCommittedBSCAssessment
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAssessorList_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            frm.ShowDialog()
            mStrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

#End Region


    'Pinkal (5-MAY-2012) -- Start
    'Enhancement : TRA Changes

#Region "Combobox Event"

    Private Sub cboAssessor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
        Try
            Dim objAssessor As New clsBSC_Analysis_Master
            Dim dsList As DataSet = objAssessor.getEmployeeBasedAssessor(CInt(cboAssessor.SelectedValue), "List", True)
            cboEmployee.DisplayMember = "NAME"
            cboEmployee.ValueMember = "Id"
            cboEmployee.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessor_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (5-MAY-2012) -- End




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnUnlockCommit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnUnlockCommit.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
			Me.chkShowUncommited.Text = Language._Object.getCaption(Me.chkShowUncommited.Name, Me.chkShowUncommited.Text)
			Me.chkShowCommited.Text = Language._Object.getCaption(Me.chkShowCommited.Name, Me.chkShowCommited.Text)
			Me.lblAssessor.Text = Language._Object.getCaption(Me.lblAssessor.Name, Me.lblAssessor.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblYears.Text = Language._Object.getCaption(Me.lblYears.Name, Me.lblYears.Text)
			Me.lblAssessmentPeriods.Text = Language._Object.getCaption(Me.lblAssessmentPeriods.Name, Me.lblAssessmentPeriods.Text)
			Me.lblAssessmentdate.Text = Language._Object.getCaption(Me.lblAssessmentdate.Name, Me.lblAssessmentdate.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
			Me.colhAssessmentPeriod.Text = Language._Object.getCaption(CStr(Me.colhAssessmentPeriod.Tag), Me.colhAssessmentPeriod.Text)
			Me.colhAssessmentYear.Text = Language._Object.getCaption(CStr(Me.colhAssessmentYear.Tag), Me.colhAssessmentYear.Text)
			Me.colhAssessor.Text = Language._Object.getCaption(CStr(Me.colhAssessor.Tag), Me.colhAssessor.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhPercent.Text = Language._Object.getCaption(CStr(Me.colhPercent.Tag), Me.colhPercent.Text)
			Me.btnUnlockCommit.Text = Language._Object.getCaption(Me.btnUnlockCommit.Name, Me.btnUnlockCommit.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 3, "Please select atleast one Assessment to perform Operation on it.")
			Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete this Assessment?")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot delete this information. Reason : Period is closed.")
			Language.setMessage(mstrModuleName, 9, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal.")
			Language.setMessage(mstrModuleName, 10, "Sorry, you cannot unlock this commited information. Reason : Period is already Closed.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
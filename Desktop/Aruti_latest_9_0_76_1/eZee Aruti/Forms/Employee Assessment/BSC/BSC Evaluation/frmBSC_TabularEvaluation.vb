﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmBSC_TabularEvaluation

#Region " Private Variables "

    Private mstrModuleName As String = "frmBSC_TabularEvaluation"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objBSCEvaluation As clsBSC_Analysis_Master
    Private objBSCTran As clsBSC_analysis_tran
    Private mintAssessAnalysisUnkid As Integer = -1
    Private mintEmplId As Integer = 0
    Private mintYearId As Integer = 0
    Private mintPeriodId As Integer = 0
    Private mintAssessorId As Integer = 0
    Private menAssess As enAssessmentMode
    Private mdtEvaluation As DataTable
    'S.SANDEEP [ 30 MAY 2014 ] -- START
    'Private objWSetting As New clsWeight_Setting(True)
    Private objWSetting As clsWeight_Setting
    'S.SANDEEP [ 30 MAY 2014 ] -- END
    Private mdtFullBSC As DataTable
    Private dsTabularGrid As New DataSet

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkid As Integer, ByVal eAction As enAction, ByVal eAssess As enAssessmentMode) As Boolean
        Try
            mintAssessAnalysisUnkid = intUnkid
            menAction = eAction
            menAssess = eAssess
            Me.ShowDialog()
            intUnkid = mintAssessAnalysisUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetVisibility()
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                cboAssessor.Enabled = False
                lblAssessor.Enabled = False
                objbtnSearchAssessor.Enabled = False
                radInternalAssessor.Checked = False : radExternalAssessor.Checked = False
                radInternalAssessor.Enabled = False : radExternalAssessor.Enabled = False
                lblReviewer.Enabled = False
                cboReviewer.Enabled = False
                objbtnSearchReviewer.Enabled = False
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                cboAssessor.Enabled = True
                lblAssessor.Enabled = True
                objbtnSearchAssessor.Enabled = True
                lblReviewer.Visible = False
                cboReviewer.Visible = False
                objbtnSearchReviewer.Visible = False
                lnkViewBSC.Enabled = False
                radInternalAssessor.Checked = True
                radInternalAssessor.Enabled = True : radExternalAssessor.Enabled = True
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                cboAssessor.Visible = False
                lblAssessor.Visible = False
                objbtnSearchAssessor.Visible = False
                lblReviewer.Enabled = True
                cboReviewer.Enabled = True
                objbtnSearchReviewer.Enabled = True
                lnkViewBSC.Enabled = True
                radInternalAssessor.Checked = False : radExternalAssessor.Checked = False
                radInternalAssessor.Enabled = False : radExternalAssessor.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            cboYear.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
            cboAssessor.BackColor = GUI.ColorComp
            cboReviewer.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
                'End If
                'With cboEmployee
                '    .ValueMember = "employeeunkid"
                '    .DisplayMember = "employeename"
                '    .DataSource = dsCombos.Tables(0)
                '    .SelectedValue = 0
                'End With
                'S.SANDEEP [04 JUN 2015] -- END
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                dsCombos = objBSCEvaluation.getAssessorComboList("Assessor", True, True)
                With cboReviewer
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objMaster.getComboListPAYYEAR("Year", True, , , , True)
            dsCombos = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Year")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            Select Case menAssess
                Case enAssessmentMode.SELF_ASSESSMENT
                    cboEmployee.SelectedValue = objBSCEvaluation._Selfemployeeunkid
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    If objBSCEvaluation._Ext_Assessorunkid > 0 Then
                        radExternalAssessor.Checked = True
                        cboAssessor.SelectedValue = objBSCEvaluation._Ext_Assessorunkid
                    Else
                        radInternalAssessor.Checked = True
                        cboAssessor.SelectedValue = objBSCEvaluation._Assessormasterunkid
                    End If
                    cboEmployee.SelectedValue = objBSCEvaluation._Assessedemployeeunkid
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    cboReviewer.SelectedValue = objBSCEvaluation._Assessormasterunkid
                    cboEmployee.SelectedValue = objBSCEvaluation._Assessedemployeeunkid
            End Select
            cboYear.SelectedValue = objBSCEvaluation._Yearunkid
            cboPeriod.SelectedValue = objBSCEvaluation._Periodunkid
            If objBSCEvaluation._Assessmentdate <> Nothing Then
                dtpAssessdate.Value = objBSCEvaluation._Assessmentdate
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objBSCEvaluation._Yearunkid = CInt(cboYear.SelectedValue)
            objBSCEvaluation._Periodunkid = CInt(cboPeriod.SelectedValue)
            Select Case menAssess
                Case enAssessmentMode.SELF_ASSESSMENT
                    objBSCEvaluation._Selfemployeeunkid = CInt(cboEmployee.SelectedValue)
                    objBSCEvaluation._Assessedemployeeunkid = -1
                    objBSCEvaluation._Assessormasterunkid = -1
                    objBSCEvaluation._Assessoremployeeunkid = -1
                    objBSCEvaluation._Reviewerunkid = -1
                    objBSCEvaluation._Assessmodeid = enAssessmentMode.SELF_ASSESSMENT
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    objBSCEvaluation._Selfemployeeunkid = -1
                    objBSCEvaluation._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
                    If radInternalAssessor.Checked = True Then
                        objBSCEvaluation._Assessormasterunkid = CInt(cboAssessor.SelectedValue)
                        Dim intEmployeeId As Integer = -1
                        intEmployeeId = objBSCEvaluation.GetAssessorEmpId(CInt(cboAssessor.SelectedValue))
                        objBSCEvaluation._Assessoremployeeunkid = intEmployeeId
                    ElseIf radExternalAssessor.Checked = True Then
                        objBSCEvaluation._Ext_Assessorunkid = CInt(cboAssessor.SelectedValue)
                        objBSCEvaluation._Assessormasterunkid = -1
                        objBSCEvaluation._Assessoremployeeunkid = -1
                    End If
                    objBSCEvaluation._Reviewerunkid = -1
                    objBSCEvaluation._Assessmodeid = enAssessmentMode.APPRAISER_ASSESSMENT
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    objBSCEvaluation._Selfemployeeunkid = -1
                    objBSCEvaluation._Assessormasterunkid = -1
                    objBSCEvaluation._Assessoremployeeunkid = -1
                    objBSCEvaluation._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
                    objBSCEvaluation._Reviewerunkid = User._Object._Userunkid
                    objBSCEvaluation._Assessmodeid = enAssessmentMode.REVIEWER_ASSESSMENT
                    objBSCEvaluation._Assessormasterunkid = CInt(cboReviewer.SelectedValue)
                    Dim intEmployeeId As Integer = -1
                    intEmployeeId = objBSCEvaluation.GetAssessorEmpId(CInt(cboReviewer.SelectedValue))
                    objBSCEvaluation._Reviewerunkid = intEmployeeId
            End Select
            objBSCEvaluation._Assessmentdate = dtpAssessdate.Value
            objBSCEvaluation._Userunkid = User._Object._Userunkid
            If objBSCEvaluation._Committeddatetime <> Nothing Then
                objBSCEvaluation._Committeddatetime = objBSCEvaluation._Committeddatetime
            Else
                objBSCEvaluation._Committeddatetime = Nothing
            End If
            Select Case menAction
                Case enAction.ADD_ONE, enAction.ADD_CONTINUE
                    objBSCEvaluation._Isvoid = False
                    objBSCEvaluation._Voiduserunkid = -1
                    objBSCEvaluation._Voiddatetime = Nothing
                    objBSCEvaluation._Voidreason = ""
                Case enAction.EDIT_ONE
                    objBSCEvaluation._Isvoid = objBSCEvaluation._Isvoid
                    objBSCEvaluation._Voiduserunkid = objBSCEvaluation._Voiduserunkid
                    objBSCEvaluation._Voiddatetime = objBSCEvaluation._Voiddatetime
                    objBSCEvaluation._Voidreason = objBSCEvaluation._Voidreason
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function isAll_Assessed(Optional ByVal blnFlag As Boolean = True) As Boolean
        Try
            If mdtEvaluation.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), enMsgBoxStyle.Information)
                Return False
            End If
            Dim dtmp1() As DataRow = Nothing
            Dim dtmp2() As DataRow = Nothing
            'S.SANDEEP [ 11 JUL 2014 ] -- Start
            Dim dView As DataView = dsTabularGrid.Tables(0).DefaultView
            'S.SANDEEP [ 11 JUL 2014 ] -- End
            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                dtmp1 = dView.ToTable(True, "InitiativeId").Select("InitiativeId > 0") 'dsTabularGrid.Tables(0).Select("InitiativeId > 0")
                dtmp2 = mdtEvaluation.Select("initiativeunkid > 0 AND resultunkid > 0 ")
            ElseIf objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                'S.SANDEEP [04 JUL 2014] -- Start
                'Select Case objWSetting._Weight_Optionid
                Select Case objWSetting._Weight_Typeid
                    'S.SANDEEP [04 JUL 2014] -- End
                    Case enWeight_Types.WEIGHT_FIELD1
                        dtmp1 = dView.ToTable(True, "ObjectiveId").Select("ObjectiveId > 0") 'dsTabularGrid.Tables(0).Select("ObjectiveId > 0")
                        dtmp2 = mdtEvaluation.Select("objectiveunkid > 0 AND resultunkid > 0 ")
                    Case enWeight_Types.WEIGHT_FIELD2
                        dtmp1 = dView.ToTable(True, "KpiId").Select("KpiId > 0") 'dsTabularGrid.Tables(0).Select("KpiId > 0")
                        dtmp2 = mdtEvaluation.Select("kpiunkid > 0 AND resultunkid > 0 ")
                    Case enWeight_Types.WEIGHT_FIELD3
                        dtmp1 = dView.ToTable(True, "TargetId").Select("TargetId > 0") 'dsTabularGrid.Tables(0).Select("TargetId > 0")
                        dtmp2 = mdtEvaluation.Select("targetunkid > 0 AND resultunkid > 0 ")
                    Case enWeight_Types.WEIGHT_FIELD4
                        dtmp1 = dView.ToTable(True, "InitiativeId").Select("InitiativeId > 0") 'dsTabularGrid.Tables(0).Select("InitiativeId > 0")
                        dtmp2 = mdtEvaluation.Select("initiativeunkid > 0 AND resultunkid > 0 ")
                End Select
            End If

            If dtmp1.Length <> dtmp2.Length Then
                If blnFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isAll_Assessed", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub DoOperation(ByVal blnFlag As Boolean)
        Try
            cboReviewer.Enabled = blnFlag
            objbtnSearchReviewer.Enabled = blnFlag
            cboAssessor.Enabled = blnFlag
            objbtnSearchAssessor.Enabled = blnFlag
            cboEmployee.Enabled = blnFlag
            objbtnSearchEmployee.Enabled = blnFlag
            cboPeriod.Enabled = blnFlag
            cboYear.Enabled = blnFlag
            dtpAssessdate.Enabled = blnFlag
            radExternalAssessor.Enabled = blnFlag
            radInternalAssessor.Enabled = blnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillEvaluationList()
        Try
            Dim objBSC As New clsBSC_Analysis_Master
            dsTabularGrid = objBSC.GetTabularData(CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue))

            Dim iRGrp() As DataRow = dsTabularGrid.Tables(0).Select("RGrpId > 0")
            Dim iResultGrpId As Integer = 0
            If iRGrp.Length > 0 Then
                iResultGrpId = CInt(iRGrp(0).Item("RGrpId"))
            End If


            dgvData.AutoGenerateColumns = False
            dgcolhInitiative.DataPropertyName = "Initative"
            dgcolhKPI.DataPropertyName = "KPI"
            dgcolhObjective.DataPropertyName = "Objective"
            dgcolhRemark.DataPropertyName = "Remark"
            dgcolhTarget.DataPropertyName = "Target"
            dgcolhWeight.DataPropertyName = "Weight"
            objdgcolhGrpId.DataPropertyName = "GrpId"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"

            dgcolhWeight.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhWeight.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

            Dim dsCombo As New DataSet
            dsCombo = (New clsresult_master).getComboList("List", True, iResultGrpId)
            dgcolhResult.ValueMember = "Id"
            dgcolhResult.DisplayMember = "Name"
            dgcolhResult.DataSource = dsCombo.Tables(0)
            dgcolhResult.DataPropertyName = "Id"
            dgvData.DataSource = dsTabularGrid.Tables(0)

            Dim pCell As New clsMergeCell
            For Each dgvRow As DataGridViewRow In dgvData.Rows
                If dgvRow.Cells(dgcolhWeight.Index).Value.ToString.Trim.Length <= 0 AndAlso CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = False Then
                    pCell.MakeMerge(dgvData, dgvRow.Index, dgvRow.Cells(dgcolhResult.Index).ColumnIndex, dgvRow.Cells(dgcolhResult.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                    dgvRow.ReadOnly = True
                ElseIf dgvRow.Cells(dgcolhWeight.Index).Value.ToString.Trim.Length <= 0 AndAlso CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = True Then
                    pCell.MakeMerge(dgvData, dgvRow.Index, 2, dgvRow.Cells.Count - 1, Color.Gray, Color.White, Nothing, "", picStayView.Image)
                    dgvRow.ReadOnly = True
                Else
                    If menAction <> enAction.EDIT_ONE Then
                        dgvRow.Cells(dgcolhResult.Index).Value = 0
                    Else
                        Dim dtmp() As DataRow = Nothing
                        If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                            dtmp = mdtEvaluation.Select("initiativeunkid = '" & dsTabularGrid.Tables(0).Rows(dgvRow.Index).Item("InitiativeId").ToString & "' AND AUD <> 'D' ")
                        ElseIf objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                            'S.SANDEEP [04 JUL 2014] -- Start
                            'Select Case objWSetting._Weight_Optionid
                            Select Case objWSetting._Weight_Typeid
                                'S.SANDEEP [04 JUL 2014] -- End
                                Case enWeight_Types.WEIGHT_FIELD1
                                    dtmp = mdtEvaluation.Select("objectiveunkid = '" & dsTabularGrid.Tables(0).Rows(dgvRow.Index).Item("ObjectiveId").ToString & "' AND AUD <> 'D' ")
                                Case enWeight_Types.WEIGHT_FIELD2
                                    dtmp = mdtEvaluation.Select("kpiunkid = '" & dsTabularGrid.Tables(0).Rows(dgvRow.Index).Item("KpiId").ToString & "' AND AUD <> 'D' ")
                                Case enWeight_Types.WEIGHT_FIELD3
                                    dtmp = mdtEvaluation.Select("targetunkid= '" & dsTabularGrid.Tables(0).Rows(dgvRow.Index).Item("TargetId").ToString & "' AND AUD <> 'D' ")
                                Case enWeight_Types.WEIGHT_FIELD4
                                    dtmp = mdtEvaluation.Select("initiativeunkid = '" & dsTabularGrid.Tables(0).Rows(dgvRow.Index).Item("InitiativeId").ToString & "' AND AUD <> 'D' ")
                            End Select
                        End If
                        RemoveHandler dgvData.CellValueChanged, AddressOf dgvData_CellValueChanged
                        If dtmp.Length > 0 Then
                            dgvRow.Cells(dgcolhResult.Index).Value = CInt(dtmp(0).Item("resultunkid"))
                            dgvRow.Cells(dgcolhRemark.Index).Value = dtmp(0).Item("remark")
                        End If
                        AddHandler dgvData.CellValueChanged, AddressOf dgvData_CellValueChanged
                    End If
                End If

                If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvRow.ReadOnly = True
                    If dgvRow.Cells(objdgcolhCollaps.Index).Value Is Nothing Then
                        dgvRow.Cells(objdgcolhCollaps.Index).Value = "-"
                        dgvRow.Cells(objdgcolhCollaps.Index).Style.ForeColor = Color.White
                    End If
                    dgvRow.DefaultCellStyle.BackColor = Color.Gray
                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.Gray
                    dgvRow.DefaultCellStyle.ForeColor = Color.White
                Else
                    dgvRow.Cells(objdgcolhCollaps.Index).Value = ""
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEvaluationList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Emplolyee is compulsory information.Please Select Emplolyee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Return False
            ElseIf CInt(cboYear.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Year is compulsory information.Please Select Year."), enMsgBoxStyle.Information)
                cboYear.Select()
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Return False
            ElseIf CInt(cboAssessor.SelectedValue) = 0 And menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Assessors is compulsory information.Please Select Assessors."), enMsgBoxStyle.Information)
                cboAssessor.Select()
                Return False
            End If

            Dim dsYr As New DataSet : Dim oCompany As New clsCompany_Master
            dsYr = oCompany.GetFinancialYearList(Company._Object._Companyunkid, User._Object._Userunkid, "List", CInt(cboYear.SelectedValue))
            If dsYr.Tables("List").Rows.Count > 0 Then
                If dtpAssessdate.Value.Date > eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date Or _
                   dtpAssessdate.Value.Date < eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date Then
                    Dim strMsg As String = Language.getMessage(mstrModuleName, 8, "Assessment date should be in between ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date & _
                                           Language.getMessage(mstrModuleName, 9, " And ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    dtpAssessdate.Focus()
                    Return False
                End If
            Else
                If dtpAssessdate.Value.Date > FinancialYear._Object._Database_End_Date.Date Or _
                   dtpAssessdate.Value.Date < FinancialYear._Object._Database_Start_Date.Date Then
                    Dim strMsg As String = Language.getMessage(mstrModuleName, 8, "Assessment date should be in between ") & FinancialYear._Object._Database_Start_Date.Date & _
                                           Language.getMessage(mstrModuleName, 9, " And ") & FinancialYear._Object._Database_End_Date.Date
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    dtpAssessdate.Focus()
                    Return False
                End If
            End If

            If menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                If CInt(cboReviewer.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Reviewer is mandatory information. Please create Reviewer first. Assessment -> Setups -> Reviewer."), enMsgBoxStyle.Information)
                    cboReviewer.Focus()
                    Return False
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Evaluated_Data(ByVal iResult As Integer)
        Try
            Dim dRow As DataRow = Nothing
            Dim dTemp() As DataRow = Nothing
            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                dTemp = mdtEvaluation.Select("initiativeunkid = '" & dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("InitiativeId").ToString & "' AND AUD <> 'D' ")
            ElseIf objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                'S.SANDEEP [04 JUL 2014] -- Start
                'Select Case objWSetting._Weight_Optionid
                Select Case objWSetting._Weight_Typeid
                    'S.SANDEEP [04 JUL 2014] -- End
                    Case enWeight_Types.WEIGHT_FIELD1
                        dTemp = mdtEvaluation.Select("objectiveunkid = '" & dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("ObjectiveId").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD2
                        dTemp = mdtEvaluation.Select("kpiunkid = '" & dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("KpiId").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD3
                        dTemp = mdtEvaluation.Select("targetunkid= '" & dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("TargetId").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD4
                        dTemp = mdtEvaluation.Select("initiativeunkid = '" & dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("InitiativeId").ToString & "' AND AUD <> 'D' ")
                End Select
            End If
            If dTemp.Length <= 0 Then
                dRow = mdtEvaluation.NewRow
                dRow.Item("analysistranunkid") = -1
                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                dRow.Item("objectiveunkid") = CInt(dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("ObjectiveId"))
                dRow.Item("resultunkid") = iResult
                dRow.Item("remark") = dgvData.CurrentRow.Cells(dgcolhRemark.Index).Value.ToString
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("kpiunkid") = CInt(dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("KpiId"))
                dRow.Item("targetunkid") = CInt(dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("TargetId"))
                dRow.Item("initiativeunkid") = CInt(dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("InitiativeId"))
                dRow.Item("perspectiveunkid") = CInt(dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("PerspectiveId"))
                mdtEvaluation.Rows.Add(dRow)
            Else
                If menAction <> enAction.EDIT_ONE Then
                    dTemp(0).Item("analysistranunkid") = -1
                Else
                    dTemp(0).Item("analysistranunkid") = dTemp(0).Item("analysistranunkid")
                End If
                dTemp(0).Item("analysisunkid") = mintAssessAnalysisUnkid
                dTemp(0).Item("objectiveunkid") = CInt(dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("ObjectiveId"))
                dTemp(0).Item("resultunkid") = iResult
                dTemp(0).Item("remark") = dgvData.CurrentRow.Cells(dgcolhRemark.Index).Value.ToString
                If IsDBNull(dTemp(0).Item("AUD")) Or CStr(dTemp(0).Item("AUD")).ToString.Trim = "" Then
                    dTemp(0).Item("AUD") = "U"
                End If
                dTemp(0).Item("GUID") = Guid.NewGuid.ToString
                dTemp(0).Item("isvoid") = False
                dTemp(0).Item("voiduserunkid") = -1
                dTemp(0).Item("voiddatetime") = DBNull.Value
                dTemp(0).Item("voidreason") = ""
                dTemp(0).Item("kpiunkid") = CInt(dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("KpiId"))
                dTemp(0).Item("targetunkid") = CInt(dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("TargetId"))
                dTemp(0).Item("initiativeunkid") = CInt(dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("InitiativeId"))
                dTemp(0).Item("perspectiveunkid") = CInt(dsTabularGrid.Tables(0).Rows(dgvData.CurrentRow.Index).Item("PerspectiveId"))
                mdtEvaluation.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Evaluated_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBSC_TabularEvaluation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBSCEvaluation = New clsBSC_Analysis_Master
        objBSCTran = New clsBSC_analysis_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objBSCEvaluation._Analysisunkid = mintAssessAnalysisUnkid
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                cboAssessor.Enabled = False
                objbtnSearchAssessor.Enabled = False
                cboYear.Enabled = False
                cboPeriod.Enabled = False
                radExternalAssessor.Enabled = False
                radInternalAssessor.Enabled = False
                cboReviewer.Enabled = False
                objbtnSearchReviewer.Enabled = False
            End If
            Call GetValue()
            objBSCTran._AnalysisUnkid = mintAssessAnalysisUnkid
            mdtEvaluation = objBSCTran._DataTable
            If menAction = enAction.EDIT_ONE Then
                Call FillEvaluationList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_TabularEvaluation_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmBSC_TabularEvaluation_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Evaluation_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_TabularEvaluation_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_TabularEvaluation_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_TabularEvaluation_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objBSCEvaluation = Nothing : objBSCTran = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBSC_Analysis_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsBSC_Analysis_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSaveCommit.Click
        Dim blnFlag As Boolean = False
        Try
            Dim dRow() As datarow = mdtEvaluation.select("AUD <> 'D'")
            If dRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, Please assess atleast one item in order to save."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Call SetValue()

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNSAVECOMMIT"
                    If ConfigParameter._Object._IsAllowFinalSave = False Then
                        If isAll_Assessed() = False Then Exit Sub
                    ElseIf ConfigParameter._Object._IsAllowFinalSave = True Then
                        If isAll_Assessed(False) = False Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If
                        End If
                    End If
                    objBSCEvaluation._Iscommitted = True
                    objBSCEvaluation._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime
            End Select

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objBSCEvaluation.Update(mdtEvaluation)
            Else
                blnFlag = objBSCEvaluation.Insert(mdtEvaluation)
            End If

            If blnFlag = False And objBSCEvaluation._Message <> "" Then
                eZeeMsgBox.Show(objBSCEvaluation._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                If CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNSAVECOMMIT" AndAlso objBSCEvaluation._Iscommitted = True Then
                    Select Case menAssess
                        Case enAssessmentMode.SELF_ASSESSMENT
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objBSCEvaluation.Email_Notification(menAssess, _
                            '                                    CInt(cboEmployee.SelectedValue), _
                            '                                    CInt(cboPeriod.SelectedValue), _
                            '                                    CInt(cboYear.SelectedValue), _
                            '                                    ConfigParameter._Object._IsCompanyNeedReviewer, , , "", enLogin_Mode.DESKTOP, 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {enLogin_Mode.DESKTOP, 0} -- END
                            objBSCEvaluation.Email_Notification(menAssess, _
                                                                CInt(cboEmployee.SelectedValue), _
                                                                CInt(cboPeriod.SelectedValue), _
                                                                CInt(cboYear.SelectedValue), _
                                                                ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , "", enLogin_Mode.DESKTOP, 0)
                            'Sohail (21 Aug 2015) -- End
                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objBSCEvaluation.Email_Notification(menAssess, _
                            '                                    CInt(cboEmployee.SelectedValue), _
                            '                                    CInt(cboPeriod.SelectedValue), _
                            '                                    CInt(cboYear.SelectedValue), _
                            '                                    ConfigParameter._Object._IsCompanyNeedReviewer, , , cboAssessor.Text, enLogin_Mode.DESKTOP, 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {enLogin_Mode.DESKTOP, 0} -- END
                            objBSCEvaluation.Email_Notification(menAssess, _
                                                                CInt(cboEmployee.SelectedValue), _
                                                                CInt(cboPeriod.SelectedValue), _
                                                                CInt(cboYear.SelectedValue), _
                                                                ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , cboAssessor.Text, enLogin_Mode.DESKTOP, 0)
                            'Sohail (21 Aug 2015) -- End
                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objBSCEvaluation.Email_Notification(menAssess, _
                            '                                    CInt(cboEmployee.SelectedValue), _
                            '                                    CInt(cboPeriod.SelectedValue), _
                            '                                    CInt(cboYear.SelectedValue), _
                            '                                    ConfigParameter._Object._IsCompanyNeedReviewer, , , cboReviewer.Text, enLogin_Mode.DESKTOP, 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {enLogin_Mode.DESKTOP, 0} -- END
                            objBSCEvaluation.Email_Notification(menAssess, _
                                                                CInt(cboEmployee.SelectedValue), _
                                                                CInt(cboPeriod.SelectedValue), _
                                                                CInt(cboYear.SelectedValue), _
                                                                ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , cboReviewer.Text, enLogin_Mode.DESKTOP, 0)
                            'Sohail (21 Aug 2015) -- End
                    End Select
                End If
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBSCEvaluation = Nothing
                    objBSCEvaluation = New clsBSC_Analysis_Master
                    Call GetValue()
                    Call DoOperation(True)
                    mdtEvaluation.Rows.Clear()
                    Call FillEvaluationList()
                    Call SetVisibility()
                Else
                    Me.Close()
                End If
            End If

            Call SetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                cboEmployee.SelectedValue = 0
                cboPeriod.SelectedValue = 0
                cboYear.SelectedValue = 0
                dgvData.DataSource = Nothing
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                cboAssessor.SelectedValue = 0
                cboEmployee.SelectedValue = 0
                cboPeriod.SelectedValue = 0
                cboYear.SelectedValue = 0
                dgvData.DataSource = Nothing
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                cboReviewer.SelectedValue = 0
                cboEmployee.SelectedValue = 0
                cboPeriod.SelectedValue = 0
                cboYear.SelectedValue = 0
                dgvData.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or _
               CInt(cboYear.SelectedValue) <= 0 Or _
               CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please set following information [Employee,Year,Period] to view assessment."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call FillEvaluationList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub cboAssessor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
            If CInt(cboAssessor.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                If radInternalAssessor.Checked = True Then
                    dsList = objBSCEvaluation.getEmployeeBasedAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
                ElseIf radExternalAssessor.Checked = True Then
                    Dim objExtAssessor As New clsexternal_assessor_master
                    dsList = objExtAssessor.GetEmpBasedOnExtAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
                End If
                With cboEmployee
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("AEmp")
                    .SelectedValue = 0
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessor_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radInternalAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radInternalAssessor.CheckedChanged
        Try
            If radInternalAssessor.Checked = True Then
                cboAssessor.DataSource = Nothing
                Dim dsCombos As DataSet = objBSCEvaluation.getAssessorComboList("Assessor", True)
                With cboAssessor
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radInternalAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radExternalAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radExternalAssessor.CheckedChanged
        Try
            If radExternalAssessor.Checked = True Then
                cboAssessor.DataSource = Nothing
                Dim objExtAssessor As New clsexternal_assessor_master
                Dim dsCombos As DataSet = objExtAssessor.GetDisplayNameComboList("Assessor", True)
                With cboAssessor
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radExternalAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Try
            If CInt(cboYear.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "APeriod", True, 1)
                dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "APeriod", True, 1)
                'Sohail (21 Aug 2015) -- End
                With cboPeriod
                    .ValueMember = "periodunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("APeriod")
                    .SelectedValue = 0
                End With
            Else
                cboPeriod.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If cboEmployee.DataSource IsNot Nothing Then
                frm.ValueMember = cboEmployee.ValueMember
                frm.DisplayMember = cboEmployee.DisplayMember
                Select Case menAssess
                    Case enAssessmentMode.SELF_ASSESSMENT
                        frm.CodeMember = "employeecode"
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT
                        frm.CodeMember = "Code"
                End Select
                frm.DataSource = CType(cboEmployee.DataSource, DataTable)
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    cboEmployee.Focus()
                End If
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAssessor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAssessor.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboAssessor.ValueMember
            frm.DisplayMember = cboAssessor.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboAssessor.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboAssessor.SelectedValue = frm.SelectedValue
                cboAssessor.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAssessor_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub lnkBSCPlanning_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkBSCPlanning.LinkClicked
        'Try
        '    Dim frm As New frmBSC_Planning

        '    If User._Object._Isrighttoleft = True Then
        '        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
        '        frm.RightToLeftLayout = True
        '        Call Language.ctlRightToLeftlayOut(frm)
        '    End If

        '    frm.displayDialog(CInt(cboObjective.SelectedValue), cboObjective.Text)

        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "lnkBSCPlanning_LinkClicked", mstrModuleName)
        'Finally
        'End Try
    End Sub

    Private Sub cboReviewer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReviewer.SelectedIndexChanged
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
            If CInt(cboReviewer.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                dsList = objBSCEvaluation.getEmployeeBasedAssessor(CInt(cboReviewer.SelectedValue), "REmp", True)
                With cboEmployee
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("REmp")
                    .SelectedValue = 0
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReviewer_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchReviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchReviewer.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboReviewer.ValueMember
            frm.DisplayMember = cboReviewer.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboReviewer.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboReviewer.SelectedValue = frm.SelectedValue
                cboReviewer.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReviewer_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub lnkViewBSC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewBSC.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or _
               CInt(cboYear.SelectedValue) <= 0 Or _
               CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please set following information [Employee,Year,Period and Group] to view assessment."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim frm As New frmCommonBSC_View

            frm.displayDialog(cboEmployee.Text, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), True)
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkViewBSC_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value Is "-" Then
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "+"
                        Else
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "-"
                        End If

                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvData.Rows(i).Visible = False Then
                                    dgvData.Rows(i).Visible = True
                                Else
                                    dgvData.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            Else

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub

    Private Sub dgvData_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellEnter
        Try
            If e.ColumnIndex = dgcolhResult.Index Or e.ColumnIndex = dgcolhRemark.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellValueChanged
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If e.ColumnIndex = dgcolhRemark.Index Then
                If dgvData.Rows(e.RowIndex).Cells(dgcolhRemark.Index).Value.ToString.Trim.Length > 0 Then
                    Call Evaluated_Data(Convert.ToInt32(dgvData.CurrentRow.Cells(dgcolhResult.Index).Value))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvData.EditingControlShowing
        Dim cbo As ComboBox
        Try
            If dgvData.CurrentCell.ColumnIndex = dgcolhResult.Index AndAlso TypeOf e.Control Is ComboBox Then
                cbo = CType(e.Control, ComboBox)
                RemoveHandler cbo.SelectedIndexChanged, AddressOf cbo_SelectedIndexChanged
                AddHandler cbo.SelectedIndexChanged, AddressOf cbo_SelectedIndexChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub cbo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim cbobox As ComboBox
        cbobox = CType(sender, ComboBox)
        If cbobox.SelectedValue IsNot Nothing AndAlso TypeOf (cbobox.SelectedValue) Is Integer AndAlso CInt(cbobox.SelectedIndex) > 0 Then
            If TypeOf (cbobox.SelectedValue) Is Integer Then
                If CDec(cbobox.Text) > CDec(dgvData.CurrentRow.Cells(dgcolhWeight.Index).Value) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot set more result value than assigned weight."), enMsgBoxStyle.Information)
                    cbobox.SelectedValue = 0
                Else
                    If Validation() = False Then Exit Sub
                    Select Case menAssess
                        Case enAssessmentMode.SELF_ASSESSMENT
                            If objBSCEvaluation.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), , , mintAssessAnalysisUnkid) = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                                cbobox.SelectedValue = 0
                                Exit Sub
                            End If
                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                            If ConfigParameter._Object._AllowAssessor_Before_Emp = False Then
                                If objBSCEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1) = False Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                        "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), enMsgBoxStyle.Information)
                                    cbobox.SelectedValue = 0
                                    Exit Sub
                                End If
                            End If
                            If objBSCEvaluation.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessor.SelectedValue), , mintAssessAnalysisUnkid, radExternalAssessor.Checked) = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                                cbobox.SelectedValue = 0
                                Exit Sub
                            End If
                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                            If ConfigParameter._Object._AllowAssessor_Before_Emp = False Then
                                If objBSCEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1) = False Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                        "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), enMsgBoxStyle.Information)
                                    cbobox.SelectedValue = 0
                                    Exit Sub
                                End If

                                If objBSCEvaluation.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1) = False Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                        "Reason: As per the setting set in configuration,You are not allowed to do assessment unless assessor does assessment for the selected employee and period."), enMsgBoxStyle.Information)
                                    cbobox.SelectedValue = 0
                                    Exit Sub
                                End If

                            End If
                            If objBSCEvaluation.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), , CInt(cboReviewer.SelectedValue), mintAssessAnalysisUnkid) = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                                cbobox.SelectedValue = 0
                                Exit Sub
                            End If
                    End Select
                    Call Evaluated_Data(CInt(cbobox.SelectedValue))
                End If
            End If
        End If
    End Sub

    'S.SANDEEP [ 30 MAY 2014 ] -- START
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            objWSetting = New clsWeight_Setting(CInt(cboPeriod.SelectedValue))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 30 MAY 2014 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbBSCEvaluation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBSCEvaluation.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveCommit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveCommit.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.dgcolhObjective.HeaderText = Language._Object.getCaption(Me.dgcolhObjective.Name, Me.dgcolhObjective.HeaderText)
			Me.dgcolhKPI.HeaderText = Language._Object.getCaption(Me.dgcolhKPI.Name, Me.dgcolhKPI.HeaderText)
			Me.dgcolhTarget.HeaderText = Language._Object.getCaption(Me.dgcolhTarget.Name, Me.dgcolhTarget.HeaderText)
			Me.dgcolhInitiative.HeaderText = Language._Object.getCaption(Me.dgcolhInitiative.Name, Me.dgcolhInitiative.HeaderText)
			Me.dgcolhWeight.HeaderText = Language._Object.getCaption(Me.dgcolhWeight.Name, Me.dgcolhWeight.HeaderText)
			Me.dgcolhResult.HeaderText = Language._Object.getCaption(Me.dgcolhResult.Name, Me.dgcolhResult.HeaderText)
			Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)
			Me.lnkViewBSC.Text = Language._Object.getCaption(Me.lnkViewBSC.Name, Me.lnkViewBSC.Text)
			Me.btnSaveCommit.Text = Language._Object.getCaption(Me.btnSaveCommit.Name, Me.btnSaveCommit.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbBSCEvaluation.Text = Language._Object.getCaption(Me.gbBSCEvaluation.Name, Me.gbBSCEvaluation.Text)
			Me.radExternalAssessor.Text = Language._Object.getCaption(Me.radExternalAssessor.Name, Me.radExternalAssessor.Text)
			Me.lblAssessor.Text = Language._Object.getCaption(Me.lblAssessor.Name, Me.lblAssessor.Text)
			Me.radInternalAssessor.Text = Language._Object.getCaption(Me.radInternalAssessor.Name, Me.radInternalAssessor.Text)
			Me.lblReviewer.Text = Language._Object.getCaption(Me.lblReviewer.Name, Me.lblReviewer.Text)
			Me.lblAssessDate.Text = Language._Object.getCaption(Me.lblAssessDate.Name, Me.lblAssessDate.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
			Me.lnkBSCPlanning.Text = Language._Object.getCaption(Me.lnkBSCPlanning.Name, Me.lnkBSCPlanning.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Emplolyee is compulsory information.Please Select Emplolyee.")
			Language.setMessage(mstrModuleName, 2, "Year is compulsory information.Please Select Year.")
			Language.setMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period.")
			Language.setMessage(mstrModuleName, 7, "Assessors is compulsory information.Please Select Assessors.")
			Language.setMessage(mstrModuleName, 8, "Assessment date should be in between")
			Language.setMessage(mstrModuleName, 9, " And")
			Language.setMessage(mstrModuleName, 14, "Sorry, you cannot do Save Commit Operation as you have not assessed all objective(s) in the selected Perspective.")
			Language.setMessage(mstrModuleName, 16, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
			Language.setMessage(mstrModuleName, 17, "Reviewer is mandatory information. Please create Reviewer first. Assessment -> Setups -> Reviewer.")
			Language.setMessage(mstrModuleName, 18, "Please set following information [Employee,Year,Period and Group] to view assessment.")
			Language.setMessage(mstrModuleName, 19, "Sorry, you cannot set more result value than assigned weight.")
			Language.setMessage(mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
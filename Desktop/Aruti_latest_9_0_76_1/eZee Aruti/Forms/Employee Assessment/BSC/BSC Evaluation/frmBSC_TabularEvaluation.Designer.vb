﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBSC_TabularEvaluation
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBSC_TabularEvaluation))
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhCollaps = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhObjective = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhKPI = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTarget = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInitiative = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhWeight = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhResult = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgcolhRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.picStayView = New System.Windows.Forms.PictureBox
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbBSCEvaluation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeGradientButton
        Me.objpnlData = New System.Windows.Forms.Panel
        Me.objbtnSearch = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.radExternalAssessor = New System.Windows.Forms.RadioButton
        Me.lblAssessor = New System.Windows.Forms.Label
        Me.radInternalAssessor = New System.Windows.Forms.RadioButton
        Me.lblReviewer = New System.Windows.Forms.Label
        Me.objbtnSearchAssessor = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchReviewer = New eZee.Common.eZeeGradientButton
        Me.cboAssessor = New System.Windows.Forms.ComboBox
        Me.lblAssessDate = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.dtpAssessdate = New System.Windows.Forms.DateTimePicker
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblYear = New System.Windows.Forms.Label
        Me.cboReviewer = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lnkViewBSC = New System.Windows.Forms.LinkLabel
        Me.btnSaveCommit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnkBSCPlanning = New System.Windows.Forms.LinkLabel
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMain.SuspendLayout()
        Me.gbBSCEvaluation.SuspendLayout()
        Me.objpnlData.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.White
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollaps, Me.dgcolhObjective, Me.dgcolhKPI, Me.dgcolhTarget, Me.dgcolhInitiative, Me.dgcolhWeight, Me.dgcolhResult, Me.dgcolhRemark, Me.objdgcolhIsGrp, Me.objdgcolhGrpId})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.Size = New System.Drawing.Size(951, 404)
        Me.dgvData.TabIndex = 0
        '
        'objdgcolhCollaps
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objdgcolhCollaps.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhCollaps.HeaderText = ""
        Me.objdgcolhCollaps.Name = "objdgcolhCollaps"
        Me.objdgcolhCollaps.ReadOnly = True
        Me.objdgcolhCollaps.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhCollaps.Width = 25
        '
        'dgcolhObjective
        '
        Me.dgcolhObjective.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhObjective.HeaderText = "Objective"
        Me.dgcolhObjective.Name = "dgcolhObjective"
        Me.dgcolhObjective.ReadOnly = True
        Me.dgcolhObjective.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhKPI
        '
        Me.dgcolhKPI.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhKPI.HeaderText = "KPI"
        Me.dgcolhKPI.Name = "dgcolhKPI"
        Me.dgcolhKPI.ReadOnly = True
        Me.dgcolhKPI.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhTarget
        '
        Me.dgcolhTarget.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhTarget.HeaderText = "Target"
        Me.dgcolhTarget.Name = "dgcolhTarget"
        Me.dgcolhTarget.ReadOnly = True
        Me.dgcolhTarget.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhInitiative
        '
        Me.dgcolhInitiative.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhInitiative.HeaderText = "Initiative"
        Me.dgcolhInitiative.Name = "dgcolhInitiative"
        Me.dgcolhInitiative.ReadOnly = True
        Me.dgcolhInitiative.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhWeight
        '
        Me.dgcolhWeight.HeaderText = "Weight"
        Me.dgcolhWeight.Name = "dgcolhWeight"
        Me.dgcolhWeight.ReadOnly = True
        Me.dgcolhWeight.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhWeight.Width = 80
        '
        'dgcolhResult
        '
        Me.dgcolhResult.DropDownWidth = 250
        Me.dgcolhResult.HeaderText = "Result"
        Me.dgcolhResult.Name = "dgcolhResult"
        Me.dgcolhResult.Width = 120
        '
        'dgcolhRemark
        '
        Me.dgcolhRemark.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhRemark.HeaderText = "Remark"
        Me.dgcolhRemark.Name = "dgcolhRemark"
        Me.dgcolhRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.ReadOnly = True
        Me.objdgcolhGrpId.Visible = False
        '
        'picStayView
        '
        Me.picStayView.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.picStayView.Location = New System.Drawing.Point(927, 380)
        Me.picStayView.Name = "picStayView"
        Me.picStayView.Size = New System.Drawing.Size(23, 23)
        Me.picStayView.TabIndex = 35
        Me.picStayView.TabStop = False
        Me.picStayView.Visible = False
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbBSCEvaluation)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(974, 525)
        Me.pnlMain.TabIndex = 36
        '
        'gbBSCEvaluation
        '
        Me.gbBSCEvaluation.BorderColor = System.Drawing.Color.Black
        Me.gbBSCEvaluation.Checked = False
        Me.gbBSCEvaluation.CollapseAllExceptThis = False
        Me.gbBSCEvaluation.CollapsedHoverImage = Nothing
        Me.gbBSCEvaluation.CollapsedNormalImage = Nothing
        Me.gbBSCEvaluation.CollapsedPressedImage = Nothing
        Me.gbBSCEvaluation.CollapseOnLoad = False
        Me.gbBSCEvaluation.Controls.Add(Me.objbtnReset)
        Me.gbBSCEvaluation.Controls.Add(Me.objpnlData)
        Me.gbBSCEvaluation.Controls.Add(Me.objbtnSearch)
        Me.gbBSCEvaluation.Controls.Add(Me.cboEmployee)
        Me.gbBSCEvaluation.Controls.Add(Me.radExternalAssessor)
        Me.gbBSCEvaluation.Controls.Add(Me.lblAssessor)
        Me.gbBSCEvaluation.Controls.Add(Me.radInternalAssessor)
        Me.gbBSCEvaluation.Controls.Add(Me.lblReviewer)
        Me.gbBSCEvaluation.Controls.Add(Me.objbtnSearchAssessor)
        Me.gbBSCEvaluation.Controls.Add(Me.objbtnSearchReviewer)
        Me.gbBSCEvaluation.Controls.Add(Me.cboAssessor)
        Me.gbBSCEvaluation.Controls.Add(Me.lblAssessDate)
        Me.gbBSCEvaluation.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbBSCEvaluation.Controls.Add(Me.lblEmployee)
        Me.gbBSCEvaluation.Controls.Add(Me.lblPeriod)
        Me.gbBSCEvaluation.Controls.Add(Me.dtpAssessdate)
        Me.gbBSCEvaluation.Controls.Add(Me.cboYear)
        Me.gbBSCEvaluation.Controls.Add(Me.cboPeriod)
        Me.gbBSCEvaluation.Controls.Add(Me.lblYear)
        Me.gbBSCEvaluation.Controls.Add(Me.cboReviewer)
        Me.gbBSCEvaluation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbBSCEvaluation.ExpandedHoverImage = Nothing
        Me.gbBSCEvaluation.ExpandedNormalImage = Nothing
        Me.gbBSCEvaluation.ExpandedPressedImage = Nothing
        Me.gbBSCEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBSCEvaluation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBSCEvaluation.HeaderHeight = 25
        Me.gbBSCEvaluation.HeaderMessage = ""
        Me.gbBSCEvaluation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbBSCEvaluation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBSCEvaluation.HeightOnCollapse = 0
        Me.gbBSCEvaluation.LeftTextSpace = 0
        Me.gbBSCEvaluation.Location = New System.Drawing.Point(0, 0)
        Me.gbBSCEvaluation.Name = "gbBSCEvaluation"
        Me.gbBSCEvaluation.OpenHeight = 300
        Me.gbBSCEvaluation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBSCEvaluation.ShowBorder = True
        Me.gbBSCEvaluation.ShowCheckBox = False
        Me.gbBSCEvaluation.ShowCollapseButton = False
        Me.gbBSCEvaluation.ShowDefaultBorderColor = True
        Me.gbBSCEvaluation.ShowDownButton = False
        Me.gbBSCEvaluation.ShowHeader = True
        Me.gbBSCEvaluation.Size = New System.Drawing.Size(974, 470)
        Me.gbBSCEvaluation.TabIndex = 37
        Me.gbBSCEvaluation.Temp = 0
        Me.gbBSCEvaluation.Text = "Evaluation Info"
        Me.gbBSCEvaluation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnReset.BorderSelected = False
        Me.objbtnReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objbtnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnReset.Location = New System.Drawing.Point(948, 2)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.Size = New System.Drawing.Size(21, 21)
        Me.objbtnReset.TabIndex = 221
        '
        'objpnlData
        '
        Me.objpnlData.Controls.Add(Me.picStayView)
        Me.objpnlData.Controls.Add(Me.dgvData)
        Me.objpnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlData.Location = New System.Drawing.Point(11, 60)
        Me.objpnlData.Name = "objpnlData"
        Me.objpnlData.Size = New System.Drawing.Size(951, 404)
        Me.objpnlData.TabIndex = 387
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch.BorderSelected = False
        Me.objbtnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch.Image = Global.Aruti.Main.My.Resources.Resources.search_20
        Me.objbtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch.Location = New System.Drawing.Point(921, 2)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch.TabIndex = 222
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(328, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(155, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'radExternalAssessor
        '
        Me.radExternalAssessor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radExternalAssessor.BackColor = System.Drawing.Color.Transparent
        Me.radExternalAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExternalAssessor.Location = New System.Drawing.Point(762, 4)
        Me.radExternalAssessor.Name = "radExternalAssessor"
        Me.radExternalAssessor.Size = New System.Drawing.Size(127, 17)
        Me.radExternalAssessor.TabIndex = 378
        Me.radExternalAssessor.TabStop = True
        Me.radExternalAssessor.Text = "External Assessor"
        Me.radExternalAssessor.UseVisualStyleBackColor = False
        '
        'lblAssessor
        '
        Me.lblAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessor.Location = New System.Drawing.Point(8, 36)
        Me.lblAssessor.Name = "lblAssessor"
        Me.lblAssessor.Size = New System.Drawing.Size(56, 15)
        Me.lblAssessor.TabIndex = 343
        Me.lblAssessor.Text = "Assessor"
        Me.lblAssessor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radInternalAssessor
        '
        Me.radInternalAssessor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radInternalAssessor.BackColor = System.Drawing.Color.Transparent
        Me.radInternalAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radInternalAssessor.Location = New System.Drawing.Point(629, 4)
        Me.radInternalAssessor.Name = "radInternalAssessor"
        Me.radInternalAssessor.Size = New System.Drawing.Size(127, 17)
        Me.radInternalAssessor.TabIndex = 378
        Me.radInternalAssessor.TabStop = True
        Me.radInternalAssessor.Text = "Internal Assessor"
        Me.radInternalAssessor.UseVisualStyleBackColor = False
        '
        'lblReviewer
        '
        Me.lblReviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReviewer.Location = New System.Drawing.Point(8, 36)
        Me.lblReviewer.Name = "lblReviewer"
        Me.lblReviewer.Size = New System.Drawing.Size(56, 15)
        Me.lblReviewer.TabIndex = 347
        Me.lblReviewer.Text = "Reviewer"
        Me.lblReviewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchAssessor
        '
        Me.objbtnSearchAssessor.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAssessor.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAssessor.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAssessor.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAssessor.BorderSelected = False
        Me.objbtnSearchAssessor.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAssessor.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAssessor.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAssessor.Location = New System.Drawing.Point(237, 33)
        Me.objbtnSearchAssessor.Name = "objbtnSearchAssessor"
        Me.objbtnSearchAssessor.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAssessor.TabIndex = 345
        '
        'objbtnSearchReviewer
        '
        Me.objbtnSearchReviewer.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchReviewer.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchReviewer.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchReviewer.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchReviewer.BorderSelected = False
        Me.objbtnSearchReviewer.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchReviewer.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchReviewer.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchReviewer.Location = New System.Drawing.Point(237, 33)
        Me.objbtnSearchReviewer.Name = "objbtnSearchReviewer"
        Me.objbtnSearchReviewer.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchReviewer.TabIndex = 385
        '
        'cboAssessor
        '
        Me.cboAssessor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssessor.DropDownWidth = 350
        Me.cboAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssessor.FormattingEnabled = True
        Me.cboAssessor.Location = New System.Drawing.Point(70, 33)
        Me.cboAssessor.Name = "cboAssessor"
        Me.cboAssessor.Size = New System.Drawing.Size(161, 21)
        Me.cboAssessor.TabIndex = 344
        '
        'lblAssessDate
        '
        Me.lblAssessDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessDate.Location = New System.Drawing.Point(829, 36)
        Me.lblAssessDate.Name = "lblAssessDate"
        Me.lblAssessDate.Size = New System.Drawing.Size(37, 15)
        Me.lblAssessDate.TabIndex = 5
        Me.lblAssessDate.Text = "Date"
        Me.lblAssessDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(489, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(264, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(58, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(657, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(43, 15)
        Me.lblPeriod.TabIndex = 7
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAssessdate
        '
        Me.dtpAssessdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAssessdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAssessdate.Location = New System.Drawing.Point(872, 33)
        Me.dtpAssessdate.Name = "dtpAssessdate"
        Me.dtpAssessdate.Size = New System.Drawing.Size(90, 21)
        Me.dtpAssessdate.TabIndex = 6
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.DropDownWidth = 200
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(558, 33)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(93, 21)
        Me.cboYear.TabIndex = 4
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(706, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(117, 21)
        Me.cboPeriod.TabIndex = 8
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(516, 36)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(36, 15)
        Me.lblYear.TabIndex = 3
        Me.lblYear.Text = "Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReviewer
        '
        Me.cboReviewer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReviewer.DropDownWidth = 350
        Me.cboReviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReviewer.FormattingEnabled = True
        Me.cboReviewer.Location = New System.Drawing.Point(70, 33)
        Me.cboReviewer.Name = "cboReviewer"
        Me.cboReviewer.Size = New System.Drawing.Size(161, 21)
        Me.cboReviewer.TabIndex = 384
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lnkViewBSC)
        Me.objFooter.Controls.Add(Me.btnSaveCommit)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.lnkBSCPlanning)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 470)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(974, 55)
        Me.objFooter.TabIndex = 36
        '
        'lnkViewBSC
        '
        Me.lnkViewBSC.BackColor = System.Drawing.Color.Transparent
        Me.lnkViewBSC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkViewBSC.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkViewBSC.Location = New System.Drawing.Point(12, 23)
        Me.lnkViewBSC.Name = "lnkViewBSC"
        Me.lnkViewBSC.Size = New System.Drawing.Size(216, 16)
        Me.lnkViewBSC.TabIndex = 373
        Me.lnkViewBSC.TabStop = True
        Me.lnkViewBSC.Text = "View Balance Score Card"
        Me.lnkViewBSC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveCommit
        '
        Me.btnSaveCommit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveCommit.BackColor = System.Drawing.Color.White
        Me.btnSaveCommit.BackgroundImage = CType(resources.GetObject("btnSaveCommit.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveCommit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveCommit.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveCommit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveCommit.FlatAppearance.BorderSize = 0
        Me.btnSaveCommit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveCommit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveCommit.ForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveCommit.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveCommit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.Location = New System.Drawing.Point(646, 13)
        Me.btnSaveCommit.Name = "btnSaveCommit"
        Me.btnSaveCommit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveCommit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveCommit.Size = New System.Drawing.Size(110, 30)
        Me.btnSaveCommit.TabIndex = 0
        Me.btnSaveCommit.Text = "S&ave && Commit"
        Me.btnSaveCommit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(762, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(865, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lnkBSCPlanning
        '
        Me.lnkBSCPlanning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkBSCPlanning.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkBSCPlanning.Location = New System.Drawing.Point(16, 20)
        Me.lnkBSCPlanning.Name = "lnkBSCPlanning"
        Me.lnkBSCPlanning.Size = New System.Drawing.Size(114, 17)
        Me.lnkBSCPlanning.TabIndex = 382
        Me.lnkBSCPlanning.TabStop = True
        Me.lnkBSCPlanning.Text = "View BSC Planning "
        Me.lnkBSCPlanning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnkBSCPlanning.Visible = False
        '
        'frmBSC_TabularEvaluation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(974, 525)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBSC_TabularEvaluation"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "BSC Evaluation"
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMain.ResumeLayout(False)
        Me.gbBSCEvaluation.ResumeLayout(False)
        Me.objpnlData.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents picStayView As System.Windows.Forms.PictureBox
    Friend WithEvents objdgcolhCollaps As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhObjective As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhKPI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTarget As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInitiative As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhWeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhResult As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgcolhRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents lnkViewBSC As System.Windows.Forms.LinkLabel
    Friend WithEvents btnSaveCommit As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbBSCEvaluation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents radExternalAssessor As System.Windows.Forms.RadioButton
    Friend WithEvents lblAssessor As System.Windows.Forms.Label
    Friend WithEvents radInternalAssessor As System.Windows.Forms.RadioButton
    Friend WithEvents cboReviewer As System.Windows.Forms.ComboBox
    Friend WithEvents lblReviewer As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchAssessor As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchReviewer As eZee.Common.eZeeGradientButton
    Friend WithEvents cboAssessor As System.Windows.Forms.ComboBox
    Friend WithEvents lblAssessDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents dtpAssessdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents lnkBSCPlanning As System.Windows.Forms.LinkLabel
    Friend WithEvents objpnlData As System.Windows.Forms.Panel
    Friend WithEvents objbtnReset As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeGradientButton
End Class

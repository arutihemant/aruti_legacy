﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmBSC_KPIMeasureAddEdit
#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmBSC_KPIMeasureAddEdit"
    Private mblnCancel As Boolean = True
    Private objKPIMaster As clsKPI_Master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintKPIUnkid As Integer = -1
    Private mintObjectiveUnkid As Integer = 0
    Private mintEmployeeUnkid As Integer = 0

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'S.SANDEEP [ 30 MAY 2014 ] -- START
    'Dim objWSetting As New clsWeight_Setting(True)
    Dim objWSetting As clsWeight_Setting
    'S.SANDEEP [ 30 MAY 2014 ] -- END
    Dim mDicWeight As New Dictionary(Of Integer, String)
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintKPIUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintKPIUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Get_General_Weightage()
        Try
            Dim mDecTotal, mDecAssign As Decimal
            mDecTotal = 0 : mDecAssign = 0
            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON AndAlso objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
                'S.SANDEEP [ 02 NOV 2013 ] -- START
                'objWSetting.Get_Weight_BasedOn(enAllocation.GENERAL, enWeight_Types.WEIGHT_FIELD2, mintKPIUnkid, mDecAssign, mDecTotal)
                objWSetting.Get_Weight_BasedOn(enAllocation.GENERAL, enWeight_Types.WEIGHT_FIELD2, mintKPIUnkid, mDecAssign, mDecTotal, , CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue))
                'S.SANDEEP [ 02 NOV 2013 ] -- END
                If mDicWeight.ContainsKey(enAllocation.GENERAL) = False Then
                    mDicWeight.Add(enAllocation.GENERAL, mDecTotal.ToString & "|" & mDecAssign.ToString)
                Else
                    mDicWeight(enAllocation.GENERAL) = mDecTotal.ToString & "|" & mDecAssign.ToString
                End If

                If menAction = enAction.EDIT_ONE Then
                    If txtWeight.Visible = True AndAlso txtWeight.Decimal > 0 Then
                        If mDecTotal - txtWeight.Decimal <= 0 Then
                            objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Assigned Weightage : ") & CStr(mDecTotal) & vbCrLf
                        Else
                            objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Assigned Weightage : ") & CStr(mDecTotal - txtWeight.Decimal) & vbCrLf
                        End If
                        objlblCaption.Text &= Language.getMessage(mstrModuleName, 7, "Total Weightage : ") & CStr(mDecTotal)
                    End If
                Else
                    objlblCaption.Text = Language.getMessage(mstrModuleName, 6, "Total Remaining Weightage : ") & CStr(mDecTotal - mDecAssign)
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Get_General_Weightage", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboObjective.BackColor = GUI.ColorComp


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                cboEmployee.BackColor = GUI.ColorComp
            End If
            'Pinkal (20-Jan-2012) -- End

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtWeight.BackColor = GUI.ColorComp
            'S.SANDEEP [ 28 DEC 2012 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtWeight.Decimal = objKPIMaster._Weight
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            Dim objObjective As New clsObjective_Master
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                objObjective._Objectiveunkid = objKPIMaster._Objectiveunkid
                cboEmployee.SelectedValue = objObjective._EmployeeId
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
            Else
                Call Get_General_Weightage()
                'S.SANDEEP [ 28 DEC 2012 ] -- END
            End If
            'Pinkal (20-Jan-2012) -- End

            txtCode.Text = objKPIMaster._Code
            txtName.Text = objKPIMaster._Name
            txtDescription.Text = objKPIMaster._Description

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboYear.SelectedValue = objObjective._Yearunkid
            cboPeriod.SelectedValue = objObjective._Periodunkid
            cboObjective.SelectedValue = objKPIMaster._Objectiveunkid
            If menAction = enAction.EDIT_ONE Then
                cboYear.Enabled = False : cboPeriod.Enabled = False
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objKPIMaster._Code = txtCode.Text
            objKPIMaster._Name = txtName.Text
            objKPIMaster._Description = txtDescription.Text
            objKPIMaster._Objectiveunkid = CInt(cboObjective.SelectedValue)


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                objKPIMaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
            End If
            'Pinkal (20-Jan-2012) -- End

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objKPIMaster._Weight = txtWeight.Decimal
            'S.SANDEEP [ 28 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objKpi As New clsKPI_Master
            Dim dsList As New DataSet
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objMaster As New clsMasterData
            'S.SANDEEP [ 28 DEC 2012 ] -- END


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsBSC_ByEmployee Then

                Dim objEmployee As New clsEmployee_Master
                'Sohail (23 Nov 2012) -- Start
                'TRA - ENHANCEMENT
                'dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
                'End If
                ''Sohail (23 Nov 2012) -- End
                'With cboEmployee
                '    .ValueMember = "employeeunkid"
                '    .DisplayMember = "employeename"
                '    .DataSource = dsList.Tables(0)
                'End With
                'S.SANDEEP [04 JUN 2015] -- END
            Else

                Dim objObjective As New clsObjective_Master
                dsList = objObjective.getComboList("List", True)
                With cboObjective
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                End With
            End If

            'Pinkal (20-Jan-2012) -- End

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objMaster.getComboListPAYYEAR("List", True, , , , True)
            dsList = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "List", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With
            'S.SANDEEP [ 28 DEC 2012 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsBSC_ByEmployee Then

                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is required information. Please select employee."), enMsgBoxStyle.Information)
                    cboEmployee.Select()
                    Return False
                End If

            End If

            If CInt(cboObjective.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Objective is required information. Please select objective."), enMsgBoxStyle.Information) '?1
                cboObjective.Focus()
                Return False
            End If


            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Name cannot be blank. Name is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Return False
            End If


            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Return False
            End If

            'Pinkal (20-Jan-2012) -- End

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If txtWeight.Visible = True Then
                If txtWeight.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Weight is mandatory information. Please give weigth in order to continue."), enMsgBoxStyle.Information)
                    txtWeight.Focus()
                    Return False
                End If

                If objWSetting._Weight_Optionid <> enWeight_Options.WEIGHT_BASED_ON Then
                    If CInt(cboObjective.SelectedValue) > 0 Then
                        If mDicWeight.ContainsKey(CInt(cboObjective.SelectedValue)) = True Then
                            If CDec(mDicWeight(CInt(cboObjective.SelectedValue)).Split(CChar("|"))(0)) < (txtWeight.Decimal + CDec(mDicWeight(CInt(cboObjective.SelectedValue)).Split(CChar("|"))(1))) Then
                                If CDec(mDicWeight(CInt(cboObjective.SelectedValue)).Split(CChar("|"))(1)) > 0 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of ") & CStr(CDec(mDicWeight(CInt(cboObjective.SelectedValue)).Split(CChar("|"))(0)) - CDec(mDicWeight(CInt(cboObjective.SelectedValue)).Split(CChar("|"))(1))), enMsgBoxStyle.Information)
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of ") & mDicWeight(CInt(cboObjective.SelectedValue)).Split(CChar("|"))(0).ToString, enMsgBoxStyle.Information)
                                End If
                                txtWeight.Focus()
                                Return False
                            End If
                        End If
                    End If
                Else
                    If ConfigParameter._Object._IsBSC_ByEmployee Then
                        If CInt(cboEmployee.SelectedValue) > 0 Then
                            If mDicWeight.ContainsKey(CInt(cboEmployee.SelectedValue)) = True Then
                                If CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(0)) < (txtWeight.Decimal + CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(1))) Then
                                    If CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(1)) > 0 AndAlso CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(1)) <> 100 Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of ") & CStr(CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(0)) - CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(1))), enMsgBoxStyle.Information)
                                    Else
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of ") & mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(0).ToString, enMsgBoxStyle.Information)
                                    End If
                                    txtWeight.Focus()
                                    Return False
                                End If
                            End If
                        End If
                    Else
                        If mDicWeight.ContainsKey(CInt(enAllocation.GENERAL)) = True Then
                            If CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(0)) < (txtWeight.Decimal + CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(1))) Then
                                If CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(1)) > 0 AndAlso CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(1)) <> 100 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of ") & CStr(CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(0)) - CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(1))), enMsgBoxStyle.Information)
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of ") & mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(0).ToString, enMsgBoxStyle.Information)
                                End If
                                txtWeight.Focus()
                                Return False
                            End If
                        End If
                    End If
                End If
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            'S.SHARMA [ 22 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If menAction <> enAction.EDIT_ONE Then

                Dim objSTran As New clsobjective_status_tran
                If ConfigParameter._Object._IsBSC_ByEmployee Then
                    If objSTran.isStatusExists(CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry You cannot save this KPI." & vbCrLf & _
                                                                                "Reason : There are some KPI(s). Which are final saved/submitted for approval for the selected period."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                objSTran = Nothing

                If objWSetting.Is_FinalSaved(mintObjectiveUnkid, _
                                  CInt(cboEmployee.SelectedValue), _
                                  CInt(cboYear.SelectedValue), _
                                  CInt(cboPeriod.SelectedValue), enWeight_Types.WEIGHT_FIELD2) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot save KPI for the selected period. Reason : Some KPI are final saved for the selected period."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                If menAction = enAction.EDIT_ONE Then
                    Dim sMsg As String = ""
                    sMsg = objWSetting.Is_Valid_Weight(mintKPIUnkid, txtWeight.Decimal, enWeight_Types.WEIGHT_FIELD2)
                    If sMsg.Trim.Length > 0 Then
                        eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
            End If
            'S.SHARMA [ 22 JAN 2013 ] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Public Sub OpenAddEdit_Form()
        Try
            Dim frm As New frmBSC_ObjectiveAddEdit
            Dim intRefId As Integer = -1

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objObjective As New clsObjective_Master
                dsList = objObjective.getComboList("List", True)
                With cboObjective
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objObjective = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "OpenAddEdit_Form", mstrModuleName)
        End Try
    End Sub
    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisibility()
        Try
            Select Case objWSetting._Weight_Optionid
                Case enWeight_Options.WEIGHT_EACH_ITEM
                    txtWeight.Visible = True
                    objlblCaption.Visible = True
                    lblWeight.Visible = True
                Case enWeight_Options.WEIGHT_BASED_ON
                    Select Case objWSetting._Weight_Typeid
                        Case enWeight_Types.WEIGHT_FIELD2
                            txtWeight.Visible = True
                            objlblCaption.Visible = True
                            lblWeight.Visible = True
                        Case Else
                            txtWeight.Visible = False
                            objlblCaption.Visible = False
                            lblWeight.Visible = False
                    End Select
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValid() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objKPIMaster.Update()
            Else
                blnFlag = objKPIMaster.Insert()
            End If

            If blnFlag = False And objKPIMaster._Message <> "" Then
                eZeeMsgBox.Show(objKPIMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objKPIMaster = Nothing
                    objKPIMaster = New clsKPI_Master
                    Call GetValue()

                    'Pinkal (20-Jan-2012) -- Start
                    'Enhancement : TRA Changes

                    If ConfigParameter._Object._IsBSC_ByEmployee Then
                        cboEmployee.Select()
                    Else
                        cboObjective.Select()
                    End If

                    'Pinkal (20-Jan-2012) -- End

                    cboEmployee.SelectedValue = mintEmployeeUnkid
                    cboObjective.SelectedValue = mintObjectiveUnkid

                Else
                    mintKPIUnkid = objKPIMaster._Kpiunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            Call objFrm.displayDialog(txtName.Text, objKPIMaster._Name1, objKPIMaster._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddObjective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddObjective.Click
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsBSC_ByEmployee Then

                If ConfigParameter._Object._IsBSCObjectiveSaved = False Then

                    If eZeeMsgBox.Show(Language.getMessage("frmBSC_ObjectiveList", 4, "As per your configuration setting Balanced Score Card Obective is set to base on employee. " & vbCrLf & " If you want to continue with the same setting please select 'Yes' or you can change the settings to 'general'." & vbCrLf & "Aruti Configuration -> Options -> General -> 'Balance Score Card Evaluation Settings'." & vbCrLf & " Do You wish to continue ? "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        OpenAddEdit_Form()
                    Else
                        Exit Sub
                    End If

                Else
                    OpenAddEdit_Form()
                End If

            Else
                OpenAddEdit_Form()
            End If

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddObjective_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchAllocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAllocation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- End


    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchObjective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchObjective.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboObjective.ValueMember
                .DisplayMember = cboObjective.DisplayMember
                .DataSource = CType(cboObjective.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboObjective.SelectedValue = frm.SelectedValue
                cboObjective.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchObjective_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Form Events "

    Private Sub frmBSC_KPIMeasureAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objKPIMaster = Nothing
    End Sub

    Private Sub frmBSC_KPIMeasureAddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_KPIMeasureAddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_KPIMeasureAddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_KPIMeasureAddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_KPIMeasureAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objKPIMaster = New clsKPI_Master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()



            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            Dim p As New Point
            p.X = 1
            If ConfigParameter._Object._IsBSC_ByEmployee = False Then
                p.Y = 28
                pnlKPI.Location = p
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Me.Size = New Size(363, 294)
                Me.Size = New Size(414, 338)
                'S.SANDEEP [ 28 DEC 2012 ] -- END
                cboObjective.Select()
            Else
                p.Y = 57
                pnlKPI.Location = p
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Me.Size = New Size(363, 318)
                Me.Size = New Size(414, 362)
                'S.SANDEEP [ 28 DEC 2012 ] -- END
                cboEmployee.Select()
            End If

            'Pinkal (20-Jan-2012) -- End

            Call setColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objKPIMaster._Kpiunkid = mintKPIUnkid

                'Pinkal (20-Jan-2012) -- Start
                'Enhancement : TRA Changes
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                'Pinkal (20-Jan-2012) -- End

            End If

            Call GetValue()

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 28 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_KPIMeasureAddEdit_Load", mstrModuleName)
        End Try

    End Sub

#End Region

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

#Region "Combobox Event"

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            'S.SANDEEP [ 30 MAY 2014 ] -- START
            objWSetting = New clsWeight_Setting(CInt(cboPeriod.SelectedValue))
            'S.SANDEEP [ 30 MAY 2014 ] -- END

            Dim objObjective As New clsObjective_Master
            Dim dsList As DataSet = objObjective.getComboList("List", True, , CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
            cboObjective.DisplayMember = "name"
            cboObjective.ValueMember = "id"
            cboObjective.DataSource = dsList.Tables("List")
            'If CType(sender, ComboBox).Name.ToUpper = "CBOEMPLOYEE" Then
            If CInt(cboEmployee.SelectedValue) > 0 Then
                mintEmployeeUnkid = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Select Case objWSetting._Weight_Optionid
                    Case enWeight_Options.WEIGHT_BASED_ON
                        Select Case objWSetting._Weight_Typeid
                            Case enWeight_Types.WEIGHT_FIELD2
                                Dim mDecTotal, mDecAssign As Decimal
                                mDecTotal = 0 : mDecAssign = 0
                                If ConfigParameter._Object._IsBSC_ByEmployee Then
                                    'S.SANDEEP [ 02 NOV 2013 ] -- START
                                    'objWSetting.Get_Weight_BasedOn(enAllocation.EMPLOYEE, enWeight_Types.WEIGHT_FIELD2, mintKPIUnkid, mDecAssign, mDecTotal, mintEmployeeUnkid, CInt(cboYear.SelectedValue))
                                    objWSetting.Get_Weight_BasedOn(enAllocation.EMPLOYEE, enWeight_Types.WEIGHT_FIELD2, mintKPIUnkid, mDecAssign, mDecTotal, mintEmployeeUnkid, CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue))
                                    'S.SANDEEP [ 02 NOV 2013 ] -- END
                                    If mDicWeight.ContainsKey(CInt(cboEmployee.SelectedValue)) = False Then
                                        mDicWeight.Add(CInt(cboEmployee.SelectedValue), mDecTotal.ToString & "|" & mDecAssign.ToString)
                                    Else
                                        mDicWeight(CInt(cboEmployee.SelectedValue)) = mDecTotal.ToString & "|" & mDecAssign.ToString
                                    End If
                                    If menAction = enAction.EDIT_ONE Then
                                        If txtWeight.Visible = True AndAlso txtWeight.Decimal > 0 Then
                                            If mDecTotal - txtWeight.Decimal <= 0 Then
                                                objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Assigned Weightage : ") & CStr(mDecTotal) & vbCrLf
                                            Else
                                                objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Assigned Weightage : ") & CStr(mDecAssign) & vbCrLf
                                            End If
                                            objlblCaption.Text &= Language.getMessage(mstrModuleName, 7, "Total Weightage : ") & CStr(mDecTotal)
                                        End If
                                    Else
                                        objlblCaption.Text = Language.getMessage(mstrModuleName, 6, "Total Remaining Weightage : ") & CStr(mDecTotal - mDecAssign)
                                    End If
                                End If
                        End Select
                End Select
            Else
                objlblCaption.Text = ""
                'S.SANDEEP [ 28 DEC 2012 ] -- END
            End If
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboObjective_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboObjective.SelectedIndexChanged
        Try
            If CInt(cboObjective.SelectedValue) > 0 Then
                mintObjectiveUnkid = CInt(cboObjective.SelectedValue)

                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Select Case objWSetting._Weight_Optionid
                    Case enWeight_Options.WEIGHT_EACH_ITEM
                        Dim mDecTotal, mDecAssign As Decimal
                        mDecTotal = 0 : mDecAssign = 0
                        objWSetting.Get_Each_Item_Weight(enWeight_Types.WEIGHT_FIELD1, CInt(cboObjective.SelectedValue), mintKPIUnkid, mDecAssign, mDecTotal)
                        If mDicWeight.ContainsKey(CInt(cboObjective.SelectedValue)) = False Then
                            mDicWeight.Add(CInt(cboObjective.SelectedValue), mDecTotal.ToString & "|" & mDecAssign.ToString)
                        Else
                            mDicWeight(CInt(cboObjective.SelectedValue)) = mDecTotal.ToString & "|" & mDecAssign.ToString
                        End If
                        If menAction = enAction.EDIT_ONE Then
                            If txtWeight.Visible = True AndAlso txtWeight.Decimal > 0 Then
                                If mDecTotal - txtWeight.Decimal <= 0 Then
                                    objlblCaption.Text = Language.getMessage(mstrModuleName, 11, "Total Assigned Weightage : ") & CStr(mDecTotal) & vbCrLf
                                Else
                                    objlblCaption.Text = Language.getMessage(mstrModuleName, 11, "Total Assigned Weightage : ") & CStr(mDecTotal - txtWeight.Decimal) & vbCrLf
                                End If
                                objlblCaption.Text &= Language.getMessage(mstrModuleName, 10, "Total Objective Weightage : ") & CStr(mDecTotal)
                            End If
                        Else
                            objlblCaption.Text = Language.getMessage(mstrModuleName, 5, "Total Remaining Weightage : ") & CStr(mDecTotal - mDecAssign)
                        End If
                End Select
                'S.SANDEEP [ 28 DEC 2012 ] -- END
            Else
                If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then objlblCaption.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As New DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region
    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Tab Events "

    Private Sub tabpDescription_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabpDescription.Enter
        Try
            objbtnOtherLanguage.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tabpDescription_Enter", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tabpKPI_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabpKPI.Enter
        Try
            objbtnOtherLanguage.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tabpKPI_Enter", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
            Call SetLanguage()
			
			Me.gbKPIMeasure.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbKPIMeasure.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbKPIMeasure.Text = Language._Object.getCaption(Me.gbKPIMeasure.Name, Me.gbKPIMeasure.Text)
			Me.lblAssessmentItemCode.Text = Language._Object.getCaption(Me.lblAssessmentItemCode.Name, Me.lblAssessmentItemCode.Text)
			Me.lblObjective.Text = Language._Object.getCaption(Me.lblObjective.Name, Me.lblObjective.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.tabpKPI.Text = Language._Object.getCaption(Me.tabpKPI.Name, Me.tabpKPI.Text)
            Me.tabpDescription.Text = Language._Object.getCaption(Me.tabpDescription.Name, Me.tabpDescription.Text)

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
            Language.setMessage(mstrModuleName, 1, "Employee is required information. Please select employee.")
            Language.setMessage(mstrModuleName, 2, "Objective is required information. Please select objective.")
            Language.setMessage(mstrModuleName, 3, "Name cannot be blank. Name is required information.")
            Language.setMessage(mstrModuleName, 4, "Code cannot be blank. Code is required information.")
            Language.setMessage("frmBSC_ObjectiveList", 4, "As per your configuration setting Balanced Score Card Obective is set to base on employee. " & vbCrLf & " If you want to continue with the same setting please select 'Yes' or you can change the settings to 'general'." & vbCrLf & "Aruti Configuration -> Options -> General -> 'Balance Score Card Evaluation Settings'." & vbCrLf & " Do You wish to continue ?")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

   

End Class
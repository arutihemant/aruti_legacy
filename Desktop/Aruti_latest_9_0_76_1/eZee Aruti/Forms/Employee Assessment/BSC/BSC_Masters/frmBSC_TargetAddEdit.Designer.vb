﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBSC_TargetAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBSC_TargetAddEdit))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbTargetAddEdit = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlTarget = New System.Windows.Forms.Panel
        Me.lblWeight = New System.Windows.Forms.Label
        Me.txtWeight = New eZee.TextBox.NumericTextBox
        Me.objbtnSearchKPI = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchObjective = New eZee.Common.eZeeGradientButton
        Me.cboKPI = New System.Windows.Forms.ComboBox
        Me.lblKPI = New System.Windows.Forms.Label
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.objbtnAddKPI = New eZee.Common.eZeeGradientButton
        Me.tabcTargets = New System.Windows.Forms.TabControl
        Me.tabpTarget = New System.Windows.Forms.TabPage
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.tabpDescription = New System.Windows.Forms.TabPage
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.cboObjective = New System.Windows.Forms.ComboBox
        Me.lblObjective = New System.Windows.Forms.Label
        Me.lblAssessmentItemCode = New System.Windows.Forms.Label
        Me.objbtnAddObjective = New eZee.Common.eZeeGradientButton
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblCaption = New System.Windows.Forms.RichTextBox
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblYear = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.pnlMain.SuspendLayout()
        Me.gbTargetAddEdit.SuspendLayout()
        Me.pnlTarget.SuspendLayout()
        Me.tabcTargets.SuspendLayout()
        Me.tabpTarget.SuspendLayout()
        Me.tabpDescription.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbTargetAddEdit)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(419, 362)
        Me.pnlMain.TabIndex = 3
        '
        'gbTargetAddEdit
        '
        Me.gbTargetAddEdit.BorderColor = System.Drawing.Color.Black
        Me.gbTargetAddEdit.Checked = False
        Me.gbTargetAddEdit.CollapseAllExceptThis = False
        Me.gbTargetAddEdit.CollapsedHoverImage = Nothing
        Me.gbTargetAddEdit.CollapsedNormalImage = Nothing
        Me.gbTargetAddEdit.CollapsedPressedImage = Nothing
        Me.gbTargetAddEdit.CollapseOnLoad = False
        Me.gbTargetAddEdit.Controls.Add(Me.pnlTarget)
        Me.gbTargetAddEdit.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbTargetAddEdit.Controls.Add(Me.cboEmployee)
        Me.gbTargetAddEdit.Controls.Add(Me.lblEmployee)
        Me.gbTargetAddEdit.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbTargetAddEdit.ExpandedHoverImage = Nothing
        Me.gbTargetAddEdit.ExpandedNormalImage = Nothing
        Me.gbTargetAddEdit.ExpandedPressedImage = Nothing
        Me.gbTargetAddEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTargetAddEdit.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTargetAddEdit.HeaderHeight = 25
        Me.gbTargetAddEdit.HeaderMessage = ""
        Me.gbTargetAddEdit.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTargetAddEdit.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTargetAddEdit.HeightOnCollapse = 0
        Me.gbTargetAddEdit.LeftTextSpace = 0
        Me.gbTargetAddEdit.Location = New System.Drawing.Point(0, 0)
        Me.gbTargetAddEdit.Name = "gbTargetAddEdit"
        Me.gbTargetAddEdit.OpenHeight = 119
        Me.gbTargetAddEdit.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTargetAddEdit.ShowBorder = True
        Me.gbTargetAddEdit.ShowCheckBox = False
        Me.gbTargetAddEdit.ShowCollapseButton = False
        Me.gbTargetAddEdit.ShowDefaultBorderColor = True
        Me.gbTargetAddEdit.ShowDownButton = False
        Me.gbTargetAddEdit.ShowHeader = True
        Me.gbTargetAddEdit.Size = New System.Drawing.Size(419, 307)
        Me.gbTargetAddEdit.TabIndex = 0
        Me.gbTargetAddEdit.Temp = 0
        Me.gbTargetAddEdit.Text = "Target Info."
        Me.gbTargetAddEdit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlTarget
        '
        Me.pnlTarget.Controls.Add(Me.cboPeriod)
        Me.pnlTarget.Controls.Add(Me.lblPeriod)
        Me.pnlTarget.Controls.Add(Me.lblYear)
        Me.pnlTarget.Controls.Add(Me.cboYear)
        Me.pnlTarget.Controls.Add(Me.lblWeight)
        Me.pnlTarget.Controls.Add(Me.txtWeight)
        Me.pnlTarget.Controls.Add(Me.objbtnSearchKPI)
        Me.pnlTarget.Controls.Add(Me.objbtnSearchObjective)
        Me.pnlTarget.Controls.Add(Me.cboKPI)
        Me.pnlTarget.Controls.Add(Me.lblKPI)
        Me.pnlTarget.Controls.Add(Me.objbtnOtherLanguage)
        Me.pnlTarget.Controls.Add(Me.objbtnAddKPI)
        Me.pnlTarget.Controls.Add(Me.tabcTargets)
        Me.pnlTarget.Controls.Add(Me.cboObjective)
        Me.pnlTarget.Controls.Add(Me.lblObjective)
        Me.pnlTarget.Controls.Add(Me.lblAssessmentItemCode)
        Me.pnlTarget.Controls.Add(Me.objbtnAddObjective)
        Me.pnlTarget.Controls.Add(Me.txtCode)
        Me.pnlTarget.Location = New System.Drawing.Point(3, 58)
        Me.pnlTarget.Name = "pnlTarget"
        Me.pnlTarget.Size = New System.Drawing.Size(413, 246)
        Me.pnlTarget.TabIndex = 321
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(236, 87)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(55, 15)
        Me.lblWeight.TabIndex = 327
        Me.lblWeight.Text = "Weight %"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWeight
        '
        Me.txtWeight.AllowNegative = False
        Me.txtWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtWeight.DigitsInGroup = 0
        Me.txtWeight.Flags = 65536
        Me.txtWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeight.Location = New System.Drawing.Point(297, 84)
        Me.txtWeight.MaxDecimalPlaces = 2
        Me.txtWeight.MaxWholeDigits = 3
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Prefix = ""
        Me.txtWeight.RangeMax = 1.7976931348623157E+308
        Me.txtWeight.RangeMin = -1.7976931348623157E+308
        Me.txtWeight.Size = New System.Drawing.Size(50, 21)
        Me.txtWeight.TabIndex = 328
        Me.txtWeight.Text = "0.00"
        Me.txtWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnSearchKPI
        '
        Me.objbtnSearchKPI.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchKPI.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchKPI.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchKPI.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchKPI.BorderSelected = False
        Me.objbtnSearchKPI.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchKPI.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchKPI.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchKPI.Location = New System.Drawing.Point(353, 58)
        Me.objbtnSearchKPI.Name = "objbtnSearchKPI"
        Me.objbtnSearchKPI.Size = New System.Drawing.Size(20, 19)
        Me.objbtnSearchKPI.TabIndex = 324
        '
        'objbtnSearchObjective
        '
        Me.objbtnSearchObjective.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchObjective.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchObjective.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchObjective.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchObjective.BorderSelected = False
        Me.objbtnSearchObjective.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchObjective.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchObjective.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchObjective.Location = New System.Drawing.Point(353, 31)
        Me.objbtnSearchObjective.Name = "objbtnSearchObjective"
        Me.objbtnSearchObjective.Size = New System.Drawing.Size(20, 19)
        Me.objbtnSearchObjective.TabIndex = 320
        '
        'cboKPI
        '
        Me.cboKPI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboKPI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboKPI.FormattingEnabled = True
        Me.cboKPI.Location = New System.Drawing.Point(88, 57)
        Me.cboKPI.Name = "cboKPI"
        Me.cboKPI.Size = New System.Drawing.Size(259, 21)
        Me.cboKPI.TabIndex = 322
        '
        'lblKPI
        '
        Me.lblKPI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblKPI.Location = New System.Drawing.Point(4, 60)
        Me.lblKPI.Name = "lblKPI"
        Me.lblKPI.Size = New System.Drawing.Size(79, 15)
        Me.lblKPI.TabIndex = 321
        Me.lblKPI.Text = "KPI / Measures"
        Me.lblKPI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(378, 106)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 10
        '
        'objbtnAddKPI
        '
        Me.objbtnAddKPI.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddKPI.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddKPI.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddKPI.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddKPI.BorderSelected = False
        Me.objbtnAddKPI.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddKPI.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddKPI.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddKPI.Location = New System.Drawing.Point(378, 57)
        Me.objbtnAddKPI.Name = "objbtnAddKPI"
        Me.objbtnAddKPI.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddKPI.TabIndex = 323
        '
        'tabcTargets
        '
        Me.tabcTargets.Controls.Add(Me.tabpTarget)
        Me.tabcTargets.Controls.Add(Me.tabpDescription)
        Me.tabcTargets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcTargets.Location = New System.Drawing.Point(7, 111)
        Me.tabcTargets.Name = "tabcTargets"
        Me.tabcTargets.SelectedIndex = 0
        Me.tabcTargets.Size = New System.Drawing.Size(392, 130)
        Me.tabcTargets.TabIndex = 314
        '
        'tabpTarget
        '
        Me.tabpTarget.Controls.Add(Me.txtName)
        Me.tabpTarget.Location = New System.Drawing.Point(4, 22)
        Me.tabpTarget.Name = "tabpTarget"
        Me.tabpTarget.Size = New System.Drawing.Size(384, 104)
        Me.tabpTarget.TabIndex = 0
        Me.tabpTarget.Tag = "tabpTarget"
        Me.tabpTarget.Text = "Target"
        Me.tabpTarget.UseVisualStyleBackColor = True
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(0, 0)
        Me.txtName.Multiline = True
        Me.txtName.Name = "txtName"
        Me.txtName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtName.Size = New System.Drawing.Size(384, 104)
        Me.txtName.TabIndex = 5
        '
        'tabpDescription
        '
        Me.tabpDescription.Controls.Add(Me.txtDescription)
        Me.tabpDescription.Location = New System.Drawing.Point(4, 22)
        Me.tabpDescription.Name = "tabpDescription"
        Me.tabpDescription.Size = New System.Drawing.Size(384, 96)
        Me.tabpDescription.TabIndex = 1
        Me.tabpDescription.Tag = "tabpDescription"
        Me.tabpDescription.Text = "Description"
        Me.tabpDescription.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(0, 0)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(384, 96)
        Me.txtDescription.TabIndex = 6
        '
        'cboObjective
        '
        Me.cboObjective.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboObjective.FormattingEnabled = True
        Me.cboObjective.Location = New System.Drawing.Point(88, 30)
        Me.cboObjective.Name = "cboObjective"
        Me.cboObjective.Size = New System.Drawing.Size(259, 21)
        Me.cboObjective.TabIndex = 2
        '
        'lblObjective
        '
        Me.lblObjective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObjective.Location = New System.Drawing.Point(4, 33)
        Me.lblObjective.Name = "lblObjective"
        Me.lblObjective.Size = New System.Drawing.Size(79, 15)
        Me.lblObjective.TabIndex = 0
        Me.lblObjective.Text = "Objective"
        Me.lblObjective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAssessmentItemCode
        '
        Me.lblAssessmentItemCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentItemCode.Location = New System.Drawing.Point(4, 87)
        Me.lblAssessmentItemCode.Name = "lblAssessmentItemCode"
        Me.lblAssessmentItemCode.Size = New System.Drawing.Size(79, 15)
        Me.lblAssessmentItemCode.TabIndex = 6
        Me.lblAssessmentItemCode.Text = "Code"
        Me.lblAssessmentItemCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddObjective
        '
        Me.objbtnAddObjective.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddObjective.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddObjective.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddObjective.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddObjective.BorderSelected = False
        Me.objbtnAddObjective.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddObjective.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddObjective.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddObjective.Location = New System.Drawing.Point(378, 30)
        Me.objbtnAddObjective.Name = "objbtnAddObjective"
        Me.objbtnAddObjective.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddObjective.TabIndex = 313
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(88, 84)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(142, 21)
        Me.txtCode.TabIndex = 4
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(355, 35)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(20, 19)
        Me.objbtnSearchEmployee.TabIndex = 319
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(91, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(259, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 37)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(77, 15)
        Me.lblEmployee.TabIndex = 317
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblCaption)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 307)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(419, 55)
        Me.objFooter.TabIndex = 1
        '
        'objlblCaption
        '
        Me.objlblCaption.BackColor = System.Drawing.SystemColors.Control
        Me.objlblCaption.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.objlblCaption.DetectUrls = False
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblCaption.Location = New System.Drawing.Point(3, 3)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.ReadOnly = True
        Me.objlblCaption.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.objlblCaption.ShortcutsEnabled = False
        Me.objlblCaption.Size = New System.Drawing.Size(200, 48)
        Me.objlblCaption.TabIndex = 9
        Me.objlblCaption.Text = ""
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(209, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(312, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 8
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(227, 3)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(120, 21)
        Me.cboPeriod.TabIndex = 334
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(177, 6)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(44, 15)
        Me.lblPeriod.TabIndex = 333
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(4, 6)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(79, 15)
        Me.lblYear.TabIndex = 331
        Me.lblYear.Text = "Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.DropDownWidth = 200
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(88, 3)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(83, 21)
        Me.cboYear.TabIndex = 332
        '
        'frmBSC_TargetAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(419, 362)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBSC_TargetAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Target"
        Me.pnlMain.ResumeLayout(False)
        Me.gbTargetAddEdit.ResumeLayout(False)
        Me.pnlTarget.ResumeLayout(False)
        Me.pnlTarget.PerformLayout()
        Me.tabcTargets.ResumeLayout(False)
        Me.tabpTarget.ResumeLayout(False)
        Me.tabpTarget.PerformLayout()
        Me.tabpDescription.ResumeLayout(False)
        Me.tabpDescription.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbTargetAddEdit As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboObjective As System.Windows.Forms.ComboBox
    Friend WithEvents lblAssessmentItemCode As System.Windows.Forms.Label
    Friend WithEvents lblObjective As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnAddObjective As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlTarget As System.Windows.Forms.Panel
    Friend WithEvents tabcTargets As System.Windows.Forms.TabControl
    Friend WithEvents tabpTarget As System.Windows.Forms.TabPage
    Friend WithEvents tabpDescription As System.Windows.Forms.TabPage
    Friend WithEvents objbtnSearchKPI As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchObjective As eZee.Common.eZeeGradientButton
    Friend WithEvents cboKPI As System.Windows.Forms.ComboBox
    Friend WithEvents lblKPI As System.Windows.Forms.Label
    Friend WithEvents objbtnAddKPI As eZee.Common.eZeeGradientButton
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents txtWeight As eZee.TextBox.NumericTextBox
    Friend WithEvents objlblCaption As System.Windows.Forms.RichTextBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
End Class

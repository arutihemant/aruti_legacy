Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.Diagnostics
Imports System.Drawing.Drawing2D

Public Class clsMergeCell
    Inherits DataGridViewTextBoxCell
    Private m_nLeftColumn As Integer = 0
    Private m_nRightColumn As Integer = 0

    Private mTextAllignment As StringAlignment = StringAlignment.Near
    Private mForeColor As Color = Color.Black
    Private mBackColor As Color = SystemColors.Control
    Private mimgEndImage As Image = Nothing

    Public Property EndImage() As Image
        Get
            Return mimgEndImage
        End Get
        Set(ByVal value As Image)
            mimgEndImage = value
        End Set
    End Property

    Public Property BaclColor() As Color
        Get
            Return mBackColor
        End Get
        Set(ByVal value As Color)
            mBackColor = value
        End Set
    End Property

    Public Property ForeColor() As Color
        Get
            Return mForeColor
        End Get
        Set(ByVal value As Color)
            mForeColor = value
        End Set
    End Property

    Public Property TextAllignment() As StringAlignment
        Get
            Return mTextAllignment
        End Get
        Set(ByVal value As StringAlignment)
            mTextAllignment = value
        End Set
    End Property

    ''' <summary> 
    ''' Column Index of the left-most cell to be merged. 
    ''' This cell controls the merged text. 
    ''' </summary> 
    Public Property LeftColumn() As Integer
        Get
            Return m_nLeftColumn
        End Get
        Set(ByVal value As Integer)
            m_nLeftColumn = value
        End Set
    End Property

    ''' <summary> 
    ''' Column Index of the right-most cell to be merged 
    ''' </summary> 
    Public Property RightColumn() As Integer
        Get
            Return m_nRightColumn
        End Get
        Set(ByVal value As Integer)
            m_nRightColumn = value
        End Set
    End Property

    Protected Overloads Overrides Sub Paint(ByVal graphics As Graphics, ByVal clipBounds As Rectangle, ByVal cellBounds As Rectangle, ByVal rowIndex As Integer, ByVal cellState As DataGridViewElementStates, ByVal value As Object, _
    ByVal formattedValue As Object, ByVal errorText As String, ByVal cellStyle As DataGridViewCellStyle, ByVal advancedBorderStyle As DataGridViewAdvancedBorderStyle, ByVal paintParts As DataGridViewPaintParts)
        Try
            Dim mergeindex As Integer = ColumnIndex - m_nLeftColumn
            Dim i As Integer
            Dim nWidth As Integer
            Dim nWidthLeft As Integer
            Dim strText As String

            Dim pen As New Pen(Brushes.Black)

            ' Draw the background 
            graphics.FillRectangle(New SolidBrush(mBackColor), cellBounds)
            'graphics.FillRectangle(New LinearGradientBrush(New RectangleF(cellBounds.X, cellBounds.Y + (cellBounds.Y / 2), cellBounds.Width, cellBounds.Height - (cellBounds.Y / 2)), Color.White, mBackColor, LinearGradientMode.Vertical), cellBounds)
            'graphics.FillRectangle(New LinearGradientBrush(New RectangleF(cellBounds.X, cellBounds.Y, cellBounds.Width, cellBounds.Height - (cellBounds.Y / 2)), mBackColor, Color.White, LinearGradientMode.Vertical), cellBounds)
            'If ColumnIndex = m_nRightColumn Then
            '    graphics.FillRectangle(New SolidBrush(mBackColor), cellBounds.X, cellBounds.Y, cellBounds.Width - 20, cellBounds.Height)
            'Else
            '    graphics.FillRectangle(New SolidBrush(mBackColor), cellBounds)
            'End If

            ' Draw the separator for rows 
            graphics.DrawLine(New Pen(New SolidBrush(SystemColors.ControlDark)), cellBounds.Left, cellBounds.Bottom - 1, cellBounds.Right, cellBounds.Bottom - 1)

            ' Draw the right vertical line for the cell 
            If ColumnIndex = m_nRightColumn Then
                graphics.DrawLine(New Pen(New SolidBrush(SystemColors.ControlDark)), cellBounds.Right - 1, cellBounds.Top, cellBounds.Right - 1, cellBounds.Bottom)
                graphics.DrawImage(mimgEndImage, cellBounds.Right - 22, cellBounds.Top, 21, 21)
            End If

            ' Draw the text 
            Dim rectDest As RectangleF = RectangleF.Empty
            Dim sf As New StringFormat()
            sf.Alignment = mTextAllignment
            sf.LineAlignment = StringAlignment.Center
            'sf.Trimming = StringTrimming.EllipsisCharacter
            sf.Trimming = StringTrimming.Word
            ' Determine the total width of the merged cell 
            nWidth = 0
            For i = m_nLeftColumn To m_nRightColumn
                nWidth += Me.OwningRow.Cells(i).Size.Width
            Next

            ' Determine the width before the current cell. 
            nWidthLeft = 0
            For i = m_nLeftColumn To ColumnIndex - 1
                nWidthLeft += Me.OwningRow.Cells(i).Size.Width
            Next

            ' Retrieve the text to be displayed 
            strText = Me.OwningRow.Cells(m_nLeftColumn).Value.ToString()


            Dim br As SolidBrush
            br = New SolidBrush(mForeColor)

            Dim fn As Font
            fn = New Font("Arial", 8, FontStyle.Regular)

            'rectDest = New RectangleF(cellBounds.Left - nWidthLeft, cellBounds.Top, nWidth, cellBounds.Height)
            rectDest = New RectangleF(cellBounds.Left - nWidthLeft, cellBounds.Top + (cellBounds.Height / 2) - (fn.Height / 2), nWidth, fn.Height)
            graphics.DrawString(strText, fn, br, rectDest, sf)
        Catch ex As Exception
            Trace.WriteLine(ex.ToString())
        End Try
    End Sub

    'Dipti L Jain (25 Nov 2008)---start
    'Issue : Jonny 
    'Public Sub MakeMerge(ByRef dg As DataGridView, ByVal intRowIndex As Integer, ByVal intStartIndex As Integer,ByVal intEndIndex As Integer, ByVal BackColor As Color,ByVal ForeColor As Color, ByVal strDisplayData As String ,ByVal strTagString As String ,ByVal img As Image )
    Public Sub MakeMerge(ByRef dg As DataGridView, _
                         ByVal intRowIndex As Integer, _
                         ByVal intStartIndex As Integer, _
                         ByVal intEndIndex As Integer, _
                         ByVal BackColor As Color, _
                         ByVal ForeColor As Color, _
                         Optional ByVal strDisplayData As String = Nothing, _
                         Optional ByVal strTagString As String = Nothing, _
                         Optional ByVal img As Image = Nothing)
        'Dipti L Jain (25 Nov 2008)---end

        Try
            Dim pCell As clsMergeCell
            Dim j As Integer
            For j = intStartIndex To intEndIndex
                dg.Rows(intRowIndex).Cells(j) = New clsMergeCell()
                pCell = DirectCast(dg.Rows(intRowIndex).Cells(j), clsMergeCell)

                'Dipti L Jain (25 Nov 2008)---start
                'Issue : Jonny 
                'dg.Rows(intRowIndex).Cells(j).Value = strDisplayData
                If Not strDisplayData Is Nothing Then
                    dg.Rows(intRowIndex).Cells(j).Value = strDisplayData
                End If
                'Dipti L Jain (25 Nov 2008)---end

                'Dipti L Jain (25 Nov 2008)---start
                'Issue : Jonny 
                'dg.Rows(intRowIndex).Cells(j).Tag = strTagString
                If Not strTagString Is Nothing Then
                    dg.Rows(intRowIndex).Cells(j).Tag = strTagString
                End If
                'Dipti L Jain (25 Nov 2008)---end

                pCell.LeftColumn = intStartIndex
                pCell.RightColumn = intEndIndex
                pCell.ForeColor = ForeColor
                pCell.BaclColor = BackColor

                'Dipti L Jain (25 Nov 2008)---start
                'Issue : Jonny 
                'pCell.EndImage = img
                If Not img Is Nothing Then
                    pCell.EndImage = img
                End If
                'Dipti L Jain (25 Nov 2008)---end
            Next
        Catch ex As Exception

        End Try
    End Sub

End Class


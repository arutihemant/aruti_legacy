﻿
Public Class GroupByGrid
    Inherits DataGridView

    Private intLastGroupColIndex As Integer = -1
    Private mblnIgnoreFirstColumn As Boolean = False

    Public Property IgnoreFirstColumn() As Boolean
        Get
            Return mblnIgnoreFirstColumn
        End Get
        Set(ByVal value As Boolean)
            mblnIgnoreFirstColumn = value
        End Set
    End Property

    Protected Overrides Sub OnCellFormatting(ByVal args As DataGridViewCellFormattingEventArgs)
        ' Call home to base
        MyBase.OnCellFormatting(args)

        ' First row always displays
        If args.RowIndex = 0 Then
            Return
        End If

        If IsRepeatedCellValue(args.RowIndex, args.ColumnIndex) Then
            args.Value = String.Empty
            args.FormattingApplied = True
        End If
    End Sub

    Private Function IsRepeatedCellValue(ByVal rowIndex As Integer, ByVal colIndex As Integer) As Boolean
        Dim currCell As DataGridViewCell = Rows(rowIndex).Cells(colIndex)
        Dim prevCell As DataGridViewCell = Rows(rowIndex - 1).Cells(colIndex)
        Dim colStart As Integer = 0
        If mblnIgnoreFirstColumn = True Then
            colStart = 1
        End If
        If IsDBNull(currCell.Value) = True Then Exit Function

        If mblnIgnoreFirstColumn = True AndAlso colIndex = 0 Then
            Return False
        End If

        If (colIndex = colStart AndAlso (currCell.Value = prevCell.Value OrElse (currCell.Value IsNot Nothing AndAlso prevCell.Value IsNot Nothing AndAlso currCell.Value.ToString() = prevCell.Value.ToString()))) Then
            Return True
        ElseIf (colIndex > colStart AndAlso (currCell.Value Is prevCell.Value OrElse (currCell.Value IsNot Nothing AndAlso prevCell.Value IsNot Nothing AndAlso currCell.Value.ToString() = prevCell.Value.ToString()))) Then
            For i As Integer = colStart To colIndex - 1
                Dim ccell As DataGridViewCell = Rows(rowIndex).Cells(i)
                Dim pcell As DataGridViewCell = Rows(rowIndex - 1).Cells(i)

                If IsDBNull(ccell.Value) = True Then Exit Function
                If IsDBNull(prevCell.Value) = True Then Exit Function
                'If IsDBNull(ccell.Value) = True Then Continue For
                'If IsDBNull(prevCell.Value) = True Then Continue For

                If Not (ccell.Value = pcell.Value OrElse (ccell.Value IsNot Nothing AndAlso pcell.Value IsNot Nothing AndAlso ccell.Value.ToString() = pcell.Value.ToString())) Then
                    Return False
                End If
            Next
            Return True
        End If

        If (currCell.Value Is prevCell.Value) OrElse (currCell.Value IsNot Nothing AndAlso prevCell.Value IsNot Nothing AndAlso currCell.Value.ToString() = prevCell.Value.ToString()) Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Overrides Sub OnCellPainting(ByVal args As DataGridViewCellPaintingEventArgs)
        MyBase.OnCellPainting(args)

        args.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None

        ' Ignore column and row headers and first row
        If args.RowIndex < 1 OrElse args.ColumnIndex < 0 Then
            Return
        End If

        If IsRepeatedCellValue(args.RowIndex, args.ColumnIndex) Then
            args.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None
            intLastGroupColIndex = args.ColumnIndex
        Else
            'If args.ColumnIndex > 0 Then
            '    'Dim a As New DataGridViewAdvancedBorderStyle
            '    ''a.Top = Me.Rows(args.RowIndex).Cells(args.ColumnIndex - 1).Style
            '    'args.AdvancedBorderStyle.Top = a.All
            '    Dim cidx As Integer = If(intLastGroupColIndex > -1, intLastGroupColIndex, 0)
            '    If Me.Rows(args.RowIndex).Cells(cidx).EditedFormattedValue.ToString = "" Then
            '        args.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None
            '    Else
            '        args.AdvancedBorderStyle.Top = AdvancedCellBorderStyle.Top
            '    End If
            'Else
            '    args.AdvancedBorderStyle.Top = AdvancedCellBorderStyle.Top
            'End If
            args.AdvancedBorderStyle.Top = AdvancedCellBorderStyle.Top
        End If

        'Dim a As New DataGridViewAdvancedBorderStyle
        'Dim b As New DataGridViewAdvancedBorderStyle
        'a.Top = DataGridViewAdvancedCellBorderStyle.Single
        'b.Top = DataGridViewAdvancedCellBorderStyle.Single



        'If intLastGroupColIndex > 0 AndAlso Me.Rows(args.RowIndex + 1) IsNot Nothing Then
        '    If Me.Rows(args.RowIndex + 1).Cells(intLastGroupColIndex).Value = args.Value.ToString AndAlso Me.Rows(args.RowIndex).Cells(intLastGroupColIndex).EditedFormattedValue.ToString <> "" Then

        '        args.AdvancedBorderStyle.Top = AdvancedCellBorderStyle.Top

        '    End If
        'End If
        'If intLastGroupColIndex > 0 Then
        '    If Me.Rows(args.RowIndex - 1).Cells(intLastGroupColIndex).EditedFormattedValue <> "" Then
        '        Me.Rows(args.RowIndex - 1).Cells(args.ColumnIndex).AdjustCellBorderStyle(a, b, True, True, True, True)
        '    End If
        'End If

        If args.RowIndex = RowCount - 1 Then
            args.AdvancedBorderStyle.Bottom = AdvancedCellBorderStyle.Bottom
        End If

        'If args.ColumnIndex = Me.ColumnCount - 2 Then
        '    intLastGroupColIndex = -1
        'End If
    End Sub
End Class

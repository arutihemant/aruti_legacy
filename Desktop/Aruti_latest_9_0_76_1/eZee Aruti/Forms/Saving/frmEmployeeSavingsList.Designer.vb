﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeSavingsList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeSavingsList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lvEmployeeSavings = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmpCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmpName = New System.Windows.Forms.ColumnHeader
        Me.colhSavingCode = New System.Windows.Forms.ColumnHeader
        Me.colhSavingScheme = New System.Windows.Forms.ColumnHeader
        Me.colhPayPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhBalAmount = New System.Windows.Forms.ColumnHeader
        Me.colhInterestRate = New System.Windows.Forms.ColumnHeader
        Me.colhInterestAccrued = New System.Windows.Forms.ColumnHeader
        Me.colhContribution = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.colhStopDate = New System.Windows.Forms.ColumnHeader
        Me.colhRemarks = New System.Windows.Forms.ColumnHeader
        Me.colhTotalContribution = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuPayment = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDeposit = New System.Windows.Forms.ToolStripMenuItem
        Me.btnPayment = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnChangeStatus = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboSavingsScheme = New System.Windows.Forms.ComboBox
        Me.lblSavingScheme = New System.Windows.Forms.Label
        Me.objSavingScheme = New eZee.Common.eZeeGradientButton
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtContribution = New eZee.TextBox.NumericTextBox
        Me.lblContribution = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.objbtnEmployeeSavingSearch = New eZee.Common.eZeeGradientButton
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lvEmployeeSavings)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(797, 439)
        Me.pnlMainInfo.TabIndex = 0
        '
        'lvEmployeeSavings
        '
        Me.lvEmployeeSavings.BackColorOnChecked = True
        Me.lvEmployeeSavings.ColumnHeaders = Nothing
        Me.lvEmployeeSavings.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmpCode, Me.colhEmpName, Me.colhSavingCode, Me.colhSavingScheme, Me.colhPayPeriod, Me.colhBalAmount, Me.colhInterestRate, Me.colhInterestAccrued, Me.colhContribution, Me.colhStatus, Me.colhStopDate, Me.colhRemarks, Me.colhTotalContribution})
        Me.lvEmployeeSavings.CompulsoryColumns = ""
        Me.lvEmployeeSavings.FullRowSelect = True
        Me.lvEmployeeSavings.GridLines = True
        Me.lvEmployeeSavings.GroupingColumn = Nothing
        Me.lvEmployeeSavings.HideSelection = False
        Me.lvEmployeeSavings.Location = New System.Drawing.Point(12, 162)
        Me.lvEmployeeSavings.MinColumnWidth = 50
        Me.lvEmployeeSavings.MultiSelect = False
        Me.lvEmployeeSavings.Name = "lvEmployeeSavings"
        Me.lvEmployeeSavings.OptionalColumns = ""
        Me.lvEmployeeSavings.ShowMoreItem = False
        Me.lvEmployeeSavings.ShowSaveItem = False
        Me.lvEmployeeSavings.ShowSelectAll = True
        Me.lvEmployeeSavings.ShowSizeAllColumnsToFit = True
        Me.lvEmployeeSavings.Size = New System.Drawing.Size(773, 220)
        Me.lvEmployeeSavings.Sortable = True
        Me.lvEmployeeSavings.TabIndex = 4
        Me.lvEmployeeSavings.UseCompatibleStateImageBehavior = False
        Me.lvEmployeeSavings.View = System.Windows.Forms.View.Details
        '
        'colhEmpCode
        '
        Me.colhEmpCode.Tag = "colhEmpCode"
        Me.colhEmpCode.Text = "Emp Code"
        '
        'colhEmpName
        '
        Me.colhEmpName.Tag = "colhEmpName"
        Me.colhEmpName.Text = "Employee"
        Me.colhEmpName.Width = 100
        '
        'colhSavingCode
        '
        Me.colhSavingCode.Tag = "colhSavingCode"
        Me.colhSavingCode.Text = "Savings Code"
        '
        'colhSavingScheme
        '
        Me.colhSavingScheme.Tag = "colhSavingScheme"
        Me.colhSavingScheme.Text = "Saving Scheme"
        Me.colhSavingScheme.Width = 110
        '
        'colhPayPeriod
        '
        Me.colhPayPeriod.Tag = "colhPayPeriod"
        Me.colhPayPeriod.Text = "Pay Period"
        Me.colhPayPeriod.Width = 70
        '
        'colhBalAmount
        '
        Me.colhBalAmount.Tag = "colhBalAmount"
        Me.colhBalAmount.Text = "Balance Amount"
        Me.colhBalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhBalAmount.Width = 100
        '
        'colhInterestRate
        '
        Me.colhInterestRate.Tag = "colhInterestRate"
        Me.colhInterestRate.Text = "Interest Rate"
        Me.colhInterestRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhInterestRate.Width = 80
        '
        'colhInterestAccrued
        '
        Me.colhInterestAccrued.Tag = "colhInterestAccrued"
        Me.colhInterestAccrued.Text = "Interest Accrued"
        Me.colhInterestAccrued.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhInterestAccrued.Width = 100
        '
        'colhContribution
        '
        Me.colhContribution.Tag = "colhContribution"
        Me.colhContribution.Text = "Contribution"
        Me.colhContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhContribution.Width = 100
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 85
        '
        'colhStopDate
        '
        Me.colhStopDate.Tag = "colhStopDate"
        Me.colhStopDate.Text = "Stop Date"
        Me.colhStopDate.Width = 100
        '
        'colhRemarks
        '
        Me.colhRemarks.Tag = "colhRemarks"
        Me.colhRemarks.Text = "Remarks"
        Me.colhRemarks.Width = 150
        '
        'colhTotalContribution
        '
        Me.colhTotalContribution.Tag = "colhTotalContribution"
        Me.colhTotalContribution.Text = "TotalContribution"
        Me.colhTotalContribution.Width = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(797, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Employee Savings List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnPayment)
        Me.objFooter.Controls.Add(Me.btnChangeStatus)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 384)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(797, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(98, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 6
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPayment, Me.mnuDeposit})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(206, 48)
        '
        'mnuPayment
        '
        Me.mnuPayment.Name = "mnuPayment"
        Me.mnuPayment.Size = New System.Drawing.Size(205, 22)
        Me.mnuPayment.Text = "&Repayment / Withdrawal"
        '
        'mnuDeposit
        '
        Me.mnuDeposit.Name = "mnuDeposit"
        Me.mnuDeposit.Size = New System.Drawing.Size(205, 22)
        Me.mnuDeposit.Text = "&Deposit"
        '
        'btnPayment
        '
        Me.btnPayment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPayment.BackColor = System.Drawing.Color.White
        Me.btnPayment.BackgroundImage = CType(resources.GetObject("btnPayment.BackgroundImage"), System.Drawing.Image)
        Me.btnPayment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPayment.BorderColor = System.Drawing.Color.Empty
        Me.btnPayment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPayment.FlatAppearance.BorderSize = 0
        Me.btnPayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPayment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPayment.ForeColor = System.Drawing.Color.Black
        Me.btnPayment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPayment.GradientForeColor = System.Drawing.Color.Black
        Me.btnPayment.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPayment.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPayment.Location = New System.Drawing.Point(116, 13)
        Me.btnPayment.Name = "btnPayment"
        Me.btnPayment.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPayment.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPayment.Size = New System.Drawing.Size(35, 30)
        Me.btnPayment.TabIndex = 6
        Me.btnPayment.Text = "&Payment"
        Me.btnPayment.UseVisualStyleBackColor = True
        Me.btnPayment.Visible = False
        '
        'btnChangeStatus
        '
        Me.btnChangeStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnChangeStatus.BackColor = System.Drawing.Color.White
        Me.btnChangeStatus.BackgroundImage = CType(resources.GetObject("btnChangeStatus.BackgroundImage"), System.Drawing.Image)
        Me.btnChangeStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnChangeStatus.BorderColor = System.Drawing.Color.Empty
        Me.btnChangeStatus.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnChangeStatus.FlatAppearance.BorderSize = 0
        Me.btnChangeStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnChangeStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChangeStatus.ForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnChangeStatus.GradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeStatus.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.Location = New System.Drawing.Point(245, 13)
        Me.btnChangeStatus.Name = "btnChangeStatus"
        Me.btnChangeStatus.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeStatus.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.Size = New System.Drawing.Size(128, 30)
        Me.btnChangeStatus.TabIndex = 5
        Me.btnChangeStatus.Text = "&Change Status"
        Me.btnChangeStatus.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(585, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(482, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(379, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(688, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboSavingsScheme)
        Me.gbFilterCriteria.Controls.Add(Me.lblSavingScheme)
        Me.gbFilterCriteria.Controls.Add(Me.objSavingScheme)
        Me.gbFilterCriteria.Controls.Add(Me.txtRemark)
        Me.gbFilterCriteria.Controls.Add(Me.lblRemark)
        Me.gbFilterCriteria.Controls.Add(Me.txtContribution)
        Me.gbFilterCriteria.Controls.Add(Me.lblContribution)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnEmployeeSavingSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(773, 90)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSavingsScheme
        '
        Me.cboSavingsScheme.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboSavingsScheme.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboSavingsScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSavingsScheme.DropDownWidth = 300
        Me.cboSavingsScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSavingsScheme.FormattingEnabled = True
        Me.cboSavingsScheme.Location = New System.Drawing.Point(99, 62)
        Me.cboSavingsScheme.Name = "cboSavingsScheme"
        Me.cboSavingsScheme.Size = New System.Drawing.Size(255, 21)
        Me.cboSavingsScheme.TabIndex = 8
        '
        'lblSavingScheme
        '
        Me.lblSavingScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSavingScheme.Location = New System.Drawing.Point(7, 65)
        Me.lblSavingScheme.Name = "lblSavingScheme"
        Me.lblSavingScheme.Size = New System.Drawing.Size(86, 15)
        Me.lblSavingScheme.TabIndex = 7
        Me.lblSavingScheme.Text = "Saving Scheme"
        '
        'objSavingScheme
        '
        Me.objSavingScheme.BackColor = System.Drawing.Color.Transparent
        Me.objSavingScheme.BackColor1 = System.Drawing.Color.Transparent
        Me.objSavingScheme.BackColor2 = System.Drawing.Color.Transparent
        Me.objSavingScheme.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSavingScheme.BorderSelected = False
        Me.objSavingScheme.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSavingScheme.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSavingScheme.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSavingScheme.Location = New System.Drawing.Point(360, 62)
        Me.objSavingScheme.Name = "objSavingScheme"
        Me.objSavingScheme.Size = New System.Drawing.Size(21, 21)
        Me.objSavingScheme.TabIndex = 9
        '
        'txtRemark
        '
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.Location = New System.Drawing.Point(661, 62)
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(105, 21)
        Me.txtRemark.TabIndex = 13
        Me.txtRemark.Visible = False
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(594, 65)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(65, 15)
        Me.lblRemark.TabIndex = 12
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.Visible = False
        '
        'txtContribution
        '
        Me.txtContribution.AllowNegative = True
        Me.txtContribution.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtContribution.DigitsInGroup = 0
        Me.txtContribution.Flags = 0
        Me.txtContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContribution.Location = New System.Drawing.Point(470, 62)
        Me.txtContribution.MaxDecimalPlaces = 6
        Me.txtContribution.MaxWholeDigits = 21
        Me.txtContribution.Name = "txtContribution"
        Me.txtContribution.Prefix = ""
        Me.txtContribution.RangeMax = 1.7976931348623157E+308
        Me.txtContribution.RangeMin = -1.7976931348623157E+308
        Me.txtContribution.Size = New System.Drawing.Size(118, 21)
        Me.txtContribution.TabIndex = 11
        Me.txtContribution.Text = "0"
        Me.txtContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblContribution
        '
        Me.lblContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContribution.Location = New System.Drawing.Point(384, 65)
        Me.lblContribution.Name = "lblContribution"
        Me.lblContribution.Size = New System.Drawing.Size(83, 15)
        Me.lblContribution.TabIndex = 10
        Me.lblContribution.Text = "Contribution"
        Me.lblContribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(99, 35)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(255, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(7, 38)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(86, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(659, 35)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(107, 21)
        Me.cboStatus.TabIndex = 6
        '
        'objbtnEmployeeSavingSearch
        '
        Me.objbtnEmployeeSavingSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnEmployeeSavingSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnEmployeeSavingSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnEmployeeSavingSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnEmployeeSavingSearch.BorderSelected = False
        Me.objbtnEmployeeSavingSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnEmployeeSavingSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnEmployeeSavingSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnEmployeeSavingSearch.Location = New System.Drawing.Point(360, 35)
        Me.objbtnEmployeeSavingSearch.Name = "objbtnEmployeeSavingSearch"
        Me.objbtnEmployeeSavingSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnEmployeeSavingSearch.TabIndex = 2
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(594, 38)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(62, 15)
        Me.lblStatus.TabIndex = 5
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(471, 35)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(117, 21)
        Me.cboPayPeriod.TabIndex = 4
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(384, 36)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(84, 16)
        Me.lblPayPeriod.TabIndex = 3
        Me.lblPayPeriod.Text = "Pay Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(746, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(723, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'frmEmployeeSavingsList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(797, 439)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeSavingsList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Savings List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtContribution As eZee.TextBox.NumericTextBox
    Friend WithEvents lblContribution As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnEmployeeSavingSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents btnChangeStatus As eZee.Common.eZeeLightButton
    Friend WithEvents lvEmployeeSavings As eZee.Common.eZeeListView
    Friend WithEvents colhSavingScheme As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPayPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBalAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInterestAccrued As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhContribution As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemarks As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents colhEmpName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTotalContribution As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnPayment As eZee.Common.eZeeLightButton
    Friend WithEvents colhStopDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboSavingsScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblSavingScheme As System.Windows.Forms.Label
    Friend WithEvents objSavingScheme As eZee.Common.eZeeGradientButton
    Friend WithEvents colhEmpCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSavingCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInterestRate As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuPayment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDeposit As System.Windows.Forms.ToolStripMenuItem
End Class

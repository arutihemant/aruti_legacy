﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBenefitplanList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBenefitplanList))
        Me.gbBenefitplan = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboBenefitgroup = New System.Windows.Forms.ComboBox
        Me.lblBenefitGroup = New System.Windows.Forms.Label
        Me.pnlBenefitplan = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvBenefitplan = New eZee.Common.eZeeListView(Me.components)
        Me.colhBenefitgroup = New System.Windows.Forms.ColumnHeader
        Me.colhBenefitplancode = New System.Windows.Forms.ColumnHeader
        Me.colhBenefitplanname = New System.Windows.Forms.ColumnHeader
        Me.colhBenefitplandesc = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbBenefitplan.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBenefitplan.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbBenefitplan
        '
        Me.gbBenefitplan.BorderColor = System.Drawing.Color.Black
        Me.gbBenefitplan.Checked = False
        Me.gbBenefitplan.CollapseAllExceptThis = False
        Me.gbBenefitplan.CollapsedHoverImage = Nothing
        Me.gbBenefitplan.CollapsedNormalImage = Nothing
        Me.gbBenefitplan.CollapsedPressedImage = Nothing
        Me.gbBenefitplan.CollapseOnLoad = False
        Me.gbBenefitplan.Controls.Add(Me.objbtnReset)
        Me.gbBenefitplan.Controls.Add(Me.objbtnSearch)
        Me.gbBenefitplan.Controls.Add(Me.cboBenefitgroup)
        Me.gbBenefitplan.Controls.Add(Me.lblBenefitGroup)
        Me.gbBenefitplan.ExpandedHoverImage = Nothing
        Me.gbBenefitplan.ExpandedNormalImage = Nothing
        Me.gbBenefitplan.ExpandedPressedImage = Nothing
        Me.gbBenefitplan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBenefitplan.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBenefitplan.HeaderHeight = 25
        Me.gbBenefitplan.HeightOnCollapse = 0
        Me.gbBenefitplan.LeftTextSpace = 0
        Me.gbBenefitplan.Location = New System.Drawing.Point(7, 66)
        Me.gbBenefitplan.Name = "gbBenefitplan"
        Me.gbBenefitplan.OpenHeight = 91
        Me.gbBenefitplan.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBenefitplan.ShowBorder = True
        Me.gbBenefitplan.ShowCheckBox = False
        Me.gbBenefitplan.ShowCollapseButton = False
        Me.gbBenefitplan.ShowDefaultBorderColor = True
        Me.gbBenefitplan.ShowDownButton = False
        Me.gbBenefitplan.ShowHeader = True
        Me.gbBenefitplan.Size = New System.Drawing.Size(678, 62)
        Me.gbBenefitplan.TabIndex = 8
        Me.gbBenefitplan.Temp = 0
        Me.gbBenefitplan.Text = "Filter Criteria"
        Me.gbBenefitplan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(651, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(628, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboBenefitgroup
        '
        Me.cboBenefitgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBenefitgroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBenefitgroup.FormattingEnabled = True
        Me.cboBenefitgroup.Location = New System.Drawing.Point(88, 33)
        Me.cboBenefitgroup.Name = "cboBenefitgroup"
        Me.cboBenefitgroup.Size = New System.Drawing.Size(197, 21)
        Me.cboBenefitgroup.TabIndex = 87
        '
        'lblBenefitGroup
        '
        Me.lblBenefitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitGroup.Location = New System.Drawing.Point(8, 36)
        Me.lblBenefitGroup.Name = "lblBenefitGroup"
        Me.lblBenefitGroup.Size = New System.Drawing.Size(74, 15)
        Me.lblBenefitGroup.TabIndex = 85
        Me.lblBenefitGroup.Text = "Benefit Group"
        Me.lblBenefitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlBenefitplan
        '
        Me.pnlBenefitplan.Controls.Add(Me.objFooter)
        Me.pnlBenefitplan.Controls.Add(Me.lvBenefitplan)
        Me.pnlBenefitplan.Controls.Add(Me.gbBenefitplan)
        Me.pnlBenefitplan.Controls.Add(Me.eZeeHeader)
        Me.pnlBenefitplan.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBenefitplan.Location = New System.Drawing.Point(0, 0)
        Me.pnlBenefitplan.Name = "pnlBenefitplan"
        Me.pnlBenefitplan.Size = New System.Drawing.Size(693, 428)
        Me.pnlBenefitplan.TabIndex = 1
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 373)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(693, 55)
        Me.objFooter.TabIndex = 10
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(499, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 72
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(403, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 71
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(307, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 70
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(595, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 69
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lvBenefitplan
        '
        Me.lvBenefitplan.BackColorOnChecked = False
        Me.lvBenefitplan.ColumnHeaders = Nothing
        Me.lvBenefitplan.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhBenefitgroup, Me.colhBenefitplancode, Me.colhBenefitplanname, Me.colhBenefitplandesc})
        Me.lvBenefitplan.CompulsoryColumns = ""
        Me.lvBenefitplan.FullRowSelect = True
        Me.lvBenefitplan.GridLines = True
        Me.lvBenefitplan.GroupingColumn = Nothing
        Me.lvBenefitplan.HideSelection = False
        Me.lvBenefitplan.Location = New System.Drawing.Point(7, 134)
        Me.lvBenefitplan.MinColumnWidth = 50
        Me.lvBenefitplan.MultiSelect = False
        Me.lvBenefitplan.Name = "lvBenefitplan"
        Me.lvBenefitplan.OptionalColumns = ""
        Me.lvBenefitplan.ShowMoreItem = False
        Me.lvBenefitplan.ShowSaveItem = False
        Me.lvBenefitplan.ShowSelectAll = True
        Me.lvBenefitplan.ShowSizeAllColumnsToFit = True
        Me.lvBenefitplan.Size = New System.Drawing.Size(678, 233)
        Me.lvBenefitplan.Sortable = True
        Me.lvBenefitplan.TabIndex = 9
        Me.lvBenefitplan.UseCompatibleStateImageBehavior = False
        Me.lvBenefitplan.View = System.Windows.Forms.View.Details
        '
        'colhBenefitgroup
        '
        Me.colhBenefitgroup.Tag = "colhBenefitgroup"
        Me.colhBenefitgroup.Text = "Benefit Group"
        Me.colhBenefitgroup.Width = 120
        '
        'colhBenefitplancode
        '
        Me.colhBenefitplancode.Tag = "colhBenefitplancode"
        Me.colhBenefitplancode.Text = "Plan Code"
        Me.colhBenefitplancode.Width = 120
        '
        'colhBenefitplanname
        '
        Me.colhBenefitplanname.Tag = "colhBenefitplanname"
        Me.colhBenefitplanname.Text = "Plan Name"
        Me.colhBenefitplanname.Width = 160
        '
        'colhBenefitplandesc
        '
        Me.colhBenefitplandesc.Tag = "colhBenefitplandesc"
        Me.colhBenefitplandesc.Text = "Description"
        Me.colhBenefitplandesc.Width = 265
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(693, 60)
        Me.eZeeHeader.TabIndex = 7
        Me.eZeeHeader.Title = "Benefit Plan"
        '
        'frmBenefitplanList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(693, 428)
        Me.Controls.Add(Me.pnlBenefitplan)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBenefitplanList"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Benefit Plan List"
        Me.gbBenefitplan.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBenefitplan.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbBenefitplan As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboBenefitgroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBenefitGroup As System.Windows.Forms.Label
    Friend WithEvents pnlBenefitplan As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvBenefitplan As eZee.Common.eZeeListView
    Friend WithEvents colhBenefitgroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBenefitplancode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBenefitplanname As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBenefitplandesc As System.Windows.Forms.ColumnHeader
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSicksheet_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSicksheet_AddEdit))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchSickSheetAllocation = New eZee.Common.eZeeGradientButton
        Me.cboSickSheetAllocation = New System.Windows.Forms.ComboBox
        Me.LblSickSheetAllocation = New System.Windows.Forms.Label
        Me.txtjob = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchProvider = New eZee.Common.eZeeGradientButton
        Me.cboProvider = New System.Windows.Forms.ComboBox
        Me.lblProvider = New System.Windows.Forms.Label
        Me.cboMedicalCover = New System.Windows.Forms.ComboBox
        Me.lblCover = New System.Windows.Forms.Label
        Me.lblJob = New System.Windows.Forms.Label
        Me.objbtnSearchEmpCode = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmp = New eZee.Common.eZeeGradientButton
        Me.txtEmployeeName = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployeeName = New System.Windows.Forms.Label
        Me.txtEmployeeCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmployeecode = New System.Windows.Forms.Label
        Me.dtpsheetDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.txtSheetNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblSheetNo = New System.Windows.Forms.Label
        Me.gbDependantsList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objChkAll = New System.Windows.Forms.CheckBox
        Me.lvDependantsList = New System.Windows.Forms.ListView
        Me.objColhSelect = New System.Windows.Forms.ColumnHeader
        Me.colhEmpName = New System.Windows.Forms.ColumnHeader
        Me.colhGender = New System.Windows.Forms.ColumnHeader
        Me.colhAge = New System.Windows.Forms.ColumnHeader
        Me.colhMonth = New System.Windows.Forms.ColumnHeader
        Me.colhRelation = New System.Windows.Forms.ColumnHeader
        Me.EZeeFooter1.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.gbDependantsList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 389)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(640, 55)
        Me.EZeeFooter1.TabIndex = 23
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(430, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(533, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 10
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchSickSheetAllocation)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboSickSheetAllocation)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.LblSickSheetAllocation)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtjob)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchProvider)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboProvider)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblProvider)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboMedicalCover)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblCover)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblJob)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchEmpCode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchEmp)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtEmployeeName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmployeeName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtEmployeeCode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmployeecode)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.dtpsheetDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtSheetNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblSheetNo)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(6, 7)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(626, 172)
        Me.EZeeCollapsibleContainer1.TabIndex = 24
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Sick Sheet Information"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchSickSheetAllocation
        '
        Me.objbtnSearchSickSheetAllocation.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSickSheetAllocation.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSickSheetAllocation.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSickSheetAllocation.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSickSheetAllocation.BorderSelected = False
        Me.objbtnSearchSickSheetAllocation.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSickSheetAllocation.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSickSheetAllocation.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSickSheetAllocation.Location = New System.Drawing.Point(600, 89)
        Me.objbtnSearchSickSheetAllocation.Name = "objbtnSearchSickSheetAllocation"
        Me.objbtnSearchSickSheetAllocation.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSickSheetAllocation.TabIndex = 99
        '
        'cboSickSheetAllocation
        '
        Me.cboSickSheetAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSickSheetAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSickSheetAllocation.FormattingEnabled = True
        Me.cboSickSheetAllocation.Location = New System.Drawing.Point(438, 89)
        Me.cboSickSheetAllocation.Name = "cboSickSheetAllocation"
        Me.cboSickSheetAllocation.Size = New System.Drawing.Size(156, 21)
        Me.cboSickSheetAllocation.TabIndex = 98
        '
        'LblSickSheetAllocation
        '
        Me.LblSickSheetAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSickSheetAllocation.Location = New System.Drawing.Point(341, 92)
        Me.LblSickSheetAllocation.Name = "LblSickSheetAllocation"
        Me.LblSickSheetAllocation.Size = New System.Drawing.Size(90, 30)
        Me.LblSickSheetAllocation.TabIndex = 97
        Me.LblSickSheetAllocation.Text = "#Allocation"
        '
        'txtjob
        '
        Me.txtjob.BackColor = System.Drawing.Color.White
        Me.txtjob.Flags = 0
        Me.txtjob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtjob.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtjob.Location = New System.Drawing.Point(105, 116)
        Me.txtjob.Name = "txtjob"
        Me.txtjob.ReadOnly = True
        Me.txtjob.Size = New System.Drawing.Size(203, 21)
        Me.txtjob.TabIndex = 6
        '
        'objbtnSearchProvider
        '
        Me.objbtnSearchProvider.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchProvider.BorderSelected = False
        Me.objbtnSearchProvider.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchProvider.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchProvider.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchProvider.Location = New System.Drawing.Point(314, 62)
        Me.objbtnSearchProvider.Name = "objbtnSearchProvider"
        Me.objbtnSearchProvider.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchProvider.TabIndex = 95
        '
        'cboProvider
        '
        Me.cboProvider.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProvider.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProvider.FormattingEnabled = True
        Me.cboProvider.Location = New System.Drawing.Point(105, 62)
        Me.cboProvider.Name = "cboProvider"
        Me.cboProvider.Size = New System.Drawing.Size(203, 21)
        Me.cboProvider.TabIndex = 3
        '
        'lblProvider
        '
        Me.lblProvider.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProvider.Location = New System.Drawing.Point(8, 65)
        Me.lblProvider.Name = "lblProvider"
        Me.lblProvider.Size = New System.Drawing.Size(90, 15)
        Me.lblProvider.TabIndex = 93
        Me.lblProvider.Text = "Provider Name"
        '
        'cboMedicalCover
        '
        Me.cboMedicalCover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMedicalCover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMedicalCover.FormattingEnabled = True
        Me.cboMedicalCover.Location = New System.Drawing.Point(105, 143)
        Me.cboMedicalCover.Name = "cboMedicalCover"
        Me.cboMedicalCover.Size = New System.Drawing.Size(203, 21)
        Me.cboMedicalCover.TabIndex = 7
        '
        'lblCover
        '
        Me.lblCover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCover.Location = New System.Drawing.Point(8, 146)
        Me.lblCover.Name = "lblCover"
        Me.lblCover.Size = New System.Drawing.Size(90, 15)
        Me.lblCover.TabIndex = 91
        Me.lblCover.Text = "Medical Cover"
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(8, 119)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(90, 15)
        Me.lblJob.TabIndex = 89
        Me.lblJob.Text = "Job / Position"
        '
        'objbtnSearchEmpCode
        '
        Me.objbtnSearchEmpCode.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmpCode.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmpCode.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmpCode.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmpCode.BorderSelected = False
        Me.objbtnSearchEmpCode.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmpCode.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmpCode.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmpCode.Location = New System.Drawing.Point(600, 62)
        Me.objbtnSearchEmpCode.Name = "objbtnSearchEmpCode"
        Me.objbtnSearchEmpCode.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmpCode.TabIndex = 88
        '
        'objbtnSearchEmp
        '
        Me.objbtnSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmp.BorderSelected = False
        Me.objbtnSearchEmp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmp.Location = New System.Drawing.Point(314, 89)
        Me.objbtnSearchEmp.Name = "objbtnSearchEmp"
        Me.objbtnSearchEmp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmp.TabIndex = 87
        '
        'txtEmployeeName
        '
        Me.txtEmployeeName.BackColor = System.Drawing.Color.White
        Me.txtEmployeeName.Flags = 0
        Me.txtEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployeeName.Location = New System.Drawing.Point(105, 89)
        Me.txtEmployeeName.Name = "txtEmployeeName"
        Me.txtEmployeeName.ReadOnly = True
        Me.txtEmployeeName.Size = New System.Drawing.Size(203, 21)
        Me.txtEmployeeName.TabIndex = 5
        '
        'lblEmployeeName
        '
        Me.lblEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeName.Location = New System.Drawing.Point(8, 92)
        Me.lblEmployeeName.Name = "lblEmployeeName"
        Me.lblEmployeeName.Size = New System.Drawing.Size(90, 15)
        Me.lblEmployeeName.TabIndex = 11
        Me.lblEmployeeName.Text = "Employee Name"
        '
        'txtEmployeeCode
        '
        Me.txtEmployeeCode.BackColor = System.Drawing.Color.White
        Me.txtEmployeeCode.Flags = 0
        Me.txtEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployeeCode.Location = New System.Drawing.Point(438, 62)
        Me.txtEmployeeCode.Name = "txtEmployeeCode"
        Me.txtEmployeeCode.ReadOnly = True
        Me.txtEmployeeCode.Size = New System.Drawing.Size(156, 21)
        Me.txtEmployeeCode.TabIndex = 4
        '
        'lblEmployeecode
        '
        Me.lblEmployeecode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeecode.Location = New System.Drawing.Point(341, 65)
        Me.lblEmployeecode.Name = "lblEmployeecode"
        Me.lblEmployeecode.Size = New System.Drawing.Size(90, 15)
        Me.lblEmployeecode.TabIndex = 9
        Me.lblEmployeecode.Text = "Employee Code"
        '
        'dtpsheetDate
        '
        Me.dtpsheetDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpsheetDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpsheetDate.Location = New System.Drawing.Point(438, 34)
        Me.dtpsheetDate.Name = "dtpsheetDate"
        Me.dtpsheetDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpsheetDate.TabIndex = 2
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(341, 37)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(90, 15)
        Me.lblDate.TabIndex = 3
        Me.lblDate.Text = "Date"
        '
        'txtSheetNo
        '
        Me.txtSheetNo.Flags = 0
        Me.txtSheetNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSheetNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSheetNo.Location = New System.Drawing.Point(105, 34)
        Me.txtSheetNo.Name = "txtSheetNo"
        Me.txtSheetNo.Size = New System.Drawing.Size(203, 21)
        Me.txtSheetNo.TabIndex = 1
        '
        'lblSheetNo
        '
        Me.lblSheetNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSheetNo.Location = New System.Drawing.Point(8, 37)
        Me.lblSheetNo.Name = "lblSheetNo"
        Me.lblSheetNo.Size = New System.Drawing.Size(90, 15)
        Me.lblSheetNo.TabIndex = 1
        Me.lblSheetNo.Text = "Sheet No"
        '
        'gbDependantsList
        '
        Me.gbDependantsList.BorderColor = System.Drawing.Color.Black
        Me.gbDependantsList.Checked = False
        Me.gbDependantsList.CollapseAllExceptThis = False
        Me.gbDependantsList.CollapsedHoverImage = Nothing
        Me.gbDependantsList.CollapsedNormalImage = Nothing
        Me.gbDependantsList.CollapsedPressedImage = Nothing
        Me.gbDependantsList.CollapseOnLoad = False
        Me.gbDependantsList.Controls.Add(Me.pnlEmployeeList)
        Me.gbDependantsList.ExpandedHoverImage = Nothing
        Me.gbDependantsList.ExpandedNormalImage = Nothing
        Me.gbDependantsList.ExpandedPressedImage = Nothing
        Me.gbDependantsList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDependantsList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDependantsList.HeaderHeight = 25
        Me.gbDependantsList.HeaderMessage = ""
        Me.gbDependantsList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbDependantsList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDependantsList.HeightOnCollapse = 0
        Me.gbDependantsList.LeftTextSpace = 0
        Me.gbDependantsList.Location = New System.Drawing.Point(6, 184)
        Me.gbDependantsList.Name = "gbDependantsList"
        Me.gbDependantsList.OpenHeight = 300
        Me.gbDependantsList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDependantsList.ShowBorder = True
        Me.gbDependantsList.ShowCheckBox = False
        Me.gbDependantsList.ShowCollapseButton = False
        Me.gbDependantsList.ShowDefaultBorderColor = True
        Me.gbDependantsList.ShowDownButton = False
        Me.gbDependantsList.ShowHeader = True
        Me.gbDependantsList.Size = New System.Drawing.Size(627, 198)
        Me.gbDependantsList.TabIndex = 25
        Me.gbDependantsList.Temp = 0
        Me.gbDependantsList.Text = "Depedants List"
        Me.gbDependantsList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Controls.Add(Me.objChkAll)
        Me.pnlEmployeeList.Controls.Add(Me.lvDependantsList)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(623, 171)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objChkAll
        '
        Me.objChkAll.AutoSize = True
        Me.objChkAll.Location = New System.Drawing.Point(8, 3)
        Me.objChkAll.Name = "objChkAll"
        Me.objChkAll.Size = New System.Drawing.Size(15, 14)
        Me.objChkAll.TabIndex = 89
        Me.objChkAll.UseVisualStyleBackColor = True
        '
        'lvDependantsList
        '
        Me.lvDependantsList.CheckBoxes = True
        Me.lvDependantsList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhSelect, Me.colhEmpName, Me.colhGender, Me.colhAge, Me.colhMonth, Me.colhRelation})
        Me.lvDependantsList.FullRowSelect = True
        Me.lvDependantsList.GridLines = True
        Me.lvDependantsList.Location = New System.Drawing.Point(0, 0)
        Me.lvDependantsList.Name = "lvDependantsList"
        Me.lvDependantsList.Size = New System.Drawing.Size(620, 169)
        Me.lvDependantsList.TabIndex = 8
        Me.lvDependantsList.UseCompatibleStateImageBehavior = False
        Me.lvDependantsList.View = System.Windows.Forms.View.Details
        '
        'objColhSelect
        '
        Me.objColhSelect.Tag = "objColhSelect"
        Me.objColhSelect.Text = ""
        Me.objColhSelect.Width = 25
        '
        'colhEmpName
        '
        Me.colhEmpName.Tag = "colhEmpName"
        Me.colhEmpName.Text = "Name"
        Me.colhEmpName.Width = 250
        '
        'colhGender
        '
        Me.colhGender.Tag = "colhGender"
        Me.colhGender.Text = "Gender"
        Me.colhGender.Width = 92
        '
        'colhAge
        '
        Me.colhAge.Tag = "colhAge"
        Me.colhAge.Text = "Age"
        Me.colhAge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'colhMonth
        '
        Me.colhMonth.Tag = "colhMonth"
        Me.colhMonth.Text = "Month"
        Me.colhMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'colhRelation
        '
        Me.colhRelation.Tag = "colhRelation"
        Me.colhRelation.Text = "Relation"
        Me.colhRelation.Width = 129
        '
        'frmSicksheet_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(640, 444)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.gbDependantsList)
        Me.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSicksheet_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Sick Sheet Form"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.PerformLayout()
        Me.gbDependantsList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblSheetNo As System.Windows.Forms.Label
    Friend WithEvents txtSheetNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents txtEmployeeName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmployeeName As System.Windows.Forms.Label
    Friend WithEvents txtEmployeeCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmployeecode As System.Windows.Forms.Label
    Friend WithEvents dtpsheetDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSearchEmpCode As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmp As eZee.Common.eZeeGradientButton
    Friend WithEvents cboMedicalCover As System.Windows.Forms.ComboBox
    Friend WithEvents lblCover As System.Windows.Forms.Label
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents gbDependantsList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents lvDependantsList As System.Windows.Forms.ListView
    Friend WithEvents colhEmpName As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboProvider As System.Windows.Forms.ComboBox
    Friend WithEvents lblProvider As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchProvider As eZee.Common.eZeeGradientButton
    Friend WithEvents txtjob As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents colhGender As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAge As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRelation As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMonth As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhSelect As System.Windows.Forms.ColumnHeader
    Friend WithEvents objChkAll As System.Windows.Forms.CheckBox
    Friend WithEvents LblSickSheetAllocation As System.Windows.Forms.Label
    Friend WithEvents cboSickSheetAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchSickSheetAllocation As eZee.Common.eZeeGradientButton
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMedical_Claim_List
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMedical_Claim_List))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lvClaimList = New eZee.Common.eZeeListView(Me.components)
        Me.colhInvoiceNo = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhInstitute = New System.Windows.Forms.ColumnHeader
        Me.colhInvoiceAmt = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.objcolhStatusunkid = New System.Windows.Forms.ColumnHeader
        Me.objColhIsFinal = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchProvider = New eZee.Common.eZeeGradientButton
        Me.chkMyInvoiceOnly = New System.Windows.Forms.CheckBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.txtToTotInvoiceAmt = New eZee.TextBox.NumericTextBox
        Me.lblToTotInvoiceAmt = New System.Windows.Forms.Label
        Me.txtFromTotInvoiceAmt = New eZee.TextBox.NumericTextBox
        Me.txtInvoiceNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblFromTotInvoiceAmt = New System.Windows.Forms.Label
        Me.lblInvoiceNo = New System.Windows.Forms.Label
        Me.cboProvider = New System.Windows.Forms.ComboBox
        Me.lblProvider = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.lblTreatmentDate = New System.Windows.Forms.Label
        Me.dtpTreatmentDate = New System.Windows.Forms.DateTimePicker
        Me.txtServiceNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblServiceNo = New System.Windows.Forms.Label
        Me.cboService = New System.Windows.Forms.ComboBox
        Me.lblService = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.BtnOperation = New eZee.Common.eZeeSplitButton
        Me.cmClaimStatus = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btnExportStatus = New System.Windows.Forms.ToolStripMenuItem
        Me.btnCancelExport = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmClaimStatus.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lvClaimList)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.lblTreatmentDate)
        Me.pnlMainInfo.Controls.Add(Me.dtpTreatmentDate)
        Me.pnlMainInfo.Controls.Add(Me.txtServiceNo)
        Me.pnlMainInfo.Controls.Add(Me.lblServiceNo)
        Me.pnlMainInfo.Controls.Add(Me.cboService)
        Me.pnlMainInfo.Controls.Add(Me.lblService)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(697, 506)
        Me.pnlMainInfo.TabIndex = 0
        '
        'lvClaimList
        '
        Me.lvClaimList.BackColorOnChecked = True
        Me.lvClaimList.ColumnHeaders = Nothing
        Me.lvClaimList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhInvoiceNo, Me.colhPeriod, Me.colhInstitute, Me.colhInvoiceAmt, Me.colhStatus, Me.objcolhStatusunkid, Me.objColhIsFinal})
        Me.lvClaimList.CompulsoryColumns = ""
        Me.lvClaimList.FullRowSelect = True
        Me.lvClaimList.GridLines = True
        Me.lvClaimList.GroupingColumn = Nothing
        Me.lvClaimList.HideSelection = False
        Me.lvClaimList.Location = New System.Drawing.Point(12, 185)
        Me.lvClaimList.MinColumnWidth = 50
        Me.lvClaimList.MultiSelect = False
        Me.lvClaimList.Name = "lvClaimList"
        Me.lvClaimList.OptionalColumns = ""
        Me.lvClaimList.ShowMoreItem = False
        Me.lvClaimList.ShowSaveItem = False
        Me.lvClaimList.ShowSelectAll = True
        Me.lvClaimList.ShowSizeAllColumnsToFit = True
        Me.lvClaimList.Size = New System.Drawing.Size(674, 258)
        Me.lvClaimList.Sortable = True
        Me.lvClaimList.TabIndex = 10
        Me.lvClaimList.UseCompatibleStateImageBehavior = False
        Me.lvClaimList.View = System.Windows.Forms.View.Details
        '
        'colhInvoiceNo
        '
        Me.colhInvoiceNo.Tag = "colhInvoiceNo"
        Me.colhInvoiceNo.Text = "Invoice No"
        Me.colhInvoiceNo.Width = 100
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Pay Period"
        Me.colhPeriod.Width = 120
        '
        'colhInstitute
        '
        Me.colhInstitute.Tag = "colhInstitute"
        Me.colhInstitute.Text = "Provider"
        Me.colhInstitute.Width = 190
        '
        'colhInvoiceAmt
        '
        Me.colhInvoiceAmt.Tag = "colhInvoiceAmt"
        Me.colhInvoiceAmt.Text = "Invoice Amount"
        Me.colhInvoiceAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhInvoiceAmt.Width = 140
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 120
        '
        'objcolhStatusunkid
        '
        Me.objcolhStatusunkid.Tag = "objcolhStatusunkid"
        Me.objcolhStatusunkid.Text = "Statusunkid"
        Me.objcolhStatusunkid.Width = 0
        '
        'objColhIsFinal
        '
        Me.objColhIsFinal.Tag = "objColhIsFinal"
        Me.objColhIsFinal.Text = "IsFinal"
        Me.objColhIsFinal.Width = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchProvider)
        Me.gbFilterCriteria.Controls.Add(Me.chkMyInvoiceOnly)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeStraightLine2)
        Me.gbFilterCriteria.Controls.Add(Me.txtToTotInvoiceAmt)
        Me.gbFilterCriteria.Controls.Add(Me.lblToTotInvoiceAmt)
        Me.gbFilterCriteria.Controls.Add(Me.txtFromTotInvoiceAmt)
        Me.gbFilterCriteria.Controls.Add(Me.txtInvoiceNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromTotInvoiceAmt)
        Me.gbFilterCriteria.Controls.Add(Me.lblInvoiceNo)
        Me.gbFilterCriteria.Controls.Add(Me.cboProvider)
        Me.gbFilterCriteria.Controls.Add(Me.lblProvider)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(674, 113)
        Me.gbFilterCriteria.TabIndex = 12
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchProvider
        '
        Me.objbtnSearchProvider.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchProvider.BorderSelected = False
        Me.objbtnSearchProvider.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchProvider.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchProvider.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchProvider.Location = New System.Drawing.Point(291, 59)
        Me.objbtnSearchProvider.Name = "objbtnSearchProvider"
        Me.objbtnSearchProvider.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchProvider.TabIndex = 252
        '
        'chkMyInvoiceOnly
        '
        Me.chkMyInvoiceOnly.BackColor = System.Drawing.Color.Transparent
        Me.chkMyInvoiceOnly.Checked = True
        Me.chkMyInvoiceOnly.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMyInvoiceOnly.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMyInvoiceOnly.Location = New System.Drawing.Point(495, 4)
        Me.chkMyInvoiceOnly.Name = "chkMyInvoiceOnly"
        Me.chkMyInvoiceOnly.Size = New System.Drawing.Size(125, 17)
        Me.chkMyInvoiceOnly.TabIndex = 250
        Me.chkMyInvoiceOnly.Text = "My Invoice Only"
        Me.chkMyInvoiceOnly.UseVisualStyleBackColor = False
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 150
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(493, 30)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(169, 21)
        Me.cboStatus.TabIndex = 247
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(328, 31)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(158, 19)
        Me.lblStatus.TabIndex = 248
        Me.lblStatus.Text = "Status"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 150
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(117, 85)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(169, 21)
        Me.cboPeriod.TabIndex = 245
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(9, 88)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(101, 15)
        Me.lblPayPeriod.TabIndex = 246
        Me.lblPayPeriod.Text = "Pay Period"
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(318, 26)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(6, 83)
        Me.EZeeStraightLine2.TabIndex = 244
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'txtToTotInvoiceAmt
        '
        Me.txtToTotInvoiceAmt.AllowNegative = True
        Me.txtToTotInvoiceAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtToTotInvoiceAmt.DigitsInGroup = 0
        Me.txtToTotInvoiceAmt.Flags = 0
        Me.txtToTotInvoiceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtToTotInvoiceAmt.Location = New System.Drawing.Point(493, 85)
        Me.txtToTotInvoiceAmt.MaxDecimalPlaces = 4
        Me.txtToTotInvoiceAmt.MaxWholeDigits = 9
        Me.txtToTotInvoiceAmt.Name = "txtToTotInvoiceAmt"
        Me.txtToTotInvoiceAmt.Prefix = ""
        Me.txtToTotInvoiceAmt.RangeMax = 1.7976931348623157E+308
        Me.txtToTotInvoiceAmt.RangeMin = -1.7976931348623157E+308
        Me.txtToTotInvoiceAmt.Size = New System.Drawing.Size(169, 21)
        Me.txtToTotInvoiceAmt.TabIndex = 242
        Me.txtToTotInvoiceAmt.Text = "0"
        Me.txtToTotInvoiceAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblToTotInvoiceAmt
        '
        Me.lblToTotInvoiceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToTotInvoiceAmt.Location = New System.Drawing.Point(328, 86)
        Me.lblToTotInvoiceAmt.Name = "lblToTotInvoiceAmt"
        Me.lblToTotInvoiceAmt.Size = New System.Drawing.Size(158, 19)
        Me.lblToTotInvoiceAmt.TabIndex = 241
        Me.lblToTotInvoiceAmt.Text = "Total Invoice To Amount"
        Me.lblToTotInvoiceAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFromTotInvoiceAmt
        '
        Me.txtFromTotInvoiceAmt.AllowNegative = True
        Me.txtFromTotInvoiceAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtFromTotInvoiceAmt.DigitsInGroup = 0
        Me.txtFromTotInvoiceAmt.Flags = 0
        Me.txtFromTotInvoiceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFromTotInvoiceAmt.Location = New System.Drawing.Point(493, 57)
        Me.txtFromTotInvoiceAmt.MaxDecimalPlaces = 4
        Me.txtFromTotInvoiceAmt.MaxWholeDigits = 9
        Me.txtFromTotInvoiceAmt.Name = "txtFromTotInvoiceAmt"
        Me.txtFromTotInvoiceAmt.Prefix = ""
        Me.txtFromTotInvoiceAmt.RangeMax = 1.7976931348623157E+308
        Me.txtFromTotInvoiceAmt.RangeMin = -1.7976931348623157E+308
        Me.txtFromTotInvoiceAmt.Size = New System.Drawing.Size(169, 21)
        Me.txtFromTotInvoiceAmt.TabIndex = 238
        Me.txtFromTotInvoiceAmt.Text = "0"
        Me.txtFromTotInvoiceAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInvoiceNo
        '
        Me.txtInvoiceNo.Flags = 0
        Me.txtInvoiceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInvoiceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtInvoiceNo.Location = New System.Drawing.Point(117, 30)
        Me.txtInvoiceNo.Name = "txtInvoiceNo"
        Me.txtInvoiceNo.Size = New System.Drawing.Size(169, 21)
        Me.txtInvoiceNo.TabIndex = 237
        '
        'lblFromTotInvoiceAmt
        '
        Me.lblFromTotInvoiceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromTotInvoiceAmt.Location = New System.Drawing.Point(328, 58)
        Me.lblFromTotInvoiceAmt.Name = "lblFromTotInvoiceAmt"
        Me.lblFromTotInvoiceAmt.Size = New System.Drawing.Size(158, 19)
        Me.lblFromTotInvoiceAmt.TabIndex = 240
        Me.lblFromTotInvoiceAmt.Text = "Total Invoice From Amount"
        Me.lblFromTotInvoiceAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInvoiceNo
        '
        Me.lblInvoiceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInvoiceNo.Location = New System.Drawing.Point(9, 33)
        Me.lblInvoiceNo.Name = "lblInvoiceNo"
        Me.lblInvoiceNo.Size = New System.Drawing.Size(101, 15)
        Me.lblInvoiceNo.TabIndex = 239
        Me.lblInvoiceNo.Text = "Invoice No"
        Me.lblInvoiceNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboProvider
        '
        Me.cboProvider.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProvider.DropDownWidth = 150
        Me.cboProvider.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProvider.FormattingEnabled = True
        Me.cboProvider.Location = New System.Drawing.Point(117, 57)
        Me.cboProvider.Name = "cboProvider"
        Me.cboProvider.Size = New System.Drawing.Size(169, 21)
        Me.cboProvider.TabIndex = 6
        '
        'lblProvider
        '
        Me.lblProvider.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProvider.Location = New System.Drawing.Point(9, 60)
        Me.lblProvider.Name = "lblProvider"
        Me.lblProvider.Size = New System.Drawing.Size(101, 15)
        Me.lblProvider.TabIndex = 221
        Me.lblProvider.Text = "Service Provider"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(647, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(624, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(697, 60)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Medical Claim List"
        '
        'lblTreatmentDate
        '
        Me.lblTreatmentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTreatmentDate.Location = New System.Drawing.Point(237, 123)
        Me.lblTreatmentDate.Name = "lblTreatmentDate"
        Me.lblTreatmentDate.Size = New System.Drawing.Size(89, 15)
        Me.lblTreatmentDate.TabIndex = 241
        Me.lblTreatmentDate.Text = "Treatment Date"
        '
        'dtpTreatmentDate
        '
        Me.dtpTreatmentDate.Checked = False
        Me.dtpTreatmentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTreatmentDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTreatmentDate.Location = New System.Drawing.Point(327, 120)
        Me.dtpTreatmentDate.Name = "dtpTreatmentDate"
        Me.dtpTreatmentDate.ShowCheckBox = True
        Me.dtpTreatmentDate.Size = New System.Drawing.Size(111, 21)
        Me.dtpTreatmentDate.TabIndex = 236
        '
        'txtServiceNo
        '
        Me.txtServiceNo.Flags = 0
        Me.txtServiceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServiceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtServiceNo.Location = New System.Drawing.Point(550, 120)
        Me.txtServiceNo.Name = "txtServiceNo"
        Me.txtServiceNo.Size = New System.Drawing.Size(123, 21)
        Me.txtServiceNo.TabIndex = 238
        '
        'lblServiceNo
        '
        Me.lblServiceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServiceNo.Location = New System.Drawing.Point(459, 123)
        Me.lblServiceNo.Name = "lblServiceNo"
        Me.lblServiceNo.Size = New System.Drawing.Size(83, 15)
        Me.lblServiceNo.TabIndex = 240
        Me.lblServiceNo.Text = "Provider No"
        '
        'cboService
        '
        Me.cboService.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboService.DropDownWidth = 150
        Me.cboService.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboService.FormattingEnabled = True
        Me.cboService.Location = New System.Drawing.Point(550, 93)
        Me.cboService.Name = "cboService"
        Me.cboService.Size = New System.Drawing.Size(123, 21)
        Me.cboService.TabIndex = 237
        '
        'lblService
        '
        Me.lblService.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblService.Location = New System.Drawing.Point(459, 96)
        Me.lblService.Name = "lblService"
        Me.lblService.Size = New System.Drawing.Size(83, 15)
        Me.lblService.TabIndex = 239
        Me.lblService.Text = "Service"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.BtnOperation)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 451)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(697, 55)
        Me.objFooter.TabIndex = 11
        '
        'BtnOperation
        '
        Me.BtnOperation.BorderColor = System.Drawing.Color.Black
        Me.BtnOperation.ContextMenuStrip = Me.cmClaimStatus
        Me.BtnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.BtnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.BtnOperation.Location = New System.Drawing.Point(12, 13)
        Me.BtnOperation.Name = "BtnOperation"
        Me.BtnOperation.ShowDefaultBorderColor = True
        Me.BtnOperation.Size = New System.Drawing.Size(93, 30)
        Me.BtnOperation.SplitButtonMenu = Me.cmClaimStatus
        Me.BtnOperation.TabIndex = 228
        Me.BtnOperation.Text = "Operation"
        '
        'cmClaimStatus
        '
        Me.cmClaimStatus.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnExportStatus, Me.btnCancelExport})
        Me.cmClaimStatus.Name = "cmImportAccrueLeave"
        Me.cmClaimStatus.Size = New System.Drawing.Size(153, 48)
        '
        'btnExportStatus
        '
        Me.btnExportStatus.Name = "btnExportStatus"
        Me.btnExportStatus.Size = New System.Drawing.Size(152, 22)
        Me.btnExportStatus.Text = "Ex&port"
        '
        'btnCancelExport
        '
        Me.btnCancelExport.Name = "btnCancelExport"
        Me.btnCancelExport.Size = New System.Drawing.Size(152, 22)
        Me.btnCancelExport.Text = "Cancel E&xport"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(495, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 13
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(591, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 14
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(398, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 12
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(302, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 11
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(291, 86)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 254
        '
        'frmMedical_Claim_List
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(697, 506)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMedical_Claim_List"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Medical Claim List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmClaimStatus.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lvClaimList As eZee.Common.eZeeListView
    Friend WithEvents cboProvider As System.Windows.Forms.ComboBox
    Friend WithEvents lblProvider As System.Windows.Forms.Label
    Friend WithEvents lblTreatmentDate As System.Windows.Forms.Label
    Friend WithEvents dtpTreatmentDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtServiceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblServiceNo As System.Windows.Forms.Label
    Friend WithEvents cboService As System.Windows.Forms.ComboBox
    Friend WithEvents lblService As System.Windows.Forms.Label
    Friend WithEvents txtFromTotInvoiceAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents txtInvoiceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFromTotInvoiceAmt As System.Windows.Forms.Label
    Friend WithEvents lblInvoiceNo As System.Windows.Forms.Label
    Friend WithEvents txtToTotInvoiceAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblToTotInvoiceAmt As System.Windows.Forms.Label
    Friend WithEvents colhInvoiceNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInstitute As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInvoiceAmt As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objcolhStatusunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhIsFinal As System.Windows.Forms.ColumnHeader
    Friend WithEvents BtnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmClaimStatus As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents btnExportStatus As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnCancelExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkMyInvoiceOnly As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchProvider As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssignGroupBenefit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssignGroupBenefit))
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlBenefitGroupAssign = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.lblJobGroup = New System.Windows.Forms.Label
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.cboGradeGroup = New System.Windows.Forms.ComboBox
        Me.lblTeam = New System.Windows.Forms.Label
        Me.cboTeams = New System.Windows.Forms.ComboBox
        Me.lblUnitGroup = New System.Windows.Forms.Label
        Me.cboUnitGroup = New System.Windows.Forms.ComboBox
        Me.lblSectionGroup = New System.Windows.Forms.Label
        Me.cboSectionGroup = New System.Windows.Forms.ComboBox
        Me.cboDepartmentGrp = New System.Windows.Forms.ComboBox
        Me.lblDepartmentGroup = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.cboUnit = New System.Windows.Forms.ComboBox
        Me.lblUnit = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.lblGrade = New System.Windows.Forms.Label
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.lblClass = New System.Windows.Forms.Label
        Me.cboSections = New System.Windows.Forms.ComboBox
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.gbBenefitPlan = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.radApplytoAll = New System.Windows.Forms.RadioButton
        Me.radApplytoChecked = New System.Windows.Forms.RadioButton
        Me.rdShowOnlyReinstatedEmp = New System.Windows.Forms.RadioButton
        Me.rdShowOnlyNewHiredEmp = New System.Windows.Forms.RadioButton
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.btnSetPeriod = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlBenfitPlan = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgBenefitPlan = New System.Windows.Forms.DataGridView
        Me.objdgcolhIscheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhExempt = New System.Windows.Forms.DataGridViewLinkColumn
        Me.dgcolhBenefitPlan = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTranshead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBenefitPlanunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTranheadunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPeriodId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhexemptperiodid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhDefaultAmount = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.chkOverwritePrevEDSlabHeads = New System.Windows.Forms.CheckBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.chkOverwrite = New System.Windows.Forms.CheckBox
        Me.cboBenefitGroup = New System.Windows.Forms.ComboBox
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.lblBenefitType = New System.Windows.Forms.Label
        Me.objStLine1 = New eZee.Common.eZeeStraightLine
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objpnlEmployeeList = New System.Windows.Forms.Panel
        Me.objChkAllEmployee = New System.Windows.Forms.CheckBox
        Me.lvEmployeeList = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhEmployeeCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmployeeName = New System.Windows.Forms.ColumnHeader
        Me.gbGroupAssign = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpAssignDate = New System.Windows.Forms.DateTimePicker
        Me.radDoNotOverwrite = New System.Windows.Forms.RadioButton
        Me.cboValueBasis = New System.Windows.Forms.ComboBox
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.radAdd = New System.Windows.Forms.RadioButton
        Me.lblValueBasis = New System.Windows.Forms.Label
        Me.radOverwrite = New System.Windows.Forms.RadioButton
        Me.lblBenefitInAmount = New System.Windows.Forms.Label
        Me.txtBenefitAmount = New eZee.TextBox.NumericTextBox
        Me.lblBenefitsInPercent = New System.Windows.Forms.Label
        Me.txtDBBenefitInPercent = New eZee.TextBox.NumericTextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuViewDeleteEmpBenefits = New System.Windows.Forms.ToolStripMenuItem
        Me.btnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlBenefitGroupAssign.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbBenefitPlan.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pnlBenfitPlan.SuspendLayout()
        CType(Me.dgBenefitPlan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEmployeeList.SuspendLayout()
        Me.objpnlEmployeeList.SuspendLayout()
        Me.gbGroupAssign.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlBenefitGroupAssign
        '
        Me.pnlBenefitGroupAssign.Controls.Add(Me.gbFilterCriteria)
        Me.pnlBenefitGroupAssign.Controls.Add(Me.gbBenefitPlan)
        Me.pnlBenefitGroupAssign.Controls.Add(Me.gbEmployeeList)
        Me.pnlBenefitGroupAssign.Controls.Add(Me.gbGroupAssign)
        Me.pnlBenefitGroupAssign.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBenefitGroupAssign.Location = New System.Drawing.Point(0, 0)
        Me.pnlBenefitGroupAssign.Name = "pnlBenefitGroupAssign"
        Me.pnlBenefitGroupAssign.Size = New System.Drawing.Size(899, 606)
        Me.pnlBenefitGroupAssign.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblGradeLevel)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.cboGradeLevel)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboClassGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblClassGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblCostCenter)
        Me.gbFilterCriteria.Controls.Add(Me.cboJobGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblJobGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblGradeGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboCostCenter)
        Me.gbFilterCriteria.Controls.Add(Me.cboGradeGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblTeam)
        Me.gbFilterCriteria.Controls.Add(Me.cboTeams)
        Me.gbFilterCriteria.Controls.Add(Me.lblUnitGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboUnitGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblSectionGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboSectionGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartmentGrp)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartmentGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboBranch)
        Me.gbFilterCriteria.Controls.Add(Me.lblBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.lblJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboUnit)
        Me.gbFilterCriteria.Controls.Add(Me.lblUnit)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboGrade)
        Me.gbFilterCriteria.Controls.Add(Me.lblGrade)
        Me.gbFilterCriteria.Controls.Add(Me.cboClass)
        Me.gbFilterCriteria.Controls.Add(Me.lblClass)
        Me.gbFilterCriteria.Controls.Add(Me.cboSections)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.lblSection)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartment)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(267, 6)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(621, 196)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(418, 35)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(67, 17)
        Me.lblGradeLevel.TabIndex = 155
        Me.lblGradeLevel.Text = "Grade Level"
        Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(594, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownWidth = 300
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(488, 33)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(122, 21)
        Me.cboGradeLevel.TabIndex = 156
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(570, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownWidth = 300
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(488, 113)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(122, 21)
        Me.cboClassGroup.TabIndex = 160
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(418, 115)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(67, 17)
        Me.lblClassGroup.TabIndex = 159
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(418, 170)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(67, 17)
        Me.lblCostCenter.TabIndex = 81
        Me.lblCostCenter.Text = "Cost Center"
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownWidth = 300
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.Location = New System.Drawing.Point(488, 59)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(122, 21)
        Me.cboJobGroup.TabIndex = 158
        '
        'lblJobGroup
        '
        Me.lblJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobGroup.Location = New System.Drawing.Point(418, 61)
        Me.lblJobGroup.Name = "lblJobGroup"
        Me.lblJobGroup.Size = New System.Drawing.Size(67, 17)
        Me.lblJobGroup.TabIndex = 157
        Me.lblJobGroup.Text = "Job Group"
        Me.lblJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(211, 115)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(77, 17)
        Me.lblGradeGroup.TabIndex = 154
        Me.lblGradeGroup.Text = "Grade Group"
        Me.lblGradeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownWidth = 300
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Items.AddRange(New Object() {"Select Section", "Website", "Desktop"})
        Me.cboCostCenter.Location = New System.Drawing.Point(488, 168)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(122, 21)
        Me.cboCostCenter.TabIndex = 2
        '
        'cboGradeGroup
        '
        Me.cboGradeGroup.DropDownWidth = 300
        Me.cboGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGroup.FormattingEnabled = True
        Me.cboGradeGroup.Location = New System.Drawing.Point(291, 113)
        Me.cboGradeGroup.Name = "cboGradeGroup"
        Me.cboGradeGroup.Size = New System.Drawing.Size(122, 21)
        Me.cboGradeGroup.TabIndex = 153
        '
        'lblTeam
        '
        Me.lblTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.Location = New System.Drawing.Point(211, 88)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(77, 17)
        Me.lblTeam.TabIndex = 150
        Me.lblTeam.Text = "Team"
        Me.lblTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTeams
        '
        Me.cboTeams.DropDownWidth = 300
        Me.cboTeams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeams.FormattingEnabled = True
        Me.cboTeams.Location = New System.Drawing.Point(291, 86)
        Me.cboTeams.Name = "cboTeams"
        Me.cboTeams.Size = New System.Drawing.Size(122, 21)
        Me.cboTeams.TabIndex = 151
        '
        'lblUnitGroup
        '
        Me.lblUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitGroup.Location = New System.Drawing.Point(211, 35)
        Me.lblUnitGroup.Name = "lblUnitGroup"
        Me.lblUnitGroup.Size = New System.Drawing.Size(77, 17)
        Me.lblUnitGroup.TabIndex = 147
        Me.lblUnitGroup.Text = "Unit Group"
        Me.lblUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnitGroup
        '
        Me.cboUnitGroup.DropDownWidth = 300
        Me.cboUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGroup.FormattingEnabled = True
        Me.cboUnitGroup.Location = New System.Drawing.Point(291, 33)
        Me.cboUnitGroup.Name = "cboUnitGroup"
        Me.cboUnitGroup.Size = New System.Drawing.Size(122, 21)
        Me.cboUnitGroup.TabIndex = 148
        '
        'lblSectionGroup
        '
        Me.lblSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSectionGroup.Location = New System.Drawing.Point(5, 115)
        Me.lblSectionGroup.Name = "lblSectionGroup"
        Me.lblSectionGroup.Size = New System.Drawing.Size(75, 16)
        Me.lblSectionGroup.TabIndex = 145
        Me.lblSectionGroup.Text = "Sec. Group"
        Me.lblSectionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSectionGroup
        '
        Me.cboSectionGroup.DropDownWidth = 300
        Me.cboSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGroup.FormattingEnabled = True
        Me.cboSectionGroup.Location = New System.Drawing.Point(83, 113)
        Me.cboSectionGroup.Name = "cboSectionGroup"
        Me.cboSectionGroup.Size = New System.Drawing.Size(122, 21)
        Me.cboSectionGroup.TabIndex = 146
        '
        'cboDepartmentGrp
        '
        Me.cboDepartmentGrp.DropDownWidth = 300
        Me.cboDepartmentGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartmentGrp.FormattingEnabled = True
        Me.cboDepartmentGrp.Location = New System.Drawing.Point(83, 59)
        Me.cboDepartmentGrp.Name = "cboDepartmentGrp"
        Me.cboDepartmentGrp.Size = New System.Drawing.Size(122, 21)
        Me.cboDepartmentGrp.TabIndex = 144
        '
        'lblDepartmentGroup
        '
        Me.lblDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartmentGroup.Location = New System.Drawing.Point(5, 61)
        Me.lblDepartmentGroup.Name = "lblDepartmentGroup"
        Me.lblDepartmentGroup.Size = New System.Drawing.Size(75, 16)
        Me.lblDepartmentGroup.TabIndex = 143
        Me.lblDepartmentGroup.Text = "Dept. Group"
        Me.lblDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBranch
        '
        Me.cboBranch.DropDownWidth = 300
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Items.AddRange(New Object() {"Select Section", "Website", "Desktop"})
        Me.cboBranch.Location = New System.Drawing.Point(83, 33)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(122, 21)
        Me.cboBranch.TabIndex = 7
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(5, 35)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(75, 16)
        Me.lblBranch.TabIndex = 83
        Me.lblBranch.Text = "Branch"
        '
        'cboJob
        '
        Me.cboJob.DropDownWidth = 300
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Items.AddRange(New Object() {"Select Department", "Developer", "Tester", "Sales and Support"})
        Me.cboJob.Location = New System.Drawing.Point(488, 86)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(122, 21)
        Me.cboJob.TabIndex = 3
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(418, 88)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(67, 17)
        Me.lblJob.TabIndex = 69
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnit
        '
        Me.cboUnit.DropDownWidth = 300
        Me.cboUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnit.FormattingEnabled = True
        Me.cboUnit.Items.AddRange(New Object() {"Select Section", "Website", "Desktop"})
        Me.cboUnit.Location = New System.Drawing.Point(291, 59)
        Me.cboUnit.Name = "cboUnit"
        Me.cboUnit.Size = New System.Drawing.Size(122, 21)
        Me.cboUnit.TabIndex = 5
        '
        'lblUnit
        '
        Me.lblUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnit.Location = New System.Drawing.Point(211, 61)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(77, 17)
        Me.lblUnit.TabIndex = 79
        Me.lblUnit.Text = "Unit"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 180
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Items.AddRange(New Object() {"Select Section", "Website", "Desktop"})
        Me.cboEmployee.Location = New System.Drawing.Point(83, 168)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(330, 21)
        Me.cboEmployee.TabIndex = 8
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(5, 170)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(75, 16)
        Me.lblEmployee.TabIndex = 76
        Me.lblEmployee.Text = "Employee"
        '
        'cboGrade
        '
        Me.cboGrade.DropDownWidth = 300
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Items.AddRange(New Object() {"Select Section", "Website", "Desktop"})
        Me.cboGrade.Location = New System.Drawing.Point(291, 141)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(122, 21)
        Me.cboGrade.TabIndex = 1
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(211, 143)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(77, 17)
        Me.lblGrade.TabIndex = 16
        Me.lblGrade.Text = "Grade"
        '
        'cboClass
        '
        Me.cboClass.DropDownWidth = 300
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Items.AddRange(New Object() {"Select Section", "Website", "Desktop"})
        Me.cboClass.Location = New System.Drawing.Point(488, 141)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(122, 21)
        Me.cboClass.TabIndex = 6
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(418, 143)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(67, 17)
        Me.lblClass.TabIndex = 12
        Me.lblClass.Text = "Class"
        '
        'cboSections
        '
        Me.cboSections.DropDownWidth = 300
        Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSections.FormattingEnabled = True
        Me.cboSections.Items.AddRange(New Object() {"Select Section", "Website", "Desktop"})
        Me.cboSections.Location = New System.Drawing.Point(83, 141)
        Me.cboSections.Name = "cboSections"
        Me.cboSections.Size = New System.Drawing.Size(122, 21)
        Me.cboSections.TabIndex = 4
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownWidth = 300
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Items.AddRange(New Object() {"Select Department", "Developer", "Tester", "Sales and Support"})
        Me.cboDepartment.Location = New System.Drawing.Point(83, 86)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(122, 21)
        Me.cboDepartment.TabIndex = 0
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(5, 143)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(75, 16)
        Me.lblSection.TabIndex = 4
        Me.lblSection.Text = "Section"
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(5, 88)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(75, 16)
        Me.lblDepartment.TabIndex = 3
        Me.lblDepartment.Text = "Department"
        '
        'gbBenefitPlan
        '
        Me.gbBenefitPlan.BorderColor = System.Drawing.Color.Black
        Me.gbBenefitPlan.Checked = False
        Me.gbBenefitPlan.CollapseAllExceptThis = False
        Me.gbBenefitPlan.CollapsedHoverImage = Nothing
        Me.gbBenefitPlan.CollapsedNormalImage = Nothing
        Me.gbBenefitPlan.CollapsedPressedImage = Nothing
        Me.gbBenefitPlan.CollapseOnLoad = False
        Me.gbBenefitPlan.Controls.Add(Me.Panel1)
        Me.gbBenefitPlan.Controls.Add(Me.rdShowOnlyReinstatedEmp)
        Me.gbBenefitPlan.Controls.Add(Me.rdShowOnlyNewHiredEmp)
        Me.gbBenefitPlan.Controls.Add(Me.objLine1)
        Me.gbBenefitPlan.Controls.Add(Me.cboPeriod)
        Me.gbBenefitPlan.Controls.Add(Me.btnSetPeriod)
        Me.gbBenefitPlan.Controls.Add(Me.pnlBenfitPlan)
        Me.gbBenefitPlan.Controls.Add(Me.chkOverwritePrevEDSlabHeads)
        Me.gbBenefitPlan.Controls.Add(Me.lblPeriod)
        Me.gbBenefitPlan.Controls.Add(Me.chkOverwrite)
        Me.gbBenefitPlan.Controls.Add(Me.cboBenefitGroup)
        Me.gbBenefitPlan.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.gbBenefitPlan.Controls.Add(Me.lblBenefitType)
        Me.gbBenefitPlan.Controls.Add(Me.objStLine1)
        Me.gbBenefitPlan.ExpandedHoverImage = Nothing
        Me.gbBenefitPlan.ExpandedNormalImage = Nothing
        Me.gbBenefitPlan.ExpandedPressedImage = Nothing
        Me.gbBenefitPlan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBenefitPlan.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBenefitPlan.HeaderHeight = 25
        Me.gbBenefitPlan.HeaderMessage = ""
        Me.gbBenefitPlan.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBenefitPlan.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBenefitPlan.HeightOnCollapse = 0
        Me.gbBenefitPlan.LeftTextSpace = 0
        Me.gbBenefitPlan.Location = New System.Drawing.Point(266, 207)
        Me.gbBenefitPlan.Name = "gbBenefitPlan"
        Me.gbBenefitPlan.OpenHeight = 300
        Me.gbBenefitPlan.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBenefitPlan.ShowBorder = True
        Me.gbBenefitPlan.ShowCheckBox = False
        Me.gbBenefitPlan.ShowCollapseButton = False
        Me.gbBenefitPlan.ShowDefaultBorderColor = True
        Me.gbBenefitPlan.ShowDownButton = False
        Me.gbBenefitPlan.ShowHeader = True
        Me.gbBenefitPlan.Size = New System.Drawing.Size(624, 341)
        Me.gbBenefitPlan.TabIndex = 2
        Me.gbBenefitPlan.Temp = 0
        Me.gbBenefitPlan.Text = "Benefit Plan info"
        Me.gbBenefitPlan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.radApplytoAll)
        Me.Panel1.Controls.Add(Me.radApplytoChecked)
        Me.Panel1.Location = New System.Drawing.Point(476, 31)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(143, 57)
        Me.Panel1.TabIndex = 306
        '
        'radApplytoAll
        '
        Me.radApplytoAll.Checked = True
        Me.radApplytoAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoAll.Location = New System.Drawing.Point(8, 8)
        Me.radApplytoAll.Name = "radApplytoAll"
        Me.radApplytoAll.Size = New System.Drawing.Size(121, 18)
        Me.radApplytoAll.TabIndex = 233
        Me.radApplytoAll.TabStop = True
        Me.radApplytoAll.Text = "Apply to All"
        Me.radApplytoAll.UseVisualStyleBackColor = True
        '
        'radApplytoChecked
        '
        Me.radApplytoChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoChecked.Location = New System.Drawing.Point(8, 31)
        Me.radApplytoChecked.Name = "radApplytoChecked"
        Me.radApplytoChecked.Size = New System.Drawing.Size(121, 18)
        Me.radApplytoChecked.TabIndex = 299
        Me.radApplytoChecked.Text = "Apply To Checked"
        Me.radApplytoChecked.UseVisualStyleBackColor = True
        '
        'rdShowOnlyReinstatedEmp
        '
        Me.rdShowOnlyReinstatedEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdShowOnlyReinstatedEmp.Location = New System.Drawing.Point(259, 117)
        Me.rdShowOnlyReinstatedEmp.Name = "rdShowOnlyReinstatedEmp"
        Me.rdShowOnlyReinstatedEmp.Size = New System.Drawing.Size(207, 17)
        Me.rdShowOnlyReinstatedEmp.TabIndex = 305
        Me.rdShowOnlyReinstatedEmp.TabStop = True
        Me.rdShowOnlyReinstatedEmp.Text = "Show only Reinstated Employee(s)"
        Me.rdShowOnlyReinstatedEmp.UseVisualStyleBackColor = True
        '
        'rdShowOnlyNewHiredEmp
        '
        Me.rdShowOnlyNewHiredEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdShowOnlyNewHiredEmp.Location = New System.Drawing.Point(17, 117)
        Me.rdShowOnlyNewHiredEmp.Name = "rdShowOnlyNewHiredEmp"
        Me.rdShowOnlyNewHiredEmp.Size = New System.Drawing.Size(236, 17)
        Me.rdShowOnlyNewHiredEmp.TabIndex = 304
        Me.rdShowOnlyNewHiredEmp.TabStop = True
        Me.rdShowOnlyNewHiredEmp.Text = "Show only Newly Hired Employee(s)"
        Me.rdShowOnlyNewHiredEmp.UseVisualStyleBackColor = True
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(468, 25)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(6, 110)
        Me.objLine1.TabIndex = 298
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 300
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(323, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(132, 21)
        Me.cboPeriod.TabIndex = 296
        '
        'btnSetPeriod
        '
        Me.btnSetPeriod.BackColor = System.Drawing.Color.White
        Me.btnSetPeriod.BackgroundImage = CType(resources.GetObject("btnSetPeriod.BackgroundImage"), System.Drawing.Image)
        Me.btnSetPeriod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSetPeriod.BorderColor = System.Drawing.Color.Empty
        Me.btnSetPeriod.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSetPeriod.FlatAppearance.BorderSize = 0
        Me.btnSetPeriod.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetPeriod.ForeColor = System.Drawing.Color.Black
        Me.btnSetPeriod.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSetPeriod.GradientForeColor = System.Drawing.Color.Black
        Me.btnSetPeriod.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSetPeriod.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSetPeriod.Location = New System.Drawing.Point(479, 105)
        Me.btnSetPeriod.Name = "btnSetPeriod"
        Me.btnSetPeriod.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSetPeriod.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSetPeriod.Size = New System.Drawing.Size(97, 30)
        Me.btnSetPeriod.TabIndex = 2
        Me.btnSetPeriod.Text = "Set Period"
        Me.btnSetPeriod.UseVisualStyleBackColor = True
        '
        'pnlBenfitPlan
        '
        Me.pnlBenfitPlan.Controls.Add(Me.objchkSelectAll)
        Me.pnlBenfitPlan.Controls.Add(Me.dgBenefitPlan)
        Me.pnlBenfitPlan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlBenfitPlan.Location = New System.Drawing.Point(9, 141)
        Me.pnlBenfitPlan.Name = "pnlBenfitPlan"
        Me.pnlBenfitPlan.Size = New System.Drawing.Size(605, 195)
        Me.pnlBenfitPlan.TabIndex = 294
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(7, 7)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 293
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgBenefitPlan
        '
        Me.dgBenefitPlan.AllowUserToAddRows = False
        Me.dgBenefitPlan.AllowUserToDeleteRows = False
        Me.dgBenefitPlan.BackgroundColor = System.Drawing.Color.White
        Me.dgBenefitPlan.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgBenefitPlan.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhIscheck, Me.objdgcolhExempt, Me.dgcolhBenefitPlan, Me.dgcolhTranshead, Me.dgcolhAmount, Me.dgcolhPeriod, Me.objdgcolhBenefitPlanunkid, Me.objdgcolhTranheadunkid, Me.objdgcolhPeriodId, Me.objdgcolhexemptperiodid, Me.objdgcolhDefaultAmount})
        Me.dgBenefitPlan.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgBenefitPlan.Location = New System.Drawing.Point(0, 0)
        Me.dgBenefitPlan.MultiSelect = False
        Me.dgBenefitPlan.Name = "dgBenefitPlan"
        Me.dgBenefitPlan.RowHeadersVisible = False
        Me.dgBenefitPlan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgBenefitPlan.Size = New System.Drawing.Size(605, 195)
        Me.dgBenefitPlan.TabIndex = 292
        '
        'objdgcolhIscheck
        '
        Me.objdgcolhIscheck.Frozen = True
        Me.objdgcolhIscheck.HeaderText = ""
        Me.objdgcolhIscheck.Name = "objdgcolhIscheck"
        Me.objdgcolhIscheck.Width = 25
        '
        'objdgcolhExempt
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.objdgcolhExempt.DefaultCellStyle = DataGridViewCellStyle5
        Me.objdgcolhExempt.Frozen = True
        Me.objdgcolhExempt.HeaderText = ""
        Me.objdgcolhExempt.Name = "objdgcolhExempt"
        Me.objdgcolhExempt.ReadOnly = True
        Me.objdgcolhExempt.Text = ""
        Me.objdgcolhExempt.Width = 80
        '
        'dgcolhBenefitPlan
        '
        Me.dgcolhBenefitPlan.HeaderText = "Benefit Plan"
        Me.dgcolhBenefitPlan.Name = "dgcolhBenefitPlan"
        Me.dgcolhBenefitPlan.ReadOnly = True
        Me.dgcolhBenefitPlan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhBenefitPlan.Width = 150
        '
        'dgcolhTranshead
        '
        Me.dgcolhTranshead.HeaderText = "Tran. Head"
        Me.dgcolhTranshead.Name = "dgcolhTranshead"
        Me.dgcolhTranshead.ReadOnly = True
        Me.dgcolhTranshead.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTranshead.Width = 150
        '
        'dgcolhAmount
        '
        Me.dgcolhAmount.AllowNegative = False
        Me.dgcolhAmount.DecimalLength = 3
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhAmount.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgcolhAmount.HeaderText = "Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhPeriod
        '
        Me.dgcolhPeriod.HeaderText = "Period"
        Me.dgcolhPeriod.Name = "dgcolhPeriod"
        Me.dgcolhPeriod.ReadOnly = True
        Me.dgcolhPeriod.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhBenefitPlanunkid
        '
        Me.objdgcolhBenefitPlanunkid.HeaderText = "BenefitPlanunkid"
        Me.objdgcolhBenefitPlanunkid.Name = "objdgcolhBenefitPlanunkid"
        Me.objdgcolhBenefitPlanunkid.ReadOnly = True
        Me.objdgcolhBenefitPlanunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhBenefitPlanunkid.Visible = False
        '
        'objdgcolhTranheadunkid
        '
        Me.objdgcolhTranheadunkid.HeaderText = "Tranheadunkid"
        Me.objdgcolhTranheadunkid.Name = "objdgcolhTranheadunkid"
        Me.objdgcolhTranheadunkid.ReadOnly = True
        Me.objdgcolhTranheadunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhTranheadunkid.Visible = False
        '
        'objdgcolhPeriodId
        '
        Me.objdgcolhPeriodId.HeaderText = "PeriodId"
        Me.objdgcolhPeriodId.Name = "objdgcolhPeriodId"
        Me.objdgcolhPeriodId.ReadOnly = True
        Me.objdgcolhPeriodId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhPeriodId.Visible = False
        '
        'objdgcolhexemptperiodid
        '
        Me.objdgcolhexemptperiodid.HeaderText = "Exemptperiodid"
        Me.objdgcolhexemptperiodid.Name = "objdgcolhexemptperiodid"
        Me.objdgcolhexemptperiodid.ReadOnly = True
        Me.objdgcolhexemptperiodid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhexemptperiodid.Visible = False
        '
        'objdgcolhDefaultAmount
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "F0"
        Me.objdgcolhDefaultAmount.DefaultCellStyle = DataGridViewCellStyle7
        Me.objdgcolhDefaultAmount.HeaderText = "Default Amount"
        Me.objdgcolhDefaultAmount.Name = "objdgcolhDefaultAmount"
        Me.objdgcolhDefaultAmount.ReadOnly = True
        Me.objdgcolhDefaultAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhDefaultAmount.Visible = False
        '
        'chkOverwritePrevEDSlabHeads
        '
        Me.chkOverwritePrevEDSlabHeads.Enabled = False
        Me.chkOverwritePrevEDSlabHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwritePrevEDSlabHeads.Location = New System.Drawing.Point(17, 95)
        Me.chkOverwritePrevEDSlabHeads.Name = "chkOverwritePrevEDSlabHeads"
        Me.chkOverwritePrevEDSlabHeads.Size = New System.Drawing.Size(236, 17)
        Me.chkOverwritePrevEDSlabHeads.TabIndex = 290
        Me.chkOverwritePrevEDSlabHeads.Text = "Overwrite Previous ED Slab heads"
        Me.chkOverwritePrevEDSlabHeads.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(226, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(83, 15)
        Me.lblPeriod.TabIndex = 287
        Me.lblPeriod.Text = "Effective Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkOverwrite
        '
        Me.chkOverwrite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwrite.Location = New System.Drawing.Point(17, 73)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.Size = New System.Drawing.Size(236, 17)
        Me.chkOverwrite.TabIndex = 281
        Me.chkOverwrite.Text = "Overwrite selected benefit heads if exist"
        Me.chkOverwrite.UseVisualStyleBackColor = True
        '
        'cboBenefitGroup
        '
        Me.cboBenefitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBenefitGroup.DropDownWidth = 300
        Me.cboBenefitGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBenefitGroup.FormattingEnabled = True
        Me.cboBenefitGroup.Location = New System.Drawing.Point(88, 33)
        Me.cboBenefitGroup.Name = "cboBenefitGroup"
        Me.cboBenefitGroup.Size = New System.Drawing.Size(132, 21)
        Me.cboBenefitGroup.TabIndex = 0
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(259, 73)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(207, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 289
        Me.chkCopyPreviousEDSlab.Text = "Copy Previous ED Slab"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'lblBenefitType
        '
        Me.lblBenefitType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitType.Location = New System.Drawing.Point(8, 36)
        Me.lblBenefitType.Name = "lblBenefitType"
        Me.lblBenefitType.Size = New System.Drawing.Size(72, 15)
        Me.lblBenefitType.TabIndex = 113
        Me.lblBenefitType.Text = "Benefit Group"
        Me.lblBenefitType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objStLine1
        '
        Me.objStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objStLine1.ForeColor = System.Drawing.Color.Gray
        Me.objStLine1.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.objStLine1.Location = New System.Drawing.Point(10, 61)
        Me.objStLine1.Name = "objStLine1"
        Me.objStLine1.Size = New System.Drawing.Size(460, 7)
        Me.objStLine1.TabIndex = 129
        Me.objStLine1.Text = "EZeeStraightLine1"
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.objpnlEmployeeList)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(12, 6)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 377
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(249, 543)
        Me.gbEmployeeList.TabIndex = 0
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "List"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnlEmployeeList
        '
        Me.objpnlEmployeeList.Controls.Add(Me.objChkAllEmployee)
        Me.objpnlEmployeeList.Controls.Add(Me.lvEmployeeList)
        Me.objpnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.objpnlEmployeeList.Name = "objpnlEmployeeList"
        Me.objpnlEmployeeList.Size = New System.Drawing.Size(247, 514)
        Me.objpnlEmployeeList.TabIndex = 2
        '
        'objChkAllEmployee
        '
        Me.objChkAllEmployee.AutoSize = True
        Me.objChkAllEmployee.Location = New System.Drawing.Point(6, 4)
        Me.objChkAllEmployee.Name = "objChkAllEmployee"
        Me.objChkAllEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objChkAllEmployee.TabIndex = 124
        Me.objChkAllEmployee.UseVisualStyleBackColor = True
        '
        'lvEmployeeList
        '
        Me.lvEmployeeList.BackColorOnChecked = True
        Me.lvEmployeeList.CheckBoxes = True
        Me.lvEmployeeList.ColumnHeaders = Nothing
        Me.lvEmployeeList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhEmployeeCode, Me.colhEmployeeName})
        Me.lvEmployeeList.CompulsoryColumns = ""
        Me.lvEmployeeList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEmployeeList.FullRowSelect = True
        Me.lvEmployeeList.GridLines = True
        Me.lvEmployeeList.GroupingColumn = Nothing
        Me.lvEmployeeList.HideSelection = False
        Me.lvEmployeeList.Location = New System.Drawing.Point(0, 0)
        Me.lvEmployeeList.MinColumnWidth = 50
        Me.lvEmployeeList.MultiSelect = False
        Me.lvEmployeeList.Name = "lvEmployeeList"
        Me.lvEmployeeList.OptionalColumns = ""
        Me.lvEmployeeList.ShowMoreItem = False
        Me.lvEmployeeList.ShowSaveItem = False
        Me.lvEmployeeList.ShowSelectAll = True
        Me.lvEmployeeList.ShowSizeAllColumnsToFit = True
        Me.lvEmployeeList.Size = New System.Drawing.Size(247, 514)
        Me.lvEmployeeList.Sortable = True
        Me.lvEmployeeList.TabIndex = 125
        Me.lvEmployeeList.UseCompatibleStateImageBehavior = False
        Me.lvEmployeeList.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = "objcolhCheck"
        Me.objcolhCheck.Width = 25
        '
        'colhEmployeeCode
        '
        Me.colhEmployeeCode.Tag = "colhEmployeeCode"
        Me.colhEmployeeCode.Text = "Code"
        Me.colhEmployeeCode.Width = 80
        '
        'colhEmployeeName
        '
        Me.colhEmployeeName.Tag = "colhEmployeeName"
        Me.colhEmployeeName.Text = "Employee Name"
        Me.colhEmployeeName.Width = 138
        '
        'gbGroupAssign
        '
        Me.gbGroupAssign.BorderColor = System.Drawing.Color.Black
        Me.gbGroupAssign.Checked = False
        Me.gbGroupAssign.CollapseAllExceptThis = False
        Me.gbGroupAssign.CollapsedHoverImage = Nothing
        Me.gbGroupAssign.CollapsedNormalImage = Nothing
        Me.gbGroupAssign.CollapsedPressedImage = Nothing
        Me.gbGroupAssign.CollapseOnLoad = False
        Me.gbGroupAssign.Controls.Add(Me.dtpEndDate)
        Me.gbGroupAssign.Controls.Add(Me.lblEndDate)
        Me.gbGroupAssign.Controls.Add(Me.lblStartDate)
        Me.gbGroupAssign.Controls.Add(Me.dtpAssignDate)
        Me.gbGroupAssign.Controls.Add(Me.radDoNotOverwrite)
        Me.gbGroupAssign.Controls.Add(Me.cboValueBasis)
        Me.gbGroupAssign.Controls.Add(Me.btnEdit)
        Me.gbGroupAssign.Controls.Add(Me.radAdd)
        Me.gbGroupAssign.Controls.Add(Me.lblValueBasis)
        Me.gbGroupAssign.Controls.Add(Me.radOverwrite)
        Me.gbGroupAssign.Controls.Add(Me.lblBenefitInAmount)
        Me.gbGroupAssign.Controls.Add(Me.txtBenefitAmount)
        Me.gbGroupAssign.Controls.Add(Me.lblBenefitsInPercent)
        Me.gbGroupAssign.Controls.Add(Me.txtDBBenefitInPercent)
        Me.gbGroupAssign.ExpandedHoverImage = Nothing
        Me.gbGroupAssign.ExpandedNormalImage = Nothing
        Me.gbGroupAssign.ExpandedPressedImage = Nothing
        Me.gbGroupAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbGroupAssign.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbGroupAssign.HeaderHeight = 25
        Me.gbGroupAssign.HeaderMessage = ""
        Me.gbGroupAssign.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbGroupAssign.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbGroupAssign.HeightOnCollapse = 0
        Me.gbGroupAssign.LeftTextSpace = 0
        Me.gbGroupAssign.Location = New System.Drawing.Point(277, 211)
        Me.gbGroupAssign.Name = "gbGroupAssign"
        Me.gbGroupAssign.OpenHeight = 376
        Me.gbGroupAssign.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbGroupAssign.ShowBorder = True
        Me.gbGroupAssign.ShowCheckBox = False
        Me.gbGroupAssign.ShowCollapseButton = False
        Me.gbGroupAssign.ShowDefaultBorderColor = True
        Me.gbGroupAssign.ShowDownButton = False
        Me.gbGroupAssign.ShowHeader = True
        Me.gbGroupAssign.Size = New System.Drawing.Size(450, 26)
        Me.gbGroupAssign.TabIndex = 0
        Me.gbGroupAssign.Temp = 0
        Me.gbGroupAssign.Text = "Group Benefit Assignment"
        Me.gbGroupAssign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbGroupAssign.Visible = False
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(484, 31)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(95, 21)
        Me.dtpEndDate.TabIndex = 121
        Me.dtpEndDate.Visible = False
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(441, 34)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(69, 15)
        Me.lblEndDate.TabIndex = 122
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEndDate.Visible = False
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(264, 34)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(60, 15)
        Me.lblStartDate.TabIndex = 120
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblStartDate.Visible = False
        '
        'dtpAssignDate
        '
        Me.dtpAssignDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAssignDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAssignDate.Location = New System.Drawing.Point(330, 31)
        Me.dtpAssignDate.Name = "dtpAssignDate"
        Me.dtpAssignDate.Size = New System.Drawing.Size(85, 21)
        Me.dtpAssignDate.TabIndex = 119
        Me.dtpAssignDate.Visible = False
        '
        'radDoNotOverwrite
        '
        Me.radDoNotOverwrite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDoNotOverwrite.Location = New System.Drawing.Point(34, 27)
        Me.radDoNotOverwrite.Name = "radDoNotOverwrite"
        Me.radDoNotOverwrite.Size = New System.Drawing.Size(131, 17)
        Me.radDoNotOverwrite.TabIndex = 126
        Me.radDoNotOverwrite.TabStop = True
        Me.radDoNotOverwrite.Text = "Do not Overwrite"
        Me.radDoNotOverwrite.UseVisualStyleBackColor = True
        Me.radDoNotOverwrite.Visible = False
        '
        'cboValueBasis
        '
        Me.cboValueBasis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboValueBasis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboValueBasis.FormattingEnabled = True
        Me.cboValueBasis.Location = New System.Drawing.Point(83, 39)
        Me.cboValueBasis.Name = "cboValueBasis"
        Me.cboValueBasis.Size = New System.Drawing.Size(116, 21)
        Me.cboValueBasis.TabIndex = 14
        Me.cboValueBasis.Visible = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(205, 28)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(85, 30)
        Me.btnEdit.TabIndex = 124
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        Me.btnEdit.Visible = False
        '
        'radAdd
        '
        Me.radAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAdd.Location = New System.Drawing.Point(84, 34)
        Me.radAdd.Name = "radAdd"
        Me.radAdd.Size = New System.Drawing.Size(131, 17)
        Me.radAdd.TabIndex = 126
        Me.radAdd.TabStop = True
        Me.radAdd.Text = "Add if it does not Exist"
        Me.radAdd.UseVisualStyleBackColor = True
        Me.radAdd.Visible = False
        '
        'lblValueBasis
        '
        Me.lblValueBasis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValueBasis.Location = New System.Drawing.Point(3, 41)
        Me.lblValueBasis.Name = "lblValueBasis"
        Me.lblValueBasis.Size = New System.Drawing.Size(74, 15)
        Me.lblValueBasis.TabIndex = 13
        Me.lblValueBasis.Text = "Value Basis"
        Me.lblValueBasis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblValueBasis.Visible = False
        '
        'radOverwrite
        '
        Me.radOverwrite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radOverwrite.Location = New System.Drawing.Point(68, 28)
        Me.radOverwrite.Name = "radOverwrite"
        Me.radOverwrite.Size = New System.Drawing.Size(131, 17)
        Me.radOverwrite.TabIndex = 126
        Me.radOverwrite.TabStop = True
        Me.radOverwrite.Text = "Overwrite on Existing"
        Me.radOverwrite.UseVisualStyleBackColor = True
        Me.radOverwrite.Visible = False
        '
        'lblBenefitInAmount
        '
        Me.lblBenefitInAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitInAmount.Location = New System.Drawing.Point(309, 42)
        Me.lblBenefitInAmount.Name = "lblBenefitInAmount"
        Me.lblBenefitInAmount.Size = New System.Drawing.Size(49, 15)
        Me.lblBenefitInAmount.TabIndex = 25
        Me.lblBenefitInAmount.Text = "Amount"
        Me.lblBenefitInAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBenefitInAmount.Visible = False
        '
        'txtBenefitAmount
        '
        Me.txtBenefitAmount.AllowNegative = True
        Me.txtBenefitAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtBenefitAmount.DigitsInGroup = 0
        Me.txtBenefitAmount.Flags = 0
        Me.txtBenefitAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBenefitAmount.Location = New System.Drawing.Point(364, 39)
        Me.txtBenefitAmount.MaxDecimalPlaces = 6
        Me.txtBenefitAmount.MaxWholeDigits = 21
        Me.txtBenefitAmount.Name = "txtBenefitAmount"
        Me.txtBenefitAmount.Prefix = ""
        Me.txtBenefitAmount.RangeMax = 1.7976931348623157E+308
        Me.txtBenefitAmount.RangeMin = -1.7976931348623157E+308
        Me.txtBenefitAmount.Size = New System.Drawing.Size(68, 21)
        Me.txtBenefitAmount.TabIndex = 0
        Me.txtBenefitAmount.Text = "0.00"
        Me.txtBenefitAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtBenefitAmount.Visible = False
        '
        'lblBenefitsInPercent
        '
        Me.lblBenefitsInPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitsInPercent.Location = New System.Drawing.Point(205, 42)
        Me.lblBenefitsInPercent.Name = "lblBenefitsInPercent"
        Me.lblBenefitsInPercent.Size = New System.Drawing.Size(60, 15)
        Me.lblBenefitsInPercent.TabIndex = 24
        Me.lblBenefitsInPercent.Text = "Per.(%)"
        Me.lblBenefitsInPercent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBenefitsInPercent.Visible = False
        '
        'txtDBBenefitInPercent
        '
        Me.txtDBBenefitInPercent.AllowNegative = True
        Me.txtDBBenefitInPercent.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDBBenefitInPercent.DigitsInGroup = 0
        Me.txtDBBenefitInPercent.Flags = 0
        Me.txtDBBenefitInPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBBenefitInPercent.Location = New System.Drawing.Point(270, 39)
        Me.txtDBBenefitInPercent.MaxDecimalPlaces = 6
        Me.txtDBBenefitInPercent.MaxWholeDigits = 21
        Me.txtDBBenefitInPercent.Name = "txtDBBenefitInPercent"
        Me.txtDBBenefitInPercent.Prefix = ""
        Me.txtDBBenefitInPercent.RangeMax = 1.7976931348623157E+308
        Me.txtDBBenefitInPercent.RangeMin = -1.7976931348623157E+308
        Me.txtDBBenefitInPercent.Size = New System.Drawing.Size(33, 21)
        Me.txtDBBenefitInPercent.TabIndex = 27
        Me.txtDBBenefitInPercent.Text = "0"
        Me.txtDBBenefitInPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDBBenefitInPercent.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnAssign)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 553)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(899, 53)
        Me.objFooter.TabIndex = 1
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.ContextMenuStrip = Me.cmnuOperation
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(13, 11)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(106, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperation.TabIndex = 122
        Me.btnOperation.Text = "Operations"
        Me.btnOperation.Visible = False
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuViewDeleteEmpBenefits})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(295, 26)
        '
        'mnuViewDeleteEmpBenefits
        '
        Me.mnuViewDeleteEmpBenefits.Name = "mnuViewDeleteEmpBenefits"
        Me.mnuViewDeleteEmpBenefits.Size = New System.Drawing.Size(294, 22)
        Me.mnuViewDeleteEmpBenefits.Text = "View / Delete Assigned Employee Benefits"
        '
        'btnAssign
        '
        Me.btnAssign.BackColor = System.Drawing.Color.White
        Me.btnAssign.BackgroundImage = CType(resources.GetObject("btnAssign.BackgroundImage"), System.Drawing.Image)
        Me.btnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAssign.BorderColor = System.Drawing.Color.Empty
        Me.btnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAssign.FlatAppearance.BorderSize = 0
        Me.btnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAssign.ForeColor = System.Drawing.Color.Black
        Me.btnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Location = New System.Drawing.Point(682, 11)
        Me.btnAssign.Name = "btnAssign"
        Me.btnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Size = New System.Drawing.Size(97, 30)
        Me.btnAssign.TabIndex = 0
        Me.btnAssign.Text = "&Assign"
        Me.btnAssign.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(785, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Benefit Plan"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Tran. Head"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 150
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn3.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Period"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Tranheadunkid"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "PeriodId"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Exemptperiodid"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'frmAssignGroupBenefit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(899, 606)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlBenefitGroupAssign)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssignGroupBenefit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assign Benefit Group "
        Me.pnlBenefitGroupAssign.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbBenefitPlan.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.pnlBenfitPlan.ResumeLayout(False)
        Me.pnlBenfitPlan.PerformLayout()
        CType(Me.dgBenefitPlan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEmployeeList.ResumeLayout(False)
        Me.objpnlEmployeeList.ResumeLayout(False)
        Me.objpnlEmployeeList.PerformLayout()
        Me.gbGroupAssign.ResumeLayout(False)
        Me.gbGroupAssign.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlBenefitGroupAssign As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbGroupAssign As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objpnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpAssignDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboBenefitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBenefitType As System.Windows.Forms.Label
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents objChkAllEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents gbBenefitPlan As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboValueBasis As System.Windows.Forms.ComboBox
    Friend WithEvents lblValueBasis As System.Windows.Forms.Label
    Friend WithEvents txtDBBenefitInPercent As eZee.TextBox.NumericTextBox
    Friend WithEvents lblBenefitsInPercent As System.Windows.Forms.Label
    Friend WithEvents txtBenefitAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblBenefitInAmount As System.Windows.Forms.Label
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents lvEmployeeList As eZee.Common.eZeeListView
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployeeCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployeeName As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkOverwrite As System.Windows.Forms.CheckBox
    Friend WithEvents radDoNotOverwrite As System.Windows.Forms.RadioButton
    Friend WithEvents radOverwrite As System.Windows.Forms.RadioButton
    Friend WithEvents radAdd As System.Windows.Forms.RadioButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboUnit As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnit As System.Windows.Forms.Label
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents chkOverwritePrevEDSlabHeads As System.Windows.Forms.CheckBox
    Friend WithEvents objStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents dgBenefitPlan As System.Windows.Forms.DataGridView
    Friend WithEvents pnlBenfitPlan As System.Windows.Forms.Panel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSetPeriod As eZee.Common.eZeeLightButton
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents radApplytoAll As System.Windows.Forms.RadioButton
    Friend WithEvents radApplytoChecked As System.Windows.Forms.RadioButton
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIscheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhExempt As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents dgcolhBenefitPlan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTranshead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBenefitPlanunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTranheadunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPeriodId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhexemptperiodid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhDefaultAmount As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuViewDeleteEmpBenefits As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboDepartmentGrp As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartmentGroup As System.Windows.Forms.Label
    Friend WithEvents lblSectionGroup As System.Windows.Forms.Label
    Friend WithEvents cboSectionGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitGroup As System.Windows.Forms.Label
    Friend WithEvents cboUnitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents cboTeams As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblJobGroup As System.Windows.Forms.Label
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents rdShowOnlyReinstatedEmp As System.Windows.Forms.RadioButton
    Friend WithEvents rdShowOnlyNewHiredEmp As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class

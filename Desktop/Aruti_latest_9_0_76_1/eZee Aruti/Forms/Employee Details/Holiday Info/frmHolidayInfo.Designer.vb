﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHolidayInfo
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHolidayInfo))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lvHolidays = New System.Windows.Forms.ListView
        Me.colhAHDate = New System.Windows.Forms.ColumnHeader
        Me.colhAHName = New System.Windows.Forms.ColumnHeader
        Me.lvUnAssignedHoliday = New System.Windows.Forms.ListView
        Me.colhUHDate = New System.Windows.Forms.ColumnHeader
        Me.colhUHoliday = New System.Windows.Forms.ColumnHeader
        Me.gbRemark = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlRemark = New System.Windows.Forms.Panel
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.eHHolidayInfo = New eZee.Common.eZeeHeading
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.lblCaption = New System.Windows.Forms.Label
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objcolhACheck = New System.Windows.Forms.ColumnHeader
        Me.objcolhECheck = New System.Windows.Forms.ColumnHeader
        Me.objchkACheck = New System.Windows.Forms.CheckBox
        Me.objchkUCheck = New System.Windows.Forms.CheckBox
        Me.pnlMainInfo.SuspendLayout()
        Me.gbRemark.SuspendLayout()
        Me.pnlRemark.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objchkUCheck)
        Me.pnlMainInfo.Controls.Add(Me.objchkACheck)
        Me.pnlMainInfo.Controls.Add(Me.lvHolidays)
        Me.pnlMainInfo.Controls.Add(Me.lvUnAssignedHoliday)
        Me.pnlMainInfo.Controls.Add(Me.gbRemark)
        Me.pnlMainInfo.Controls.Add(Me.eHHolidayInfo)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.EZeeFooter1)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(659, 423)
        Me.pnlMainInfo.TabIndex = 0
        '
        'lvHolidays
        '
        Me.lvHolidays.AllowDrop = True
        Me.lvHolidays.CheckBoxes = True
        Me.lvHolidays.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhACheck, Me.colhAHDate, Me.colhAHName})
        Me.lvHolidays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvHolidays.FullRowSelect = True
        Me.lvHolidays.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvHolidays.Location = New System.Drawing.Point(4, 87)
        Me.lvHolidays.MultiSelect = False
        Me.lvHolidays.Name = "lvHolidays"
        Me.lvHolidays.Size = New System.Drawing.Size(428, 137)
        Me.lvHolidays.TabIndex = 11
        Me.lvHolidays.UseCompatibleStateImageBehavior = False
        Me.lvHolidays.View = System.Windows.Forms.View.Details
        '
        'colhAHDate
        '
        Me.colhAHDate.Tag = "colhAHDate"
        Me.colhAHDate.Text = "Date"
        Me.colhAHDate.Width = 95
        '
        'colhAHName
        '
        Me.colhAHName.Tag = "colhAHName"
        Me.colhAHName.Text = "Assigned Holiday"
        Me.colhAHName.Width = 272
        '
        'lvUnAssignedHoliday
        '
        Me.lvUnAssignedHoliday.AllowDrop = True
        Me.lvUnAssignedHoliday.CheckBoxes = True
        Me.lvUnAssignedHoliday.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhECheck, Me.colhUHDate, Me.colhUHoliday})
        Me.lvUnAssignedHoliday.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvUnAssignedHoliday.FullRowSelect = True
        Me.lvUnAssignedHoliday.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvUnAssignedHoliday.Location = New System.Drawing.Point(4, 230)
        Me.lvUnAssignedHoliday.MultiSelect = False
        Me.lvUnAssignedHoliday.Name = "lvUnAssignedHoliday"
        Me.lvUnAssignedHoliday.Size = New System.Drawing.Size(426, 135)
        Me.lvUnAssignedHoliday.TabIndex = 11
        Me.lvUnAssignedHoliday.UseCompatibleStateImageBehavior = False
        Me.lvUnAssignedHoliday.View = System.Windows.Forms.View.Details
        '
        'colhUHDate
        '
        Me.colhUHDate.Tag = "colhUHDate"
        Me.colhUHDate.Text = "Date"
        Me.colhUHDate.Width = 95
        '
        'colhUHoliday
        '
        Me.colhUHoliday.Tag = "colhUHoliday"
        Me.colhUHoliday.Text = "Exempted Holiday"
        Me.colhUHoliday.Width = 272
        '
        'gbRemark
        '
        Me.gbRemark.BorderColor = System.Drawing.Color.Black
        Me.gbRemark.Checked = False
        Me.gbRemark.CollapseAllExceptThis = False
        Me.gbRemark.CollapsedHoverImage = Nothing
        Me.gbRemark.CollapsedNormalImage = Nothing
        Me.gbRemark.CollapsedPressedImage = Nothing
        Me.gbRemark.CollapseOnLoad = False
        Me.gbRemark.Controls.Add(Me.pnlRemark)
        Me.gbRemark.ExpandedHoverImage = Nothing
        Me.gbRemark.ExpandedNormalImage = Nothing
        Me.gbRemark.ExpandedPressedImage = Nothing
        Me.gbRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRemark.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRemark.HeaderHeight = 25
        Me.gbRemark.HeaderMessage = ""
        Me.gbRemark.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbRemark.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRemark.HeightOnCollapse = 0
        Me.gbRemark.LeftTextSpace = 0
        Me.gbRemark.Location = New System.Drawing.Point(436, 87)
        Me.gbRemark.Name = "gbRemark"
        Me.gbRemark.OpenHeight = 300
        Me.gbRemark.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRemark.ShowBorder = True
        Me.gbRemark.ShowCheckBox = False
        Me.gbRemark.ShowCollapseButton = False
        Me.gbRemark.ShowDefaultBorderColor = True
        Me.gbRemark.ShowDownButton = False
        Me.gbRemark.ShowHeader = True
        Me.gbRemark.Size = New System.Drawing.Size(220, 278)
        Me.gbRemark.TabIndex = 7
        Me.gbRemark.Temp = 0
        Me.gbRemark.Text = "Remark"
        Me.gbRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlRemark
        '
        Me.pnlRemark.Controls.Add(Me.txtRemark)
        Me.pnlRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlRemark.Location = New System.Drawing.Point(2, 26)
        Me.pnlRemark.Name = "pnlRemark"
        Me.pnlRemark.Size = New System.Drawing.Size(215, 251)
        Me.pnlRemark.TabIndex = 2
        '
        'txtRemark
        '
        Me.txtRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(215, 251)
        Me.txtRemark.TabIndex = 12
        '
        'eHHolidayInfo
        '
        Me.eHHolidayInfo.BorderColor = System.Drawing.Color.Black
        Me.eHHolidayInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eHHolidayInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.eHHolidayInfo.Location = New System.Drawing.Point(4, 59)
        Me.eHHolidayInfo.Name = "eHHolidayInfo"
        Me.eHHolidayInfo.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        Me.eHHolidayInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.eHHolidayInfo.ShowDefaultBorderColor = True
        Me.eHHolidayInfo.Size = New System.Drawing.Size(652, 27)
        Me.eHHolidayInfo.TabIndex = 7
        Me.eHHolidayInfo.Text = "Holiday Information"
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = "Description"
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(659, 58)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Holiday Information"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.lblCaption)
        Me.EZeeFooter1.Controls.Add(Me.btnOk)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 368)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(659, 55)
        Me.EZeeFooter1.TabIndex = 4
        '
        'lblCaption
        '
        Me.lblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption.ForeColor = System.Drawing.Color.Maroon
        Me.lblCaption.Location = New System.Drawing.Point(13, 19)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(401, 17)
        Me.lblCaption.TabIndex = 72
        Me.lblCaption.Text = "Drag and Drop Checked Holiday(s) to Assign and Unassigned."
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(455, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(93, 30)
        Me.btnOk.TabIndex = 71
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(554, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 70
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objcolhACheck
        '
        Me.objcolhACheck.Tag = "objcolhACheck"
        Me.objcolhACheck.Text = ""
        Me.objcolhACheck.Width = 25
        '
        'objcolhECheck
        '
        Me.objcolhECheck.Tag = "objcolhECheck"
        Me.objcolhECheck.Text = ""
        Me.objcolhECheck.Width = 25
        '
        'objchkACheck
        '
        Me.objchkACheck.AutoSize = True
        Me.objchkACheck.Location = New System.Drawing.Point(12, 91)
        Me.objchkACheck.Name = "objchkACheck"
        Me.objchkACheck.Size = New System.Drawing.Size(15, 14)
        Me.objchkACheck.TabIndex = 12
        Me.objchkACheck.UseVisualStyleBackColor = True
        '
        'objchkUCheck
        '
        Me.objchkUCheck.AutoSize = True
        Me.objchkUCheck.Location = New System.Drawing.Point(12, 234)
        Me.objchkUCheck.Name = "objchkUCheck"
        Me.objchkUCheck.Size = New System.Drawing.Size(15, 14)
        Me.objchkUCheck.TabIndex = 13
        Me.objchkUCheck.UseVisualStyleBackColor = True
        '
        'frmHolidayInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(659, 423)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHolidayInfo"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Holiday Information"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        Me.gbRemark.ResumeLayout(False)
        Me.pnlRemark.ResumeLayout(False)
        Me.pnlRemark.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents lvHolidays As System.Windows.Forms.ListView
    Friend WithEvents colhAHDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAHName As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbRemark As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlRemark As System.Windows.Forms.Panel
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents lvUnAssignedHoliday As System.Windows.Forms.ListView
    Friend WithEvents colhUHDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUHoliday As System.Windows.Forms.ColumnHeader
    Friend WithEvents eHHolidayInfo As eZee.Common.eZeeHeading
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents objcolhACheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhECheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkUCheck As System.Windows.Forms.CheckBox
    Friend WithEvents objchkACheck As System.Windows.Forms.CheckBox
End Class

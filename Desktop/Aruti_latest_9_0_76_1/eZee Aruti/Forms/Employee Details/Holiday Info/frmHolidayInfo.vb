﻿
#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language
#End Region

Public Class frmHolidayInfo

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmHolidayInfo"
    Private mblnCancel As Boolean = True
    Private mstrHoidayIds As String = String.Empty
    Private mstrRemark As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private objEmpHoliday As clsemployee_holiday
    Private StrSource As String = ""
    Private mblnDeleteAll As Boolean = False
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByVal intEmpId As Integer, ByRef strHolidayIds As String, ByRef strRemark As String, ByRef blnDeleteAll As Boolean) As Boolean
        Try
            mintEmployeeId = intEmpId
            mstrHoidayIds = strHolidayIds
            mstrRemark = strRemark
            mblnDeleteAll = blnDeleteAll
            Me.ShowDialog()
            strHolidayIds = mstrHoidayIds
            strRemark = mstrRemark
            blnDeleteAll = mblnDeleteAll
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Functions "

    'Private Sub FillCombo()
    '    Dim objMaster As New clsMasterData
    '    Dim dsCombo As New DataSet
    '    Try
    '        dsCombo = objMaster.getComboListPAYYEAR("Year", True)
    '        With cboLeaveYear
    '            .ValueMember = "Id"
    '            .DisplayMember = "name"
    '            .DataSource = dsCombo.Tables(0)
    '            .SelectedValue = FinancialYear._Object._YearUnkid
    '        End With
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub FillHolidayList()
        Dim dsHoliday As New DataSet
        Dim dtHoliday As DataTable = Nothing
        Dim strSearch As String = String.Empty
        Dim StrAssignedHoliday As String = ""
        Try
            Dim objHoliday As New clsholiday_master
            Dim objEHoliday As New clsemployee_holiday

            If mintEmployeeId <= 0 Then
                dsHoliday = objHoliday.GetList("Holiday", True)
                dtHoliday = New DataView(dsHoliday.Tables("Holiday"), "", "", DataViewRowState.CurrentRows).ToTable
            Else

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'dsHoliday = objEHoliday.GetList("Holiday", mintEmployeeId)
                dsHoliday = objEHoliday.GetList("Holiday", FinancialYear._Object._DatabaseName _
                                                , User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                , True, ConfigParameter._Object._IsIncludeInactiveEmp, False, mintEmployeeId, Nothing, "", "")
                'Pinkal (24-Aug-2015) -- End


                dtHoliday = New DataView(dsHoliday.Tables("Holiday"), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            lvHolidays.Items.Clear()
            If Not dtHoliday Is Nothing Then
                Dim lvItem As ListViewItem

                For Each drRow As DataRow In dtHoliday.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = ""
                    lvItem.Tag = drRow("holidayunkid")
                    lvItem.SubItems.Add(eZeeDate.convertDate(drRow("holidaydate").ToString).ToShortDateString)
                    lvItem.SubItems.Add(drRow("holidayname").ToString)
                    lvHolidays.Items.Add(lvItem)
                    StrAssignedHoliday &= "," & lvItem.Tag.ToString
                Next
                If StrAssignedHoliday.Trim.Length > 0 Then StrAssignedHoliday = Mid(StrAssignedHoliday, 2)
            End If

            dsHoliday = objHoliday.GetList("Holiday", True)
            If StrAssignedHoliday.Trim.Length > 0 Then
                dtHoliday = New DataView(dsHoliday.Tables("Holiday"), "holidayunkid NOT IN(" & StrAssignedHoliday & ")", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtHoliday = New DataView(dsHoliday.Tables("Holiday"), "", "", DataViewRowState.CurrentRows).ToTable
            End If
                lvUnAssignedHoliday.Items.Clear()
                If Not dtHoliday Is Nothing Then
                    Dim lvItem As ListViewItem

                    For Each drRow As DataRow In dtHoliday.Rows
                        lvItem = New ListViewItem
                        lvItem.Text = ""
                        lvItem.Tag = drRow("holidayunkid")
                        lvItem.SubItems.Add(eZeeDate.convertDate(drRow("holidaydate").ToString).ToShortDateString)
                        lvItem.SubItems.Add(drRow("holidayname").ToString)
                        lvUnAssignedHoliday.Items.Add(lvItem)
                    Next
                End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillHolidayList", mstrModuleName)
        End Try
    End Sub

    Private Function GetSelectedHolidayID() As String
        Dim strHoliday As String = String.Empty
        Try

            For i As Integer = 0 To lvHolidays.Items.Count - 1
                strHoliday = strHoliday & CInt(lvHolidays.Items(i).Tag) & ","
            Next

            If strHoliday.Length > 0 Then
                strHoliday = strHoliday.Remove(strHoliday.Length - 1)
            End If

            Return strHoliday

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSelectedHolidayID", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Private Sub SetListViewOperation(ByVal LView As ListView, ByVal blnOperation As Boolean)
        Try
            For Each lvItem As ListViewItem In LView.Items
                lvItem.Checked = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmHolidayInfo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmpHoliday = New clsemployee_holiday
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            'Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Call FillCombo()

            FillHolidayList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidayInfo_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHolidayInfo_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidayInfo_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHolidayInfo_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.O Then
                btnOk_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidayInfo_KeyDown", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            'Call SetMessages()

            objfrm.displayDialog(Me)

            'Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Buttons "

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            Call btnClose_Click(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mstrHoidayIds = GetSelectedHolidayID()
            If mstrHoidayIds.Trim.Length <= 0 Then
                mblnDeleteAll = True
            End If
            mstrRemark = txtRemark.Text
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    'Private Sub cboLeaveYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        FillHolidayList()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboLeaveYear_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub ListView_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles lvHolidays.ItemDrag, lvUnAssignedHoliday.ItemDrag
        Try
            RemoveHandler lvHolidays.ItemChecked, AddressOf lvHolidays_ItemChecked
            RemoveHandler lvUnAssignedHoliday.ItemChecked, AddressOf lvUnAssignedHoliday_ItemChecked
            RemoveHandler objchkACheck.CheckedChanged, AddressOf objchkACheck_CheckedChanged
            RemoveHandler objchkUCheck.CheckedChanged, AddressOf objchkUCheck_CheckedChanged

            Dim myItem As ListViewItem
            Dim myItems(sender.CheckedItems.Count) As ListViewItem

            StrSource = CType(sender, ListView).Name

            Dim i As Integer = 0

            ' Loop though the SelectedItems collection for the source.
            'For Each myItem In sender.SelectedItems
            For Each myItem In sender.CheckedItems
                ' Add the ListViewItem to the array of ListViewItems.
                myItems(i) = myItem
                i = i + 1
            Next
            ' Create a DataObject containg the array of ListViewItems.
            sender.DoDragDrop(New DataObject("System.Windows.Forms.ListViewItem()", myItems), DragDropEffects.Move)

            objchkACheck.CheckState = CheckState.Unchecked
            objchkUCheck.CheckState = CheckState.Unchecked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ListView_ItemDrag", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ListView_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles lvHolidays.DragEnter, lvUnAssignedHoliday.DragEnter
        Try
            ' Check for the custom DataFormat ListViewItem array.
            If e.Data.GetDataPresent("System.Windows.Forms.ListViewItem()") Then
                e.Effect = DragDropEffects.Move
            Else
                e.Effect = DragDropEffects.None
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ListView_DragEnter", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ListView_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles lvHolidays.DragDrop, lvUnAssignedHoliday.DragDrop
        Try
            If CType(sender, ListView).Name <> StrSource Then
                Dim myItem As ListViewItem
                Dim myItems() As ListViewItem = CType(e.Data.GetData("System.Windows.Forms.ListViewItem()"), ListViewItem())
                Dim i As Integer = 0

                For Each myItem In myItems
                    If myItem IsNot Nothing Then
                        ' Remove the item from the source list.
                        If sender Is lvHolidays Then
                            lvUnAssignedHoliday.Items.Remove(lvUnAssignedHoliday.CheckedItems.Item(0))
                        Else
                            lvHolidays.Items.Remove(lvHolidays.CheckedItems.Item(0))
                        End If

                        ' Add the item to the target list.
                        sender.Items.Add(myItems(i))
                        myItems(i).Checked = False
                        i = i + 1
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ListView_DragDrop", mstrModuleName)
        Finally
            AddHandler lvHolidays.ItemChecked, AddressOf lvHolidays_ItemChecked
            AddHandler lvUnAssignedHoliday.ItemChecked, AddressOf lvUnAssignedHoliday_ItemChecked
            AddHandler objchkACheck.CheckedChanged, AddressOf objchkACheck_CheckedChanged
            AddHandler objchkUCheck.CheckedChanged, AddressOf objchkUCheck_CheckedChanged
        End Try
    End Sub

    Private Sub objchkUCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkUCheck.CheckedChanged
        Try
            RemoveHandler lvUnAssignedHoliday.ItemChecked, AddressOf lvUnAssignedHoliday_ItemChecked
            Call SetListViewOperation(lvUnAssignedHoliday, CBool(objchkUCheck.CheckState))
            AddHandler lvUnAssignedHoliday.ItemChecked, AddressOf lvUnAssignedHoliday_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkUCheck_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvUnAssignedHoliday_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvUnAssignedHoliday.ItemChecked
        Try
            RemoveHandler objchkUCheck.CheckedChanged, AddressOf objchkUCheck_CheckedChanged
            If lvUnAssignedHoliday.CheckedItems.Count <= 0 Then
                objchkUCheck.CheckState = CheckState.Unchecked
            ElseIf lvUnAssignedHoliday.CheckedItems.Count < lvUnAssignedHoliday.Items.Count Then
                objchkUCheck.CheckState = CheckState.Indeterminate
            ElseIf lvUnAssignedHoliday.CheckedItems.Count = lvUnAssignedHoliday.Items.Count Then
                objchkUCheck.CheckState = CheckState.Checked
            End If
            AddHandler objchkUCheck.CheckedChanged, AddressOf objchkUCheck_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvUnAssignedHoliday_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkACheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkACheck.CheckedChanged
        Try
            RemoveHandler lvHolidays.ItemChecked, AddressOf lvHolidays_ItemChecked
            Call SetListViewOperation(lvHolidays, CBool(objchkACheck.CheckState))
            AddHandler lvHolidays.ItemChecked, AddressOf lvHolidays_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkACheck_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvHolidays_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvHolidays.ItemChecked
        Try
            RemoveHandler objchkACheck.CheckedChanged, AddressOf objchkACheck_CheckedChanged
            If lvHolidays.CheckedItems.Count <= 0 Then
                objchkACheck.CheckState = CheckState.Unchecked
            ElseIf lvHolidays.CheckedItems.Count < lvHolidays.Items.Count Then
                objchkACheck.CheckState = CheckState.Indeterminate
            ElseIf lvHolidays.CheckedItems.Count = lvHolidays.Items.Count Then
                objchkACheck.CheckState = CheckState.Checked
            End If
            AddHandler objchkACheck.CheckedChanged, AddressOf objchkACheck_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvHolidays_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbRemark.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbRemark.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.colhAHDate.Text = Language._Object.getCaption(CStr(Me.colhAHDate.Tag), Me.colhAHDate.Text)
			Me.colhAHName.Text = Language._Object.getCaption(CStr(Me.colhAHName.Tag), Me.colhAHName.Text)
			Me.gbRemark.Text = Language._Object.getCaption(Me.gbRemark.Name, Me.gbRemark.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.colhUHDate.Text = Language._Object.getCaption(CStr(Me.colhUHDate.Tag), Me.colhUHDate.Text)
			Me.colhUHoliday.Text = Language._Object.getCaption(CStr(Me.colhUHoliday.Tag), Me.colhUHoliday.Text)
			Me.eHHolidayInfo.Text = Language._Object.getCaption(Me.eHHolidayInfo.Name, Me.eHHolidayInfo.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'Public Class frmHolidayInfo

'#Region " Private Variables "
'    Private ReadOnly mstrModuleName As String = "frmHolidayInfo"
'    Private mblnCancel As Boolean = True
'    Private mstrHoidayIds As String = String.Empty
'    Private mstrRemark As String = String.Empty
'    Private mintEmployeeId As Integer = -1
'    Private objEmpHoliday As clsemployee_holiday
'#End Region

'#Region " Display Dialog "
'    Public Function displayDialog(ByVal intEmpId As Integer, ByRef strHolidayIds As String, ByRef strRemark As String) As Boolean
'        Try
'            mintEmployeeId = intEmpId
'            mstrHoidayIds = strHolidayIds
'            mstrRemark = strRemark
'            Me.ShowDialog()
'            strHolidayIds = mstrHoidayIds
'            strRemark = mstrRemark
'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function
'#End Region

'#Region " Private Functions "
'    Private Sub FillCombo()
'        Dim objMaster As New clsMasterData
'        Dim dsCombo As New DataSet
'        Try
'            dsCombo = objMaster.getComboListPAYYEAR("Year", True)
'            With cboLeaveYear
'                .ValueMember = "Id"
'                .DisplayMember = "name"
'                .DataSource = dsCombo.Tables(0)
'                .SelectedValue = FinancialYear._Object._YearUnkid
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillHolidayList()
'        Dim dsHoliday As DataSet = Nothing
'        Dim dtHoliday As DataTable = Nothing
'        Dim strSearch As String = String.Empty
'        Try
'            Dim objHoliday As New clsholiday_master
'            dsHoliday = objHoliday.GetList("Holiday", True)
'            If CInt(cboLeaveYear.SelectedValue) > 0 Then
'                strSearch = "AND yearunkid=" & CInt(cboLeaveYear.SelectedValue)
'            End If

'            If strSearch.Length > 0 Then
'                strSearch = strSearch.Substring(3)
'                dtHoliday = New DataView(dsHoliday.Tables("Holiday"), strSearch, "", DataViewRowState.CurrentRows).ToTable
'            End If

'            lvHolidays.Items.Clear()
'            If Not dtHoliday Is Nothing Then
'                Dim lvItem As ListViewItem

'                For Each drRow As DataRow In dtHoliday.Rows
'                    lvItem = New ListViewItem
'                    lvItem.Text = eZeeDate.convertDate(drRow("holidaydate").ToString).ToShortDateString
'                    lvItem.Tag = drRow("holidayunkid")
'                    lvItem.SubItems.Add(drRow("holidayname").ToString)
'                    lvHolidays.Items.Add(lvItem)
'                Next
'            End If
'            If lvHolidays.Items.Count > 0 Then
'                chkHolidayAll.Enabled = True
'            Else
'                chkHolidayAll.Enabled = False
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillHolidayList", mstrModuleName)
'        End Try
'    End Sub

'    Private Function GetSelectedHolidayID() As String
'        Dim strHoliday As String = String.Empty
'        Try

'            For i As Integer = 0 To lvHolidays.CheckedItems.Count - 1
'                strHoliday = strHoliday & CInt(lvHolidays.CheckedItems(i).Tag) & ","
'            Next
'            If strHoliday.Length > 0 Then
'                strHoliday = strHoliday.Remove(strHoliday.Length - 1)
'            End If

'            Return strHoliday

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetSelectedHolidayID", mstrModuleName)
'            Return Nothing
'        End Try
'    End Function

'    Private Sub SetSelectedHoliday()
'        Dim dtTable As DataTable = Nothing
'        Try
'            dtTable = objEmpHoliday.GetEmployeeHoliday(mintEmployeeId)
'            If dtTable.Rows.Count > 0 Then
'                For i As Integer = 0 To lvHolidays.Items.Count - 1
'                    For j As Integer = 0 To dtTable.Rows.Count - 1
'                        If CInt(lvHolidays.Items(i).Tag) = CInt(dtTable.Rows(j)("holidayunkid")) Then
'                            lvHolidays.Items(i).Checked = True
'                        End If
'                    Next
'                Next
'                objEmpHoliday._Holidaytranunkid = CInt(dtTable.Rows(0)("holidaytranunkid"))
'                txtRemark.Text = objEmpHoliday._Remarks
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetSelectedHoliday", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Form's Event "
'    Private Sub frmHolidayInfo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        objEmpHoliday = New clsemployee_holiday
'        Try
'            Call Set_Logo(Me, gApplicationType)

'            'Anjan (02 Sep 2011)-Start
'            'Issue : Including Language Settings.
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            'Anjan (02 Sep 2011)-End 

'            Call FillCombo()
'            Call SetSelectedHoliday()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmHolidayInfo_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmHolidayInfo_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
'        Try
'            If Asc(e.KeyChar) = 13 Then
'                SendKeys.Send("{TAB}")
'                e.Handled = True
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmHolidayInfo_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmHolidayInfo_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
'        Try
'            If e.Control = True And e.KeyCode = Keys.O Then
'                btnOk_Click(sender, e)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmHolidayInfo_KeyDown", mstrModuleName)
'        End Try
'    End Sub
'    'Anjan (02 Sep 2011)-Start
'    'Issue : Including Language Settings.
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'Anjan (02 Sep 2011)-End 

'#End Region

'#Region " Buttons "

'    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
'        Try
'            Call btnClose_Click(sender, e)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            mstrHoidayIds = GetSelectedHolidayID()
'            mstrRemark = txtRemark.Text
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Controls "

'    Private Sub cboLeaveYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLeaveYear.SelectedIndexChanged
'        Try
'            FillHolidayList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboLeaveYear_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub chkHolidayAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Try
'            For i As Integer = 0 To lvHolidays.Items.Count - 1
'                lvHolidays.Items(i).Checked = CBool(chkHolidayAll.CheckState)
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "chkHolidayAll_CheckedChanged", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'	'<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'	Private Sub OtherSettings()
'		Try
'			Me.SuspendLayout()

'			Call SetLanguage()

'			Me.gbHolidays.GradientColor = GUI._eZeeContainerHeaderBackColor 
'			Me.gbHolidays.ForeColor = GUI._eZeeContainerHeaderForeColor 

'			Me.gbRemark.GradientColor = GUI._eZeeContainerHeaderBackColor 
'			Me.gbRemark.ForeColor = GUI._eZeeContainerHeaderForeColor 


'			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
'			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
'			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
'			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
'			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


'			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
'			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

'			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
'			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


'			Me.ResumeLayout()
'		Catch Ex As Exception
'			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
'		End Try
'	End Sub


'	Private Sub SetLanguage()
'		Try
'			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

'			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
'			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
'			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
'			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
'			Me.gbHolidays.Text = Language._Object.getCaption(Me.gbHolidays.Name, Me.gbHolidays.Text)
'			Me.chkHolidayAll.Text = Language._Object.getCaption(Me.chkHolidayAll.Name, Me.chkHolidayAll.Text)
'			Me.lblholidays.Text = Language._Object.getCaption(Me.lblholidays.Name, Me.lblholidays.Text)
'			Me.lblLeaveYear.Text = Language._Object.getCaption(Me.lblLeaveYear.Name, Me.lblLeaveYear.Text)
'			Me.gbRemark.Text = Language._Object.getCaption(Me.gbRemark.Name, Me.gbRemark.Text)

'		Catch Ex As Exception
'			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
'		End Try
'	End Sub


'	Private Sub SetMessages()
'		Try

'		Catch Ex As Exception
'			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
'		End Try
'	End Sub
'#End Region 'Language & UI Settings
'	'</Language>
'End Class
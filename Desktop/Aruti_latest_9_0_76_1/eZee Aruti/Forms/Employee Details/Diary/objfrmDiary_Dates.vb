﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Dates

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mobjEmployee As clsEmployee_Master
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mobjEmployee = New clsEmployee_Master
        mintEmployeeId = intEmpId
        mobjEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = mintEmployeeId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDiary_Dates_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_Dates_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dsList As New DataSet
        Try
            If mobjEmployee._Birthdate <> Nothing Then
                txtBirthday.Text = CStr(mobjEmployee._Birthdate.Date)
                txtAge.Text = DateDiff(DateInterval.Year, mobjEmployee._Birthdate.Date, ConfigParameter._Object._CurrentDateAndTime.Date).ToString
                If mobjEmployee._Birthdate.Month > ConfigParameter._Object._CurrentDateAndTime.Month Then
                    txtNxtBirthday.Text = CDate(mobjEmployee._Birthdate.AddYears(CInt(txtAge.Text))).ToShortDateString
                Else
                    txtNxtBirthday.Text = CDate(mobjEmployee._Birthdate.AddYears(CInt(txtAge.Text) + 1)).ToShortDateString
                End If
            Else
                txtBirthday.Text = "" : txtAge.Text = "" : txtNxtBirthday.Text = ""
            End If

            If mobjEmployee._Anniversary_Date <> Nothing Then
                txtAnniversary.Text = CStr(mobjEmployee._Anniversary_Date.Date)
                txtYears.Text = DateDiff(DateInterval.Year, mobjEmployee._Anniversary_Date.Date, ConfigParameter._Object._CurrentDateAndTime.Date).ToString
                If mobjEmployee._Anniversary_Date.Month > ConfigParameter._Object._CurrentDateAndTime.Month Then
                    txtNxtAnniversary.Text = CDate(mobjEmployee._Anniversary_Date.AddYears(CInt(txtYears.Text))).ToShortDateString
                Else
                    txtNxtAnniversary.Text = CDate(mobjEmployee._Anniversary_Date.AddYears(CInt(txtYears.Text) + 1)).ToShortDateString
                End If
            Else
                txtAnniversary.Text = "" : txtYears.Text = "" : txtNxtAnniversary.Text = ""
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = mobjEmployee.AT_Emp_Susp_Prob(mintEmployeeId)
            dsList = mobjEmployee.AT_Emp_Susp_Prob(mintEmployeeId, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'S.SANDEEP [04 JUN 2015] -- END

            Call Fill(dsList)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill(ByVal dsData As DataSet)
        Try
            Dim iCnt As Integer = 1
            Dim iGrpId As Integer = -1

            For Each dtRow As DataRow In dsData.Tables(0).Rows
                Dim lvitem As New ListViewItem

                If iGrpId <> CInt(dtRow.Item("ModeId")) Then
                    iCnt = 1
                    iGrpId = CInt(dtRow.Item("ModeId"))
                End If

                lvitem.Text = iCnt.ToString
                lvitem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("FrmDate").ToString).ToShortDateString)
                If dtRow.Item("ToDate").ToString.Trim.Length > 0 Then
                    lvitem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("ToDate").ToString).ToShortDateString)
                Else
                    lvitem.SubItems.Add("")
                End If
                lvitem.SubItems.Add(dtRow.Item("Remark").ToString)
                lvitem.SubItems.Add(dtRow.Item("Mode").ToString)
                lvitem.SubItems.Add(dtRow.Item("PEx").ToString)
                lvitem.SubItems.Add(dtRow.Item("ModeId").ToString)
                lvEmpDates.Items.Add(lvitem)

                iCnt += 1
            Next

            lvEmpDates.GridLines = False
            lvEmpDates.GroupingColumn = objcolhMode
            lvEmpDates.DisplayGroups(True)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub lvSuspension_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs)
        Try
            e.Item.Selected = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvProbation_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs)
        Try
            e.Item.Selected = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvProbation_ItemSelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elEmployeeDates.Text = Language._Object.getCaption(Me.elEmployeeDates.Name, Me.elEmployeeDates.Text)
			Me.colhSrNo.Text = Language._Object.getCaption(CStr(Me.colhSrNo.Tag), Me.colhSrNo.Text)
			Me.colhFromDate.Text = Language._Object.getCaption(CStr(Me.colhFromDate.Tag), Me.colhFromDate.Text)
			Me.colhToDate.Text = Language._Object.getCaption(CStr(Me.colhToDate.Tag), Me.colhToDate.Text)
			Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
			Me.lblYears.Text = Language._Object.getCaption(Me.lblYears.Name, Me.lblYears.Text)
			Me.lblNxtAnniversary.Text = Language._Object.getCaption(Me.lblNxtAnniversary.Name, Me.lblNxtAnniversary.Text)
			Me.lblAnniversary.Text = Language._Object.getCaption(Me.lblAnniversary.Name, Me.lblAnniversary.Text)
			Me.lblAge.Text = Language._Object.getCaption(Me.lblAge.Name, Me.lblAge.Text)
			Me.lblNxtBirthday.Text = Language._Object.getCaption(Me.lblNxtBirthday.Name, Me.lblNxtBirthday.Text)
			Me.lblBirthday.Text = Language._Object.getCaption(Me.lblBirthday.Name, Me.lblBirthday.Text)
			Me.elBirthday_Anniversary.Text = Language._Object.getCaption(Me.elBirthday_Anniversary.Name, Me.elBirthday_Anniversary.Text)
			Me.colhPayExcluded.Text = Language._Object.getCaption(CStr(Me.colhPayExcluded.Tag), Me.colhPayExcluded.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
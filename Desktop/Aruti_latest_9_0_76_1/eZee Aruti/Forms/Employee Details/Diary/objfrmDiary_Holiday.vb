﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Holiday

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mobjHolidays As clsemployee_holiday
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mobjHolidays = New clsemployee_holiday
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDiary_Holiday_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_Holiday_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Try

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'dsList = mobjHolidays.GetList("List")
            dsList = mobjHolidays.GetList("List", FinancialYear._Object._DatabaseName _
                                                        , User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                        , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, -1, Nothing, "", "")
            'Pinkal (24-Aug-2015) -- End


            If dsList.Tables(0).Rows.Count > 0 Then
                dtTable = New DataView(dsList.Tables(0), "employeeunkid = '" & mintEmployeeId & "' AND holidaydate < '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "'", "", DataViewRowState.CurrentRows).ToTable
                Call Fill(dtTable, lvOldHoliday)

                dtTable = New DataView(dsList.Tables(0), "employeeunkid = '" & mintEmployeeId & "'AND holidaydate > '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "'", "", DataViewRowState.CurrentRows).ToTable
                Call Fill(dtTable, lvNewHoliday)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill(ByVal dsData As DataTable, ByVal LVList As ListView)
        Try
            For Each dtRow As DataRow In dsData.Rows
                Dim lvitem As New ListViewItem
                lvitem.UseItemStyleForSubItems = False

                lvitem.Text = dtRow.Item("holidayname").ToString
                lvitem.ForeColor = Color.White
                lvitem.BackColor = Color.FromArgb(CInt(dtRow.Item("color")))
                lvitem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("holidaydate").ToString).ToShortDateString)

                LVList.Items.Add(lvitem)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub lvHolidayListView_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvNewHoliday.ItemSelectionChanged, lvOldHoliday.ItemSelectionChanged
        Try
            e.Item.Selected = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvHolidayListView_ItemSelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elPreviousHolidays.Text = Language._Object.getCaption(Me.elPreviousHolidays.Name, Me.elPreviousHolidays.Text)
			Me.elUpcomingHoliday.Text = Language._Object.getCaption(Me.elUpcomingHoliday.Name, Me.elUpcomingHoliday.Text)
			Me.colhPreviousHoliday.Text = Language._Object.getCaption(CStr(Me.colhPreviousHoliday.Tag), Me.colhPreviousHoliday.Text)
			Me.colhPreviousdates.Text = Language._Object.getCaption(CStr(Me.colhPreviousdates.Tag), Me.colhPreviousdates.Text)
			Me.colhUpcomingHolidays.Text = Language._Object.getCaption(CStr(Me.colhUpcomingHolidays.Tag), Me.colhUpcomingHolidays.Text)
			Me.colhUpcomingdate.Text = Language._Object.getCaption(CStr(Me.colhUpcomingdate.Tag), Me.colhUpcomingdate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
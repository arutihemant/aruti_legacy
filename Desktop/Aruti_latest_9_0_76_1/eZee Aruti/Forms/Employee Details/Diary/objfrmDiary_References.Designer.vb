﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_References
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.elEmployeeReferences = New eZee.Common.eZeeLine
        Me.lvRefreeList = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhRefreeName = New System.Windows.Forms.ColumnHeader
        Me.colhCountry = New System.Windows.Forms.ColumnHeader
        Me.colhIdNo = New System.Windows.Forms.ColumnHeader
        Me.colhEmail = New System.Windows.Forms.ColumnHeader
        Me.colhTelNo = New System.Windows.Forms.ColumnHeader
        Me.colhMobile = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'elEmployeeReferences
        '
        Me.elEmployeeReferences.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elEmployeeReferences.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elEmployeeReferences.Location = New System.Drawing.Point(3, 12)
        Me.elEmployeeReferences.Name = "elEmployeeReferences"
        Me.elEmployeeReferences.Size = New System.Drawing.Size(575, 17)
        Me.elEmployeeReferences.TabIndex = 317
        Me.elEmployeeReferences.Text = "Employee References"
        Me.elEmployeeReferences.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvRefreeList
        '
        Me.lvRefreeList.BackColorOnChecked = True
        Me.lvRefreeList.ColumnHeaders = Nothing
        Me.lvRefreeList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhRefreeName, Me.colhCountry, Me.colhIdNo, Me.colhEmail, Me.colhTelNo, Me.colhMobile})
        Me.lvRefreeList.CompulsoryColumns = ""
        Me.lvRefreeList.FullRowSelect = True
        Me.lvRefreeList.GridLines = True
        Me.lvRefreeList.GroupingColumn = Nothing
        Me.lvRefreeList.HideSelection = False
        Me.lvRefreeList.Location = New System.Drawing.Point(6, 32)
        Me.lvRefreeList.MinColumnWidth = 50
        Me.lvRefreeList.MultiSelect = False
        Me.lvRefreeList.Name = "lvRefreeList"
        Me.lvRefreeList.OptionalColumns = ""
        Me.lvRefreeList.ShowMoreItem = False
        Me.lvRefreeList.ShowSaveItem = False
        Me.lvRefreeList.ShowSelectAll = True
        Me.lvRefreeList.ShowSizeAllColumnsToFit = True
        Me.lvRefreeList.Size = New System.Drawing.Size(572, 374)
        Me.lvRefreeList.Sortable = True
        Me.lvRefreeList.TabIndex = 318
        Me.lvRefreeList.UseCompatibleStateImageBehavior = False
        Me.lvRefreeList.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 0
        '
        'colhRefreeName
        '
        Me.colhRefreeName.Tag = "colhRefreeName"
        Me.colhRefreeName.Text = "Reference Name"
        Me.colhRefreeName.Width = 200
        '
        'colhCountry
        '
        Me.colhCountry.Tag = "colhCountry"
        Me.colhCountry.Text = "Country"
        Me.colhCountry.Width = 130
        '
        'colhIdNo
        '
        Me.colhIdNo.Tag = "colhIdNo"
        Me.colhIdNo.Text = "Company"
        Me.colhIdNo.Width = 130
        '
        'colhEmail
        '
        Me.colhEmail.Tag = "colhEmail"
        Me.colhEmail.Text = "Email"
        Me.colhEmail.Width = 150
        '
        'colhTelNo
        '
        Me.colhTelNo.Tag = "colhTelNo"
        Me.colhTelNo.Text = "Telephone No."
        Me.colhTelNo.Width = 100
        '
        'colhMobile
        '
        Me.colhMobile.Tag = "colhMobile"
        Me.colhMobile.Text = "Mobile No."
        Me.colhMobile.Width = 100
        '
        'objfrmDiary_References
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lvRefreeList)
        Me.Controls.Add(Me.elEmployeeReferences)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_References"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elEmployeeReferences As eZee.Common.eZeeLine
    Friend WithEvents lvRefreeList As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRefreeName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCountry As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIdNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmail As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTelNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMobile As System.Windows.Forms.ColumnHeader
End Class

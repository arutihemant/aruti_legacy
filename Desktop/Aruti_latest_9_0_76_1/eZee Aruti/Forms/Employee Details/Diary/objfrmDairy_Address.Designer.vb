﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Address
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbAddress = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkEmergencyContact = New System.Windows.Forms.LinkLabel
        Me.lnkClose = New System.Windows.Forms.LinkLabel
        Me.pnlEmergencyContact = New System.Windows.Forms.Panel
        Me.txtEmg3City = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmg3ZipCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmg3State = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmg3Country = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmg2City = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmg2ZipCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmg2State = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmg2Country = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmg1City = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmg1ZipCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmg1State = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmg1Country = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgEmail3 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgEmail3 = New System.Windows.Forms.Label
        Me.txtEmgFax3 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgFax3 = New System.Windows.Forms.Label
        Me.txtEmgFirstName3 = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgAltNo3 = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgTelNo3 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgTelNo3 = New System.Windows.Forms.Label
        Me.lblEmgAltNo3 = New System.Windows.Forms.Label
        Me.lblEmgFirstName3 = New System.Windows.Forms.Label
        Me.txtEmgLastName3 = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgPlotNo3 = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgMobile3 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgMobile3 = New System.Windows.Forms.Label
        Me.lblEmgLastName3 = New System.Windows.Forms.Label
        Me.lblEmgPlotNo3 = New System.Windows.Forms.Label
        Me.txtEmgAddress3 = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgProvince3 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgProvince3 = New System.Windows.Forms.Label
        Me.lblEmgAddress3 = New System.Windows.Forms.Label
        Me.lblEmgPostCountry3 = New System.Windows.Forms.Label
        Me.txtEmgEstate3 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgEstate3 = New System.Windows.Forms.Label
        Me.lblEmgPostTown3 = New System.Windows.Forms.Label
        Me.lblEmgState3 = New System.Windows.Forms.Label
        Me.txtEmgRoad3 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgPostCode3 = New System.Windows.Forms.Label
        Me.lblEmgRoad3 = New System.Windows.Forms.Label
        Me.lnEmergencyContact3 = New eZee.Common.eZeeLine
        Me.txtEmgEmail2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgEmail2 = New System.Windows.Forms.Label
        Me.txtEmgFax2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgFax2 = New System.Windows.Forms.Label
        Me.txtEmgFirstName2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgAltNo2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgTelNo2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgTelNo2 = New System.Windows.Forms.Label
        Me.lblEmgAltNo2 = New System.Windows.Forms.Label
        Me.lblEmgFirstName2 = New System.Windows.Forms.Label
        Me.txtEmgLastName2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgPlotNo2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgMobile2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgMobile2 = New System.Windows.Forms.Label
        Me.lblEmgLastName2 = New System.Windows.Forms.Label
        Me.lblEmgPlotNo2 = New System.Windows.Forms.Label
        Me.txtEmgAddress2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgProvince2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgProvince2 = New System.Windows.Forms.Label
        Me.lblEmgAddress2 = New System.Windows.Forms.Label
        Me.lblEmgPostCountry2 = New System.Windows.Forms.Label
        Me.txtEmgEstate2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgEstate2 = New System.Windows.Forms.Label
        Me.lblEmgPostTown2 = New System.Windows.Forms.Label
        Me.lblEmgState2 = New System.Windows.Forms.Label
        Me.txtEmgRoad2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgPostCode2 = New System.Windows.Forms.Label
        Me.lblEmgRoad2 = New System.Windows.Forms.Label
        Me.lnEmergencyContact2 = New eZee.Common.eZeeLine
        Me.lnEmergencyContact1 = New eZee.Common.eZeeLine
        Me.txtEmgEmail = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgEmail = New System.Windows.Forms.Label
        Me.txtEmgFax = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgFax = New System.Windows.Forms.Label
        Me.txtEmgFirstName = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgAltNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgTelNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgTelNo = New System.Windows.Forms.Label
        Me.lblEmgAltNo = New System.Windows.Forms.Label
        Me.lblEmgFirstName = New System.Windows.Forms.Label
        Me.txtEmgLastName = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgPlotNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgMobile = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgMobile = New System.Windows.Forms.Label
        Me.lblEmgLastName = New System.Windows.Forms.Label
        Me.lblEmgPlotNo = New System.Windows.Forms.Label
        Me.txtEmgAddress = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmgProvince = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgProvince = New System.Windows.Forms.Label
        Me.lblEmgAddress = New System.Windows.Forms.Label
        Me.lblEmgPostCountry = New System.Windows.Forms.Label
        Me.txtEmgEstate = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgEstate = New System.Windows.Forms.Label
        Me.lblEmgPostTown = New System.Windows.Forms.Label
        Me.lblEmgState = New System.Windows.Forms.Label
        Me.txtEmgRoad = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmgPostCode = New System.Windows.Forms.Label
        Me.lblEmgRoad = New System.Windows.Forms.Label
        Me.pnlPersonalAddress = New System.Windows.Forms.Panel
        Me.txtDZipCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtDCountry = New eZee.TextBox.AlphanumericTextBox
        Me.txtDCity = New eZee.TextBox.AlphanumericTextBox
        Me.txtDState = New eZee.TextBox.AlphanumericTextBox
        Me.txtPresentZipCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtPresentCountry = New eZee.TextBox.AlphanumericTextBox
        Me.txtPresentCity = New eZee.TextBox.AlphanumericTextBox
        Me.txtPresentState = New eZee.TextBox.AlphanumericTextBox
        Me.lblDomicileState = New System.Windows.Forms.Label
        Me.lblDomicileAddress2 = New System.Windows.Forms.Label
        Me.lblAddress2 = New System.Windows.Forms.Label
        Me.lblPresentState = New System.Windows.Forms.Label
        Me.lblPostTown = New System.Windows.Forms.Label
        Me.txtDomicileAltNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblDomicileAltNo = New System.Windows.Forms.Label
        Me.txtRoad = New eZee.TextBox.AlphanumericTextBox
        Me.lblRoad = New System.Windows.Forms.Label
        Me.txtDomicilePlotNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblDomicilePlotNo = New System.Windows.Forms.Label
        Me.txtDomicileProvince = New eZee.TextBox.AlphanumericTextBox
        Me.lblDomicileProvince = New System.Windows.Forms.Label
        Me.lblDomicilePostCode = New System.Windows.Forms.Label
        Me.txtFax = New eZee.TextBox.AlphanumericTextBox
        Me.lblFax = New System.Windows.Forms.Label
        Me.txtAlternativeNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtEstate = New eZee.TextBox.AlphanumericTextBox
        Me.lblEstate = New System.Windows.Forms.Label
        Me.txtProvince = New eZee.TextBox.AlphanumericTextBox
        Me.lblProvince = New System.Windows.Forms.Label
        Me.txtDomicileFax = New eZee.TextBox.AlphanumericTextBox
        Me.lblDomicileFax = New System.Windows.Forms.Label
        Me.txtDomicileMobile = New eZee.TextBox.AlphanumericTextBox
        Me.lblDomicileMobileNo = New System.Windows.Forms.Label
        Me.txtDomicileTelNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblDomicileTelNo = New System.Windows.Forms.Label
        Me.txtDomicileEstate = New eZee.TextBox.AlphanumericTextBox
        Me.txtDomicileRoad = New eZee.TextBox.AlphanumericTextBox
        Me.txtDomicileEmail = New eZee.TextBox.AlphanumericTextBox
        Me.txtDomicileAddress2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblDomicileRoad = New System.Windows.Forms.Label
        Me.lblDomicileEState = New System.Windows.Forms.Label
        Me.lblDomicileEmail = New System.Windows.Forms.Label
        Me.lblDomicilePostTown = New System.Windows.Forms.Label
        Me.lblDomicilePostCountry = New System.Windows.Forms.Label
        Me.txtDomicileAddress1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblDomicileAddress1 = New System.Windows.Forms.Label
        Me.lnDomicileAddress = New eZee.Common.eZeeLine
        Me.lblAlternativeNo = New System.Windows.Forms.Label
        Me.txtMobile = New eZee.TextBox.AlphanumericTextBox
        Me.txtPlotNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmail = New eZee.TextBox.AlphanumericTextBox
        Me.txtTelNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtAddress2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblPloteNo = New System.Windows.Forms.Label
        Me.lblMobile = New System.Windows.Forms.Label
        Me.lblEmail = New System.Windows.Forms.Label
        Me.lblTelNo = New System.Windows.Forms.Label
        Me.lblPostCountry = New System.Windows.Forms.Label
        Me.lblPostcode = New System.Windows.Forms.Label
        Me.txtAddress1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress = New System.Windows.Forms.Label
        Me.lnPresentAddress = New eZee.Common.eZeeLine
        Me.gbAddress.SuspendLayout()
        Me.pnlEmergencyContact.SuspendLayout()
        Me.pnlPersonalAddress.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbAddress
        '
        Me.gbAddress.BorderColor = System.Drawing.Color.Black
        Me.gbAddress.Checked = False
        Me.gbAddress.CollapseAllExceptThis = False
        Me.gbAddress.CollapsedHoverImage = Nothing
        Me.gbAddress.CollapsedNormalImage = Nothing
        Me.gbAddress.CollapsedPressedImage = Nothing
        Me.gbAddress.CollapseOnLoad = False
        Me.gbAddress.Controls.Add(Me.lnkEmergencyContact)
        Me.gbAddress.Controls.Add(Me.lnkClose)
        Me.gbAddress.Controls.Add(Me.pnlEmergencyContact)
        Me.gbAddress.Controls.Add(Me.pnlPersonalAddress)
        Me.gbAddress.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAddress.ExpandedHoverImage = Nothing
        Me.gbAddress.ExpandedNormalImage = Nothing
        Me.gbAddress.ExpandedPressedImage = Nothing
        Me.gbAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAddress.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAddress.HeaderHeight = 25
        Me.gbAddress.HeaderMessage = ""
        Me.gbAddress.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAddress.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAddress.HeightOnCollapse = 0
        Me.gbAddress.LeftTextSpace = 0
        Me.gbAddress.Location = New System.Drawing.Point(0, 0)
        Me.gbAddress.Name = "gbAddress"
        Me.gbAddress.OpenHeight = 351
        Me.gbAddress.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAddress.ShowBorder = True
        Me.gbAddress.ShowCheckBox = False
        Me.gbAddress.ShowCollapseButton = False
        Me.gbAddress.ShowDefaultBorderColor = True
        Me.gbAddress.ShowDownButton = False
        Me.gbAddress.ShowHeader = True
        Me.gbAddress.Size = New System.Drawing.Size(581, 418)
        Me.gbAddress.TabIndex = 1
        Me.gbAddress.Temp = 0
        Me.gbAddress.Text = "Address Info"
        Me.gbAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkEmergencyContact
        '
        Me.lnkEmergencyContact.BackColor = System.Drawing.Color.Transparent
        Me.lnkEmergencyContact.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEmergencyContact.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkEmergencyContact.Location = New System.Drawing.Point(420, 5)
        Me.lnkEmergencyContact.Name = "lnkEmergencyContact"
        Me.lnkEmergencyContact.Size = New System.Drawing.Size(153, 15)
        Me.lnkEmergencyContact.TabIndex = 221
        Me.lnkEmergencyContact.TabStop = True
        Me.lnkEmergencyContact.Text = "[ Show Emergency Contact ]"
        Me.lnkEmergencyContact.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lnkClose
        '
        Me.lnkClose.BackColor = System.Drawing.Color.Transparent
        Me.lnkClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkClose.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkClose.Location = New System.Drawing.Point(541, 5)
        Me.lnkClose.Name = "lnkClose"
        Me.lnkClose.Size = New System.Drawing.Size(32, 15)
        Me.lnkClose.TabIndex = 225
        Me.lnkClose.TabStop = True
        Me.lnkClose.Text = "[ X ]"
        Me.lnkClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlEmergencyContact
        '
        Me.pnlEmergencyContact.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEmergencyContact.AutoScroll = True
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmg3City)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmg3ZipCode)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmg3State)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmg3Country)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmg2City)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmg2ZipCode)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmg2State)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmg2Country)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmg1City)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmg1ZipCode)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmg1State)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmg1Country)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgEmail3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgEmail3)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgFax3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgFax3)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgFirstName3)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgAltNo3)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgTelNo3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgTelNo3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgAltNo3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgFirstName3)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgLastName3)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgPlotNo3)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgMobile3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgMobile3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgLastName3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgPlotNo3)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgAddress3)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgProvince3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgProvince3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgAddress3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgPostCountry3)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgEstate3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgEstate3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgPostTown3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgState3)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgRoad3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgPostCode3)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgRoad3)
        Me.pnlEmergencyContact.Controls.Add(Me.lnEmergencyContact3)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgEmail2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgEmail2)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgFax2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgFax2)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgFirstName2)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgAltNo2)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgTelNo2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgTelNo2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgAltNo2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgFirstName2)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgLastName2)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgPlotNo2)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgMobile2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgMobile2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgLastName2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgPlotNo2)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgAddress2)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgProvince2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgProvince2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgAddress2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgPostCountry2)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgEstate2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgEstate2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgPostTown2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgState2)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgRoad2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgPostCode2)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgRoad2)
        Me.pnlEmergencyContact.Controls.Add(Me.lnEmergencyContact2)
        Me.pnlEmergencyContact.Controls.Add(Me.lnEmergencyContact1)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgEmail)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgEmail)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgFax)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgFax)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgFirstName)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgAltNo)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgTelNo)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgTelNo)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgAltNo)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgFirstName)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgLastName)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgPlotNo)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgMobile)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgMobile)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgLastName)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgPlotNo)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgAddress)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgProvince)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgProvince)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgAddress)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgPostCountry)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgEstate)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgEstate)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgPostTown)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgState)
        Me.pnlEmergencyContact.Controls.Add(Me.txtEmgRoad)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgPostCode)
        Me.pnlEmergencyContact.Controls.Add(Me.lblEmgRoad)
        Me.pnlEmergencyContact.Location = New System.Drawing.Point(16, 26)
        Me.pnlEmergencyContact.Name = "pnlEmergencyContact"
        Me.pnlEmergencyContact.Size = New System.Drawing.Size(557, 380)
        Me.pnlEmergencyContact.TabIndex = 223
        '
        'txtEmg3City
        '
        Me.txtEmg3City.BackColor = System.Drawing.Color.White
        Me.txtEmg3City.Flags = 0
        Me.txtEmg3City.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmg3City.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmg3City.Location = New System.Drawing.Point(97, 574)
        Me.txtEmg3City.Name = "txtEmg3City"
        Me.txtEmg3City.ReadOnly = True
        Me.txtEmg3City.Size = New System.Drawing.Size(187, 21)
        Me.txtEmg3City.TabIndex = 193
        '
        'txtEmg3ZipCode
        '
        Me.txtEmg3ZipCode.BackColor = System.Drawing.Color.White
        Me.txtEmg3ZipCode.Flags = 0
        Me.txtEmg3ZipCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmg3ZipCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmg3ZipCode.Location = New System.Drawing.Point(367, 574)
        Me.txtEmg3ZipCode.Name = "txtEmg3ZipCode"
        Me.txtEmg3ZipCode.ReadOnly = True
        Me.txtEmg3ZipCode.Size = New System.Drawing.Size(187, 21)
        Me.txtEmg3ZipCode.TabIndex = 192
        '
        'txtEmg3State
        '
        Me.txtEmg3State.BackColor = System.Drawing.Color.White
        Me.txtEmg3State.Flags = 0
        Me.txtEmg3State.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmg3State.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmg3State.Location = New System.Drawing.Point(367, 547)
        Me.txtEmg3State.Name = "txtEmg3State"
        Me.txtEmg3State.ReadOnly = True
        Me.txtEmg3State.Size = New System.Drawing.Size(187, 21)
        Me.txtEmg3State.TabIndex = 191
        '
        'txtEmg3Country
        '
        Me.txtEmg3Country.BackColor = System.Drawing.Color.White
        Me.txtEmg3Country.Flags = 0
        Me.txtEmg3Country.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmg3Country.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmg3Country.Location = New System.Drawing.Point(97, 547)
        Me.txtEmg3Country.Name = "txtEmg3Country"
        Me.txtEmg3Country.ReadOnly = True
        Me.txtEmg3Country.Size = New System.Drawing.Size(187, 21)
        Me.txtEmg3Country.TabIndex = 190
        '
        'txtEmg2City
        '
        Me.txtEmg2City.BackColor = System.Drawing.Color.White
        Me.txtEmg2City.Flags = 0
        Me.txtEmg2City.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmg2City.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmg2City.Location = New System.Drawing.Point(97, 341)
        Me.txtEmg2City.Name = "txtEmg2City"
        Me.txtEmg2City.ReadOnly = True
        Me.txtEmg2City.Size = New System.Drawing.Size(187, 21)
        Me.txtEmg2City.TabIndex = 189
        '
        'txtEmg2ZipCode
        '
        Me.txtEmg2ZipCode.BackColor = System.Drawing.Color.White
        Me.txtEmg2ZipCode.Flags = 0
        Me.txtEmg2ZipCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmg2ZipCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmg2ZipCode.Location = New System.Drawing.Point(367, 341)
        Me.txtEmg2ZipCode.Name = "txtEmg2ZipCode"
        Me.txtEmg2ZipCode.ReadOnly = True
        Me.txtEmg2ZipCode.Size = New System.Drawing.Size(187, 21)
        Me.txtEmg2ZipCode.TabIndex = 188
        '
        'txtEmg2State
        '
        Me.txtEmg2State.BackColor = System.Drawing.Color.White
        Me.txtEmg2State.Flags = 0
        Me.txtEmg2State.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmg2State.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmg2State.Location = New System.Drawing.Point(367, 314)
        Me.txtEmg2State.Name = "txtEmg2State"
        Me.txtEmg2State.ReadOnly = True
        Me.txtEmg2State.Size = New System.Drawing.Size(187, 21)
        Me.txtEmg2State.TabIndex = 187
        '
        'txtEmg2Country
        '
        Me.txtEmg2Country.BackColor = System.Drawing.Color.White
        Me.txtEmg2Country.Flags = 0
        Me.txtEmg2Country.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmg2Country.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmg2Country.Location = New System.Drawing.Point(97, 314)
        Me.txtEmg2Country.Name = "txtEmg2Country"
        Me.txtEmg2Country.ReadOnly = True
        Me.txtEmg2Country.Size = New System.Drawing.Size(187, 21)
        Me.txtEmg2Country.TabIndex = 186
        '
        'txtEmg1City
        '
        Me.txtEmg1City.BackColor = System.Drawing.Color.White
        Me.txtEmg1City.Flags = 0
        Me.txtEmg1City.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmg1City.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmg1City.Location = New System.Drawing.Point(97, 105)
        Me.txtEmg1City.Name = "txtEmg1City"
        Me.txtEmg1City.ReadOnly = True
        Me.txtEmg1City.Size = New System.Drawing.Size(187, 21)
        Me.txtEmg1City.TabIndex = 185
        '
        'txtEmg1ZipCode
        '
        Me.txtEmg1ZipCode.BackColor = System.Drawing.Color.White
        Me.txtEmg1ZipCode.Flags = 0
        Me.txtEmg1ZipCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmg1ZipCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmg1ZipCode.Location = New System.Drawing.Point(367, 105)
        Me.txtEmg1ZipCode.Name = "txtEmg1ZipCode"
        Me.txtEmg1ZipCode.ReadOnly = True
        Me.txtEmg1ZipCode.Size = New System.Drawing.Size(187, 21)
        Me.txtEmg1ZipCode.TabIndex = 184
        '
        'txtEmg1State
        '
        Me.txtEmg1State.BackColor = System.Drawing.Color.White
        Me.txtEmg1State.Flags = 0
        Me.txtEmg1State.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmg1State.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmg1State.Location = New System.Drawing.Point(367, 78)
        Me.txtEmg1State.Name = "txtEmg1State"
        Me.txtEmg1State.ReadOnly = True
        Me.txtEmg1State.Size = New System.Drawing.Size(187, 21)
        Me.txtEmg1State.TabIndex = 183
        '
        'txtEmg1Country
        '
        Me.txtEmg1Country.BackColor = System.Drawing.Color.White
        Me.txtEmg1Country.Flags = 0
        Me.txtEmg1Country.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmg1Country.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmg1Country.Location = New System.Drawing.Point(97, 78)
        Me.txtEmg1Country.Name = "txtEmg1Country"
        Me.txtEmg1Country.ReadOnly = True
        Me.txtEmg1Country.Size = New System.Drawing.Size(187, 21)
        Me.txtEmg1Country.TabIndex = 182
        '
        'txtEmgEmail3
        '
        Me.txtEmgEmail3.BackColor = System.Drawing.Color.White
        Me.txtEmgEmail3.Flags = 0
        Me.txtEmgEmail3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgEmail3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgEmail3.Location = New System.Drawing.Point(367, 682)
        Me.txtEmgEmail3.Name = "txtEmgEmail3"
        Me.txtEmgEmail3.ReadOnly = True
        Me.txtEmgEmail3.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgEmail3.TabIndex = 181
        '
        'lblEmgEmail3
        '
        Me.lblEmgEmail3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgEmail3.Location = New System.Drawing.Point(288, 685)
        Me.lblEmgEmail3.Name = "lblEmgEmail3"
        Me.lblEmgEmail3.Size = New System.Drawing.Size(76, 15)
        Me.lblEmgEmail3.TabIndex = 180
        Me.lblEmgEmail3.Text = "Email"
        Me.lblEmgEmail3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgFax3
        '
        Me.txtEmgFax3.BackColor = System.Drawing.Color.White
        Me.txtEmgFax3.Flags = 0
        Me.txtEmgFax3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgFax3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgFax3.Location = New System.Drawing.Point(482, 628)
        Me.txtEmgFax3.Name = "txtEmgFax3"
        Me.txtEmgFax3.ReadOnly = True
        Me.txtEmgFax3.Size = New System.Drawing.Size(72, 21)
        Me.txtEmgFax3.TabIndex = 179
        '
        'lblEmgFax3
        '
        Me.lblEmgFax3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgFax3.Location = New System.Drawing.Point(445, 631)
        Me.lblEmgFax3.Name = "lblEmgFax3"
        Me.lblEmgFax3.Size = New System.Drawing.Size(31, 15)
        Me.lblEmgFax3.TabIndex = 178
        Me.lblEmgFax3.Text = "Fax"
        Me.lblEmgFax3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgFirstName3
        '
        Me.txtEmgFirstName3.BackColor = System.Drawing.Color.White
        Me.txtEmgFirstName3.Flags = 0
        Me.txtEmgFirstName3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgFirstName3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgFirstName3.Location = New System.Drawing.Point(97, 493)
        Me.txtEmgFirstName3.Name = "txtEmgFirstName3"
        Me.txtEmgFirstName3.ReadOnly = True
        Me.txtEmgFirstName3.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgFirstName3.TabIndex = 151
        '
        'txtEmgAltNo3
        '
        Me.txtEmgAltNo3.BackColor = System.Drawing.Color.White
        Me.txtEmgAltNo3.Flags = 0
        Me.txtEmgAltNo3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgAltNo3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgAltNo3.Location = New System.Drawing.Point(97, 628)
        Me.txtEmgAltNo3.Name = "txtEmgAltNo3"
        Me.txtEmgAltNo3.ReadOnly = True
        Me.txtEmgAltNo3.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgAltNo3.TabIndex = 175
        '
        'txtEmgTelNo3
        '
        Me.txtEmgTelNo3.BackColor = System.Drawing.Color.White
        Me.txtEmgTelNo3.Flags = 0
        Me.txtEmgTelNo3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgTelNo3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgTelNo3.Location = New System.Drawing.Point(367, 628)
        Me.txtEmgTelNo3.Name = "txtEmgTelNo3"
        Me.txtEmgTelNo3.ReadOnly = True
        Me.txtEmgTelNo3.Size = New System.Drawing.Size(72, 21)
        Me.txtEmgTelNo3.TabIndex = 177
        '
        'lblEmgTelNo3
        '
        Me.lblEmgTelNo3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgTelNo3.Location = New System.Drawing.Point(288, 631)
        Me.lblEmgTelNo3.Name = "lblEmgTelNo3"
        Me.lblEmgTelNo3.Size = New System.Drawing.Size(76, 15)
        Me.lblEmgTelNo3.TabIndex = 176
        Me.lblEmgTelNo3.Text = "Tel. No"
        Me.lblEmgTelNo3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgAltNo3
        '
        Me.lblEmgAltNo3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgAltNo3.Location = New System.Drawing.Point(11, 631)
        Me.lblEmgAltNo3.Name = "lblEmgAltNo3"
        Me.lblEmgAltNo3.Size = New System.Drawing.Size(83, 15)
        Me.lblEmgAltNo3.TabIndex = 174
        Me.lblEmgAltNo3.Text = "Alt. No"
        Me.lblEmgAltNo3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgFirstName3
        '
        Me.lblEmgFirstName3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgFirstName3.Location = New System.Drawing.Point(11, 496)
        Me.lblEmgFirstName3.Name = "lblEmgFirstName3"
        Me.lblEmgFirstName3.Size = New System.Drawing.Size(83, 15)
        Me.lblEmgFirstName3.TabIndex = 150
        Me.lblEmgFirstName3.Text = "First Name"
        Me.lblEmgFirstName3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgLastName3
        '
        Me.txtEmgLastName3.BackColor = System.Drawing.Color.White
        Me.txtEmgLastName3.Flags = 0
        Me.txtEmgLastName3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgLastName3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgLastName3.Location = New System.Drawing.Point(367, 493)
        Me.txtEmgLastName3.Name = "txtEmgLastName3"
        Me.txtEmgLastName3.ReadOnly = True
        Me.txtEmgLastName3.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgLastName3.TabIndex = 153
        '
        'txtEmgPlotNo3
        '
        Me.txtEmgPlotNo3.BackColor = System.Drawing.Color.White
        Me.txtEmgPlotNo3.Flags = 0
        Me.txtEmgPlotNo3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgPlotNo3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgPlotNo3.Location = New System.Drawing.Point(97, 655)
        Me.txtEmgPlotNo3.Name = "txtEmgPlotNo3"
        Me.txtEmgPlotNo3.ReadOnly = True
        Me.txtEmgPlotNo3.Size = New System.Drawing.Size(186, 21)
        Me.txtEmgPlotNo3.TabIndex = 171
        '
        'txtEmgMobile3
        '
        Me.txtEmgMobile3.BackColor = System.Drawing.Color.White
        Me.txtEmgMobile3.Flags = 0
        Me.txtEmgMobile3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgMobile3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgMobile3.Location = New System.Drawing.Point(97, 682)
        Me.txtEmgMobile3.Name = "txtEmgMobile3"
        Me.txtEmgMobile3.ReadOnly = True
        Me.txtEmgMobile3.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgMobile3.TabIndex = 173
        '
        'lblEmgMobile3
        '
        Me.lblEmgMobile3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgMobile3.Location = New System.Drawing.Point(11, 685)
        Me.lblEmgMobile3.Name = "lblEmgMobile3"
        Me.lblEmgMobile3.Size = New System.Drawing.Size(83, 15)
        Me.lblEmgMobile3.TabIndex = 172
        Me.lblEmgMobile3.Text = "Mobile"
        Me.lblEmgMobile3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgLastName3
        '
        Me.lblEmgLastName3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgLastName3.Location = New System.Drawing.Point(288, 496)
        Me.lblEmgLastName3.Name = "lblEmgLastName3"
        Me.lblEmgLastName3.Size = New System.Drawing.Size(76, 15)
        Me.lblEmgLastName3.TabIndex = 152
        Me.lblEmgLastName3.Text = "Last Name"
        Me.lblEmgLastName3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgPlotNo3
        '
        Me.lblEmgPlotNo3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPlotNo3.Location = New System.Drawing.Point(11, 658)
        Me.lblEmgPlotNo3.Name = "lblEmgPlotNo3"
        Me.lblEmgPlotNo3.Size = New System.Drawing.Size(83, 15)
        Me.lblEmgPlotNo3.TabIndex = 170
        Me.lblEmgPlotNo3.Text = "Plot No"
        Me.lblEmgPlotNo3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgAddress3
        '
        Me.txtEmgAddress3.BackColor = System.Drawing.Color.White
        Me.txtEmgAddress3.Flags = 0
        Me.txtEmgAddress3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgAddress3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgAddress3.Location = New System.Drawing.Point(97, 520)
        Me.txtEmgAddress3.Name = "txtEmgAddress3"
        Me.txtEmgAddress3.ReadOnly = True
        Me.txtEmgAddress3.Size = New System.Drawing.Size(457, 21)
        Me.txtEmgAddress3.TabIndex = 155
        '
        'txtEmgProvince3
        '
        Me.txtEmgProvince3.BackColor = System.Drawing.Color.White
        Me.txtEmgProvince3.Flags = 0
        Me.txtEmgProvince3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgProvince3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgProvince3.Location = New System.Drawing.Point(367, 601)
        Me.txtEmgProvince3.Name = "txtEmgProvince3"
        Me.txtEmgProvince3.ReadOnly = True
        Me.txtEmgProvince3.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgProvince3.TabIndex = 165
        '
        'lblEmgProvince3
        '
        Me.lblEmgProvince3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgProvince3.Location = New System.Drawing.Point(288, 604)
        Me.lblEmgProvince3.Name = "lblEmgProvince3"
        Me.lblEmgProvince3.Size = New System.Drawing.Size(76, 15)
        Me.lblEmgProvince3.TabIndex = 164
        Me.lblEmgProvince3.Text = "Prov/Region"
        Me.lblEmgProvince3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgAddress3
        '
        Me.lblEmgAddress3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgAddress3.Location = New System.Drawing.Point(11, 523)
        Me.lblEmgAddress3.Name = "lblEmgAddress3"
        Me.lblEmgAddress3.Size = New System.Drawing.Size(83, 15)
        Me.lblEmgAddress3.TabIndex = 154
        Me.lblEmgAddress3.Text = "Address"
        Me.lblEmgAddress3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgPostCountry3
        '
        Me.lblEmgPostCountry3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPostCountry3.Location = New System.Drawing.Point(11, 550)
        Me.lblEmgPostCountry3.Name = "lblEmgPostCountry3"
        Me.lblEmgPostCountry3.Size = New System.Drawing.Size(83, 15)
        Me.lblEmgPostCountry3.TabIndex = 156
        Me.lblEmgPostCountry3.Text = "Post Country"
        Me.lblEmgPostCountry3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgEstate3
        '
        Me.txtEmgEstate3.BackColor = System.Drawing.Color.White
        Me.txtEmgEstate3.Flags = 0
        Me.txtEmgEstate3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgEstate3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgEstate3.Location = New System.Drawing.Point(97, 601)
        Me.txtEmgEstate3.Name = "txtEmgEstate3"
        Me.txtEmgEstate3.ReadOnly = True
        Me.txtEmgEstate3.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgEstate3.TabIndex = 169
        '
        'lblEmgEstate3
        '
        Me.lblEmgEstate3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgEstate3.Location = New System.Drawing.Point(11, 604)
        Me.lblEmgEstate3.Name = "lblEmgEstate3"
        Me.lblEmgEstate3.Size = New System.Drawing.Size(83, 15)
        Me.lblEmgEstate3.TabIndex = 168
        Me.lblEmgEstate3.Text = "Estate"
        Me.lblEmgEstate3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgPostTown3
        '
        Me.lblEmgPostTown3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPostTown3.Location = New System.Drawing.Point(11, 577)
        Me.lblEmgPostTown3.Name = "lblEmgPostTown3"
        Me.lblEmgPostTown3.Size = New System.Drawing.Size(83, 15)
        Me.lblEmgPostTown3.TabIndex = 160
        Me.lblEmgPostTown3.Text = "Post Town"
        Me.lblEmgPostTown3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgState3
        '
        Me.lblEmgState3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgState3.Location = New System.Drawing.Point(288, 550)
        Me.lblEmgState3.Name = "lblEmgState3"
        Me.lblEmgState3.Size = New System.Drawing.Size(76, 15)
        Me.lblEmgState3.TabIndex = 158
        Me.lblEmgState3.Text = "State"
        Me.lblEmgState3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgRoad3
        '
        Me.txtEmgRoad3.BackColor = System.Drawing.Color.White
        Me.txtEmgRoad3.Flags = 0
        Me.txtEmgRoad3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgRoad3.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgRoad3.Location = New System.Drawing.Point(367, 655)
        Me.txtEmgRoad3.Name = "txtEmgRoad3"
        Me.txtEmgRoad3.ReadOnly = True
        Me.txtEmgRoad3.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgRoad3.TabIndex = 167
        '
        'lblEmgPostCode3
        '
        Me.lblEmgPostCode3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPostCode3.Location = New System.Drawing.Point(288, 577)
        Me.lblEmgPostCode3.Name = "lblEmgPostCode3"
        Me.lblEmgPostCode3.Size = New System.Drawing.Size(76, 15)
        Me.lblEmgPostCode3.TabIndex = 162
        Me.lblEmgPostCode3.Text = "Post Code"
        Me.lblEmgPostCode3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgRoad3
        '
        Me.lblEmgRoad3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgRoad3.Location = New System.Drawing.Point(288, 658)
        Me.lblEmgRoad3.Name = "lblEmgRoad3"
        Me.lblEmgRoad3.Size = New System.Drawing.Size(76, 15)
        Me.lblEmgRoad3.TabIndex = 166
        Me.lblEmgRoad3.Text = "Road/Street"
        Me.lblEmgRoad3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnEmergencyContact3
        '
        Me.lnEmergencyContact3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmergencyContact3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmergencyContact3.Location = New System.Drawing.Point(3, 473)
        Me.lnEmergencyContact3.Name = "lnEmergencyContact3"
        Me.lnEmergencyContact3.Size = New System.Drawing.Size(281, 15)
        Me.lnEmergencyContact3.TabIndex = 149
        Me.lnEmergencyContact3.Text = "Emergency Contact 3 "
        Me.lnEmergencyContact3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgEmail2
        '
        Me.txtEmgEmail2.BackColor = System.Drawing.Color.White
        Me.txtEmgEmail2.Flags = 0
        Me.txtEmgEmail2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgEmail2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgEmail2.Location = New System.Drawing.Point(367, 449)
        Me.txtEmgEmail2.Name = "txtEmgEmail2"
        Me.txtEmgEmail2.ReadOnly = True
        Me.txtEmgEmail2.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgEmail2.TabIndex = 147
        '
        'lblEmgEmail2
        '
        Me.lblEmgEmail2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgEmail2.Location = New System.Drawing.Point(288, 452)
        Me.lblEmgEmail2.Name = "lblEmgEmail2"
        Me.lblEmgEmail2.Size = New System.Drawing.Size(75, 15)
        Me.lblEmgEmail2.TabIndex = 146
        Me.lblEmgEmail2.Text = "Email"
        Me.lblEmgEmail2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgFax2
        '
        Me.txtEmgFax2.BackColor = System.Drawing.Color.White
        Me.txtEmgFax2.Flags = 0
        Me.txtEmgFax2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgFax2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgFax2.Location = New System.Drawing.Point(482, 395)
        Me.txtEmgFax2.Name = "txtEmgFax2"
        Me.txtEmgFax2.ReadOnly = True
        Me.txtEmgFax2.Size = New System.Drawing.Size(72, 21)
        Me.txtEmgFax2.TabIndex = 145
        '
        'lblEmgFax2
        '
        Me.lblEmgFax2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgFax2.Location = New System.Drawing.Point(445, 398)
        Me.lblEmgFax2.Name = "lblEmgFax2"
        Me.lblEmgFax2.Size = New System.Drawing.Size(31, 15)
        Me.lblEmgFax2.TabIndex = 144
        Me.lblEmgFax2.Text = "Fax"
        Me.lblEmgFax2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgFirstName2
        '
        Me.txtEmgFirstName2.BackColor = System.Drawing.Color.White
        Me.txtEmgFirstName2.Flags = 0
        Me.txtEmgFirstName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgFirstName2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgFirstName2.Location = New System.Drawing.Point(97, 260)
        Me.txtEmgFirstName2.Name = "txtEmgFirstName2"
        Me.txtEmgFirstName2.ReadOnly = True
        Me.txtEmgFirstName2.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgFirstName2.TabIndex = 117
        '
        'txtEmgAltNo2
        '
        Me.txtEmgAltNo2.BackColor = System.Drawing.Color.White
        Me.txtEmgAltNo2.Flags = 0
        Me.txtEmgAltNo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgAltNo2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgAltNo2.Location = New System.Drawing.Point(97, 395)
        Me.txtEmgAltNo2.Name = "txtEmgAltNo2"
        Me.txtEmgAltNo2.ReadOnly = True
        Me.txtEmgAltNo2.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgAltNo2.TabIndex = 141
        '
        'txtEmgTelNo2
        '
        Me.txtEmgTelNo2.BackColor = System.Drawing.Color.White
        Me.txtEmgTelNo2.Flags = 0
        Me.txtEmgTelNo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgTelNo2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgTelNo2.Location = New System.Drawing.Point(367, 395)
        Me.txtEmgTelNo2.Name = "txtEmgTelNo2"
        Me.txtEmgTelNo2.ReadOnly = True
        Me.txtEmgTelNo2.Size = New System.Drawing.Size(72, 21)
        Me.txtEmgTelNo2.TabIndex = 143
        '
        'lblEmgTelNo2
        '
        Me.lblEmgTelNo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgTelNo2.Location = New System.Drawing.Point(288, 398)
        Me.lblEmgTelNo2.Name = "lblEmgTelNo2"
        Me.lblEmgTelNo2.Size = New System.Drawing.Size(75, 15)
        Me.lblEmgTelNo2.TabIndex = 142
        Me.lblEmgTelNo2.Text = "Tel. No"
        Me.lblEmgTelNo2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgAltNo2
        '
        Me.lblEmgAltNo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgAltNo2.Location = New System.Drawing.Point(12, 398)
        Me.lblEmgAltNo2.Name = "lblEmgAltNo2"
        Me.lblEmgAltNo2.Size = New System.Drawing.Size(81, 15)
        Me.lblEmgAltNo2.TabIndex = 140
        Me.lblEmgAltNo2.Text = "Alt. No"
        Me.lblEmgAltNo2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgFirstName2
        '
        Me.lblEmgFirstName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgFirstName2.Location = New System.Drawing.Point(12, 263)
        Me.lblEmgFirstName2.Name = "lblEmgFirstName2"
        Me.lblEmgFirstName2.Size = New System.Drawing.Size(81, 15)
        Me.lblEmgFirstName2.TabIndex = 116
        Me.lblEmgFirstName2.Text = "First Name"
        Me.lblEmgFirstName2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgLastName2
        '
        Me.txtEmgLastName2.BackColor = System.Drawing.Color.White
        Me.txtEmgLastName2.Flags = 0
        Me.txtEmgLastName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgLastName2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgLastName2.Location = New System.Drawing.Point(367, 260)
        Me.txtEmgLastName2.Name = "txtEmgLastName2"
        Me.txtEmgLastName2.ReadOnly = True
        Me.txtEmgLastName2.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgLastName2.TabIndex = 119
        '
        'txtEmgPlotNo2
        '
        Me.txtEmgPlotNo2.BackColor = System.Drawing.Color.White
        Me.txtEmgPlotNo2.Flags = 0
        Me.txtEmgPlotNo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgPlotNo2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgPlotNo2.Location = New System.Drawing.Point(97, 422)
        Me.txtEmgPlotNo2.Name = "txtEmgPlotNo2"
        Me.txtEmgPlotNo2.ReadOnly = True
        Me.txtEmgPlotNo2.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgPlotNo2.TabIndex = 137
        '
        'txtEmgMobile2
        '
        Me.txtEmgMobile2.BackColor = System.Drawing.Color.White
        Me.txtEmgMobile2.Flags = 0
        Me.txtEmgMobile2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgMobile2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgMobile2.Location = New System.Drawing.Point(97, 449)
        Me.txtEmgMobile2.Name = "txtEmgMobile2"
        Me.txtEmgMobile2.ReadOnly = True
        Me.txtEmgMobile2.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgMobile2.TabIndex = 139
        '
        'lblEmgMobile2
        '
        Me.lblEmgMobile2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgMobile2.Location = New System.Drawing.Point(12, 452)
        Me.lblEmgMobile2.Name = "lblEmgMobile2"
        Me.lblEmgMobile2.Size = New System.Drawing.Size(81, 15)
        Me.lblEmgMobile2.TabIndex = 138
        Me.lblEmgMobile2.Text = "Mobile"
        Me.lblEmgMobile2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgLastName2
        '
        Me.lblEmgLastName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgLastName2.Location = New System.Drawing.Point(288, 263)
        Me.lblEmgLastName2.Name = "lblEmgLastName2"
        Me.lblEmgLastName2.Size = New System.Drawing.Size(75, 15)
        Me.lblEmgLastName2.TabIndex = 118
        Me.lblEmgLastName2.Text = "Last Name"
        Me.lblEmgLastName2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgPlotNo2
        '
        Me.lblEmgPlotNo2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPlotNo2.Location = New System.Drawing.Point(12, 425)
        Me.lblEmgPlotNo2.Name = "lblEmgPlotNo2"
        Me.lblEmgPlotNo2.Size = New System.Drawing.Size(81, 15)
        Me.lblEmgPlotNo2.TabIndex = 136
        Me.lblEmgPlotNo2.Text = "Plot No"
        Me.lblEmgPlotNo2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgAddress2
        '
        Me.txtEmgAddress2.BackColor = System.Drawing.Color.White
        Me.txtEmgAddress2.Flags = 0
        Me.txtEmgAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgAddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgAddress2.Location = New System.Drawing.Point(97, 287)
        Me.txtEmgAddress2.Name = "txtEmgAddress2"
        Me.txtEmgAddress2.ReadOnly = True
        Me.txtEmgAddress2.Size = New System.Drawing.Size(457, 21)
        Me.txtEmgAddress2.TabIndex = 121
        '
        'txtEmgProvince2
        '
        Me.txtEmgProvince2.BackColor = System.Drawing.Color.White
        Me.txtEmgProvince2.Flags = 0
        Me.txtEmgProvince2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgProvince2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgProvince2.Location = New System.Drawing.Point(367, 368)
        Me.txtEmgProvince2.Name = "txtEmgProvince2"
        Me.txtEmgProvince2.ReadOnly = True
        Me.txtEmgProvince2.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgProvince2.TabIndex = 131
        '
        'lblEmgProvince2
        '
        Me.lblEmgProvince2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgProvince2.Location = New System.Drawing.Point(288, 371)
        Me.lblEmgProvince2.Name = "lblEmgProvince2"
        Me.lblEmgProvince2.Size = New System.Drawing.Size(75, 15)
        Me.lblEmgProvince2.TabIndex = 130
        Me.lblEmgProvince2.Text = "Prov/Region"
        Me.lblEmgProvince2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgAddress2
        '
        Me.lblEmgAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgAddress2.Location = New System.Drawing.Point(12, 290)
        Me.lblEmgAddress2.Name = "lblEmgAddress2"
        Me.lblEmgAddress2.Size = New System.Drawing.Size(80, 15)
        Me.lblEmgAddress2.TabIndex = 120
        Me.lblEmgAddress2.Text = "Address"
        Me.lblEmgAddress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgPostCountry2
        '
        Me.lblEmgPostCountry2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPostCountry2.Location = New System.Drawing.Point(12, 317)
        Me.lblEmgPostCountry2.Name = "lblEmgPostCountry2"
        Me.lblEmgPostCountry2.Size = New System.Drawing.Size(81, 15)
        Me.lblEmgPostCountry2.TabIndex = 122
        Me.lblEmgPostCountry2.Text = "Post Country"
        Me.lblEmgPostCountry2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgEstate2
        '
        Me.txtEmgEstate2.BackColor = System.Drawing.Color.White
        Me.txtEmgEstate2.Flags = 0
        Me.txtEmgEstate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgEstate2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgEstate2.Location = New System.Drawing.Point(97, 368)
        Me.txtEmgEstate2.Name = "txtEmgEstate2"
        Me.txtEmgEstate2.ReadOnly = True
        Me.txtEmgEstate2.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgEstate2.TabIndex = 135
        '
        'lblEmgEstate2
        '
        Me.lblEmgEstate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgEstate2.Location = New System.Drawing.Point(12, 371)
        Me.lblEmgEstate2.Name = "lblEmgEstate2"
        Me.lblEmgEstate2.Size = New System.Drawing.Size(81, 15)
        Me.lblEmgEstate2.TabIndex = 134
        Me.lblEmgEstate2.Text = "Estate"
        Me.lblEmgEstate2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgPostTown2
        '
        Me.lblEmgPostTown2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPostTown2.Location = New System.Drawing.Point(12, 344)
        Me.lblEmgPostTown2.Name = "lblEmgPostTown2"
        Me.lblEmgPostTown2.Size = New System.Drawing.Size(81, 15)
        Me.lblEmgPostTown2.TabIndex = 126
        Me.lblEmgPostTown2.Text = "Post Town"
        Me.lblEmgPostTown2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgState2
        '
        Me.lblEmgState2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgState2.Location = New System.Drawing.Point(288, 317)
        Me.lblEmgState2.Name = "lblEmgState2"
        Me.lblEmgState2.Size = New System.Drawing.Size(75, 15)
        Me.lblEmgState2.TabIndex = 124
        Me.lblEmgState2.Text = "State"
        Me.lblEmgState2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgRoad2
        '
        Me.txtEmgRoad2.BackColor = System.Drawing.Color.White
        Me.txtEmgRoad2.Flags = 0
        Me.txtEmgRoad2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgRoad2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgRoad2.Location = New System.Drawing.Point(367, 422)
        Me.txtEmgRoad2.Name = "txtEmgRoad2"
        Me.txtEmgRoad2.ReadOnly = True
        Me.txtEmgRoad2.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgRoad2.TabIndex = 133
        '
        'lblEmgPostCode2
        '
        Me.lblEmgPostCode2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPostCode2.Location = New System.Drawing.Point(288, 344)
        Me.lblEmgPostCode2.Name = "lblEmgPostCode2"
        Me.lblEmgPostCode2.Size = New System.Drawing.Size(75, 15)
        Me.lblEmgPostCode2.TabIndex = 128
        Me.lblEmgPostCode2.Text = "Post Code"
        Me.lblEmgPostCode2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgRoad2
        '
        Me.lblEmgRoad2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgRoad2.Location = New System.Drawing.Point(288, 425)
        Me.lblEmgRoad2.Name = "lblEmgRoad2"
        Me.lblEmgRoad2.Size = New System.Drawing.Size(75, 15)
        Me.lblEmgRoad2.TabIndex = 132
        Me.lblEmgRoad2.Text = "Road/Street"
        Me.lblEmgRoad2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnEmergencyContact2
        '
        Me.lnEmergencyContact2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmergencyContact2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmergencyContact2.Location = New System.Drawing.Point(3, 239)
        Me.lnEmergencyContact2.Name = "lnEmergencyContact2"
        Me.lnEmergencyContact2.Size = New System.Drawing.Size(281, 15)
        Me.lnEmergencyContact2.TabIndex = 115
        Me.lnEmergencyContact2.Text = "Emergency Contact 2 "
        Me.lnEmergencyContact2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnEmergencyContact1
        '
        Me.lnEmergencyContact1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmergencyContact1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmergencyContact1.Location = New System.Drawing.Point(3, 6)
        Me.lnEmergencyContact1.Name = "lnEmergencyContact1"
        Me.lnEmergencyContact1.Size = New System.Drawing.Size(281, 15)
        Me.lnEmergencyContact1.TabIndex = 79
        Me.lnEmergencyContact1.Text = "Emergency Contact 1 "
        Me.lnEmergencyContact1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgEmail
        '
        Me.txtEmgEmail.BackColor = System.Drawing.Color.White
        Me.txtEmgEmail.Flags = 0
        Me.txtEmgEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgEmail.Location = New System.Drawing.Point(367, 213)
        Me.txtEmgEmail.Name = "txtEmgEmail"
        Me.txtEmgEmail.ReadOnly = True
        Me.txtEmgEmail.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgEmail.TabIndex = 114
        '
        'lblEmgEmail
        '
        Me.lblEmgEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgEmail.Location = New System.Drawing.Point(287, 216)
        Me.lblEmgEmail.Name = "lblEmgEmail"
        Me.lblEmgEmail.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgEmail.TabIndex = 113
        Me.lblEmgEmail.Text = "Email"
        Me.lblEmgEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgFax
        '
        Me.txtEmgFax.BackColor = System.Drawing.Color.White
        Me.txtEmgFax.Flags = 0
        Me.txtEmgFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgFax.Location = New System.Drawing.Point(482, 159)
        Me.txtEmgFax.Name = "txtEmgFax"
        Me.txtEmgFax.ReadOnly = True
        Me.txtEmgFax.Size = New System.Drawing.Size(72, 21)
        Me.txtEmgFax.TabIndex = 112
        '
        'lblEmgFax
        '
        Me.lblEmgFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgFax.Location = New System.Drawing.Point(445, 162)
        Me.lblEmgFax.Name = "lblEmgFax"
        Me.lblEmgFax.Size = New System.Drawing.Size(31, 15)
        Me.lblEmgFax.TabIndex = 111
        Me.lblEmgFax.Text = "Fax"
        Me.lblEmgFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgFirstName
        '
        Me.txtEmgFirstName.BackColor = System.Drawing.Color.White
        Me.txtEmgFirstName.Flags = 0
        Me.txtEmgFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgFirstName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgFirstName.Location = New System.Drawing.Point(97, 24)
        Me.txtEmgFirstName.Name = "txtEmgFirstName"
        Me.txtEmgFirstName.ReadOnly = True
        Me.txtEmgFirstName.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgFirstName.TabIndex = 81
        '
        'txtEmgAltNo
        '
        Me.txtEmgAltNo.BackColor = System.Drawing.Color.White
        Me.txtEmgAltNo.Flags = 0
        Me.txtEmgAltNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgAltNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgAltNo.Location = New System.Drawing.Point(97, 159)
        Me.txtEmgAltNo.Name = "txtEmgAltNo"
        Me.txtEmgAltNo.ReadOnly = True
        Me.txtEmgAltNo.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgAltNo.TabIndex = 108
        '
        'txtEmgTelNo
        '
        Me.txtEmgTelNo.BackColor = System.Drawing.Color.White
        Me.txtEmgTelNo.Flags = 0
        Me.txtEmgTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgTelNo.Location = New System.Drawing.Point(367, 159)
        Me.txtEmgTelNo.Name = "txtEmgTelNo"
        Me.txtEmgTelNo.ReadOnly = True
        Me.txtEmgTelNo.Size = New System.Drawing.Size(72, 21)
        Me.txtEmgTelNo.TabIndex = 110
        '
        'lblEmgTelNo
        '
        Me.lblEmgTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgTelNo.Location = New System.Drawing.Point(287, 162)
        Me.lblEmgTelNo.Name = "lblEmgTelNo"
        Me.lblEmgTelNo.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgTelNo.TabIndex = 109
        Me.lblEmgTelNo.Text = "Tel. No"
        Me.lblEmgTelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgAltNo
        '
        Me.lblEmgAltNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgAltNo.Location = New System.Drawing.Point(12, 162)
        Me.lblEmgAltNo.Name = "lblEmgAltNo"
        Me.lblEmgAltNo.Size = New System.Drawing.Size(82, 15)
        Me.lblEmgAltNo.TabIndex = 107
        Me.lblEmgAltNo.Text = "Alt. No"
        Me.lblEmgAltNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgFirstName
        '
        Me.lblEmgFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgFirstName.Location = New System.Drawing.Point(12, 27)
        Me.lblEmgFirstName.Name = "lblEmgFirstName"
        Me.lblEmgFirstName.Size = New System.Drawing.Size(82, 15)
        Me.lblEmgFirstName.TabIndex = 80
        Me.lblEmgFirstName.Text = "First Name"
        Me.lblEmgFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgLastName
        '
        Me.txtEmgLastName.BackColor = System.Drawing.Color.White
        Me.txtEmgLastName.Flags = 0
        Me.txtEmgLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgLastName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgLastName.Location = New System.Drawing.Point(367, 24)
        Me.txtEmgLastName.Name = "txtEmgLastName"
        Me.txtEmgLastName.ReadOnly = True
        Me.txtEmgLastName.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgLastName.TabIndex = 83
        '
        'txtEmgPlotNo
        '
        Me.txtEmgPlotNo.BackColor = System.Drawing.Color.White
        Me.txtEmgPlotNo.Flags = 0
        Me.txtEmgPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgPlotNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgPlotNo.Location = New System.Drawing.Point(97, 186)
        Me.txtEmgPlotNo.Name = "txtEmgPlotNo"
        Me.txtEmgPlotNo.ReadOnly = True
        Me.txtEmgPlotNo.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgPlotNo.TabIndex = 104
        '
        'txtEmgMobile
        '
        Me.txtEmgMobile.BackColor = System.Drawing.Color.White
        Me.txtEmgMobile.Flags = 0
        Me.txtEmgMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgMobile.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgMobile.Location = New System.Drawing.Point(97, 213)
        Me.txtEmgMobile.Name = "txtEmgMobile"
        Me.txtEmgMobile.ReadOnly = True
        Me.txtEmgMobile.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgMobile.TabIndex = 106
        '
        'lblEmgMobile
        '
        Me.lblEmgMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgMobile.Location = New System.Drawing.Point(12, 216)
        Me.lblEmgMobile.Name = "lblEmgMobile"
        Me.lblEmgMobile.Size = New System.Drawing.Size(82, 15)
        Me.lblEmgMobile.TabIndex = 105
        Me.lblEmgMobile.Text = "Mobile"
        Me.lblEmgMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgLastName
        '
        Me.lblEmgLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgLastName.Location = New System.Drawing.Point(287, 27)
        Me.lblEmgLastName.Name = "lblEmgLastName"
        Me.lblEmgLastName.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgLastName.TabIndex = 82
        Me.lblEmgLastName.Text = "Last Name"
        Me.lblEmgLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgPlotNo
        '
        Me.lblEmgPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPlotNo.Location = New System.Drawing.Point(12, 189)
        Me.lblEmgPlotNo.Name = "lblEmgPlotNo"
        Me.lblEmgPlotNo.Size = New System.Drawing.Size(82, 15)
        Me.lblEmgPlotNo.TabIndex = 103
        Me.lblEmgPlotNo.Text = "Plot No"
        Me.lblEmgPlotNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgAddress
        '
        Me.txtEmgAddress.BackColor = System.Drawing.Color.White
        Me.txtEmgAddress.Flags = 0
        Me.txtEmgAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgAddress.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgAddress.Location = New System.Drawing.Point(97, 51)
        Me.txtEmgAddress.Name = "txtEmgAddress"
        Me.txtEmgAddress.ReadOnly = True
        Me.txtEmgAddress.Size = New System.Drawing.Size(457, 21)
        Me.txtEmgAddress.TabIndex = 85
        '
        'txtEmgProvince
        '
        Me.txtEmgProvince.BackColor = System.Drawing.Color.White
        Me.txtEmgProvince.Flags = 0
        Me.txtEmgProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgProvince.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgProvince.Location = New System.Drawing.Point(367, 131)
        Me.txtEmgProvince.Name = "txtEmgProvince"
        Me.txtEmgProvince.ReadOnly = True
        Me.txtEmgProvince.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgProvince.TabIndex = 98
        '
        'lblEmgProvince
        '
        Me.lblEmgProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgProvince.Location = New System.Drawing.Point(287, 134)
        Me.lblEmgProvince.Name = "lblEmgProvince"
        Me.lblEmgProvince.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgProvince.TabIndex = 97
        Me.lblEmgProvince.Text = "Prov/Region"
        Me.lblEmgProvince.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgAddress
        '
        Me.lblEmgAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgAddress.Location = New System.Drawing.Point(12, 54)
        Me.lblEmgAddress.Name = "lblEmgAddress"
        Me.lblEmgAddress.Size = New System.Drawing.Size(82, 15)
        Me.lblEmgAddress.TabIndex = 84
        Me.lblEmgAddress.Text = "Address"
        Me.lblEmgAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgPostCountry
        '
        Me.lblEmgPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPostCountry.Location = New System.Drawing.Point(12, 81)
        Me.lblEmgPostCountry.Name = "lblEmgPostCountry"
        Me.lblEmgPostCountry.Size = New System.Drawing.Size(82, 15)
        Me.lblEmgPostCountry.TabIndex = 86
        Me.lblEmgPostCountry.Text = "Post Country"
        Me.lblEmgPostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgEstate
        '
        Me.txtEmgEstate.BackColor = System.Drawing.Color.White
        Me.txtEmgEstate.Flags = 0
        Me.txtEmgEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgEstate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgEstate.Location = New System.Drawing.Point(97, 132)
        Me.txtEmgEstate.Name = "txtEmgEstate"
        Me.txtEmgEstate.ReadOnly = True
        Me.txtEmgEstate.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgEstate.TabIndex = 102
        '
        'lblEmgEstate
        '
        Me.lblEmgEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgEstate.Location = New System.Drawing.Point(12, 135)
        Me.lblEmgEstate.Name = "lblEmgEstate"
        Me.lblEmgEstate.Size = New System.Drawing.Size(82, 15)
        Me.lblEmgEstate.TabIndex = 101
        Me.lblEmgEstate.Text = "Estate"
        Me.lblEmgEstate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgPostTown
        '
        Me.lblEmgPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPostTown.Location = New System.Drawing.Point(12, 108)
        Me.lblEmgPostTown.Name = "lblEmgPostTown"
        Me.lblEmgPostTown.Size = New System.Drawing.Size(82, 15)
        Me.lblEmgPostTown.TabIndex = 90
        Me.lblEmgPostTown.Text = "Post Town"
        Me.lblEmgPostTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgState
        '
        Me.lblEmgState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgState.Location = New System.Drawing.Point(287, 81)
        Me.lblEmgState.Name = "lblEmgState"
        Me.lblEmgState.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgState.TabIndex = 88
        Me.lblEmgState.Text = "State"
        Me.lblEmgState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmgRoad
        '
        Me.txtEmgRoad.BackColor = System.Drawing.Color.White
        Me.txtEmgRoad.Flags = 0
        Me.txtEmgRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmgRoad.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmgRoad.Location = New System.Drawing.Point(367, 186)
        Me.txtEmgRoad.Name = "txtEmgRoad"
        Me.txtEmgRoad.ReadOnly = True
        Me.txtEmgRoad.Size = New System.Drawing.Size(187, 21)
        Me.txtEmgRoad.TabIndex = 100
        '
        'lblEmgPostCode
        '
        Me.lblEmgPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPostCode.Location = New System.Drawing.Point(287, 108)
        Me.lblEmgPostCode.Name = "lblEmgPostCode"
        Me.lblEmgPostCode.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgPostCode.TabIndex = 92
        Me.lblEmgPostCode.Text = "Post Code"
        Me.lblEmgPostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgRoad
        '
        Me.lblEmgRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgRoad.Location = New System.Drawing.Point(287, 189)
        Me.lblEmgRoad.Name = "lblEmgRoad"
        Me.lblEmgRoad.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgRoad.TabIndex = 99
        Me.lblEmgRoad.Text = "Road/Street"
        Me.lblEmgRoad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlPersonalAddress
        '
        Me.pnlPersonalAddress.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlPersonalAddress.AutoScroll = True
        Me.pnlPersonalAddress.BackColor = System.Drawing.Color.Transparent
        Me.pnlPersonalAddress.Controls.Add(Me.txtDZipCode)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDCountry)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDCity)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDState)
        Me.pnlPersonalAddress.Controls.Add(Me.txtPresentZipCode)
        Me.pnlPersonalAddress.Controls.Add(Me.txtPresentCountry)
        Me.pnlPersonalAddress.Controls.Add(Me.txtPresentCity)
        Me.pnlPersonalAddress.Controls.Add(Me.txtPresentState)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicileState)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicileAddress2)
        Me.pnlPersonalAddress.Controls.Add(Me.lblAddress2)
        Me.pnlPersonalAddress.Controls.Add(Me.lblPresentState)
        Me.pnlPersonalAddress.Controls.Add(Me.lblPostTown)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDomicileAltNo)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicileAltNo)
        Me.pnlPersonalAddress.Controls.Add(Me.txtRoad)
        Me.pnlPersonalAddress.Controls.Add(Me.lblRoad)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDomicilePlotNo)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicilePlotNo)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDomicileProvince)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicileProvince)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicilePostCode)
        Me.pnlPersonalAddress.Controls.Add(Me.txtFax)
        Me.pnlPersonalAddress.Controls.Add(Me.lblFax)
        Me.pnlPersonalAddress.Controls.Add(Me.txtAlternativeNo)
        Me.pnlPersonalAddress.Controls.Add(Me.txtEstate)
        Me.pnlPersonalAddress.Controls.Add(Me.lblEstate)
        Me.pnlPersonalAddress.Controls.Add(Me.txtProvince)
        Me.pnlPersonalAddress.Controls.Add(Me.lblProvince)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDomicileFax)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicileFax)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDomicileMobile)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicileMobileNo)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDomicileTelNo)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicileTelNo)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDomicileEstate)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDomicileRoad)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDomicileEmail)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDomicileAddress2)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicileRoad)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicileEState)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicileEmail)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicilePostTown)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicilePostCountry)
        Me.pnlPersonalAddress.Controls.Add(Me.txtDomicileAddress1)
        Me.pnlPersonalAddress.Controls.Add(Me.lblDomicileAddress1)
        Me.pnlPersonalAddress.Controls.Add(Me.lnDomicileAddress)
        Me.pnlPersonalAddress.Controls.Add(Me.lblAlternativeNo)
        Me.pnlPersonalAddress.Controls.Add(Me.txtMobile)
        Me.pnlPersonalAddress.Controls.Add(Me.txtPlotNo)
        Me.pnlPersonalAddress.Controls.Add(Me.txtEmail)
        Me.pnlPersonalAddress.Controls.Add(Me.txtTelNo)
        Me.pnlPersonalAddress.Controls.Add(Me.txtAddress2)
        Me.pnlPersonalAddress.Controls.Add(Me.lblPloteNo)
        Me.pnlPersonalAddress.Controls.Add(Me.lblMobile)
        Me.pnlPersonalAddress.Controls.Add(Me.lblEmail)
        Me.pnlPersonalAddress.Controls.Add(Me.lblTelNo)
        Me.pnlPersonalAddress.Controls.Add(Me.lblPostCountry)
        Me.pnlPersonalAddress.Controls.Add(Me.lblPostcode)
        Me.pnlPersonalAddress.Controls.Add(Me.txtAddress1)
        Me.pnlPersonalAddress.Controls.Add(Me.lblAddress)
        Me.pnlPersonalAddress.Controls.Add(Me.lnPresentAddress)
        Me.pnlPersonalAddress.Location = New System.Drawing.Point(2, 27)
        Me.pnlPersonalAddress.Name = "pnlPersonalAddress"
        Me.pnlPersonalAddress.Size = New System.Drawing.Size(82, 49)
        Me.pnlPersonalAddress.TabIndex = 219
        '
        'txtDZipCode
        '
        Me.txtDZipCode.BackColor = System.Drawing.Color.White
        Me.txtDZipCode.Flags = 0
        Me.txtDZipCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDZipCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDZipCode.Location = New System.Drawing.Point(370, 318)
        Me.txtDZipCode.Name = "txtDZipCode"
        Me.txtDZipCode.ReadOnly = True
        Me.txtDZipCode.Size = New System.Drawing.Size(169, 21)
        Me.txtDZipCode.TabIndex = 70
        '
        'txtDCountry
        '
        Me.txtDCountry.BackColor = System.Drawing.Color.White
        Me.txtDCountry.Flags = 0
        Me.txtDCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDCountry.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDCountry.Location = New System.Drawing.Point(113, 291)
        Me.txtDCountry.Name = "txtDCountry"
        Me.txtDCountry.ReadOnly = True
        Me.txtDCountry.Size = New System.Drawing.Size(169, 21)
        Me.txtDCountry.TabIndex = 69
        '
        'txtDCity
        '
        Me.txtDCity.BackColor = System.Drawing.Color.White
        Me.txtDCity.Flags = 0
        Me.txtDCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDCity.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDCity.Location = New System.Drawing.Point(113, 318)
        Me.txtDCity.Name = "txtDCity"
        Me.txtDCity.ReadOnly = True
        Me.txtDCity.Size = New System.Drawing.Size(169, 21)
        Me.txtDCity.TabIndex = 68
        '
        'txtDState
        '
        Me.txtDState.BackColor = System.Drawing.Color.White
        Me.txtDState.Flags = 0
        Me.txtDState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDState.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDState.Location = New System.Drawing.Point(370, 291)
        Me.txtDState.Name = "txtDState"
        Me.txtDState.ReadOnly = True
        Me.txtDState.Size = New System.Drawing.Size(169, 21)
        Me.txtDState.TabIndex = 67
        '
        'txtPresentZipCode
        '
        Me.txtPresentZipCode.BackColor = System.Drawing.Color.White
        Me.txtPresentZipCode.Flags = 0
        Me.txtPresentZipCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresentZipCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPresentZipCode.Location = New System.Drawing.Point(370, 80)
        Me.txtPresentZipCode.Name = "txtPresentZipCode"
        Me.txtPresentZipCode.ReadOnly = True
        Me.txtPresentZipCode.Size = New System.Drawing.Size(169, 21)
        Me.txtPresentZipCode.TabIndex = 66
        '
        'txtPresentCountry
        '
        Me.txtPresentCountry.BackColor = System.Drawing.Color.White
        Me.txtPresentCountry.Flags = 0
        Me.txtPresentCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresentCountry.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPresentCountry.Location = New System.Drawing.Point(113, 53)
        Me.txtPresentCountry.Name = "txtPresentCountry"
        Me.txtPresentCountry.ReadOnly = True
        Me.txtPresentCountry.Size = New System.Drawing.Size(169, 21)
        Me.txtPresentCountry.TabIndex = 65
        '
        'txtPresentCity
        '
        Me.txtPresentCity.BackColor = System.Drawing.Color.White
        Me.txtPresentCity.Flags = 0
        Me.txtPresentCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresentCity.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPresentCity.Location = New System.Drawing.Point(113, 80)
        Me.txtPresentCity.Name = "txtPresentCity"
        Me.txtPresentCity.ReadOnly = True
        Me.txtPresentCity.Size = New System.Drawing.Size(169, 21)
        Me.txtPresentCity.TabIndex = 64
        '
        'txtPresentState
        '
        Me.txtPresentState.BackColor = System.Drawing.Color.White
        Me.txtPresentState.Flags = 0
        Me.txtPresentState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresentState.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPresentState.Location = New System.Drawing.Point(370, 53)
        Me.txtPresentState.Name = "txtPresentState"
        Me.txtPresentState.ReadOnly = True
        Me.txtPresentState.Size = New System.Drawing.Size(169, 21)
        Me.txtPresentState.TabIndex = 63
        '
        'lblDomicileState
        '
        Me.lblDomicileState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicileState.Location = New System.Drawing.Point(285, 294)
        Me.lblDomicileState.Name = "lblDomicileState"
        Me.lblDomicileState.Size = New System.Drawing.Size(81, 15)
        Me.lblDomicileState.TabIndex = 39
        Me.lblDomicileState.Text = "State"
        Me.lblDomicileState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDomicileAddress2
        '
        Me.lblDomicileAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicileAddress2.Location = New System.Drawing.Point(285, 267)
        Me.lblDomicileAddress2.Name = "lblDomicileAddress2"
        Me.lblDomicileAddress2.Size = New System.Drawing.Size(81, 15)
        Me.lblDomicileAddress2.TabIndex = 35
        Me.lblDomicileAddress2.Text = "Address2"
        Me.lblDomicileAddress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAddress2
        '
        Me.lblAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress2.Location = New System.Drawing.Point(285, 29)
        Me.lblAddress2.Name = "lblAddress2"
        Me.lblAddress2.Size = New System.Drawing.Size(82, 15)
        Me.lblAddress2.TabIndex = 3
        Me.lblAddress2.Text = "Address2"
        Me.lblAddress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPresentState
        '
        Me.lblPresentState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPresentState.Location = New System.Drawing.Point(285, 56)
        Me.lblPresentState.Name = "lblPresentState"
        Me.lblPresentState.Size = New System.Drawing.Size(82, 15)
        Me.lblPresentState.TabIndex = 7
        Me.lblPresentState.Text = "State"
        Me.lblPresentState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostTown
        '
        Me.lblPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostTown.Location = New System.Drawing.Point(18, 83)
        Me.lblPostTown.Name = "lblPostTown"
        Me.lblPostTown.Size = New System.Drawing.Size(90, 15)
        Me.lblPostTown.TabIndex = 9
        Me.lblPostTown.Text = "Post Town"
        Me.lblPostTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDomicileAltNo
        '
        Me.txtDomicileAltNo.BackColor = System.Drawing.Color.White
        Me.txtDomicileAltNo.Flags = 0
        Me.txtDomicileAltNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicileAltNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicileAltNo.Location = New System.Drawing.Point(370, 426)
        Me.txtDomicileAltNo.Name = "txtDomicileAltNo"
        Me.txtDomicileAltNo.ReadOnly = True
        Me.txtDomicileAltNo.Size = New System.Drawing.Size(169, 21)
        Me.txtDomicileAltNo.TabIndex = 56
        '
        'lblDomicileAltNo
        '
        Me.lblDomicileAltNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicileAltNo.Location = New System.Drawing.Point(285, 429)
        Me.lblDomicileAltNo.Name = "lblDomicileAltNo"
        Me.lblDomicileAltNo.Size = New System.Drawing.Size(81, 15)
        Me.lblDomicileAltNo.TabIndex = 55
        Me.lblDomicileAltNo.Text = "Alternative No"
        Me.lblDomicileAltNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRoad
        '
        Me.txtRoad.BackColor = System.Drawing.Color.White
        Me.txtRoad.Flags = 0
        Me.txtRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRoad.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRoad.Location = New System.Drawing.Point(370, 107)
        Me.txtRoad.Name = "txtRoad"
        Me.txtRoad.ReadOnly = True
        Me.txtRoad.Size = New System.Drawing.Size(169, 21)
        Me.txtRoad.TabIndex = 16
        '
        'lblRoad
        '
        Me.lblRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRoad.Location = New System.Drawing.Point(285, 110)
        Me.lblRoad.Name = "lblRoad"
        Me.lblRoad.Size = New System.Drawing.Size(82, 15)
        Me.lblRoad.TabIndex = 15
        Me.lblRoad.Text = "Road/Street"
        Me.lblRoad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDomicilePlotNo
        '
        Me.txtDomicilePlotNo.BackColor = System.Drawing.Color.White
        Me.txtDomicilePlotNo.Flags = 0
        Me.txtDomicilePlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicilePlotNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicilePlotNo.Location = New System.Drawing.Point(113, 372)
        Me.txtDomicilePlotNo.Name = "txtDomicilePlotNo"
        Me.txtDomicilePlotNo.ReadOnly = True
        Me.txtDomicilePlotNo.Size = New System.Drawing.Size(169, 21)
        Me.txtDomicilePlotNo.TabIndex = 52
        '
        'lblDomicilePlotNo
        '
        Me.lblDomicilePlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicilePlotNo.Location = New System.Drawing.Point(18, 375)
        Me.lblDomicilePlotNo.Name = "lblDomicilePlotNo"
        Me.lblDomicilePlotNo.Size = New System.Drawing.Size(90, 15)
        Me.lblDomicilePlotNo.TabIndex = 51
        Me.lblDomicilePlotNo.Text = "Plot No"
        Me.lblDomicilePlotNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDomicileProvince
        '
        Me.txtDomicileProvince.BackColor = System.Drawing.Color.White
        Me.txtDomicileProvince.Flags = 0
        Me.txtDomicileProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicileProvince.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicileProvince.Location = New System.Drawing.Point(113, 345)
        Me.txtDomicileProvince.Name = "txtDomicileProvince"
        Me.txtDomicileProvince.ReadOnly = True
        Me.txtDomicileProvince.Size = New System.Drawing.Size(169, 21)
        Me.txtDomicileProvince.TabIndex = 46
        '
        'lblDomicileProvince
        '
        Me.lblDomicileProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicileProvince.Location = New System.Drawing.Point(18, 348)
        Me.lblDomicileProvince.Name = "lblDomicileProvince"
        Me.lblDomicileProvince.Size = New System.Drawing.Size(90, 15)
        Me.lblDomicileProvince.TabIndex = 45
        Me.lblDomicileProvince.Text = "Prov/Region"
        Me.lblDomicileProvince.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDomicilePostCode
        '
        Me.lblDomicilePostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicilePostCode.Location = New System.Drawing.Point(285, 321)
        Me.lblDomicilePostCode.Name = "lblDomicilePostCode"
        Me.lblDomicilePostCode.Size = New System.Drawing.Size(81, 15)
        Me.lblDomicilePostCode.TabIndex = 43
        Me.lblDomicilePostCode.Text = "Post Code"
        Me.lblDomicilePostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFax
        '
        Me.txtFax.BackColor = System.Drawing.Color.White
        Me.txtFax.Flags = 0
        Me.txtFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFax.Location = New System.Drawing.Point(370, 161)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.ReadOnly = True
        Me.txtFax.Size = New System.Drawing.Size(169, 21)
        Me.txtFax.TabIndex = 28
        '
        'lblFax
        '
        Me.lblFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFax.Location = New System.Drawing.Point(285, 164)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(82, 15)
        Me.lblFax.TabIndex = 27
        Me.lblFax.Text = "Fax"
        Me.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAlternativeNo
        '
        Me.txtAlternativeNo.BackColor = System.Drawing.Color.White
        Me.txtAlternativeNo.Flags = 0
        Me.txtAlternativeNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAlternativeNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAlternativeNo.Location = New System.Drawing.Point(370, 188)
        Me.txtAlternativeNo.Name = "txtAlternativeNo"
        Me.txtAlternativeNo.ReadOnly = True
        Me.txtAlternativeNo.Size = New System.Drawing.Size(169, 21)
        Me.txtAlternativeNo.TabIndex = 24
        '
        'txtEstate
        '
        Me.txtEstate.BackColor = System.Drawing.Color.White
        Me.txtEstate.Flags = 0
        Me.txtEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEstate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEstate.Location = New System.Drawing.Point(113, 188)
        Me.txtEstate.Name = "txtEstate"
        Me.txtEstate.ReadOnly = True
        Me.txtEstate.Size = New System.Drawing.Size(169, 21)
        Me.txtEstate.TabIndex = 18
        '
        'lblEstate
        '
        Me.lblEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstate.Location = New System.Drawing.Point(18, 191)
        Me.lblEstate.Name = "lblEstate"
        Me.lblEstate.Size = New System.Drawing.Size(90, 15)
        Me.lblEstate.TabIndex = 17
        Me.lblEstate.Text = "Estate"
        Me.lblEstate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtProvince
        '
        Me.txtProvince.BackColor = System.Drawing.Color.White
        Me.txtProvince.Flags = 0
        Me.txtProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProvince.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtProvince.Location = New System.Drawing.Point(113, 107)
        Me.txtProvince.Name = "txtProvince"
        Me.txtProvince.ReadOnly = True
        Me.txtProvince.Size = New System.Drawing.Size(169, 21)
        Me.txtProvince.TabIndex = 14
        '
        'lblProvince
        '
        Me.lblProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProvince.Location = New System.Drawing.Point(18, 110)
        Me.lblProvince.Name = "lblProvince"
        Me.lblProvince.Size = New System.Drawing.Size(90, 15)
        Me.lblProvince.TabIndex = 13
        Me.lblProvince.Text = "Prov/Region"
        Me.lblProvince.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDomicileFax
        '
        Me.txtDomicileFax.BackColor = System.Drawing.Color.White
        Me.txtDomicileFax.Flags = 0
        Me.txtDomicileFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicileFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicileFax.Location = New System.Drawing.Point(370, 399)
        Me.txtDomicileFax.Name = "txtDomicileFax"
        Me.txtDomicileFax.ReadOnly = True
        Me.txtDomicileFax.Size = New System.Drawing.Size(169, 21)
        Me.txtDomicileFax.TabIndex = 60
        '
        'lblDomicileFax
        '
        Me.lblDomicileFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicileFax.Location = New System.Drawing.Point(285, 402)
        Me.lblDomicileFax.Name = "lblDomicileFax"
        Me.lblDomicileFax.Size = New System.Drawing.Size(81, 15)
        Me.lblDomicileFax.TabIndex = 59
        Me.lblDomicileFax.Text = "Fax"
        Me.lblDomicileFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDomicileMobile
        '
        Me.txtDomicileMobile.BackColor = System.Drawing.Color.White
        Me.txtDomicileMobile.Flags = 0
        Me.txtDomicileMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicileMobile.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicileMobile.Location = New System.Drawing.Point(370, 372)
        Me.txtDomicileMobile.Name = "txtDomicileMobile"
        Me.txtDomicileMobile.ReadOnly = True
        Me.txtDomicileMobile.Size = New System.Drawing.Size(169, 21)
        Me.txtDomicileMobile.TabIndex = 54
        '
        'lblDomicileMobileNo
        '
        Me.lblDomicileMobileNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicileMobileNo.Location = New System.Drawing.Point(285, 375)
        Me.lblDomicileMobileNo.Name = "lblDomicileMobileNo"
        Me.lblDomicileMobileNo.Size = New System.Drawing.Size(81, 15)
        Me.lblDomicileMobileNo.TabIndex = 53
        Me.lblDomicileMobileNo.Text = "Mobile"
        Me.lblDomicileMobileNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDomicileTelNo
        '
        Me.txtDomicileTelNo.BackColor = System.Drawing.Color.White
        Me.txtDomicileTelNo.Flags = 0
        Me.txtDomicileTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicileTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicileTelNo.Location = New System.Drawing.Point(113, 399)
        Me.txtDomicileTelNo.Name = "txtDomicileTelNo"
        Me.txtDomicileTelNo.ReadOnly = True
        Me.txtDomicileTelNo.Size = New System.Drawing.Size(169, 21)
        Me.txtDomicileTelNo.TabIndex = 58
        '
        'lblDomicileTelNo
        '
        Me.lblDomicileTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicileTelNo.Location = New System.Drawing.Point(18, 402)
        Me.lblDomicileTelNo.Name = "lblDomicileTelNo"
        Me.lblDomicileTelNo.Size = New System.Drawing.Size(90, 15)
        Me.lblDomicileTelNo.TabIndex = 57
        Me.lblDomicileTelNo.Text = "Tel. No"
        Me.lblDomicileTelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDomicileEstate
        '
        Me.txtDomicileEstate.BackColor = System.Drawing.Color.White
        Me.txtDomicileEstate.Flags = 0
        Me.txtDomicileEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicileEstate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicileEstate.Location = New System.Drawing.Point(113, 426)
        Me.txtDomicileEstate.Name = "txtDomicileEstate"
        Me.txtDomicileEstate.ReadOnly = True
        Me.txtDomicileEstate.Size = New System.Drawing.Size(169, 21)
        Me.txtDomicileEstate.TabIndex = 50
        '
        'txtDomicileRoad
        '
        Me.txtDomicileRoad.BackColor = System.Drawing.Color.White
        Me.txtDomicileRoad.Flags = 0
        Me.txtDomicileRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicileRoad.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicileRoad.Location = New System.Drawing.Point(370, 345)
        Me.txtDomicileRoad.Name = "txtDomicileRoad"
        Me.txtDomicileRoad.ReadOnly = True
        Me.txtDomicileRoad.Size = New System.Drawing.Size(169, 21)
        Me.txtDomicileRoad.TabIndex = 48
        '
        'txtDomicileEmail
        '
        Me.txtDomicileEmail.BackColor = System.Drawing.Color.White
        Me.txtDomicileEmail.Flags = 0
        Me.txtDomicileEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicileEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicileEmail.Location = New System.Drawing.Point(113, 453)
        Me.txtDomicileEmail.Name = "txtDomicileEmail"
        Me.txtDomicileEmail.ReadOnly = True
        Me.txtDomicileEmail.Size = New System.Drawing.Size(426, 21)
        Me.txtDomicileEmail.TabIndex = 62
        '
        'txtDomicileAddress2
        '
        Me.txtDomicileAddress2.BackColor = System.Drawing.Color.White
        Me.txtDomicileAddress2.Flags = 0
        Me.txtDomicileAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicileAddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicileAddress2.Location = New System.Drawing.Point(370, 264)
        Me.txtDomicileAddress2.Name = "txtDomicileAddress2"
        Me.txtDomicileAddress2.ReadOnly = True
        Me.txtDomicileAddress2.Size = New System.Drawing.Size(169, 21)
        Me.txtDomicileAddress2.TabIndex = 36
        '
        'lblDomicileRoad
        '
        Me.lblDomicileRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicileRoad.Location = New System.Drawing.Point(285, 348)
        Me.lblDomicileRoad.Name = "lblDomicileRoad"
        Me.lblDomicileRoad.Size = New System.Drawing.Size(81, 15)
        Me.lblDomicileRoad.TabIndex = 47
        Me.lblDomicileRoad.Text = "Road/Street"
        Me.lblDomicileRoad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDomicileEState
        '
        Me.lblDomicileEState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicileEState.Location = New System.Drawing.Point(18, 429)
        Me.lblDomicileEState.Name = "lblDomicileEState"
        Me.lblDomicileEState.Size = New System.Drawing.Size(90, 15)
        Me.lblDomicileEState.TabIndex = 49
        Me.lblDomicileEState.Text = "Estate"
        Me.lblDomicileEState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDomicileEmail
        '
        Me.lblDomicileEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicileEmail.Location = New System.Drawing.Point(18, 456)
        Me.lblDomicileEmail.Name = "lblDomicileEmail"
        Me.lblDomicileEmail.Size = New System.Drawing.Size(90, 15)
        Me.lblDomicileEmail.TabIndex = 61
        Me.lblDomicileEmail.Text = "Email"
        Me.lblDomicileEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDomicilePostTown
        '
        Me.lblDomicilePostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicilePostTown.Location = New System.Drawing.Point(18, 321)
        Me.lblDomicilePostTown.Name = "lblDomicilePostTown"
        Me.lblDomicilePostTown.Size = New System.Drawing.Size(90, 15)
        Me.lblDomicilePostTown.TabIndex = 41
        Me.lblDomicilePostTown.Text = "Post Town"
        Me.lblDomicilePostTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDomicilePostCountry
        '
        Me.lblDomicilePostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicilePostCountry.Location = New System.Drawing.Point(18, 294)
        Me.lblDomicilePostCountry.Name = "lblDomicilePostCountry"
        Me.lblDomicilePostCountry.Size = New System.Drawing.Size(90, 15)
        Me.lblDomicilePostCountry.TabIndex = 37
        Me.lblDomicilePostCountry.Text = "Post Country"
        Me.lblDomicilePostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDomicileAddress1
        '
        Me.txtDomicileAddress1.BackColor = System.Drawing.Color.White
        Me.txtDomicileAddress1.Flags = 0
        Me.txtDomicileAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDomicileAddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDomicileAddress1.Location = New System.Drawing.Point(113, 264)
        Me.txtDomicileAddress1.Name = "txtDomicileAddress1"
        Me.txtDomicileAddress1.ReadOnly = True
        Me.txtDomicileAddress1.Size = New System.Drawing.Size(169, 21)
        Me.txtDomicileAddress1.TabIndex = 34
        '
        'lblDomicileAddress1
        '
        Me.lblDomicileAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDomicileAddress1.Location = New System.Drawing.Point(18, 267)
        Me.lblDomicileAddress1.Name = "lblDomicileAddress1"
        Me.lblDomicileAddress1.Size = New System.Drawing.Size(90, 15)
        Me.lblDomicileAddress1.TabIndex = 33
        Me.lblDomicileAddress1.Text = "Address1"
        Me.lblDomicileAddress1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnDomicileAddress
        '
        Me.lnDomicileAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnDomicileAddress.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnDomicileAddress.Location = New System.Drawing.Point(11, 244)
        Me.lnDomicileAddress.Name = "lnDomicileAddress"
        Me.lnDomicileAddress.Size = New System.Drawing.Size(271, 14)
        Me.lnDomicileAddress.TabIndex = 31
        Me.lnDomicileAddress.Text = "Domicile/Recruitment Address"
        '
        'lblAlternativeNo
        '
        Me.lblAlternativeNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAlternativeNo.Location = New System.Drawing.Point(285, 191)
        Me.lblAlternativeNo.Name = "lblAlternativeNo"
        Me.lblAlternativeNo.Size = New System.Drawing.Size(82, 15)
        Me.lblAlternativeNo.TabIndex = 23
        Me.lblAlternativeNo.Text = "Alternative No"
        Me.lblAlternativeNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMobile
        '
        Me.txtMobile.BackColor = System.Drawing.Color.White
        Me.txtMobile.Flags = 0
        Me.txtMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobile.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMobile.Location = New System.Drawing.Point(370, 134)
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.ReadOnly = True
        Me.txtMobile.Size = New System.Drawing.Size(169, 21)
        Me.txtMobile.TabIndex = 22
        '
        'txtPlotNo
        '
        Me.txtPlotNo.BackColor = System.Drawing.Color.White
        Me.txtPlotNo.Flags = 0
        Me.txtPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlotNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPlotNo.Location = New System.Drawing.Point(113, 134)
        Me.txtPlotNo.Name = "txtPlotNo"
        Me.txtPlotNo.ReadOnly = True
        Me.txtPlotNo.Size = New System.Drawing.Size(169, 21)
        Me.txtPlotNo.TabIndex = 20
        '
        'txtEmail
        '
        Me.txtEmail.BackColor = System.Drawing.Color.White
        Me.txtEmail.Flags = 0
        Me.txtEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmail.Location = New System.Drawing.Point(113, 215)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.ReadOnly = True
        Me.txtEmail.Size = New System.Drawing.Size(426, 21)
        Me.txtEmail.TabIndex = 30
        '
        'txtTelNo
        '
        Me.txtTelNo.BackColor = System.Drawing.Color.White
        Me.txtTelNo.Flags = 0
        Me.txtTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTelNo.Location = New System.Drawing.Point(113, 161)
        Me.txtTelNo.Name = "txtTelNo"
        Me.txtTelNo.ReadOnly = True
        Me.txtTelNo.Size = New System.Drawing.Size(169, 21)
        Me.txtTelNo.TabIndex = 26
        '
        'txtAddress2
        '
        Me.txtAddress2.BackColor = System.Drawing.Color.White
        Me.txtAddress2.Flags = 0
        Me.txtAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress2.Location = New System.Drawing.Point(370, 26)
        Me.txtAddress2.Name = "txtAddress2"
        Me.txtAddress2.ReadOnly = True
        Me.txtAddress2.Size = New System.Drawing.Size(169, 21)
        Me.txtAddress2.TabIndex = 4
        '
        'lblPloteNo
        '
        Me.lblPloteNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPloteNo.Location = New System.Drawing.Point(18, 137)
        Me.lblPloteNo.Name = "lblPloteNo"
        Me.lblPloteNo.Size = New System.Drawing.Size(90, 15)
        Me.lblPloteNo.TabIndex = 19
        Me.lblPloteNo.Text = "Plot No"
        Me.lblPloteNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMobile
        '
        Me.lblMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMobile.Location = New System.Drawing.Point(285, 137)
        Me.lblMobile.Name = "lblMobile"
        Me.lblMobile.Size = New System.Drawing.Size(82, 15)
        Me.lblMobile.TabIndex = 21
        Me.lblMobile.Text = "Mobile"
        Me.lblMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(18, 218)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(90, 15)
        Me.lblEmail.TabIndex = 29
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTelNo
        '
        Me.lblTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTelNo.Location = New System.Drawing.Point(18, 164)
        Me.lblTelNo.Name = "lblTelNo"
        Me.lblTelNo.Size = New System.Drawing.Size(90, 15)
        Me.lblTelNo.TabIndex = 25
        Me.lblTelNo.Text = "Tel. No"
        Me.lblTelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostCountry
        '
        Me.lblPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostCountry.Location = New System.Drawing.Point(18, 56)
        Me.lblPostCountry.Name = "lblPostCountry"
        Me.lblPostCountry.Size = New System.Drawing.Size(90, 15)
        Me.lblPostCountry.TabIndex = 5
        Me.lblPostCountry.Text = "Post Country"
        Me.lblPostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostcode
        '
        Me.lblPostcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostcode.Location = New System.Drawing.Point(285, 83)
        Me.lblPostcode.Name = "lblPostcode"
        Me.lblPostcode.Size = New System.Drawing.Size(82, 15)
        Me.lblPostcode.TabIndex = 11
        Me.lblPostcode.Text = "Post Code"
        Me.lblPostcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddress1
        '
        Me.txtAddress1.BackColor = System.Drawing.Color.White
        Me.txtAddress1.Flags = 0
        Me.txtAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress1.Location = New System.Drawing.Point(113, 26)
        Me.txtAddress1.Name = "txtAddress1"
        Me.txtAddress1.ReadOnly = True
        Me.txtAddress1.Size = New System.Drawing.Size(169, 21)
        Me.txtAddress1.TabIndex = 2
        '
        'lblAddress
        '
        Me.lblAddress.BackColor = System.Drawing.Color.Transparent
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(18, 29)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(90, 15)
        Me.lblAddress.TabIndex = 1
        Me.lblAddress.Text = "Address1"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnPresentAddress
        '
        Me.lnPresentAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnPresentAddress.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnPresentAddress.Location = New System.Drawing.Point(11, 9)
        Me.lnPresentAddress.Name = "lnPresentAddress"
        Me.lnPresentAddress.Size = New System.Drawing.Size(271, 14)
        Me.lnPresentAddress.TabIndex = 0
        Me.lnPresentAddress.Text = "Present Address"
        '
        'objfrmDiary_Address
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.gbAddress)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "objfrmDiary_Address"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.gbAddress.ResumeLayout(False)
        Me.pnlEmergencyContact.ResumeLayout(False)
        Me.pnlEmergencyContact.PerformLayout()
        Me.pnlPersonalAddress.ResumeLayout(False)
        Me.pnlPersonalAddress.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbAddress As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlPersonalAddress As System.Windows.Forms.Panel
    Friend WithEvents lblDomicileState As System.Windows.Forms.Label
    Friend WithEvents lblDomicileAddress2 As System.Windows.Forms.Label
    Friend WithEvents lblAddress2 As System.Windows.Forms.Label
    Friend WithEvents lblPresentState As System.Windows.Forms.Label
    Friend WithEvents lblPostTown As System.Windows.Forms.Label
    Friend WithEvents txtDomicileAltNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileAltNo As System.Windows.Forms.Label
    Friend WithEvents txtRoad As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRoad As System.Windows.Forms.Label
    Friend WithEvents txtDomicilePlotNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicilePlotNo As System.Windows.Forms.Label
    Friend WithEvents txtDomicileProvince As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileProvince As System.Windows.Forms.Label
    Friend WithEvents lblDomicilePostCode As System.Windows.Forms.Label
    Friend WithEvents txtFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents txtAlternativeNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEstate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEstate As System.Windows.Forms.Label
    Friend WithEvents txtProvince As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblProvince As System.Windows.Forms.Label
    Friend WithEvents txtDomicileFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileFax As System.Windows.Forms.Label
    Friend WithEvents txtDomicileMobile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileMobileNo As System.Windows.Forms.Label
    Friend WithEvents txtDomicileTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileTelNo As System.Windows.Forms.Label
    Friend WithEvents txtDomicileEstate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDomicileRoad As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDomicileEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDomicileAddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileRoad As System.Windows.Forms.Label
    Friend WithEvents lblDomicileEState As System.Windows.Forms.Label
    Friend WithEvents lblDomicileEmail As System.Windows.Forms.Label
    Friend WithEvents lblDomicilePostTown As System.Windows.Forms.Label
    Friend WithEvents lblDomicilePostCountry As System.Windows.Forms.Label
    Friend WithEvents txtDomicileAddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDomicileAddress1 As System.Windows.Forms.Label
    Friend WithEvents lnDomicileAddress As eZee.Common.eZeeLine
    Friend WithEvents lblAlternativeNo As System.Windows.Forms.Label
    Friend WithEvents txtMobile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPlotNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPloteNo As System.Windows.Forms.Label
    Friend WithEvents lblMobile As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents lblTelNo As System.Windows.Forms.Label
    Friend WithEvents lblPostCountry As System.Windows.Forms.Label
    Friend WithEvents lblPostcode As System.Windows.Forms.Label
    Friend WithEvents txtAddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lnPresentAddress As eZee.Common.eZeeLine
    Friend WithEvents txtPresentZipCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPresentCountry As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPresentCity As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPresentState As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDZipCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDCountry As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDCity As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDState As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lnkEmergencyContact As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlEmergencyContact As System.Windows.Forms.Panel
    Friend WithEvents txtEmg1City As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmg1ZipCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmg1State As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmg1Country As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgEmail3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgEmail3 As System.Windows.Forms.Label
    Friend WithEvents txtEmgFax3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgFax3 As System.Windows.Forms.Label
    Friend WithEvents txtEmgFirstName3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgAltNo3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgTelNo3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgTelNo3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgAltNo3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgFirstName3 As System.Windows.Forms.Label
    Friend WithEvents txtEmgLastName3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgPlotNo3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgMobile3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgMobile3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgLastName3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgPlotNo3 As System.Windows.Forms.Label
    Friend WithEvents txtEmgAddress3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgProvince3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgProvince3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgAddress3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgPostCountry3 As System.Windows.Forms.Label
    Friend WithEvents txtEmgEstate3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgEstate3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgPostTown3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgState3 As System.Windows.Forms.Label
    Friend WithEvents txtEmgRoad3 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgPostCode3 As System.Windows.Forms.Label
    Friend WithEvents lblEmgRoad3 As System.Windows.Forms.Label
    Friend WithEvents lnEmergencyContact3 As eZee.Common.eZeeLine
    Friend WithEvents txtEmgEmail2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgEmail2 As System.Windows.Forms.Label
    Friend WithEvents txtEmgFax2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgFax2 As System.Windows.Forms.Label
    Friend WithEvents txtEmgFirstName2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgAltNo2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgTelNo2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgTelNo2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgAltNo2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgFirstName2 As System.Windows.Forms.Label
    Friend WithEvents txtEmgLastName2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgPlotNo2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgMobile2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgMobile2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgLastName2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgPlotNo2 As System.Windows.Forms.Label
    Friend WithEvents txtEmgAddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgProvince2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgProvince2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgAddress2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgPostCountry2 As System.Windows.Forms.Label
    Friend WithEvents txtEmgEstate2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgEstate2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgPostTown2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgState2 As System.Windows.Forms.Label
    Friend WithEvents txtEmgRoad2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgPostCode2 As System.Windows.Forms.Label
    Friend WithEvents lblEmgRoad2 As System.Windows.Forms.Label
    Friend WithEvents lnEmergencyContact2 As eZee.Common.eZeeLine
    Friend WithEvents lnEmergencyContact1 As eZee.Common.eZeeLine
    Friend WithEvents txtEmgEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgEmail As System.Windows.Forms.Label
    Friend WithEvents txtEmgFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgFax As System.Windows.Forms.Label
    Friend WithEvents txtEmgFirstName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgAltNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgTelNo As System.Windows.Forms.Label
    Friend WithEvents lblEmgAltNo As System.Windows.Forms.Label
    Friend WithEvents lblEmgFirstName As System.Windows.Forms.Label
    Friend WithEvents txtEmgLastName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgPlotNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgMobile As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgMobile As System.Windows.Forms.Label
    Friend WithEvents lblEmgLastName As System.Windows.Forms.Label
    Friend WithEvents lblEmgPlotNo As System.Windows.Forms.Label
    Friend WithEvents txtEmgAddress As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmgProvince As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgProvince As System.Windows.Forms.Label
    Friend WithEvents lblEmgAddress As System.Windows.Forms.Label
    Friend WithEvents lblEmgPostCountry As System.Windows.Forms.Label
    Friend WithEvents txtEmgEstate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgEstate As System.Windows.Forms.Label
    Friend WithEvents lblEmgPostTown As System.Windows.Forms.Label
    Friend WithEvents lblEmgState As System.Windows.Forms.Label
    Friend WithEvents txtEmgRoad As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmgPostCode As System.Windows.Forms.Label
    Friend WithEvents lblEmgRoad As System.Windows.Forms.Label
    Friend WithEvents txtEmg2City As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmg2ZipCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmg2State As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmg2Country As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmg3City As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmg3ZipCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmg3State As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmg3Country As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lnkClose As System.Windows.Forms.LinkLabel
End Class

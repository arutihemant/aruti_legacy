﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Dates
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.elEmployeeDates = New eZee.Common.eZeeLine
        Me.lvEmpDates = New eZee.Common.eZeeListView(Me.components)
        Me.colhSrNo = New System.Windows.Forms.ColumnHeader
        Me.colhFromDate = New System.Windows.Forms.ColumnHeader
        Me.colhToDate = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhMode = New System.Windows.Forms.ColumnHeader
        Me.colhPayExcluded = New System.Windows.Forms.ColumnHeader
        Me.objcolhModeId = New System.Windows.Forms.ColumnHeader
        Me.lblYears = New System.Windows.Forms.Label
        Me.txtYears = New eZee.TextBox.AlphanumericTextBox
        Me.txtNxtAnniversary = New eZee.TextBox.AlphanumericTextBox
        Me.lblNxtAnniversary = New System.Windows.Forms.Label
        Me.txtAnniversary = New eZee.TextBox.AlphanumericTextBox
        Me.lblAnniversary = New System.Windows.Forms.Label
        Me.lblAge = New System.Windows.Forms.Label
        Me.txtAge = New eZee.TextBox.AlphanumericTextBox
        Me.txtNxtBirthday = New eZee.TextBox.AlphanumericTextBox
        Me.lblNxtBirthday = New System.Windows.Forms.Label
        Me.txtBirthday = New eZee.TextBox.AlphanumericTextBox
        Me.lblBirthday = New System.Windows.Forms.Label
        Me.elBirthday_Anniversary = New eZee.Common.eZeeLine
        Me.SuspendLayout()
        '
        'elEmployeeDates
        '
        Me.elEmployeeDates.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elEmployeeDates.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elEmployeeDates.Location = New System.Drawing.Point(2, 87)
        Me.elEmployeeDates.Name = "elEmployeeDates"
        Me.elEmployeeDates.Size = New System.Drawing.Size(575, 17)
        Me.elEmployeeDates.TabIndex = 309
        Me.elEmployeeDates.Text = "Employee Dates"
        Me.elEmployeeDates.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvEmpDates
        '
        Me.lvEmpDates.BackColorOnChecked = False
        Me.lvEmpDates.ColumnHeaders = Nothing
        Me.lvEmpDates.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhSrNo, Me.colhFromDate, Me.colhToDate, Me.colhRemark, Me.objcolhMode, Me.colhPayExcluded, Me.objcolhModeId})
        Me.lvEmpDates.CompulsoryColumns = ""
        Me.lvEmpDates.FullRowSelect = True
        Me.lvEmpDates.GridLines = True
        Me.lvEmpDates.GroupingColumn = Nothing
        Me.lvEmpDates.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvEmpDates.HideSelection = False
        Me.lvEmpDates.Location = New System.Drawing.Point(5, 107)
        Me.lvEmpDates.MinColumnWidth = 50
        Me.lvEmpDates.MultiSelect = False
        Me.lvEmpDates.Name = "lvEmpDates"
        Me.lvEmpDates.OptionalColumns = ""
        Me.lvEmpDates.ShowMoreItem = False
        Me.lvEmpDates.ShowSaveItem = False
        Me.lvEmpDates.ShowSelectAll = True
        Me.lvEmpDates.ShowSizeAllColumnsToFit = True
        Me.lvEmpDates.Size = New System.Drawing.Size(572, 299)
        Me.lvEmpDates.Sortable = True
        Me.lvEmpDates.TabIndex = 312
        Me.lvEmpDates.UseCompatibleStateImageBehavior = False
        Me.lvEmpDates.View = System.Windows.Forms.View.Details
        '
        'colhSrNo
        '
        Me.colhSrNo.Tag = "colhSrNo"
        Me.colhSrNo.Text = "Sr."
        Me.colhSrNo.Width = 30
        '
        'colhFromDate
        '
        Me.colhFromDate.Tag = "colhFromDate"
        Me.colhFromDate.Text = "From Date"
        Me.colhFromDate.Width = 85
        '
        'colhToDate
        '
        Me.colhToDate.Tag = "colhToDate"
        Me.colhToDate.Text = "To Date"
        Me.colhToDate.Width = 85
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 250
        '
        'objcolhMode
        '
        Me.objcolhMode.Tag = "objcolhMode"
        Me.objcolhMode.Text = ""
        Me.objcolhMode.Width = 0
        '
        'colhPayExcluded
        '
        Me.colhPayExcluded.Tag = "colhPayExcluded"
        Me.colhPayExcluded.Text = "Payroll Excluded "
        Me.colhPayExcluded.Width = 95
        '
        'objcolhModeId
        '
        Me.objcolhModeId.Tag = "objcolhModeId"
        Me.objcolhModeId.Text = ""
        Me.objcolhModeId.Width = 0
        '
        'lblYears
        '
        Me.lblYears.Location = New System.Drawing.Point(241, 66)
        Me.lblYears.Name = "lblYears"
        Me.lblYears.Size = New System.Drawing.Size(45, 15)
        Me.lblYears.TabIndex = 419
        Me.lblYears.Text = "Years"
        Me.lblYears.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtYears
        '
        Me.txtYears.BackColor = System.Drawing.SystemColors.Window
        Me.txtYears.Flags = 0
        Me.txtYears.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtYears.Location = New System.Drawing.Point(292, 63)
        Me.txtYears.Name = "txtYears"
        Me.txtYears.ReadOnly = True
        Me.txtYears.Size = New System.Drawing.Size(40, 21)
        Me.txtYears.TabIndex = 418
        Me.txtYears.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNxtAnniversary
        '
        Me.txtNxtAnniversary.BackColor = System.Drawing.SystemColors.Window
        Me.txtNxtAnniversary.Flags = 0
        Me.txtNxtAnniversary.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtNxtAnniversary.Location = New System.Drawing.Point(446, 63)
        Me.txtNxtAnniversary.Name = "txtNxtAnniversary"
        Me.txtNxtAnniversary.ReadOnly = True
        Me.txtNxtAnniversary.Size = New System.Drawing.Size(122, 21)
        Me.txtNxtAnniversary.TabIndex = 417
        '
        'lblNxtAnniversary
        '
        Me.lblNxtAnniversary.Location = New System.Drawing.Point(337, 66)
        Me.lblNxtAnniversary.Name = "lblNxtAnniversary"
        Me.lblNxtAnniversary.Size = New System.Drawing.Size(104, 15)
        Me.lblNxtAnniversary.TabIndex = 416
        Me.lblNxtAnniversary.Text = "Next Anniversary"
        Me.lblNxtAnniversary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAnniversary
        '
        Me.txtAnniversary.BackColor = System.Drawing.SystemColors.Window
        Me.txtAnniversary.Flags = 0
        Me.txtAnniversary.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAnniversary.Location = New System.Drawing.Point(104, 63)
        Me.txtAnniversary.Name = "txtAnniversary"
        Me.txtAnniversary.ReadOnly = True
        Me.txtAnniversary.Size = New System.Drawing.Size(132, 21)
        Me.txtAnniversary.TabIndex = 415
        '
        'lblAnniversary
        '
        Me.lblAnniversary.Location = New System.Drawing.Point(14, 66)
        Me.lblAnniversary.Name = "lblAnniversary"
        Me.lblAnniversary.Size = New System.Drawing.Size(84, 15)
        Me.lblAnniversary.TabIndex = 414
        Me.lblAnniversary.Text = "Anniversary"
        Me.lblAnniversary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAge
        '
        Me.lblAge.Location = New System.Drawing.Point(241, 39)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(45, 15)
        Me.lblAge.TabIndex = 413
        Me.lblAge.Text = "Age"
        Me.lblAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAge
        '
        Me.txtAge.BackColor = System.Drawing.SystemColors.Window
        Me.txtAge.Flags = 0
        Me.txtAge.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAge.Location = New System.Drawing.Point(292, 36)
        Me.txtAge.Name = "txtAge"
        Me.txtAge.ReadOnly = True
        Me.txtAge.Size = New System.Drawing.Size(40, 21)
        Me.txtAge.TabIndex = 412
        Me.txtAge.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNxtBirthday
        '
        Me.txtNxtBirthday.BackColor = System.Drawing.SystemColors.Window
        Me.txtNxtBirthday.Flags = 0
        Me.txtNxtBirthday.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtNxtBirthday.Location = New System.Drawing.Point(446, 36)
        Me.txtNxtBirthday.Name = "txtNxtBirthday"
        Me.txtNxtBirthday.ReadOnly = True
        Me.txtNxtBirthday.Size = New System.Drawing.Size(122, 21)
        Me.txtNxtBirthday.TabIndex = 411
        '
        'lblNxtBirthday
        '
        Me.lblNxtBirthday.Location = New System.Drawing.Point(337, 39)
        Me.lblNxtBirthday.Name = "lblNxtBirthday"
        Me.lblNxtBirthday.Size = New System.Drawing.Size(104, 15)
        Me.lblNxtBirthday.TabIndex = 410
        Me.lblNxtBirthday.Text = "Next Birthday"
        Me.lblNxtBirthday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBirthday
        '
        Me.txtBirthday.BackColor = System.Drawing.SystemColors.Window
        Me.txtBirthday.Flags = 0
        Me.txtBirthday.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBirthday.Location = New System.Drawing.Point(104, 36)
        Me.txtBirthday.Name = "txtBirthday"
        Me.txtBirthday.ReadOnly = True
        Me.txtBirthday.Size = New System.Drawing.Size(132, 21)
        Me.txtBirthday.TabIndex = 409
        '
        'lblBirthday
        '
        Me.lblBirthday.Location = New System.Drawing.Point(14, 39)
        Me.lblBirthday.Name = "lblBirthday"
        Me.lblBirthday.Size = New System.Drawing.Size(84, 15)
        Me.lblBirthday.TabIndex = 408
        Me.lblBirthday.Text = "Birthday"
        Me.lblBirthday.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elBirthday_Anniversary
        '
        Me.elBirthday_Anniversary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elBirthday_Anniversary.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elBirthday_Anniversary.Location = New System.Drawing.Point(3, 9)
        Me.elBirthday_Anniversary.Name = "elBirthday_Anniversary"
        Me.elBirthday_Anniversary.Size = New System.Drawing.Size(575, 17)
        Me.elBirthday_Anniversary.TabIndex = 407
        Me.elBirthday_Anniversary.Text = "Birthday && Anniversary"
        Me.elBirthday_Anniversary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objfrmDiary_Dates
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lblYears)
        Me.Controls.Add(Me.txtYears)
        Me.Controls.Add(Me.txtNxtAnniversary)
        Me.Controls.Add(Me.lblNxtAnniversary)
        Me.Controls.Add(Me.txtAnniversary)
        Me.Controls.Add(Me.lblAnniversary)
        Me.Controls.Add(Me.lblAge)
        Me.Controls.Add(Me.txtAge)
        Me.Controls.Add(Me.txtNxtBirthday)
        Me.Controls.Add(Me.lblNxtBirthday)
        Me.Controls.Add(Me.txtBirthday)
        Me.Controls.Add(Me.lblBirthday)
        Me.Controls.Add(Me.elBirthday_Anniversary)
        Me.Controls.Add(Me.lvEmpDates)
        Me.Controls.Add(Me.elEmployeeDates)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_Dates"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents elEmployeeDates As eZee.Common.eZeeLine
    Friend WithEvents lvEmpDates As eZee.Common.eZeeListView
    Friend WithEvents colhSrNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFromDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhToDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMode As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhModeId As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblYears As System.Windows.Forms.Label
    Friend WithEvents txtYears As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtNxtAnniversary As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblNxtAnniversary As System.Windows.Forms.Label
    Friend WithEvents txtAnniversary As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAnniversary As System.Windows.Forms.Label
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents txtAge As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtNxtBirthday As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblNxtBirthday As System.Windows.Forms.Label
    Friend WithEvents txtBirthday As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBirthday As System.Windows.Forms.Label
    Friend WithEvents elBirthday_Anniversary As eZee.Common.eZeeLine
    Friend WithEvents colhPayExcluded As System.Windows.Forms.ColumnHeader
End Class

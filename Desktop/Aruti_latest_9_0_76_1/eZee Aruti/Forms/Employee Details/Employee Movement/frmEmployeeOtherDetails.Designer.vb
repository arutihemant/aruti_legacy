﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeOtherDetails
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeOtherDetails))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objgbInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchCCTrnHead = New eZee.Common.eZeeGradientButton
        Me.pnlData = New System.Windows.Forms.Panel
        Me.lblAppointmentdate = New System.Windows.Forms.Label
        Me.txtDate = New System.Windows.Forms.TextBox
        Me.cboCCTrnHead = New System.Windows.Forms.ComboBox
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.objbtnAddReason = New eZee.Common.eZeeGradientButton
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.objSearchReason = New eZee.Common.eZeeGradientButton
        Me.lblReason = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnl1 = New System.Windows.Forms.Panel
        Me.dgvHistory = New System.Windows.Forms.DataGridView
        Me.objdgcolhViewPending = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhOperationType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChangeDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhValue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFromEmp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhdetailtranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAppointDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhrehiretranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhtranguid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhOperationTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboChangeReason = New System.Windows.Forms.ComboBox
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblPendingData = New System.Windows.Forms.Label
        Me.objlblRehire = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.pnlMain.SuspendLayout()
        Me.objgbInformation.SuspendLayout()
        Me.pnlData.SuspendLayout()
        Me.pnl1.SuspendLayout()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objgbInformation)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(642, 381)
        Me.pnlMain.TabIndex = 2
        '
        'objgbInformation
        '
        Me.objgbInformation.BorderColor = System.Drawing.Color.Black
        Me.objgbInformation.Checked = False
        Me.objgbInformation.CollapseAllExceptThis = False
        Me.objgbInformation.CollapsedHoverImage = Nothing
        Me.objgbInformation.CollapsedNormalImage = Nothing
        Me.objgbInformation.CollapsedPressedImage = Nothing
        Me.objgbInformation.CollapseOnLoad = False
        Me.objgbInformation.Controls.Add(Me.lnkAllocation)
        Me.objgbInformation.Controls.Add(Me.objbtnSearchCCTrnHead)
        Me.objgbInformation.Controls.Add(Me.pnlData)
        Me.objgbInformation.Controls.Add(Me.cboCCTrnHead)
        Me.objgbInformation.Controls.Add(Me.objlblCaption)
        Me.objgbInformation.Controls.Add(Me.objbtnAddReason)
        Me.objgbInformation.Controls.Add(Me.objelLine1)
        Me.objgbInformation.Controls.Add(Me.objSearchReason)
        Me.objgbInformation.Controls.Add(Me.lblReason)
        Me.objgbInformation.Controls.Add(Me.btnSave)
        Me.objgbInformation.Controls.Add(Me.pnl1)
        Me.objgbInformation.Controls.Add(Me.objbtnSearchEmployee)
        Me.objgbInformation.Controls.Add(Me.cboChangeReason)
        Me.objgbInformation.Controls.Add(Me.dtpEffectiveDate)
        Me.objgbInformation.Controls.Add(Me.cboEmployee)
        Me.objgbInformation.Controls.Add(Me.lblEffectiveDate)
        Me.objgbInformation.Controls.Add(Me.lblEmployee)
        Me.objgbInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objgbInformation.ExpandedHoverImage = Nothing
        Me.objgbInformation.ExpandedNormalImage = Nothing
        Me.objgbInformation.ExpandedPressedImage = Nothing
        Me.objgbInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbInformation.HeaderHeight = 25
        Me.objgbInformation.HeaderMessage = ""
        Me.objgbInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.objgbInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbInformation.HeightOnCollapse = 0
        Me.objgbInformation.LeftTextSpace = 0
        Me.objgbInformation.Location = New System.Drawing.Point(0, 0)
        Me.objgbInformation.Name = "objgbInformation"
        Me.objgbInformation.OpenHeight = 300
        Me.objgbInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbInformation.ShowBorder = True
        Me.objgbInformation.ShowCheckBox = False
        Me.objgbInformation.ShowCollapseButton = False
        Me.objgbInformation.ShowDefaultBorderColor = True
        Me.objgbInformation.ShowDownButton = False
        Me.objgbInformation.ShowHeader = True
        Me.objgbInformation.Size = New System.Drawing.Size(642, 331)
        Me.objgbInformation.TabIndex = 0
        Me.objgbInformation.Temp = 0
        Me.objgbInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCCTrnHead
        '
        Me.objbtnSearchCCTrnHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCCTrnHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCCTrnHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCCTrnHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCCTrnHead.BorderSelected = False
        Me.objbtnSearchCCTrnHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCCTrnHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCCTrnHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCCTrnHead.Location = New System.Drawing.Point(518, 64)
        Me.objbtnSearchCCTrnHead.Name = "objbtnSearchCCTrnHead"
        Me.objbtnSearchCCTrnHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCCTrnHead.TabIndex = 185
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.lblAppointmentdate)
        Me.pnlData.Controls.Add(Me.txtDate)
        Me.pnlData.Location = New System.Drawing.Point(462, 100)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(167, 25)
        Me.pnlData.TabIndex = 187
        '
        'lblAppointmentdate
        '
        Me.lblAppointmentdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointmentdate.Location = New System.Drawing.Point(3, 5)
        Me.lblAppointmentdate.Name = "lblAppointmentdate"
        Me.lblAppointmentdate.Size = New System.Drawing.Size(61, 15)
        Me.lblAppointmentdate.TabIndex = 183
        Me.lblAppointmentdate.Text = "Appt. Date"
        Me.lblAppointmentdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDate
        '
        Me.txtDate.BackColor = System.Drawing.Color.White
        Me.txtDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.Location = New System.Drawing.Point(70, 2)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.ReadOnly = True
        Me.txtDate.Size = New System.Drawing.Size(94, 21)
        Me.txtDate.TabIndex = 184
        '
        'cboCCTrnHead
        '
        Me.cboCCTrnHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCCTrnHead.DropDownWidth = 120
        Me.cboCCTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCCTrnHead.FormattingEnabled = True
        Me.cboCCTrnHead.Location = New System.Drawing.Point(101, 64)
        Me.cboCCTrnHead.Name = "cboCCTrnHead"
        Me.cboCCTrnHead.Size = New System.Drawing.Size(411, 21)
        Me.cboCCTrnHead.TabIndex = 210
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(12, 67)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(83, 15)
        Me.objlblCaption.TabIndex = 209
        Me.objlblCaption.Text = "#value"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddReason
        '
        Me.objbtnAddReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReason.BorderSelected = False
        Me.objbtnAddReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReason.Location = New System.Drawing.Point(408, 102)
        Me.objbtnAddReason.Name = "objbtnAddReason"
        Me.objbtnAddReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReason.TabIndex = 207
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(12, 88)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(618, 9)
        Me.objelLine1.TabIndex = 205
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objSearchReason
        '
        Me.objSearchReason.BackColor = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchReason.BorderSelected = False
        Me.objSearchReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchReason.Location = New System.Drawing.Point(435, 102)
        Me.objSearchReason.Name = "objSearchReason"
        Me.objSearchReason.Size = New System.Drawing.Size(21, 21)
        Me.objSearchReason.TabIndex = 204
        '
        'lblReason
        '
        Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReason.Location = New System.Drawing.Point(12, 105)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(83, 15)
        Me.lblReason.TabIndex = 202
        Me.lblReason.Text = "Reason"
        Me.lblReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(545, 37)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(84, 48)
        Me.btnSave.TabIndex = 37
        Me.btnSave.Text = "&Save Changes"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'pnl1
        '
        Me.pnl1.Controls.Add(Me.dgvHistory)
        Me.pnl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl1.Location = New System.Drawing.Point(12, 131)
        Me.pnl1.Name = "pnl1"
        Me.pnl1.Size = New System.Drawing.Size(618, 192)
        Me.pnl1.TabIndex = 173
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToResizeRows = False
        Me.dgvHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhViewPending, Me.objdgcolhEdit, Me.objdgcolhDelete, Me.objdgcolhOperationType, Me.dgcolhChangeDate, Me.objdgcolhValue, Me.dgcolhReason, Me.objdgcolhFromEmp, Me.objdgcolhdetailtranunkid, Me.objdgcolhAppointDate, Me.objdgcolhrehiretranunkid, Me.objdgcolhtranguid, Me.objdgcolhOperationTypeId})
        Me.dgvHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHistory.Location = New System.Drawing.Point(0, 0)
        Me.dgvHistory.MultiSelect = False
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.Size = New System.Drawing.Size(618, 192)
        Me.dgvHistory.TabIndex = 0
        '
        'objdgcolhViewPending
        '
        Me.objdgcolhViewPending.Frozen = True
        Me.objdgcolhViewPending.HeaderText = ""
        Me.objdgcolhViewPending.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.objdgcolhViewPending.Name = "objdgcolhViewPending"
        Me.objdgcolhViewPending.ReadOnly = True
        Me.objdgcolhViewPending.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhViewPending.Width = 25
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'objdgcolhOperationType
        '
        Me.objdgcolhOperationType.Frozen = True
        Me.objdgcolhOperationType.HeaderText = "OperationType"
        Me.objdgcolhOperationType.Name = "objdgcolhOperationType"
        Me.objdgcolhOperationType.ReadOnly = True
        Me.objdgcolhOperationType.Visible = False
        '
        'dgcolhChangeDate
        '
        Me.dgcolhChangeDate.Frozen = True
        Me.dgcolhChangeDate.HeaderText = "Effective Date"
        Me.dgcolhChangeDate.Name = "dgcolhChangeDate"
        Me.dgcolhChangeDate.ReadOnly = True
        Me.dgcolhChangeDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhValue
        '
        Me.objdgcolhValue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhValue.Frozen = True
        Me.objdgcolhValue.HeaderText = ""
        Me.objdgcolhValue.Name = "objdgcolhValue"
        Me.objdgcolhValue.ReadOnly = True
        Me.objdgcolhValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhValue.Width = 285
        '
        'dgcolhReason
        '
        Me.dgcolhReason.Frozen = True
        Me.dgcolhReason.HeaderText = "Reason"
        Me.dgcolhReason.Name = "dgcolhReason"
        Me.dgcolhReason.ReadOnly = True
        Me.dgcolhReason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhReason.Width = 180
        '
        'objdgcolhFromEmp
        '
        Me.objdgcolhFromEmp.Frozen = True
        Me.objdgcolhFromEmp.HeaderText = "objdgcolhFromEmp"
        Me.objdgcolhFromEmp.Name = "objdgcolhFromEmp"
        Me.objdgcolhFromEmp.ReadOnly = True
        Me.objdgcolhFromEmp.Visible = False
        '
        'objdgcolhdetailtranunkid
        '
        Me.objdgcolhdetailtranunkid.Frozen = True
        Me.objdgcolhdetailtranunkid.HeaderText = "objdgcolhdetailtranunkid"
        Me.objdgcolhdetailtranunkid.Name = "objdgcolhdetailtranunkid"
        Me.objdgcolhdetailtranunkid.ReadOnly = True
        Me.objdgcolhdetailtranunkid.Visible = False
        '
        'objdgcolhAppointDate
        '
        Me.objdgcolhAppointDate.Frozen = True
        Me.objdgcolhAppointDate.HeaderText = "objdgcolhAppointDate"
        Me.objdgcolhAppointDate.Name = "objdgcolhAppointDate"
        Me.objdgcolhAppointDate.ReadOnly = True
        Me.objdgcolhAppointDate.Visible = False
        '
        'objdgcolhrehiretranunkid
        '
        Me.objdgcolhrehiretranunkid.Frozen = True
        Me.objdgcolhrehiretranunkid.HeaderText = "objdgcolhrehiretranunkid"
        Me.objdgcolhrehiretranunkid.Name = "objdgcolhrehiretranunkid"
        Me.objdgcolhrehiretranunkid.ReadOnly = True
        Me.objdgcolhrehiretranunkid.Visible = False
        '
        'objdgcolhtranguid
        '
        Me.objdgcolhtranguid.Frozen = True
        Me.objdgcolhtranguid.HeaderText = "objdgcolhtranguid"
        Me.objdgcolhtranguid.Name = "objdgcolhtranguid"
        Me.objdgcolhtranguid.ReadOnly = True
        Me.objdgcolhtranguid.Visible = False
        '
        'objdgcolhOperationTypeId
        '
        Me.objdgcolhOperationTypeId.HeaderText = "objdgcolhOperationTypeId"
        Me.objdgcolhOperationTypeId.Name = "objdgcolhOperationTypeId"
        Me.objdgcolhOperationTypeId.ReadOnly = True
        Me.objdgcolhOperationTypeId.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(518, 37)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 4
        '
        'cboChangeReason
        '
        Me.cboChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChangeReason.DropDownWidth = 400
        Me.cboChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChangeReason.FormattingEnabled = True
        Me.cboChangeReason.Location = New System.Drawing.Point(101, 102)
        Me.cboChangeReason.Name = "cboChangeReason"
        Me.cboChangeReason.Size = New System.Drawing.Size(301, 21)
        Me.cboChangeReason.TabIndex = 203
        '
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Checked = False
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(101, 37)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.ShowCheckBox = True
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpEffectiveDate.TabIndex = 1
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 450
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(279, 37)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(233, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(12, 40)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(83, 15)
        Me.lblEffectiveDate.TabIndex = 0
        Me.lblEffectiveDate.Text = "Effective Date"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(211, 40)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(62, 15)
        Me.lblEmployee.TabIndex = 2
        Me.lblEmployee.Text = "Employee"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblPendingData)
        Me.objFooter.Controls.Add(Me.objlblRehire)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 331)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(642, 50)
        Me.objFooter.TabIndex = 1
        '
        'lblPendingData
        '
        Me.lblPendingData.BackColor = System.Drawing.Color.PowderBlue
        Me.lblPendingData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPendingData.ForeColor = System.Drawing.Color.Black
        Me.lblPendingData.Location = New System.Drawing.Point(371, 18)
        Me.lblPendingData.Name = "lblPendingData"
        Me.lblPendingData.Size = New System.Drawing.Size(155, 17)
        Me.lblPendingData.TabIndex = 10
        Me.lblPendingData.Text = "Pending Approval"
        Me.lblPendingData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objlblRehire
        '
        Me.objlblRehire.BackColor = System.Drawing.Color.Orange
        Me.objlblRehire.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblRehire.ForeColor = System.Drawing.Color.Black
        Me.objlblRehire.Location = New System.Drawing.Point(12, 18)
        Me.objlblRehire.Name = "objlblRehire"
        Me.objlblRehire.Size = New System.Drawing.Size(155, 17)
        Me.objlblRehire.TabIndex = 9
        Me.objlblRehire.Text = "Employee Rehired"
        Me.objlblRehire.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(536, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(563, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(76, 13)
        Me.lnkAllocation.TabIndex = 234
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'frmEmployeeOtherDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(642, 381)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeOtherDetails"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.pnlMain.ResumeLayout(False)
        Me.objgbInformation.ResumeLayout(False)
        Me.pnlData.ResumeLayout(False)
        Me.pnlData.PerformLayout()
        Me.pnl1.ResumeLayout(False)
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objgbInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents lblAppointmentdate As System.Windows.Forms.Label
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents cboCCTrnHead As System.Windows.Forms.ComboBox
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents objbtnAddReason As eZee.Common.eZeeGradientButton
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents objSearchReason As eZee.Common.eZeeGradientButton
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents pnl1 As System.Windows.Forms.Panel
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchCCTrnHead As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblRehire As System.Windows.Forms.Label
    Friend WithEvents lblPendingData As System.Windows.Forms.Label
    Friend WithEvents objdgcolhViewPending As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhOperationType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChangeDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhValue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFromEmp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhdetailtranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAppointDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhrehiretranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhtranguid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhOperationTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
End Class

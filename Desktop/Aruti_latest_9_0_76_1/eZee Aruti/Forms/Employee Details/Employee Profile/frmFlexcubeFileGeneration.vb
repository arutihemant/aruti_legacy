﻿#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.IO
Imports System.Text.RegularExpressions

#End Region

Public Class frmFlexcubeData

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmFlexcubeFileGeneration"
    Private objEmployee As clsEmployee_Master
    Private mdtEmployee As DataTable
    Private mvwEmployee As DataView
    Private mstrAdvanceFilter As String = ""
    Private mintProgressCount As Integer = 0
    Private mDicReportName As Dictionary(Of Integer, String)

#End Region

#Region " Form's Events "

    Private Sub frmFlexcubeFileGeneration_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmployee = Nothing
    End Sub

    Private Sub frmFlexcubeFileGeneration_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Master"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFlexcubeFileGeneration_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmFlexcubeFileGeneration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmployee = New clsEmployee_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFlexcubeFileGeneration_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            With cboResponseType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 7, "Success"))
                .Items.Add(Language.getMessage(mstrModuleName, 8, "Failure"))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub CommonOperation()
        Try
            mdtEmployee.AcceptChanges()
            If mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please check atleast one employee to generte file."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            mDicReportName = mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).ToDictionary(Of Integer, String)(Function(row) row.Field(Of Integer)("employeeunkid"), Function(row) Regex.Replace(row.Field(Of String)("employeecode"), "[^0-9a-zA-Z]+", ""))
            pbProgress_Report.Value = 0
            pbProgress_Report.Maximum = mDicReportName.Keys.Count
            mintProgressCount = 1
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CommonOperation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillEmployee()
        Dim dsData As New DataSet
        Dim strFilter As String = ""
        Try

            If cboGenerateFile.SelectedIndex <= 0 Then
                If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()
                Exit Sub
            End If

            If cboGenerateFile.SelectedIndex > 0 AndAlso cboGenerateFile.SelectedIndex <= 2 Then
                strFilter &= "AND filetypeid = " & cboGenerateFile.SelectedIndex
            End If

            If mstrAdvanceFilter.Length > 0 Then
                strFilter &= "AND " & mstrAdvanceFilter
            End If

            If cboResponseType.SelectedIndex = 1 Then
                strFilter &= "AND iserror = 0 "
            ElseIf cboResponseType.SelectedIndex = 2 Then
                strFilter &= "AND iserror = 1 "
            End If

            If strFilter.Trim.Length > 0 Then
                strFilter = strFilter.Substring(3)
            End If

            dsData = objEmployee.GetFlexcubeErrorList(FinancialYear._Object._DatabaseName, _
                                                      User._Object._Userunkid, _
                                                      FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      ConfigParameter._Object._UserAccessModeSetting, _
                                                      True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                      "List", True, strFilter, True)

            mdtEmployee = dsData.Tables(0).Copy
            mvwEmployee = mdtEmployee.DefaultView
            dgvAEmployee.AutoGenerateColumns = False

            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEName.DataPropertyName = "rEmp"
            dgcolhRDate.DataPropertyName = "rDate"
            dgcolhRequestTime.DataPropertyName = "rTime"
            dgcolhMsgId.DataPropertyName = "MsgId"
            dgcolhErrorDescription.DataPropertyName = "eDesc"
            dgcolhFileType.DataPropertyName = "rFile"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgvAEmployee.DataSource = mvwEmployee

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ShowCount()
        Try
            Call objbtnSearch.ShowResult(CStr(dgvAEmployee.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAdvanceFilter = ""
            Call FillEmployee()
            ShowCount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                Call FillEmployee()
                ShowCount()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            mvwEmployee.RowFilter = strSearch
            ShowCount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboResponseType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboResponseType.SelectedIndexChanged
        Try
            If cboResponseType.SelectedIndex > 0 Then
                With cboGenerateFile
                    .Items.Clear()
                    .Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
                    .Items.Add(Language.getMessage(mstrModuleName, 3, "Customer"))
                    .Items.Add(Language.getMessage(mstrModuleName, 4, "Account"))
                    .Items.Add(Language.getMessage(mstrModuleName, 5, "Both"))
                    .SelectedIndex = 0
                End With
                objlblType.Text = cboResponseType.Text
            Else
                With cboGenerateFile
                    .Items.Clear()
                    .Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))                    
                    .SelectedIndex = 0
                End With
                objlblType.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboResponseType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboGenerateFile_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGenerateFile.SelectedIndexChanged
        Try
            If cboGenerateFile.SelectedIndex <= 0 Then
                If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()
                Exit Sub
            End If
            Call FillEmployee()
            ShowCount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGenerateFile_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Data Grid Event(s) "

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick, dgvAEmployee.CellContentDoubleClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = mvwEmployee.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If mvwEmployee.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvAEmployee_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvAEmployee.CellPainting
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If dgvAEmployee.Columns(e.ColumnIndex).DataPropertyName = dgcolhRDate.DataPropertyName Then
                If dgvAEmployee.Rows(e.RowIndex).Cells(e.ColumnIndex).Value IsNot Nothing AndAlso IsDBNull(dgvAEmployee.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False AndAlso dgvAEmployee.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString().Trim.Length > 0 AndAlso IsDate(dgvAEmployee.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                    dgvAEmployee.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = eZeeDate.convertDate(dgvAEmployee.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString()).ToShortDateString
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellPainting", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
            For Each dr As DataRowView In mvwEmployee
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvAEmployee.Refresh()
            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnStopProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnStopProcess.GradientForeColor = GUI._ButttonFontColor

            Me.btnPost.GradientBackColor = GUI._ButttonBackColor
            Me.btnPost.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbEmployee.Text = Language._Object.getCaption(Me.gbEmployee.Name, Me.gbEmployee.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.btnStopProcess.Text = Language._Object.getCaption(Me.btnStopProcess.Name, Me.btnStopProcess.Text)
            Me.pbProgress_Report.Text = Language._Object.getCaption(Me.pbProgress_Report.Name, Me.pbProgress_Report.Text)
            Me.btnPost.Text = Language._Object.getCaption(Me.btnPost.Name, Me.btnPost.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.objlblType.Text = Language._Object.getCaption(Me.objlblType.Name, Me.objlblType.Text)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.dgcolhErrorDescription.HeaderText = Language._Object.getCaption(Me.dgcolhErrorDescription.Name, Me.dgcolhErrorDescription.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Please check atleast one employee to generte file.")
            Language.setMessage(mstrModuleName, 3, "Customer")
            Language.setMessage(mstrModuleName, 4, "Account")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

    
End Class

'Public Class frmFlexcubeFileGeneration

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmFlexcubeFileGeneration"
'    Private objEmployee As clsEmployee_Master
'    Private mdtEmployee As DataTable
'    Private mvwEmployee As DataView
'    Private mstrAdvanceFilter As String = ""
'    Private mintProgressCount As Integer = 0
'    Private mDicReportName As Dictionary(Of Integer, String)

'#End Region

'#Region " Form's Events "

'    Private Sub frmFlexcubeFileGeneration_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        objEmployee = Nothing
'    End Sub

'    Private Sub frmFlexcubeFileGeneration_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsEmployee_Master.SetMessages()
'            objfrm._Other_ModuleNames = "clsEmployee_Master"
'            objfrm.displayDialog(Me)

'            Call Language.setLanguage(Me.Name)
'            Call SetLanguage()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmFlexcubeFileGeneration_LanguageClick", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmFlexcubeFileGeneration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objEmployee = New clsEmployee_Master
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            Call FillCombo()
'            Call FillEmployee()
'            Call SetVisibility()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmFlexcubeFileGeneration_Load", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Private Methods "

'    Private Sub SetVisibility()
'        Try

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Try
'            With cboGenerateFile
'                .Items.Clear()
'                .Items.Add(Language.getMessage(mstrModuleName, 3, "Customer File"))
'                .Items.Add(Language.getMessage(mstrModuleName, 4, "Account File"))
'                .SelectedIndex = 0
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub CommonOperation()
'        Try
'            mdtEmployee.AcceptChanges()
'            If mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please check atleast one employee to generte file."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            mDicReportName = mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).ToDictionary(Of Integer, String)(Function(row) row.Field(Of Integer)("employeeunkid"), Function(row) Regex.Replace(row.Field(Of String)("employeecode"), "[^0-9a-zA-Z]+", ""))
'            pbProgress_Report.Value = 0
'            pbProgress_Report.Maximum = mDicReportName.Keys.Count
'            mintProgressCount = 1
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "CommonOperation", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub FillEmployee()
'        Dim dsData As New DataSet
'        Try
'            dsData = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
'                                                 User._Object._Userunkid, _
'                                                 FinancialYear._Object._YearUnkid, _
'                                                 Company._Object._Companyunkid, _
'                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
'                                                 ConfigParameter._Object._UserAccessModeSetting, _
'                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", , , , , , , , , , , , , , , , mstrAdvanceFilter)
'            Dim dCol As New DataColumn
'            dCol.DataType = GetType(System.Boolean)
'            dCol.DefaultValue = False
'            dCol.ColumnName = "ischeck"
'            dsData.Tables(0).Columns.Add(dCol)

'            mdtEmployee = dsData.Tables(0).Copy
'            mvwEmployee = mdtEmployee.DefaultView

'            dgvAEmployee.AutoGenerateColumns = False
'            objdgcolhECheck.DataPropertyName = "ischeck"
'            dgcolhEName.DataPropertyName = "EmpCodeName"
'            objdgcolhEmpId.DataPropertyName = "employeeunkid"
'            dgvAEmployee.DataSource = mvwEmployee
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub EanbleDisableControls(ByVal blnFlag As Boolean)
'        btnStopProcess.Enabled = Not blnFlag
'        btnClose.Enabled = blnFlag : btnExport.Enabled = blnFlag
'        gbEmployee.Enabled = blnFlag
'    End Sub

'#End Region

'#Region " Button's Events "

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
'        Try
'            mstrAdvanceFilter = ""
'            Call FillEmployee()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnStopProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStopProcess.Click
'        Try
'            If bgWorker.IsBusy Then
'                bgWorker.CancelAsync()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnStopProcess_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
'        Try
'            If ConfigParameter._Object._ExportDataPath.Trim.Length <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please set Export Data Path from Aruti configuration -> Options -> Path."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            Call CommonOperation()
'            EanbleDisableControls(False)
'            bgWorker.RunWorkerAsync()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Link Event "

'    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
'        Dim frm As New frmAdvanceSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
'                mstrAdvanceFilter = frm._GetFilterString
'                Call FillEmployee()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'#End Region

'#Region " Textbox Event(s) "

'    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
'        Try
'            Select Case e.KeyCode
'                Case Windows.Forms.Keys.Down
'                    If dgvAEmployee.Rows.Count > 0 Then
'                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
'                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
'                    End If
'                Case Windows.Forms.Keys.Up
'                    If dgvAEmployee.Rows.Count > 0 Then
'                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
'                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
'                    End If
'            End Select
'        Catch ex As Exception
'            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
'        Try
'            Dim strSearch As String = ""
'            If txtSearchEmp.Text.Trim.Length > 0 Then
'                strSearch = dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
'            End If
'            mvwEmployee.RowFilter = strSearch
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Data Grid Event(s) "

'    Private Sub dgvAEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick, dgvAEmployee.CellContentDoubleClick
'        Try
'            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

'            If e.ColumnIndex = objdgcolhECheck.Index Then

'                If Me.dgvAEmployee.IsCurrentCellDirty Then
'                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
'                End If

'                Dim drRow As DataRow() = mvwEmployee.ToTable.Select("ischeck = true", "")
'                If drRow.Length > 0 Then
'                    If mvwEmployee.ToTable.Rows.Count = drRow.Length Then
'                        objchkEmployee.CheckState = CheckState.Checked
'                    Else
'                        objchkEmployee.CheckState = CheckState.Indeterminate
'                    End If
'                Else
'                    objchkEmployee.CheckState = CheckState.Unchecked
'                End If
'            End If

'            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Checkbox Event "

'    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
'        Try
'            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
'            For Each dr As DataRowView In mvwEmployee
'                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
'            Next
'            dgvAEmployee.Refresh()
'            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Background Worker Event(s) "

'    Private Sub bgWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker.DoWork
'        Try
'            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
'            For Each ikey As Integer In mDicReportName.Keys
'                If bgWorker.CancellationPending = True Then
'                    e.Cancel = True
'                    Call EanbleDisableControls(True)
'                    Exit For
'                End If
'                Dim strBuilder, strPrefix As String
'                strPrefix = ""
'                Select Case cboGenerateFile.SelectedIndex
'                    Case 0  'CUSTOMER
'                        strPrefix = "CUST_"
'                    Case 1  'ACCOUNNT
'                        strPrefix = "ACCT_"
'                End Select
'                Dim strMessageId As String = ""
'                strBuilder = objEmployee.GenerateFileString(cboGenerateFile.SelectedIndex, ConfigParameter._Object._FlexcubeAccountCategory, ConfigParameter._Object._FlexcubeAccountClass, ikey, ConfigParameter._Object._EmployeeAsOnDate, strMessageId)
'                If strBuilder.Trim.Length > 0 Then
'                    Dim strFileName As String = String.Empty
'                    strFileName = strPrefix & mDicReportName(ikey) & "_" & Format(Now, "yyyymmdd") & ".xml"
'                    System.IO.File.WriteAllText(ConfigParameter._Object._ExportDataPath + "\" & strFileName, strBuilder)
'                    If objEmployee.InsertFlexcubeRequest(ikey, 0, cboGenerateFile.SelectedIndex, strFileName, strBuilder, "", strMessageId) = False Then
'                        If bgWorker.CancellationPending = True Then
'                            e.Cancel = True
'                            Call EanbleDisableControls(True)
'                            Exit For
'                        End If
'                    End If
'                End If
'                bgWorker.ReportProgress(mintProgressCount)
'                mintProgressCount = mintProgressCount + 1
'            Next
'            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "bgWorker_DoWork", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub bgWorker_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgWorker.RunWorkerCompleted
'        Call EanbleDisableControls(True)
'        pbProgress_Report.Value = 0
'        objlnkValue.Text = ""
'    End Sub

'    Private Sub bgWorker_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgWorker.ProgressChanged
'        If pbProgress_Report.Value > pbProgress_Report.Maximum Then
'            pbProgress_Report.Value = pbProgress_Report.Value
'        Else
'            pbProgress_Report.Value = e.ProgressPercentage
'        End If
'        objlnkValue.Text = Language.getMessage(mstrModuleName, 200, "Processed : ") & " " & pbProgress_Report.Value.ToString & " / " & pbProgress_Report.Maximum.ToString
'    End Sub

'#End Region

'    '<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'    Private Sub OtherSettings()
'        Try
'            Me.SuspendLayout()

'            Call SetLanguage()

'            Me.gbEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
'            Me.gbEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor

'            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
'            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


'            Me.btnStopProcess.GradientBackColor = GUI._ButttonBackColor
'            Me.btnStopProcess.GradientForeColor = GUI._ButttonFontColor

'            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
'            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

'            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
'            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


'            Me.ResumeLayout()
'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetLanguage()
'        Try
'            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

'            Me.gbEmployee.Text = Language._Object.getCaption(Me.gbEmployee.Name, Me.gbEmployee.Text)
'            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
'            Me.btnStopProcess.Text = Language._Object.getCaption(Me.btnStopProcess.Name, Me.btnStopProcess.Text)
'            Me.pbProgress_Report.Text = Language._Object.getCaption(Me.pbProgress_Report.Name, Me.pbProgress_Report.Text)
'            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
'            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
'            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
'            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
'            Me.lblExportFileType.Text = Language._Object.getCaption(Me.lblExportFileType.Name, Me.lblExportFileType.Text)

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetMessages()
'        Try
'            Language.setMessage(mstrModuleName, 1, "Please set Export Data Path from Aruti configuration -> Options -> Path.")
'            Language.setMessage(mstrModuleName, 2, "Please check atleast one employee to generte file.")
'            Language.setMessage(mstrModuleName, 3, "Customer File")
'            Language.setMessage(mstrModuleName, 4, "Account File")
'            Language.setMessage(mstrModuleName, 200, "Processed :")

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
'        End Try
'    End Sub
'#End Region 'Language & UI Settings
'    '</Language>
'End Class
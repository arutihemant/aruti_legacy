﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMissingEmployeeList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMissingEmployeeList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbMissingEmployeeInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objSpc1 = New System.Windows.Forms.SplitContainer
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchEmp = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvAEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lnkGenerateDetails = New System.Windows.Forms.LinkLabel
        Me.lblMovements = New System.Windows.Forms.Label
        Me.chkShowWorkPermit = New System.Windows.Forms.CheckBox
        Me.chkShowRehire = New System.Windows.Forms.CheckBox
        Me.chkShowDates = New System.Windows.Forms.CheckBox
        Me.chkShowCostCenters = New System.Windows.Forms.CheckBox
        Me.chkShowCategorization = New System.Windows.Forms.CheckBox
        Me.chkShowTransfer = New System.Windows.Forms.CheckBox
        Me.chkShowActiveEmployee = New System.Windows.Forms.CheckBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuExportAll = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportCheckedEmployee = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuShowMovementDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.lnkNote = New System.Windows.Forms.Label
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbMissingEmployeeInfo.SuspendLayout()
        Me.objSpc1.Panel1.SuspendLayout()
        Me.objSpc1.Panel2.SuspendLayout()
        Me.objSpc1.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbMissingEmployeeInfo)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(653, 403)
        Me.pnlMain.TabIndex = 0
        '
        'gbMissingEmployeeInfo
        '
        Me.gbMissingEmployeeInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMissingEmployeeInfo.Checked = False
        Me.gbMissingEmployeeInfo.CollapseAllExceptThis = False
        Me.gbMissingEmployeeInfo.CollapsedHoverImage = Nothing
        Me.gbMissingEmployeeInfo.CollapsedNormalImage = Nothing
        Me.gbMissingEmployeeInfo.CollapsedPressedImage = Nothing
        Me.gbMissingEmployeeInfo.CollapseOnLoad = False
        Me.gbMissingEmployeeInfo.Controls.Add(Me.objSpc1)
        Me.gbMissingEmployeeInfo.Controls.Add(Me.chkShowActiveEmployee)
        Me.gbMissingEmployeeInfo.Controls.Add(Me.objbtnReset)
        Me.gbMissingEmployeeInfo.Controls.Add(Me.objbtnSearch)
        Me.gbMissingEmployeeInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbMissingEmployeeInfo.ExpandedHoverImage = Nothing
        Me.gbMissingEmployeeInfo.ExpandedNormalImage = Nothing
        Me.gbMissingEmployeeInfo.ExpandedPressedImage = Nothing
        Me.gbMissingEmployeeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMissingEmployeeInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMissingEmployeeInfo.HeaderHeight = 25
        Me.gbMissingEmployeeInfo.HeaderMessage = ""
        Me.gbMissingEmployeeInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbMissingEmployeeInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMissingEmployeeInfo.HeightOnCollapse = 0
        Me.gbMissingEmployeeInfo.LeftTextSpace = 0
        Me.gbMissingEmployeeInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbMissingEmployeeInfo.Name = "gbMissingEmployeeInfo"
        Me.gbMissingEmployeeInfo.OpenHeight = 300
        Me.gbMissingEmployeeInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMissingEmployeeInfo.ShowBorder = True
        Me.gbMissingEmployeeInfo.ShowCheckBox = False
        Me.gbMissingEmployeeInfo.ShowCollapseButton = False
        Me.gbMissingEmployeeInfo.ShowDefaultBorderColor = True
        Me.gbMissingEmployeeInfo.ShowDownButton = False
        Me.gbMissingEmployeeInfo.ShowHeader = True
        Me.gbMissingEmployeeInfo.Size = New System.Drawing.Size(653, 353)
        Me.gbMissingEmployeeInfo.TabIndex = 117
        Me.gbMissingEmployeeInfo.Temp = 0
        Me.gbMissingEmployeeInfo.Text = "Employee Info"
        Me.gbMissingEmployeeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objSpc1
        '
        Me.objSpc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objSpc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.objSpc1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objSpc1.IsSplitterFixed = True
        Me.objSpc1.Location = New System.Drawing.Point(2, 26)
        Me.objSpc1.Name = "objSpc1"
        '
        'objSpc1.Panel1
        '
        Me.objSpc1.Panel1.Controls.Add(Me.tblpAssessorEmployee)
        '
        'objSpc1.Panel2
        '
        Me.objSpc1.Panel2.Controls.Add(Me.lnkGenerateDetails)
        Me.objSpc1.Panel2.Controls.Add(Me.lblMovements)
        Me.objSpc1.Panel2.Controls.Add(Me.chkShowWorkPermit)
        Me.objSpc1.Panel2.Controls.Add(Me.chkShowRehire)
        Me.objSpc1.Panel2.Controls.Add(Me.chkShowDates)
        Me.objSpc1.Panel2.Controls.Add(Me.chkShowCostCenters)
        Me.objSpc1.Panel2.Controls.Add(Me.chkShowCategorization)
        Me.objSpc1.Panel2.Controls.Add(Me.chkShowTransfer)
        Me.objSpc1.Size = New System.Drawing.Size(649, 325)
        Me.objSpc1.SplitterDistance = 463
        Me.objSpc1.SplitterWidth = 1
        Me.objSpc1.TabIndex = 308
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(0, 0)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(461, 323)
        Me.tblpAssessorEmployee.TabIndex = 304
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchEmp.Flags = 0
        Me.txtSearchEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(455, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.objchkEmployee)
        Me.objpnlEmp.Controls.Add(Me.dgvAEmployee)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(455, 291)
        Me.objpnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvAEmployee
        '
        Me.dgvAEmployee.AllowUserToAddRows = False
        Me.dgvAEmployee.AllowUserToDeleteRows = False
        Me.dgvAEmployee.AllowUserToResizeColumns = False
        Me.dgvAEmployee.AllowUserToResizeRows = False
        Me.dgvAEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAEmployee.ColumnHeadersHeight = 21
        Me.dgvAEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvAEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvAEmployee.MultiSelect = False
        Me.dgvAEmployee.Name = "dgvAEmployee"
        Me.dgvAEmployee.RowHeadersVisible = False
        Me.dgvAEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAEmployee.Size = New System.Drawing.Size(455, 291)
        Me.dgvAEmployee.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'lnkGenerateDetails
        '
        Me.lnkGenerateDetails.ActiveLinkColor = System.Drawing.Color.Red
        Me.lnkGenerateDetails.BackColor = System.Drawing.Color.Transparent
        Me.lnkGenerateDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkGenerateDetails.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkGenerateDetails.Location = New System.Drawing.Point(5, 172)
        Me.lnkGenerateDetails.Name = "lnkGenerateDetails"
        Me.lnkGenerateDetails.Size = New System.Drawing.Size(171, 17)
        Me.lnkGenerateDetails.TabIndex = 26
        Me.lnkGenerateDetails.TabStop = True
        Me.lnkGenerateDetails.Text = "[ Generate Details ]"
        Me.lnkGenerateDetails.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkGenerateDetails.VisitedLinkColor = System.Drawing.Color.Blue
        '
        'lblMovements
        '
        Me.lblMovements.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMovements.ForeColor = System.Drawing.Color.Maroon
        Me.lblMovements.Location = New System.Drawing.Point(5, 7)
        Me.lblMovements.Name = "lblMovements"
        Me.lblMovements.Size = New System.Drawing.Size(171, 15)
        Me.lblMovements.TabIndex = 25
        Me.lblMovements.TabStop = True
        Me.lblMovements.Text = "Employee Movments"
        Me.lblMovements.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowWorkPermit
        '
        Me.chkShowWorkPermit.Checked = True
        Me.chkShowWorkPermit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowWorkPermit.Location = New System.Drawing.Point(19, 144)
        Me.chkShowWorkPermit.Name = "chkShowWorkPermit"
        Me.chkShowWorkPermit.Size = New System.Drawing.Size(168, 17)
        Me.chkShowWorkPermit.TabIndex = 6
        Me.chkShowWorkPermit.Text = "Show Workpermit"
        Me.chkShowWorkPermit.UseVisualStyleBackColor = True
        '
        'chkShowRehire
        '
        Me.chkShowRehire.Checked = True
        Me.chkShowRehire.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowRehire.Location = New System.Drawing.Point(19, 121)
        Me.chkShowRehire.Name = "chkShowRehire"
        Me.chkShowRehire.Size = New System.Drawing.Size(168, 17)
        Me.chkShowRehire.TabIndex = 5
        Me.chkShowRehire.Text = "Show Re-Hire"
        Me.chkShowRehire.UseVisualStyleBackColor = True
        '
        'chkShowDates
        '
        Me.chkShowDates.Checked = True
        Me.chkShowDates.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowDates.Location = New System.Drawing.Point(19, 98)
        Me.chkShowDates.Name = "chkShowDates"
        Me.chkShowDates.Size = New System.Drawing.Size(168, 17)
        Me.chkShowDates.TabIndex = 4
        Me.chkShowDates.Text = "Show Dates"
        Me.chkShowDates.UseVisualStyleBackColor = True
        '
        'chkShowCostCenters
        '
        Me.chkShowCostCenters.Checked = True
        Me.chkShowCostCenters.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowCostCenters.Location = New System.Drawing.Point(19, 75)
        Me.chkShowCostCenters.Name = "chkShowCostCenters"
        Me.chkShowCostCenters.Size = New System.Drawing.Size(168, 17)
        Me.chkShowCostCenters.TabIndex = 3
        Me.chkShowCostCenters.Text = "Show Cost-Center"
        Me.chkShowCostCenters.UseVisualStyleBackColor = True
        '
        'chkShowCategorization
        '
        Me.chkShowCategorization.Checked = True
        Me.chkShowCategorization.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowCategorization.Location = New System.Drawing.Point(19, 52)
        Me.chkShowCategorization.Name = "chkShowCategorization"
        Me.chkShowCategorization.Size = New System.Drawing.Size(168, 17)
        Me.chkShowCategorization.TabIndex = 2
        Me.chkShowCategorization.Text = "Show Recategorization"
        Me.chkShowCategorization.UseVisualStyleBackColor = True
        '
        'chkShowTransfer
        '
        Me.chkShowTransfer.Checked = True
        Me.chkShowTransfer.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowTransfer.Location = New System.Drawing.Point(19, 29)
        Me.chkShowTransfer.Name = "chkShowTransfer"
        Me.chkShowTransfer.Size = New System.Drawing.Size(168, 17)
        Me.chkShowTransfer.TabIndex = 1
        Me.chkShowTransfer.Text = "Show Transfers"
        Me.chkShowTransfer.UseVisualStyleBackColor = True
        '
        'chkShowActiveEmployee
        '
        Me.chkShowActiveEmployee.BackColor = System.Drawing.Color.Transparent
        Me.chkShowActiveEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowActiveEmployee.Location = New System.Drawing.Point(400, 4)
        Me.chkShowActiveEmployee.Name = "chkShowActiveEmployee"
        Me.chkShowActiveEmployee.Size = New System.Drawing.Size(198, 17)
        Me.chkShowActiveEmployee.TabIndex = 306
        Me.chkShowActiveEmployee.Text = "Show Only Active Employee List"
        Me.chkShowActiveEmployee.UseVisualStyleBackColor = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(627, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 71
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(602, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 70
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.lnkNote)
        Me.objFooter.Controls.Add(Me.lnkAllocation)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 353)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(653, 50)
        Me.objFooter.TabIndex = 116
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(439, 11)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(103, 30)
        Me.btnOperations.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperations.TabIndex = 238
        Me.btnOperations.Text = "&Operations"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExportAll, Me.mnuExportCheckedEmployee, Me.mnuShowMovementDetails})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(212, 70)
        '
        'mnuExportAll
        '
        Me.mnuExportAll.Name = "mnuExportAll"
        Me.mnuExportAll.Size = New System.Drawing.Size(211, 22)
        Me.mnuExportAll.Tag = "mnuExportAll"
        Me.mnuExportAll.Text = "Export All Employee"
        '
        'mnuExportCheckedEmployee
        '
        Me.mnuExportCheckedEmployee.Name = "mnuExportCheckedEmployee"
        Me.mnuExportCheckedEmployee.Size = New System.Drawing.Size(211, 22)
        Me.mnuExportCheckedEmployee.Tag = "mnuExportCheckedEmployee"
        Me.mnuExportCheckedEmployee.Text = "Export Checked Employee"
        '
        'mnuShowMovementDetails
        '
        Me.mnuShowMovementDetails.Name = "mnuShowMovementDetails"
        Me.mnuShowMovementDetails.Size = New System.Drawing.Size(211, 22)
        Me.mnuShowMovementDetails.Tag = "mnuShowMovementDetails"
        Me.mnuShowMovementDetails.Text = "Show Movement Details"
        '
        'lnkNote
        '
        Me.lnkNote.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkNote.ForeColor = System.Drawing.Color.Maroon
        Me.lnkNote.Location = New System.Drawing.Point(12, 11)
        Me.lnkNote.Name = "lnkNote"
        Me.lnkNote.Size = New System.Drawing.Size(421, 30)
        Me.lnkNote.TabIndex = 24
        Me.lnkNote.TabStop = True
        Me.lnkNote.Text = "Note : All approved employees will be listed here, And no user access filter is a" & _
            "pplied."
        Me.lnkNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(410, 19)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(95, 15)
        Me.lnkAllocation.TabIndex = 237
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkAllocation.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(548, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 23
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmMissingEmployeeList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(653, 403)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMissingEmployeeList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Find Missing Employee(s)"
        Me.pnlMain.ResumeLayout(False)
        Me.gbMissingEmployeeInfo.ResumeLayout(False)
        Me.objSpc1.Panel1.ResumeLayout(False)
        Me.objSpc1.Panel2.ResumeLayout(False)
        Me.objSpc1.ResumeLayout(False)
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        Me.objpnlEmp.PerformLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbMissingEmployeeInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvAEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents lnkNote As System.Windows.Forms.Label
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkShowActiveEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuExportAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportCheckedEmployee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuShowMovementDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSpc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents chkShowRehire As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowDates As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowCostCenters As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowCategorization As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowTransfer As System.Windows.Forms.CheckBox
    Friend WithEvents lblMovements As System.Windows.Forms.Label
    Friend WithEvents chkShowWorkPermit As System.Windows.Forms.CheckBox
    Friend WithEvents lnkGenerateDetails As System.Windows.Forms.LinkLabel
End Class

﻿#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.IO
Imports System.Text.RegularExpressions

#End Region

Public Class frmFlexcubeErrorView

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmFlexcubeErrorView"
    Private objEmployee As clsEmployee_Master
    Private mdtEmployee As DataTable
    Private mvwEmployee As DataView
    Private mstrAdvanceFilter As String = ""

#End Region

#Region " Form's Events "

    Private Sub frmFlexcubeErrorView_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmployee = Nothing
    End Sub

    Private Sub frmFlexcubeErrorView_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Master"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFlexcubeErrorView_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmFlexcubeErrorView_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmployee = New clsEmployee_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFlexcubeFileGeneration_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            With cboRequestType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Customer Request"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Account Request"))
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Block User Request"))
                'S.SANDEEP |13-DEC-2019| -- START
                'ISSUE/ENHANCEMENT : FlexCube {Exit Block User}
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Block Exit Staff Request"))
                'S.SANDEEP |13-DEC-2019| -- END
                .SelectedIndex = 0
            End With
            With cboResponseType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 7, "Success"))
                .Items.Add(Language.getMessage(mstrModuleName, 8, "Failure"))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillEmployee()
        Dim dsData As New DataSet
        Dim strFilter As String = ""
        Try

            If cboRequestType.SelectedIndex <= 0 Then
                If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()
                Exit Sub
            End If

            If cboRequestType.SelectedIndex > 0 Then
                strFilter &= "AND FC.filetypeid = " & cboRequestType.SelectedIndex
            End If

            If mstrAdvanceFilter.Length > 0 Then
                strFilter &= "AND " & mstrAdvanceFilter & " "
            End If

            If cboResponseType.SelectedIndex = 1 Then
                strFilter &= "AND FC.iserror = 0 "
            ElseIf cboResponseType.SelectedIndex = 2 Then
                strFilter &= "AND FC.iserror = 1 "
            End If

            If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                strFilter &= "AND CONVERT(NVARCHAR(8),FC.requestdate,112) BETWEEN '" & eZeeDate.convertDate(dtpStartDate.Value).ToString() & "' AND '" & eZeeDate.convertDate(dtpEndDate.Value).ToString() & "' "
            End If

            If strFilter.Trim.Length > 0 Then
                strFilter = strFilter.Substring(3)
            End If

            dsData = objEmployee.GetFlexcubeErrorList(FinancialYear._Object._DatabaseName, _
                                                      User._Object._Userunkid, _
                                                      FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      ConfigParameter._Object._UserAccessModeSetting, _
                                                      True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                      "List", True, strFilter, True)

            mdtEmployee = dsData.Tables(0).Copy
            mvwEmployee = mdtEmployee.DefaultView
            dgvAEmployee.AutoGenerateColumns = False

            dgcolhUserId.DataPropertyName = "User Id"
            dgcolhNames.DataPropertyName = "Names"
            dgcolhLvFormNo.DataPropertyName = "Leave Form No"
            dgcolhLvStDate.DataPropertyName = "Leave Start Date"
            dgcolhLvRtDate.DataPropertyName = "Leave End Date"
            dgcolhReqDate.DataPropertyName = "Date"
            dgcolhRefNo.DataPropertyName = "Reference Numbers"
            dgcolhStatus.DataPropertyName = "Status"
            dgcolhFlexMsg.DataPropertyName = "Message"
            'S.SANDEEP |13-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : FlexCube {Exit Block User}
            dgcolhTermDate.DataPropertyName = "Exit Date"
            'S.SANDEEP |13-DEC-2019| -- END

            dgvAEmployee.DataSource = mvwEmployee
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ShowCount()
        Try
            Call objbtnSearch.ShowResult(CStr(dgvAEmployee.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboRequestType.SelectedIndex = 0
            mstrAdvanceFilter = ""
            ShowCount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillEmployee()
            ShowCount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportData.Click
        Try
            If dgvAEmployee.RowCount <= 0 Then Exit Sub

            Dim StrFilter As String = String.Empty
            StrFilter &= "," & Language.getMessage(mstrModuleName, 500, "Total Employee") & " : " & dgvAEmployee.RowCount.ToString

            If cboRequestType.SelectedIndex > 0 Then
                StrFilter &= "," & lblRequestType.Text & " : " & cboRequestType.Text
            End If

            If cboResponseType.SelectedIndex > 0 Then
                StrFilter &= "," & lblResponseType.Text & " : " & cboResponseType.Text
            End If

            If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                StrFilter &= "," & lblReqDateFrom.Text & " : " & dtpStartDate.Value.Date.ToShortDateString
                StrFilter &= "," & lblReqDateTo.Text & " : " & dtpEndDate.Value.Date.ToShortDateString
            End If

            'S.SANDEEP |13-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : FlexCube {Exit Block User}
            'Select Case cboRequestType.SelectedIndex
            '    Case 0, 1, 2
            '        mvwEmployee.Table.Columns.Remove(dgcolhUserId.DataPropertyName)
            '        mvwEmployee.Table.Columns.Remove(dgcolhLvFormNo.DataPropertyName)
            '        mvwEmployee.Table.Columns.Remove(dgcolhLvStDate.DataPropertyName)
            '        mvwEmployee.Table.Columns.Remove(dgcolhLvRtDate.DataPropertyName)
            'End Select

            If mvwEmployee.Table.Columns.Contains("filetypeid") Then
                mvwEmployee.Table.Columns.Remove("filetypeid")
            End If
            Select Case cboRequestType.SelectedIndex
                Case 0, 1, 2
                    mvwEmployee.Table.Columns.Remove(dgcolhUserId.DataPropertyName)
                    mvwEmployee.Table.Columns.Remove(dgcolhLvFormNo.DataPropertyName)
                    mvwEmployee.Table.Columns.Remove(dgcolhLvStDate.DataPropertyName)
                    mvwEmployee.Table.Columns.Remove(dgcolhLvRtDate.DataPropertyName)
                    mvwEmployee.Table.Columns.Remove(dgcolhTermDate.DataPropertyName)
                Case 3
                    mvwEmployee.Table.Columns.Remove(dgcolhTermDate.DataPropertyName)
                Case 4
                    mvwEmployee.Table.Columns.Remove(dgcolhUserId.DataPropertyName)
                    mvwEmployee.Table.Columns.Remove(dgcolhLvFormNo.DataPropertyName)
                    mvwEmployee.Table.Columns.Remove(dgcolhLvStDate.DataPropertyName)
                    mvwEmployee.Table.Columns.Remove(dgcolhLvRtDate.DataPropertyName)
            End Select
            'S.SANDEEP |13-DEC-2019| -- END
            

            If StrFilter.Trim.Length > 0 Then StrFilter = Mid(StrFilter, 2)
            Dim objEmpListing As New ArutiReports.clsEmployee_Listing(User._Object._Languageunkid, Company._Object._Companyunkid)
            If objEmpListing.ExportFlexData(StrFilter, mvwEmployee.ToTable(), enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, cboRequestType.Text) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "Employee List exported successfully to the given path."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportData_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                frm._Hr_EmployeeTable_Alias = "EM"
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " ComboBox Event(s) "

    Private Sub cboRequestType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRequestType.SelectedIndexChanged
        Try
            If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()
            dgvAEmployee.DataSource = Nothing
            'S.SANDEEP |13-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : FlexCube {Exit Block User}
            'Select Case cboRequestType.SelectedIndex
            '    Case 0, 1, 2
            '        dgcolhUserId.Visible = False
            '        dgcolhLvFormNo.Visible = False
            '        dgcolhLvStDate.Visible = False
            '        dgcolhLvRtDate.Visible = False
            '    Case Else
            '        dgcolhUserId.Visible = True
            '        dgcolhLvFormNo.Visible = True
            '        dgcolhLvStDate.Visible = True
            '        dgcolhLvRtDate.Visible = True
            'End Select
            Select Case cboRequestType.SelectedIndex
                Case 0, 1, 2
                    dgcolhUserId.Visible = False
                    dgcolhLvFormNo.Visible = False
                    dgcolhLvStDate.Visible = False
                    dgcolhLvRtDate.Visible = False
                    dgcolhTermDate.Visible = False
                Case 4
                    dgcolhUserId.Visible = False
                    dgcolhLvFormNo.Visible = False
                    dgcolhLvStDate.Visible = False
                    dgcolhLvRtDate.Visible = False
                    dgcolhTermDate.Visible = True
                Case Else
                    dgcolhUserId.Visible = True
                    dgcolhLvFormNo.Visible = True
                    dgcolhLvStDate.Visible = True
                    dgcolhLvRtDate.Visible = True
                    dgcolhTermDate.Visible = False
            End Select
            'S.SANDEEP |13-DEC-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboRequestType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If mvwEmployee IsNot Nothing Then
                Dim strSearch As String = ""
                If txtSearchEmp.Text.Trim.Length > 0 Then
                    strSearch = dgcolhUserId.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                                dgcolhNames.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                                dgcolhLvFormNo.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                                dgcolhRefNo.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                                dgcolhStatus.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                                dgcolhFlexMsg.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' "
                End If
                mvwEmployee.RowFilter = strSearch
                ShowCount()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilter_Criteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilter_Criteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnExportData.GradientBackColor = GUI._ButttonBackColor
            Me.btnExportData.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbEmployee.Text = Language._Object.getCaption(Me.gbEmployee.Name, Me.gbEmployee.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilter_Criteria.Text = Language._Object.getCaption(Me.gbFilter_Criteria.Name, Me.gbFilter_Criteria.Text)
            Me.lblRequestType.Text = Language._Object.getCaption(Me.lblRequestType.Name, Me.lblRequestType.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.lblResponseType.Text = Language._Object.getCaption(Me.lblResponseType.Name, Me.lblResponseType.Text)
            Me.btnExportData.Text = Language._Object.getCaption(Me.btnExportData.Name, Me.btnExportData.Text)
            Me.lblReqDateTo.Text = Language._Object.getCaption(Me.lblReqDateTo.Name, Me.lblReqDateTo.Text)
            Me.lblReqDateFrom.Text = Language._Object.getCaption(Me.lblReqDateFrom.Name, Me.lblReqDateFrom.Text)
            Me.dgcolhUserId.HeaderText = Language._Object.getCaption(Me.dgcolhUserId.Name, Me.dgcolhUserId.HeaderText)
            Me.dgcolhNames.HeaderText = Language._Object.getCaption(Me.dgcolhNames.Name, Me.dgcolhNames.HeaderText)
            Me.dgcolhLvFormNo.HeaderText = Language._Object.getCaption(Me.dgcolhLvFormNo.Name, Me.dgcolhLvFormNo.HeaderText)
            Me.dgcolhLvStDate.HeaderText = Language._Object.getCaption(Me.dgcolhLvStDate.Name, Me.dgcolhLvStDate.HeaderText)
            Me.dgcolhLvRtDate.HeaderText = Language._Object.getCaption(Me.dgcolhLvRtDate.Name, Me.dgcolhLvRtDate.HeaderText)
            Me.dgcolhReqDate.HeaderText = Language._Object.getCaption(Me.dgcolhReqDate.Name, Me.dgcolhReqDate.HeaderText)
            Me.dgcolhRefNo.HeaderText = Language._Object.getCaption(Me.dgcolhRefNo.Name, Me.dgcolhRefNo.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.dgcolhFlexMsg.HeaderText = Language._Object.getCaption(Me.dgcolhFlexMsg.Name, Me.dgcolhFlexMsg.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Customer")
            Language.setMessage(mstrModuleName, 3, "Account")
            Language.setMessage(mstrModuleName, 4, "BlockUser")
            Language.setMessage(mstrModuleName, 6, "Select")
            Language.setMessage(mstrModuleName, 7, "Success")
            Language.setMessage(mstrModuleName, 8, "Failure")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMembershipHead
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMembershipHead))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbHeadInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboEffectivePeriod = New System.Windows.Forms.ComboBox
        Me.lblEffectivePeriod = New System.Windows.Forms.Label
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.lblCoHead = New System.Windows.Forms.Label
        Me.txtCoHead = New System.Windows.Forms.TextBox
        Me.lblEmpHead = New System.Windows.Forms.Label
        Me.txtEmpHead = New System.Windows.Forms.TextBox
        Me.lblMembership = New System.Windows.Forms.Label
        Me.txtMembership = New System.Windows.Forms.TextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.pnlData = New System.Windows.Forms.Panel
        Me.chkOverwrite = New System.Windows.Forms.CheckBox
        Me.chkOverwritePrevEDSlabHeads = New System.Windows.Forms.CheckBox
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbHeadInformation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.pnlData.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbHeadInformation)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(414, 219)
        Me.pnlMain.TabIndex = 0
        '
        'gbHeadInformation
        '
        Me.gbHeadInformation.BorderColor = System.Drawing.Color.Black
        Me.gbHeadInformation.Checked = False
        Me.gbHeadInformation.CollapseAllExceptThis = False
        Me.gbHeadInformation.CollapsedHoverImage = Nothing
        Me.gbHeadInformation.CollapsedNormalImage = Nothing
        Me.gbHeadInformation.CollapsedPressedImage = Nothing
        Me.gbHeadInformation.CollapseOnLoad = False
        Me.gbHeadInformation.Controls.Add(Me.cboEffectivePeriod)
        Me.gbHeadInformation.Controls.Add(Me.lblEffectivePeriod)
        Me.gbHeadInformation.Controls.Add(Me.objelLine1)
        Me.gbHeadInformation.Controls.Add(Me.lblCoHead)
        Me.gbHeadInformation.Controls.Add(Me.txtCoHead)
        Me.gbHeadInformation.Controls.Add(Me.lblEmpHead)
        Me.gbHeadInformation.Controls.Add(Me.txtEmpHead)
        Me.gbHeadInformation.Controls.Add(Me.lblMembership)
        Me.gbHeadInformation.Controls.Add(Me.txtMembership)
        Me.gbHeadInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbHeadInformation.ExpandedHoverImage = Nothing
        Me.gbHeadInformation.ExpandedNormalImage = Nothing
        Me.gbHeadInformation.ExpandedPressedImage = Nothing
        Me.gbHeadInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHeadInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbHeadInformation.HeaderHeight = 25
        Me.gbHeadInformation.HeaderMessage = ""
        Me.gbHeadInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbHeadInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbHeadInformation.HeightOnCollapse = 0
        Me.gbHeadInformation.LeftTextSpace = 0
        Me.gbHeadInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbHeadInformation.Name = "gbHeadInformation"
        Me.gbHeadInformation.OpenHeight = 300
        Me.gbHeadInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbHeadInformation.ShowBorder = True
        Me.gbHeadInformation.ShowCheckBox = False
        Me.gbHeadInformation.ShowCollapseButton = False
        Me.gbHeadInformation.ShowDefaultBorderColor = True
        Me.gbHeadInformation.ShowDownButton = False
        Me.gbHeadInformation.ShowHeader = True
        Me.gbHeadInformation.Size = New System.Drawing.Size(414, 164)
        Me.gbHeadInformation.TabIndex = 3
        Me.gbHeadInformation.Temp = 0
        Me.gbHeadInformation.Text = "Head Information"
        Me.gbHeadInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEffectivePeriod
        '
        Me.cboEffectivePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEffectivePeriod.FormattingEnabled = True
        Me.cboEffectivePeriod.Location = New System.Drawing.Point(103, 133)
        Me.cboEffectivePeriod.Name = "cboEffectivePeriod"
        Me.cboEffectivePeriod.Size = New System.Drawing.Size(299, 21)
        Me.cboEffectivePeriod.TabIndex = 10
        '
        'lblEffectivePeriod
        '
        Me.lblEffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectivePeriod.Location = New System.Drawing.Point(8, 135)
        Me.lblEffectivePeriod.Name = "lblEffectivePeriod"
        Me.lblEffectivePeriod.Size = New System.Drawing.Size(88, 15)
        Me.lblEffectivePeriod.TabIndex = 9
        Me.lblEffectivePeriod.Text = "Effective Period"
        Me.lblEffectivePeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(11, 57)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(391, 16)
        Me.objelLine1.TabIndex = 7
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCoHead
        '
        Me.lblCoHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCoHead.Location = New System.Drawing.Point(8, 108)
        Me.lblCoHead.Name = "lblCoHead"
        Me.lblCoHead.Size = New System.Drawing.Size(88, 15)
        Me.lblCoHead.TabIndex = 6
        Me.lblCoHead.Text = "Co. Head"
        Me.lblCoHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCoHead
        '
        Me.txtCoHead.BackColor = System.Drawing.Color.White
        Me.txtCoHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCoHead.Location = New System.Drawing.Point(103, 105)
        Me.txtCoHead.Name = "txtCoHead"
        Me.txtCoHead.ReadOnly = True
        Me.txtCoHead.Size = New System.Drawing.Size(299, 21)
        Me.txtCoHead.TabIndex = 5
        '
        'lblEmpHead
        '
        Me.lblEmpHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpHead.Location = New System.Drawing.Point(8, 81)
        Me.lblEmpHead.Name = "lblEmpHead"
        Me.lblEmpHead.Size = New System.Drawing.Size(88, 15)
        Me.lblEmpHead.TabIndex = 4
        Me.lblEmpHead.Text = "Emp. Head"
        Me.lblEmpHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmpHead
        '
        Me.txtEmpHead.BackColor = System.Drawing.Color.White
        Me.txtEmpHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpHead.Location = New System.Drawing.Point(103, 78)
        Me.txtEmpHead.Name = "txtEmpHead"
        Me.txtEmpHead.ReadOnly = True
        Me.txtEmpHead.Size = New System.Drawing.Size(299, 21)
        Me.txtEmpHead.TabIndex = 3
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(8, 36)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(88, 15)
        Me.lblMembership.TabIndex = 1
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMembership
        '
        Me.txtMembership.BackColor = System.Drawing.Color.White
        Me.txtMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMembership.Location = New System.Drawing.Point(103, 33)
        Me.txtMembership.Name = "txtMembership"
        Me.txtMembership.ReadOnly = True
        Me.txtMembership.Size = New System.Drawing.Size(299, 21)
        Me.txtMembership.TabIndex = 2
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.pnlData)
        Me.objFooter.Controls.Add(Me.btnOk)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 164)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(414, 55)
        Me.objFooter.TabIndex = 2
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.chkOverwrite)
        Me.pnlData.Controls.Add(Me.chkOverwritePrevEDSlabHeads)
        Me.pnlData.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.pnlData.Location = New System.Drawing.Point(2, 2)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(293, 52)
        Me.pnlData.TabIndex = 242
        '
        'chkOverwrite
        '
        Me.chkOverwrite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwrite.Location = New System.Drawing.Point(158, 6)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.Size = New System.Drawing.Size(132, 17)
        Me.chkOverwrite.TabIndex = 244
        Me.chkOverwrite.Text = "Overwrite Heads"
        Me.chkOverwrite.UseVisualStyleBackColor = True
        '
        'chkOverwritePrevEDSlabHeads
        '
        Me.chkOverwritePrevEDSlabHeads.Enabled = False
        Me.chkOverwritePrevEDSlabHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwritePrevEDSlabHeads.Location = New System.Drawing.Point(8, 28)
        Me.chkOverwritePrevEDSlabHeads.Name = "chkOverwritePrevEDSlabHeads"
        Me.chkOverwritePrevEDSlabHeads.Size = New System.Drawing.Size(282, 17)
        Me.chkOverwritePrevEDSlabHeads.TabIndex = 243
        Me.chkOverwritePrevEDSlabHeads.Text = "Overwrite Previous ED Slab heads"
        Me.chkOverwritePrevEDSlabHeads.UseVisualStyleBackColor = True
        Me.chkOverwritePrevEDSlabHeads.Visible = False
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(8, 6)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(144, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 242
        Me.chkCopyPreviousEDSlab.Text = "Copy Previous ED Slab"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(301, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(101, 30)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(301, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(101, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'frmMembershipHead
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(414, 219)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMembershipHead"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assign Head "
        Me.pnlMain.ResumeLayout(False)
        Me.gbHeadInformation.ResumeLayout(False)
        Me.gbHeadInformation.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.pnlData.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents gbHeadInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents txtMembership As System.Windows.Forms.TextBox
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents lblCoHead As System.Windows.Forms.Label
    Friend WithEvents txtCoHead As System.Windows.Forms.TextBox
    Friend WithEvents lblEmpHead As System.Windows.Forms.Label
    Friend WithEvents txtEmpHead As System.Windows.Forms.TextBox
    Friend WithEvents cboEffectivePeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblEffectivePeriod As System.Windows.Forms.Label
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents chkOverwritePrevEDSlabHeads As System.Windows.Forms.CheckBox
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents chkOverwrite As System.Windows.Forms.CheckBox
End Class

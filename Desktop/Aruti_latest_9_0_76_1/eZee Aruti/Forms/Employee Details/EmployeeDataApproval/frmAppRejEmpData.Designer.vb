﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAppRejEmpData
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAppRejEmpData))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.txtApprover = New System.Windows.Forms.TextBox
        Me.txtLevel = New System.Windows.Forms.TextBox
        Me.lblLevel = New System.Windows.Forms.Label
        Me.lblLoggedInUser = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.objefemailFooter = New eZee.Common.eZeeFooter
        Me.lblotherqualificationnote = New System.Windows.Forms.Label
        Me.btnShowMyReport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReject = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.cboEmployeeData = New System.Windows.Forms.ComboBox
        Me.lblEmployeeData = New System.Windows.Forms.Label
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.picStayView = New System.Windows.Forms.PictureBox
        Me.lblSelectOperationType = New System.Windows.Forms.Label
        Me.cboOperationType = New System.Windows.Forms.ComboBox
        Me.cmDownloadAttachment = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.btndownloadallattachment = New System.Windows.Forms.ToolStripMenuItem
        Me.btndownloadnewattachment = New System.Windows.Forms.ToolStripMenuItem
        Me.btndownloaddeletedattachment = New System.Windows.Forms.ToolStripMenuItem
        Me.tblPanel = New System.Windows.Forms.TableLayoutPanel
        Me.pnlOperation = New System.Windows.Forms.Panel
        Me.radApplytoAll = New System.Windows.Forms.RadioButton
        Me.radApplytoChecked = New System.Windows.Forms.RadioButton
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.lnkSet = New System.Windows.Forms.LinkLabel
        Me.cboEffectivePeriod = New System.Windows.Forms.ComboBox
        Me.lblSetPeriod = New System.Windows.Forms.Label
        Me.pnlDetail = New System.Windows.Forms.Panel
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn36 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn38 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn39 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn40 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn41 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn42 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn43 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn44 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn45 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn46 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn47 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn48 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn49 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn50 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn51 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn52 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn53 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn54 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn55 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn56 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn57 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn58 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn59 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn60 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn61 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn62 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn63 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn64 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn65 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn66 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn67 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn68 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn69 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn70 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn71 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn72 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn73 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn74 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn75 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn76 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhicheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhDownloadDocument = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhSkillCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSkill = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhQualifyGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhQualification = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAwardDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAwardToDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInstitute = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCompany = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStartDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEndDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSupervisor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRefreeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTelNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMobile = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDBName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDBRelation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDBbirthdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDBIdentifyNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDBGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSrNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdentityNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdIssuePlace = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdIssueDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdExpiryDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIdDLClass = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddress1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddress2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFirstname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLastname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressState = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressCity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressZipCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressProvince = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressRoad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressEstate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressProvince1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressRoad1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressChiefdom = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressVillage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressTown = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressMobile = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressTel_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressPlotNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressAltNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAddressFax = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhmembershipname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhmembershipno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhperiod_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhissue_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhstart_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhexpiry_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMemRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthCity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthCertificateNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthTown1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthVillage1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthState = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthWard = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthVillage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthChiefdom = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhComplexion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBloodGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEyeColor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNationality = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEthinCity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReligion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHair = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMaritalStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhExtraTel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLanguage1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLanguage2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLanguage3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLanguage4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHeight = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhWeight = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMaritalDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAllergies = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDisabilities = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSportsHobbies = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInactiveReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhVoidReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhqualificationgroupunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhqualificationunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhqualificationtranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhformname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhnewattachdocumentid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhdeleteattachdocumentid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhdepedantunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhAddressTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objefemailFooter.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmDownloadAttachment.SuspendLayout()
        Me.tblPanel.SuspendLayout()
        Me.pnlOperation.SuspendLayout()
        Me.pnlDetail.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.txtApprover)
        Me.gbFilterCriteria.Controls.Add(Me.txtLevel)
        Me.gbFilterCriteria.Controls.Add(Me.lblLevel)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoggedInUser)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 100
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(792, 65)
        Me.gbFilterCriteria.TabIndex = 2
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(656, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 14)
        Me.lnkAllocation.TabIndex = 142
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'txtApprover
        '
        Me.txtApprover.BackColor = System.Drawing.SystemColors.Info
        Me.txtApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApprover.Location = New System.Drawing.Point(417, 34)
        Me.txtApprover.Name = "txtApprover"
        Me.txtApprover.ReadOnly = True
        Me.txtApprover.Size = New System.Drawing.Size(192, 21)
        Me.txtApprover.TabIndex = 109
        '
        'txtLevel
        '
        Me.txtLevel.BackColor = System.Drawing.SystemColors.Info
        Me.txtLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLevel.Location = New System.Drawing.Point(667, 34)
        Me.txtLevel.Name = "txtLevel"
        Me.txtLevel.ReadOnly = True
        Me.txtLevel.Size = New System.Drawing.Size(113, 21)
        Me.txtLevel.TabIndex = 1
        '
        'lblLevel
        '
        Me.lblLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevel.Location = New System.Drawing.Point(615, 37)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(46, 15)
        Me.lblLevel.TabIndex = 108
        Me.lblLevel.Text = "Level"
        '
        'lblLoggedInUser
        '
        Me.lblLoggedInUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoggedInUser.Location = New System.Drawing.Point(346, 37)
        Me.lblLoggedInUser.Name = "lblLoggedInUser"
        Me.lblLoggedInUser.Size = New System.Drawing.Size(65, 15)
        Me.lblLoggedInUser.TabIndex = 107
        Me.lblLoggedInUser.Text = "Approver"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(319, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 105
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(77, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(236, 21)
        Me.cboEmployee.TabIndex = 104
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(10, 37)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(61, 15)
        Me.lblEmployee.TabIndex = 103
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnReset
        '
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(767, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 95
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(743, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 94
        Me.objbtnSearch.TabStop = False
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(25, 383)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(68, 15)
        Me.lblRemark.TabIndex = 106
        Me.lblRemark.Text = "Remark"
        '
        'txtRemark
        '
        Me.txtRemark.Location = New System.Drawing.Point(99, 380)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(705, 45)
        Me.txtRemark.TabIndex = 105
        '
        'objefemailFooter
        '
        Me.objefemailFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefemailFooter.Controls.Add(Me.lblotherqualificationnote)
        Me.objefemailFooter.Controls.Add(Me.btnShowMyReport)
        Me.objefemailFooter.Controls.Add(Me.btnApprove)
        Me.objefemailFooter.Controls.Add(Me.btnReject)
        Me.objefemailFooter.Controls.Add(Me.btnClose)
        Me.objefemailFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefemailFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefemailFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefemailFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objefemailFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objefemailFooter.Location = New System.Drawing.Point(0, 431)
        Me.objefemailFooter.Name = "objefemailFooter"
        Me.objefemailFooter.Size = New System.Drawing.Size(816, 55)
        Me.objefemailFooter.TabIndex = 107
        '
        'lblotherqualificationnote
        '
        Me.lblotherqualificationnote.AutoSize = True
        Me.lblotherqualificationnote.ForeColor = System.Drawing.Color.Blue
        Me.lblotherqualificationnote.Location = New System.Drawing.Point(312, 22)
        Me.lblotherqualificationnote.Name = "lblotherqualificationnote"
        Me.lblotherqualificationnote.Size = New System.Drawing.Size(216, 13)
        Me.lblotherqualificationnote.TabIndex = 87
        Me.lblotherqualificationnote.Text = "Note: This color indicates other qualification"
        '
        'btnShowMyReport
        '
        Me.btnShowMyReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnShowMyReport.BackColor = System.Drawing.Color.White
        Me.btnShowMyReport.BackgroundImage = CType(resources.GetObject("btnShowMyReport.BackgroundImage"), System.Drawing.Image)
        Me.btnShowMyReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnShowMyReport.BorderColor = System.Drawing.Color.Empty
        Me.btnShowMyReport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnShowMyReport.Enabled = False
        Me.btnShowMyReport.FlatAppearance.BorderSize = 0
        Me.btnShowMyReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnShowMyReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShowMyReport.ForeColor = System.Drawing.Color.Black
        Me.btnShowMyReport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnShowMyReport.GradientForeColor = System.Drawing.Color.Black
        Me.btnShowMyReport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnShowMyReport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnShowMyReport.Location = New System.Drawing.Point(212, 13)
        Me.btnShowMyReport.Name = "btnShowMyReport"
        Me.btnShowMyReport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnShowMyReport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnShowMyReport.Size = New System.Drawing.Size(94, 30)
        Me.btnShowMyReport.TabIndex = 86
        Me.btnShowMyReport.Text = "&My Report"
        Me.btnShowMyReport.UseVisualStyleBackColor = True
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(12, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(94, 30)
        Me.btnApprove.TabIndex = 85
        Me.btnApprove.Text = "&Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        '
        'btnReject
        '
        Me.btnReject.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReject.BackColor = System.Drawing.Color.White
        Me.btnReject.BackgroundImage = CType(resources.GetObject("btnReject.BackgroundImage"), System.Drawing.Image)
        Me.btnReject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReject.BorderColor = System.Drawing.Color.Empty
        Me.btnReject.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReject.FlatAppearance.BorderSize = 0
        Me.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReject.ForeColor = System.Drawing.Color.Black
        Me.btnReject.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReject.GradientForeColor = System.Drawing.Color.Black
        Me.btnReject.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReject.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReject.Location = New System.Drawing.Point(112, 13)
        Me.btnReject.Name = "btnReject"
        Me.btnReject.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReject.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReject.Size = New System.Drawing.Size(94, 30)
        Me.btnReject.TabIndex = 85
        Me.btnReject.Text = "&Reject"
        Me.btnReject.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(710, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 85
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(7, 6)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 108
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhicheck, Me.objdgcolhDownloadDocument, Me.dgcolhSkillCategory, Me.dgcolhSkill, Me.dgcolhDescription, Me.dgcolhQualifyGroup, Me.dgcolhQualification, Me.dgcolhAwardDate, Me.dgcolhAwardToDate, Me.dgcolhInstitute, Me.dgcolhRefNo, Me.dgcolhCompany, Me.dgcolhJob, Me.dgcolhStartDate, Me.dgcolhEndDate, Me.dgcolhSupervisor, Me.dgcolhRemark, Me.dgcolhRefreeName, Me.dgcolhCountry, Me.dgcolhIdNo, Me.dgcolhEmail, Me.dgcolhTelNo, Me.dgcolhMobile, Me.dgcolhDBName, Me.dgcolhDBRelation, Me.dgcolhDBbirthdate, Me.dgcolhDBIdentifyNo, Me.dgcolhDBGender, Me.dgcolhIdType, Me.dgcolhSrNo, Me.dgcolhIdentityNo, Me.dgcolhIdCountry, Me.dgcolhIdIssuePlace, Me.dgcolhIdIssueDate, Me.dgcolhIdExpiryDate, Me.dgcolhIdDLClass, Me.dgcolhAddressType, Me.dgcolhAddress1, Me.dgcolhAddress2, Me.dgcolhFirstname, Me.dgcolhLastname, Me.dgcolhAddressCountry, Me.dgcolhAddressState, Me.dgcolhAddressCity, Me.dgcolhAddressZipCode, Me.dgcolhAddressProvince, Me.dgcolhAddressRoad, Me.dgcolhAddressEstate, Me.dgcolhAddressProvince1, Me.dgcolhAddressRoad1, Me.dgcolhAddressChiefdom, Me.dgcolhAddressVillage, Me.dgcolhAddressTown, Me.dgcolhAddressMobile, Me.dgcolhAddressTel_no, Me.dgcolhAddressPlotNo, Me.dgcolhAddressAltNo, Me.dgcolhAddressEmail, Me.dgcolhAddressFax, Me.dgcolhCategory, Me.dgcolhmembershipname, Me.dgcolhmembershipno, Me.dgcolhperiod_name, Me.dgcolhissue_date, Me.dgcolhstart_date, Me.dgcolhexpiry_date, Me.dgcolhMemRemark, Me.dgcolhBirthCountry, Me.dgcolhBirthCity, Me.dgcolhBirthCertificateNo, Me.dgcolhBirthTown1, Me.dgcolhBirthVillage1, Me.dgcolhBirthState, Me.dgcolhBirthWard, Me.dgcolhBirthVillage, Me.dgcolhBirthChiefdom, Me.dgcolhComplexion, Me.dgcolhBloodGroup, Me.dgcolhEyeColor, Me.dgcolhNationality, Me.dgcolhEthinCity, Me.dgcolhReligion, Me.dgcolhHair, Me.dgcolhMaritalStatus, Me.dgcolhExtraTel, Me.dgcolhLanguage1, Me.dgcolhLanguage2, Me.dgcolhLanguage3, Me.dgcolhLanguage4, Me.dgcolhHeight, Me.dgcolhWeight, Me.dgcolhMaritalDate, Me.dgcolhAllergies, Me.dgcolhDisabilities, Me.dgcolhSportsHobbies, Me.dgcolhInactiveReason, Me.dgcolhVoidReason, Me.objdgcolhIsGrp, Me.objdgcolhEmpid, Me.objcolhqualificationgroupunkid, Me.objcolhqualificationunkid, Me.objcolhqualificationtranunkid, Me.objcolhformname, Me.objcolhnewattachdocumentid, Me.objcolhdeleteattachdocumentid, Me.objcolhdepedantunkid, Me.objcolhAddressTypeId})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.Size = New System.Drawing.Size(786, 255)
        Me.dgvData.TabIndex = 109
        '
        'cboEmployeeData
        '
        Me.cboEmployeeData.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeData.DropDownWidth = 400
        Me.cboEmployeeData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeData.FormattingEnabled = True
        Me.cboEmployeeData.Location = New System.Drawing.Point(359, 83)
        Me.cboEmployeeData.Name = "cboEmployeeData"
        Me.cboEmployeeData.Size = New System.Drawing.Size(212, 21)
        Me.cboEmployeeData.TabIndex = 110
        '
        'lblEmployeeData
        '
        Me.lblEmployeeData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeData.Location = New System.Drawing.Point(277, 86)
        Me.lblEmployeeData.Name = "lblEmployeeData"
        Me.lblEmployeeData.Size = New System.Drawing.Size(76, 15)
        Me.lblEmployeeData.TabIndex = 113
        Me.lblEmployeeData.Text = "Select Data"
        '
        'txtSearch
        '
        Me.txtSearch.Location = New System.Drawing.Point(12, 83)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(259, 21)
        Me.txtSearch.TabIndex = 110
        '
        'picStayView
        '
        Me.picStayView.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.picStayView.Location = New System.Drawing.Point(647, 24)
        Me.picStayView.Name = "picStayView"
        Me.picStayView.Size = New System.Drawing.Size(23, 23)
        Me.picStayView.TabIndex = 114
        Me.picStayView.TabStop = False
        Me.picStayView.Visible = False
        '
        'lblSelectOperationType
        '
        Me.lblSelectOperationType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectOperationType.Location = New System.Drawing.Point(577, 86)
        Me.lblSelectOperationType.Name = "lblSelectOperationType"
        Me.lblSelectOperationType.Size = New System.Drawing.Size(90, 15)
        Me.lblSelectOperationType.TabIndex = 117
        Me.lblSelectOperationType.Text = "Operation Type"
        '
        'cboOperationType
        '
        Me.cboOperationType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOperationType.DropDownWidth = 400
        Me.cboOperationType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOperationType.FormattingEnabled = True
        Me.cboOperationType.Location = New System.Drawing.Point(673, 83)
        Me.cboOperationType.Name = "cboOperationType"
        Me.cboOperationType.Size = New System.Drawing.Size(131, 21)
        Me.cboOperationType.TabIndex = 116
        '
        'cmDownloadAttachment
        '
        Me.cmDownloadAttachment.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btndownloadallattachment, Me.btndownloadnewattachment, Me.btndownloaddeletedattachment})
        Me.cmDownloadAttachment.Name = "ContextMenuStrip1"
        Me.cmDownloadAttachment.Size = New System.Drawing.Size(174, 70)
        '
        'btndownloadallattachment
        '
        Me.btndownloadallattachment.Name = "btndownloadallattachment"
        Me.btndownloadallattachment.Size = New System.Drawing.Size(173, 22)
        Me.btndownloadallattachment.Text = "All Document"
        '
        'btndownloadnewattachment
        '
        Me.btndownloadnewattachment.Name = "btndownloadnewattachment"
        Me.btndownloadnewattachment.Size = New System.Drawing.Size(173, 22)
        Me.btndownloadnewattachment.Text = "Newly Added"
        '
        'btndownloaddeletedattachment
        '
        Me.btndownloaddeletedattachment.Name = "btndownloaddeletedattachment"
        Me.btndownloaddeletedattachment.Size = New System.Drawing.Size(173, 22)
        Me.btndownloaddeletedattachment.Text = "Deleted Document"
        '
        'tblPanel
        '
        Me.tblPanel.ColumnCount = 1
        Me.tblPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblPanel.Controls.Add(Me.pnlOperation, 0, 0)
        Me.tblPanel.Controls.Add(Me.pnlDetail, 0, 1)
        Me.tblPanel.Location = New System.Drawing.Point(12, 111)
        Me.tblPanel.Name = "tblPanel"
        Me.tblPanel.RowCount = 2
        Me.tblPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2.0!))
        Me.tblPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblPanel.Size = New System.Drawing.Size(792, 263)
        Me.tblPanel.TabIndex = 119
        '
        'pnlOperation
        '
        Me.pnlOperation.Controls.Add(Me.radApplytoAll)
        Me.pnlOperation.Controls.Add(Me.radApplytoChecked)
        Me.pnlOperation.Controls.Add(Me.EZeeStraightLine2)
        Me.pnlOperation.Controls.Add(Me.lnkSet)
        Me.pnlOperation.Controls.Add(Me.cboEffectivePeriod)
        Me.pnlOperation.Controls.Add(Me.lblSetPeriod)
        Me.pnlOperation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlOperation.Location = New System.Drawing.Point(3, 3)
        Me.pnlOperation.Name = "pnlOperation"
        Me.pnlOperation.Size = New System.Drawing.Size(786, 1)
        Me.pnlOperation.TabIndex = 121
        '
        'radApplytoAll
        '
        Me.radApplytoAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radApplytoAll.BackColor = System.Drawing.Color.Transparent
        Me.radApplytoAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoAll.Location = New System.Drawing.Point(433, 5)
        Me.radApplytoAll.Name = "radApplytoAll"
        Me.radApplytoAll.Size = New System.Drawing.Size(102, 17)
        Me.radApplytoAll.TabIndex = 116
        Me.radApplytoAll.TabStop = True
        Me.radApplytoAll.Text = "Apply to All"
        Me.radApplytoAll.UseVisualStyleBackColor = False
        '
        'radApplytoChecked
        '
        Me.radApplytoChecked.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radApplytoChecked.BackColor = System.Drawing.Color.Transparent
        Me.radApplytoChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoChecked.Location = New System.Drawing.Point(541, 5)
        Me.radApplytoChecked.Name = "radApplytoChecked"
        Me.radApplytoChecked.Size = New System.Drawing.Size(129, 17)
        Me.radApplytoChecked.TabIndex = 117
        Me.radApplytoChecked.TabStop = True
        Me.radApplytoChecked.Text = "Apply To Checked"
        Me.radApplytoChecked.UseVisualStyleBackColor = False
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(676, 5)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(5, 17)
        Me.EZeeStraightLine2.TabIndex = 118
        '
        'lnkSet
        '
        Me.lnkSet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSet.BackColor = System.Drawing.Color.Transparent
        Me.lnkSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSet.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSet.Location = New System.Drawing.Point(689, 5)
        Me.lnkSet.Name = "lnkSet"
        Me.lnkSet.Size = New System.Drawing.Size(77, 17)
        Me.lnkSet.TabIndex = 119
        Me.lnkSet.TabStop = True
        Me.lnkSet.Text = "[ SET ]"
        Me.lnkSet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboEffectivePeriod
        '
        Me.cboEffectivePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEffectivePeriod.DropDownWidth = 400
        Me.cboEffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEffectivePeriod.FormattingEnabled = True
        Me.cboEffectivePeriod.Location = New System.Drawing.Point(107, 3)
        Me.cboEffectivePeriod.Name = "cboEffectivePeriod"
        Me.cboEffectivePeriod.Size = New System.Drawing.Size(212, 21)
        Me.cboEffectivePeriod.TabIndex = 114
        '
        'lblSetPeriod
        '
        Me.lblSetPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSetPeriod.Location = New System.Drawing.Point(6, 6)
        Me.lblSetPeriod.Name = "lblSetPeriod"
        Me.lblSetPeriod.Size = New System.Drawing.Size(95, 15)
        Me.lblSetPeriod.TabIndex = 115
        Me.lblSetPeriod.Text = "Effective Period"
        '
        'pnlDetail
        '
        Me.pnlDetail.Controls.Add(Me.objchkAll)
        Me.pnlDetail.Controls.Add(Me.dgvData)
        Me.pnlDetail.Controls.Add(Me.picStayView)
        Me.pnlDetail.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlDetail.Location = New System.Drawing.Point(3, 5)
        Me.pnlDetail.Name = "pnlDetail"
        Me.pnlDetail.Size = New System.Drawing.Size(786, 255)
        Me.pnlDetail.TabIndex = 122
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Skill Category"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Skill"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Description"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Qualification Group"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Qualification"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Award Date"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Award To Date"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Institute"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Ref. No."
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Company"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Job"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Start Date"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "End Date"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Supervisor"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "Refree Name"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Country"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "Id. No."
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "Tel. No."
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "Mobile"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.Visible = False
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "objdgcolhEmpid"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.Visible = False
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.HeaderText = "objcolhqualificationgroupunkid"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.Visible = False
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.HeaderText = "objcolhqualificationunkid"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.Visible = False
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.HeaderText = "Gender"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.ReadOnly = True
        Me.DataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn27.Visible = False
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.HeaderText = "objdgcolhEmpid"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = True
        Me.DataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn28.Visible = False
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.HeaderText = "objcolhqualificationgroupunkid"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = True
        Me.DataGridViewTextBoxColumn29.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn29.Visible = False
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.HeaderText = "objcolhqualificationunkid"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.ReadOnly = True
        Me.DataGridViewTextBoxColumn30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn30.Visible = False
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.HeaderText = "objcolhlinkedmasterid"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.ReadOnly = True
        Me.DataGridViewTextBoxColumn31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn31.Visible = False
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.HeaderText = "objcolhformname"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.ReadOnly = True
        Me.DataGridViewTextBoxColumn32.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn32.Visible = False
        Me.DataGridViewTextBoxColumn32.Width = 5
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.HeaderText = "objcolhnewattachdocumentid"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.ReadOnly = True
        Me.DataGridViewTextBoxColumn33.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn33.Visible = False
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.HeaderText = "objcolhdeleteattachdocumentid"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.ReadOnly = True
        Me.DataGridViewTextBoxColumn34.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn34.Visible = False
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.HeaderText = "objcolhdepedantunkid"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        Me.DataGridViewTextBoxColumn35.Visible = False
        '
        'DataGridViewTextBoxColumn36
        '
        Me.DataGridViewTextBoxColumn36.HeaderText = "Address1"
        Me.DataGridViewTextBoxColumn36.Name = "DataGridViewTextBoxColumn36"
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.HeaderText = "Address2"
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        '
        'DataGridViewTextBoxColumn38
        '
        Me.DataGridViewTextBoxColumn38.HeaderText = "First Name"
        Me.DataGridViewTextBoxColumn38.Name = "DataGridViewTextBoxColumn38"
        '
        'DataGridViewTextBoxColumn39
        '
        Me.DataGridViewTextBoxColumn39.HeaderText = "Last Name"
        Me.DataGridViewTextBoxColumn39.Name = "DataGridViewTextBoxColumn39"
        '
        'DataGridViewTextBoxColumn40
        '
        Me.DataGridViewTextBoxColumn40.HeaderText = "Country"
        Me.DataGridViewTextBoxColumn40.Name = "DataGridViewTextBoxColumn40"
        '
        'DataGridViewTextBoxColumn41
        '
        Me.DataGridViewTextBoxColumn41.HeaderText = "State"
        Me.DataGridViewTextBoxColumn41.Name = "DataGridViewTextBoxColumn41"
        '
        'DataGridViewTextBoxColumn42
        '
        Me.DataGridViewTextBoxColumn42.HeaderText = "City"
        Me.DataGridViewTextBoxColumn42.Name = "DataGridViewTextBoxColumn42"
        '
        'DataGridViewTextBoxColumn43
        '
        Me.DataGridViewTextBoxColumn43.HeaderText = "ZipCode"
        Me.DataGridViewTextBoxColumn43.Name = "DataGridViewTextBoxColumn43"
        '
        'DataGridViewTextBoxColumn44
        '
        Me.DataGridViewTextBoxColumn44.HeaderText = "Province"
        Me.DataGridViewTextBoxColumn44.Name = "DataGridViewTextBoxColumn44"
        '
        'DataGridViewTextBoxColumn45
        '
        Me.DataGridViewTextBoxColumn45.HeaderText = "Road"
        Me.DataGridViewTextBoxColumn45.Name = "DataGridViewTextBoxColumn45"
        '
        'DataGridViewTextBoxColumn46
        '
        Me.DataGridViewTextBoxColumn46.HeaderText = "Estate"
        Me.DataGridViewTextBoxColumn46.Name = "DataGridViewTextBoxColumn46"
        '
        'DataGridViewTextBoxColumn47
        '
        Me.DataGridViewTextBoxColumn47.HeaderText = "Province1"
        Me.DataGridViewTextBoxColumn47.Name = "DataGridViewTextBoxColumn47"
        '
        'DataGridViewTextBoxColumn48
        '
        Me.DataGridViewTextBoxColumn48.HeaderText = "Road1"
        Me.DataGridViewTextBoxColumn48.Name = "DataGridViewTextBoxColumn48"
        '
        'DataGridViewTextBoxColumn49
        '
        Me.DataGridViewTextBoxColumn49.HeaderText = "Chiefdom"
        Me.DataGridViewTextBoxColumn49.Name = "DataGridViewTextBoxColumn49"
        '
        'DataGridViewTextBoxColumn50
        '
        Me.DataGridViewTextBoxColumn50.HeaderText = "Village"
        Me.DataGridViewTextBoxColumn50.Name = "DataGridViewTextBoxColumn50"
        '
        'DataGridViewTextBoxColumn51
        '
        Me.DataGridViewTextBoxColumn51.HeaderText = "Town"
        Me.DataGridViewTextBoxColumn51.Name = "DataGridViewTextBoxColumn51"
        '
        'DataGridViewTextBoxColumn52
        '
        Me.DataGridViewTextBoxColumn52.HeaderText = "Mobile"
        Me.DataGridViewTextBoxColumn52.Name = "DataGridViewTextBoxColumn52"
        '
        'DataGridViewTextBoxColumn53
        '
        Me.DataGridViewTextBoxColumn53.HeaderText = "Telephone Number"
        Me.DataGridViewTextBoxColumn53.Name = "DataGridViewTextBoxColumn53"
        '
        'DataGridViewTextBoxColumn54
        '
        Me.DataGridViewTextBoxColumn54.HeaderText = "Plot Number"
        Me.DataGridViewTextBoxColumn54.Name = "DataGridViewTextBoxColumn54"
        '
        'DataGridViewTextBoxColumn55
        '
        Me.DataGridViewTextBoxColumn55.HeaderText = "Alternate Number"
        Me.DataGridViewTextBoxColumn55.Name = "DataGridViewTextBoxColumn55"
        '
        'DataGridViewTextBoxColumn56
        '
        Me.DataGridViewTextBoxColumn56.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn56.Name = "DataGridViewTextBoxColumn56"
        '
        'DataGridViewTextBoxColumn57
        '
        Me.DataGridViewTextBoxColumn57.HeaderText = "Fax"
        Me.DataGridViewTextBoxColumn57.Name = "DataGridViewTextBoxColumn57"
        '
        'DataGridViewTextBoxColumn58
        '
        Me.DataGridViewTextBoxColumn58.HeaderText = "Membership Category"
        Me.DataGridViewTextBoxColumn58.Name = "DataGridViewTextBoxColumn58"
        Me.DataGridViewTextBoxColumn58.ReadOnly = True
        Me.DataGridViewTextBoxColumn58.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn59
        '
        Me.DataGridViewTextBoxColumn59.HeaderText = "Membership Name"
        Me.DataGridViewTextBoxColumn59.Name = "DataGridViewTextBoxColumn59"
        Me.DataGridViewTextBoxColumn59.ReadOnly = True
        Me.DataGridViewTextBoxColumn59.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn60
        '
        Me.DataGridViewTextBoxColumn60.HeaderText = "Membership No."
        Me.DataGridViewTextBoxColumn60.Name = "DataGridViewTextBoxColumn60"
        Me.DataGridViewTextBoxColumn60.ReadOnly = True
        Me.DataGridViewTextBoxColumn60.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn61
        '
        Me.DataGridViewTextBoxColumn61.HeaderText = "Period Name"
        Me.DataGridViewTextBoxColumn61.Name = "DataGridViewTextBoxColumn61"
        Me.DataGridViewTextBoxColumn61.ReadOnly = True
        Me.DataGridViewTextBoxColumn61.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn62
        '
        Me.DataGridViewTextBoxColumn62.HeaderText = "Issue Date"
        Me.DataGridViewTextBoxColumn62.Name = "DataGridViewTextBoxColumn62"
        Me.DataGridViewTextBoxColumn62.ReadOnly = True
        Me.DataGridViewTextBoxColumn62.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn63
        '
        Me.DataGridViewTextBoxColumn63.HeaderText = "Start Date"
        Me.DataGridViewTextBoxColumn63.Name = "DataGridViewTextBoxColumn63"
        Me.DataGridViewTextBoxColumn63.ReadOnly = True
        Me.DataGridViewTextBoxColumn63.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn64
        '
        Me.DataGridViewTextBoxColumn64.HeaderText = "Expiry Date"
        Me.DataGridViewTextBoxColumn64.Name = "DataGridViewTextBoxColumn64"
        Me.DataGridViewTextBoxColumn64.ReadOnly = True
        Me.DataGridViewTextBoxColumn64.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn65
        '
        Me.DataGridViewTextBoxColumn65.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn65.Name = "DataGridViewTextBoxColumn65"
        Me.DataGridViewTextBoxColumn65.ReadOnly = True
        Me.DataGridViewTextBoxColumn65.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn66
        '
        Me.DataGridViewTextBoxColumn66.HeaderText = "objdgcolhMemnewattachdocumnetid"
        Me.DataGridViewTextBoxColumn66.Name = "DataGridViewTextBoxColumn66"
        Me.DataGridViewTextBoxColumn66.ReadOnly = True
        Me.DataGridViewTextBoxColumn66.Visible = False
        '
        'DataGridViewTextBoxColumn67
        '
        Me.DataGridViewTextBoxColumn67.HeaderText = "objdgcolhMemdeleteattachdocumnetid"
        Me.DataGridViewTextBoxColumn67.Name = "DataGridViewTextBoxColumn67"
        Me.DataGridViewTextBoxColumn67.ReadOnly = True
        Me.DataGridViewTextBoxColumn67.Visible = False
        '
        'DataGridViewTextBoxColumn68
        '
        Me.DataGridViewTextBoxColumn68.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn68.Name = "DataGridViewTextBoxColumn68"
        Me.DataGridViewTextBoxColumn68.Visible = False
        '
        'DataGridViewTextBoxColumn69
        '
        Me.DataGridViewTextBoxColumn69.HeaderText = "objdgcolhEmpid"
        Me.DataGridViewTextBoxColumn69.Name = "DataGridViewTextBoxColumn69"
        Me.DataGridViewTextBoxColumn69.Visible = False
        '
        'DataGridViewTextBoxColumn70
        '
        Me.DataGridViewTextBoxColumn70.HeaderText = "objcolhqualificationgroupunkid"
        Me.DataGridViewTextBoxColumn70.Name = "DataGridViewTextBoxColumn70"
        Me.DataGridViewTextBoxColumn70.Visible = False
        '
        'DataGridViewTextBoxColumn71
        '
        Me.DataGridViewTextBoxColumn71.HeaderText = "objcolhqualificationunkid"
        Me.DataGridViewTextBoxColumn71.Name = "DataGridViewTextBoxColumn71"
        Me.DataGridViewTextBoxColumn71.Visible = False
        '
        'DataGridViewTextBoxColumn72
        '
        Me.DataGridViewTextBoxColumn72.HeaderText = "objcolhqualificationtranunkid"
        Me.DataGridViewTextBoxColumn72.Name = "DataGridViewTextBoxColumn72"
        Me.DataGridViewTextBoxColumn72.Visible = False
        '
        'DataGridViewTextBoxColumn73
        '
        Me.DataGridViewTextBoxColumn73.HeaderText = "objcolhformname"
        Me.DataGridViewTextBoxColumn73.Name = "DataGridViewTextBoxColumn73"
        Me.DataGridViewTextBoxColumn73.Visible = False
        Me.DataGridViewTextBoxColumn73.Width = 5
        '
        'DataGridViewTextBoxColumn74
        '
        Me.DataGridViewTextBoxColumn74.HeaderText = "objcolhnewattachdocumentid"
        Me.DataGridViewTextBoxColumn74.Name = "DataGridViewTextBoxColumn74"
        Me.DataGridViewTextBoxColumn74.Visible = False
        '
        'DataGridViewTextBoxColumn75
        '
        Me.DataGridViewTextBoxColumn75.HeaderText = "objcolhdeleteattachdocumentid"
        Me.DataGridViewTextBoxColumn75.Name = "DataGridViewTextBoxColumn75"
        Me.DataGridViewTextBoxColumn75.Visible = False
        '
        'DataGridViewTextBoxColumn76
        '
        Me.DataGridViewTextBoxColumn76.HeaderText = "objcolhdepedantunkid"
        Me.DataGridViewTextBoxColumn76.Name = "DataGridViewTextBoxColumn76"
        Me.DataGridViewTextBoxColumn76.Visible = False
        '
        'objdgcolhicheck
        '
        Me.objdgcolhicheck.Frozen = True
        Me.objdgcolhicheck.HeaderText = ""
        Me.objdgcolhicheck.Name = "objdgcolhicheck"
        Me.objdgcolhicheck.Width = 25
        '
        'objdgcolhDownloadDocument
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle1.NullValue = Nothing
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Transparent
        Me.objdgcolhDownloadDocument.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhDownloadDocument.Frozen = True
        Me.objdgcolhDownloadDocument.HeaderText = ""
        Me.objdgcolhDownloadDocument.Name = "objdgcolhDownloadDocument"
        Me.objdgcolhDownloadDocument.Width = 25
        '
        'dgcolhSkillCategory
        '
        Me.dgcolhSkillCategory.HeaderText = "Skill Category"
        Me.dgcolhSkillCategory.Name = "dgcolhSkillCategory"
        Me.dgcolhSkillCategory.ReadOnly = True
        Me.dgcolhSkillCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSkill
        '
        Me.dgcolhSkill.HeaderText = "Skill"
        Me.dgcolhSkill.Name = "dgcolhSkill"
        Me.dgcolhSkill.ReadOnly = True
        Me.dgcolhSkill.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDescription
        '
        Me.dgcolhDescription.HeaderText = "Description"
        Me.dgcolhDescription.Name = "dgcolhDescription"
        Me.dgcolhDescription.ReadOnly = True
        Me.dgcolhDescription.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhQualifyGroup
        '
        Me.dgcolhQualifyGroup.HeaderText = "Qualification Group"
        Me.dgcolhQualifyGroup.Name = "dgcolhQualifyGroup"
        Me.dgcolhQualifyGroup.ReadOnly = True
        Me.dgcolhQualifyGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhQualification
        '
        Me.dgcolhQualification.HeaderText = "Qualification"
        Me.dgcolhQualification.Name = "dgcolhQualification"
        Me.dgcolhQualification.ReadOnly = True
        Me.dgcolhQualification.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAwardDate
        '
        Me.dgcolhAwardDate.HeaderText = "Award Date"
        Me.dgcolhAwardDate.Name = "dgcolhAwardDate"
        Me.dgcolhAwardDate.ReadOnly = True
        Me.dgcolhAwardDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAwardToDate
        '
        Me.dgcolhAwardToDate.HeaderText = "Award To Date"
        Me.dgcolhAwardToDate.Name = "dgcolhAwardToDate"
        Me.dgcolhAwardToDate.ReadOnly = True
        Me.dgcolhAwardToDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhInstitute
        '
        Me.dgcolhInstitute.HeaderText = "Institute"
        Me.dgcolhInstitute.Name = "dgcolhInstitute"
        Me.dgcolhInstitute.ReadOnly = True
        Me.dgcolhInstitute.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhRefNo
        '
        Me.dgcolhRefNo.HeaderText = "Ref. No."
        Me.dgcolhRefNo.Name = "dgcolhRefNo"
        Me.dgcolhRefNo.ReadOnly = True
        Me.dgcolhRefNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCompany
        '
        Me.dgcolhCompany.HeaderText = "Company"
        Me.dgcolhCompany.Name = "dgcolhCompany"
        Me.dgcolhCompany.ReadOnly = True
        Me.dgcolhCompany.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhJob
        '
        Me.dgcolhJob.HeaderText = "Job"
        Me.dgcolhJob.Name = "dgcolhJob"
        Me.dgcolhJob.ReadOnly = True
        Me.dgcolhJob.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhStartDate
        '
        Me.dgcolhStartDate.HeaderText = "Start Date"
        Me.dgcolhStartDate.Name = "dgcolhStartDate"
        Me.dgcolhStartDate.ReadOnly = True
        Me.dgcolhStartDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEndDate
        '
        Me.dgcolhEndDate.HeaderText = "End Date"
        Me.dgcolhEndDate.Name = "dgcolhEndDate"
        Me.dgcolhEndDate.ReadOnly = True
        Me.dgcolhEndDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSupervisor
        '
        Me.dgcolhSupervisor.HeaderText = "Supervisor"
        Me.dgcolhSupervisor.Name = "dgcolhSupervisor"
        Me.dgcolhSupervisor.ReadOnly = True
        '
        'dgcolhRemark
        '
        Me.dgcolhRemark.HeaderText = "Remark"
        Me.dgcolhRemark.Name = "dgcolhRemark"
        Me.dgcolhRemark.ReadOnly = True
        Me.dgcolhRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhRefreeName
        '
        Me.dgcolhRefreeName.HeaderText = "Refree Name"
        Me.dgcolhRefreeName.Name = "dgcolhRefreeName"
        Me.dgcolhRefreeName.ReadOnly = True
        Me.dgcolhRefreeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCountry
        '
        Me.dgcolhCountry.HeaderText = "Country"
        Me.dgcolhCountry.Name = "dgcolhCountry"
        Me.dgcolhCountry.ReadOnly = True
        Me.dgcolhCountry.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdNo
        '
        Me.dgcolhIdNo.HeaderText = "Id. No."
        Me.dgcolhIdNo.Name = "dgcolhIdNo"
        Me.dgcolhIdNo.ReadOnly = True
        Me.dgcolhIdNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEmail
        '
        Me.dgcolhEmail.HeaderText = "Email"
        Me.dgcolhEmail.Name = "dgcolhEmail"
        Me.dgcolhEmail.ReadOnly = True
        Me.dgcolhEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhTelNo
        '
        Me.dgcolhTelNo.HeaderText = "Tel. No."
        Me.dgcolhTelNo.Name = "dgcolhTelNo"
        Me.dgcolhTelNo.ReadOnly = True
        Me.dgcolhTelNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhMobile
        '
        Me.dgcolhMobile.HeaderText = "Mobile"
        Me.dgcolhMobile.Name = "dgcolhMobile"
        Me.dgcolhMobile.ReadOnly = True
        Me.dgcolhMobile.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDBName
        '
        Me.dgcolhDBName.HeaderText = "Name"
        Me.dgcolhDBName.Name = "dgcolhDBName"
        Me.dgcolhDBName.ReadOnly = True
        '
        'dgcolhDBRelation
        '
        Me.dgcolhDBRelation.HeaderText = "Relation"
        Me.dgcolhDBRelation.Name = "dgcolhDBRelation"
        Me.dgcolhDBRelation.ReadOnly = True
        '
        'dgcolhDBbirthdate
        '
        Me.dgcolhDBbirthdate.HeaderText = "Birth Date"
        Me.dgcolhDBbirthdate.Name = "dgcolhDBbirthdate"
        Me.dgcolhDBbirthdate.ReadOnly = True
        '
        'dgcolhDBIdentifyNo
        '
        Me.dgcolhDBIdentifyNo.HeaderText = "Identify No."
        Me.dgcolhDBIdentifyNo.Name = "dgcolhDBIdentifyNo"
        Me.dgcolhDBIdentifyNo.ReadOnly = True
        '
        'dgcolhDBGender
        '
        Me.dgcolhDBGender.HeaderText = "Gender"
        Me.dgcolhDBGender.Name = "dgcolhDBGender"
        '
        'dgcolhIdType
        '
        Me.dgcolhIdType.HeaderText = "Identity Type"
        Me.dgcolhIdType.Name = "dgcolhIdType"
        Me.dgcolhIdType.ReadOnly = True
        Me.dgcolhIdType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSrNo
        '
        Me.dgcolhSrNo.HeaderText = "Serial No."
        Me.dgcolhSrNo.Name = "dgcolhSrNo"
        Me.dgcolhSrNo.ReadOnly = True
        Me.dgcolhSrNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdentityNo
        '
        Me.dgcolhIdentityNo.HeaderText = "Identity No."
        Me.dgcolhIdentityNo.Name = "dgcolhIdentityNo"
        Me.dgcolhIdentityNo.ReadOnly = True
        Me.dgcolhIdentityNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdCountry
        '
        Me.dgcolhIdCountry.HeaderText = "Country"
        Me.dgcolhIdCountry.Name = "dgcolhIdCountry"
        Me.dgcolhIdCountry.ReadOnly = True
        Me.dgcolhIdCountry.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdIssuePlace
        '
        Me.dgcolhIdIssuePlace.HeaderText = "Issue Place"
        Me.dgcolhIdIssuePlace.Name = "dgcolhIdIssuePlace"
        Me.dgcolhIdIssuePlace.ReadOnly = True
        Me.dgcolhIdIssuePlace.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdIssueDate
        '
        Me.dgcolhIdIssueDate.HeaderText = "Issue Date"
        Me.dgcolhIdIssueDate.Name = "dgcolhIdIssueDate"
        Me.dgcolhIdIssueDate.ReadOnly = True
        Me.dgcolhIdIssueDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdExpiryDate
        '
        Me.dgcolhIdExpiryDate.HeaderText = "Expiry Date"
        Me.dgcolhIdExpiryDate.Name = "dgcolhIdExpiryDate"
        Me.dgcolhIdExpiryDate.ReadOnly = True
        Me.dgcolhIdExpiryDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIdDLClass
        '
        Me.dgcolhIdDLClass.HeaderText = "DL Class"
        Me.dgcolhIdDLClass.Name = "dgcolhIdDLClass"
        Me.dgcolhIdDLClass.ReadOnly = True
        Me.dgcolhIdDLClass.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAddressType
        '
        Me.dgcolhAddressType.HeaderText = "Address Type"
        Me.dgcolhAddressType.Name = "dgcolhAddressType"
        Me.dgcolhAddressType.ReadOnly = True
        '
        'dgcolhAddress1
        '
        Me.dgcolhAddress1.HeaderText = "Address1"
        Me.dgcolhAddress1.Name = "dgcolhAddress1"
        Me.dgcolhAddress1.ReadOnly = True
        '
        'dgcolhAddress2
        '
        Me.dgcolhAddress2.HeaderText = "Address2"
        Me.dgcolhAddress2.Name = "dgcolhAddress2"
        Me.dgcolhAddress2.ReadOnly = True
        '
        'dgcolhFirstname
        '
        Me.dgcolhFirstname.HeaderText = "First Name"
        Me.dgcolhFirstname.Name = "dgcolhFirstname"
        Me.dgcolhFirstname.ReadOnly = True
        '
        'dgcolhLastname
        '
        Me.dgcolhLastname.HeaderText = "Last Name"
        Me.dgcolhLastname.Name = "dgcolhLastname"
        Me.dgcolhLastname.ReadOnly = True
        '
        'dgcolhAddressCountry
        '
        Me.dgcolhAddressCountry.HeaderText = "Country"
        Me.dgcolhAddressCountry.Name = "dgcolhAddressCountry"
        Me.dgcolhAddressCountry.ReadOnly = True
        '
        'dgcolhAddressState
        '
        Me.dgcolhAddressState.HeaderText = "State"
        Me.dgcolhAddressState.Name = "dgcolhAddressState"
        Me.dgcolhAddressState.ReadOnly = True
        '
        'dgcolhAddressCity
        '
        Me.dgcolhAddressCity.HeaderText = "City"
        Me.dgcolhAddressCity.Name = "dgcolhAddressCity"
        Me.dgcolhAddressCity.ReadOnly = True
        '
        'dgcolhAddressZipCode
        '
        Me.dgcolhAddressZipCode.HeaderText = "ZipCode"
        Me.dgcolhAddressZipCode.Name = "dgcolhAddressZipCode"
        Me.dgcolhAddressZipCode.ReadOnly = True
        '
        'dgcolhAddressProvince
        '
        Me.dgcolhAddressProvince.HeaderText = "Province"
        Me.dgcolhAddressProvince.Name = "dgcolhAddressProvince"
        Me.dgcolhAddressProvince.ReadOnly = True
        '
        'dgcolhAddressRoad
        '
        Me.dgcolhAddressRoad.HeaderText = "Road"
        Me.dgcolhAddressRoad.Name = "dgcolhAddressRoad"
        Me.dgcolhAddressRoad.ReadOnly = True
        '
        'dgcolhAddressEstate
        '
        Me.dgcolhAddressEstate.HeaderText = "Estate"
        Me.dgcolhAddressEstate.Name = "dgcolhAddressEstate"
        Me.dgcolhAddressEstate.ReadOnly = True
        '
        'dgcolhAddressProvince1
        '
        Me.dgcolhAddressProvince1.HeaderText = "Province1"
        Me.dgcolhAddressProvince1.Name = "dgcolhAddressProvince1"
        Me.dgcolhAddressProvince1.ReadOnly = True
        '
        'dgcolhAddressRoad1
        '
        Me.dgcolhAddressRoad1.HeaderText = "Road1"
        Me.dgcolhAddressRoad1.Name = "dgcolhAddressRoad1"
        Me.dgcolhAddressRoad1.ReadOnly = True
        '
        'dgcolhAddressChiefdom
        '
        Me.dgcolhAddressChiefdom.HeaderText = "Chiefdom"
        Me.dgcolhAddressChiefdom.Name = "dgcolhAddressChiefdom"
        Me.dgcolhAddressChiefdom.ReadOnly = True
        '
        'dgcolhAddressVillage
        '
        Me.dgcolhAddressVillage.HeaderText = "Village"
        Me.dgcolhAddressVillage.Name = "dgcolhAddressVillage"
        Me.dgcolhAddressVillage.ReadOnly = True
        '
        'dgcolhAddressTown
        '
        Me.dgcolhAddressTown.HeaderText = "Town"
        Me.dgcolhAddressTown.Name = "dgcolhAddressTown"
        Me.dgcolhAddressTown.ReadOnly = True
        '
        'dgcolhAddressMobile
        '
        Me.dgcolhAddressMobile.HeaderText = "Mobile"
        Me.dgcolhAddressMobile.Name = "dgcolhAddressMobile"
        Me.dgcolhAddressMobile.ReadOnly = True
        '
        'dgcolhAddressTel_no
        '
        Me.dgcolhAddressTel_no.HeaderText = "Telephone Number"
        Me.dgcolhAddressTel_no.Name = "dgcolhAddressTel_no"
        Me.dgcolhAddressTel_no.ReadOnly = True
        '
        'dgcolhAddressPlotNo
        '
        Me.dgcolhAddressPlotNo.HeaderText = "Plot Number"
        Me.dgcolhAddressPlotNo.Name = "dgcolhAddressPlotNo"
        Me.dgcolhAddressPlotNo.ReadOnly = True
        '
        'dgcolhAddressAltNo
        '
        Me.dgcolhAddressAltNo.HeaderText = "Alternate Number"
        Me.dgcolhAddressAltNo.Name = "dgcolhAddressAltNo"
        Me.dgcolhAddressAltNo.ReadOnly = True
        '
        'dgcolhAddressEmail
        '
        Me.dgcolhAddressEmail.HeaderText = "Email"
        Me.dgcolhAddressEmail.Name = "dgcolhAddressEmail"
        Me.dgcolhAddressEmail.ReadOnly = True
        '
        'dgcolhAddressFax
        '
        Me.dgcolhAddressFax.HeaderText = "Fax"
        Me.dgcolhAddressFax.Name = "dgcolhAddressFax"
        Me.dgcolhAddressFax.ReadOnly = True
        '
        'dgcolhCategory
        '
        Me.dgcolhCategory.HeaderText = "Membership Category"
        Me.dgcolhCategory.Name = "dgcolhCategory"
        Me.dgcolhCategory.ReadOnly = True
        Me.dgcolhCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhmembershipname
        '
        Me.dgcolhmembershipname.HeaderText = "Membership Name"
        Me.dgcolhmembershipname.Name = "dgcolhmembershipname"
        Me.dgcolhmembershipname.ReadOnly = True
        Me.dgcolhmembershipname.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhmembershipno
        '
        Me.dgcolhmembershipno.HeaderText = "Membership No."
        Me.dgcolhmembershipno.Name = "dgcolhmembershipno"
        Me.dgcolhmembershipno.ReadOnly = True
        Me.dgcolhmembershipno.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhperiod_name
        '
        Me.dgcolhperiod_name.HeaderText = "Period Name"
        Me.dgcolhperiod_name.Name = "dgcolhperiod_name"
        Me.dgcolhperiod_name.ReadOnly = True
        Me.dgcolhperiod_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhissue_date
        '
        Me.dgcolhissue_date.HeaderText = "Issue Date"
        Me.dgcolhissue_date.Name = "dgcolhissue_date"
        Me.dgcolhissue_date.ReadOnly = True
        Me.dgcolhissue_date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhstart_date
        '
        Me.dgcolhstart_date.HeaderText = "Start Date"
        Me.dgcolhstart_date.Name = "dgcolhstart_date"
        Me.dgcolhstart_date.ReadOnly = True
        Me.dgcolhstart_date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhexpiry_date
        '
        Me.dgcolhexpiry_date.HeaderText = "Expiry Date"
        Me.dgcolhexpiry_date.Name = "dgcolhexpiry_date"
        Me.dgcolhexpiry_date.ReadOnly = True
        Me.dgcolhexpiry_date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhMemRemark
        '
        Me.dgcolhMemRemark.HeaderText = "Remark"
        Me.dgcolhMemRemark.Name = "dgcolhMemRemark"
        Me.dgcolhMemRemark.ReadOnly = True
        Me.dgcolhMemRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhBirthCountry
        '
        Me.dgcolhBirthCountry.HeaderText = "Country"
        Me.dgcolhBirthCountry.Name = "dgcolhBirthCountry"
        '
        'dgcolhBirthCity
        '
        Me.dgcolhBirthCity.HeaderText = "City"
        Me.dgcolhBirthCity.Name = "dgcolhBirthCity"
        '
        'dgcolhBirthCertificateNo
        '
        Me.dgcolhBirthCertificateNo.HeaderText = "Certificate No."
        Me.dgcolhBirthCertificateNo.Name = "dgcolhBirthCertificateNo"
        '
        'dgcolhBirthTown1
        '
        Me.dgcolhBirthTown1.HeaderText = "Town1"
        Me.dgcolhBirthTown1.Name = "dgcolhBirthTown1"
        '
        'dgcolhBirthVillage1
        '
        Me.dgcolhBirthVillage1.HeaderText = "Village1"
        Me.dgcolhBirthVillage1.Name = "dgcolhBirthVillage1"
        '
        'dgcolhBirthState
        '
        Me.dgcolhBirthState.HeaderText = "State"
        Me.dgcolhBirthState.Name = "dgcolhBirthState"
        '
        'dgcolhBirthWard
        '
        Me.dgcolhBirthWard.HeaderText = "Ward"
        Me.dgcolhBirthWard.Name = "dgcolhBirthWard"
        '
        'dgcolhBirthVillage
        '
        Me.dgcolhBirthVillage.HeaderText = "Village"
        Me.dgcolhBirthVillage.Name = "dgcolhBirthVillage"
        '
        'dgcolhBirthChiefdom
        '
        Me.dgcolhBirthChiefdom.HeaderText = "Chiefdom"
        Me.dgcolhBirthChiefdom.Name = "dgcolhBirthChiefdom"
        '
        'dgcolhComplexion
        '
        Me.dgcolhComplexion.HeaderText = "Complexion"
        Me.dgcolhComplexion.Name = "dgcolhComplexion"
        '
        'dgcolhBloodGroup
        '
        Me.dgcolhBloodGroup.HeaderText = "Blood Group"
        Me.dgcolhBloodGroup.Name = "dgcolhBloodGroup"
        '
        'dgcolhEyeColor
        '
        Me.dgcolhEyeColor.HeaderText = "Eye Color"
        Me.dgcolhEyeColor.Name = "dgcolhEyeColor"
        '
        'dgcolhNationality
        '
        Me.dgcolhNationality.HeaderText = "Nationality"
        Me.dgcolhNationality.Name = "dgcolhNationality"
        '
        'dgcolhEthinCity
        '
        Me.dgcolhEthinCity.HeaderText = "EthinCity"
        Me.dgcolhEthinCity.Name = "dgcolhEthinCity"
        '
        'dgcolhReligion
        '
        Me.dgcolhReligion.HeaderText = "Religion"
        Me.dgcolhReligion.Name = "dgcolhReligion"
        '
        'dgcolhHair
        '
        Me.dgcolhHair.HeaderText = "Hair"
        Me.dgcolhHair.Name = "dgcolhHair"
        '
        'dgcolhMaritalStatus
        '
        Me.dgcolhMaritalStatus.HeaderText = "Marital Status"
        Me.dgcolhMaritalStatus.Name = "dgcolhMaritalStatus"
        '
        'dgcolhExtraTel
        '
        Me.dgcolhExtraTel.HeaderText = "Extra Telephone"
        Me.dgcolhExtraTel.Name = "dgcolhExtraTel"
        '
        'dgcolhLanguage1
        '
        Me.dgcolhLanguage1.HeaderText = "Language1"
        Me.dgcolhLanguage1.Name = "dgcolhLanguage1"
        '
        'dgcolhLanguage2
        '
        Me.dgcolhLanguage2.HeaderText = "Language2"
        Me.dgcolhLanguage2.Name = "dgcolhLanguage2"
        '
        'dgcolhLanguage3
        '
        Me.dgcolhLanguage3.HeaderText = "Language3"
        Me.dgcolhLanguage3.Name = "dgcolhLanguage3"
        '
        'dgcolhLanguage4
        '
        Me.dgcolhLanguage4.HeaderText = "Language4"
        Me.dgcolhLanguage4.Name = "dgcolhLanguage4"
        '
        'dgcolhHeight
        '
        Me.dgcolhHeight.HeaderText = "Height"
        Me.dgcolhHeight.Name = "dgcolhHeight"
        '
        'dgcolhWeight
        '
        Me.dgcolhWeight.HeaderText = "Weight"
        Me.dgcolhWeight.Name = "dgcolhWeight"
        '
        'dgcolhMaritalDate
        '
        Me.dgcolhMaritalDate.HeaderText = "Marital Date"
        Me.dgcolhMaritalDate.Name = "dgcolhMaritalDate"
        '
        'dgcolhAllergies
        '
        Me.dgcolhAllergies.HeaderText = "Allergies"
        Me.dgcolhAllergies.Name = "dgcolhAllergies"
        '
        'dgcolhDisabilities
        '
        Me.dgcolhDisabilities.HeaderText = "Disabilities"
        Me.dgcolhDisabilities.Name = "dgcolhDisabilities"
        '
        'dgcolhSportsHobbies
        '
        Me.dgcolhSportsHobbies.HeaderText = "Sports/Hobbies"
        Me.dgcolhSportsHobbies.Name = "dgcolhSportsHobbies"
        '
        'dgcolhInactiveReason
        '
        Me.dgcolhInactiveReason.HeaderText = "Inactive Reason"
        Me.dgcolhInactiveReason.Name = "dgcolhInactiveReason"
        Me.dgcolhInactiveReason.ReadOnly = True
        Me.dgcolhInactiveReason.Width = 150
        '
        'dgcolhVoidReason
        '
        Me.dgcolhVoidReason.HeaderText = "Void Reason"
        Me.dgcolhVoidReason.Name = "dgcolhVoidReason"
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhEmpid
        '
        Me.objdgcolhEmpid.HeaderText = "objdgcolhEmpid"
        Me.objdgcolhEmpid.Name = "objdgcolhEmpid"
        Me.objdgcolhEmpid.Visible = False
        '
        'objcolhqualificationgroupunkid
        '
        Me.objcolhqualificationgroupunkid.HeaderText = "objcolhqualificationgroupunkid"
        Me.objcolhqualificationgroupunkid.Name = "objcolhqualificationgroupunkid"
        Me.objcolhqualificationgroupunkid.Visible = False
        '
        'objcolhqualificationunkid
        '
        Me.objcolhqualificationunkid.HeaderText = "objcolhqualificationunkid"
        Me.objcolhqualificationunkid.Name = "objcolhqualificationunkid"
        Me.objcolhqualificationunkid.Visible = False
        '
        'objcolhqualificationtranunkid
        '
        Me.objcolhqualificationtranunkid.HeaderText = "objcolhqualificationtranunkid"
        Me.objcolhqualificationtranunkid.Name = "objcolhqualificationtranunkid"
        Me.objcolhqualificationtranunkid.Visible = False
        '
        'objcolhformname
        '
        Me.objcolhformname.HeaderText = "objcolhformname"
        Me.objcolhformname.Name = "objcolhformname"
        Me.objcolhformname.Visible = False
        Me.objcolhformname.Width = 5
        '
        'objcolhnewattachdocumentid
        '
        Me.objcolhnewattachdocumentid.HeaderText = "objcolhnewattachdocumentid"
        Me.objcolhnewattachdocumentid.Name = "objcolhnewattachdocumentid"
        Me.objcolhnewattachdocumentid.Visible = False
        '
        'objcolhdeleteattachdocumentid
        '
        Me.objcolhdeleteattachdocumentid.HeaderText = "objcolhdeleteattachdocumentid"
        Me.objcolhdeleteattachdocumentid.Name = "objcolhdeleteattachdocumentid"
        Me.objcolhdeleteattachdocumentid.Visible = False
        '
        'objcolhdepedantunkid
        '
        Me.objcolhdepedantunkid.HeaderText = "objcolhdepedantunkid"
        Me.objcolhdepedantunkid.Name = "objcolhdepedantunkid"
        Me.objcolhdepedantunkid.Visible = False
        '
        'objcolhAddressTypeId
        '
        Me.objcolhAddressTypeId.HeaderText = "objcolhAddressTypeId"
        Me.objcolhAddressTypeId.Name = "objcolhAddressTypeId"
        Me.objcolhAddressTypeId.ReadOnly = True
        Me.objcolhAddressTypeId.Visible = False
        '
        'frmAppRejEmpData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(816, 486)
        Me.Controls.Add(Me.tblPanel)
        Me.Controls.Add(Me.lblSelectOperationType)
        Me.Controls.Add(Me.cboOperationType)
        Me.Controls.Add(Me.lblEmployeeData)
        Me.Controls.Add(Me.txtSearch)
        Me.Controls.Add(Me.cboEmployeeData)
        Me.Controls.Add(Me.objefemailFooter)
        Me.Controls.Add(Me.lblRemark)
        Me.Controls.Add(Me.txtRemark)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAppRejEmpData"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Appove/Reject Employee Data"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objefemailFooter.ResumeLayout(False)
        Me.objefemailFooter.PerformLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmDownloadAttachment.ResumeLayout(False)
        Me.tblPanel.ResumeLayout(False)
        Me.pnlOperation.ResumeLayout(False)
        Me.pnlDetail.ResumeLayout(False)
        Me.pnlDetail.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtApprover As System.Windows.Forms.TextBox
    Friend WithEvents txtLevel As System.Windows.Forms.TextBox
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents lblLoggedInUser As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents objefemailFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnShowMyReport As eZee.Common.eZeeLightButton
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents btnReject As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents cboEmployeeData As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeData As System.Windows.Forms.Label
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents picStayView As System.Windows.Forms.PictureBox
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblotherqualificationnote As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblSelectOperationType As System.Windows.Forms.Label
    Friend WithEvents cboOperationType As System.Windows.Forms.ComboBox
    Friend WithEvents cmDownloadAttachment As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents btndownloadallattachment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btndownloadnewattachment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btndownloaddeletedattachment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn36 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn37 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn38 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn39 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn40 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn41 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn42 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn43 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn44 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn45 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn46 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn47 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn48 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn49 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn50 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn51 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn52 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn53 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn54 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn55 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn56 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn57 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn58 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn59 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn60 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn61 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn62 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn63 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn64 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn65 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn66 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn67 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn68 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn69 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn70 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn71 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn72 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn73 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn74 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn75 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn76 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tblPanel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pnlOperation As System.Windows.Forms.Panel
    Friend WithEvents radApplytoAll As System.Windows.Forms.RadioButton
    Friend WithEvents radApplytoChecked As System.Windows.Forms.RadioButton
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents lnkSet As System.Windows.Forms.LinkLabel
    Friend WithEvents cboEffectivePeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblSetPeriod As System.Windows.Forms.Label
    Friend WithEvents pnlDetail As System.Windows.Forms.Panel
    Friend WithEvents objdgcolhicheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhDownloadDocument As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhSkillCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSkill As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDescription As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhQualifyGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhQualification As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAwardDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAwardToDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInstitute As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCompany As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStartDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEndDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSupervisor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRefreeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTelNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMobile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDBName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDBRelation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDBbirthdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDBIdentifyNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDBGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSrNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdentityNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdIssuePlace As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdIssueDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdExpiryDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIdDLClass As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddress1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddress2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFirstname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLastname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressState As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressCity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressZipCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressProvince As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressRoad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressEstate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressProvince1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressRoad1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressChiefdom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressVillage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressTown As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressMobile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressTel_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressPlotNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressAltNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAddressFax As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhmembershipname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhmembershipno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhperiod_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhissue_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhstart_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhexpiry_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMemRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthCity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthCertificateNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthTown1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthVillage1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthState As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthWard As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthVillage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthChiefdom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhComplexion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBloodGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEyeColor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNationality As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEthinCity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReligion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHair As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMaritalStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhExtraTel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLanguage1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLanguage2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLanguage3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLanguage4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhWeight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMaritalDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAllergies As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDisabilities As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSportsHobbies As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInactiveReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhVoidReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhqualificationgroupunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhqualificationunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhqualificationtranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhformname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhnewattachdocumentid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhdeleteattachdocumentid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhdepedantunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhAddressTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

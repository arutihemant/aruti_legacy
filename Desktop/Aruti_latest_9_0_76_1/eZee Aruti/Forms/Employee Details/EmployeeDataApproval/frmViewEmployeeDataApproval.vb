﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib

Public Class frmViewEmployeeDataApproval

#Region " Private Varaibles "

    Private objEmpApprovalDataTran As New clsEmployeeDataApproval
    Private ReadOnly mstrModuleName As String = "frmViewEmployeeDataApproval"
    Private mintUserId As Integer
    Private mintPriority As Integer
    Private mintPrivilegeId As Integer
    Private meFillType As enScreenName
    Private mblnIsResidentPermit As Boolean
    Private mdvData As DataView
    'S.SANDEEP [09-AUG-2018] -- START
    Private mstrFilterString As String = String.Empty
    Private mblnFromApprovalScreen As Boolean = True
    'S.SANDEEP [09-AUG-2018] -- END
    Private mOperType As clsEmployeeDataApproval.enOperationType

 'S.SANDEEP |15-APR-2019| -- START
    Private iCellIndex As Integer = 0
 'S.SANDEEP |15-APR-2019| -- END
#End Region

#Region " Display Dialog "

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.



    'Public Function displayDialog(ByVal intUserId As Integer, _
    '                              ByVal intPriority As Integer, _
    '                              ByVal intPrivilegeId As Integer, _
    '                              ByVal eFillType As enScreenName, _
    '                              ByVal eOperType As clsEmployeeDataApproval.enOperationType, _
    '                              Optional ByVal strFilter As String = "", _
    '                              Optional ByVal blnFromApprovalScreen As Boolean = True) As Boolean

    Public Function displayDialog(ByVal intUserId As Integer, _
                                  ByVal intPriority As Integer, _
                                  ByVal intPrivilegeId As Integer, _
                                  ByVal eFillType As enScreenName, _
                                  ByVal eOperType As clsEmployeeDataApproval.enOperationType, _
                                  Optional ByVal strFilter As String = "", _
                                  Optional ByVal blnFromApprovalScreen As Boolean = True) As Boolean
        'Gajanan [17-DEC-2018] -- End

        Try
            mintUserId = intUserId
            mintPriority = intPriority
            mintPrivilegeId = intPrivilegeId
            meFillType = eFillType
            mOperType = eOperType

            If mOperType <> clsEmployeeDataApproval.enOperationType.NONE Then
                cboOperationType.Enabled = False
            End If

            mstrFilterString = strFilter
            mblnFromApprovalScreen = blnFromApprovalScreen
            If mblnFromApprovalScreen = False Then
                Me.Text = Language.getMessage(mstrModuleName, 1, "Approval Status")
            Else
                Me.Text = Language.getMessage(mstrModuleName, 2, "My Report")
            End If
            Me.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Function "

    Private Sub FillGrid()
        Try
            Dim dt As DataTable = objEmpApprovalDataTran.GetNextEmployeeApprovers(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, mintPrivilegeId, meFillType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, CType(cboOperationType.SelectedValue, clsEmployeeDataApproval.enOperationType), Nothing, mintPriority, True, mstrFilterString)
 'S.SANDEEP |15-APR-2019| -- START


            Dim xColName As DataGridViewColumn = Nothing

If CInt(meFillType) = CInt(enScreenName.frmQualificationsList) Then
                iCellIndex = dgcolhQualifyGroup.Index
                xColName = dgcolhQualifyGroup
            ElseIf CInt(meFillType) = CInt(enScreenName.frmEmployee_Skill_List) Then
                iCellIndex = dgcolhSkillCategory.Index
                xColName = dgcolhSkillCategory
            ElseIf CInt(meFillType) = CInt(enScreenName.frmEmployeeRefereeList) Then
                iCellIndex = dgcolhRefreeName.Index
                xColName = dgcolhRefreeName
            ElseIf CInt(meFillType) = CInt(enScreenName.frmJobHistory_ExperienceList) Then
                iCellIndex = dgcolhCompany.Index
                xColName = dgcolhCompany
            ElseIf CInt(meFillType) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
                iCellIndex = dgcolhDBName.Index
                xColName = dgcolhDBName
            ElseIf CInt(meFillType) = CInt(enScreenName.frmIdentityInfoList) Then
                iCellIndex = dgcolhIdType.Index
                xColName = dgcolhIdType
            ElseIf CInt(meFillType) = CInt(enScreenName.frmAddressList) Then
                iCellIndex = dgcolhAddressType.Index
                xColName = dgcolhAddressType
            ElseIf CInt(meFillType) = CInt(enScreenName.frmEmergencyAddressList) Then
                iCellIndex = dgcolhAddressType.Index
                xColName = dgcolhAddressType
            ElseIf CInt(meFillType) = CInt(enScreenName.frmMembershipInfoList) Then
                iCellIndex = dgcolhCategory.Index
                xColName = dgcolhCategory

            ElseIf CInt(meFillType) = CInt(enScreenName.frmOtherinfo) Then
                iCellIndex = dgcolhComplexion.Index
                xColName = dgcolhComplexion

            ElseIf CInt(meFillType) = CInt(enScreenName.frmBirthinfo) Then
                iCellIndex = dgcolhBirthCountry.Index
                xColName = dgcolhBirthCountry
            End If
 'S.SANDEEP |15-APR-2019| -- END

            If mblnFromApprovalScreen Then
 'S.SANDEEP |15-APR-2019| -- START


 
                'Dim dtmp() As DataRow = dt.Select("isgrp = 1", dt.Columns(iCellIndex).ColumnName)
                'If dtmp.Length > 0 Then
                '    Dim strGrpName As String = String.Empty
                '    For i As Integer = 0 To dtmp.Length - 1
                '        If strGrpName <> dtmp(i)(iCellIndex).ToString() Then
                '            strGrpName = dtmp(i)(iCellIndex).ToString()
                '        Else
                '            dt.Rows.Remove(dtmp(i))
                '        End If
                '    Next
                'End If
'S.SANDEEP |15-APR-2019| -- END
                dt = New DataView(dt, "priority <= " & mintPriority, "employeeunkid,priority", DataViewRowState.CurrentRows).ToTable()
            End If
            SetGridColums()

            dgvData.AutoGenerateColumns = False


            objdgcolhIsGrp.DataPropertyName = "isgrp"
            objdgcolhEmpid.DataPropertyName = "employeeunkid"
            objcolhqualificationgroupunkid.DataPropertyName = "qualificationgroupunkid"
            objcolhqualificationunkid.DataPropertyName = "qualificationunkid"

            dgcolhSkillCategory.DataPropertyName = "scategory"
            dgcolhSkill.DataPropertyName = "skill"
            dgcolhDescription.DataPropertyName = "description"

            dgcolhQualifyGroup.DataPropertyName = "qualificationgrpname"
            dgcolhQualification.DataPropertyName = "qualificationname"
            dgcolhAwardDate.DataPropertyName = "award_start_date"
            dgcolhAwardToDate.DataPropertyName = "award_end_date"
            dgcolhInstitute.DataPropertyName = "institute_name"
            dgcolhRefNo.DataPropertyName = "reference_no"

            dgcolhCompany.DataPropertyName = "cname"
            dgcolhJob.DataPropertyName = "old_job"
            dgcolhStartDate.DataPropertyName = "start_date"
            dgcolhEndDate.DataPropertyName = "end_date"
            dgcolhSupervisor.DataPropertyName = "supervisor"
            dgcolhRemark.DataPropertyName = "remark"

            dgcolhRefreeName.DataPropertyName = "rname"
            dgcolhCountry.DataPropertyName = "Country"
            dgcolhIdNo.DataPropertyName = "Company"
            dgcolhEmail.DataPropertyName = "Email"
            dgcolhTelNo.DataPropertyName = "telephone_no"
            dgcolhMobile.DataPropertyName = "mobile_no"



            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            dgcolhDBName.DataPropertyName = "dependantname"
            dgcolhRelation.DataPropertyName = "relation"
            dgcolhbirthdate.DataPropertyName = "birthdate"
            dgcolhIdentifyNo.DataPropertyName = "identify_no"
            dgcolhGender.DataPropertyName = "gender"
            objcolhdepedantunkid.DataPropertyName = "dpndtbeneficetranunkid"

            dgcolhIdType.DataPropertyName = "IdType"
            dgcolhSrNo.DataPropertyName = "serial_no"
            dgcolhIdentityNo.DataPropertyName = "identity_no"
            dgcolhIdCountry.DataPropertyName = "Country"
            dgcolhIdIssuePlace.DataPropertyName = "issued_place"
            dgcolhIdIssueDate.DataPropertyName = "issue_date"
            dgcolhIdExpiryDate.DataPropertyName = "expiry_date"
            dgcolhIdDLClass.DataPropertyName = "dl_class"
            'Gajanan [22-Feb-2019] -- End


            'S.SANDEEP |15-APR-2019| -- START
            dgcolhCategory.DataPropertyName = "Category"
            dgcolhmembershipname.DataPropertyName = "membershipname"
            dgcolhmembershipno.DataPropertyName = "membershipno"
            dgcolhperiod_name.DataPropertyName = "period_name"

            dgcolhissue_date.DataPropertyName = "issue_date"
            dgcolhissue_date.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat

            dgcolhstart_date.DataPropertyName = "start_date"
            dgcolhstart_date.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat

            dgcolhexpiry_date.DataPropertyName = "expiry_date"
            dgcolhexpiry_date.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat

            dgcolhMemRemark.DataPropertyName = "remark"
            'S.SANDEEP |15-APR-2019| -- END

            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Select Case meFillType
                Case enScreenName.frmAddressList
                    dgcolhAddressType.DataPropertyName = "addresstype"
                    dgcolhAddress1.DataPropertyName = "address1"
                    dgcolhAddress2.DataPropertyName = "address2"
                    dgcolhAddressCountry.DataPropertyName = "country"
                    dgcolhAddressState.DataPropertyName = "state"
                    dgcolhAddressCity.DataPropertyName = "city"
                    dgcolhAddressZipCode.DataPropertyName = "zipcode_code"
                    dgcolhAddressProvince.DataPropertyName = "provicnce"
                    dgcolhAddressRoad.DataPropertyName = "road"
                    dgcolhAddressEstate.DataPropertyName = "estate"
                    dgcolhAddressProvince1.DataPropertyName = "provicnce1"
                    dgcolhAddressRoad1.DataPropertyName = "Road1"
                    dgcolhAddressChiefdom.DataPropertyName = "chiefdom"
                    dgcolhAddressVillage.DataPropertyName = "village"
                    dgcolhAddressTown.DataPropertyName = "town"
                    dgcolhAddressMobile.DataPropertyName = "mobile"
                    dgcolhAddressTel_no.DataPropertyName = "tel_no"
                    dgcolhAddressPlotNo.DataPropertyName = "plotNo"
                    dgcolhAddressAltNo.DataPropertyName = "alternateno"
                    dgcolhAddressEmail.DataPropertyName = "email"
                    dgcolhAddressFax.DataPropertyName = "fax"
                Case enScreenName.frmEmergencyAddressList
                    dgcolhAddressType.DataPropertyName = "addresstype"
                    dgcolhFirstname.DataPropertyName = "firstname"
                    dgcolhLastname.DataPropertyName = "lastname"
                    dgcolhAddressCountry.DataPropertyName = "country"
                    dgcolhAddressState.DataPropertyName = "state"
                    dgcolhAddressCity.DataPropertyName = "city"
                    dgcolhAddressZipCode.DataPropertyName = "zipcode_code"
                    dgcolhAddressProvince.DataPropertyName = "provicnce"
                    dgcolhAddressRoad.DataPropertyName = "road"
                    dgcolhAddressEstate.DataPropertyName = "estate"
                    dgcolhAddressPlotNo.DataPropertyName = "plotNo"
                    dgcolhAddressMobile.DataPropertyName = "mobile"
                    dgcolhAddressAltNo.DataPropertyName = "alternateno"
                    dgcolhAddressTel_no.DataPropertyName = "tel_no"
                    dgcolhAddressFax.DataPropertyName = "fax"
                    dgcolhAddressEmail.DataPropertyName = "email"

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Case enScreenName.frmBirthinfo

                    dgcolhBirthCountry.DataPropertyName = "country"
                    dgcolhBirthState.DataPropertyName = "state"
                    dgcolhBirthCity.DataPropertyName = "city"
                    dgcolhBirthCertificateNo.DataPropertyName = "birthcertificateno"
                    dgcolhBirthWard.DataPropertyName = "birth_ward"
                    dgcolhBirthVillage.DataPropertyName = "birth_village"
                    dgcolhBirthTown1.DataPropertyName = "town"
                    dgcolhBirthVillage1.DataPropertyName = "village"
                    dgcolhBirthChiefdom.DataPropertyName = "chiefdom"



                   
                Case enScreenName.frmOtherinfo
                    dgcolhComplexion.DataPropertyName = "Complexion"
                    dgcolhBloodGroup.DataPropertyName = "BloodGroup"
                    dgcolhEyeColor.DataPropertyName = "EyeColor"
                    dgcolhNationality.DataPropertyName = "Nationality"
                    dgcolhEthinCity.DataPropertyName = "Ethnicity"
                    dgcolhReligion.DataPropertyName = "Religion"
                    dgcolhHair.DataPropertyName = "HairColor"
                    dgcolhMaritalStatus.DataPropertyName = "Maritalstatus"
                    dgcolhExtraTel.DataPropertyName = "ExtTelephoneno"
                    dgcolhLanguage1.DataPropertyName = "language1"
                    dgcolhLanguage2.DataPropertyName = "language2"
                    dgcolhLanguage3.DataPropertyName = "language3"
                    dgcolhLanguage4.DataPropertyName = "language4"
                    dgcolhHeight.DataPropertyName = "height"
                    dgcolhWeight.DataPropertyName = "weight"
                    dgcolhMaritalDate.DataPropertyName = "anniversary_date"
                    dgcolhAllergies.DataPropertyName = "Allergies"
                    dgcolhSportsHobbies.DataPropertyName = "sports_hobbies"

                    'Gajanan [17-April-2019] -- End

            End Select
            'Gajanan [18-Mar-2019] -- End

            dgcolhApprover.DataPropertyName = "username"
            dgcolhLevel.DataPropertyName = "levelname"
            dgcolhStatus.DataPropertyName = "iStatus"


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            dgcolhVoidReason.DataPropertyName = "voidreason"
            'Gajanan [17-April-2019] -- End

            mdvData = dt.DefaultView
            dgvData.DataSource = mdvData

            objdgcolhicheck.Visible = False

            If meFillType = enScreenName.frmQualificationsList Then
                For Each xdgvr As DataGridViewRow In dgvData.Rows
                    If (CStr(xdgvr.Cells(objcolhqualificationgroupunkid.Index).Value).Trim.Length > 0 AndAlso CInt(xdgvr.Cells(objcolhqualificationunkid.Index).Value) <= 0) And _
                        (CStr(xdgvr.Cells(objcolhqualificationunkid.Index).Value).Trim.Length > 0 AndAlso CInt(xdgvr.Cells(objcolhqualificationunkid.Index).Value) <= 0) Then
                        xdgvr.DefaultCellStyle.ForeColor = Color.Blue
                        lblotherqualificationnote.Visible = True
                    End If
                Next
            Else
                lblotherqualificationnote.Visible = False
            End If

            Select Case meFillType
                Case enScreenName.frmQualificationsList
                    dgcolhQualifyGroup.Visible = True
                    dgcolhQualification.Visible = True
                    dgcolhAwardDate.Visible = True
                    dgcolhAwardDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat
                    dgcolhAwardToDate.Visible = True
                    dgcolhAwardToDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat
                    dgcolhInstitute.Visible = True
                    dgcolhRefNo.Visible = True

                Case enScreenName.frmJobHistory_ExperienceList
                    dgcolhCompany.Visible = True
                    dgcolhJob.Visible = True
                    dgcolhStartDate.Visible = True
                    dgcolhStartDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat
                    dgcolhEndDate.Visible = True
                    dgcolhEndDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat
                    dgcolhSupervisor.Visible = True
                    dgcolhRemark.Visible = True

                Case enScreenName.frmEmployeeRefereeList
                    dgcolhRefreeName.Visible = True
                    dgcolhCountry.Visible = True
                    dgcolhIdNo.Visible = True
                    dgcolhEmail.Visible = True
                    dgcolhTelNo.Visible = True
                    dgcolhMobile.Visible = True

                Case enScreenName.frmEmployee_Skill_List
                    dgcolhSkillCategory.Visible = True
                    dgcolhSkill.Visible = True
                    dgcolhDescription.Visible = True

                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Case enScreenName.frmDependantsAndBeneficiariesList
                    dgcolhDBName.Visible = True
                    dgcolhRelation.Visible = True
                    dgcolhbirthdate.Visible = True
                    dgcolhIdentifyNo.Visible = True
                    dgcolhGender.Visible = True

                Case enScreenName.frmIdentityInfoList
                    dgcolhIdType.Visible = True
                    dgcolhSrNo.Visible = True
                    dgcolhIdentityNo.Visible = True
                    dgcolhIdCountry.Visible = True
                    dgcolhIdIssuePlace.Visible = True
                    dgcolhIdIssueDate.Visible = True
                    dgcolhIdExpiryDate.Visible = True
                    dgcolhIdDLClass.Visible = True
                    'Gajanan [22-Feb-2019] -- End




                    'Gajanan [18-Mar-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Case enScreenName.frmAddressList
                    dgcolhAddressType.Visible = True
                    dgcolhAddress1.Visible = True
                    dgcolhAddress2.Visible = True
                    dgcolhAddressCountry.Visible = True
                    dgcolhAddressState.Visible = True
                    dgcolhAddressCity.Visible = True
                    dgcolhAddressZipCode.Visible = True
                    dgcolhAddressProvince.Visible = True
                    dgcolhAddressRoad.Visible = True
                    dgcolhAddressEstate.Visible = True
                    dgcolhAddressProvince1.Visible = True
                    dgcolhAddressRoad1.Visible = True
                    dgcolhAddressChiefdom.Visible = True
                    dgcolhAddressVillage.Visible = True
                    dgcolhAddressTown.Visible = True
                    dgcolhAddressMobile.Visible = True
                    dgcolhAddressTel_no.Visible = True
                    dgcolhAddressPlotNo.Visible = True
                    dgcolhAddressAltNo.Visible = True
                    dgcolhAddressEmail.Visible = True
                    dgcolhAddressFax.Visible = True




                Case enScreenName.frmEmergencyAddressList
                    dgcolhAddressType.Visible = True
                    dgcolhFirstname.Visible = True
                    dgcolhLastname.Visible = True
                    dgcolhAddressCountry.Visible = True
                    dgcolhAddressState.Visible = True
                    dgcolhAddressCity.Visible = True
                    dgcolhAddressZipCode.Visible = True
                    dgcolhAddressProvince.Visible = True
                    dgcolhAddressRoad.Visible = True
                    dgcolhAddressEstate.Visible = True
                    dgcolhAddressPlotNo.Visible = True
                    dgcolhAddressMobile.Visible = True
                    dgcolhAddressAltNo.Visible = True
                    dgcolhAddressTel_no.Visible = True
                    dgcolhAddressFax.Visible = True
                    dgcolhAddressEmail.Visible = True

                    'Gajanan [18-Mar-2019] -- End

                    'S.SANDEEP |15-APR-2019| -- START
                Case enScreenName.frmMembershipInfoList
                    dgcolhCategory.Visible = True
                    dgcolhmembershipname.Visible = True
                    dgcolhmembershipno.Visible = True
                    dgcolhperiod_name.Visible = True
                    dgcolhissue_date.Visible = True
                    dgcolhstart_date.Visible = True
                    dgcolhexpiry_date.Visible = True
                    dgcolhMemRemark.Visible = True
                    'S.SANDEEP |15-APR-2019| -- END



                Case enScreenName.frmBirthinfo
                    dgcolhBirthCountry.Visible = True
                    dgcolhBirthState.Visible = True
                    dgcolhBirthCity.Visible = True
                    dgcolhBirthCertificateNo.Visible = True
                    dgcolhBirthWard.Visible = True
                    dgcolhBirthVillage.Visible = True
                    dgcolhBirthTown1.Visible = True
                    dgcolhBirthVillage1.Visible = True
                    dgcolhBirthChiefdom.Visible = True

                   
                Case enScreenName.frmOtherinfo
                    dgcolhComplexion.Visible = True
                    dgcolhBloodGroup.Visible = True
                    dgcolhEyeColor.Visible = True
                    dgcolhNationality.Visible = True
                    dgcolhEthinCity.Visible = True
                    dgcolhReligion.Visible = True
                    dgcolhHair.Visible = True
                    dgcolhMaritalStatus.Visible = True
                    dgcolhExtraTel.Visible = True
                    dgcolhLanguage1.Visible = True
                    dgcolhLanguage2.Visible = True
                    dgcolhLanguage3.Visible = True
                    dgcolhLanguage4.Visible = True
                    dgcolhHeight.Visible = True
                    dgcolhWeight.Visible = True
                    dgcolhMaritalDate.Visible = True
                    dgcolhAllergies.Visible = True
                    dgcolhDisabilities.Visible = True
                    dgcolhSportsHobbies.Visible = True

                   
            End Select

            If xColName IsNot Nothing Then
                Dim dtmp() As DataRow = dt.Select("isgrp = 1", xColName.DataPropertyName)
                If dtmp.Length > 0 Then
                    Dim strGrpName As String = String.Empty
                    For i As Integer = 0 To dtmp.Length - 1
                        If strGrpName <> dtmp(i)(xColName.DataPropertyName).ToString() Then
                            strGrpName = dtmp(i)(xColName.DataPropertyName).ToString()
                        Else
                            dt.Rows.Remove(dtmp(i))
                        End If
                    Next
                End If

            End If
           
            dgcolhApprover.Visible = True
            dgcolhLevel.Visible = True
            dgcolhStatus.Visible = True
            SetGridStyle()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub
    Private Sub SetGridStyle()
        Try
 'S.SANDEEP |15-APR-2019| -- START




            'Dim iCellIndex As Integer = 0

            'If CInt(meFillType) = CInt(enScreenName.frmQualificationsList) Then
            '    iCellIndex = dgcolhQualifyGroup.Index
            'ElseIf CInt(meFillType) = CInt(enScreenName.frmEmployee_Skill_List) Then
            '    iCellIndex = dgcolhSkillCategory.Index
            'ElseIf CInt(meFillType) = CInt(enScreenName.frmEmployeeRefereeList) Then
            '    iCellIndex = dgcolhRefreeName.Index
            'ElseIf CInt(meFillType) = CInt(enScreenName.frmJobHistory_ExperienceList) Then
            '    iCellIndex = dgcolhCompany.Index
            '    'Gajanan [22-Feb-2019] -- Start
            '    'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'ElseIf CInt(meFillType) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
            '    iCellIndex = dgcolhDBName.Index
            'ElseIf CInt(meFillType) = CInt(enScreenName.frmIdentityInfoList) Then
            '    iCellIndex = dgcolhIdType.Index
            '    'Gajanan [22-Feb-2019] -- End

            '    'Gajanan [18-Mar-2019] -- Start
            '    'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'ElseIf CInt(meFillType) = CInt(enScreenName.frmAddressList) Then
            '    iCellIndex = dgcolhAddressType.Index

            'ElseIf CInt(meFillType) = CInt(enScreenName.frmEmergencyAddressList) Then
            '    iCellIndex = dgcolhAddressType.Index
            '    'Gajanan [18-Mar-2019] -- End

            'End If
 'S.SANDEEP |15-APR-2019| -- END

            Dim pCell As New clsMergeCell
            For Each dgvRow As DataGridViewRow In dgvData.Rows
                If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = True Then
                    'pCell.MakeMerge(dgvData, dgvRow.Index, 1, dgvRow.Cells.Count - 1, Color.Gray, Color.White, dgvRow.Cells(dgcolhSkillCategory.Index).Value.ToString, "", picStayView.Image)
                    pCell.MakeMerge(dgvData, dgvRow.Index, iCellIndex, dgvRow.Cells.Count - 1, Color.Gray, Color.White, dgvRow.Cells(iCellIndex).Value.ToString, "", picStayView.Image)
                End If
            Next
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridStyle", mstrModuleName)
        Finally
        End Try
    End Sub
    
    Private Sub SetGridColums()
        Try
            For Each dgCol As DataGridViewColumn In dgvData.Columns
                dgCol.Visible = False
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColums", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            dsList = objEmpApprovalDataTran.getOperationTypeList("List", True)
            With cboOperationType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private Function SetRowStyle(ByVal xRow As DataGridViewRow, ByVal iCellIndex As Integer, ByVal pCell As clsMergeCell, ByVal intEndIndex As Integer) As Boolean
        pCell.MakeMerge(dgvData, xRow.Index, iCellIndex, intEndIndex, Color.Gray, Color.White, xRow.Cells(iCellIndex).Value.ToString, "", picStayView.Image)
    End Function
    'Gajanan [22-Feb-2019] -- End

#End Region

#Region " Buttons "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub
#End Region

    Private Sub cboOperationType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOperationType.SelectedIndexChanged
        Try
            If CInt(cboOperationType.SelectedValue) > 0 Then
                FillGrid()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOperationType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#Region " Form's Events "
    Private Sub frmViewEmployeeDataApproval_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmpApprovalDataTran = New clsEmployeeDataApproval
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Call SetGridColums()
            FillCombo()

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'Call FillGrid()
            If CInt(mOperType) > 0 Then
                cboOperationType.SelectedValue = mOperType
            Else
            Call FillGrid()
            End If
            'Gajanan [17-DEC-2018] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmViewApproval_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmViewMovementApproval_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmpApprovalDataTran = Nothing
    End Sub
#End Region

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.dgcolhSkillCategory.HeaderText = Language._Object.getCaption(Me.dgcolhSkillCategory.Name, Me.dgcolhSkillCategory.HeaderText)
			Me.dgcolhSkill.HeaderText = Language._Object.getCaption(Me.dgcolhSkill.Name, Me.dgcolhSkill.HeaderText)
			Me.dgcolhDescription.HeaderText = Language._Object.getCaption(Me.dgcolhDescription.Name, Me.dgcolhDescription.HeaderText)
			Me.dgcolhQualifyGroup.HeaderText = Language._Object.getCaption(Me.dgcolhQualifyGroup.Name, Me.dgcolhQualifyGroup.HeaderText)
			Me.dgcolhQualification.HeaderText = Language._Object.getCaption(Me.dgcolhQualification.Name, Me.dgcolhQualification.HeaderText)
			Me.dgcolhAwardDate.HeaderText = Language._Object.getCaption(Me.dgcolhAwardDate.Name, Me.dgcolhAwardDate.HeaderText)
			Me.dgcolhAwardToDate.HeaderText = Language._Object.getCaption(Me.dgcolhAwardToDate.Name, Me.dgcolhAwardToDate.HeaderText)
			Me.dgcolhInstitute.HeaderText = Language._Object.getCaption(Me.dgcolhInstitute.Name, Me.dgcolhInstitute.HeaderText)
			Me.dgcolhRefNo.HeaderText = Language._Object.getCaption(Me.dgcolhRefNo.Name, Me.dgcolhRefNo.HeaderText)
			Me.dgcolhCompany.HeaderText = Language._Object.getCaption(Me.dgcolhCompany.Name, Me.dgcolhCompany.HeaderText)
			Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
			Me.dgcolhStartDate.HeaderText = Language._Object.getCaption(Me.dgcolhStartDate.Name, Me.dgcolhStartDate.HeaderText)
			Me.dgcolhEndDate.HeaderText = Language._Object.getCaption(Me.dgcolhEndDate.Name, Me.dgcolhEndDate.HeaderText)
			Me.dgcolhSupervisor.HeaderText = Language._Object.getCaption(Me.dgcolhSupervisor.Name, Me.dgcolhSupervisor.HeaderText)
			Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)
			Me.dgcolhRefreeName.HeaderText = Language._Object.getCaption(Me.dgcolhRefreeName.Name, Me.dgcolhRefreeName.HeaderText)
			Me.dgcolhCountry.HeaderText = Language._Object.getCaption(Me.dgcolhCountry.Name, Me.dgcolhCountry.HeaderText)
			Me.dgcolhIdNo.HeaderText = Language._Object.getCaption(Me.dgcolhIdNo.Name, Me.dgcolhIdNo.HeaderText)
			Me.dgcolhEmail.HeaderText = Language._Object.getCaption(Me.dgcolhEmail.Name, Me.dgcolhEmail.HeaderText)
			Me.dgcolhTelNo.HeaderText = Language._Object.getCaption(Me.dgcolhTelNo.Name, Me.dgcolhTelNo.HeaderText)
			Me.dgcolhMobile.HeaderText = Language._Object.getCaption(Me.dgcolhMobile.Name, Me.dgcolhMobile.HeaderText)
			Me.dgcolhLevel.HeaderText = Language._Object.getCaption(Me.dgcolhLevel.Name, Me.dgcolhLevel.HeaderText)
			Me.dgcolhApprover.HeaderText = Language._Object.getCaption(Me.dgcolhApprover.Name, Me.dgcolhApprover.HeaderText)
			Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Approval Status")
			Language.setMessage(mstrModuleName, 2, "My Report")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmployeeReferences

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployeeReferences"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objReferences As clsEmployee_Ref_Tran
    Private mintReferencesUnkid As Integer = -1
    Private mintSeletedEmpId As Integer = -1
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintReferencesUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintReferencesUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            txtReferenceNo.BackColor = GUI.ColorComp
            txtRemark.BackColor = GUI.ColorOptional
            cboEmployee.BackColor = GUI.ColorComp
            cboReference.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objReferences._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objReferences._Reference_No = txtReferenceNo.Text
            objReferences._Referencesunkid = CInt(cboReference.SelectedValue)
            objReferences._Remark = txtRemark.Text
            If mintReferencesUnkid = -1 Then
                objReferences._Userunkid = User._Object._Userunkid
                objReferences._Isvoid = False
                objReferences._Voiddatetime = Nothing
                objReferences._Voidreason = ""
                objReferences._Voiduserunkid = -1
            Else
                objReferences._Userunkid = objReferences._Userunkid
                objReferences._Isvoid = objReferences._Isvoid
                objReferences._Voiddatetime = objReferences._Voiddatetime
                objReferences._Voidreason = objReferences._Voidreason
                objReferences._Voiduserunkid = objReferences._Voiduserunkid
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtReferenceNo.Text = CStr(objReferences._Reference_No)
            txtRemark.Text = CStr(objReferences._Remark)
            cboEmployee.SelectedValue = CInt(objReferences._Employeeunkid)
            cboReference.SelectedValue = CInt(objReferences._Referencesunkid)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmp.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If menAction = enAction.EDIT_ONE Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If

            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

            dsCombos = objCommon.getComboList(CType(CInt(30), clsCommon_Master.enCommonMaster), True, "Ref")
            With cboReference
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Ref")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboReference.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "References is compulsory information. Please select References to continue."), enMsgBoxStyle.Information)
                cboReference.Focus()
                Return False
            End If

            If txtReferenceNo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Reference No. cannot be blank. Reference No. is compulsory information."), enMsgBoxStyle.Information)
                txtReferenceNo.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "
    Private Sub frmEmployeeReferences_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objReferences = Nothing
    End Sub

    Private Sub frmEmployeeReferences_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmEmployeeReferences_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployeeReferences_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objReferences = New clsEmployee_Ref_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objReferences._Referencestranunkid = mintReferencesUnkid
                'Sandeep [ 17 Aug 2010 ] -- Start
                cboEmployee.Enabled = False
                'Sandeep [ 17 Aug 2010 ] -- End 
            End If

            Call GetValue()

            cboEmployee.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeReferences_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Ref_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Ref_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objReferences.Update()
            Else
                blnFlag = objReferences.Insert()
            End If

            If blnFlag = False And objReferences._Message <> "" Then
                eZeeMsgBox.Show(objReferences._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objReferences = Nothing
                    objReferences = New clsEmployee_Ref_Tran
                    Call GetValue()
                    cboEmployee.Focus()
                Else
                    mintReferencesUnkid = objReferences._Referencestranunkid
                    Me.Close()
                End If
            End If

            If mintSeletedEmpId <> -1 Then
                cboEmployee.SelectedValue = mintSeletedEmpId
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddReferences_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReferences.Click
        Try
            Dim dsCombos As New DataSet
            Dim mintRefeId As Integer = -1
            Dim objComm As New clsCommon_Master
            Dim frm As New frmCommonMaster

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            'Sandeep [ 14 Aug 2010 ] -- Start
            'frm.displayDialog(mintRefeId, clsCommon_Master.enCommonMaster.EMPLOYEE_REFERENCES, enAction.ADD_ONE)
            'dsCombos = objComm.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEE_REFERENCES, True, "Ref")
            'With cboReference
            '    .ValueMember = "masterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Asset")
            '    .SelectedValue = mintRefeId
            'End With
            'frm.displayDialog(mintRefeId, clsCommon_Master.enCommonMaster.EMPLOYEE_REFERENCES, enAction.ADD_ONE)
            If mintRefeId <> -1 Then
                'dsCombos = objComm.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEE_REFERENCES, True, "Ref")
                With cboReference
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Ref")
                    .SelectedValue = mintRefeId
                End With
            End If
            'Sandeep [ 14 Aug 2010 ] -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReferences_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 
            With objfrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If objfrm.DisplayDialog Then
                cboEmployee.SelectedValue = objfrm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Contols "
    Private Sub cboEmployee_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectionChangeCommitted
        Try
            mintSeletedEmpId = CInt(cboEmployee.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 17 Aug 2010 ] -- Start
    '    Private Sub cboReference_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReference.SelectedIndexChanged
    '        Try
    '            txtReferenceNo.Text = ""
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "cboReference_SelectedIndexChanged", mstrModuleName)
    '        End Try
    '    End Sub
    'Sandeep [ 17 Aug 2010 ] -- End 
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbEmployeeReferences.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeReferences.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbEmployeeReferences.Text = Language._Object.getCaption(Me.gbEmployeeReferences.Name, Me.gbEmployeeReferences.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblReferencesNo.Text = Language._Object.getCaption(Me.lblReferencesNo.Name, Me.lblReferencesNo.Text)
			Me.lblReference.Text = Language._Object.getCaption(Me.lblReference.Name, Me.lblReference.Text)
			Me.lnReferenceInfo.Text = Language._Object.getCaption(Me.lnReferenceInfo.Name, Me.lnReferenceInfo.Text)
			Me.lnEmployeeName.Text = Language._Object.getCaption(Me.lnEmployeeName.Name, Me.lnEmployeeName.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 2, "References is compulsory information. Please select References to continue.")
			Language.setMessage(mstrModuleName, 3, "Reference No. cannot be blank. Reference No. is compulsory information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
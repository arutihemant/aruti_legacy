﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVacancyMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVacancyMapping))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtComments = New eZee.TextBox.AlphanumericTextBox
        Me.lblComments = New System.Windows.Forms.Label
        Me.dtpPossibleStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblPossibleStartDate = New System.Windows.Forms.Label
        Me.lblVacancyFoudOutFrom = New System.Windows.Forms.Label
        Me.txtVacancyFoudOutFrom = New eZee.TextBox.AlphanumericTextBox
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.lblVacancyType = New System.Windows.Forms.Label
        Me.lblVacancy = New System.Windows.Forms.Label
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.lvOldVacancy = New eZee.Common.eZeeListView(Me.components)
        Me.colhVacancy = New System.Windows.Forms.ColumnHeader
        Me.objcolhApplicant = New System.Windows.Forms.ColumnHeader
        Me.colhVacancyFoundOutFrom = New System.Windows.Forms.ColumnHeader
        Me.colhPossibleStartDate = New System.Windows.Forms.ColumnHeader
        Me.colhComments = New System.Windows.Forms.ColumnHeader
        Me.lblApplicant = New System.Windows.Forms.Label
        Me.txtApplicant = New eZee.TextBox.AlphanumericTextBox
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboVacancyFoudOutFrom = New System.Windows.Forms.ComboBox
        Me.chkOtherVFF = New System.Windows.Forms.CheckBox
        Me.pnlMain.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(775, 471)
        Me.pnlMain.TabIndex = 0
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkOtherVFF)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboVacancyFoudOutFrom)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtComments)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblComments)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.dtpPossibleStartDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblPossibleStartDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblVacancyFoudOutFrom)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtVacancyFoudOutFrom)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboVacancyType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblVacancyType)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblVacancy)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchVacancy)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboVacancy)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lvOldVacancy)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblApplicant)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtApplicant)
        Me.EZeeCollapsibleContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(775, 416)
        Me.EZeeCollapsibleContainer1.TabIndex = 1
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Applicant Vacancy Mapping"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtComments
        '
        Me.txtComments.BackColor = System.Drawing.Color.White
        Me.txtComments.Flags = 0
        Me.txtComments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtComments.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtComments.Location = New System.Drawing.Point(143, 339)
        Me.txtComments.Multiline = True
        Me.txtComments.Name = "txtComments"
        Me.txtComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtComments.Size = New System.Drawing.Size(620, 71)
        Me.txtComments.TabIndex = 99
        '
        'lblComments
        '
        Me.lblComments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComments.Location = New System.Drawing.Point(11, 342)
        Me.lblComments.Name = "lblComments"
        Me.lblComments.Size = New System.Drawing.Size(126, 15)
        Me.lblComments.TabIndex = 98
        Me.lblComments.Text = "Comments"
        Me.lblComments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpPossibleStartDate
        '
        Me.dtpPossibleStartDate.Checked = False
        Me.dtpPossibleStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPossibleStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPossibleStartDate.Location = New System.Drawing.Point(143, 312)
        Me.dtpPossibleStartDate.Name = "dtpPossibleStartDate"
        Me.dtpPossibleStartDate.ShowCheckBox = True
        Me.dtpPossibleStartDate.Size = New System.Drawing.Size(121, 21)
        Me.dtpPossibleStartDate.TabIndex = 97
        '
        'lblPossibleStartDate
        '
        Me.lblPossibleStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPossibleStartDate.Location = New System.Drawing.Point(11, 317)
        Me.lblPossibleStartDate.Name = "lblPossibleStartDate"
        Me.lblPossibleStartDate.Size = New System.Drawing.Size(126, 15)
        Me.lblPossibleStartDate.TabIndex = 96
        Me.lblPossibleStartDate.Text = "Earliest Possible St. Date"
        Me.lblPossibleStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVacancyFoudOutFrom
        '
        Me.lblVacancyFoudOutFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyFoudOutFrom.Location = New System.Drawing.Point(9, 285)
        Me.lblVacancyFoudOutFrom.Name = "lblVacancyFoudOutFrom"
        Me.lblVacancyFoudOutFrom.Size = New System.Drawing.Size(128, 20)
        Me.lblVacancyFoudOutFrom.TabIndex = 95
        Me.lblVacancyFoudOutFrom.Text = "Vacancy Foud Out From"
        Me.lblVacancyFoudOutFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVacancyFoudOutFrom
        '
        Me.txtVacancyFoudOutFrom.BackColor = System.Drawing.Color.White
        Me.txtVacancyFoudOutFrom.Flags = 0
        Me.txtVacancyFoudOutFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVacancyFoudOutFrom.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVacancyFoudOutFrom.Location = New System.Drawing.Point(367, 285)
        Me.txtVacancyFoudOutFrom.Name = "txtVacancyFoudOutFrom"
        Me.txtVacancyFoudOutFrom.Size = New System.Drawing.Size(395, 21)
        Me.txtVacancyFoudOutFrom.TabIndex = 94
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.DropDownWidth = 400
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(143, 231)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(249, 21)
        Me.cboVacancyType.TabIndex = 92
        '
        'lblVacancyType
        '
        Me.lblVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyType.Location = New System.Drawing.Point(11, 234)
        Me.lblVacancyType.Name = "lblVacancyType"
        Me.lblVacancyType.Size = New System.Drawing.Size(126, 15)
        Me.lblVacancyType.TabIndex = 91
        Me.lblVacancyType.Text = "Vacancy Type"
        Me.lblVacancyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVacancy
        '
        Me.lblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancy.Location = New System.Drawing.Point(11, 261)
        Me.lblVacancy.Name = "lblVacancy"
        Me.lblVacancy.Size = New System.Drawing.Size(126, 15)
        Me.lblVacancy.TabIndex = 89
        Me.lblVacancy.Text = "Vacancy"
        Me.lblVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(398, 258)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 88
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 400
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(143, 258)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(249, 21)
        Me.cboVacancy.TabIndex = 4
        '
        'lvOldVacancy
        '
        Me.lvOldVacancy.BackColorOnChecked = False
        Me.lvOldVacancy.ColumnHeaders = Nothing
        Me.lvOldVacancy.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhVacancy, Me.objcolhApplicant, Me.colhVacancyFoundOutFrom, Me.colhPossibleStartDate, Me.colhComments})
        Me.lvOldVacancy.CompulsoryColumns = ""
        Me.lvOldVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvOldVacancy.FullRowSelect = True
        Me.lvOldVacancy.GridLines = True
        Me.lvOldVacancy.GroupingColumn = Nothing
        Me.lvOldVacancy.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvOldVacancy.HideSelection = False
        Me.lvOldVacancy.Location = New System.Drawing.Point(11, 61)
        Me.lvOldVacancy.MinColumnWidth = 50
        Me.lvOldVacancy.MultiSelect = False
        Me.lvOldVacancy.Name = "lvOldVacancy"
        Me.lvOldVacancy.OptionalColumns = ""
        Me.lvOldVacancy.ShowMoreItem = False
        Me.lvOldVacancy.ShowSaveItem = False
        Me.lvOldVacancy.ShowSelectAll = True
        Me.lvOldVacancy.ShowSizeAllColumnsToFit = True
        Me.lvOldVacancy.Size = New System.Drawing.Size(752, 164)
        Me.lvOldVacancy.Sortable = True
        Me.lvOldVacancy.TabIndex = 3
        Me.lvOldVacancy.UseCompatibleStateImageBehavior = False
        Me.lvOldVacancy.View = System.Windows.Forms.View.Details
        '
        'colhVacancy
        '
        Me.colhVacancy.Tag = "colhVacancy"
        Me.colhVacancy.Text = "Vacancy"
        Me.colhVacancy.Width = 415
        '
        'objcolhApplicant
        '
        Me.objcolhApplicant.Tag = "objcolhApplicant"
        Me.objcolhApplicant.Text = ""
        Me.objcolhApplicant.Width = 0
        '
        'colhVacancyFoundOutFrom
        '
        Me.colhVacancyFoundOutFrom.Tag = "colhVacancyFoundOutFrom"
        Me.colhVacancyFoundOutFrom.Text = "Found out From"
        Me.colhVacancyFoundOutFrom.Width = 90
        '
        'colhPossibleStartDate
        '
        Me.colhPossibleStartDate.Tag = "colhPossibleStartDate"
        Me.colhPossibleStartDate.Text = "Possible Start Date"
        Me.colhPossibleStartDate.Width = 110
        '
        'colhComments
        '
        Me.colhComments.Tag = "colhComments"
        Me.colhComments.Text = "Comments"
        Me.colhComments.Width = 120
        '
        'lblApplicant
        '
        Me.lblApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicant.Location = New System.Drawing.Point(8, 36)
        Me.lblApplicant.Name = "lblApplicant"
        Me.lblApplicant.Size = New System.Drawing.Size(109, 15)
        Me.lblApplicant.TabIndex = 2
        Me.lblApplicant.Text = "Applicant"
        Me.lblApplicant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtApplicant
        '
        Me.txtApplicant.BackColor = System.Drawing.Color.White
        Me.txtApplicant.Flags = 0
        Me.txtApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicant.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplicant.Location = New System.Drawing.Point(123, 33)
        Me.txtApplicant.Name = "txtApplicant"
        Me.txtApplicant.ReadOnly = True
        Me.txtApplicant.Size = New System.Drawing.Size(639, 21)
        Me.txtApplicant.TabIndex = 1
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 416)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(775, 55)
        Me.EZeeFooter1.TabIndex = 159
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(665, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 156
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(562, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 157
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'cboVacancyFoudOutFrom
        '
        Me.cboVacancyFoudOutFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyFoudOutFrom.DropDownWidth = 400
        Me.cboVacancyFoudOutFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyFoudOutFrom.FormattingEnabled = True
        Me.cboVacancyFoudOutFrom.Location = New System.Drawing.Point(143, 285)
        Me.cboVacancyFoudOutFrom.Name = "cboVacancyFoudOutFrom"
        Me.cboVacancyFoudOutFrom.Size = New System.Drawing.Size(121, 21)
        Me.cboVacancyFoudOutFrom.TabIndex = 101
        '
        'chkOtherVFF
        '
        Me.chkOtherVFF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOtherVFF.Location = New System.Drawing.Point(270, 288)
        Me.chkOtherVFF.Name = "chkOtherVFF"
        Me.chkOtherVFF.Size = New System.Drawing.Size(91, 17)
        Me.chkOtherVFF.TabIndex = 1
        Me.chkOtherVFF.Text = "Others"
        Me.chkOtherVFF.UseVisualStyleBackColor = True
        '
        'frmVacancyMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(775, 471)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmVacancyMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Vacancy Mapping"
        Me.pnlMain.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblApplicant As System.Windows.Forms.Label
    Friend WithEvents txtApplicant As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lvOldVacancy As eZee.Common.eZeeListView
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancy As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents colhVacancy As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhApplicant As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancyType As System.Windows.Forms.Label
    Friend WithEvents lblVacancyFoudOutFrom As System.Windows.Forms.Label
    Friend WithEvents txtVacancyFoudOutFrom As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPossibleStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpPossibleStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtComments As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblComments As System.Windows.Forms.Label
    Friend WithEvents colhVacancyFoundOutFrom As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPossibleStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhComments As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboVacancyFoudOutFrom As System.Windows.Forms.ComboBox
    Friend WithEvents chkOtherVFF As System.Windows.Forms.CheckBox
End Class

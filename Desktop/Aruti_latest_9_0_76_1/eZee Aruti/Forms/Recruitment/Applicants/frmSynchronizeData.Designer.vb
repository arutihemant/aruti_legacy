﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSynchronizeData
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSynchronizeData))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.lblProgress = New System.Windows.Forms.Label
        Me.objeZeeWait = New eZee.Common.eZeeWait
        Me.btnStart = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbgwExport_Import = New System.ComponentModel.BackgroundWorker
        Me.objntfSyncData = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.chkSendApplicantJobAlerts = New System.Windows.Forms.CheckBox
        Me.Panel1.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkSendApplicantJobAlerts)
        Me.Panel1.Controls.Add(Me.objlblCaption)
        Me.Panel1.Controls.Add(Me.EZeeFooter1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(447, 171)
        Me.Panel1.TabIndex = 0
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.ForeColor = System.Drawing.Color.Maroon
        Me.objlblCaption.Location = New System.Drawing.Point(12, 9)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(423, 102)
        Me.objlblCaption.TabIndex = 3
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.lblProgress)
        Me.EZeeFooter1.Controls.Add(Me.objeZeeWait)
        Me.EZeeFooter1.Controls.Add(Me.btnStart)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 121)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(447, 50)
        Me.EZeeFooter1.TabIndex = 2
        '
        'lblProgress
        '
        Me.lblProgress.Location = New System.Drawing.Point(94, 19)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(111, 13)
        Me.lblProgress.TabIndex = 5
        '
        'objeZeeWait
        '
        Me.objeZeeWait.Active = False
        Me.objeZeeWait.CircleRadius = 19
        Me.objeZeeWait.Location = New System.Drawing.Point(0, 3)
        Me.objeZeeWait.Name = "objeZeeWait"
        Me.objeZeeWait.NumberSpoke = 10
        Me.objeZeeWait.RotationSpeed = 100
        Me.objeZeeWait.Size = New System.Drawing.Size(52, 46)
        Me.objeZeeWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.objeZeeWait.SpokeHeight = 7
        Me.objeZeeWait.SpokeThickness = 6
        Me.objeZeeWait.TabIndex = 4
        '
        'btnStart
        '
        Me.btnStart.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStart.BackColor = System.Drawing.Color.White
        Me.btnStart.BackgroundImage = CType(resources.GetObject("btnStart.BackgroundImage"), System.Drawing.Image)
        Me.btnStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnStart.BorderColor = System.Drawing.Color.Empty
        Me.btnStart.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnStart.FlatAppearance.BorderSize = 0
        Me.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStart.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStart.ForeColor = System.Drawing.Color.Black
        Me.btnStart.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnStart.GradientForeColor = System.Drawing.Color.Black
        Me.btnStart.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStart.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnStart.Location = New System.Drawing.Point(224, 10)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStart.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnStart.Size = New System.Drawing.Size(97, 30)
        Me.btnStart.TabIndex = 2
        Me.btnStart.Text = "&Start"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(327, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(108, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "Stop && &Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbgwExport_Import
        '
        Me.objbgwExport_Import.WorkerReportsProgress = True
        Me.objbgwExport_Import.WorkerSupportsCancellation = True
        '
        'objntfSyncData
        '
        Me.objntfSyncData.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.objntfSyncData.Icon = CType(resources.GetObject("objntfSyncData.Icon"), System.Drawing.Icon)
        Me.objntfSyncData.Visible = True
        '
        'chkSendApplicantJobAlerts
        '
        Me.chkSendApplicantJobAlerts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSendApplicantJobAlerts.Location = New System.Drawing.Point(15, 94)
        Me.chkSendApplicantJobAlerts.Name = "chkSendApplicantJobAlerts"
        Me.chkSendApplicantJobAlerts.Size = New System.Drawing.Size(217, 17)
        Me.chkSendApplicantJobAlerts.TabIndex = 4
        Me.chkSendApplicantJobAlerts.Text = "Send Job Alerts to Applicant"
        Me.chkSendApplicantJobAlerts.UseVisualStyleBackColor = True
        '
        'frmSynchronizeData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(447, 171)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSynchronizeData"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Panel1.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnStart As eZee.Common.eZeeLightButton
    Friend WithEvents objeZeeWait As eZee.Common.eZeeWait
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents objbgwExport_Import As System.ComponentModel.BackgroundWorker
    Friend WithEvents objntfSyncData As System.Windows.Forms.NotifyIcon
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents chkSendApplicantJobAlerts As System.Windows.Forms.CheckBox
End Class

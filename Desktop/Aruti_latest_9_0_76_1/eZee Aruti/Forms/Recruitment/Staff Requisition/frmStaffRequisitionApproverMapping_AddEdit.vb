﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmStaffRequisitionApproverMapping_AddEdit

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmStaffRequisitionApproverMapping_AddEdit"
    Private mblnCancel As Boolean = True
    Private objStaffRequisitionApprover As clsStaffRequisition_approver_mapping
    Private menAction As enAction = enAction.ADD_ONE
    Private mintStaffRequisitionApproverUnkid As Integer = -1
    Private mintSelectedLevelId As Integer = 0
    'Hemant (03 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
    Private mdtNewTable As DataTable = Nothing
    Private mdtOldTable As DataTable = Nothing
    Private marrOldStaffRequisitionApprover As New ArrayList
    Private mdsAllocationList As DataSet = Nothing
    'Hemant (03 Sep 2019) -- End
    Private mdtMainOldTable As DataTable = Nothing     'Hemant (24 Sep 2019)
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintStaffRequisitionApproverUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintStaffRequisitionApproverUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboApproverUser.BackColor = GUI.ColorComp
            cboLevel.BackColor = GUI.ColorComp
            cboAllocation.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Combo()
        Dim objUsr As New clsUserAddEdit
        Dim objLevel As New clsStaffRequisitionApproverlevel_master
        Dim objOption As New clsPassowdOptions
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet = Nothing
        Try
            dsList = objLevel.getListForCombo("List", True)
            With cboLevel
                .ValueMember = "levelunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            Dim intPrivilegeId As Integer = 843
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'Select Case objOption._UserLogingModeId
            '    Case enAuthenticationMode.BASIC_AUTHENTICATION
            '        If objOption._IsEmployeeAsUser Then
            '            dsList = objUsr.getComboList("List", True, False, True, Company._Object._Companyunkid, intPrivilegeId, FinancialYear._Object._YearUnkid)
            '        Else
            '            dsList = objUsr.getComboList("List", True, False, , Company._Object._Companyunkid, intPrivilegeId)
            '        End If
            '    Case enAuthenticationMode.AD_BASIC_AUTHENTICATION, enAuthenticationMode.AD_SSO_AUTHENTICATION
            '        dsList = objUsr.getComboList("List", True, True, , Company._Object._Companyunkid, intPrivilegeId)
            'End Select

            'Nilay (01-Mar-2016) -- Start
            'dsList = objUsr.getNewComboList("List", , True, Company._Object._Companyunkid, intPrivilegeId)
            dsList = objUsr.getNewComboList("List", , True, Company._Object._Companyunkid, intPrivilegeId.ToString(), FinancialYear._Object._YearUnkid, False)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            With cboApproverUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With

            dsList = objMaster.GetEAllocation_Notification("List")
            With cboAllocation
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsList.Dispose() : objUsr = Nothing : objLevel = Nothing : objOption = Nothing : objMaster = Nothing
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboLevel.SelectedValue = objStaffRequisitionApprover._Levelunkid
            cboApproverUser.SelectedValue = objStaffRequisitionApprover._Userapproverunkid
            cboAllocation.SelectedValue = objStaffRequisitionApprover._AllocationId     'Hemant (03 Sep 2019)
            cboAllocation_SelectedIndexChanged(cboAllocation, New EventArgs) 'Hemant (23 Sep 2019)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objStaffRequisitionApprover._Levelunkid = CInt(cboLevel.SelectedValue)
            objStaffRequisitionApprover._Userapproverunkid = CInt(cboApproverUser.SelectedValue)
            objStaffRequisitionApprover._AllocationId = CInt(cboAllocation.SelectedValue)
            objStaffRequisitionApprover._AllocationUnkId = CInt(lvAllocationList.CheckedItems(0).Tag)
            Dim lstChecked As List(Of String) = (From p In lvAllocationList.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToList
            objStaffRequisitionApprover._AllocationUnkIDs = String.Join(",", lstChecked.ToArray)

            If menAction = enAction.EDIT_ONE Then
                objStaffRequisitionApprover._Isvoid = objStaffRequisitionApprover._Isvoid
                objStaffRequisitionApprover._Voiddatetime = objStaffRequisitionApprover._Voiddatetime
                objStaffRequisitionApprover._Voiduserunkid = objStaffRequisitionApprover._Voiduserunkid
            Else
                objStaffRequisitionApprover._Isvoid = False
                objStaffRequisitionApprover._Voiddatetime = Nothing
                objStaffRequisitionApprover._Voiduserunkid = -1
            End If
            objStaffRequisitionApprover._Userunkid = User._Object._Userunkid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Staff Requisition Approver Level is mandatory information. Please select Staff Requisition Approver Level."), enMsgBoxStyle.Information)
                cboLevel.Focus()
                Return False
            ElseIf CInt(cboApproverUser.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Staff Requisition Approver User is mandatory information. Please select Staff Requisition Approver User."), enMsgBoxStyle.Information)
                cboApproverUser.Focus()
                Return False
            ElseIf lvAllocationList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select atleast one item from the list."), enMsgBoxStyle.Information)
                lvAllocationList.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvAllocationList.Items
                RemoveHandler lvAllocationList.ItemChecked, AddressOf lvAllocationList_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvAllocationList.ItemChecked, AddressOf lvAllocationList_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form's Event "

    Private Sub frmStaffRequisitionApproverMapping_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objStaffRequisitionApprover = New clsStaffRequisition_approver_mapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Call SetColor()

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call Fill_Combo()

            If menAction = enAction.EDIT_ONE Then
                objStaffRequisitionApprover._StaffRequisitionapproverunkid = mintStaffRequisitionApproverUnkid
                'Hemant (03 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
                'Hemant (24 Sep 2019) -- Start
                'cboLevel.Enabled = False
                'objbtnSearchLevel.Enabled = False
                'cboApproverUser.Enabled = False
                'objbtnSearchUser.Enabled = False
                'cboAllocation.Enabled = False
                Dim StrSearch As String = String.Empty
                Dim dsList As New DataSet
                If CInt(objStaffRequisitionApprover._Levelunkid) > 0 Then
                    StrSearch &= "AND rcstaffrequisition_approver_mapping.levelunkid = " & CInt(objStaffRequisitionApprover._Levelunkid) & " "
                End If

                If CInt(objStaffRequisitionApprover._Userapproverunkid) > 0 Then
                    StrSearch &= "AND rcstaffrequisition_approver_mapping.userapproverunkid = " & CInt(objStaffRequisitionApprover._Userapproverunkid) & " "
                End If

                If CInt(objStaffRequisitionApprover._AllocationId) > 0 Then
                    StrSearch &= "AND rcstaffrequisition_approver_mapping.allocationid = " & CInt(objStaffRequisitionApprover._AllocationId) & " "
                End If

                If StrSearch.Trim.Length > 0 Then StrSearch = StrSearch.Substring(3)

                dsList = objStaffRequisitionApprover.GetList("List", , , , StrSearch, "rcstaffrequisitionlevel_master.levelname")
                mdtMainOldTable = mdtOldTable.Clone
                Dim dRow As DataRow
                mdtMainOldTable.Clear()
                For Each drRow As DataRow In dsList.Tables(0).Rows
                    dRow = mdtMainOldTable.NewRow
                    dRow.Item("staffrequisitionapproverunkid") = drRow.Item("staffrequisitionapproverunkid")
                    dRow.Item("levelunkid") = drRow.Item("levelunkid")
                    dRow.Item("userapproverunkid") = drRow.Item("userapproverunkid")
                    dRow.Item("allocationid") = drRow.Item("allocationid")
                    dRow.Item("allocationunkid") = drRow.Item("allocationunkid")
                    dRow.Item("userunkid") = drRow.Item("userunkid")
                    dRow.Item("isvoid") = drRow.Item("isvoid")
                    dRow.Item("voiduserunkid") = drRow.Item("voiduserunkid")
                    dRow.Item("voiddatetime") = drRow.Item("voiddatetime")
                    dRow.Item("voidreason") = drRow.Item("voidreason")
                    dRow.Item("AUD") = ""

                    mdtMainOldTable.Rows.Add(dRow)
                Next
                'Hemant (24 Sep 2019) -- End
                'Hemant (03 Sep 2019) -- End
            End If

            GetValue()
            cboLevel.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffRequisitionApproverMapping_AddEdit_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub frmStaffRequisitionApproverMapping_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            ElseIf e.KeyCode = Keys.Return Then
                SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffRequisitionApproverMapping_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStaffRequisitionApproverMapping_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objStaffRequisitionApprover = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsStaffRequisition_approver_mapping.SetMessages()
            objfrm._Other_ModuleNames = "clsStaffRequisition_approver_mapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub

            Call SetValue()

            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
            If menAction = enAction.EDIT_ONE Then

                mdtNewTable = mdtOldTable.Clone

                Dim marrNewStaffRequisitionApprover As New ArrayList
                marrNewStaffRequisitionApprover.AddRange((From p In lvAllocationList.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)

                'Hemant (24 Sep 2019) -- Start
                'Dim add() As String = (From p In marrNewStaffRequisitionApprover Where (marrOldStaffRequisitionApprover.Contains(p.ToString) = False) Select (p.ToString)).ToArray
                'Dim del() As String = (From p In marrOldStaffRequisitionApprover Where (marrNewStaffRequisitionApprover.Contains(p.ToString) = False) Select (p.ToString)).ToArray

                'Dim dr_Row As List(Of DataRow) = Nothing
                'Dim dRow As DataRow

                'dr_Row = (From p In mdtOldTable Where (del.Contains(p.Item("allocationunkid").ToString) = True)).ToList

                'If dr_Row IsNot Nothing Then
                '    For Each row As DataRow In dr_Row
                '        dRow = mdtNewTable.NewRow

                '        dRow.Item("staffrequisitionapproverunkid") = CInt(row.Item("staffrequisitionapproverunkid"))
                '        dRow.Item("levelunkid") = CInt(row.Item("levelunkid"))
                '        dRow.Item("userapproverunkid") = CInt(row.Item("userapproverunkid"))
                '        dRow.Item("allocationid") = CInt(row.Item("allocationid"))
                '        dRow.Item("allocationunkid") = CInt(row.Item("allocationunkid"))
                '        dRow.Item("userunkid") = CInt(row.Item("userunkid"))
                '        dRow.Item("isvoid") = True
                '        dRow.Item("voiduserunkid") = User._Object._Userunkid
                '        dRow.Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                '        dRow.Item("voidreason") = ""
                '        dRow.Item("AUD") = "D"

                '        mdtNewTable.Rows.Add(dRow)
                '    Next
                'End If

                'Dim dr As List(Of DataRow) = (From p In mdsAllocationList.Tables(0).AsEnumerable Where (add.Contains(p.Item("id").ToString) = True)).ToList

                'If dr IsNot Nothing Then
                '    For Each dtRow In dr
                '        dRow = mdtNewTable.NewRow

                '        dRow.Item("staffrequisitionapproverunkid") = -1
                '        dRow.Item("levelunkid") = CInt(cboLevel.SelectedValue)
                '        dRow.Item("userapproverunkid") = CInt(cboApproverUser.SelectedValue)
                '        dRow.Item("allocationid") = CInt(cboAllocation.SelectedValue)
                '        dRow.Item("allocationunkid") = CInt(dtRow.Item("id"))
                '        dRow.Item("userunkid") = User._Object._Userunkid
                '        dRow.Item("isvoid") = False
                '        dRow.Item("voiduserunkid") = -1
                '        dRow.Item("voiddatetime") = DBNull.Value
                '        dRow.Item("voidreason") = ""
                '        dRow.Item("AUD") = "A"

                '        mdtNewTable.Rows.Add(dRow)

                '    Next
                'End If

                Dim drRow As DataRow
                If mdtOldTable IsNot Nothing Then
                    For Each row As DataRow In mdtOldTable.Rows
                        drRow = mdtNewTable.NewRow

                        drRow.Item("staffrequisitionapproverunkid") = CInt(row.Item("staffrequisitionapproverunkid"))
                        drRow.Item("levelunkid") = CInt(row.Item("levelunkid"))
                        drRow.Item("userapproverunkid") = CInt(row.Item("userapproverunkid"))
                        drRow.Item("allocationid") = CInt(row.Item("allocationid"))
                        drRow.Item("allocationunkid") = CInt(row.Item("allocationunkid"))
                        drRow.Item("userunkid") = CInt(row.Item("userunkid"))
                        If CInt(drRow.Item("levelunkid")) <> CInt(cboLevel.SelectedValue) OrElse _
                           CInt(drRow.Item("userapproverunkid")) <> CInt(cboApproverUser.SelectedValue) OrElse _
                           CInt(drRow.Item("allocationid")) <> CInt(cboAllocation.SelectedValue) Then

                            drRow.Item("isvoid") = True
                            drRow.Item("voiduserunkid") = User._Object._Userunkid
                            drRow.Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drRow.Item("voidreason") = ""
                            drRow.Item("AUD") = "D"
                        ElseIf CInt(drRow.Item("levelunkid")) = CInt(cboLevel.SelectedValue) AndAlso _
                           CInt(drRow.Item("userapproverunkid")) = CInt(cboApproverUser.SelectedValue) AndAlso _
                           CInt(drRow.Item("allocationid")) = CInt(cboAllocation.SelectedValue) AndAlso _
                           marrNewStaffRequisitionApprover.Contains(row.Item("allocationunkid").ToString) = False Then

                            drRow.Item("isvoid") = True
                            drRow.Item("voiduserunkid") = User._Object._Userunkid
                            drRow.Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drRow.Item("voidreason") = ""
                            drRow.Item("AUD") = "D"
                        Else
                            drRow.Item("isvoid") = False
                            drRow.Item("voiduserunkid") = -1
                            drRow.Item("voiddatetime") = DBNull.Value
                            drRow.Item("voidreason") = ""
                            drRow.Item("AUD") = ""
                        End If

                        mdtNewTable.Rows.Add(drRow)
                    Next
                End If
               
                For Each iRow As Integer In marrNewStaffRequisitionApprover
                    If mdtOldTable.Select("levelunkid = " & CInt(cboLevel.SelectedValue) & " AND userapproverunkid = " & CInt(cboApproverUser.SelectedValue) & " AND allocationid = " & CInt(cboAllocation.SelectedValue) & " AND allocationunkid = " & CInt(iRow) & " ").Count <= 0 Then
                        drRow = mdtNewTable.NewRow

                        drRow.Item("staffrequisitionapproverunkid") = -1
                        drRow.Item("levelunkid") = CInt(cboLevel.SelectedValue)
                        drRow.Item("userapproverunkid") = CInt(cboApproverUser.SelectedValue)
                        drRow.Item("allocationid") = CInt(cboAllocation.SelectedValue)
                        drRow.Item("allocationunkid") = iRow
                        drRow.Item("userunkid") = User._Object._Userunkid
                        drRow.Item("isvoid") = False
                        drRow.Item("voiduserunkid") = -1
                        drRow.Item("voiddatetime") = DBNull.Value
                        drRow.Item("voidreason") = ""
                        drRow.Item("AUD") = "A"

                        mdtNewTable.Rows.Add(drRow)
                    End If
                Next
                'Hemant (24 Sep 2019) -- End
            End If
            'Hemant (03 Sep 2019) -- End

            If menAction = enAction.EDIT_ONE Then
                'Hemant (03 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
                'blnFlag = objStaffRequisitionApprover.Update()
                blnFlag = objStaffRequisitionApprover.InsertUpdateDelete(mdtNewTable, ConfigParameter._Object._CurrentDateAndTime)
                'Hemant (03 Sep 2019) -- End
            Else
                blnFlag = objStaffRequisitionApprover.InsertAll()
            End If

            If blnFlag = False And objStaffRequisitionApprover._Message <> "" Then
                eZeeMsgBox.Show(objStaffRequisitionApprover._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objStaffRequisitionApprover = Nothing
                    objStaffRequisitionApprover = New clsStaffRequisition_approver_mapping
                    Call GetValue() : cboLevel.SelectedValue = mintSelectedLevelId
                    objchkSelectAll.Checked = False
                Else
                    mintStaffRequisitionApproverUnkid = objStaffRequisitionApprover._StaffRequisitionapproverunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If


            With objFrm
                .ValueMember = cboApproverUser.ValueMember
                .DisplayMember = cboApproverUser.DisplayMember
                'Sohail (17 Apr 2020) -- Start
                'NMB Issue # : On staff requisition approver mapping, when searching for the staff requisition approver, the error is coming.
                '.CodeMember = "code"
                .CodeMember = "Display"
                'Sohail (17 Apr 2020) -- End
                .DataSource = CType(cboApproverUser.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboApproverUser.SelectedValue = objFrm.SelectedValue
            End If
            cboApproverUser.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If


            With objFrm
                .ValueMember = cboLevel.ValueMember
                .CodeMember = cboLevel.DisplayMember
                .DisplayMember = "Display"
                .DataSource = CType(cboApproverUser.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboApproverUser.SelectedValue = objFrm.SelectedValue
            End If
            cboApproverUser.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLevel.SelectedIndexChanged
        Try
            If CInt(cboLevel.SelectedValue) > 0 Then mintSelectedLevelId = CInt(cboLevel.SelectedValue)
            cboAllocation_SelectedIndexChanged(cboAllocation, New EventArgs)     'Hemant (24 Sep 2019)
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Hemant (24 Sep 2019) -- Start
    Private Sub cboApproverUser_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboApproverUser.SelectedIndexChanged
        Try
            If CInt(cboApproverUser.SelectedValue) > 0 Then cboAllocation_SelectedIndexChanged(cboAllocation, New EventArgs)
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboApproverUser_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (24 Sep 2019) -- End

    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Dim dsCombos As DataSet = Nothing
        'Hemant (03 Sep 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
        Dim dsList As New DataSet
        Dim StrSearch As String = String.Empty
        'Hemant (03 Sep 2019) -- End
        Try

            lvAllocationList.Items.Clear()

            Select Case CInt(cboAllocation.SelectedValue)

                Case enAllocation.BRANCH
                    dsCombos = objStation.getComboList("Station")
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.DEPARTMENT_GROUP
                    dsCombos = objDeptGrp.getComboList("DeptGrp")
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.DEPARTMENT
                    dsCombos = objDepartment.getComboList("Department")
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.SECTION_GROUP
                    dsCombos = objSectionGrp.getComboList("List")
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.SECTION
                    dsCombos = objSection.getComboList("Section")
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.UNIT_GROUP
                    dsCombos = objUnitGroup.getComboList("List")
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.UNIT
                    dsCombos = objUnit.getComboList("Unit")
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.TEAM
                    dsCombos = objTeam.getComboList("List")
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.JOB_GROUP
                    dsCombos = objJobGrp.getComboList("JobGrp")
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.JOBS
                    dsCombos = objJob.getComboList("Job", True)
                    dsCombos.Tables(0).Rows.RemoveAt(0)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.CLASS_GROUP
                    dsCombos = objClassGrp.getComboList("ClassGrp")
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                Case enAllocation.CLASSES
                    dsCombos = objClass.getComboList("Class")
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"

            End Select

            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
            mdsAllocationList = dsCombos

            Dim strAllocationIDs As String = String.Empty
            If menAction = enAction.EDIT_ONE Then
                If CInt(cboLevel.SelectedValue) > 0 Then
                    'Hemant (24 Sep 2019) -- Start
                    'StrSearch &= "AND rcstaffrequisition_approver_mapping.levelunkid = " & CInt(objStaffRequisitionApprover._Levelunkid) & " "
                    StrSearch &= "AND rcstaffrequisition_approver_mapping.levelunkid = " & CInt(cboLevel.SelectedValue) & " "
                    'Hemant (24 Sep 2019) -- End
                End If

                If CInt(cboApproverUser.SelectedValue) > 0 Then
                    'Hemant (24 Sep 2019) -- Start
                    'StrSearch &= "AND rcstaffrequisition_approver_mapping.userapproverunkid = " & CInt(objStaffRequisitionApprover._Userapproverunkid) & " "
                    StrSearch &= "AND rcstaffrequisition_approver_mapping.userapproverunkid = " & CInt(cboApproverUser.SelectedValue) & " "
                    'Hemant (24 Sep 2019) -- End
                End If

                If CInt(cboAllocation.SelectedValue) > 0 Then
                    StrSearch &= "AND rcstaffrequisition_approver_mapping.allocationid = " & CInt(cboAllocation.SelectedValue) & " "
                End If

                If StrSearch.Trim.Length > 0 Then StrSearch = StrSearch.Substring(3)

                dsList = objStaffRequisitionApprover.GetList("List", , , , StrSearch, "rcstaffrequisitionlevel_master.levelname")
                mdtOldTable = objStaffRequisitionApprover._Datasource
                Dim dRow As DataRow
                mdtOldTable.Clear()
                For Each drRow As DataRow In dsList.Tables(0).Rows
                    dRow = mdtOldTable.NewRow
                    dRow.Item("staffrequisitionapproverunkid") = drRow.Item("staffrequisitionapproverunkid")
                    dRow.Item("levelunkid") = drRow.Item("levelunkid")
                    dRow.Item("userapproverunkid") = drRow.Item("userapproverunkid")
                    dRow.Item("allocationid") = drRow.Item("allocationid")
                    dRow.Item("allocationunkid") = drRow.Item("allocationunkid")
                    dRow.Item("userunkid") = drRow.Item("userunkid")
                    dRow.Item("isvoid") = drRow.Item("isvoid")
                    dRow.Item("voiduserunkid") = drRow.Item("voiduserunkid")
                    dRow.Item("voiddatetime") = drRow.Item("voiddatetime")
                    dRow.Item("voidreason") = drRow.Item("voidreason")
                    dRow.Item("AUD") = ""

                    mdtOldTable.Rows.Add(dRow)
                Next
                strAllocationIDs = String.Join(",", (From p In mdtOldTable Select (p.Item("allocationunkid").ToString)).ToArray())
                marrOldStaffRequisitionApprover.Clear()
                marrOldStaffRequisitionApprover.AddRange((From p In mdtOldTable Select (p.Item("allocationunkid").ToString)).ToArray)
                'Hemant (24 Sep 2019) -- Start
                If mdtMainOldTable IsNot Nothing AndAlso mdtMainOldTable.Rows.Count > 0 Then
                    mdtOldTable.Merge(mdtMainOldTable)
                    mdtOldTable = mdtOldTable.DefaultView.ToTable(True)
                End If
                'Hemant (24 Sep 2019) -- End
            End If
            'Hemant (03 Sep 2019) -- End

            Dim lvItem As ListViewItem

            If dsCombos IsNot Nothing Then

                For Each dsRow As DataRow In dsCombos.Tables(0).Rows
                    lvItem = New ListViewItem

                    lvItem.Text = ""
                    lvItem.Tag = CInt(dsRow.Item("Id"))

                    lvItem.SubItems.Add(dsRow.Item("Name").ToString)

                    'Hemant (03 Sep 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
                    'Hemant (24 Sep 2019) -- Start
                    'If strAllocationIDs.Contains(dsRow.Item("Id").ToString) = True Then
                    If marrOldStaffRequisitionApprover.Contains(dsRow.Item("Id").ToString) = True Then
                        'Hemant (24 Sep 2019) -- End
                        lvItem.Checked = True
                    Else
                        lvItem.Checked = False
                    End If
                    'Hemant (03 Sep 2019) -- End

                    RemoveHandler lvAllocationList.ItemChecked, AddressOf lvAllocationList_ItemChecked
                    lvAllocationList.Items.Add(lvItem)
                    AddHandler lvAllocationList.ItemChecked, AddressOf lvAllocationList_ItemChecked
                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocation_SelectedIndexChanged", mstrModuleName)
        Finally
            objStation = Nothing
            objDeptGrp = Nothing
            objDepartment = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objClassGrp = Nothing
            objClass = Nothing
            objSectionGrp = Nothing
            objUnitGroup = Nothing
            objTeam = Nothing
            dsCombos = Nothing
        End Try
    End Sub
#End Region

#Region " Listview Events "
    Private Sub lvAllocationList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocationList.ItemChecked
        Try
            If lvAllocationList.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvAllocationList.CheckedItems.Count < lvAllocationList.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvAllocationList.CheckedItems.Count = lvAllocationList.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAll(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Textbox Events "
    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If lvAllocationList.Items.Count <= 0 Then Exit Sub
            lvAllocationList.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvAllocationList.FindItemWithText(txtSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvAllocationList.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbApproverMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbApproverMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbApproverMapping.Text = Language._Object.getCaption(Me.gbApproverMapping.Name, Me.gbApproverMapping.Text)
			Me.lblApprLevel.Text = Language._Object.getCaption(Me.lblApprLevel.Name, Me.lblApprLevel.Text)
			Me.lblApprUser.Text = Language._Object.getCaption(Me.lblApprUser.Name, Me.lblApprUser.Text)
			Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
			Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.lblSearch.Text = Language._Object.getCaption(Me.lblSearch.Name, Me.lblSearch.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Staff Requisition Approver Level is mandatory information. Please select Staff Requisition Approver Level.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Staff Requisition Approver User is mandatory information. Please select Staff Requisition Approver User.")
			Language.setMessage(mstrModuleName, 3, "Please select atleast one item from the list.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportAccount

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportAccount"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Dim mdicEmpAdded As New Dictionary(Of String, Integer)

#End Region

#Region " Private Functions & Procedures "
    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Function SetDataCombo() As Boolean
        Try
            For Each ctrl As Control In gbFiledMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                For Each ctrl As Control In gbFiledMapping.Controls
                    If TypeOf ctrl Is ComboBox Then
                        CType(ctrl, ComboBox).Items.Add(dtColumns.ColumnName)
                    End If
                Next
            Next

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

    Private Function SetupGrid() As Boolean
        Dim objMaster As New clsMasterData
        Dim ds As DataSet
        Try
            ds = objMaster.getComboListAccountGroup("AccGroup")
            dgvMapping.AutoGenerateColumns = False

            Dim cboHeads As DataGridViewComboBoxColumn
            cboHeads = CType(dgvMapping.Columns(colhDBAccGroup.Index), DataGridViewComboBoxColumn)
            With cboHeads
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                .DisplayMember = "name"
                .ValueMember = "id"
                .DataSource = ds.Tables("AccGroup")
            End With


            Dim dRow As DataGridViewRow
            dgvMapping.Rows.Clear()
            Dim dtTable As DataTable = New DataView(mds_ImportData.Tables(0)).ToTable(True, cboAccountGroup.Text)
            For Each dtRow As DataRow In dtTable.Rows

                If dtRow.Item(cboAccountGroup.Text).ToString = "" Then Continue For


                dRow = New DataGridViewRow
                dRow.CreateCells(dgvMapping)

                dRow.Cells(0).Value = dtRow.Item(cboAccountGroup.Text).ToString

                dgvMapping.Rows.Add(dRow)
            Next

            colhFileAccGroup.DataPropertyName = ""

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetupGrid", mstrModuleName)
        End Try
    End Function

    Private Function SetupData() As Boolean
        Dim objAccount As clsAccount_master
        Dim dRow() As DataRow
        Try
            If mds_ImportData.Tables(0).Columns.Contains("_accgrpid") = False Then
                mds_ImportData.Tables(0).Columns.Add("_accgrpid", System.Type.GetType("System.Int32")).DefaultValue = 0
            End If
            If mds_ImportData.Tables(0).Columns.Contains("_rowtypeid") = False Then
                mds_ImportData.Tables(0).Columns.Add("_rowtypeid", System.Type.GetType("System.Int32")).DefaultValue = 999
            End If
            If mds_ImportData.Tables(0).Columns.Contains("IsChecked") = False Then
                mds_ImportData.Tables(0).Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            End If

            For Each dgRow As DataGridViewRow In dgvMapping.Rows

                If Convert.ToInt32(dgRow.Cells(colhDBAccGroup.Index).Value) > 0 Then

                    dRow = mds_ImportData.Tables(0).Select(" " & cboAccountGroup.Text & " = '" & dgRow.Cells(colhFileAccGroup.Index).FormattedValue.ToString & "' ")
                    For Each dsRow As DataRow In dRow
                        dsRow.Item("_accgrpid") = CInt(dgRow.Cells(colhDBAccGroup.Index).Value)

                        objAccount = New clsAccount_master
                        If objAccount.isExist(dsRow.Item(cboAccountCode.Text).ToString, "") = True Then
                            dsRow.Item("_rowtypeid") = 1 ' Code Alrady Exist
                        ElseIf objAccount.isExist("", dsRow.Item(cboAccountName.Text).ToString) = True Then
                            dsRow.Item("_rowtypeid") = 2 ' Name Alrady Exist
                        Else
                            dsRow.Item("_rowtypeid") = 0 'Proper Entry
                        End If
                        dsRow.AcceptChanges()
                    Next
                End If
            Next


            dvGriddata = New DataView(mds_ImportData.Tables(0))

            dvGriddata.RowFilter = "_rowtypeid = 0 "

            Call FillGirdView()

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetupData", mstrModuleName)
        End Try
    End Function

    Private Sub FillGirdView()
        Dim objMaster As New clsMasterData
        Dim ds As DataSet
        Try
            For Each dtRow As DataRowView In dvGriddata
                dtRow.Item("IsChecked") = False
            Next

            dgData.AutoGenerateColumns = False

            ds = objMaster.getComboListAccountGroup("AccGroup")
            Dim cboAccGrp As DataGridViewComboBoxColumn
            cboAccGrp = CType(dgData.Columns(dgcolhAccGrp.Index), DataGridViewComboBoxColumn)
            With cboAccGrp
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                .ReadOnly = True
                .DisplayMember = "name"
                .ValueMember = "id"
                .DataSource = ds.Tables("AccGroup")
            End With

            objdgcolhCheck.DataPropertyName = "IsChecked"
            dgcolhAccCode.DataPropertyName = cboAccountCode.Text
            dgcolhAccName.DataPropertyName = cboAccountName.Text
            dgcolhAccGrp.DataPropertyName = "_accgrpid"

            dgData.DataSource = dvGriddata
            dgData.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
        Try
          
            If dvGriddata Is Nothing Then Exit Sub

            For Each dtRow As DataRowView In dvGriddata
                dtRow.Item("IsChecked") = chkSelectAll.Checked
            Next
            dgData.DataSource = dvGriddata
            dgData.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " From's Events "

    Private Sub frmImportAccount_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'Sohail (10 Jul 2014) -- Start
            'Enhancement - Custom Language.
            clsAccount_master.SetMessages()
            objfrm._Other_ModuleNames = "clsAccount_master"
            'Sohail (10 Jul 2014) -- End
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmImportAccount_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    Private Sub frmImportAccount_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            Call OtherSettings()

            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportAccount_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            'objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"|XML File(*.xml)|*.xml"
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim mdtFilteredTable As DataTable
        Dim objAccount As clsAccount_master
        Try
            If dvGriddata Is Nothing Then Exit Sub

            dvGriddata.RowFilter = "_rowtypeid = 0 "
            mdtFilteredTable = New DataView(dvGriddata.ToTable, "_rowtypeid = 0 AND  IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If mdtFilteredTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Tick atleast one Account from list to Import."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure, you want to Import selected Accounts?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.None Then
                Exit Try
            End If

            Cursor.Current = Cursors.WaitCursor
            For Each dtRow As DataRow In mdtFilteredTable.Rows
                objAccount = New clsAccount_master

                With objAccount
                    ._Account_Code = dtRow.Item(cboAccountCode.Text).ToString.Trim
                    ._Account_Name = dtRow.Item(cboAccountName.Text).ToString.Trim
                    ._Accountgroup_Id = CInt(dtRow.Item("_accgrpid"))
                    ._Isactive = True

                    If .Insert() = False AndAlso ._Message <> "" Then
                        eZeeMsgBox.Show(._Message)
                        Exit Sub
                    End If
                End With
            Next

            Cursor.Current = Cursors.Default

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Imported Successfully!"), enMsgBoxStyle.Information)

            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If dvGriddata Is Nothing Then Exit Sub

            Dim savDialog As New SaveFileDialog
            savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
            If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                If modGlobal.Export_ErrorList(savDialog.FileName, dvGriddata.ToTable, "Import Account Wizard") = True Then
                    Process.Start(savDialog.FileName)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Datagridview's Events "
    Private Sub dgvMapping_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMapping.CellEnter
        Try
            If e.ColumnIndex = colhDBAccGroup.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMapping.CellClick
        Try
            If e.ColumnIndex = objcolhSearch.Index Then
                If e.RowIndex < 0 Then Exit Sub

                Dim objfrm As New frmCommonSearch
                Dim objAccGroup As New clsMasterData
                Dim dsList As DataSet
                'If User._Object._RightToLeft = True Then
                '    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                '    objfrm.RightToLeftLayout = True
                '    Call Language.ctlRightToLeftlayOut(objfrm)
                'End If
                dsList = objAccGroup.getComboListAccountGroup("AccGrp")
                dsList.Tables("AccGrp").Columns.Add("code", System.Type.GetType("System.String")).DefaultValue = ""
                For Each dsRow As DataRow In dsList.Tables("AccGrp").Rows
                    dsRow.Item("code") = dsRow.Item("id").ToString
                    dsRow.AcceptChanges()
                Next

                objfrm.DataSource = dsList.Tables("AccGrp")
                objfrm.ValueMember = "id"
                objfrm.DisplayMember = "name"
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    Dim cb As DataGridViewComboBoxCell = TryCast(dgvMapping.CurrentRow.Cells(colhDBAccGroup.Index), DataGridViewComboBoxCell)
                    If cb IsNot Nothing Then
                        cb.Value = Convert.ToInt32(objfrm.SelectedValue)
                        SendKeys.Send("{LEFT}")
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_CellClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_CellMouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvMapping.CellMouseMove
        Try
            If e.RowIndex < 0 Then Exit Sub
            If e.ColumnIndex = objcolhSearch.Index Then
                Cursor.Current = Cursors.Hand
            Else
                Cursor.Current = Cursors.Default
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_CellMouseMove", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvMapping_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvMapping.DataError
        Try
            eZeeMsgBox.Show(e.Exception.Message, enMsgBoxStyle.Critical)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvMapping_DataError", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " eZee Wizard "
    Private Sub eWizAccount_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eWizAccount.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eWizAccount.Pages.IndexOf(ewpFileSelection)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)

                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    If SetDataCombo() = False Then
                        e.Cancel = True
                        Exit Sub
                    End If

                Case eWizAccount.Pages.IndexOf(ewpMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFiledMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                If CType(ctrl, ComboBox).Text = "" Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please the select proper field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    e.Cancel = True
                                    ctrl.Focus()
                                    Exit Select
                                End If
                            End If
                        Next
                        If SetupGrid() = False Then
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If

                Case eWizAccount.Pages.IndexOf(ewpGrpMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each dgvRow As DataGridViewRow In dgvMapping.Rows
                            If CInt(dgvRow.Cells(colhDBAccGroup.Index).Value) <= 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Map All Account Group to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                dgvRow.Cells(colhDBAccGroup.Index).Selected = True
                                e.Cancel = True
                                Exit Select
                            End If
                        Next
                        If SetupData() = False Then
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eWizAccount_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            Call CheckAll(chkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    Private Sub mnuShowSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowSuccessful.Click
        Try
            btnImport.Enabled = True
            If dvGriddata Is Nothing Then Exit Sub

            dvGriddata.RowFilter = "_rowtypeid = 0 "

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuCodeExist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuCodeExist.Click
        Try
            btnImport.Enabled = False
            If dvGriddata Is Nothing Then Exit Sub

            dvGriddata.RowFilter = "_rowtypeid = 1 "

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuCodeExist_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuNameExist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuNameExist.Click
        Try
            btnImport.Enabled = False
            If dvGriddata Is Nothing Then Exit Sub

            dvGriddata.RowFilter = "_rowtypeid = 2 "

            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFiledMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFiledMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbAccGroupMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAccGroupMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

			Me.btnFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor

			Me.btnImport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnImport.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eWizAccount.CancelText = Language._Object.getCaption(Me.eWizAccount.Name & "_CancelText" , Me.eWizAccount.CancelText)
			Me.eWizAccount.NextText = Language._Object.getCaption(Me.eWizAccount.Name & "_NextText" , Me.eWizAccount.NextText)
			Me.eWizAccount.BackText = Language._Object.getCaption(Me.eWizAccount.Name & "_BackText" , Me.eWizAccount.BackText)
			Me.eWizAccount.FinishText = Language._Object.getCaption(Me.eWizAccount.Name & "_FinishText" , Me.eWizAccount.FinishText)
			Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
			Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
			Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
			Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
			Me.gbFiledMapping.Text = Language._Object.getCaption(Me.gbFiledMapping.Name, Me.gbFiledMapping.Text)
			Me.lblAccountName.Text = Language._Object.getCaption(Me.lblAccountName.Name, Me.lblAccountName.Text)
			Me.lblAccountGroup.Text = Language._Object.getCaption(Me.lblAccountGroup.Name, Me.lblAccountGroup.Text)
			Me.lblAccountCode.Text = Language._Object.getCaption(Me.lblAccountCode.Name, Me.lblAccountCode.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.gbAccGroupMapping.Text = Language._Object.getCaption(Me.gbAccGroupMapping.Name, Me.gbAccGroupMapping.Text)
			Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
			Me.mnuShowSuccessful.Text = Language._Object.getCaption(Me.mnuShowSuccessful.Name, Me.mnuShowSuccessful.Text)
			Me.mnuCodeExist.Text = Language._Object.getCaption(Me.mnuCodeExist.Name, Me.mnuCodeExist.Text)
			Me.mnuNameExist.Text = Language._Object.getCaption(Me.mnuNameExist.Name, Me.mnuNameExist.Text)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.colhFileAccGroup.HeaderText = Language._Object.getCaption(Me.colhFileAccGroup.Name, Me.colhFileAccGroup.HeaderText)
			Me.colhDBAccGroup.HeaderText = Language._Object.getCaption(Me.colhDBAccGroup.Name, Me.colhDBAccGroup.HeaderText)
			Me.dgcolhAccCode.HeaderText = Language._Object.getCaption(Me.dgcolhAccCode.Name, Me.dgcolhAccCode.HeaderText)
			Me.dgcolhAccName.HeaderText = Language._Object.getCaption(Me.dgcolhAccName.Name, Me.dgcolhAccName.HeaderText)
			Me.dgcolhAccGrp.HeaderText = Language._Object.getCaption(Me.dgcolhAccGrp.Name, Me.dgcolhAccGrp.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please the select proper file to Import Data from.")
			Language.setMessage(mstrModuleName, 2, "Please the select proper field to Import Data.")
			Language.setMessage(mstrModuleName, 3, "Please Map All Account Group to Import Data.")
			Language.setMessage(mstrModuleName, 4, "Please Tick atleast one Account from list to Import.")
			Language.setMessage(mstrModuleName, 5, "Are you sure, you want to Import selected Accounts?")
			Language.setMessage(mstrModuleName, 6, "Data Imported Successfully!")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProceedingsApprovalAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProceedingsApprovalAddEdit))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboReferenceNo = New System.Windows.Forms.ComboBox
        Me.objbtnSearchReferenceNo = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.pnlData = New System.Windows.Forms.Panel
        Me.dgvProceedingDetails = New System.Windows.Forms.DataGridView
        Me.elProceedingCountDetails = New eZee.Common.eZeeLine
        Me.txtInterdictionDate = New System.Windows.Forms.TextBox
        Me.txtChargeDate = New System.Windows.Forms.TextBox
        Me.lblChargeDescription = New System.Windows.Forms.Label
        Me.txtChargeDescription = New System.Windows.Forms.TextBox
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.txtDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.txtJobTitle = New eZee.TextBox.AlphanumericTextBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblJobTitle = New System.Windows.Forms.Label
        Me.lblInterdictionDate = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnScanAttachDoc = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAdd = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhCount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhProceedingDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhProceedingDetails = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPIComments = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCommentRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDisciplineAction = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCommittee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInvestigator = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApproved = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCountStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPenaltyEffectiveDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPenaltyExpiryDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhdisciplinefiletranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProceedingMastId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgvProceedingDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(944, 562)
        Me.pnlMain.TabIndex = 0
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboReferenceNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchReferenceNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.pnlData)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.elProceedingCountDetails)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtInterdictionDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtChargeDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblChargeDescription)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtChargeDescription)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblRefNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtDepartment)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtJobTitle)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblDepartment)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblJobTitle)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblInterdictionDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblPersonalInvolved)
        Me.EZeeCollapsibleContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(944, 507)
        Me.EZeeCollapsibleContainer1.TabIndex = 1
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Proceedings && Approval"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReferenceNo
        '
        Me.cboReferenceNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboReferenceNo.DropDownWidth = 400
        Me.cboReferenceNo.Enabled = False
        Me.cboReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReferenceNo.FormattingEnabled = True
        Me.cboReferenceNo.Location = New System.Drawing.Point(110, 115)
        Me.cboReferenceNo.Name = "cboReferenceNo"
        Me.cboReferenceNo.Size = New System.Drawing.Size(279, 21)
        Me.cboReferenceNo.TabIndex = 206
        '
        'objbtnSearchReferenceNo
        '
        Me.objbtnSearchReferenceNo.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchReferenceNo.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchReferenceNo.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchReferenceNo.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchReferenceNo.BorderSelected = False
        Me.objbtnSearchReferenceNo.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchReferenceNo.Enabled = False
        Me.objbtnSearchReferenceNo.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchReferenceNo.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchReferenceNo.Location = New System.Drawing.Point(395, 115)
        Me.objbtnSearchReferenceNo.Name = "objbtnSearchReferenceNo"
        Me.objbtnSearchReferenceNo.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchReferenceNo.TabIndex = 203
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(395, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 202
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.dgvProceedingDetails)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(12, 160)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(920, 341)
        Me.pnlData.TabIndex = 197
        '
        'dgvProceedingDetails
        '
        Me.dgvProceedingDetails.AllowUserToAddRows = False
        Me.dgvProceedingDetails.AllowUserToDeleteRows = False
        Me.dgvProceedingDetails.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvProceedingDetails.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvProceedingDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvProceedingDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvProceedingDetails.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhAdd, Me.objdgcolhEdit, Me.objdgcolhDelete, Me.dgcolhCount, Me.dgcolhProceedingDate, Me.dgcolhProceedingDetails, Me.dgcolhPIComments, Me.dgcolhCommentRemark, Me.dgcolhDisciplineAction, Me.dgcolhCommittee, Me.dgcolhInvestigator, Me.dgcolhApproved, Me.dgcolhCountStatus, Me.dgcolhPenaltyEffectiveDate, Me.dgcolhPenaltyExpiryDate, Me.objdgcolhGUID, Me.objcolhdisciplinefiletranunkid, Me.objdgcolhProceedingMastId})
        Me.dgvProceedingDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvProceedingDetails.GridColor = System.Drawing.SystemColors.AppWorkspace
        Me.dgvProceedingDetails.Location = New System.Drawing.Point(0, 0)
        Me.dgvProceedingDetails.Name = "dgvProceedingDetails"
        Me.dgvProceedingDetails.ReadOnly = True
        Me.dgvProceedingDetails.RowHeadersVisible = False
        Me.dgvProceedingDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvProceedingDetails.Size = New System.Drawing.Size(920, 341)
        Me.dgvProceedingDetails.TabIndex = 0
        '
        'elProceedingCountDetails
        '
        Me.elProceedingCountDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elProceedingCountDetails.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elProceedingCountDetails.Location = New System.Drawing.Point(12, 139)
        Me.elProceedingCountDetails.Name = "elProceedingCountDetails"
        Me.elProceedingCountDetails.Size = New System.Drawing.Size(920, 17)
        Me.elProceedingCountDetails.TabIndex = 181
        Me.elProceedingCountDetails.Text = "Count Proceedings Details"
        Me.elProceedingCountDetails.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtInterdictionDate
        '
        Me.txtInterdictionDate.BackColor = System.Drawing.Color.White
        Me.txtInterdictionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterdictionDate.Location = New System.Drawing.Point(802, 34)
        Me.txtInterdictionDate.Name = "txtInterdictionDate"
        Me.txtInterdictionDate.ReadOnly = True
        Me.txtInterdictionDate.Size = New System.Drawing.Size(130, 21)
        Me.txtInterdictionDate.TabIndex = 171
        '
        'txtChargeDate
        '
        Me.txtChargeDate.BackColor = System.Drawing.Color.White
        Me.txtChargeDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChargeDate.Location = New System.Drawing.Point(545, 34)
        Me.txtChargeDate.Name = "txtChargeDate"
        Me.txtChargeDate.ReadOnly = True
        Me.txtChargeDate.Size = New System.Drawing.Size(130, 21)
        Me.txtChargeDate.TabIndex = 170
        '
        'lblChargeDescription
        '
        Me.lblChargeDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChargeDescription.Location = New System.Drawing.Point(443, 63)
        Me.lblChargeDescription.Name = "lblChargeDescription"
        Me.lblChargeDescription.Size = New System.Drawing.Size(96, 44)
        Me.lblChargeDescription.TabIndex = 138
        Me.lblChargeDescription.Text = "General Charge Description"
        '
        'txtChargeDescription
        '
        Me.txtChargeDescription.BackColor = System.Drawing.Color.White
        Me.txtChargeDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChargeDescription.Location = New System.Drawing.Point(545, 61)
        Me.txtChargeDescription.Multiline = True
        Me.txtChargeDescription.Name = "txtChargeDescription"
        Me.txtChargeDescription.ReadOnly = True
        Me.txtChargeDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtChargeDescription.Size = New System.Drawing.Size(387, 75)
        Me.txtChargeDescription.TabIndex = 119
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(12, 117)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(92, 17)
        Me.lblRefNo.TabIndex = 128
        Me.lblRefNo.Text = "Reference No."
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(110, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(279, 21)
        Me.cboEmployee.TabIndex = 135
        '
        'txtDepartment
        '
        Me.txtDepartment.BackColor = System.Drawing.SystemColors.Window
        Me.txtDepartment.Flags = 0
        Me.txtDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDepartment.Location = New System.Drawing.Point(110, 88)
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.ReadOnly = True
        Me.txtDepartment.Size = New System.Drawing.Size(279, 21)
        Me.txtDepartment.TabIndex = 134
        '
        'txtJobTitle
        '
        Me.txtJobTitle.BackColor = System.Drawing.SystemColors.Window
        Me.txtJobTitle.Flags = 0
        Me.txtJobTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobTitle.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtJobTitle.Location = New System.Drawing.Point(110, 61)
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.ReadOnly = True
        Me.txtJobTitle.Size = New System.Drawing.Size(279, 21)
        Me.txtJobTitle.TabIndex = 133
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(12, 90)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(92, 17)
        Me.lblDepartment.TabIndex = 132
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobTitle
        '
        Me.lblJobTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobTitle.Location = New System.Drawing.Point(12, 63)
        Me.lblJobTitle.Name = "lblJobTitle"
        Me.lblJobTitle.Size = New System.Drawing.Size(92, 17)
        Me.lblJobTitle.TabIndex = 131
        Me.lblJobTitle.Text = "Job Title"
        Me.lblJobTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInterdictionDate
        '
        Me.lblInterdictionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterdictionDate.Location = New System.Drawing.Point(701, 36)
        Me.lblInterdictionDate.Name = "lblInterdictionDate"
        Me.lblInterdictionDate.Size = New System.Drawing.Size(95, 17)
        Me.lblInterdictionDate.TabIndex = 129
        Me.lblInterdictionDate.Text = "Interdiction Date "
        Me.lblInterdictionDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(443, 36)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(96, 17)
        Me.lblDate.TabIndex = 125
        Me.lblDate.Text = "Charge Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(12, 36)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(92, 17)
        Me.lblPersonalInvolved.TabIndex = 126
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnScanAttachDoc)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 507)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(944, 55)
        Me.objFooter.TabIndex = 86
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(732, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(835, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnScanAttachDoc
        '
        Me.btnScanAttachDoc.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnScanAttachDoc.BackColor = System.Drawing.Color.White
        Me.btnScanAttachDoc.BackgroundImage = CType(resources.GetObject("btnScanAttachDoc.BackgroundImage"), System.Drawing.Image)
        Me.btnScanAttachDoc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnScanAttachDoc.BorderColor = System.Drawing.Color.Empty
        Me.btnScanAttachDoc.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnScanAttachDoc.FlatAppearance.BorderSize = 0
        Me.btnScanAttachDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnScanAttachDoc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnScanAttachDoc.ForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnScanAttachDoc.GradientForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnScanAttachDoc.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.Location = New System.Drawing.Point(776, 23)
        Me.btnScanAttachDoc.Name = "btnScanAttachDoc"
        Me.btnScanAttachDoc.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnScanAttachDoc.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnScanAttachDoc.Size = New System.Drawing.Size(10, 10)
        Me.btnScanAttachDoc.TabIndex = 17
        Me.btnScanAttachDoc.Text = "Scan/Attach &Document"
        Me.btnScanAttachDoc.UseVisualStyleBackColor = True
        Me.btnScanAttachDoc.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle17
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Count"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 50
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle18
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Proceeding Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle19
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = "Proceeding Details"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 200
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle20
        Me.DataGridViewTextBoxColumn4.Frozen = True
        Me.DataGridViewTextBoxColumn4.HeaderText = "Discipline Action"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 150
        '
        'DataGridViewTextBoxColumn5
        '
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle21
        Me.DataGridViewTextBoxColumn5.Frozen = True
        Me.DataGridViewTextBoxColumn5.HeaderText = "Committee"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle22
        Me.DataGridViewTextBoxColumn6.Frozen = True
        Me.DataGridViewTextBoxColumn6.HeaderText = "Investigators"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle23
        Me.DataGridViewTextBoxColumn7.Frozen = True
        Me.DataGridViewTextBoxColumn7.HeaderText = "Person Involved Comments"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 150
        '
        'DataGridViewTextBoxColumn8
        '
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle24
        Me.DataGridViewTextBoxColumn8.Frozen = True
        Me.DataGridViewTextBoxColumn8.HeaderText = "Approved Status"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn9
        '
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn9.DefaultCellStyle = DataGridViewCellStyle25
        Me.DataGridViewTextBoxColumn9.Frozen = True
        Me.DataGridViewTextBoxColumn9.HeaderText = "Count Status"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn10
        '
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn10.DefaultCellStyle = DataGridViewCellStyle26
        Me.DataGridViewTextBoxColumn10.Frozen = True
        Me.DataGridViewTextBoxColumn10.HeaderText = "dgcolhGUID"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.Frozen = True
        Me.DataGridViewTextBoxColumn11.HeaderText = "objcolhdisciplinefiletranunkid"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.Frozen = True
        Me.DataGridViewTextBoxColumn12.HeaderText = "objdgcolhProceedingMastId"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn13.DefaultCellStyle = DataGridViewCellStyle27
        Me.DataGridViewTextBoxColumn13.HeaderText = "objdgcolhProceedingMastId"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn14.DefaultCellStyle = DataGridViewCellStyle28
        Me.DataGridViewTextBoxColumn14.HeaderText = "objcolhdisciplinefiletranunkid"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn15.DefaultCellStyle = DataGridViewCellStyle29
        Me.DataGridViewTextBoxColumn15.HeaderText = "objdgcolhProceedingMastId"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'objdgcolhAdd
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.NullValue = CType(resources.GetObject("DataGridViewCellStyle1.NullValue"), Object)
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhAdd.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhAdd.Frozen = True
        Me.objdgcolhAdd.HeaderText = ""
        Me.objdgcolhAdd.Image = Global.Aruti.Main.My.Resources.Resources.add_16
        Me.objdgcolhAdd.Name = "objdgcolhAdd"
        Me.objdgcolhAdd.ReadOnly = True
        Me.objdgcolhAdd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhAdd.Width = 25
        '
        'objdgcolhEdit
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.NullValue = CType(resources.GetObject("DataGridViewCellStyle2.NullValue"), Object)
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhEdit.DefaultCellStyle = DataGridViewCellStyle2
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.NullValue = CType(resources.GetObject("DataGridViewCellStyle3.NullValue"), Object)
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhDelete.DefaultCellStyle = DataGridViewCellStyle3
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'dgcolhCount
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        Me.dgcolhCount.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhCount.HeaderText = "Count"
        Me.dgcolhCount.Name = "dgcolhCount"
        Me.dgcolhCount.ReadOnly = True
        Me.dgcolhCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCount.Width = 50
        '
        'dgcolhProceedingDate
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        Me.dgcolhProceedingDate.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgcolhProceedingDate.HeaderText = "Proceeding Date"
        Me.dgcolhProceedingDate.Name = "dgcolhProceedingDate"
        Me.dgcolhProceedingDate.ReadOnly = True
        Me.dgcolhProceedingDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhProceedingDetails
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhProceedingDetails.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgcolhProceedingDetails.HeaderText = "Proceeding Details"
        Me.dgcolhProceedingDetails.Name = "dgcolhProceedingDetails"
        Me.dgcolhProceedingDetails.ReadOnly = True
        Me.dgcolhProceedingDetails.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhProceedingDetails.Width = 200
        '
        'dgcolhPIComments
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhPIComments.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgcolhPIComments.HeaderText = "Person Involved Comments"
        Me.dgcolhPIComments.Name = "dgcolhPIComments"
        Me.dgcolhPIComments.ReadOnly = True
        Me.dgcolhPIComments.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhPIComments.Width = 150
        '
        'dgcolhCommentRemark
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhCommentRemark.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgcolhCommentRemark.HeaderText = "Comments/Remark"
        Me.dgcolhCommentRemark.Name = "dgcolhCommentRemark"
        Me.dgcolhCommentRemark.ReadOnly = True
        '
        'dgcolhDisciplineAction
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhDisciplineAction.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgcolhDisciplineAction.HeaderText = "Discipline Action"
        Me.dgcolhDisciplineAction.Name = "dgcolhDisciplineAction"
        Me.dgcolhDisciplineAction.ReadOnly = True
        Me.dgcolhDisciplineAction.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCommittee
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhCommittee.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgcolhCommittee.HeaderText = "Committee"
        Me.dgcolhCommittee.Name = "dgcolhCommittee"
        Me.dgcolhCommittee.ReadOnly = True
        Me.dgcolhCommittee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhInvestigator
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhInvestigator.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgcolhInvestigator.HeaderText = "Members/Investigators"
        Me.dgcolhInvestigator.Name = "dgcolhInvestigator"
        Me.dgcolhInvestigator.ReadOnly = True
        Me.dgcolhInvestigator.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhApproved
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhApproved.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgcolhApproved.HeaderText = "Approved Status"
        Me.dgcolhApproved.Name = "dgcolhApproved"
        Me.dgcolhApproved.ReadOnly = True
        Me.dgcolhApproved.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCountStatus
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhCountStatus.DefaultCellStyle = DataGridViewCellStyle13
        Me.dgcolhCountStatus.HeaderText = "Count Status"
        Me.dgcolhCountStatus.Name = "dgcolhCountStatus"
        Me.dgcolhCountStatus.ReadOnly = True
        Me.dgcolhCountStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhPenaltyEffectiveDate
        '
        Me.dgcolhPenaltyEffectiveDate.HeaderText = "Penalty Effective Date"
        Me.dgcolhPenaltyEffectiveDate.Name = "dgcolhPenaltyEffectiveDate"
        Me.dgcolhPenaltyEffectiveDate.ReadOnly = True
        Me.dgcolhPenaltyEffectiveDate.Visible = False
        '
        'dgcolhPenaltyExpiryDate
        '
        Me.dgcolhPenaltyExpiryDate.HeaderText = "Penalty Expiry Date"
        Me.dgcolhPenaltyExpiryDate.Name = "dgcolhPenaltyExpiryDate"
        Me.dgcolhPenaltyExpiryDate.ReadOnly = True
        Me.dgcolhPenaltyExpiryDate.Visible = False
        '
        'objdgcolhGUID
        '
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhGUID.DefaultCellStyle = DataGridViewCellStyle14
        Me.objdgcolhGUID.HeaderText = "objdgcolhGUID"
        Me.objdgcolhGUID.Name = "objdgcolhGUID"
        Me.objdgcolhGUID.ReadOnly = True
        Me.objdgcolhGUID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhGUID.Visible = False
        '
        'objcolhdisciplinefiletranunkid
        '
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhdisciplinefiletranunkid.DefaultCellStyle = DataGridViewCellStyle15
        Me.objcolhdisciplinefiletranunkid.HeaderText = "objcolhdisciplinefiletranunkid"
        Me.objcolhdisciplinefiletranunkid.Name = "objcolhdisciplinefiletranunkid"
        Me.objcolhdisciplinefiletranunkid.ReadOnly = True
        Me.objcolhdisciplinefiletranunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhdisciplinefiletranunkid.Visible = False
        '
        'objdgcolhProceedingMastId
        '
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhProceedingMastId.DefaultCellStyle = DataGridViewCellStyle16
        Me.objdgcolhProceedingMastId.HeaderText = "objdgcolhProceedingMastId"
        Me.objdgcolhProceedingMastId.Name = "objdgcolhProceedingMastId"
        Me.objdgcolhProceedingMastId.ReadOnly = True
        Me.objdgcolhProceedingMastId.Visible = False
        '
        'frmProceedingsApprovalAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 562)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProceedingsApprovalAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Proceedings"
        Me.pnlMain.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        CType(Me.dgvProceedingDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnScanAttachDoc As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblChargeDescription As System.Windows.Forms.Label
    Friend WithEvents txtChargeDescription As System.Windows.Forms.TextBox
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents txtDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtJobTitle As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblJobTitle As System.Windows.Forms.Label
    Friend WithEvents lblInterdictionDate As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents txtInterdictionDate As System.Windows.Forms.TextBox
    Friend WithEvents txtChargeDate As System.Windows.Forms.TextBox
    Friend WithEvents elProceedingCountDetails As eZee.Common.eZeeLine
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchReferenceNo As eZee.Common.eZeeGradientButton
    Friend WithEvents cboReferenceNo As System.Windows.Forms.ComboBox
    Friend WithEvents dgvProceedingDetails As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAdd As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhCount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhProceedingDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhProceedingDetails As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPIComments As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCommentRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDisciplineAction As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCommittee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInvestigator As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApproved As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCountStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPenaltyEffectiveDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPenaltyExpiryDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhdisciplinefiletranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProceedingMastId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

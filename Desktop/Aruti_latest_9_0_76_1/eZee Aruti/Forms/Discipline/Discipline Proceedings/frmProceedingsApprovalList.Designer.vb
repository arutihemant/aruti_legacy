﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisciplineProceedingList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisciplineProceedingList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dgvChargesList = New Aruti.Main.GroupByGrid
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChargeDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPersonInvloved = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCountDesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOffcenceCat = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOffence = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCountStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLastResolutionDesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDisciPenalty = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFinalStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChargeDesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProceedingMasterId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhDisciplineFileId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProceedingTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsSubmitForApproval = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsApproved = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objemailFooter = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboReferenceNo = New System.Windows.Forms.ComboBox
        Me.objbtnSearchRefNo = New eZee.Common.eZeeGradientButton
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblFinalStatus = New System.Windows.Forms.Label
        Me.cboCountStatus = New System.Windows.Forms.ComboBox
        Me.lblCountStatus = New System.Windows.Forms.Label
        Me.dtpChargeDtTo = New System.Windows.Forms.DateTimePicker
        Me.cboDisciplineAction = New System.Windows.Forms.ComboBox
        Me.lblDisciplineAction = New System.Windows.Forms.Label
        Me.lblChgDtTo = New System.Windows.Forms.Label
        Me.dtpChargeDtFrom = New System.Windows.Forms.DateTimePicker
        Me.lblChgDtFrom = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuScanAttachDocs = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSubmitForApproval = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuApproveDisapproveProceedings = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExemptTransHead = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPostTransHead = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuViewDisciplineHead = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExemptedHeadsList = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPostedHeadsList = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDisciplineCharge = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        CType(Me.dgvChargesList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objemailFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objchkAll)
        Me.pnlMain.Controls.Add(Me.dgvChargesList)
        Me.pnlMain.Controls.Add(Me.objemailFooter)
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(944, 572)
        Me.pnlMain.TabIndex = 0
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(9, 108)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 8
        Me.objchkAll.UseVisualStyleBackColor = True
        Me.objchkAll.Visible = False
        '
        'dgvChargesList
        '
        Me.dgvChargesList.AllowUserToAddRows = False
        Me.dgvChargesList.AllowUserToDeleteRows = False
        Me.dgvChargesList.AllowUserToResizeRows = False
        Me.dgvChargesList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvChargesList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvChargesList.ColumnHeadersHeight = 30
        Me.dgvChargesList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvChargesList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhRefNo, Me.dgcolhChargeDate, Me.dgcolhPersonInvloved, Me.dgcolhCount, Me.dgcolhCountDesc, Me.dgcolhOffcenceCat, Me.dgcolhOffence, Me.dgcolhCountStatus, Me.dgcolhLastResolutionDesc, Me.dgcolhDisciPenalty, Me.dgcolhFinalStatus, Me.dgcolhChargeDesc, Me.dgcolhCategory, Me.objdgcolhProceedingMasterId, Me.objdgcolhDisciplineFileId, Me.objdgcolhEmployeeId, Me.objdgcolhProceedingTranId, Me.objdgcolhIsSubmitForApproval, Me.objdgcolhIsApproved})
        Me.dgvChargesList.IgnoreFirstColumn = False
        Me.dgvChargesList.Location = New System.Drawing.Point(2, 97)
        Me.dgvChargesList.MultiSelect = False
        Me.dgvChargesList.Name = "dgvChargesList"
        Me.dgvChargesList.RowHeadersVisible = False
        Me.dgvChargesList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvChargesList.Size = New System.Drawing.Size(940, 422)
        Me.dgvChargesList.TabIndex = 89
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objdgcolhCheck.Visible = False
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhRefNo
        '
        Me.dgcolhRefNo.HeaderText = "Ref No."
        Me.dgcolhRefNo.Name = "dgcolhRefNo"
        Me.dgcolhRefNo.ReadOnly = True
        Me.dgcolhRefNo.Width = 75
        '
        'dgcolhChargeDate
        '
        Me.dgcolhChargeDate.HeaderText = "Charge Date"
        Me.dgcolhChargeDate.Name = "dgcolhChargeDate"
        Me.dgcolhChargeDate.ReadOnly = True
        '
        'dgcolhPersonInvloved
        '
        Me.dgcolhPersonInvloved.HeaderText = "Person Invloved"
        Me.dgcolhPersonInvloved.Name = "dgcolhPersonInvloved"
        Me.dgcolhPersonInvloved.ReadOnly = True
        Me.dgcolhPersonInvloved.Width = 125
        '
        'dgcolhCount
        '
        Me.dgcolhCount.HeaderText = "Count"
        Me.dgcolhCount.Name = "dgcolhCount"
        Me.dgcolhCount.ReadOnly = True
        Me.dgcolhCount.Width = 75
        '
        'dgcolhCountDesc
        '
        Me.dgcolhCountDesc.HeaderText = "Incident"
        Me.dgcolhCountDesc.Name = "dgcolhCountDesc"
        Me.dgcolhCountDesc.ReadOnly = True
        Me.dgcolhCountDesc.Width = 150
        '
        'dgcolhOffcenceCat
        '
        Me.dgcolhOffcenceCat.HeaderText = "Offence Category"
        Me.dgcolhOffcenceCat.Name = "dgcolhOffcenceCat"
        Me.dgcolhOffcenceCat.ReadOnly = True
        Me.dgcolhOffcenceCat.Width = 140
        '
        'dgcolhOffence
        '
        Me.dgcolhOffence.HeaderText = "Offence"
        Me.dgcolhOffence.Name = "dgcolhOffence"
        Me.dgcolhOffence.ReadOnly = True
        Me.dgcolhOffence.Width = 130
        '
        'dgcolhCountStatus
        '
        Me.dgcolhCountStatus.HeaderText = "Count Status"
        Me.dgcolhCountStatus.Name = "dgcolhCountStatus"
        Me.dgcolhCountStatus.ReadOnly = True
        '
        'dgcolhLastResolutionDesc
        '
        Me.dgcolhLastResolutionDesc.HeaderText = "Last Resolution"
        Me.dgcolhLastResolutionDesc.Name = "dgcolhLastResolutionDesc"
        Me.dgcolhLastResolutionDesc.ReadOnly = True
        Me.dgcolhLastResolutionDesc.Width = 125
        '
        'dgcolhDisciPenalty
        '
        Me.dgcolhDisciPenalty.HeaderText = "Disciplinary Penalty"
        Me.dgcolhDisciPenalty.Name = "dgcolhDisciPenalty"
        Me.dgcolhDisciPenalty.ReadOnly = True
        Me.dgcolhDisciPenalty.Width = 125
        '
        'dgcolhFinalStatus
        '
        Me.dgcolhFinalStatus.HeaderText = "Status"
        Me.dgcolhFinalStatus.Name = "dgcolhFinalStatus"
        Me.dgcolhFinalStatus.ReadOnly = True
        Me.dgcolhFinalStatus.Width = 85
        '
        'dgcolhChargeDesc
        '
        Me.dgcolhChargeDesc.HeaderText = "Charge Description"
        Me.dgcolhChargeDesc.Name = "dgcolhChargeDesc"
        Me.dgcolhChargeDesc.Visible = False
        '
        'dgcolhCategory
        '
        Me.dgcolhCategory.HeaderText = "Category"
        Me.dgcolhCategory.Name = "dgcolhCategory"
        Me.dgcolhCategory.ReadOnly = True
        '
        'objdgcolhProceedingMasterId
        '
        Me.objdgcolhProceedingMasterId.HeaderText = "objdgcolhProceedingMasterId"
        Me.objdgcolhProceedingMasterId.Name = "objdgcolhProceedingMasterId"
        Me.objdgcolhProceedingMasterId.ReadOnly = True
        Me.objdgcolhProceedingMasterId.Visible = False
        '
        'objdgcolhDisciplineFileId
        '
        Me.objdgcolhDisciplineFileId.HeaderText = "objdgcolhDisciplineFileId"
        Me.objdgcolhDisciplineFileId.Name = "objdgcolhDisciplineFileId"
        Me.objdgcolhDisciplineFileId.ReadOnly = True
        Me.objdgcolhDisciplineFileId.Visible = False
        '
        'objdgcolhEmployeeId
        '
        Me.objdgcolhEmployeeId.HeaderText = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.Name = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.ReadOnly = True
        Me.objdgcolhEmployeeId.Visible = False
        '
        'objdgcolhProceedingTranId
        '
        Me.objdgcolhProceedingTranId.HeaderText = "objdgcolhProceedingTranId"
        Me.objdgcolhProceedingTranId.Name = "objdgcolhProceedingTranId"
        Me.objdgcolhProceedingTranId.Visible = False
        '
        'objdgcolhIsSubmitForApproval
        '
        Me.objdgcolhIsSubmitForApproval.HeaderText = "objdgcolhIsSubmitForApproval"
        Me.objdgcolhIsSubmitForApproval.Name = "objdgcolhIsSubmitForApproval"
        Me.objdgcolhIsSubmitForApproval.Visible = False
        '
        'objdgcolhIsApproved
        '
        Me.objdgcolhIsApproved.HeaderText = "objdgcolhIsApproved"
        Me.objdgcolhIsApproved.Name = "objdgcolhIsApproved"
        Me.objdgcolhIsApproved.Visible = False
        '
        'objemailFooter
        '
        Me.objemailFooter.BorderColor = System.Drawing.Color.Silver
        Me.objemailFooter.Controls.Add(Me.btnOk)
        Me.objemailFooter.Controls.Add(Me.btnEClose)
        Me.objemailFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objemailFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objemailFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objemailFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objemailFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objemailFooter.Location = New System.Drawing.Point(0, 472)
        Me.objemailFooter.Name = "objemailFooter"
        Me.objemailFooter.Size = New System.Drawing.Size(944, 50)
        Me.objemailFooter.TabIndex = 88
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(732, 10)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 17
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnEClose
        '
        Me.btnEClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEClose.BackColor = System.Drawing.Color.White
        Me.btnEClose.BackgroundImage = CType(resources.GetObject("btnEClose.BackgroundImage"), System.Drawing.Image)
        Me.btnEClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEClose.BorderColor = System.Drawing.Color.Empty
        Me.btnEClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEClose.FlatAppearance.BorderSize = 0
        Me.btnEClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEClose.ForeColor = System.Drawing.Color.Black
        Me.btnEClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Location = New System.Drawing.Point(835, 10)
        Me.btnEClose.Name = "btnEClose"
        Me.btnEClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Size = New System.Drawing.Size(97, 30)
        Me.btnEClose.TabIndex = 16
        Me.btnEClose.Text = "&Close"
        Me.btnEClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboReferenceNo)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchRefNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblRefNo)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblFinalStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboCountStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblCountStatus)
        Me.gbFilterCriteria.Controls.Add(Me.dtpChargeDtTo)
        Me.gbFilterCriteria.Controls.Add(Me.cboDisciplineAction)
        Me.gbFilterCriteria.Controls.Add(Me.lblDisciplineAction)
        Me.gbFilterCriteria.Controls.Add(Me.lblChgDtTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpChargeDtFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblChgDtFrom)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lblPersonalInvolved)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(2, 2)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(940, 94)
        Me.gbFilterCriteria.TabIndex = 6
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReferenceNo
        '
        Me.cboReferenceNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboReferenceNo.DropDownWidth = 300
        Me.cboReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReferenceNo.FormattingEnabled = True
        Me.cboReferenceNo.Location = New System.Drawing.Point(555, 34)
        Me.cboReferenceNo.Name = "cboReferenceNo"
        Me.cboReferenceNo.Size = New System.Drawing.Size(141, 21)
        Me.cboReferenceNo.TabIndex = 135
        '
        'objbtnSearchRefNo
        '
        Me.objbtnSearchRefNo.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchRefNo.BorderSelected = False
        Me.objbtnSearchRefNo.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchRefNo.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchRefNo.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchRefNo.Location = New System.Drawing.Point(702, 34)
        Me.objbtnSearchRefNo.Name = "objbtnSearchRefNo"
        Me.objbtnSearchRefNo.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchRefNo.TabIndex = 134
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(464, 36)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(85, 17)
        Me.lblRefNo.TabIndex = 132
        Me.lblRefNo.Text = "Reference No."
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 300
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(820, 61)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(110, 21)
        Me.cboStatus.TabIndex = 129
        '
        'lblFinalStatus
        '
        Me.lblFinalStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinalStatus.Location = New System.Drawing.Point(729, 63)
        Me.lblFinalStatus.Name = "lblFinalStatus"
        Me.lblFinalStatus.Size = New System.Drawing.Size(85, 17)
        Me.lblFinalStatus.TabIndex = 128
        Me.lblFinalStatus.Text = "Status"
        Me.lblFinalStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCountStatus
        '
        Me.cboCountStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountStatus.DropDownWidth = 300
        Me.cboCountStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountStatus.FormattingEnabled = True
        Me.cboCountStatus.Location = New System.Drawing.Point(820, 34)
        Me.cboCountStatus.Name = "cboCountStatus"
        Me.cboCountStatus.Size = New System.Drawing.Size(110, 21)
        Me.cboCountStatus.TabIndex = 127
        '
        'lblCountStatus
        '
        Me.lblCountStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountStatus.Location = New System.Drawing.Point(729, 36)
        Me.lblCountStatus.Name = "lblCountStatus"
        Me.lblCountStatus.Size = New System.Drawing.Size(85, 17)
        Me.lblCountStatus.TabIndex = 126
        Me.lblCountStatus.Text = "Count Status"
        Me.lblCountStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpChargeDtTo
        '
        Me.dtpChargeDtTo.Checked = False
        Me.dtpChargeDtTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpChargeDtTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpChargeDtTo.Location = New System.Drawing.Point(339, 61)
        Me.dtpChargeDtTo.Name = "dtpChargeDtTo"
        Me.dtpChargeDtTo.ShowCheckBox = True
        Me.dtpChargeDtTo.Size = New System.Drawing.Size(112, 21)
        Me.dtpChargeDtTo.TabIndex = 120
        '
        'cboDisciplineAction
        '
        Me.cboDisciplineAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineAction.DropDownWidth = 300
        Me.cboDisciplineAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineAction.FormattingEnabled = True
        Me.cboDisciplineAction.Location = New System.Drawing.Point(555, 61)
        Me.cboDisciplineAction.Name = "cboDisciplineAction"
        Me.cboDisciplineAction.Size = New System.Drawing.Size(168, 21)
        Me.cboDisciplineAction.TabIndex = 124
        '
        'lblDisciplineAction
        '
        Me.lblDisciplineAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplineAction.Location = New System.Drawing.Point(464, 63)
        Me.lblDisciplineAction.Name = "lblDisciplineAction"
        Me.lblDisciplineAction.Size = New System.Drawing.Size(85, 17)
        Me.lblDisciplineAction.TabIndex = 123
        Me.lblDisciplineAction.Text = "Disc. Action"
        Me.lblDisciplineAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblChgDtTo
        '
        Me.lblChgDtTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChgDtTo.Location = New System.Drawing.Point(239, 63)
        Me.lblChgDtTo.Name = "lblChgDtTo"
        Me.lblChgDtTo.Size = New System.Drawing.Size(91, 17)
        Me.lblChgDtTo.TabIndex = 121
        Me.lblChgDtTo.Text = "Charge Date To"
        Me.lblChgDtTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpChargeDtFrom
        '
        Me.dtpChargeDtFrom.Checked = False
        Me.dtpChargeDtFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpChargeDtFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpChargeDtFrom.Location = New System.Drawing.Point(119, 61)
        Me.dtpChargeDtFrom.Name = "dtpChargeDtFrom"
        Me.dtpChargeDtFrom.ShowCheckBox = True
        Me.dtpChargeDtFrom.Size = New System.Drawing.Size(112, 21)
        Me.dtpChargeDtFrom.TabIndex = 118
        '
        'lblChgDtFrom
        '
        Me.lblChgDtFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChgDtFrom.Location = New System.Drawing.Point(12, 63)
        Me.lblChgDtFrom.Name = "lblChgDtFrom"
        Me.lblChgDtFrom.Size = New System.Drawing.Size(101, 17)
        Me.lblChgDtFrom.TabIndex = 119
        Me.lblChgDtFrom.Text = "Charge Date From"
        Me.lblChgDtFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(119, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(305, 21)
        Me.cboEmployee.TabIndex = 116
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(430, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 117
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(913, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(890, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(12, 36)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(101, 17)
        Me.lblPersonalInvolved.TabIndex = 113
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnAdd)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 522)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(944, 50)
        Me.objFooter.TabIndex = 4
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 10)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(103, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 125
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuScanAttachDocs, Me.mnuSubmitForApproval, Me.mnuApproveDisapproveProceedings, Me.mnuExemptTransHead, Me.mnuPostTransHead, Me.mnuViewDisciplineHead, Me.mnuDisciplineCharge})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(255, 180)
        '
        'mnuScanAttachDocs
        '
        Me.mnuScanAttachDocs.Name = "mnuScanAttachDocs"
        Me.mnuScanAttachDocs.Size = New System.Drawing.Size(254, 22)
        Me.mnuScanAttachDocs.Text = "Browse"
        Me.mnuScanAttachDocs.Visible = False
        '
        'mnuSubmitForApproval
        '
        Me.mnuSubmitForApproval.Name = "mnuSubmitForApproval"
        Me.mnuSubmitForApproval.Size = New System.Drawing.Size(254, 22)
        Me.mnuSubmitForApproval.Text = "Submit For Approvals"
        '
        'mnuApproveDisapproveProceedings
        '
        Me.mnuApproveDisapproveProceedings.Name = "mnuApproveDisapproveProceedings"
        Me.mnuApproveDisapproveProceedings.Size = New System.Drawing.Size(254, 22)
        Me.mnuApproveDisapproveProceedings.Text = "Approve/Disapprove Proceedings "
        '
        'mnuExemptTransHead
        '
        Me.mnuExemptTransHead.Name = "mnuExemptTransHead"
        Me.mnuExemptTransHead.Size = New System.Drawing.Size(254, 22)
        Me.mnuExemptTransHead.Text = "Exempt Transaction Head"
        '
        'mnuPostTransHead
        '
        Me.mnuPostTransHead.Name = "mnuPostTransHead"
        Me.mnuPostTransHead.Size = New System.Drawing.Size(254, 22)
        Me.mnuPostTransHead.Text = "Post Transaction Head"
        '
        'mnuViewDisciplineHead
        '
        Me.mnuViewDisciplineHead.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExemptedHeadsList, Me.mnuPostedHeadsList})
        Me.mnuViewDisciplineHead.Name = "mnuViewDisciplineHead"
        Me.mnuViewDisciplineHead.Size = New System.Drawing.Size(254, 22)
        Me.mnuViewDisciplineHead.Text = "View Discipline Head"
        '
        'mnuExemptedHeadsList
        '
        Me.mnuExemptedHeadsList.Name = "mnuExemptedHeadsList"
        Me.mnuExemptedHeadsList.Size = New System.Drawing.Size(162, 22)
        Me.mnuExemptedHeadsList.Text = "Exempted Heads"
        '
        'mnuPostedHeadsList
        '
        Me.mnuPostedHeadsList.Name = "mnuPostedHeadsList"
        Me.mnuPostedHeadsList.Size = New System.Drawing.Size(162, 22)
        Me.mnuPostedHeadsList.Text = "Posted Heads"
        '
        'mnuDisciplineCharge
        '
        Me.mnuDisciplineCharge.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrint, Me.mnuPreview})
        Me.mnuDisciplineCharge.Name = "mnuDisciplineCharge"
        Me.mnuDisciplineCharge.Size = New System.Drawing.Size(254, 22)
        Me.mnuDisciplineCharge.Text = "Discipline Charge"
        Me.mnuDisciplineCharge.Visible = False
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(115, 22)
        Me.mnuPrint.Text = "Print"
        '
        'mnuPreview
        '
        Me.mnuPreview.Name = "mnuPreview"
        Me.mnuPreview.Size = New System.Drawing.Size(115, 22)
        Me.mnuPreview.Text = "Preview"
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(689, 10)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(140, 30)
        Me.btnEdit.TabIndex = 123
        Me.btnEdit.Text = "&Edit/View Proceedings"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(543, 10)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(140, 30)
        Me.btnAdd.TabIndex = 122
        Me.btnAdd.Text = "Post &New Proceedings"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(835, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 121
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(732, 10)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 124
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Reference No"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Charge Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 80
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Interdiction Date"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 80
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Person Involved"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "General Description"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 250
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "No. Of Count"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 50
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Notification Date"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 80
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Response Date"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 80
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Response Type"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 125
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "First Hearing Date"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 80
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Next Hearing Date"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 80
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Charge Status"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Category"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "objdgcolhDiscFileunkid"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "objdgcolhEmailAddress"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "objdgcolhCategoryId"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'frmDisciplineProceedingList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 572)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDisciplineProceedingList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Disciplinary Proceeding List"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        CType(Me.dgvChargesList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objemailFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents objemailFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnEClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents dtpChargeDtTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblChgDtTo As System.Windows.Forms.Label
    Friend WithEvents dtpChargeDtFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblChgDtFrom As System.Windows.Forms.Label
    Friend WithEvents cboDisciplineAction As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisciplineAction As System.Windows.Forms.Label
    Friend WithEvents cboCountStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblCountStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblFinalStatus As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchRefNo As eZee.Common.eZeeGradientButton
    Friend WithEvents cboReferenceNo As System.Windows.Forms.ComboBox
    Friend WithEvents dgvChargesList As Aruti.Main.GroupByGrid
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuScanAttachDocs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSubmitForApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExemptTransHead As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPostTransHead As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewDisciplineHead As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExemptedHeadsList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPostedHeadsList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDisciplineCharge As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuApproveDisapproveProceedings As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChargeDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPersonInvloved As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCountDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOffcenceCat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOffence As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCountStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLastResolutionDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDisciPenalty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFinalStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChargeDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProceedingMasterId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhDisciplineFileId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProceedingTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsSubmitForApproval As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsApproved As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

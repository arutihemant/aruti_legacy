﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data


#End Region

Public Class frmApproveDisapproveCharges

    Private ReadOnly mstrModuleName As String = "frmApproveDisapproveCharges"
    Private objProceedingMaster As clsdiscipline_proceeding_master
    Private mintDisciplineProceedingMasterunkid As Integer = 0
    Private mintDisciplineFileUnkid As Integer = 0
    Private mdtTran As DataTable
    Private mblnCancel As Boolean = True

#Region " Display Dialog "

    Public Function displayDialog(ByVal intDisciplineProceedingMasterunkid As Integer, ByVal intDisciplineFileUnkid As Integer) As Boolean
        Try
            mintDisciplineProceedingMasterunkid = intDisciplineProceedingMasterunkid
            mintDisciplineFileUnkid = intDisciplineFileUnkid
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmApproveDisapproveCharges_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmApproveDisapproveCharges_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsdiscipline_proceeding_master.SetMessages()
            objfrm._Other_ModuleNames = "clsdiscipline_proceeding_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineProceedingList_LanguageClick", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapproveCharges_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objProceedingMaster = New clsdiscipline_proceeding_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Fill_Proceeding_Details()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveCharges_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Fill_Proceeding_Details()
        Try
            Dim dTable As DataTable = Nothing
            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'dTable = objProceedingMaster.ProceedingApprove_Disapprove(mintDisciplineProceedingMasterunkid, "List")
            dTable = objProceedingMaster.ProceedingApprove_Disapprove(mintDisciplineProceedingMasterunkid, "List", ConfigParameter._Object._AddProceedingAgainstEachCount)
            'S.SANDEEP |01-OCT-2019| -- END
            If dTable IsNot Nothing Then
                dgvProceedingDetails.AutoGenerateColumns = False
                dgcolhParticulars.DataPropertyName = "Particulars"
                dgcolhValues.DataPropertyName = "Details"
                dgvProceedingDetails.DataSource = dTable
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Proceeding_Details", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        mblnCancel = False
        Me.Close()
    End Sub

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            If txtRemark.Text.Trim.Length <= 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Are you sure, you want to approve the proceedings without remark?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
            End If

            objProceedingMaster._DisciplineProceedingMasterunkid = mintDisciplineProceedingMasterunkid
            mdtTran = objProceedingMaster._ProceedingTable
            'S.SANDEEP [24 MAY 2016] -- Start
            'Email Notification
            objProceedingMaster._IsApprovedDisApproved = True
            objProceedingMaster._DisciplineFileUnkid = mintDisciplineFileUnkid
            'S.SANDEEP [24 MAY 2016] -- End

            'S.SANDEEP [24 OCT 2016] -- START
            'ENHANCEMENT : DISCIPLINE AUDIT TRAILS
            objProceedingMaster._Userunkid = User._Object._Userunkid
            'S.SANDEEP [24 OCT 2016] -- END



            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then

                mdtTran.Rows(0)("approval_date") = dtpApprovalDate.Value
                mdtTran.Rows(0)("approval_remark") = txtRemark.Text
                mdtTran.Rows(0)("AUD") = "U"
                mdtTran.Rows(0)("isapproved") = True
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'If objProceedingMaster.InsertUpdateDeleteProceeding(ConfigParameter._Object._CurrentDateAndTime, mdtTran, Nothing, _
                '                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True) = False Then
                'S.SANDEEP |11-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'If objProceedingMaster.InsertUpdateDeleteProceeding(ConfigParameter._Object._CurrentDateAndTime, mdtTran, Nothing, _
                '                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, Company._Object._Companyunkid) = False Then
                If objProceedingMaster.InsertUpdateDeleteProceeding(ConfigParameter._Object._CurrentDateAndTime, mdtTran, Nothing, Nothing, _
                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, Company._Object._Companyunkid, "", False, ConfigParameter._Object._AddProceedingAgainstEachCount) = False Then 'S.SANDEEP |25-MAR-2020| -- START {False} -- END
                    'S.SANDEEP |11-NOV-2019| -- END

                    'Sohail (30 Nov 2017) -- End
                    eZeeMsgBox.Show(objProceedingMaster._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Charge count approved successfully."), enMsgBoxStyle.Information)
                    btnClose_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbProceedingInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbProceedingInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnDisapprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnDisapprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.btnDisapprove.Text = Language._Object.getCaption(Me.btnDisapprove.Name, Me.btnDisapprove.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.lblApprovalDate.Text = Language._Object.getCaption(Me.lblApprovalDate.Name, Me.lblApprovalDate.Text)
            Me.gbProceedingInfo.Text = Language._Object.getCaption(Me.gbProceedingInfo.Name, Me.gbProceedingInfo.Text)
            Me.lblProceedingInfo.Text = Language._Object.getCaption(Me.lblProceedingInfo.Name, Me.lblProceedingInfo.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.dgcolhParticulars.HeaderText = Language._Object.getCaption(Me.dgcolhParticulars.Name, Me.dgcolhParticulars.HeaderText)
            Me.dgcolhValues.HeaderText = Language._Object.getCaption(Me.dgcolhValues.Name, Me.dgcolhValues.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Are you sure, you want to approve the proceedings without remark?")
            Language.setMessage(mstrModuleName, 2, "Charge count approved successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
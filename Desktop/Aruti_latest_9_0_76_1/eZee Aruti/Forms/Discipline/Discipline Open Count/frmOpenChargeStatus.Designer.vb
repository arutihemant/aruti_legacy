﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOpenChargeStatus
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOpenChargeStatus))
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle37 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle38 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle41 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle42 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle43 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle44 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle45 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle46 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle47 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle48 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbDiscipilneFiling = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvProceedingDetails = New System.Windows.Forms.DataGridView
        Me.objdgcolhAdd = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhCount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhProceedingDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhProceedingDetails = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPIComments = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCommentRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDisciplineAction = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCommittee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInvestigator = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApproved = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCountStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhdisciplinefiletranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProceedingMastId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblChargeDescription = New System.Windows.Forms.Label
        Me.txtChargeDescription = New System.Windows.Forms.TextBox
        Me.txtChargeDate = New System.Windows.Forms.TextBox
        Me.txtinterdictiondate = New System.Windows.Forms.TextBox
        Me.txtReferenceNo = New eZee.TextBox.AlphanumericTextBox
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.txtDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.txtJobTitle = New eZee.TextBox.AlphanumericTextBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblJobTitle = New System.Windows.Forms.Label
        Me.lblInterdictionDate = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objbtnOpen = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlData = New System.Windows.Forms.Panel
        Me.objchkCheckAll = New System.Windows.Forms.CheckBox
        Me.lvChargesCount = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhCount = New System.Windows.Forms.ColumnHeader
        Me.colhIncident = New System.Windows.Forms.ColumnHeader
        Me.colhOffCategory = New System.Windows.Forms.ColumnHeader
        Me.colhOffence = New System.Windows.Forms.ColumnHeader
        Me.colhSeverity = New System.Windows.Forms.ColumnHeader
        Me.objcolhoffencecategoryunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhoffenceunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhfileunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhProceedingMastId = New System.Windows.Forms.ColumnHeader
        Me.lblOpeningReason = New System.Windows.Forms.Label
        Me.cboReason = New System.Windows.Forms.ComboBox
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.pnlMain.SuspendLayout()
        Me.gbDiscipilneFiling.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvProceedingDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.pnlData.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbDiscipilneFiling)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(903, 486)
        Me.pnlMain.TabIndex = 0
        '
        'gbDiscipilneFiling
        '
        Me.gbDiscipilneFiling.BorderColor = System.Drawing.Color.Black
        Me.gbDiscipilneFiling.Checked = False
        Me.gbDiscipilneFiling.CollapseAllExceptThis = False
        Me.gbDiscipilneFiling.CollapsedHoverImage = Nothing
        Me.gbDiscipilneFiling.CollapsedNormalImage = Nothing
        Me.gbDiscipilneFiling.CollapsedPressedImage = Nothing
        Me.gbDiscipilneFiling.CollapseOnLoad = False
        Me.gbDiscipilneFiling.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbDiscipilneFiling.Controls.Add(Me.cboReason)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblOpeningReason)
        Me.gbDiscipilneFiling.Controls.Add(Me.Panel1)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblChargeDescription)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtChargeDescription)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtChargeDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtinterdictiondate)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtReferenceNo)
        Me.gbDiscipilneFiling.Controls.Add(Me.objLine1)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblRefNo)
        Me.gbDiscipilneFiling.Controls.Add(Me.cboEmployee)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtDepartment)
        Me.gbDiscipilneFiling.Controls.Add(Me.txtJobTitle)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblDepartment)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblJobTitle)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblInterdictionDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblDate)
        Me.gbDiscipilneFiling.Controls.Add(Me.lblPersonalInvolved)
        Me.gbDiscipilneFiling.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbDiscipilneFiling.ExpandedHoverImage = Nothing
        Me.gbDiscipilneFiling.ExpandedNormalImage = Nothing
        Me.gbDiscipilneFiling.ExpandedPressedImage = Nothing
        Me.gbDiscipilneFiling.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDiscipilneFiling.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDiscipilneFiling.HeaderHeight = 25
        Me.gbDiscipilneFiling.HeaderMessage = ""
        Me.gbDiscipilneFiling.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbDiscipilneFiling.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDiscipilneFiling.HeightOnCollapse = 0
        Me.gbDiscipilneFiling.LeftTextSpace = 0
        Me.gbDiscipilneFiling.Location = New System.Drawing.Point(0, 0)
        Me.gbDiscipilneFiling.Name = "gbDiscipilneFiling"
        Me.gbDiscipilneFiling.OpenHeight = 300
        Me.gbDiscipilneFiling.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDiscipilneFiling.ShowBorder = True
        Me.gbDiscipilneFiling.ShowCheckBox = False
        Me.gbDiscipilneFiling.ShowCollapseButton = False
        Me.gbDiscipilneFiling.ShowDefaultBorderColor = True
        Me.gbDiscipilneFiling.ShowDownButton = False
        Me.gbDiscipilneFiling.ShowHeader = True
        Me.gbDiscipilneFiling.Size = New System.Drawing.Size(903, 431)
        Me.gbDiscipilneFiling.TabIndex = 87
        Me.gbDiscipilneFiling.Temp = 0
        Me.gbDiscipilneFiling.Text = "Charges Count(s) Information"
        Me.gbDiscipilneFiling.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvProceedingDetails)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(12, 155)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(879, 270)
        Me.Panel1.TabIndex = 242
        '
        'dgvProceedingDetails
        '
        Me.dgvProceedingDetails.AllowUserToAddRows = False
        Me.dgvProceedingDetails.AllowUserToDeleteRows = False
        Me.dgvProceedingDetails.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvProceedingDetails.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvProceedingDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvProceedingDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvProceedingDetails.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhAdd, Me.objdgcolhEdit, Me.objdgcolhDelete, Me.dgcolhCount, Me.dgcolhProceedingDate, Me.dgcolhProceedingDetails, Me.dgcolhPIComments, Me.dgcolhCommentRemark, Me.dgcolhDisciplineAction, Me.dgcolhCommittee, Me.dgcolhInvestigator, Me.dgcolhApproved, Me.dgcolhCountStatus, Me.objdgcolhGUID, Me.objcolhdisciplinefiletranunkid, Me.objdgcolhProceedingMastId})
        Me.dgvProceedingDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvProceedingDetails.GridColor = System.Drawing.SystemColors.AppWorkspace
        Me.dgvProceedingDetails.Location = New System.Drawing.Point(0, 0)
        Me.dgvProceedingDetails.Name = "dgvProceedingDetails"
        Me.dgvProceedingDetails.ReadOnly = True
        Me.dgvProceedingDetails.RowHeadersVisible = False
        Me.dgvProceedingDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvProceedingDetails.Size = New System.Drawing.Size(879, 270)
        Me.dgvProceedingDetails.TabIndex = 0
        '
        'objdgcolhAdd
        '
        DataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle33.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle33.NullValue = CType(resources.GetObject("DataGridViewCellStyle33.NullValue"), Object)
        DataGridViewCellStyle33.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhAdd.DefaultCellStyle = DataGridViewCellStyle33
        Me.objdgcolhAdd.Frozen = True
        Me.objdgcolhAdd.HeaderText = ""
        Me.objdgcolhAdd.Image = Global.Aruti.Main.My.Resources.Resources.add_16
        Me.objdgcolhAdd.Name = "objdgcolhAdd"
        Me.objdgcolhAdd.ReadOnly = True
        Me.objdgcolhAdd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhAdd.Width = 25
        '
        'objdgcolhEdit
        '
        DataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle34.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle34.NullValue = CType(resources.GetObject("DataGridViewCellStyle34.NullValue"), Object)
        DataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle34.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhEdit.DefaultCellStyle = DataGridViewCellStyle34
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        DataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle35.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle35.NullValue = CType(resources.GetObject("DataGridViewCellStyle35.NullValue"), Object)
        DataGridViewCellStyle35.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhDelete.DefaultCellStyle = DataGridViewCellStyle35
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'dgcolhCount
        '
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        Me.dgcolhCount.DefaultCellStyle = DataGridViewCellStyle36
        Me.dgcolhCount.HeaderText = "Count"
        Me.dgcolhCount.Name = "dgcolhCount"
        Me.dgcolhCount.ReadOnly = True
        Me.dgcolhCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhCount.Width = 50
        '
        'dgcolhProceedingDate
        '
        DataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        Me.dgcolhProceedingDate.DefaultCellStyle = DataGridViewCellStyle37
        Me.dgcolhProceedingDate.HeaderText = "Proceeding Date"
        Me.dgcolhProceedingDate.Name = "dgcolhProceedingDate"
        Me.dgcolhProceedingDate.ReadOnly = True
        Me.dgcolhProceedingDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhProceedingDetails
        '
        DataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhProceedingDetails.DefaultCellStyle = DataGridViewCellStyle38
        Me.dgcolhProceedingDetails.HeaderText = "Proceeding Details"
        Me.dgcolhProceedingDetails.Name = "dgcolhProceedingDetails"
        Me.dgcolhProceedingDetails.ReadOnly = True
        Me.dgcolhProceedingDetails.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhProceedingDetails.Width = 200
        '
        'dgcolhPIComments
        '
        DataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhPIComments.DefaultCellStyle = DataGridViewCellStyle39
        Me.dgcolhPIComments.HeaderText = "Person Involved Comments"
        Me.dgcolhPIComments.Name = "dgcolhPIComments"
        Me.dgcolhPIComments.ReadOnly = True
        Me.dgcolhPIComments.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhPIComments.Width = 150
        '
        'dgcolhCommentRemark
        '
        DataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhCommentRemark.DefaultCellStyle = DataGridViewCellStyle40
        Me.dgcolhCommentRemark.HeaderText = "Comments/Remark"
        Me.dgcolhCommentRemark.Name = "dgcolhCommentRemark"
        Me.dgcolhCommentRemark.ReadOnly = True
        '
        'dgcolhDisciplineAction
        '
        DataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhDisciplineAction.DefaultCellStyle = DataGridViewCellStyle41
        Me.dgcolhDisciplineAction.HeaderText = "Discipline Action"
        Me.dgcolhDisciplineAction.Name = "dgcolhDisciplineAction"
        Me.dgcolhDisciplineAction.ReadOnly = True
        Me.dgcolhDisciplineAction.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCommittee
        '
        DataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhCommittee.DefaultCellStyle = DataGridViewCellStyle42
        Me.dgcolhCommittee.HeaderText = "Committee"
        Me.dgcolhCommittee.Name = "dgcolhCommittee"
        Me.dgcolhCommittee.ReadOnly = True
        Me.dgcolhCommittee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhInvestigator
        '
        DataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhInvestigator.DefaultCellStyle = DataGridViewCellStyle43
        Me.dgcolhInvestigator.HeaderText = "Members/Investigators"
        Me.dgcolhInvestigator.Name = "dgcolhInvestigator"
        Me.dgcolhInvestigator.ReadOnly = True
        Me.dgcolhInvestigator.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhApproved
        '
        DataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhApproved.DefaultCellStyle = DataGridViewCellStyle44
        Me.dgcolhApproved.HeaderText = "Approved Status"
        Me.dgcolhApproved.Name = "dgcolhApproved"
        Me.dgcolhApproved.ReadOnly = True
        Me.dgcolhApproved.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCountStatus
        '
        DataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhCountStatus.DefaultCellStyle = DataGridViewCellStyle45
        Me.dgcolhCountStatus.HeaderText = "Count Status"
        Me.dgcolhCountStatus.Name = "dgcolhCountStatus"
        Me.dgcolhCountStatus.ReadOnly = True
        Me.dgcolhCountStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhGUID
        '
        DataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle46.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhGUID.DefaultCellStyle = DataGridViewCellStyle46
        Me.objdgcolhGUID.HeaderText = "objdgcolhGUID"
        Me.objdgcolhGUID.Name = "objdgcolhGUID"
        Me.objdgcolhGUID.ReadOnly = True
        Me.objdgcolhGUID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhGUID.Visible = False
        '
        'objcolhdisciplinefiletranunkid
        '
        DataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhdisciplinefiletranunkid.DefaultCellStyle = DataGridViewCellStyle47
        Me.objcolhdisciplinefiletranunkid.HeaderText = "objcolhdisciplinefiletranunkid"
        Me.objcolhdisciplinefiletranunkid.Name = "objcolhdisciplinefiletranunkid"
        Me.objcolhdisciplinefiletranunkid.ReadOnly = True
        Me.objcolhdisciplinefiletranunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhdisciplinefiletranunkid.Visible = False
        '
        'objdgcolhProceedingMastId
        '
        DataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhProceedingMastId.DefaultCellStyle = DataGridViewCellStyle48
        Me.objdgcolhProceedingMastId.HeaderText = "objdgcolhProceedingMastId"
        Me.objdgcolhProceedingMastId.Name = "objdgcolhProceedingMastId"
        Me.objdgcolhProceedingMastId.ReadOnly = True
        Me.objdgcolhProceedingMastId.Visible = False
        '
        'lblChargeDescription
        '
        Me.lblChargeDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChargeDescription.Location = New System.Drawing.Point(472, 69)
        Me.lblChargeDescription.Name = "lblChargeDescription"
        Me.lblChargeDescription.Size = New System.Drawing.Size(84, 29)
        Me.lblChargeDescription.TabIndex = 238
        Me.lblChargeDescription.Text = "Charge Description"
        Me.lblChargeDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtChargeDescription
        '
        Me.txtChargeDescription.BackColor = System.Drawing.Color.White
        Me.txtChargeDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChargeDescription.Location = New System.Drawing.Point(562, 65)
        Me.txtChargeDescription.Multiline = True
        Me.txtChargeDescription.Name = "txtChargeDescription"
        Me.txtChargeDescription.ReadOnly = True
        Me.txtChargeDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtChargeDescription.Size = New System.Drawing.Size(329, 75)
        Me.txtChargeDescription.TabIndex = 119
        '
        'txtChargeDate
        '
        Me.txtChargeDate.BackColor = System.Drawing.Color.White
        Me.txtChargeDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChargeDate.Location = New System.Drawing.Point(129, 38)
        Me.txtChargeDate.Name = "txtChargeDate"
        Me.txtChargeDate.ReadOnly = True
        Me.txtChargeDate.Size = New System.Drawing.Size(113, 21)
        Me.txtChargeDate.TabIndex = 145
        '
        'txtinterdictiondate
        '
        Me.txtinterdictiondate.BackColor = System.Drawing.Color.White
        Me.txtinterdictiondate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtinterdictiondate.Location = New System.Drawing.Point(349, 38)
        Me.txtinterdictiondate.Name = "txtinterdictiondate"
        Me.txtinterdictiondate.ReadOnly = True
        Me.txtinterdictiondate.Size = New System.Drawing.Size(113, 21)
        Me.txtinterdictiondate.TabIndex = 144
        '
        'txtReferenceNo
        '
        Me.txtReferenceNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtReferenceNo.Flags = 0
        Me.txtReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReferenceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReferenceNo.Location = New System.Drawing.Point(562, 38)
        Me.txtReferenceNo.Name = "txtReferenceNo"
        Me.txtReferenceNo.ReadOnly = True
        Me.txtReferenceNo.Size = New System.Drawing.Size(186, 21)
        Me.txtReferenceNo.TabIndex = 95
        '
        'objLine1
        '
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(12, 143)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(879, 9)
        Me.objLine1.TabIndex = 117
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(469, 40)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(87, 17)
        Me.lblRefNo.TabIndex = 96
        Me.lblRefNo.Text = "Reference No."
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Enabled = False
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(129, 65)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(333, 21)
        Me.cboEmployee.TabIndex = 111
        '
        'txtDepartment
        '
        Me.txtDepartment.BackColor = System.Drawing.SystemColors.Window
        Me.txtDepartment.Flags = 0
        Me.txtDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDepartment.Location = New System.Drawing.Point(129, 119)
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.ReadOnly = True
        Me.txtDepartment.Size = New System.Drawing.Size(333, 21)
        Me.txtDepartment.TabIndex = 108
        '
        'txtJobTitle
        '
        Me.txtJobTitle.BackColor = System.Drawing.SystemColors.Window
        Me.txtJobTitle.Flags = 0
        Me.txtJobTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobTitle.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtJobTitle.Location = New System.Drawing.Point(129, 92)
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.ReadOnly = True
        Me.txtJobTitle.Size = New System.Drawing.Size(333, 21)
        Me.txtJobTitle.TabIndex = 107
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(12, 121)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(111, 17)
        Me.lblDepartment.TabIndex = 106
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobTitle
        '
        Me.lblJobTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobTitle.Location = New System.Drawing.Point(12, 94)
        Me.lblJobTitle.Name = "lblJobTitle"
        Me.lblJobTitle.Size = New System.Drawing.Size(111, 17)
        Me.lblJobTitle.TabIndex = 105
        Me.lblJobTitle.Text = "Job Title"
        Me.lblJobTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInterdictionDate
        '
        Me.lblInterdictionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterdictionDate.Location = New System.Drawing.Point(248, 40)
        Me.lblInterdictionDate.Name = "lblInterdictionDate"
        Me.lblInterdictionDate.Size = New System.Drawing.Size(95, 17)
        Me.lblInterdictionDate.TabIndex = 97
        Me.lblInterdictionDate.Text = "Interdiction Date "
        Me.lblInterdictionDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(12, 40)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(111, 17)
        Me.lblDate.TabIndex = 72
        Me.lblDate.Text = "Charge Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(12, 67)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(111, 17)
        Me.lblPersonalInvolved.TabIndex = 78
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objbtnOpen)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.pnlData)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 431)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(903, 55)
        Me.objFooter.TabIndex = 86
        '
        'objbtnOpen
        '
        Me.objbtnOpen.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnOpen.BackColor = System.Drawing.Color.White
        Me.objbtnOpen.BackgroundImage = CType(resources.GetObject("objbtnOpen.BackgroundImage"), System.Drawing.Image)
        Me.objbtnOpen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnOpen.BorderColor = System.Drawing.Color.Empty
        Me.objbtnOpen.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnOpen.FlatAppearance.BorderSize = 0
        Me.objbtnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnOpen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnOpen.ForeColor = System.Drawing.Color.Black
        Me.objbtnOpen.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnOpen.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnOpen.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnOpen.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnOpen.Location = New System.Drawing.Point(584, 13)
        Me.objbtnOpen.Name = "objbtnOpen"
        Me.objbtnOpen.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnOpen.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnOpen.Size = New System.Drawing.Size(204, 30)
        Me.objbtnOpen.TabIndex = 15
        Me.objbtnOpen.Text = "&Open"
        Me.objbtnOpen.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(794, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.objchkCheckAll)
        Me.pnlData.Controls.Add(Me.lvChargesCount)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(12, 33)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(10, 10)
        Me.pnlData.TabIndex = 142
        Me.pnlData.Visible = False
        '
        'objchkCheckAll
        '
        Me.objchkCheckAll.AutoSize = True
        Me.objchkCheckAll.Location = New System.Drawing.Point(6, 5)
        Me.objchkCheckAll.Name = "objchkCheckAll"
        Me.objchkCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkCheckAll.TabIndex = 2
        Me.objchkCheckAll.UseVisualStyleBackColor = True
        '
        'lvChargesCount
        '
        Me.lvChargesCount.BackColorOnChecked = True
        Me.lvChargesCount.CheckBoxes = True
        Me.lvChargesCount.ColumnHeaders = Nothing
        Me.lvChargesCount.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhCount, Me.colhIncident, Me.colhOffCategory, Me.colhOffence, Me.colhSeverity, Me.objcolhoffencecategoryunkid, Me.objcolhoffenceunkid, Me.objcolhfileunkid, Me.objcolhProceedingMastId})
        Me.lvChargesCount.CompulsoryColumns = ""
        Me.lvChargesCount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvChargesCount.FullRowSelect = True
        Me.lvChargesCount.GridLines = True
        Me.lvChargesCount.GroupingColumn = Nothing
        Me.lvChargesCount.HideSelection = False
        Me.lvChargesCount.Location = New System.Drawing.Point(0, 0)
        Me.lvChargesCount.MinColumnWidth = 50
        Me.lvChargesCount.MultiSelect = False
        Me.lvChargesCount.Name = "lvChargesCount"
        Me.lvChargesCount.OptionalColumns = ""
        Me.lvChargesCount.ShowItemToolTips = True
        Me.lvChargesCount.ShowMoreItem = False
        Me.lvChargesCount.ShowSaveItem = False
        Me.lvChargesCount.ShowSelectAll = True
        Me.lvChargesCount.ShowSizeAllColumnsToFit = True
        Me.lvChargesCount.Size = New System.Drawing.Size(10, 10)
        Me.lvChargesCount.Sortable = True
        Me.lvChargesCount.TabIndex = 1
        Me.lvChargesCount.UseCompatibleStateImageBehavior = False
        Me.lvChargesCount.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 25
        '
        'colhCount
        '
        Me.colhCount.Tag = "colhCount"
        Me.colhCount.Text = "Count"
        Me.colhCount.Width = 45
        '
        'colhIncident
        '
        Me.colhIncident.Tag = "colhIncident"
        Me.colhIncident.Text = "Incident"
        Me.colhIncident.Width = 285
        '
        'colhOffCategory
        '
        Me.colhOffCategory.Tag = "colhOffCategory"
        Me.colhOffCategory.Text = "Offence Category"
        Me.colhOffCategory.Width = 150
        '
        'colhOffence
        '
        Me.colhOffence.Tag = "colhOffence"
        Me.colhOffence.Text = "Offence Description"
        Me.colhOffence.Width = 300
        '
        'colhSeverity
        '
        Me.colhSeverity.Tag = "colhSeverity"
        Me.colhSeverity.Text = "Severity"
        Me.colhSeverity.Width = 0
        '
        'objcolhoffencecategoryunkid
        '
        Me.objcolhoffencecategoryunkid.Tag = "objcolhoffencecategoryunkid"
        Me.objcolhoffencecategoryunkid.Width = 0
        '
        'objcolhoffenceunkid
        '
        Me.objcolhoffenceunkid.Tag = "objcolhoffenceunkid"
        Me.objcolhoffenceunkid.Width = 0
        '
        'objcolhfileunkid
        '
        Me.objcolhfileunkid.Tag = "objcolhfileunkid"
        Me.objcolhfileunkid.Width = 0
        '
        'objcolhProceedingMastId
        '
        Me.objcolhProceedingMastId.Tag = "objcolhProceedingMastId"
        Me.objcolhProceedingMastId.Width = 0
        '
        'lblOpeningReason
        '
        Me.lblOpeningReason.BackColor = System.Drawing.Color.Transparent
        Me.lblOpeningReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpeningReason.Location = New System.Drawing.Point(505, 4)
        Me.lblOpeningReason.Name = "lblOpeningReason"
        Me.lblOpeningReason.Size = New System.Drawing.Size(101, 17)
        Me.lblOpeningReason.TabIndex = 244
        Me.lblOpeningReason.Text = "Opening Reason"
        Me.lblOpeningReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReason
        '
        Me.cboReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReason.FormattingEnabled = True
        Me.cboReason.Location = New System.Drawing.Point(612, 2)
        Me.cboReason.Name = "cboReason"
        Me.cboReason.Size = New System.Drawing.Size(252, 21)
        Me.cboReason.TabIndex = 245
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(870, 2)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 246
        '
        'frmOpenChargeStatus
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(903, 486)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOpenChargeStatus"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Select Count(s) to Open"
        Me.pnlMain.ResumeLayout(False)
        Me.gbDiscipilneFiling.ResumeLayout(False)
        Me.gbDiscipilneFiling.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvProceedingDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.pnlData.ResumeLayout(False)
        Me.pnlData.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents objbtnOpen As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbDiscipilneFiling As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtChargeDescription As System.Windows.Forms.TextBox
    Friend WithEvents txtReferenceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents txtDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtJobTitle As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblJobTitle As System.Windows.Forms.Label
    Friend WithEvents lblInterdictionDate As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents lvChargesCount As eZee.Common.eZeeListView
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIncident As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhOffCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhOffence As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSeverity As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhoffencecategoryunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhoffenceunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhfileunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents txtChargeDate As System.Windows.Forms.TextBox
    Friend WithEvents txtinterdictiondate As System.Windows.Forms.TextBox
    Friend WithEvents objcolhProceedingMastId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents lblChargeDescription As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvProceedingDetails As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhAdd As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhCount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhProceedingDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhProceedingDetails As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPIComments As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCommentRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDisciplineAction As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCommittee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInvestigator As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApproved As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCountStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhdisciplinefiletranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProceedingMastId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblOpeningReason As System.Windows.Forms.Label
    Friend WithEvents cboReason As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanapprover_mapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanapprover_mapping))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objChkASelect = New System.Windows.Forms.CheckBox
        Me.lvmapApprover = New eZee.Common.eZeeListView(Me.components)
        Me.objSelect = New System.Windows.Forms.ColumnHeader
        Me.colhUser = New System.Windows.Forms.ColumnHeader
        Me.colhMapApprover = New System.Windows.Forms.ColumnHeader
        Me.objcolhMapApproverunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhMapApprovertranunkid = New System.Windows.Forms.ColumnHeader
        Me.cboUser = New System.Windows.Forms.ComboBox
        Me.lblUser = New System.Windows.Forms.Label
        Me.objbtnUnassign = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvApprover = New System.Windows.Forms.ListView
        Me.objASelect = New System.Windows.Forms.ColumnHeader
        Me.colhFromEmployee = New System.Windows.Forms.ColumnHeader
        Me.objcolhapproveunkid = New System.Windows.Forms.ColumnHeader
        Me.objbtnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.pnlMain.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objbtnSearchEmployee)
        Me.pnlMain.Controls.Add(Me.objChkASelect)
        Me.pnlMain.Controls.Add(Me.lvmapApprover)
        Me.pnlMain.Controls.Add(Me.cboUser)
        Me.pnlMain.Controls.Add(Me.lblUser)
        Me.pnlMain.Controls.Add(Me.objbtnUnassign)
        Me.pnlMain.Controls.Add(Me.lvApprover)
        Me.pnlMain.Controls.Add(Me.objbtnAssign)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(569, 376)
        Me.pnlMain.TabIndex = 0
        '
        'objChkASelect
        '
        Me.objChkASelect.AutoSize = True
        Me.objChkASelect.Location = New System.Drawing.Point(20, 17)
        Me.objChkASelect.Name = "objChkASelect"
        Me.objChkASelect.Size = New System.Drawing.Size(15, 14)
        Me.objChkASelect.TabIndex = 1
        Me.objChkASelect.UseVisualStyleBackColor = True
        '
        'lvmapApprover
        '
        Me.lvmapApprover.BackColorOnChecked = True
        Me.lvmapApprover.CheckBoxes = True
        Me.lvmapApprover.ColumnHeaders = Nothing
        Me.lvmapApprover.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objSelect, Me.colhUser, Me.colhMapApprover, Me.objcolhMapApproverunkid, Me.objcolhMapApprovertranunkid})
        Me.lvmapApprover.CompulsoryColumns = ""
        Me.lvmapApprover.FullRowSelect = True
        Me.lvmapApprover.GridLines = True
        Me.lvmapApprover.GroupingColumn = Nothing
        Me.lvmapApprover.HideSelection = False
        Me.lvmapApprover.Location = New System.Drawing.Point(312, 39)
        Me.lvmapApprover.MinColumnWidth = 50
        Me.lvmapApprover.MultiSelect = False
        Me.lvmapApprover.Name = "lvmapApprover"
        Me.lvmapApprover.OptionalColumns = ""
        Me.lvmapApprover.ShowMoreItem = False
        Me.lvmapApprover.ShowSaveItem = False
        Me.lvmapApprover.ShowSelectAll = True
        Me.lvmapApprover.ShowSizeAllColumnsToFit = True
        Me.lvmapApprover.Size = New System.Drawing.Size(245, 278)
        Me.lvmapApprover.Sortable = True
        Me.lvmapApprover.TabIndex = 114
        Me.lvmapApprover.UseCompatibleStateImageBehavior = False
        Me.lvmapApprover.View = System.Windows.Forms.View.Details
        '
        'objSelect
        '
        Me.objSelect.Tag = "objSelect"
        Me.objSelect.Text = ""
        Me.objSelect.Width = 25
        '
        'colhUser
        '
        Me.colhUser.Tag = "colhUser"
        Me.colhUser.Text = "User"
        Me.colhUser.Width = 0
        '
        'colhMapApprover
        '
        Me.colhMapApprover.Tag = "colhMapApprover"
        Me.colhMapApprover.Text = "Mapped Approver"
        Me.colhMapApprover.Width = 215
        '
        'objcolhMapApproverunkid
        '
        Me.objcolhMapApproverunkid.Tag = "objcolhMapApproverunkid"
        Me.objcolhMapApproverunkid.Text = "MapApproverunkid"
        Me.objcolhMapApproverunkid.Width = 0
        '
        'objcolhMapApprovertranunkid
        '
        Me.objcolhMapApprovertranunkid.Tag = "objcolhMapApprovertranunkid"
        Me.objcolhMapApprovertranunkid.Text = "MapApprovertranunkid"
        Me.objcolhMapApprovertranunkid.Width = 0
        '
        'cboUser
        '
        Me.cboUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUser.FormattingEnabled = True
        Me.cboUser.Location = New System.Drawing.Point(364, 12)
        Me.cboUser.Name = "cboUser"
        Me.cboUser.Size = New System.Drawing.Size(167, 21)
        Me.cboUser.TabIndex = 113
        '
        'lblUser
        '
        Me.lblUser.Location = New System.Drawing.Point(309, 15)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(49, 15)
        Me.lblUser.TabIndex = 112
        Me.lblUser.Text = "User"
        Me.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnUnassign
        '
        Me.objbtnUnassign.BackColor = System.Drawing.Color.White
        Me.objbtnUnassign.BackgroundImage = CType(resources.GetObject("objbtnUnassign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUnassign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUnassign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUnassign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUnassign.FlatAppearance.BorderSize = 0
        Me.objbtnUnassign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUnassign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnUnassign.ForeColor = System.Drawing.Color.Black
        Me.objbtnUnassign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUnassign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnassign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnassign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnassign.Image = Global.Aruti.Main.My.Resources.Resources.Previous_24x24
        Me.objbtnUnassign.Location = New System.Drawing.Point(264, 192)
        Me.objbtnUnassign.Name = "objbtnUnassign"
        Me.objbtnUnassign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnassign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnassign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnUnassign.TabIndex = 111
        Me.objbtnUnassign.UseVisualStyleBackColor = True
        '
        'lvApprover
        '
        Me.lvApprover.CheckBoxes = True
        Me.lvApprover.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objASelect, Me.colhFromEmployee, Me.objcolhapproveunkid})
        Me.lvApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvApprover.Location = New System.Drawing.Point(12, 12)
        Me.lvApprover.Name = "lvApprover"
        Me.lvApprover.Size = New System.Drawing.Size(246, 305)
        Me.lvApprover.TabIndex = 108
        Me.lvApprover.UseCompatibleStateImageBehavior = False
        Me.lvApprover.View = System.Windows.Forms.View.Details
        '
        'objASelect
        '
        Me.objASelect.Tag = "objASelect"
        Me.objASelect.Text = ""
        Me.objASelect.Width = 25
        '
        'colhFromEmployee
        '
        Me.colhFromEmployee.Tag = "colhFromEmployee"
        Me.colhFromEmployee.Text = "Approver"
        Me.colhFromEmployee.Width = 215
        '
        'objcolhapproveunkid
        '
        Me.objcolhapproveunkid.Tag = "objcolhapproveunkid"
        Me.objcolhapproveunkid.Text = "approveunkid"
        Me.objcolhapproveunkid.Width = 0
        '
        'objbtnAssign
        '
        Me.objbtnAssign.BackColor = System.Drawing.Color.White
        Me.objbtnAssign.BackgroundImage = CType(resources.GetObject("objbtnAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAssign.FlatAppearance.BorderSize = 0
        Me.objbtnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.Image = Global.Aruti.Main.My.Resources.Resources.Next_24x24
        Me.objbtnAssign.Location = New System.Drawing.Point(264, 146)
        Me.objbtnAssign.Name = "objbtnAssign"
        Me.objbtnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnAssign.TabIndex = 110
        Me.objbtnAssign.UseVisualStyleBackColor = True
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 321)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(569, 55)
        Me.EZeeFooter1.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(357, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 114
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(460, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 115
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(537, 12)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 3
        '
        'frmLoanapprover_mapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(569, 376)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanapprover_mapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Loan Approver Mapping"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objbtnUnassign As eZee.Common.eZeeLightButton
    Friend WithEvents lvApprover As System.Windows.Forms.ListView
    Friend WithEvents colhFromEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents cboUser As System.Windows.Forms.ComboBox
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvmapApprover As eZee.Common.eZeeListView
    Friend WithEvents colhUser As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMapApprover As System.Windows.Forms.ColumnHeader
    Friend WithEvents objSelect As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhapproveunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMapApprovertranunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMapApproverunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objASelect As System.Windows.Forms.ColumnHeader
    Friend WithEvents objChkASelect As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
End Class

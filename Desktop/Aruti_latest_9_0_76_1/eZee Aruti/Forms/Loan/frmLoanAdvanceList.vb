﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.clsPayment_tran

Public Class frmLoanAdvanceList

#Region " Private Varaibles "
    Private objLoan_Advance As clsLoan_Advance
    Private ReadOnly mstrModuleName As String = "frmLoanAdvanceList"
    Private mintSeletedValue As Integer = 0

    'Sohail (05 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtLoanAdvanceList As DataTable
    Private mstrFilterTitle As String
    'Sohail (05 Apr 2012) -- End
    Private mdtLoanBalance As DataTable 'Sohail (29 Jun 2015)
#End Region

#Region " Private Function "
    Private Sub FillList()
        Dim dsLoanAdvance As New DataSet
        Dim dtTable As New DataTable
        Dim StrSearching As String = String.Empty
        Dim objExchangeRate As New clsExchangeRate
        mstrFilterTitle = "" 'Sohail (05 Apr 2012)
        Try

            If User._Object.Privilege._AllowToViewLoanAdvanceList = True Then                'Pinkal (02-Jul-2012) -- Start

            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsLoanAdvance = objLoan_Advance.GetList("Loan")
            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                    dsLoanAdvance = objLoan_Advance.GetList("Loan", , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            Else
            dsLoanAdvance = objLoan_Advance.GetList("Loan")
            End If
            'Sohail (13 Jan 2012) -- End

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                mstrFilterTitle &= ", " & lblEmployee.Text & " : " & cboEmployee.Text 'Sohail (05 Apr 2012)
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                StrSearching &= "AND loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
                mstrFilterTitle &= ", " & lblLoanScheme.Text & " : " & cboLoanScheme.Text 'Sohail (05 Apr 2012)
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearching &= "AND loan_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
                mstrFilterTitle &= ", " & lblStatus.Text & " : " & cboStatus.Text 'Sohail (05 Apr 2012)
            End If

            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " "
                mstrFilterTitle &= ", " & lblPayPeriod.Text & " : " & cboPayPeriod.Text 'Sohail (05 Apr 2012)
            End If

            If CInt(cboLoanAdvance.SelectedIndex) > 0 Then
                StrSearching &= "AND Loan_AdvanceUnkid = " & CInt(cboLoanAdvance.SelectedIndex) & " "
                mstrFilterTitle &= ", " & lblLoanAdvance.Text & " : " & cboLoanAdvance.Text 'Sohail (05 Apr 2012)
            End If

            If txtAmount.Text.Trim <> "" Then
                If txtAmount.Decimal > 0 Then 'Sohail (11 May 2011)
                    StrSearching &= "AND Amount = " & txtAmount.Decimal & " "
                    mstrFilterTitle &= ", " & lblAmount.Text & " : " & txtAmount.Text 'Sohail (05 Apr 2012)
                End If
            End If

            If txtVocNo.Text.Trim <> "" Then
                StrSearching &= "AND VocNo LIKE '%" & CStr(txtVocNo.Text) & "%'" & " "
                mstrFilterTitle &= ", " & lblVocNo.Text & " : " & txtVocNo.Text 'Sohail (05 Apr 2012)
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                mstrFilterTitle = mstrFilterTitle.Substring(2) 'Sohail (05 Apr 2012)
                dtTable = New DataView(dsLoanAdvance.Tables("Loan"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsLoanAdvance.Tables("Loan")
            End If

            'Sohail (05 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            mdtLoanAdvanceList = dtTable
            'Sohail (05 Apr 2012) -- End

                'Sohail (29 Jun 2015) -- Start
                'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
                Dim dsLoanBalance As DataSet = objLoan_Advance.GetLoanBalance("Balance", "", IIf(CInt(cboEmployee.SelectedValue) > 0, cboEmployee.SelectedValue.ToString, "").ToString, IIf(CInt(cboLoanScheme.SelectedValue) > 0, cboLoanScheme.SelectedValue.ToString, "").ToString, , , ConfigParameter._Object._FirstNamethenSurname, , , FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date, UserAccessLevel._AccessLevelFilterString)
                mdtLoanBalance = dsLoanBalance.Tables("Balance")
                'Sohail (29 Jun 2015) -- End

            lvLoanAdvance.Items.Clear()
            objExchangeRate._ExchangeRateunkid = 1
            Dim lvItem As New ListViewItem

                'Sohail (29 Jun 2015) -- Start
                'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                Dim decTotalAmt As Decimal = 0
                Dim decTotalBalance As Decimal = 0
                Dim decTotalPaid As Decimal = 0
                'Sohail (29 Jun 2015) -- End

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("VocNo").ToString
                'Anjan (02 Mar 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                lvItem.SubItems.Add(dtRow.Item("empcode").ToString)
                'Anjan (02 Mar 2012)-End 
                lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)
                'Anjan (02 Mar 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                lvItem.SubItems.Add(dtRow.Item("loancode").ToString)
                'Anjan (02 Mar 2012)-End 
                lvItem.SubItems.Add(dtRow.Item("LoanScheme").ToString)
                lvItem.SubItems.Add(dtRow.Item("Loan_Advance").ToString)
                lvItem.SubItems.Add(Format(CDec(dtRow.Item("Amount").ToString), objExchangeRate._fmtCurrency)) 'Sohail (11 May 2011)
                    decTotalAmt += CDec(Format(CDec(dtRow.Item("Amount").ToString), objExchangeRate._fmtCurrency)) 'Sohail (29 Jun 2015)

                'Sandeep ( 29 JUN 2012 ) -- START
                'ENHANCEMENT : TRA COMMENTS
                lvItem.SubItems.Add(dtRow.Item("Intallments").ToString)
                'Sandeep ( 29 JUN 2012 ) -- START

                    'Sohail (29 Jun 2015) -- Start
                    'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                    Dim intUnkID As Integer = CInt(dtRow.Item("loanadvancetranunkid"))
                    Dim dr_Row As List(Of DataRow) = (From p In dsLoanBalance.Tables("Balance") Where (CInt(p.Item("loanadvancetranunkid")) = intUnkID) Select (p)).ToList
                    If dr_Row.Count > 0 Then
                        lvItem.SubItems.Add(Format(CDec(dr_Row(dr_Row.Count - 1).Item("LoanAmountCF").ToString), objExchangeRate._fmtCurrency))
                        lvItem.SubItems.Add(Format(CDec(dtRow.Item("Amount")) - CDec(dr_Row(dr_Row.Count - 1).Item("LoanAmountCF")), objExchangeRate._fmtCurrency))

                        decTotalBalance += CDec(Format(CDec(dr_Row(dr_Row.Count - 1).Item("LoanAmountCF").ToString), objExchangeRate._fmtCurrency))
                        decTotalPaid += CDec(Format(CDec(dtRow.Item("Amount")) - CDec(dr_Row(dr_Row.Count - 1).Item("LoanAmountCF")), objExchangeRate._fmtCurrency))
                    Else
                        lvItem.SubItems.Add(Format(CDec(dtRow.Item("balance_amount").ToString), objExchangeRate._fmtCurrency))
                        lvItem.SubItems.Add(Format(CDec(dtRow.Item("Amount")) - CDec(dtRow.Item("balance_amount")), objExchangeRate._fmtCurrency))

                        decTotalBalance += CDec(Format(CDec(dtRow.Item("balance_amount").ToString), objExchangeRate._fmtCurrency))
                        decTotalPaid += CDec(Format(CDec(dtRow.Item("Amount")) - CDec(dtRow.Item("balance_amount")), objExchangeRate._fmtCurrency))
                    End If
                    'Sohail (29 Jun 2015) -- End

                lvItem.SubItems.Add(dtRow.Item("PeriodName").ToString)
                RemoveHandler cboStatus.SelectedValueChanged, AddressOf cboStatus_SelectionChangeCommitted
                cboStatus.SelectedValue = CInt(dtRow.Item("loan_statusunkid").ToString)
                AddHandler cboStatus.SelectedValueChanged, AddressOf cboStatus_SelectionChangeCommitted
                lvItem.SubItems.Add(cboStatus.Text.ToString)
                lvItem.SubItems.Add(dtRow.Item("isloan").ToString)
                lvItem.SubItems.Add(dtRow.Item("loan_statusunkid").ToString)

                'S.SANDEEP [ 12 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)
                'S.SANDEEP [ 12 MAY 2012 ] -- END

                'Anjan (02 Mar 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                lvItem.SubItems.Add(dtRow.Item("empcode").ToString)
                lvItem.SubItems.Add(dtRow.Item("loancode").ToString)
                'Anjan (02 Mar 2012)-End 

                'Sandeep ( 18 JAN 2011 ) -- START
                If CBool(dtRow.Item("isbrought_forward")) = True Then lvItem.ForeColor = Color.Gray
                'Sandeep ( 18 JAN 2011 ) -- END 


                    lvItem.Tag = dtRow.Item("loanadvancetranunkid")
                
                    lvLoanAdvance.Items.Add(lvItem)
                Next

                cboStatus.SelectedValue = mintSeletedValue

                'Sohail (29 Jun 2015) -- Start
                'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                If lvLoanAdvance.Items.Count > 0 Then
                    lvItem = New ListViewItem
                    lvItem.UseItemStyleForSubItems = False
                
                    lvItem.Text = "" 'VocNo
                    lvItem.ForeColor = Color.White
                    lvItem.BackColor = Color.Blue
                    lvItem.Font = New Font(Me.Font, FontStyle.Bold)

                    lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'empcode
                    lvItem.SubItems.Add("GRAND TOTAL : ", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'EmpName
                    lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'loancode
                    lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'LoanScheme
                    lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'Loan_Advance
                    lvItem.SubItems.Add(decTotalAmt.ToString, Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'Amount
                    lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'Intallments
                    lvItem.SubItems.Add(decTotalBalance.ToString, Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'LoanAmountCF
                    lvItem.SubItems.Add(decTotalPaid.ToString, Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'balance_amount
                    lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'PeriodName
                    lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'cboStatus.Text
                    lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'isloan
                    lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'loan_statusunkid
                    lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'employeeunkid
                    lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'empcode
                    lvItem.SubItems.Add("", Color.White, Color.Blue, New Font(Me.Font, FontStyle.Bold)) 'loancode

                lvLoanAdvance.Items.Add(lvItem)
                End If
                'Sohail (29 Jun 2015) -- End

            If lvLoanAdvance.Items.Count > 16 Then
                colhEmployee.Width = 170 - 20
            Else
                colhEmployee.Width = 170
            End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Try
            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmployee.GetEmployeeList("Employee", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, True)
            'End If

            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (23 Nov 2012) -- End
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Employee")
                .SelectedValue = 0
            End With

            dsCombos = objLoanScheme.getComboList(True, "LoanScheme")
            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("LoanScheme")
                .SelectedValue = 0
            End With

            dsCombos = objMaster.GetLoan_Saving_Status("LoanStatus")
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsCombos.Tables("LoanStatus")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = 0
            End With

            cboLoanAdvance.Items.Clear()
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 4, "Select"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 5, "Loan"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 6, "Advance"))
            cboLoanAdvance.SelectedIndex = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            btnDelete.Enabled = User._Object.Privilege._DeleteLoanAdvance
            btnEdit.Enabled = User._Object.Privilege._EditLoanAdvance
            btnChangeStatus.Enabled = User._Object.Privilege._AllowChangeLoanAvanceStatus
            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'mnuPayment.Enabled = User._Object.Privilege._AddLoanAdvancePayment
            'mnuReceived.Enabled = User._Object.Privilege._AddLoanAdvanceReceived

            mnuPayment.Enabled = User._Object.Privilege._AddLoanAdvancePayment
            mnuReceived.Enabled = User._Object.Privilege._AddLoanAdvanceReceived
            mnuViewApprovalForm.Enabled = clsArutiReportClass.Is_Report_Assigned(enArutiReport.BBL_Loan_Report)
            btnPreview.Enabled = User._Object.Privilege._AllowToViewLoanAdvanceList
            'S.SANDEEP [ 29 OCT 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " From's Events "
    Private Sub frmLoanAdvanceList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objLoan_Advance = New clsLoan_Advance
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            Call FillCombo()
            'Call FillList()

            If lvLoanAdvance.Items.Count > 0 Then lvLoanAdvance.Items(0).Selected = True
            lvLoanAdvance.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanAdvanceList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvLoanAdvance.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmLoanAdvanceList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmLoanAdvanceList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objLoan_Advance = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoan_Advance.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Advance"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "
    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            'Sandeep [ 09 Oct 2010 ] -- Start
            'Issues Reported by Vimal
            'cboLoanAdvance.SelectedValue = 0
            cboLoanAdvance.SelectedIndex = 0
            'Sandeep [ 09 Oct 2010 ] -- End 
            cboLoanScheme.SelectedValue = 0
            cboPayPeriod.SelectedValue = 0
            cboStatus.SelectedValue = 0
            txtAmount.Text = ""
            txtVocNo.Text = ""
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvLoanAdvance.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvLoanAdvance.Select()
                Exit Sub
            End If

            objLoan_Advance._Loanadvancetranunkid = CInt(lvLoanAdvance.SelectedItems(0).Tag)

            'Sandeep ( 18 JAN 2011 ) -- START
            If CBool(objLoan_Advance._Isbrought_Forward) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot delete this Loan/Advance. Reason : Loan/Advance is brought forward."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sandeep ( 18 JAN 2011 ) -- END 

            Select Case objLoan_Advance._LoanStatus
                Case 3, 4
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot edit Loan/Advance. Reason : Loan/Advance status is writtenoff or completed."), enMsgBoxStyle.Information)
                    Exit Sub
            End Select


            If objLoan_Advance._Isloan = True Then
                Dim objPaymentTran As New clsPayment_tran
                If objPaymentTran.IsPaymentDone(enPayTypeId.PAYMENT, CInt(lvLoanAdvance.SelectedItems(0).Tag), enPaymentRefId.LOAN) = True Then 'S.SANDEEP [ 29 May 2013 ] -- START-- END
                    'If objPaymentTran.IsPaymentDone(enPayTypeId.PAYMENT, CInt(lvLoanAdvance.SelectedItems(0).Tag)) = True Then
                    If objLoan_Advance._Balance_Amount <= objLoan_Advance._Net_Amount Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot delete the transaction. Reason : Deduction or Repayment is already done against this transaction."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            Else
                Dim objPaymentTran As New clsPayment_tran
                If objPaymentTran.IsPaymentDone(enPayTypeId.PAYMENT, CInt(lvLoanAdvance.SelectedItems(0).Tag), enPaymentRefId.ADVANCE) = True Then 'S.SANDEEP [ 29 May 2013 ] -- START-- END
                    'If objPaymentTran.IsPaymentDone(enPayTypeId.PAYMENT, CInt(lvLoanAdvance.SelectedItems(0).Tag)) = True Then
                    If objLoan_Advance._Balance_Amount <= objLoan_Advance._Advance_Amount Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot delete the transaction. Reason : Deduction or Repayment is already done against this transaction."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                objPaymentTran = Nothing
            End If


            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLoanAdvance.SelectedItems(0).Index

            'If objLoan_Advance.isUsed(CInt(strTag(0))) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Loan. Reason: This Loan is in use."), enMsgBoxStyle.Information) '?2
            '    lvLoanAdvance.Select()
            '    Exit Sub
            'End If

            'Dim intSelectedIndex As Integer
            'intSelectedIndex = lvLoanAdvance.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Loan?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objLoan_Advance._Isvoid = True

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objLoan_Advance._Voidreason = "Testing"
                'objLoan_Advance._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.LOAN, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objLoan_Advance._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                'objLoan_Advance._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                objLoan_Advance._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End 

                objLoan_Advance._Voiduserunkid = User._Object._Userunkid

                objLoan_Advance.Delete(CInt(lvLoanAdvance.SelectedItems(0).Tag))
                'Sandeep [ 09 Oct 2010 ] -- Start
                If objLoan_Advance._Message <> "" Then
                    eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
                Else
                    lvLoanAdvance.SelectedItems(0).Remove()
                End If
                'Sandeep [ 09 Oct 2010 ] -- End 

                If lvLoanAdvance.Items.Count <= 0 Then
                    Exit Try
                End If

               
            End If
            lvLoanAdvance.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmLoanAdvance_AddEdit
        Try
            If lvLoanAdvance.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation on it."), enMsgBoxStyle.Information)
                lvLoanAdvance.Select()
                Exit Sub
            End If

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLoanAdvance.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Select Case CInt(lvLoanAdvance.SelectedItems(0).SubItems(objcolhStatusUnkid.Index).Text)
                Case 3, 4
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot edit Loan/Advance. Reason : Loan/Advance status is writtenoff or completed."), enMsgBoxStyle.Information)
                    lvLoanAdvance.Select()
                    Exit Sub
            End Select

            If frm.displayDialog(CInt(lvLoanAdvance.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If

            frm = Nothing

          

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmLoanAdvance_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnChangeStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
        Dim frm As New frmLoanStatus_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If lvLoanAdvance.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation on it."), enMsgBoxStyle.Information)
                lvLoanAdvance.Select()
                Exit Sub
            End If

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLoanAdvance.SelectedItems(0).Index

            frm.displayDialog(CInt(lvLoanAdvance.SelectedItems(0).Tag), True)

            Call FillList()

            frm = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnChangeStatus_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (05 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Try
            If mdtLoanAdvanceList Is Nothing OrElse mdtLoanAdvanceList.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please generate list to Preview report."), enMsgBoxStyle.Information)
                lvLoanAdvance.Select()
                Exit Sub
            End If

            Dim objDic As New Dictionary(Of Integer, String)
            For Each dsRow As DataRow In CType(cboStatus.DataSource, DataTable).Rows
                objDic.Add(CInt(dsRow.Item("Id")), dsRow.Item("Name").ToString)
            Next

            Dim objRptLoanAdvance As New ArutiReports.clsLoanAdvanceReport
            objRptLoanAdvance._Data_LoanAdvance = mdtLoanAdvanceList
            objRptLoanAdvance._Data_LoanBalance = mdtLoanBalance 'Sohail (29 Jun 2015)
            objRptLoanAdvance._Status_Dic = objDic
            objRptLoanAdvance._FilterTitle = mstrFilterTitle

            objRptLoanAdvance.generateReport(0, enPrintAction.Preview, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPreview_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (05 Apr 2012) -- End

    'Private Sub btnPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim frm As New frmPayment_AddEdit
    '    Try
    '        If lvLoanAdvance.SelectedItems.Count <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation on it."), enMsgBoxStyle.Information)
    '            lvLoanAdvance.Select()
    '            Exit Sub
    '        End If

    '        Dim intSelectedIndex As Integer
    '        intSelectedIndex = lvLoanAdvance.SelectedItems(0).Index

    '        If lvLoanAdvance.SelectedItems(0).SubItems(colhIsLoan.Index).Text.ToUpper = "TRUE" Then
    '            If frm.displayDialog(-1, enAction.ADD_ONE, enPaymentRefId.LOAN, CInt(lvLoanAdvance.SelectedItems(0).Tag),enPayTypeId.) Then
    '                Call FillList()
    '            End If
    '        Else
    '            If frm.displayDialog(-1, enAction.ADD_ONE, enPaymentRefId.ADVANCE, CInt(lvLoanAdvance.SelectedItems(0).Tag)) Then
    '                Call FillList()
    '            End If
    '        End If

    '        frm = Nothing

    '        lvLoanAdvance.Items(intSelectedIndex).Selected = True
    '        lvLoanAdvance.EnsureVisible(intSelectedIndex)
    '        lvLoanAdvance.Select()

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnPayment_Click", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Controls "
    Private Sub cboStatus_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedValueChanged

        Try
            mintSeletedValue = CInt(cboStatus.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub
#End Region

    Private Sub btnOperations_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOperations.Click
        Dim frm As New frmPaymentList
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If lvLoanAdvance.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation on it."), enMsgBoxStyle.Information)
                lvLoanAdvance.Select()
                Exit Sub
            End If

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            Dim objPendingLoan As New clsProcess_pending_loan
            objLoan_Advance._Loanadvancetranunkid = CInt(lvLoanAdvance.SelectedItems(0).Tag)
            objPendingLoan._Processpendingloanunkid = objLoan_Advance._Processpendingloanunkid
            If objPendingLoan._Isexternal_Entity = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry you cannot make payment operation on Loan/Advance.Reason : This Loan/Advance is from external Enity."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [ 29 May 2013 ] -- END

            If lvLoanAdvance.SelectedItems(0).SubItems(colhIsLoan.Index).Text.ToUpper = "TRUE" Then
                frm._Reference_Id = enPaymentRefId.LOAN
            Else
                frm._Reference_Id = enPaymentRefId.ADVANCE
            End If
            frm._Transaction_Id = CInt(lvLoanAdvance.SelectedItems(0).Tag)
            frm._PaymentType_Id = enPayTypeId.PAYMENT

            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOperations_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuReceived_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReceived.Click
        Dim frm As New frmPaymentList
        Try
            If lvLoanAdvance.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation on it."), enMsgBoxStyle.Information)
                lvLoanAdvance.Select()
                Exit Sub
            End If

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            'Sandeep [ 16 Oct 2010 ] -- Start


            'S.SANDEEP [ 22 APR 2014 ] -- START
            'Dim objPendingLoan As New clsProcess_pending_loan
            'objLoan_Advance._Loanadvancetranunkid = CInt(lvLoanAdvance.SelectedItems(0).Tag)
            'objPendingLoan._Processpendingloanunkid = objLoan_Advance._Processpendingloanunkid
            'If objPendingLoan._Isexternal_Entity = True Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry you cannot make payment operation on Loan/Advance.Reason : This Loan/Advance is from external Enity."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            Dim objPendingLoan As New clsProcess_pending_loan
            objLoan_Advance._Loanadvancetranunkid = CInt(lvLoanAdvance.SelectedItems(0).Tag)
            objPendingLoan._Processpendingloanunkid = objLoan_Advance._Processpendingloanunkid
            If objPendingLoan._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry you cannot make payment operation on Loan/Advance.Reason : This Loan/Advance is from external Enity."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [ 22 APR 2014 ] -- END

            
            'Sandeep [ 16 Oct 2010 ] -- End 

            If lvLoanAdvance.SelectedItems(0).SubItems(colhIsLoan.Index).Text.ToUpper = "TRUE" Then
                frm._Reference_Id = enPaymentRefId.LOAN
            Else
                frm._Reference_Id = enPaymentRefId.ADVANCE
            End If

            frm._Transaction_Id = CInt(lvLoanAdvance.SelectedItems(0).Tag)
            frm._PaymentType_Id = enPayTypeId.RECEIVED

            frm.ShowDialog()

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPayment_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPayment.Click
        Dim frm As New frmPaymentList
        Try
            If lvLoanAdvance.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation on it."), enMsgBoxStyle.Information)
                lvLoanAdvance.Select()
                Exit Sub

            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            'Sandeep [ 16 Oct 2010 ] -- Start
            Dim objPendingLoan As New clsProcess_pending_loan
            objLoan_Advance._Loanadvancetranunkid = CInt(lvLoanAdvance.SelectedItems(0).Tag)
            objPendingLoan._Processpendingloanunkid = objLoan_Advance._Processpendingloanunkid
            If objPendingLoan._Isexternal_Entity = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry you cannot make payment operation on Loan/Advance.Reason : This Loan/Advance is from external Enity."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sandeep [ 16 Oct 2010 ] -- End 

            If lvLoanAdvance.SelectedItems(0).SubItems(colhIsLoan.Index).Text.ToUpper = "TRUE" Then
                frm._Reference_Id = enPaymentRefId.LOAN
            Else
                frm._Reference_Id = enPaymentRefId.ADVANCE
            End If
            frm._Transaction_Id = CInt(lvLoanAdvance.SelectedItems(0).Tag)
            frm._PaymentType_Id = enPayTypeId.PAYMENT


            frm.ShowDialog()

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPayment_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lvLoanAdvance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvLoanAdvance.Click, lvLoanAdvance.SelectedIndexChanged
        Try
            If lvLoanAdvance.SelectedItems.Count > 0 Then
                Call SetVisibility()

                'Sohail (29 Jun 2015) -- Start
                'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                If lvLoanAdvance.SelectedItems(0).SubItems(objcolhStatusUnkid.Index).Text = "" Then 'Grand Total Row
                    btnDelete.Enabled = False
                    btnEdit.Enabled = False
                    btnChangeStatus.Enabled = False

                    mnuPayment.Enabled = False
                    mnuReceived.Enabled = False
                    mnuViewApprovalForm.Enabled = False
                    btnPreview.Enabled = False

                    btnOperations.Enabled = False
                    btnChangeStatus.Enabled = False
                    Exit Sub
                End If
                'Sohail (29 Jun 2015) -- End

                Select Case CInt(lvLoanAdvance.SelectedItems(0).SubItems(objcolhStatusUnkid.Index).Text)
                    Case 2
                        btnOperations.Enabled = False
                        btnChangeStatus.Enabled = True
                    Case 3, 4
                        btnOperations.Enabled = False
                        btnChangeStatus.Enabled = False
                    Case Else
                        btnOperations.Enabled = True
                        btnChangeStatus.Enabled = True
                End Select
                Dim objPaymentTran As New clsPayment_tran

                'S.SANDEEP [ 29 May 2013 ] -- START
                'ENHANCEMENT : ATLAS COPCO WEB CHANGES
                Dim ePRef As enPaymentRefId
                If CBool(lvLoanAdvance.SelectedItems(0).SubItems(colhIsLoan.Index).Text) = True Then
                    ePRef = enPaymentRefId.LOAN
                Else
                    ePRef = enPaymentRefId.ADVANCE
                End If
                'S.SANDEEP [ 29 May 2013 ] -- END

                If objPaymentTran.IsPaymentDone(enPayTypeId.PAYMENT, CInt(lvLoanAdvance.SelectedItems(0).Tag), ePRef) = False Then 'S.SANDEEP [ 29 May 2013 ] -- START-- END
                    ' If objPaymentTran.IsPaymentDone(enPayTypeId.PAYMENT, CInt(lvLoanAdvance.SelectedItems(0).Tag)) = False Then
                    'S.SANDEEP [ 22 APR 2014 ] -- START
                    'mnuReceived.Enabled = False

                    mnuReceived.Enabled = False

                    Dim objPendingLoan As New clsProcess_pending_loan
                    objLoan_Advance._Loanadvancetranunkid = CInt(lvLoanAdvance.SelectedItems(0).Tag)
                    objPendingLoan._Processpendingloanunkid = objLoan_Advance._Processpendingloanunkid
                    If objPendingLoan._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount > 0 Then
                        mnuReceived.Enabled = True
                    End If
                    'S.SANDEEP [ 22 APR 2014 ] -- END
                Else
                    mnuReceived.Enabled = True
                End If
                objPaymentTran = Nothing
            End If

           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLoanAdvance_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTrnHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTrnHead.Click
        Dim objFrm As New frmCommonSearch
        Try

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTrnHead_Click", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private Sub objLoanSchemeSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objLoanSchemeSearch.Click
        Dim objFrm As New frmCommonSearch
        'Dim objLoanScheme As New clsLoan_Scheme
        Try

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            With objFrm
                .ValueMember = cboLoanScheme.ValueMember
                .DisplayMember = cboLoanScheme.DisplayMember
                .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                .CodeMember = "Code"
            End With

            If objFrm.DisplayDialog Then
                cboLoanScheme.SelectedValue = objFrm.SelectedValue
                cboLoanScheme.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objLoanSchemeSearch_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Mar 2012)-End 

    Private Sub mnuViewApprovalForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewApprovalForm.Click
        Try
            If lvLoanAdvance.SelectedItems.Count > 0 Then
                Dim frm As New ArutiReports.frmBBL_Loan_Report
                frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
                frm.StartPosition = FormStartPosition.CenterParent
                frm.WindowState = FormWindowState.Normal
                frm._EmpId = CInt(lvLoanAdvance.SelectedItems(0).SubItems(objcolhemployeeunkid.Index).Text)
                frm.ShowDialog()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewApprovalForm_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 26 SEPT 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuChangeStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuChangeStatus.Click
        Dim frm As New frmGlobalLoanStatus_Change
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuChangeStatus_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 26 SEPT 2013 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnChangeStatus.GradientBackColor = GUI._ButttonBackColor 
			Me.btnChangeStatus.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperations.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

			Me.btnPreview.GradientBackColor = GUI._ButttonBackColor 
			Me.btnPreview.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.Name, Me.lblLoanAdvance.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnChangeStatus.Text = Language._Object.getCaption(Me.btnChangeStatus.Name, Me.btnChangeStatus.Text)
			Me.colhVocNo.Text = Language._Object.getCaption(CStr(Me.colhVocNo.Tag), Me.colhVocNo.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhLoanScheme.Text = Language._Object.getCaption(CStr(Me.colhLoanScheme.Tag), Me.colhLoanScheme.Text)
			Me.colhLoan_Advance.Text = Language._Object.getCaption(CStr(Me.colhLoan_Advance.Tag), Me.colhLoan_Advance.Text)
			Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
			Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
			Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
			Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
			Me.lblVocNo.Text = Language._Object.getCaption(Me.lblVocNo.Name, Me.lblVocNo.Text)
			Me.colhIsLoan.Text = Language._Object.getCaption(CStr(Me.colhIsLoan.Tag), Me.colhIsLoan.Text)
			Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
			Me.mnuPayment.Text = Language._Object.getCaption(Me.mnuPayment.Name, Me.mnuPayment.Text)
			Me.mnuReceived.Text = Language._Object.getCaption(Me.mnuReceived.Name, Me.mnuReceived.Text)
			Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)
			Me.colhLoanCode.Text = Language._Object.getCaption(CStr(Me.colhLoanCode.Tag), Me.colhLoanCode.Text)
			Me.btnPreview.Text = Language._Object.getCaption(Me.btnPreview.Name, Me.btnPreview.Text)
			Me.mnuViewApprovalForm.Text = Language._Object.getCaption(Me.mnuViewApprovalForm.Name, Me.mnuViewApprovalForm.Text)
			Me.colhInstallments.Text = Language._Object.getCaption(CStr(Me.colhInstallments.Tag), Me.colhInstallments.Text)
			Me.mnuChangeStatus.Text = Language._Object.getCaption(Me.mnuChangeStatus.Name, Me.mnuChangeStatus.Text)
			Me.colhBalance.Text = Language._Object.getCaption(CStr(Me.colhBalance.Tag), Me.colhBalance.Text)
			Me.colhPaidLoan.Text = Language._Object.getCaption(CStr(Me.colhPaidLoan.Tag), Me.colhPaidLoan.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Sorry, you cannot delete this Loan/Advance. Reason : Loan/Advance is brought forward.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Loan?")
			Language.setMessage(mstrModuleName, 4, "Select")
			Language.setMessage(mstrModuleName, 5, "Loan")
			Language.setMessage(mstrModuleName, 6, "Advance")
			Language.setMessage(mstrModuleName, 7, "Sorry, you cannot edit Loan/Advance. Reason : Loan/Advance status is writtenoff or completed.")
			Language.setMessage(mstrModuleName, 8, "Sorry, you cannot delete the transaction. Reason : Deduction or Repayment is already done against this transaction.")
			Language.setMessage(mstrModuleName, 9, "Sorry you cannot make payment operation on Loan/Advance.Reason : This Loan/Advance is from external Enity.")
			Language.setMessage(mstrModuleName, 10, "Please generate list to Preview report.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanStatus_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanStatus_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbLoanStatusInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAddSettlement = New System.Windows.Forms.LinkLabel
        Me.lblBalance = New System.Windows.Forms.Label
        Me.txtBalance = New eZee.TextBox.NumericTextBox
        Me.txtSettlementAmt = New eZee.TextBox.NumericTextBox
        Me.lblSettlementAmt = New System.Windows.Forms.Label
        Me.txtLoanScheme = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmployeeName = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbLoanStatusInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbLoanStatusInfo)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(496, 352)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 297)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(496, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(284, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(387, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbLoanStatusInfo
        '
        Me.gbLoanStatusInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLoanStatusInfo.Checked = False
        Me.gbLoanStatusInfo.CollapseAllExceptThis = False
        Me.gbLoanStatusInfo.CollapsedHoverImage = Nothing
        Me.gbLoanStatusInfo.CollapsedNormalImage = Nothing
        Me.gbLoanStatusInfo.CollapsedPressedImage = Nothing
        Me.gbLoanStatusInfo.CollapseOnLoad = False
        Me.gbLoanStatusInfo.Controls.Add(Me.lnkAddSettlement)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblBalance)
        Me.gbLoanStatusInfo.Controls.Add(Me.txtBalance)
        Me.gbLoanStatusInfo.Controls.Add(Me.txtSettlementAmt)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblSettlementAmt)
        Me.gbLoanStatusInfo.Controls.Add(Me.txtLoanScheme)
        Me.gbLoanStatusInfo.Controls.Add(Me.txtEmployeeName)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblRemarks)
        Me.gbLoanStatusInfo.Controls.Add(Me.txtRemarks)
        Me.gbLoanStatusInfo.Controls.Add(Me.cboStatus)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblStatus)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblEmpName)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblLoanScheme)
        Me.gbLoanStatusInfo.Controls.Add(Me.dtpDate)
        Me.gbLoanStatusInfo.Controls.Add(Me.lblDate)
        Me.gbLoanStatusInfo.ExpandedHoverImage = Nothing
        Me.gbLoanStatusInfo.ExpandedNormalImage = Nothing
        Me.gbLoanStatusInfo.ExpandedPressedImage = Nothing
        Me.gbLoanStatusInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLoanStatusInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLoanStatusInfo.HeaderHeight = 25
        Me.gbLoanStatusInfo.HeightOnCollapse = 0
        Me.gbLoanStatusInfo.LeftTextSpace = 0
        Me.gbLoanStatusInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbLoanStatusInfo.Name = "gbLoanStatusInfo"
        Me.gbLoanStatusInfo.OpenHeight = 300
        Me.gbLoanStatusInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLoanStatusInfo.ShowBorder = True
        Me.gbLoanStatusInfo.ShowCheckBox = False
        Me.gbLoanStatusInfo.ShowCollapseButton = False
        Me.gbLoanStatusInfo.ShowDefaultBorderColor = True
        Me.gbLoanStatusInfo.ShowDownButton = False
        Me.gbLoanStatusInfo.ShowHeader = True
        Me.gbLoanStatusInfo.Size = New System.Drawing.Size(472, 225)
        Me.gbLoanStatusInfo.TabIndex = 0
        Me.gbLoanStatusInfo.Temp = 0
        Me.gbLoanStatusInfo.Text = "Loan Status Info"
        Me.gbLoanStatusInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAddSettlement
        '
        Me.lnkAddSettlement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAddSettlement.Location = New System.Drawing.Point(227, 118)
        Me.lnkAddSettlement.Name = "lnkAddSettlement"
        Me.lnkAddSettlement.Size = New System.Drawing.Size(232, 15)
        Me.lnkAddSettlement.TabIndex = 252
        Me.lnkAddSettlement.TabStop = True
        Me.lnkAddSettlement.Text = "Add Settlement"
        '
        'lblBalance
        '
        Me.lblBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalance.Location = New System.Drawing.Point(268, 144)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(78, 15)
        Me.lblBalance.TabIndex = 250
        Me.lblBalance.Text = "Balance"
        '
        'txtBalance
        '
        Me.txtBalance.AllowNegative = True
        Me.txtBalance.DigitsInGroup = 0
        Me.txtBalance.Flags = 0
        Me.txtBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalance.Location = New System.Drawing.Point(352, 141)
        Me.txtBalance.MaxDecimalPlaces = 6
        Me.txtBalance.MaxWholeDigits = 21
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.Prefix = ""
        Me.txtBalance.RangeMax = 1.7976931348623157E+308
        Me.txtBalance.RangeMin = -1.7976931348623157E+308
        Me.txtBalance.ReadOnly = True
        Me.txtBalance.Size = New System.Drawing.Size(107, 21)
        Me.txtBalance.TabIndex = 249
        Me.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSettlementAmt
        '
        Me.txtSettlementAmt.AllowNegative = True
        Me.txtSettlementAmt.DigitsInGroup = 0
        Me.txtSettlementAmt.Flags = 0
        Me.txtSettlementAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSettlementAmt.Location = New System.Drawing.Point(113, 141)
        Me.txtSettlementAmt.MaxDecimalPlaces = 6
        Me.txtSettlementAmt.MaxWholeDigits = 21
        Me.txtSettlementAmt.Name = "txtSettlementAmt"
        Me.txtSettlementAmt.Prefix = ""
        Me.txtSettlementAmt.RangeMax = 1.7976931348623157E+308
        Me.txtSettlementAmt.RangeMin = -1.7976931348623157E+308
        Me.txtSettlementAmt.ReadOnly = True
        Me.txtSettlementAmt.Size = New System.Drawing.Size(107, 21)
        Me.txtSettlementAmt.TabIndex = 5
        Me.txtSettlementAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblSettlementAmt
        '
        Me.lblSettlementAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSettlementAmt.Location = New System.Drawing.Point(8, 144)
        Me.lblSettlementAmt.Name = "lblSettlementAmt"
        Me.lblSettlementAmt.Size = New System.Drawing.Size(99, 15)
        Me.lblSettlementAmt.TabIndex = 247
        Me.lblSettlementAmt.Text = "Settlement Amount"
        '
        'txtLoanScheme
        '
        Me.txtLoanScheme.Flags = 0
        Me.txtLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanScheme.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLoanScheme.Location = New System.Drawing.Point(113, 60)
        Me.txtLoanScheme.Name = "txtLoanScheme"
        Me.txtLoanScheme.ReadOnly = True
        Me.txtLoanScheme.Size = New System.Drawing.Size(346, 21)
        Me.txtLoanScheme.TabIndex = 1
        '
        'txtEmployeeName
        '
        Me.txtEmployeeName.Flags = 0
        Me.txtEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployeeName.Location = New System.Drawing.Point(113, 87)
        Me.txtEmployeeName.Name = "txtEmployeeName"
        Me.txtEmployeeName.ReadOnly = True
        Me.txtEmployeeName.Size = New System.Drawing.Size(346, 21)
        Me.txtEmployeeName.TabIndex = 2
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(8, 170)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(99, 15)
        Me.lblRemarks.TabIndex = 238
        Me.lblRemarks.Text = "Remarks"
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(124)}
        Me.txtRemarks.Location = New System.Drawing.Point(113, 168)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtRemarks.Size = New System.Drawing.Size(346, 49)
        Me.txtRemarks.TabIndex = 6
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(113, 114)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(107, 21)
        Me.cboStatus.TabIndex = 3
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(8, 117)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(99, 15)
        Me.lblStatus.TabIndex = 233
        Me.lblStatus.Text = "Loan Status"
        '
        'lblEmpName
        '
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(8, 90)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(99, 15)
        Me.lblEmpName.TabIndex = 232
        Me.lblEmpName.Text = "Employee Name"
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(8, 63)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(99, 15)
        Me.lblLoanScheme.TabIndex = 5
        Me.lblLoanScheme.Text = "Loan Scheme"
        '
        'dtpDate
        '
        Me.dtpDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Checked = False
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(113, 33)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(107, 21)
        Me.dtpDate.TabIndex = 0
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(8, 36)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(99, 15)
        Me.lblDate.TabIndex = 4
        Me.lblDate.Text = "Date"
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(496, 60)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Add / Edit Loan Status"
        '
        'frmLoanStatus_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(496, 352)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanStatus_AddEdit"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Loan Status"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbLoanStatusInfo.ResumeLayout(False)
        Me.gbLoanStatusInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbLoanStatusInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtLoanScheme As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmployeeName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSettlementAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblSettlementAmt As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents txtBalance As eZee.TextBox.NumericTextBox
    Friend WithEvents lnkAddSettlement As System.Windows.Forms.LinkLabel
End Class

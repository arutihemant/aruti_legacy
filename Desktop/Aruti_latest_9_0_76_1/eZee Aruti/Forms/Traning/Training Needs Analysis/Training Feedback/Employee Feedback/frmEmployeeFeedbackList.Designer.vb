﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeFeedbackList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeFeedbackList))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lvFeedbackList = New eZee.Common.eZeeListView(Me.components)
        Me.colhFeedbackDate = New System.Windows.Forms.ColumnHeader
        Me.colhECode = New System.Windows.Forms.ColumnHeader
        Me.colhEName = New System.Windows.Forms.ColumnHeader
        Me.objcolhTitle = New System.Windows.Forms.ColumnHeader
        Me.gbEmployeeInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchCourseTitle = New eZee.Common.eZeeGradientButton
        Me.cboCourseTitle = New System.Windows.Forms.ComboBox
        Me.lblCourseTitle = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1.SuspendLayout()
        Me.gbEmployeeInfo.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lvFeedbackList)
        Me.Panel1.Controls.Add(Me.gbEmployeeInfo)
        Me.Panel1.Controls.Add(Me.eZeeHeader)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(682, 400)
        Me.Panel1.TabIndex = 0
        '
        'lvFeedbackList
        '
        Me.lvFeedbackList.BackColorOnChecked = True
        Me.lvFeedbackList.ColumnHeaders = Nothing
        Me.lvFeedbackList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhFeedbackDate, Me.colhECode, Me.colhEName, Me.objcolhTitle})
        Me.lvFeedbackList.CompulsoryColumns = ""
        Me.lvFeedbackList.FullRowSelect = True
        Me.lvFeedbackList.GridLines = True
        Me.lvFeedbackList.GroupingColumn = Nothing
        Me.lvFeedbackList.HideSelection = False
        Me.lvFeedbackList.Location = New System.Drawing.Point(12, 135)
        Me.lvFeedbackList.MinColumnWidth = 50
        Me.lvFeedbackList.MultiSelect = False
        Me.lvFeedbackList.Name = "lvFeedbackList"
        Me.lvFeedbackList.OptionalColumns = ""
        Me.lvFeedbackList.ShowMoreItem = False
        Me.lvFeedbackList.ShowSaveItem = False
        Me.lvFeedbackList.ShowSelectAll = True
        Me.lvFeedbackList.ShowSizeAllColumnsToFit = True
        Me.lvFeedbackList.Size = New System.Drawing.Size(658, 206)
        Me.lvFeedbackList.Sortable = True
        Me.lvFeedbackList.TabIndex = 120
        Me.lvFeedbackList.UseCompatibleStateImageBehavior = False
        Me.lvFeedbackList.View = System.Windows.Forms.View.Details
        '
        'colhFeedbackDate
        '
        Me.colhFeedbackDate.Tag = "colhFeedbackDate"
        Me.colhFeedbackDate.Text = "Feedback Date"
        Me.colhFeedbackDate.Width = 100
        '
        'colhECode
        '
        Me.colhECode.Tag = "colhECode"
        Me.colhECode.Text = "Employee Code"
        Me.colhECode.Width = 120
        '
        'colhEName
        '
        Me.colhEName.Tag = "colhEName"
        Me.colhEName.Text = "Employee"
        Me.colhEName.Width = 433
        '
        'objcolhTitle
        '
        Me.objcolhTitle.Tag = "objcolhTitle"
        Me.objcolhTitle.Text = "objcolhTitle"
        Me.objcolhTitle.Width = 0
        '
        'gbEmployeeInfo
        '
        Me.gbEmployeeInfo.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.Checked = False
        Me.gbEmployeeInfo.CollapseAllExceptThis = False
        Me.gbEmployeeInfo.CollapsedHoverImage = Nothing
        Me.gbEmployeeInfo.CollapsedNormalImage = Nothing
        Me.gbEmployeeInfo.CollapsedPressedImage = Nothing
        Me.gbEmployeeInfo.CollapseOnLoad = False
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearchCourseTitle)
        Me.gbEmployeeInfo.Controls.Add(Me.cboCourseTitle)
        Me.gbEmployeeInfo.Controls.Add(Me.lblCourseTitle)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnReset)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearch)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.lblEmployee)
        Me.gbEmployeeInfo.ExpandedHoverImage = Nothing
        Me.gbEmployeeInfo.ExpandedNormalImage = Nothing
        Me.gbEmployeeInfo.ExpandedPressedImage = Nothing
        Me.gbEmployeeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeInfo.HeaderHeight = 25
        Me.gbEmployeeInfo.HeaderMessage = ""
        Me.gbEmployeeInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.HeightOnCollapse = 0
        Me.gbEmployeeInfo.LeftTextSpace = 0
        Me.gbEmployeeInfo.Location = New System.Drawing.Point(12, 64)
        Me.gbEmployeeInfo.Name = "gbEmployeeInfo"
        Me.gbEmployeeInfo.OpenHeight = 300
        Me.gbEmployeeInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeInfo.ShowBorder = True
        Me.gbEmployeeInfo.ShowCheckBox = False
        Me.gbEmployeeInfo.ShowCollapseButton = False
        Me.gbEmployeeInfo.ShowDefaultBorderColor = True
        Me.gbEmployeeInfo.ShowDownButton = False
        Me.gbEmployeeInfo.ShowHeader = True
        Me.gbEmployeeInfo.Size = New System.Drawing.Size(658, 64)
        Me.gbEmployeeInfo.TabIndex = 119
        Me.gbEmployeeInfo.Temp = 0
        Me.gbEmployeeInfo.Text = "Filter Criteria"
        Me.gbEmployeeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCourseTitle
        '
        Me.objbtnSearchCourseTitle.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCourseTitle.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourseTitle.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourseTitle.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCourseTitle.BorderSelected = False
        Me.objbtnSearchCourseTitle.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCourseTitle.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCourseTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCourseTitle.Location = New System.Drawing.Point(292, 34)
        Me.objbtnSearchCourseTitle.Name = "objbtnSearchCourseTitle"
        Me.objbtnSearchCourseTitle.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCourseTitle.TabIndex = 242
        '
        'cboCourseTitle
        '
        Me.cboCourseTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCourseTitle.DropDownWidth = 400
        Me.cboCourseTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCourseTitle.FormattingEnabled = True
        Me.cboCourseTitle.Location = New System.Drawing.Point(85, 34)
        Me.cboCourseTitle.Name = "cboCourseTitle"
        Me.cboCourseTitle.Size = New System.Drawing.Size(201, 21)
        Me.cboCourseTitle.TabIndex = 244
        '
        'lblCourseTitle
        '
        Me.lblCourseTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourseTitle.Location = New System.Drawing.Point(8, 36)
        Me.lblCourseTitle.Name = "lblCourseTitle"
        Me.lblCourseTitle.Size = New System.Drawing.Size(71, 15)
        Me.lblCourseTitle.TabIndex = 243
        Me.lblCourseTitle.Text = "Course Title"
        Me.lblCourseTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(631, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 23)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(607, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 23)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(621, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 215
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(414, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(201, 21)
        Me.cboEmployee.TabIndex = 229
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(343, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(65, 15)
        Me.lblEmployee.TabIndex = 227
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(682, 58)
        Me.eZeeHeader.TabIndex = 118
        Me.eZeeHeader.Title = "Employee Feedback List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 345)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(682, 55)
        Me.objFooter.TabIndex = 117
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(12, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(104, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperation.TabIndex = 75
        Me.btnOperation.Text = "Operation"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPreview, Me.mnuPrint})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(153, 70)
        '
        'mnuPreview
        '
        Me.mnuPreview.Name = "mnuPreview"
        Me.mnuPreview.Size = New System.Drawing.Size(152, 22)
        Me.mnuPreview.Tag = "mnuPreview"
        Me.mnuPreview.Text = "&Preview"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(152, 22)
        Me.mnuPrint.Tag = "mnuPrint"
        Me.mnuPrint.Text = "P&rint"
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(287, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 74
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(481, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 72
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(384, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 71
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(577, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 69
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmEmployeeFeedbackList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(682, 400)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeFeedbackList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Feedback List"
        Me.Panel1.ResumeLayout(False)
        Me.gbEmployeeInfo.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbEmployeeInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchCourseTitle As eZee.Common.eZeeGradientButton
    Friend WithEvents cboCourseTitle As System.Windows.Forms.ComboBox
    Friend WithEvents lblCourseTitle As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lvFeedbackList As eZee.Common.eZeeListView
    Friend WithEvents colhFeedbackDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhECode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhTitle As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
End Class

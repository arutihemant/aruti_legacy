﻿Option Strict On

#Region " Import "

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

#End Region

Public Class frmBindAllocation
    Private ReadOnly mstrModuleName As String = "frmCourseScheduling"

#Region " Private Variables "

    Private mblnCancel As Boolean = True
    Private mintAllocationTypeId As Integer = 0
    Private mdtAllocation As DataTable
    Private menAction As enAction
    Private mstrVoidReason As String = String.Empty
    Private mintTrainingSchedulingUnkid As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByRef mdtTran As DataTable, ByVal eAction As enAction, ByVal intTraininSchedulingId As Integer) As Boolean
        Try
            mintAllocationTypeId = intUnkId
            mdtAllocation = mdtTran
            menAction = eAction
            mintTrainingSchedulingUnkid = intTraininSchedulingId

            Me.ShowDialog()

            mdtTran = mdtAllocation
            intUnkId = mintAllocationTypeId

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objMData.GetEAllocation_Notification("List")
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = enAllocation.BRANCH
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case CInt(cboAllocations.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJGrp As New clsJobGroup
                    dList = objJGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.COST_CENTER
                    Dim objCC As New clscostcenter_master
                    dList = objCC.GetList("List")
                    Call Fill_List(dList.Tables(0), "costcenterunkid", "costcentername")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            lvAllocation.Items.Clear()
            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item(StrDisColName).ToString
                lvItem.Tag = dtRow.Item(StrIdColName)

                If mdtAllocation.AsEnumerable().Where(Function(x) x.Field(Of Integer)("allocationunkid") = CInt(lvItem.Tag)).Count > 0 Then
                    lvItem.Checked = True
                End If

                lvAllocation.Items.Add(lvItem)
            Next
            If lvAllocation.Items.Count > 8 Then
                colhAllocations.Width = 260 - 20
            Else
                colhAllocations.Width = 260
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Do_Operation(ByVal blnOpt As Boolean)
        Try
            For Each LItem As ListViewItem In lvAllocation.Items
                LItem.Checked = blnOpt
                Call AddRemoveAllocation(CInt(LItem.Tag), CStr(IIf(LItem.Checked = True, "A", "D")), LItem)
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
        End Try
    End Sub

    Private Sub AddRemoveAllocation(ByVal intAllocationId As Integer, ByVal strAUDValue As String, ByVal eItem As ListViewItem)
        Try
            Dim dtmp As DataRow() = Nothing
            dtmp = mdtAllocation.Select("allocationunkid = '" & intAllocationId & "' AND AUD <> 'D' ")
            If dtmp.Length > 0 Then
                If CInt(dtmp(0).Item("allocationtranunkid")) > 0 Then
                    If strAUDValue = "D" Then
                        If mstrVoidReason.Trim.Length <= 0 Then
                            Dim objFrmRemark As New frmRemark
                            objFrmRemark.objgbRemarks.Text = Me.Text & " " & Language.getMessage(mstrModuleName, 1, "Remark")
                            If objFrmRemark.displayDialog(mstrVoidReason, enArutiApplicatinType.Aruti_Payroll) = False Then
                                RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
                                eItem.Checked = True
                                AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
                                Exit Sub
                            End If
                        End If
                    End If
                End If
            End If

            Select Case strAUDValue.ToUpper
                Case "A"
                    If dtmp.Length <= 0 Then
                        Dim dRow As DataRow = mdtAllocation.NewRow
                        With dRow
                            .Item("allocationtranunkid") = -1
                            .Item("trainingschedulingunkid") = mintTrainingSchedulingUnkid
                            .Item("allocationunkid") = intAllocationId
                            .Item("isvoid") = False
                            .Item("voiduserunkid") = -1
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voidreason") = ""
                            .Item("iscancel") = False
                            .Item("cancellationuserunkid") = -1
                            .Item("cancellationreason") = ""
                            .Item("cancellationdate") = DBNull.Value
                            .Item("AUD") = "A"
                            .Item("GUID") = Guid.NewGuid.ToString
                        End With
                        mdtAllocation.Rows.Add(dRow)
                    End If
                Case "D"
                    If dtmp.Length > 0 Then
                        If CInt(dtmp(0).Item("allocationtranunkid")) > 0 Then
                            dtmp(0).Item("isvoid") = True
                            dtmp(0).Item("voiduserunkid") = User._Object._Userunkid
                            dtmp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            dtmp(0).Item("voidreason") = ""
                            dtmp(0).Item("AUD") = "D"
                        Else
                            mdtAllocation.Rows.Remove(dtmp(0))
                        End If
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddRemoveAllocation", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBindAllocation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            cboAllocations.SelectedValue = IIf(mintAllocationTypeId <= 0, enAllocation.BRANCH, mintAllocationTypeId)
            If menAction = enAction.EDIT_ONE AndAlso mintAllocationTypeId > 0 Then
                cboAllocations.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBindAllocation_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTraining_Scheduling.SetMessages()
            clsTraining_Trainers_Tran.SetMessages()

            objfrm._Other_ModuleNames = "clsTraining_Scheduling,clsTraining_Trainers_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If mintAllocationTypeId <= 0 Then mintAllocationTypeId = CInt(cboAllocations.SelectedValue)
            mdtAllocation.AcceptChanges()
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            colhAllocations.Text = cboAllocations.Text
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Listview Events "

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            Call AddRemoveAllocation(CInt(e.Item.Tag), CStr(IIf(e.Item.Checked = True, "A", "D")), e.Item)
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Checkbox Events "

    Private Sub objchkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            Call Do_Operation(CBool(objchkAll.CheckState))
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If lvAllocation.Items.Count <= 0 Then Exit Sub
            lvAllocation.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvAllocation.FindItemWithText(txtSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvAllocation.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbAllocationInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAllocationInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnOk.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
            Me.colhAllocations.Text = Language._Object.getCaption(CStr(Me.colhAllocations.Tag), Me.colhAllocations.Text)
            Me.gbAllocationInfo.Text = Language._Object.getCaption(Me.gbAllocationInfo.Name, Me.gbAllocationInfo.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Remark")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
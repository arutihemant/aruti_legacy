﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmEnrollmentAndCancellation

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEnrollmentAndCancellation"
    Private mblnCancel As Boolean = True
    Private objEnrollment As clsTraining_Enrollment_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintTraningEnrollmentTranUnkid As Integer = -1
    Private mblnFormLoad As Boolean = False
    Private intSelectedEmployeeId As Integer = 0
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintTraningEnrollmentTranUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintTraningEnrollmentTranUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboCourse.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            cboStatus.BackColor = GUI.ColorComp
            txtCancellationRemark.BackColor = GUI.ColorComp
            txtDepartment.BackColor = GUI.ColorComp
            txtEligibility.BackColor = GUI.ColorOptional
            'txtEndDateTime.BackColor = GUI.ColorOptional
            txtEnrollRemark.BackColor = GUI.ColorComp
            txtFilledPosition.BackColor = GUI.ColorOptional
            txtJob.BackColor = GUI.ColorComp
            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'txtLastQualification1.BackColor = GUI.ColorComp
            'S.SANDEEP [ 18 FEB 2012 ] -- END
            'txtStartDateTime.BackColor = GUI.ColorOptional
            txtTotalPosition.BackColor = GUI.ColorComp
            'Sandeep [ 23 Oct 2010 ] -- Start
            cboTrainingYears.BackColor = GUI.ColorComp
            'Sandeep [ 23 Oct 2010 ] -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboCourse.SelectedValue = objEnrollment._Trainingschedulingunkid
            cboEmployee.SelectedValue = objEnrollment._Employeeunkid
            cboStatus.SelectedValue = objEnrollment._Status_Id
            txtCancellationRemark.Text = objEnrollment._Cancellationreason
            txtEnrollRemark.Text = objEnrollment._Enroll_Remark
            objEnrollment._Iscancel = objEnrollment._Iscancel
            objEnrollment._Cancellationdate = objEnrollment._Cancellationdate
            objEnrollment._Cancellationreason = objEnrollment._Cancellationreason
            objEnrollment._Cancellationuserunkid = objEnrollment._Cancellationuserunkid
            objEnrollment._Isvoid = objEnrollment._Isvoid
            objEnrollment._Voiddatetime = objEnrollment._Voiddatetime
            objEnrollment._Voidreason = objEnrollment._Voidreason
            objEnrollment._Voiduserunkid = objEnrollment._Voiduserunkid
            'Sandeep [ 23 Oct 2010 ] -- Start
            Dim objScheduling As New clsTraining_Scheduling
            objScheduling._Trainingschedulingunkid = objEnrollment._Trainingschedulingunkid
            cboTrainingYears.SelectedValue = objScheduling._Yearunkid
            objScheduling = Nothing
            'Sandeep [ 23 Oct 2010 ] -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objEnrollment._Trainingschedulingunkid = CInt(cboCourse.SelectedValue)
            objEnrollment._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEnrollment._Status_Id = CInt(cboStatus.SelectedValue)
            objEnrollment._Cancellationreason = txtCancellationRemark.Text
            objEnrollment._Enroll_Remark = txtEnrollRemark.Text
            objEnrollment._Enroll_Date = dtpEnrolldate.Value
            'Sandeep [ 10 FEB 2011 ] -- Start
            objEnrollment._Userunkid = User._Object._Userunkid
            'Sandeep [ 10 FEB 2011 ] -- End 
            If mintTraningEnrollmentTranUnkid = -1 Then
                objEnrollment._Iscancel = False
                objEnrollment._Cancellationdate = Nothing
                objEnrollment._Cancellationreason = ""
                objEnrollment._Cancellationuserunkid = -1
                objEnrollment._Isvoid = False
                objEnrollment._Voiddatetime = Nothing
                objEnrollment._Voidreason = ""
                objEnrollment._Voiduserunkid = -1
            Else
                objEnrollment._Iscancel = objEnrollment._Iscancel
                objEnrollment._Cancellationdate = objEnrollment._Cancellationdate
                objEnrollment._Cancellationreason = objEnrollment._Cancellationreason
                objEnrollment._Cancellationuserunkid = objEnrollment._Cancellationuserunkid
                objEnrollment._Isvoid = objEnrollment._Isvoid
                objEnrollment._Voiddatetime = objEnrollment._Voiddatetime
                objEnrollment._Voidreason = objEnrollment._Voidreason
                objEnrollment._Voiduserunkid = objEnrollment._Voiduserunkid
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        'Sandeep [ 23 Oct 2010 ] -- Start
        'Dim objCourse As New clsTraining_Scheduling
        'Sandeep [ 23 Oct 2010 ] -- End 
        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        Try

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsList = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If menAction = enAction.EDIT_ONE Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With


            'Sandeep [ 23 Oct 2010 ] -- Start
            'dsList = objCourse.getComboList("Title", True)
            'With cboCourse
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsList.Tables("Title")
            '    .SelectedValue = 0
            'End With
            'Sandeep [ 23 Oct 2010 ] -- End 

            dsList = objMaster.GetTraining_Status("Status", True)
            Dim dtTable As DataTable
            If mintTraningEnrollmentTranUnkid = -1 Then
                dtTable = New DataView(dsList.Tables("Status"), "Id < 2", "", DataViewRowState.CurrentRows).ToTable
            Else
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dtTable = dsList.Tables("Status")
                dtTable = New DataView(dsList.Tables("Status"), "Id < 2", "", DataViewRowState.CurrentRows).ToTable
                'S.SANDEEP [ 28 DEC 2012 ] -- END
            End If
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dtTable
                .SelectedValue = 0
            End With

            'Sandeep [ 23 Oct 2010 ] -- Start

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objMaster.getComboListPAYYEAR("Year", True)
            dsList = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboTrainingYears
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Year")
                .SelectedValue = 0
            End With
            'Sandeep [ 23 Oct 2010 ] -- End 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            'Sandeep [ 23 Oct 2010 ] -- Start
            'objCourse = Nothing
            'Sandeep [ 23 Oct 2010 ] -- End 
            objMaster = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If mintTraningEnrollmentTranUnkid = -1 Then
                btnCancel.Visible = False
                btnMakeActive.Visible = False
                tabcRemark.TabPages.Remove(tabpCancellationRemark)
            Else
                If objEnrollment._Iscancel = True Then
                    btnMakeActive.Visible = True
                    btnCancel.Visible = False
                Else
                    btnCancel.Visible = True
                    btnMakeActive.Visible = False
                End If
            End If
            btnCancel.Enabled = User._Object.Privilege._CancelTrainingEnrollment
            objbtnAddTraining.Enabled = User._Object.Privilege._AddCourseScheduling
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            'Sandeep [ 30 September 2010 ] -- Start
            If CInt(cboCourse.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Course is compulsory information. Please select Course to continue."), enMsgBoxStyle.Information)
                cboCourse.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Status is compulsory information. Please select Status to continue"), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False
            End If


            'Sandeep [ 23 Oct 2010 ] -- Start
            'If txtTotalPosition.Text.Trim <= txtFilledPosition.Text.Trim Then
            If CInt(txtTotalPosition.Text.Trim) <= CInt(txtFilledPosition.Text.Trim) Then
                'Sandeep [ 23 Oct 2010 ] -- End 
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Maximum Position limit is reached. Please increase the postion limit in order to enroll new employee."), enMsgBoxStyle.Information)
                txtFilledPosition.Focus()
                Return False
            End If
            'Sandeep [ 30 September 2010 ] -- End
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "
    Private Sub frmEnrollmentAndCancellation_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEnrollment = Nothing
    End Sub

    Private Sub frmEnrollmentAndCancellation_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnEnroll.PerformClick()
        End If
    End Sub

    Private Sub frmEnrollmentAndCancellation_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEnrollmentAndCancellation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEnrollment = New clsTraining_Enrollment_Tran

        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objEnrollment._Trainingenrolltranunkid = mintTraningEnrollmentTranUnkid
                cboCourse.Enabled = False : objbtnAddTraining.Enabled = False
            End If

            Call GetValue()

            Call SetVisibility()

            cboEmployee.Focus()

            mblnFormLoad = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEnrollmentAndCancellation_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTraining_Enrollment_Tran.SetMessages()

            objfrm._Other_ModuleNames = "clsTraining_Enrollment_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEnroll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnroll.Click
        Dim blnFlag As Boolean = False
        Try

            'Sandeep [ 30 September 2010 ] -- Start
            If IsValid() = False Then Exit Sub
            'Sandeep [ 30 September 2010 ] -- End
            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objEnrollment.Update()
            Else
                blnFlag = objEnrollment.Insert()
            End If

            If blnFlag = False And objEnrollment._Message <> "" Then
                eZeeMsgBox.Show(objEnrollment._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objEnrollment = Nothing
                    objEnrollment = New clsTraining_Enrollment_Tran
                    Call GetValue()
                    cboEmployee.Focus()
                Else
                    mintTraningEnrollmentTranUnkid = objEnrollment._Trainingenrolltranunkid
                    Me.Close()
                End If
            End If

            If intSelectedEmployeeId > 0 Then
                cboEmployee.SelectedValue = intSelectedEmployeeId
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEnroll_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If txtCancellationRemark.Text.Trim = "" Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please write cancellation remark in order to cancel the enrollment."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Dim blnFlag As Boolean = False
        Try
            objEnrollment._Iscancel = True
            objEnrollment._Cancellationdate = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
            objEnrollment._Cancellationreason = txtCancellationRemark.Text
            objEnrollment._Cancellationuserunkid = User._Object._Userunkid

            blnFlag = objEnrollment.Update

            If blnFlag Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You have successfully cancelled the enrollment."), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnMakeActive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMakeActive.Click
        Dim blnFlag As Boolean = False
        Try
            objEnrollment._Iscancel = False
            objEnrollment._Cancellationdate = Nothing
            objEnrollment._Cancellationreason = ""
            objEnrollment._Cancellationuserunkid = -1

            blnFlag = objEnrollment.Update

            If blnFlag Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You have successfully Activated the enrollment."), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMakeActive_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddTraining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddTraining.Click
        Dim frm As New frmCourseScheduling
        Dim intRefId As Integer = -1
        Try
            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                Dim dsList As New DataSet
                Dim objCourse As New clsTraining_Scheduling
                'S.SANDEEP [ 18 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsList = objCourse.getComboList("Title", True)
                dsList = objCourse.getComboList("Title", True, , True)
                'S.SANDEEP [ 18 FEB 2012 ] -- END
                With cboCourse
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("Title")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddTraining_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Contols "
    Private Sub cboEmployee_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectionChangeCommitted
        Try
            intSelectedEmployeeId = CInt(cboEmployee.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCourse_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCourse.SelectedIndexChanged
        Try
            If CInt(cboCourse.SelectedValue) > 0 Then
                Dim intTotal As Integer = 0
                Dim ObjTraining As New clsTraining_Scheduling
                ObjTraining._Trainingschedulingunkid = CInt(cboCourse.SelectedValue)
                txtEligibility.Text = ObjTraining._Eligibility_Criteria
                txtTotalPosition.Text = CStr(ObjTraining._Postion_No)
                dtpStartDate.Value = ObjTraining._Start_Date
                dtpEndDate.Value = ObjTraining._End_Date
                objEnrollment.Get_Filled_Position(CInt(cboCourse.SelectedValue), intTotal)
                txtFilledPosition.Text = CStr(intTotal)
                txtTraningFee.Decimal = CDec(ObjTraining._Fees + ObjTraining._Misc + ObjTraining._Travel + ObjTraining._Accomodation + ObjTraining._Allowance + ObjTraining._Meals + ObjTraining._Material + ObjTraining._Exam)
                'Sandeep ( 18 JAN 2011 ) -- START
                dtpEnrolldate.MaxDate = dtpEndDate.Value
                'Sandeep ( 18 JAN 2011 ) -- END 
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCourse_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim ObjEmp As New clsEmployee_Master
                Dim ObjDept As New clsDepartment
                Dim ObjJob As New clsJobs

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'ObjEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
                ObjEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END

                ObjDept._Departmentunkid = ObjEmp._Departmentunkid
                ObjJob._Jobunkid = ObjEmp._Jobunkid

                txtDepartment.Text = ObjDept._Name
                txtJob.Text = ObjJob._Job_Name
                'S.SANDEEP [ 18 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'txtLastQualification1.Text = objEnrollment.Get_Last_Qualification(CInt(cboEmployee.SelectedValue))
                'S.SANDEEP [ 18 FEB 2012 ] -- END
                ObjEmp = Nothing
                ObjDept = Nothing
                ObjJob = Nothing
            Else
                txtDepartment.Text = ""
                txtJob.Text = ""
                'S.SANDEEP [ 18 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'txtLastQualification1.Text = ""
                'S.SANDEEP [ 18 FEB 2012 ] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 23 Oct 2010 ] -- Start
    Private Sub cboTrainingYears_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTrainingYears.SelectedIndexChanged
        Try
            If CInt(cboTrainingYears.SelectedValue) > 0 Then
                Dim objCourse As New clsTraining_Scheduling
                Dim dsList As New DataSet
                'S.SANDEEP [ 18 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsList = objCourse.getComboList("Title", True,CInt(cboTrainingYears.SelectedValue))
                dsList = objCourse.getComboList("Title", True, CInt(cboTrainingYears.SelectedValue), True)
                'S.SANDEEP [ 18 FEB 2012 ] -- END
                With cboCourse
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("Title")
                    If mintTraningEnrollmentTranUnkid = -1 Then
                        .SelectedValue = 0
                    Else
                        .SelectedValue = objEnrollment._Trainingschedulingunkid
                    End If
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrainingYears_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 23 Oct 2010 ] -- End 

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbCourseDetails.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCourseDetails.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


          
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEnroll.GradientBackColor = GUI._ButttonBackColor
            Me.btnEnroll.GradientForeColor = GUI._ButttonFontColor

            Me.btnMakeActive.GradientBackColor = GUI._ButttonBackColor
            Me.btnMakeActive.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnEnroll.Text = Language._Object.getCaption(Me.btnEnroll.Name, Me.btnEnroll.Text)
            Me.gbCourseDetails.Text = Language._Object.getCaption(Me.gbCourseDetails.Name, Me.gbCourseDetails.Text)
            Me.gbEmployeeInformation.Text = Language._Object.getCaption(Me.gbEmployeeInformation.Name, Me.gbEmployeeInformation.Text)
            Me.lblCourse.Text = Language._Object.getCaption(Me.lblCourse.Name, Me.lblCourse.Text)
            Me.lblFilledPositions.Text = Language._Object.getCaption(Me.lblFilledPositions.Name, Me.lblFilledPositions.Text)
            Me.lblNoOfPositons.Text = Language._Object.getCaption(Me.lblNoOfPositons.Name, Me.lblNoOfPositons.Text)
            Me.lblEligibility.Text = Language._Object.getCaption(Me.lblEligibility.Name, Me.lblEligibility.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
            Me.lblEnrollmentDate.Text = Language._Object.getCaption(Me.lblEnrollmentDate.Name, Me.lblEnrollmentDate.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnMakeActive.Text = Language._Object.getCaption(Me.btnMakeActive.Name, Me.btnMakeActive.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblCourseFee.Text = Language._Object.getCaption(Me.lblCourseFee.Name, Me.lblCourseFee.Text)
            Me.tabpEnrollRemark.Text = Language._Object.getCaption(Me.tabpEnrollRemark.Name, Me.tabpEnrollRemark.Text)
            Me.tabpCancellationRemark.Text = Language._Object.getCaption(Me.tabpCancellationRemark.Name, Me.tabpCancellationRemark.Text)
            Me.lblTrainingYear.Text = Language._Object.getCaption(Me.lblTrainingYear.Name, Me.lblTrainingYear.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please write cancellation remark in order to cancel the enrollment.")
            Language.setMessage(mstrModuleName, 2, "You have successfully cancelled the enrollment.")
            Language.setMessage(mstrModuleName, 3, "Course is compulsory information. Please select Course to continue.")
            Language.setMessage(mstrModuleName, 4, "Employee is compulsory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 5, "Status is compulsory information. Please select Status to continue")
            Language.setMessage(mstrModuleName, 6, "Maximum Position limit is reached. Please increase the postion limit in order to enroll new employee.")
            Language.setMessage(mstrModuleName, 7, "You have successfully Activated the enrollment.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
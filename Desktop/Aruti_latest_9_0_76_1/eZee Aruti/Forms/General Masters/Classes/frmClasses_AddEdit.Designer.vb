﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClasses_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClasses_AddEdit))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbClassesInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnAddClassGroup = New eZee.Common.eZeeGradientButton
        Me.Panel1.SuspendLayout()
        Me.gbClassesInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbClassesInfo)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(351, 243)
        Me.Panel1.TabIndex = 10
        '
        'gbClassesInfo
        '
        Me.gbClassesInfo.BorderColor = System.Drawing.Color.Black
        Me.gbClassesInfo.Checked = False
        Me.gbClassesInfo.CollapseAllExceptThis = False
        Me.gbClassesInfo.CollapsedHoverImage = Nothing
        Me.gbClassesInfo.CollapsedNormalImage = Nothing
        Me.gbClassesInfo.CollapsedPressedImage = Nothing
        Me.gbClassesInfo.CollapseOnLoad = False
        Me.gbClassesInfo.Controls.Add(Me.objbtnAddClassGroup)
        Me.gbClassesInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbClassesInfo.Controls.Add(Me.lblClassGroup)
        Me.gbClassesInfo.Controls.Add(Me.cboClassGroup)
        Me.gbClassesInfo.Controls.Add(Me.lblDescription)
        Me.gbClassesInfo.Controls.Add(Me.lblName)
        Me.gbClassesInfo.Controls.Add(Me.txtDescription)
        Me.gbClassesInfo.Controls.Add(Me.txtName)
        Me.gbClassesInfo.Controls.Add(Me.txtCode)
        Me.gbClassesInfo.Controls.Add(Me.lblCode)
        Me.gbClassesInfo.ExpandedHoverImage = Nothing
        Me.gbClassesInfo.ExpandedNormalImage = Nothing
        Me.gbClassesInfo.ExpandedPressedImage = Nothing
        Me.gbClassesInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbClassesInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbClassesInfo.HeaderHeight = 25
        Me.gbClassesInfo.HeightOnCollapse = 0
        Me.gbClassesInfo.LeftTextSpace = 0
        Me.gbClassesInfo.Location = New System.Drawing.Point(12, 12)
        Me.gbClassesInfo.Name = "gbClassesInfo"
        Me.gbClassesInfo.OpenHeight = 300
        Me.gbClassesInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbClassesInfo.ShowBorder = True
        Me.gbClassesInfo.ShowCheckBox = False
        Me.gbClassesInfo.ShowCollapseButton = False
        Me.gbClassesInfo.ShowDefaultBorderColor = True
        Me.gbClassesInfo.ShowDownButton = False
        Me.gbClassesInfo.ShowHeader = True
        Me.gbClassesInfo.Size = New System.Drawing.Size(328, 170)
        Me.gbClassesInfo.TabIndex = 0
        Me.gbClassesInfo.Temp = 0
        Me.gbClassesInfo.Text = "Classes Info"
        Me.gbClassesInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(294, 87)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 6
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(8, 36)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(69, 15)
        Me.lblClassGroup.TabIndex = 0
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(83, 33)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(205, 21)
        Me.cboClassGroup.TabIndex = 1
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(8, 116)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(69, 15)
        Me.lblDescription.TabIndex = 7
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 90)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(69, 15)
        Me.lblName.TabIndex = 4
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(83, 114)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(232, 48)
        Me.txtDescription.TabIndex = 8
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(83, 87)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(205, 21)
        Me.txtName.TabIndex = 5
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(83, 60)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(117, 21)
        Me.txtCode.TabIndex = 3
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 63)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(69, 15)
        Me.lblCode.TabIndex = 2
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 188)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(351, 55)
        Me.objFooter.TabIndex = 10
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(139, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 128
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(242, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnAddClassGroup
        '
        Me.objbtnAddClassGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddClassGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddClassGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddClassGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddClassGroup.BorderSelected = False
        Me.objbtnAddClassGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddClassGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddClassGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddClassGroup.Location = New System.Drawing.Point(294, 33)
        Me.objbtnAddClassGroup.Name = "objbtnAddClassGroup"
        Me.objbtnAddClassGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddClassGroup.TabIndex = 214
        '
        'frmClasses_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(351, 243)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmClasses_AddEdit"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Classes"
        Me.Panel1.ResumeLayout(False)
        Me.gbClassesInfo.ResumeLayout(False)
        Me.gbClassesInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbClassesInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddClassGroup As eZee.Common.eZeeGradientButton
End Class

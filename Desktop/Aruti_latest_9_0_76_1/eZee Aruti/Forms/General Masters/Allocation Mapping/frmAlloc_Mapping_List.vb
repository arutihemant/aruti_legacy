﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAlloc_Mapping_List

#Region " Private Varaibles "

    Private ReadOnly mstrModuleName As String = "frmAlloc_Mapping_List"
    Private objAlloc As clsAllocationMapping

#End Region

#Region " Private Function "

    Private Sub Fill_List()
        Dim dList As New DataSet
        Dim lvItem As ListViewItem
        'S.SANDEEP [ 05 MAR 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim dTable As DataTable
        Dim StrSearch As String = String.Empty
        'S.SANDEEP [ 05 MAR 2013 ] -- END
        Try

            RemoveHandler lvData.ItemChecked, AddressOf lvData_ItemChecked
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

            dList = objAlloc.Get_Mapped_List("List")
            'S.SANDEEP [ 05 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If CInt(cboAllocationData1.SelectedValue) > 0 Then
                StrSearch &= "AND p_referenceunkid = '" & CInt(cboAllocationData1.SelectedValue) & "' "
            End If

            If CInt(cboAllocationData2.SelectedValue) > 0 Then
                StrSearch &= "AND c_referenceunkid = '" & CInt(cboAllocationData2.SelectedValue) & "' "
            End If

            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
                dTable = New DataView(dList.Tables(0), StrSearch, "p_allocationid,mappingunkid", DataViewRowState.CurrentRows).ToTable
            Else
                dTable = New DataView(dList.Tables(0), "", "p_allocationid,mappingunkid", DataViewRowState.CurrentRows).ToTable
            End If
            'S.SANDEEP [ 05 MAR 2013 ] -- END
            lvData.Items.Clear()
            For Each dtRow As DataRow In dTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow("DisplayAllocation").ToString)
                lvItem.SubItems.Add(dtRow("p_allocationid").ToString)
                lvItem.SubItems.Add(dtRow("c_allocationid").ToString)
                lvItem.SubItems.Add(dtRow("mappingtranunkid").ToString)
                lvItem.SubItems.Add(dtRow("mappingunkid").ToString)
                lvItem.SubItems.Add(dtRow("Allocation").ToString)

                lvData.Items.Add(lvItem)
            Next
            lvData.GroupingColumn = objcolhPAllocation
            lvData.DisplayGroups(True)

            If lvData.Items.Count > 8 Then
                colhAllocationDisplay.Width = 635 - 20
            Else
                colhAllocationDisplay.Width = 635
            End If

            AddHandler lvData.ItemChecked, AddressOf lvData_ItemChecked
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub


    'Anjan (25 Oct 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew's Request
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToSetAllocationMapping
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteAllocationMapping
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Anjan (25 Oct 2012)-End

    'S.SANDEEP [ 05 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCMaster As New clsMasterData
        Try
            dsCombo = objCMaster.GetEAllocation_Notification("List", CStr(IIf(ConfigParameter._Object._IsAllocation_Hierarchy_Set = False, "", ConfigParameter._Object._Allocation_Hierarchy)))

            With cboAllocation2
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List").Copy
                .SelectedValue = 1
            End With
            Dim sLen() As String = ConfigParameter._Object._Allocation_Hierarchy.Split(CChar("|"))
            Dim dTable = New DataView(dsCombo.Tables("List").Copy, "Id < '" & CStr(sLen.GetValue(sLen.Length - 1)) & "'", "Id", DataViewRowState.CurrentRows).ToTable

            With cboAllocation1
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dTable
                .SelectedValue = 1
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Allocation_Data(ByVal intAllocationId As Integer, ByVal cboData As ComboBox)
        Dim dsList As New DataSet
        Dim m_ValueMember, m_DisplayMember As String
        m_ValueMember = "" : m_DisplayMember = ""
        Try
            Select Case intAllocationId
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dsList = objBranch.getComboList("List", True)
                    m_ValueMember = "stationunkid" : m_DisplayMember = "name"

                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.getComboList("List", True)
                    m_ValueMember = "deptgroupunkid" : m_DisplayMember = "name"

                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dsList = objDept.getComboList("List", True)
                    m_ValueMember = "departmentunkid" : m_DisplayMember = "name"

                Case enAllocation.SECTION_GROUP
                    Dim objSecGroup As New clsSectionGroup
                    dsList = objSecGroup.getComboList("List", True)
                    m_ValueMember = "sectiongroupunkid" : m_DisplayMember = "name"

                Case enAllocation.SECTION
                    Dim objSection As New clsSections
                    dsList = objSection.getComboList("List", True)
                    m_ValueMember = "sectionunkid" : m_DisplayMember = "name"

                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dsList = objUnitGrp.getComboList("List", True)
                    m_ValueMember = "unitgroupunkid" : m_DisplayMember = "name"

                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dsList = objUnit.getComboList("List", True)
                    m_ValueMember = "unitunkid" : m_DisplayMember = "name"

                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dsList = objTeam.getComboList("List", True)
                    m_ValueMember = "teamunkid" : m_DisplayMember = "name"

            End Select

            cboData.DataSource = Nothing

            With cboData
                .ValueMember = m_ValueMember
                .DisplayMember = m_DisplayMember
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Allocation_Data", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 05 MAR 2013 ] -- END


#End Region

#Region " Form's Events "

    Private Sub frmAlloc_Mapping_List_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objAlloc = New clsAllocationMapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            Call OtherSettings()

            Call SetVisibility()

            'S.SANDEEP [ 05 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call FillCombo()
            'S.SANDEEP [ 05 MAR 2013 ] -- END

            Call Fill_List()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmAlloc_Mapping_List_Load", mstrModuleName)
        End Try
    End Sub
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMasterData.SetMessages()
            objfrm._Other_ModuleNames = "clsMasterData"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmAllocation_Mapping
        Try
            frm.displayDialog(-1, enAction.ADD_ONE)
            Call Fill_List()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvData.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one item in order to delete."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            If lvData.CheckedItems.Count > 0 Then

                'S.SANDEEP [ 03 OCT 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Dim blnFlag As Boolean = False
                Dim blnIsUsed As Boolean = False
                'S.SANDEEP [ 03 OCT 2012 ] -- END

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.OTHERS, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    For Each lvItem As ListViewItem In lvData.CheckedItems

                        'S.SANDEEP [ 03 OCT 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        blnIsUsed = False
                        If objAlloc.isUsed(CInt(lvItem.SubItems(objcolhMappingId.Index).Text), CInt(lvItem.SubItems(objcolhMappingTranId.Index).Text)) = True Then
                            If blnFlag = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot void some of the mapped allocation(s). Reason : Mapped allocation(s) are used in employee(s)."), enMsgBoxStyle.Information)
                                blnFlag = True
                            End If
                            blnIsUsed = True
                        End If
                        'S.SANDEEP [ 03 OCT 2012 ] -- END

                        If blnIsUsed = False Then
                        If objAlloc.Void_Mapped_Allocation(CInt(lvItem.SubItems(objcolhMappingTranId.Index).Text), _
                                                       CInt(lvItem.SubItems(objcolhMappingId.Index).Text), _
                                                       ConfigParameter._Object._CurrentDateAndTime, _
                                                       mstrVoidReason, _
                                                       User._Object._Userunkid) = True Then
                            lvItem.Remove()
                        End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 05 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchData1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchData1.Click, objbtnSearchData2.Click
        Dim frm As New frmCommonSearch
        Try
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case "OBJBTNSEARCHDATA1"
                    With frm
                        .ValueMember = cboAllocationData1.ValueMember
                        .DisplayMember = cboAllocationData1.DisplayMember
                        .DataSource = CType(cboAllocationData1.DataSource, DataTable)
                    End With
                    If frm.DisplayDialog() = True Then
                        cboAllocationData1.SelectedValue = frm.SelectedValue
                        cboAllocationData1.Focus()
                    End If
                Case "OBJBTNSEARCHDATA2"
                    With frm
                        .ValueMember = cboAllocationData2.ValueMember
                        .DisplayMember = cboAllocationData2.DisplayMember
                        .DataSource = CType(cboAllocationData2.DataSource, DataTable)
                    End With
                    If frm.DisplayDialog() = True Then
                        cboAllocationData2.SelectedValue = frm.SelectedValue
                        cboAllocationData2.Focus()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchData1_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboAllocationData1.SelectedValue = 0
            cboAllocationData2.SelectedValue = 0
            cboAllocation1.SelectedValue = 1
            lvData.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 05 MAR 2013 ] -- END

#End Region

#Region " Controls Events "

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            RemoveHandler lvData.ItemChecked, AddressOf lvData_ItemChecked
            For Each lvItem As ListViewItem In lvData.Items
                lvItem.Checked = CBool(objChkAll.CheckState)
            Next
            AddHandler lvData.ItemChecked, AddressOf lvData_ItemChecked
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvData_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvData.ItemChecked
        Try
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            If lvData.CheckedItems.Count <= 0 Then
                objChkAll.CheckState = CheckState.Unchecked
            ElseIf lvData.CheckedItems.Count < lvData.Items.Count Then
                objChkAll.CheckState = CheckState.Indeterminate
            ElseIf lvData.CheckedItems.Count = lvData.Items.Count Then
                objChkAll.CheckState = CheckState.Checked
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "lvData_ItemChecked", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 05 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboAllocation1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocation1.SelectedIndexChanged
		Try
            If ConfigParameter._Object._Allocation_Hierarchy.Trim.Length > 0 Then
                For Each Id As String In ConfigParameter._Object._Allocation_Hierarchy.Split(CChar("|"))
                    If CInt(Id) > CInt(cboAllocation1.SelectedValue) Then
                        cboAllocation2.SelectedValue = CInt(Id)
                        cboAllocation2.Enabled = False
                        Exit For
                    End If
                Next
            End If
            objlblAlloc1.Text = cboAllocation1.Text
            objlblAlloc2.Text = cboAllocation2.Text
			
            Call Fill_Allocation_Data(CInt(cboAllocation1.SelectedValue), cboAllocationData1)
            Call Fill_Allocation_Data(CInt(cboAllocation2.SelectedValue), cboAllocationData2)
			
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboAllocation1_SelectedIndexChanged", mstrModuleName)
		End Try
	End Sub
    'S.SANDEEP [ 05 MAR 2013 ] -- END
			
#End Region
			
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.colhAllocationDisplay.Text = Language._Object.getCaption(CStr(Me.colhAllocationDisplay.Tag), Me.colhAllocationDisplay.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblAllocation1.Text = Language._Object.getCaption(Me.lblAllocation1.Name, Me.lblAllocation1.Text)
			Me.lblAllocation2.Text = Language._Object.getCaption(Me.lblAllocation2.Name, Me.lblAllocation2.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please check atleast one item in order to delete.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot void some of the mapped allocation(s). Reason : Mapped allocation(s) are used in employee(s).")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
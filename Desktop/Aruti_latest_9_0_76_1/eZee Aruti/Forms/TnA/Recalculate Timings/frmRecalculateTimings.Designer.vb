﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRecalculateTimings
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRecalculateTimings))
        Me.gbFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlMinutes = New System.Windows.Forms.Panel
        Me.lblMinutes = New System.Windows.Forms.Label
        Me.nudLimit = New System.Windows.Forms.NumericUpDown
        Me.nudMinutes = New System.Windows.Forms.NumericUpDown
        Me.lblRoundLimit = New System.Windows.Forms.Label
        Me.pnlChkBox = New System.Windows.Forms.Panel
        Me.chkInTime = New System.Windows.Forms.CheckBox
        Me.chkOutTime = New System.Windows.Forms.CheckBox
        Me.radManualRoundOff = New System.Windows.Forms.RadioButton
        Me.radGraceRoundOff = New System.Windows.Forms.RadioButton
        Me.objLine = New eZee.Common.eZeeLine
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblFdate = New System.Windows.Forms.Label
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilter.SuspendLayout()
        Me.pnlMinutes.SuspendLayout()
        CType(Me.nudLimit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMinutes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlChkBox.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilter
        '
        Me.gbFilter.BorderColor = System.Drawing.Color.Black
        Me.gbFilter.Checked = False
        Me.gbFilter.CollapseAllExceptThis = False
        Me.gbFilter.CollapsedHoverImage = Nothing
        Me.gbFilter.CollapsedNormalImage = Nothing
        Me.gbFilter.CollapsedPressedImage = Nothing
        Me.gbFilter.CollapseOnLoad = False
        Me.gbFilter.Controls.Add(Me.pnlMinutes)
        Me.gbFilter.Controls.Add(Me.pnlChkBox)
        Me.gbFilter.Controls.Add(Me.radManualRoundOff)
        Me.gbFilter.Controls.Add(Me.radGraceRoundOff)
        Me.gbFilter.Controls.Add(Me.objLine)
        Me.gbFilter.Controls.Add(Me.lnkAllocation)
        Me.gbFilter.Controls.Add(Me.objbtnReset)
        Me.gbFilter.Controls.Add(Me.objbtnSearch)
        Me.gbFilter.Controls.Add(Me.dtpToDate)
        Me.gbFilter.Controls.Add(Me.lblToDate)
        Me.gbFilter.Controls.Add(Me.dtpFromDate)
        Me.gbFilter.Controls.Add(Me.lblFdate)
        Me.gbFilter.ExpandedHoverImage = Nothing
        Me.gbFilter.ExpandedNormalImage = Nothing
        Me.gbFilter.ExpandedPressedImage = Nothing
        Me.gbFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilter.HeaderHeight = 25
        Me.gbFilter.HeaderMessage = ""
        Me.gbFilter.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilter.HeightOnCollapse = 0
        Me.gbFilter.LeftTextSpace = 0
        Me.gbFilter.Location = New System.Drawing.Point(0, 0)
        Me.gbFilter.Name = "gbFilter"
        Me.gbFilter.OpenHeight = 300
        Me.gbFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilter.ShowBorder = True
        Me.gbFilter.ShowCheckBox = False
        Me.gbFilter.ShowCollapseButton = False
        Me.gbFilter.ShowDefaultBorderColor = True
        Me.gbFilter.ShowDownButton = False
        Me.gbFilter.ShowHeader = True
        Me.gbFilter.Size = New System.Drawing.Size(404, 128)
        Me.gbFilter.TabIndex = 0
        Me.gbFilter.Temp = 0
        Me.gbFilter.Text = "Filter Criteria"
        Me.gbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMinutes
        '
        Me.pnlMinutes.Controls.Add(Me.lblMinutes)
        Me.pnlMinutes.Controls.Add(Me.nudLimit)
        Me.pnlMinutes.Controls.Add(Me.nudMinutes)
        Me.pnlMinutes.Controls.Add(Me.lblRoundLimit)
        Me.pnlMinutes.Location = New System.Drawing.Point(135, 98)
        Me.pnlMinutes.Name = "pnlMinutes"
        Me.pnlMinutes.Size = New System.Drawing.Size(258, 23)
        Me.pnlMinutes.TabIndex = 157
        '
        'lblMinutes
        '
        Me.lblMinutes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinutes.Location = New System.Drawing.Point(3, 4)
        Me.lblMinutes.Name = "lblMinutes"
        Me.lblMinutes.Size = New System.Drawing.Size(48, 15)
        Me.lblMinutes.TabIndex = 153
        Me.lblMinutes.Text = "Minutes"
        '
        'nudLimit
        '
        Me.nudLimit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudLimit.Location = New System.Drawing.Point(209, 1)
        Me.nudLimit.Maximum = New Decimal(New Integer() {59, 0, 0, 0})
        Me.nudLimit.Name = "nudLimit"
        Me.nudLimit.Size = New System.Drawing.Size(46, 21)
        Me.nudLimit.TabIndex = 154
        Me.nudLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'nudMinutes
        '
        Me.nudMinutes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMinutes.Location = New System.Drawing.Point(57, 1)
        Me.nudMinutes.Maximum = New Decimal(New Integer() {59, 0, 0, 0})
        Me.nudMinutes.Name = "nudMinutes"
        Me.nudMinutes.Size = New System.Drawing.Size(49, 21)
        Me.nudMinutes.TabIndex = 152
        Me.nudMinutes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblRoundLimit
        '
        Me.lblRoundLimit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRoundLimit.Location = New System.Drawing.Point(112, 4)
        Me.lblRoundLimit.Name = "lblRoundLimit"
        Me.lblRoundLimit.Size = New System.Drawing.Size(91, 15)
        Me.lblRoundLimit.TabIndex = 155
        Me.lblRoundLimit.Text = "Round On (Mins.)"
        '
        'pnlChkBox
        '
        Me.pnlChkBox.Controls.Add(Me.chkInTime)
        Me.pnlChkBox.Controls.Add(Me.chkOutTime)
        Me.pnlChkBox.Location = New System.Drawing.Point(135, 75)
        Me.pnlChkBox.Name = "pnlChkBox"
        Me.pnlChkBox.Size = New System.Drawing.Size(162, 23)
        Me.pnlChkBox.TabIndex = 151
        '
        'chkInTime
        '
        Me.chkInTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInTime.Location = New System.Drawing.Point(2, 3)
        Me.chkInTime.Name = "chkInTime"
        Me.chkInTime.Size = New System.Drawing.Size(71, 17)
        Me.chkInTime.TabIndex = 148
        Me.chkInTime.Text = "In Time"
        Me.chkInTime.UseVisualStyleBackColor = True
        '
        'chkOutTime
        '
        Me.chkOutTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOutTime.Location = New System.Drawing.Point(79, 3)
        Me.chkOutTime.Name = "chkOutTime"
        Me.chkOutTime.Size = New System.Drawing.Size(71, 17)
        Me.chkOutTime.TabIndex = 149
        Me.chkOutTime.Text = "Out Time"
        Me.chkOutTime.UseVisualStyleBackColor = True
        '
        'radManualRoundOff
        '
        Me.radManualRoundOff.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radManualRoundOff.Location = New System.Drawing.Point(15, 101)
        Me.radManualRoundOff.Name = "radManualRoundOff"
        Me.radManualRoundOff.Size = New System.Drawing.Size(114, 17)
        Me.radManualRoundOff.TabIndex = 147
        Me.radManualRoundOff.TabStop = True
        Me.radManualRoundOff.Text = "Manual Round Off"
        Me.radManualRoundOff.UseVisualStyleBackColor = True
        '
        'radGraceRoundOff
        '
        Me.radGraceRoundOff.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radGraceRoundOff.Location = New System.Drawing.Point(15, 78)
        Me.radGraceRoundOff.Name = "radGraceRoundOff"
        Me.radGraceRoundOff.Size = New System.Drawing.Size(114, 17)
        Me.radGraceRoundOff.TabIndex = 146
        Me.radGraceRoundOff.TabStop = True
        Me.radGraceRoundOff.Text = "Grace Round Off"
        Me.radGraceRoundOff.UseVisualStyleBackColor = True
        '
        'objLine
        '
        Me.objLine.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine.Location = New System.Drawing.Point(12, 66)
        Me.objLine.Name = "objLine"
        Me.objLine.Size = New System.Drawing.Size(381, 4)
        Me.objLine.TabIndex = 110
        Me.objLine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(255, 4)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(92, 17)
        Me.lnkAllocation.TabIndex = 144
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(379, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 143
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(353, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 142
        Me.objbtnSearch.TabStop = False
        '
        'dtpToDate
        '
        Me.dtpToDate.Checked = False
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(282, 34)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(110, 21)
        Me.dtpToDate.TabIndex = 33
        '
        'lblToDate
        '
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(214, 36)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(62, 17)
        Me.lblToDate.TabIndex = 34
        Me.lblToDate.Text = "To Date"
        Me.lblToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Checked = False
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(87, 34)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(110, 21)
        Me.dtpFromDate.TabIndex = 31
        '
        'lblFdate
        '
        Me.lblFdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFdate.Location = New System.Drawing.Point(8, 36)
        Me.lblFdate.Name = "lblFdate"
        Me.lblFdate.Size = New System.Drawing.Size(73, 17)
        Me.lblFdate.TabIndex = 32
        Me.lblFdate.Text = "From Date"
        Me.lblFdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearch, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.Panel1, 0, 1)
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(0, 129)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(403, 277)
        Me.tblpAssessorEmployee.TabIndex = 108
        '
        'txtSearch
        '
        Me.txtSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearch.Flags = 0
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch.Location = New System.Drawing.Point(3, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(397, 21)
        Me.txtSearch.TabIndex = 106
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkEmployee)
        Me.Panel1.Controls.Add(Me.dgvData)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 29)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(397, 245)
        Me.Panel1.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvData.ColumnHeadersHeight = 21
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(397, 245)
        Me.dgvData.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 407)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(405, 55)
        Me.objFooter.TabIndex = 109
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(193, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(296, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'frmRecalculateTimings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(405, 462)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.tblpAssessorEmployee)
        Me.Controls.Add(Me.gbFilter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRecalculateTimings"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Recalculate Timings"
        Me.gbFilter.ResumeLayout(False)
        Me.pnlMinutes.ResumeLayout(False)
        CType(Me.nudLimit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMinutes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlChkBox.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFdate As System.Windows.Forms.Label
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objLine As eZee.Common.eZeeLine
    Friend WithEvents radManualRoundOff As System.Windows.Forms.RadioButton
    Friend WithEvents radGraceRoundOff As System.Windows.Forms.RadioButton
    Friend WithEvents chkOutTime As System.Windows.Forms.CheckBox
    Friend WithEvents chkInTime As System.Windows.Forms.CheckBox
    Friend WithEvents pnlChkBox As System.Windows.Forms.Panel
    Friend WithEvents nudLimit As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblRoundLimit As System.Windows.Forms.Label
    Friend WithEvents nudMinutes As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMinutes As System.Windows.Forms.Label
    Friend WithEvents pnlMinutes As System.Windows.Forms.Panel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

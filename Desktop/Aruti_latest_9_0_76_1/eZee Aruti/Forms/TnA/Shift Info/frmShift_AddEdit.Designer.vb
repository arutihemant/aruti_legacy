﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmShift_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmShift_AddEdit))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbShiftSetting = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtToHalfhour = New eZee.TextBox.NumericTextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtFromHalfhour = New eZee.TextBox.NumericTextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtShortTimeMin = New eZee.TextBox.NumericTextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblShorttime = New System.Windows.Forms.Label
        Me.txtOverTimeMin = New eZee.TextBox.NumericTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblOvertime = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddShiftType = New eZee.Common.eZeeGradientButton
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.cboShiftType = New System.Windows.Forms.ComboBox
        Me.lblShiftType = New System.Windows.Forms.Label
        Me.chkParttimeshift = New System.Windows.Forms.CheckBox
        Me.txtBreakTime = New eZee.TextBox.NumericTextBox
        Me.lblMinute = New System.Windows.Forms.Label
        Me.lblcode = New System.Windows.Forms.Label
        Me.dtpEndTime = New System.Windows.Forms.DateTimePicker
        Me.lblName = New System.Windows.Forms.Label
        Me.dtpStartTime = New System.Windows.Forms.DateTimePicker
        Me.lblStartTime = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblEndTime = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lvDay = New System.Windows.Forms.ListView
        Me.lblWorkingHours = New System.Windows.Forms.Label
        Me.lblDay = New System.Windows.Forms.Label
        Me.lblBreakTime = New System.Windows.Forms.Label
        Me.txtHours = New eZee.TextBox.AlphanumericTextBox
        Me.pnlMain.SuspendLayout()
        Me.gbShiftSetting.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.Transparent
        Me.pnlMain.Controls.Add(Me.gbShiftSetting)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.gbInfo)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(417, 510)
        Me.pnlMain.TabIndex = 1
        '
        'gbShiftSetting
        '
        Me.gbShiftSetting.BorderColor = System.Drawing.Color.Black
        Me.gbShiftSetting.Checked = False
        Me.gbShiftSetting.CollapseAllExceptThis = False
        Me.gbShiftSetting.CollapsedHoverImage = Nothing
        Me.gbShiftSetting.CollapsedNormalImage = Nothing
        Me.gbShiftSetting.CollapsedPressedImage = Nothing
        Me.gbShiftSetting.CollapseOnLoad = False
        Me.gbShiftSetting.Controls.Add(Me.txtToHalfhour)
        Me.gbShiftSetting.Controls.Add(Me.Label4)
        Me.gbShiftSetting.Controls.Add(Me.txtFromHalfhour)
        Me.gbShiftSetting.Controls.Add(Me.Label3)
        Me.gbShiftSetting.Controls.Add(Me.txtShortTimeMin)
        Me.gbShiftSetting.Controls.Add(Me.Label2)
        Me.gbShiftSetting.Controls.Add(Me.lblShorttime)
        Me.gbShiftSetting.Controls.Add(Me.txtOverTimeMin)
        Me.gbShiftSetting.Controls.Add(Me.Label1)
        Me.gbShiftSetting.Controls.Add(Me.lblOvertime)
        Me.gbShiftSetting.ExpandedHoverImage = Nothing
        Me.gbShiftSetting.ExpandedNormalImage = Nothing
        Me.gbShiftSetting.ExpandedPressedImage = Nothing
        Me.gbShiftSetting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbShiftSetting.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbShiftSetting.HeaderHeight = 25
        Me.gbShiftSetting.HeaderMessage = ""
        Me.gbShiftSetting.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbShiftSetting.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbShiftSetting.HeightOnCollapse = 0
        Me.gbShiftSetting.LeftTextSpace = 0
        Me.gbShiftSetting.Location = New System.Drawing.Point(9, 331)
        Me.gbShiftSetting.Name = "gbShiftSetting"
        Me.gbShiftSetting.OpenHeight = 300
        Me.gbShiftSetting.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbShiftSetting.ShowBorder = True
        Me.gbShiftSetting.ShowCheckBox = False
        Me.gbShiftSetting.ShowCollapseButton = False
        Me.gbShiftSetting.ShowDefaultBorderColor = True
        Me.gbShiftSetting.ShowDownButton = False
        Me.gbShiftSetting.ShowHeader = True
        Me.gbShiftSetting.Size = New System.Drawing.Size(399, 117)
        Me.gbShiftSetting.TabIndex = 102
        Me.gbShiftSetting.Temp = 0
        Me.gbShiftSetting.Text = "Shift Settings"
        Me.gbShiftSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtToHalfhour
        '
        Me.txtToHalfhour.AllowNegative = False
        Me.txtToHalfhour.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtToHalfhour.DigitsInGroup = 0
        Me.txtToHalfhour.Flags = 65536
        Me.txtToHalfhour.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.txtToHalfhour.Location = New System.Drawing.Point(257, 86)
        Me.txtToHalfhour.MaxDecimalPlaces = 2
        Me.txtToHalfhour.MaxWholeDigits = 2
        Me.txtToHalfhour.Name = "txtToHalfhour"
        Me.txtToHalfhour.Prefix = ""
        Me.txtToHalfhour.RangeMax = 1.7976931348623157E+308
        Me.txtToHalfhour.RangeMin = -1.7976931348623157E+308
        Me.txtToHalfhour.Size = New System.Drawing.Size(57, 21)
        Me.txtToHalfhour.TabIndex = 22
        Me.txtToHalfhour.Text = "0"
        Me.txtToHalfhour.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label4.Location = New System.Drawing.Point(210, 90)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 13)
        Me.Label4.TabIndex = 46
        Me.Label4.Text = "And"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFromHalfhour
        '
        Me.txtFromHalfhour.AllowNegative = False
        Me.txtFromHalfhour.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtFromHalfhour.DigitsInGroup = 0
        Me.txtFromHalfhour.Flags = 65536
        Me.txtFromHalfhour.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.txtFromHalfhour.Location = New System.Drawing.Point(149, 86)
        Me.txtFromHalfhour.MaxDecimalPlaces = 2
        Me.txtFromHalfhour.MaxWholeDigits = 2
        Me.txtFromHalfhour.Name = "txtFromHalfhour"
        Me.txtFromHalfhour.Prefix = ""
        Me.txtFromHalfhour.RangeMax = 1.7976931348623157E+308
        Me.txtFromHalfhour.RangeMin = -1.7976931348623157E+308
        Me.txtFromHalfhour.Size = New System.Drawing.Size(57, 21)
        Me.txtFromHalfhour.TabIndex = 21
        Me.txtFromHalfhour.Text = "0"
        Me.txtFromHalfhour.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(135, 15)
        Me.Label3.TabIndex = 44
        Me.Label3.Text = "Mark As Half Day If Hours between "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShortTimeMin
        '
        Me.txtShortTimeMin.AllowNegative = False
        Me.txtShortTimeMin.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtShortTimeMin.DigitsInGroup = 0
        Me.txtShortTimeMin.Flags = 65536
        Me.txtShortTimeMin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.txtShortTimeMin.Location = New System.Drawing.Point(149, 59)
        Me.txtShortTimeMin.MaxDecimalPlaces = 0
        Me.txtShortTimeMin.MaxWholeDigits = 21
        Me.txtShortTimeMin.Name = "txtShortTimeMin"
        Me.txtShortTimeMin.Prefix = ""
        Me.txtShortTimeMin.RangeMax = 1.7976931348623157E+308
        Me.txtShortTimeMin.RangeMin = -1.7976931348623157E+308
        Me.txtShortTimeMin.Size = New System.Drawing.Size(57, 21)
        Me.txtShortTimeMin.TabIndex = 20
        Me.txtShortTimeMin.Text = "0"
        Me.txtShortTimeMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label2.Location = New System.Drawing.Point(210, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 13)
        Me.Label2.TabIndex = 43
        Me.Label2.Text = "(In Minute)"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblShorttime
        '
        Me.lblShorttime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblShorttime.Location = New System.Drawing.Point(8, 62)
        Me.lblShorttime.Name = "lblShorttime"
        Me.lblShorttime.Size = New System.Drawing.Size(135, 15)
        Me.lblShorttime.TabIndex = 42
        Me.lblShorttime.Text = "Count ShortTime Before"
        Me.lblShorttime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOverTimeMin
        '
        Me.txtOverTimeMin.AllowNegative = False
        Me.txtOverTimeMin.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOverTimeMin.DigitsInGroup = 0
        Me.txtOverTimeMin.Flags = 65536
        Me.txtOverTimeMin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.txtOverTimeMin.Location = New System.Drawing.Point(149, 32)
        Me.txtOverTimeMin.MaxDecimalPlaces = 0
        Me.txtOverTimeMin.MaxWholeDigits = 21
        Me.txtOverTimeMin.Name = "txtOverTimeMin"
        Me.txtOverTimeMin.Prefix = ""
        Me.txtOverTimeMin.RangeMax = 1.7976931348623157E+308
        Me.txtOverTimeMin.RangeMin = -1.7976931348623157E+308
        Me.txtOverTimeMin.Size = New System.Drawing.Size(57, 21)
        Me.txtOverTimeMin.TabIndex = 19
        Me.txtOverTimeMin.Text = "0"
        Me.txtOverTimeMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.Label1.Location = New System.Drawing.Point(210, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "(In Minute)"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOvertime
        '
        Me.lblOvertime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblOvertime.Location = New System.Drawing.Point(8, 36)
        Me.lblOvertime.Name = "lblOvertime"
        Me.lblOvertime.Size = New System.Drawing.Size(135, 15)
        Me.lblOvertime.TabIndex = 39
        Me.lblOvertime.Text = "Count OverTime After"
        Me.lblOvertime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objFooter.Location = New System.Drawing.Point(0, 455)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(417, 55)
        Me.objFooter.TabIndex = 103
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(222, 12)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 23
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(318, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 24
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbInfo
        '
        Me.gbInfo.BackColor = System.Drawing.Color.Transparent
        Me.gbInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInfo.Checked = False
        Me.gbInfo.CollapseAllExceptThis = False
        Me.gbInfo.CollapsedHoverImage = Nothing
        Me.gbInfo.CollapsedNormalImage = Nothing
        Me.gbInfo.CollapsedPressedImage = Nothing
        Me.gbInfo.CollapseOnLoad = False
        Me.gbInfo.Controls.Add(Me.objbtnAddShiftType)
        Me.gbInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbInfo.Controls.Add(Me.cboShiftType)
        Me.gbInfo.Controls.Add(Me.lblShiftType)
        Me.gbInfo.Controls.Add(Me.chkParttimeshift)
        Me.gbInfo.Controls.Add(Me.txtBreakTime)
        Me.gbInfo.Controls.Add(Me.lblMinute)
        Me.gbInfo.Controls.Add(Me.lblcode)
        Me.gbInfo.Controls.Add(Me.dtpEndTime)
        Me.gbInfo.Controls.Add(Me.lblName)
        Me.gbInfo.Controls.Add(Me.dtpStartTime)
        Me.gbInfo.Controls.Add(Me.lblStartTime)
        Me.gbInfo.Controls.Add(Me.txtName)
        Me.gbInfo.Controls.Add(Me.lblEndTime)
        Me.gbInfo.Controls.Add(Me.txtCode)
        Me.gbInfo.Controls.Add(Me.lvDay)
        Me.gbInfo.Controls.Add(Me.lblWorkingHours)
        Me.gbInfo.Controls.Add(Me.lblDay)
        Me.gbInfo.Controls.Add(Me.lblBreakTime)
        Me.gbInfo.Controls.Add(Me.txtHours)
        Me.gbInfo.ExpandedHoverImage = Nothing
        Me.gbInfo.ExpandedNormalImage = Nothing
        Me.gbInfo.ExpandedPressedImage = Nothing
        Me.gbInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInfo.HeaderHeight = 25
        Me.gbInfo.HeaderMessage = ""
        Me.gbInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInfo.HeightOnCollapse = 0
        Me.gbInfo.LeftTextSpace = 0
        Me.gbInfo.Location = New System.Drawing.Point(9, 9)
        Me.gbInfo.Name = "gbInfo"
        Me.gbInfo.OpenHeight = 300
        Me.gbInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.gbInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInfo.ShowBorder = True
        Me.gbInfo.ShowCheckBox = False
        Me.gbInfo.ShowCollapseButton = False
        Me.gbInfo.ShowDefaultBorderColor = True
        Me.gbInfo.ShowDownButton = False
        Me.gbInfo.ShowHeader = True
        Me.gbInfo.Size = New System.Drawing.Size(399, 316)
        Me.gbInfo.TabIndex = 101
        Me.gbInfo.Temp = 0
        Me.gbInfo.Text = "Shift Information"
        Me.gbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddShiftType
        '
        Me.objbtnAddShiftType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddShiftType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddShiftType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddShiftType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddShiftType.BorderSelected = False
        Me.objbtnAddShiftType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddShiftType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddShiftType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddShiftType.Location = New System.Drawing.Point(365, 32)
        Me.objbtnAddShiftType.Name = "objbtnAddShiftType"
        Me.objbtnAddShiftType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddShiftType.TabIndex = 38
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(365, 59)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 4
        '
        'cboShiftType
        '
        Me.cboShiftType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShiftType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShiftType.FormattingEnabled = True
        Me.cboShiftType.Location = New System.Drawing.Point(250, 32)
        Me.cboShiftType.Name = "cboShiftType"
        Me.cboShiftType.Size = New System.Drawing.Size(109, 21)
        Me.cboShiftType.TabIndex = 2
        '
        'lblShiftType
        '
        Me.lblShiftType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShiftType.Location = New System.Drawing.Point(178, 36)
        Me.lblShiftType.Name = "lblShiftType"
        Me.lblShiftType.Size = New System.Drawing.Size(66, 13)
        Me.lblShiftType.TabIndex = 36
        Me.lblShiftType.Text = "Shift Type"
        Me.lblShiftType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkParttimeshift
        '
        Me.chkParttimeshift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkParttimeshift.Location = New System.Drawing.Point(226, 115)
        Me.chkParttimeshift.Name = "chkParttimeshift"
        Me.chkParttimeshift.Size = New System.Drawing.Size(133, 17)
        Me.chkParttimeshift.TabIndex = 8
        Me.chkParttimeshift.Text = "Part time Shift"
        Me.chkParttimeshift.UseVisualStyleBackColor = True
        '
        'txtBreakTime
        '
        Me.txtBreakTime.AllowNegative = False
        Me.txtBreakTime.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtBreakTime.DigitsInGroup = 0
        Me.txtBreakTime.Flags = 65536
        Me.txtBreakTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.txtBreakTime.Location = New System.Drawing.Point(94, 113)
        Me.txtBreakTime.MaxDecimalPlaces = 0
        Me.txtBreakTime.MaxWholeDigits = 21
        Me.txtBreakTime.Name = "txtBreakTime"
        Me.txtBreakTime.Prefix = ""
        Me.txtBreakTime.RangeMax = 1.7976931348623157E+308
        Me.txtBreakTime.RangeMin = -1.7976931348623157E+308
        Me.txtBreakTime.Size = New System.Drawing.Size(57, 21)
        Me.txtBreakTime.TabIndex = 7
        Me.txtBreakTime.Text = "0"
        Me.txtBreakTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMinute
        '
        Me.lblMinute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblMinute.Location = New System.Drawing.Point(157, 117)
        Me.lblMinute.Name = "lblMinute"
        Me.lblMinute.Size = New System.Drawing.Size(62, 13)
        Me.lblMinute.TabIndex = 12
        Me.lblMinute.Text = "(In Minute)"
        Me.lblMinute.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblcode
        '
        Me.lblcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblcode.Location = New System.Drawing.Point(8, 36)
        Me.lblcode.Name = "lblcode"
        Me.lblcode.Size = New System.Drawing.Size(80, 13)
        Me.lblcode.TabIndex = 0
        Me.lblcode.Text = "Code"
        Me.lblcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEndTime
        '
        Me.dtpEndTime.CustomFormat = "hh:mm tt"
        Me.dtpEndTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpEndTime.Location = New System.Drawing.Point(260, 86)
        Me.dtpEndTime.Name = "dtpEndTime"
        Me.dtpEndTime.ShowUpDown = True
        Me.dtpEndTime.Size = New System.Drawing.Size(99, 21)
        Me.dtpEndTime.TabIndex = 6
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 61)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(80, 13)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartTime
        '
        Me.dtpStartTime.CustomFormat = "hh:mm tt"
        Me.dtpStartTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.dtpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpStartTime.Location = New System.Drawing.Point(94, 86)
        Me.dtpStartTime.Name = "dtpStartTime"
        Me.dtpStartTime.ShowUpDown = True
        Me.dtpStartTime.Size = New System.Drawing.Size(99, 21)
        Me.dtpStartTime.TabIndex = 5
        '
        'lblStartTime
        '
        Me.lblStartTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblStartTime.Location = New System.Drawing.Point(8, 90)
        Me.lblStartTime.Name = "lblStartTime"
        Me.lblStartTime.Size = New System.Drawing.Size(80, 13)
        Me.lblStartTime.TabIndex = 6
        Me.lblStartTime.Text = "Start Time"
        Me.lblStartTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(94, 59)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(265, 21)
        Me.txtName.TabIndex = 3
        '
        'lblEndTime
        '
        Me.lblEndTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblEndTime.Location = New System.Drawing.Point(199, 90)
        Me.lblEndTime.Name = "lblEndTime"
        Me.lblEndTime.Size = New System.Drawing.Size(62, 13)
        Me.lblEndTime.TabIndex = 8
        Me.lblEndTime.Text = "End Time"
        Me.lblEndTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(94, 32)
        Me.txtCode.MaxLength = 5
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(78, 21)
        Me.txtCode.TabIndex = 1
        '
        'lvDay
        '
        Me.lvDay.CheckBoxes = True
        Me.lvDay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lvDay.Location = New System.Drawing.Point(94, 140)
        Me.lvDay.Name = "lvDay"
        Me.lvDay.Size = New System.Drawing.Size(99, 140)
        Me.lvDay.TabIndex = 9
        Me.lvDay.UseCompatibleStateImageBehavior = False
        Me.lvDay.View = System.Windows.Forms.View.List
        '
        'lblWorkingHours
        '
        Me.lblWorkingHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblWorkingHours.Location = New System.Drawing.Point(8, 289)
        Me.lblWorkingHours.Name = "lblWorkingHours"
        Me.lblWorkingHours.Size = New System.Drawing.Size(81, 14)
        Me.lblWorkingHours.TabIndex = 17
        Me.lblWorkingHours.Text = "Working Hours"
        Me.lblWorkingHours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDay
        '
        Me.lblDay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblDay.Location = New System.Drawing.Point(8, 138)
        Me.lblDay.Name = "lblDay"
        Me.lblDay.Size = New System.Drawing.Size(35, 13)
        Me.lblDay.TabIndex = 15
        Me.lblDay.Text = "Day"
        Me.lblDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBreakTime
        '
        Me.lblBreakTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblBreakTime.Location = New System.Drawing.Point(8, 117)
        Me.lblBreakTime.Name = "lblBreakTime"
        Me.lblBreakTime.Size = New System.Drawing.Size(80, 13)
        Me.lblBreakTime.TabIndex = 10
        Me.lblBreakTime.Text = "Break Time"
        Me.lblBreakTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHours
        '
        Me.txtHours.Flags = 0
        Me.txtHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.txtHours.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtHours.Location = New System.Drawing.Point(94, 286)
        Me.txtHours.MaxLength = 5
        Me.txtHours.Name = "txtHours"
        Me.txtHours.ReadOnly = True
        Me.txtHours.Size = New System.Drawing.Size(99, 21)
        Me.txtHours.TabIndex = 18
        Me.txtHours.TabStop = False
        '
        'frmShift_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(417, 510)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmShift_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Shift"
        Me.pnlMain.ResumeLayout(False)
        Me.gbShiftSetting.ResumeLayout(False)
        Me.gbShiftSetting.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.gbInfo.ResumeLayout(False)
        Me.gbInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtBreakTime As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMinute As System.Windows.Forms.Label
    Friend WithEvents lblcode As System.Windows.Forms.Label
    Friend WithEvents dtpEndTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents dtpStartTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartTime As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEndTime As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lvDay As System.Windows.Forms.ListView
    Friend WithEvents lblWorkingHours As System.Windows.Forms.Label
    Friend WithEvents lblDay As System.Windows.Forms.Label
    Friend WithEvents lblBreakTime As System.Windows.Forms.Label
    Friend WithEvents txtHours As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkParttimeshift As System.Windows.Forms.CheckBox
    Friend WithEvents cboShiftType As System.Windows.Forms.ComboBox
    Friend WithEvents lblShiftType As System.Windows.Forms.Label
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents gbShiftSetting As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtOverTimeMin As eZee.TextBox.NumericTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblOvertime As System.Windows.Forms.Label
    Friend WithEvents txtToHalfhour As eZee.TextBox.NumericTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtFromHalfhour As eZee.TextBox.NumericTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtShortTimeMin As eZee.TextBox.NumericTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblShorttime As System.Windows.Forms.Label
    Friend WithEvents objbtnAddShiftType As eZee.Common.eZeeGradientButton
End Class

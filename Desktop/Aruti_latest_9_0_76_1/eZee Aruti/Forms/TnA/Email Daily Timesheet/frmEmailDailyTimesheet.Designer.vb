﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmailDailyTimesheet
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmailDailyTimesheet))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblDates = New System.Windows.Forms.Label
        Me.objlblDates = New System.Windows.Forms.Label
        Me.cboTnAPeriod = New System.Windows.Forms.ComboBox
        Me.lblTnAPeriod = New System.Windows.Forms.Label
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchEmp = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvAEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objAlloacationReset = New eZee.Common.eZeeGradientButton
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnEmailOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEmailClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblDates)
        Me.Panel1.Controls.Add(Me.objlblDates)
        Me.Panel1.Controls.Add(Me.cboTnAPeriod)
        Me.Panel1.Controls.Add(Me.lblTnAPeriod)
        Me.Panel1.Controls.Add(Me.tblpAssessorEmployee)
        Me.Panel1.Controls.Add(Me.objAlloacationReset)
        Me.Panel1.Controls.Add(Me.lnkAllocation)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(499, 437)
        Me.Panel1.TabIndex = 0
        '
        'lblDates
        '
        Me.lblDates.BackColor = System.Drawing.Color.Transparent
        Me.lblDates.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDates.Location = New System.Drawing.Point(15, 36)
        Me.lblDates.Name = "lblDates"
        Me.lblDates.Size = New System.Drawing.Size(83, 16)
        Me.lblDates.TabIndex = 308
        Me.lblDates.Text = "Period Dates"
        Me.lblDates.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblDates
        '
        Me.objlblDates.BackColor = System.Drawing.Color.Transparent
        Me.objlblDates.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblDates.Location = New System.Drawing.Point(104, 36)
        Me.objlblDates.Name = "objlblDates"
        Me.objlblDates.Size = New System.Drawing.Size(385, 16)
        Me.objlblDates.TabIndex = 307
        Me.objlblDates.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTnAPeriod
        '
        Me.cboTnAPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTnAPeriod.FormattingEnabled = True
        Me.cboTnAPeriod.Location = New System.Drawing.Point(104, 12)
        Me.cboTnAPeriod.Name = "cboTnAPeriod"
        Me.cboTnAPeriod.Size = New System.Drawing.Size(253, 21)
        Me.cboTnAPeriod.TabIndex = 306
        '
        'lblTnAPeriod
        '
        Me.lblTnAPeriod.BackColor = System.Drawing.Color.Transparent
        Me.lblTnAPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTnAPeriod.Location = New System.Drawing.Point(15, 14)
        Me.lblTnAPeriod.Name = "lblTnAPeriod"
        Me.lblTnAPeriod.Size = New System.Drawing.Size(83, 16)
        Me.lblTnAPeriod.TabIndex = 305
        Me.lblTnAPeriod.Text = "Select Period"
        Me.lblTnAPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(12, 57)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(477, 324)
        Me.tblpAssessorEmployee.TabIndex = 304
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchEmp.Flags = 0
        Me.txtSearchEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(471, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.objchkEmployee)
        Me.objpnlEmp.Controls.Add(Me.dgvAEmployee)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(471, 292)
        Me.objpnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvAEmployee
        '
        Me.dgvAEmployee.AllowUserToAddRows = False
        Me.dgvAEmployee.AllowUserToDeleteRows = False
        Me.dgvAEmployee.AllowUserToResizeColumns = False
        Me.dgvAEmployee.AllowUserToResizeRows = False
        Me.dgvAEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAEmployee.ColumnHeadersHeight = 22
        Me.dgvAEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.dgcolhEmail, Me.objdgcolhEmpId})
        Me.dgvAEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvAEmployee.MultiSelect = False
        Me.dgvAEmployee.Name = "dgvAEmployee"
        Me.dgvAEmployee.RowHeadersVisible = False
        Me.dgvAEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAEmployee.Size = New System.Drawing.Size(471, 292)
        Me.dgvAEmployee.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEmail
        '
        Me.dgcolhEmail.HeaderText = "Email"
        Me.dgcolhEmail.Name = "dgcolhEmail"
        Me.dgcolhEmail.ReadOnly = True
        Me.dgcolhEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmail.Width = 150
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'objAlloacationReset
        '
        Me.objAlloacationReset.BackColor = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objAlloacationReset.BorderSelected = False
        Me.objAlloacationReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objAlloacationReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objAlloacationReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objAlloacationReset.Location = New System.Drawing.Point(465, 12)
        Me.objAlloacationReset.Name = "objAlloacationReset"
        Me.objAlloacationReset.Size = New System.Drawing.Size(21, 21)
        Me.objAlloacationReset.TabIndex = 239
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(363, 15)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(96, 15)
        Me.lnkAllocation.TabIndex = 237
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnEmailOk)
        Me.objFooter.Controls.Add(Me.btnEmailClose)
        Me.objFooter.Controls.Add(Me.dtpDate)
        Me.objFooter.Controls.Add(Me.lblDate)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 387)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(499, 50)
        Me.objFooter.TabIndex = 3
        '
        'btnEmailOk
        '
        Me.btnEmailOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEmailOk.BackColor = System.Drawing.Color.White
        Me.btnEmailOk.BackgroundImage = CType(resources.GetObject("btnEmailOk.BackgroundImage"), System.Drawing.Image)
        Me.btnEmailOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmailOk.BorderColor = System.Drawing.Color.Empty
        Me.btnEmailOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEmailOk.FlatAppearance.BorderSize = 0
        Me.btnEmailOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmailOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmailOk.ForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmailOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.Location = New System.Drawing.Point(287, 11)
        Me.btnEmailOk.Name = "btnEmailOk"
        Me.btnEmailOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.Size = New System.Drawing.Size(97, 30)
        Me.btnEmailOk.TabIndex = 4
        Me.btnEmailOk.Text = "&Ok"
        Me.btnEmailOk.UseVisualStyleBackColor = True
        '
        'btnEmailClose
        '
        Me.btnEmailClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEmailClose.BackColor = System.Drawing.Color.White
        Me.btnEmailClose.BackgroundImage = CType(resources.GetObject("btnEmailClose.BackgroundImage"), System.Drawing.Image)
        Me.btnEmailClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmailClose.BorderColor = System.Drawing.Color.Empty
        Me.btnEmailClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEmailClose.FlatAppearance.BorderSize = 0
        Me.btnEmailClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmailClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmailClose.ForeColor = System.Drawing.Color.Black
        Me.btnEmailClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmailClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmailClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailClose.Location = New System.Drawing.Point(390, 11)
        Me.btnEmailClose.Name = "btnEmailClose"
        Me.btnEmailClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailClose.Size = New System.Drawing.Size(97, 30)
        Me.btnEmailClose.TabIndex = 3
        Me.btnEmailClose.Text = "&Close"
        Me.btnEmailClose.UseVisualStyleBackColor = True
        '
        'dtpDate
        '
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(178, 14)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(103, 21)
        Me.dtpDate.TabIndex = 154
        Me.dtpDate.Visible = False
        '
        'lblDate
        '
        Me.lblDate.BackColor = System.Drawing.Color.Transparent
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(46, 16)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(126, 16)
        Me.lblDate.TabIndex = 153
        Me.lblDate.Text = "Timesheet As On Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblDate.Visible = False
        '
        'frmEmailDailyTimesheet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(499, 437)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmailDailyTimesheet"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Email Daily Timesheet"
        Me.Panel1.ResumeLayout(False)
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        Me.objpnlEmp.PerformLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnEmailClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmailOk As eZee.Common.eZeeLightButton
    Public WithEvents lblDate As System.Windows.Forms.Label
    Public WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objAlloacationReset As eZee.Common.eZeeGradientButton
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvAEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboTnAPeriod As System.Windows.Forms.ComboBox
    Public WithEvents lblTnAPeriod As System.Windows.Forms.Label
    Public WithEvents objlblDates As System.Windows.Forms.Label
    Public WithEvents lblDates As System.Windows.Forms.Label
End Class

﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("acore32")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Aruti")> 
<Assembly: AssemblyProduct("acore32")> 
<Assembly: AssemblyCopyright(acore32.core.Copyrightversion)> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("9ad6726b-e845-4fc7-8569-3684119da606")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

'<Assembly: AssemblyVersion("1.0.0.0")> 
'<Assembly: AssemblyFileVersion("1.0.0.0")> 
<Assembly: AssemblyVersion(acore32.core.Version)> 
<Assembly: AssemblyFileVersion(acore32.core.Version)> 


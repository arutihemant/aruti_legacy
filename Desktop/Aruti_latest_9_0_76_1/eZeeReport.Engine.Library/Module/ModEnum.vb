Module ModEnum
End Module

Public Enum enPageOrientation
    Portrait = 0
    Landscape = 1
End Enum

Public Enum enPrintOption
    Preview = 0
    Print = 1
    Export = 2
End Enum

Public Enum enExportOption
    HTML = 0
    PDF = 1
    EXCEL = 2
    WORD = 3
    RTF = 4
    CSV = 5
    IMAGE = 6
End Enum


﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmParperSetting
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmParperSetting))
        Me.btnOk = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbPaperSize = New System.Windows.Forms.ComboBox
        Me.grpPaperSize = New System.Windows.Forms.GroupBox
        Me.txtWidth = New System.Windows.Forms.TextBox
        Me.txtHeight = New System.Windows.Forms.TextBox
        Me.lblHeight = New System.Windows.Forms.Label
        Me.lblWidth = New System.Windows.Forms.Label
        Me.grpMargins = New System.Windows.Forms.GroupBox
        Me.txtBottom = New System.Windows.Forms.TextBox
        Me.txtRight = New System.Windows.Forms.TextBox
        Me.txtTop = New System.Windows.Forms.TextBox
        Me.txtLeft = New System.Windows.Forms.TextBox
        Me.lblBottom = New System.Windows.Forms.Label
        Me.lblTop = New System.Windows.Forms.Label
        Me.lblRight = New System.Windows.Forms.Label
        Me.lblLeft = New System.Windows.Forms.Label
        Me.rbPortrait = New System.Windows.Forms.RadioButton
        Me.rbLandscape = New System.Windows.Forms.RadioButton
        Me.grpOrientation = New System.Windows.Forms.GroupBox
        Me.tlpPage = New System.Windows.Forms.TableLayoutPanel
        Me.lblPrint = New System.Windows.Forms.Label
        Me.grpPreview = New System.Windows.Forms.GroupBox
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.grpPaperSize.SuspendLayout()
        Me.grpMargins.SuspendLayout()
        Me.grpOrientation.SuspendLayout()
        Me.tlpPage.SuspendLayout()
        Me.grpPreview.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnOk
        '
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.Location = New System.Drawing.Point(311, 226)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(92, 29)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "&OK"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(409, 225)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(92, 29)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Paper Size"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbPaperSize
        '
        Me.cmbPaperSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPaperSize.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbPaperSize.ForeColor = System.Drawing.Color.SaddleBrown
        Me.cmbPaperSize.FormattingEnabled = True
        Me.cmbPaperSize.Location = New System.Drawing.Point(88, 20)
        Me.cmbPaperSize.Name = "cmbPaperSize"
        Me.cmbPaperSize.Size = New System.Drawing.Size(174, 21)
        Me.cmbPaperSize.TabIndex = 3
        '
        'grpPaperSize
        '
        Me.grpPaperSize.Controls.Add(Me.txtWidth)
        Me.grpPaperSize.Controls.Add(Me.txtHeight)
        Me.grpPaperSize.Controls.Add(Me.lblHeight)
        Me.grpPaperSize.Controls.Add(Me.lblWidth)
        Me.grpPaperSize.Controls.Add(Me.Label1)
        Me.grpPaperSize.Controls.Add(Me.cmbPaperSize)
        Me.grpPaperSize.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpPaperSize.Location = New System.Drawing.Point(7, 7)
        Me.grpPaperSize.Name = "grpPaperSize"
        Me.grpPaperSize.Size = New System.Drawing.Size(273, 76)
        Me.grpPaperSize.TabIndex = 4
        Me.grpPaperSize.TabStop = False
        Me.grpPaperSize.Text = "Paper Sizes (Inches)"
        '
        'txtWidth
        '
        Me.txtWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtWidth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWidth.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.txtWidth.Location = New System.Drawing.Point(88, 47)
        Me.txtWidth.Name = "txtWidth"
        Me.txtWidth.Size = New System.Drawing.Size(45, 21)
        Me.txtWidth.TabIndex = 15
        Me.txtWidth.Text = "0"
        Me.txtWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtHeight
        '
        Me.txtHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtHeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHeight.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.txtHeight.Location = New System.Drawing.Point(217, 47)
        Me.txtHeight.Name = "txtHeight"
        Me.txtHeight.Size = New System.Drawing.Size(45, 21)
        Me.txtHeight.TabIndex = 14
        Me.txtHeight.Text = "0"
        Me.txtHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblHeight
        '
        Me.lblHeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeight.Location = New System.Drawing.Point(159, 51)
        Me.lblHeight.Name = "lblHeight"
        Me.lblHeight.Size = New System.Drawing.Size(50, 13)
        Me.lblHeight.TabIndex = 13
        Me.lblHeight.Text = "Height"
        Me.lblHeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWidth
        '
        Me.lblWidth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWidth.Location = New System.Drawing.Point(11, 51)
        Me.lblWidth.Name = "lblWidth"
        Me.lblWidth.Size = New System.Drawing.Size(69, 13)
        Me.lblWidth.TabIndex = 12
        Me.lblWidth.Text = "Width"
        Me.lblWidth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grpMargins
        '
        Me.grpMargins.Controls.Add(Me.txtBottom)
        Me.grpMargins.Controls.Add(Me.txtRight)
        Me.grpMargins.Controls.Add(Me.txtTop)
        Me.grpMargins.Controls.Add(Me.txtLeft)
        Me.grpMargins.Controls.Add(Me.lblBottom)
        Me.grpMargins.Controls.Add(Me.lblTop)
        Me.grpMargins.Controls.Add(Me.lblRight)
        Me.grpMargins.Controls.Add(Me.lblLeft)
        Me.grpMargins.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpMargins.Location = New System.Drawing.Point(7, 89)
        Me.grpMargins.Name = "grpMargins"
        Me.grpMargins.Size = New System.Drawing.Size(273, 74)
        Me.grpMargins.TabIndex = 8
        Me.grpMargins.TabStop = False
        Me.grpMargins.Text = "Margins"
        '
        'txtBottom
        '
        Me.txtBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBottom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBottom.ForeColor = System.Drawing.Color.SaddleBrown
        Me.txtBottom.Location = New System.Drawing.Point(217, 46)
        Me.txtBottom.Name = "txtBottom"
        Me.txtBottom.Size = New System.Drawing.Size(45, 21)
        Me.txtBottom.TabIndex = 11
        Me.txtBottom.Text = "0.25"
        Me.txtBottom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRight
        '
        Me.txtRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRight.ForeColor = System.Drawing.Color.SaddleBrown
        Me.txtRight.Location = New System.Drawing.Point(217, 19)
        Me.txtRight.Name = "txtRight"
        Me.txtRight.Size = New System.Drawing.Size(45, 21)
        Me.txtRight.TabIndex = 10
        Me.txtRight.Text = "0.25"
        Me.txtRight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTop
        '
        Me.txtTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTop.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTop.ForeColor = System.Drawing.Color.SaddleBrown
        Me.txtTop.Location = New System.Drawing.Point(88, 46)
        Me.txtTop.Name = "txtTop"
        Me.txtTop.Size = New System.Drawing.Size(45, 21)
        Me.txtTop.TabIndex = 9
        Me.txtTop.Text = "0.25"
        Me.txtTop.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLeft
        '
        Me.txtLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLeft.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLeft.ForeColor = System.Drawing.Color.SaddleBrown
        Me.txtLeft.Location = New System.Drawing.Point(88, 19)
        Me.txtLeft.Name = "txtLeft"
        Me.txtLeft.Size = New System.Drawing.Size(45, 21)
        Me.txtLeft.TabIndex = 8
        Me.txtLeft.Text = "0.25"
        Me.txtLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBottom
        '
        Me.lblBottom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBottom.Location = New System.Drawing.Point(159, 50)
        Me.lblBottom.Name = "lblBottom"
        Me.lblBottom.Size = New System.Drawing.Size(50, 13)
        Me.lblBottom.TabIndex = 7
        Me.lblBottom.Text = "Bottom"
        Me.lblBottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTop
        '
        Me.lblTop.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTop.Location = New System.Drawing.Point(11, 50)
        Me.lblTop.Name = "lblTop"
        Me.lblTop.Size = New System.Drawing.Size(69, 13)
        Me.lblTop.TabIndex = 6
        Me.lblTop.Text = "Top"
        Me.lblTop.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRight
        '
        Me.lblRight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRight.Location = New System.Drawing.Point(159, 23)
        Me.lblRight.Name = "lblRight"
        Me.lblRight.Size = New System.Drawing.Size(50, 13)
        Me.lblRight.TabIndex = 5
        Me.lblRight.Text = "Right"
        Me.lblRight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLeft
        '
        Me.lblLeft.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeft.Location = New System.Drawing.Point(11, 23)
        Me.lblLeft.Name = "lblLeft"
        Me.lblLeft.Size = New System.Drawing.Size(69, 13)
        Me.lblLeft.TabIndex = 4
        Me.lblLeft.Text = "Left"
        Me.lblLeft.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rbPortrait
        '
        Me.rbPortrait.Checked = True
        Me.rbPortrait.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbPortrait.Location = New System.Drawing.Point(11, 23)
        Me.rbPortrait.Name = "rbPortrait"
        Me.rbPortrait.Size = New System.Drawing.Size(66, 17)
        Me.rbPortrait.TabIndex = 5
        Me.rbPortrait.TabStop = True
        Me.rbPortrait.Text = "Portrait"
        Me.rbPortrait.UseVisualStyleBackColor = True
        '
        'rbLandscape
        '
        Me.rbLandscape.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbLandscape.Location = New System.Drawing.Point(88, 23)
        Me.rbLandscape.Name = "rbLandscape"
        Me.rbLandscape.Size = New System.Drawing.Size(87, 17)
        Me.rbLandscape.TabIndex = 6
        Me.rbLandscape.Text = "Landscape"
        Me.rbLandscape.UseVisualStyleBackColor = True
        '
        'grpOrientation
        '
        Me.grpOrientation.Controls.Add(Me.rbLandscape)
        Me.grpOrientation.Controls.Add(Me.rbPortrait)
        Me.grpOrientation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpOrientation.Location = New System.Drawing.Point(7, 169)
        Me.grpOrientation.Name = "grpOrientation"
        Me.grpOrientation.Size = New System.Drawing.Size(273, 50)
        Me.grpOrientation.TabIndex = 7
        Me.grpOrientation.TabStop = False
        Me.grpOrientation.Text = "Orientation"
        '
        'tlpPage
        '
        Me.tlpPage.BackColor = System.Drawing.Color.White
        Me.tlpPage.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tlpPage.ColumnCount = 3
        Me.tlpPage.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 12.0!))
        Me.tlpPage.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpPage.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16.0!))
        Me.tlpPage.Controls.Add(Me.lblPrint, 1, 1)
        Me.tlpPage.Location = New System.Drawing.Point(34, 45)
        Me.tlpPage.Name = "tlpPage"
        Me.tlpPage.RowCount = 3
        Me.tlpPage.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.tlpPage.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpPage.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.tlpPage.Size = New System.Drawing.Size(100, 130)
        Me.tlpPage.TabIndex = 8
        '
        'lblPrint
        '
        Me.lblPrint.BackColor = System.Drawing.Color.White
        Me.lblPrint.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblPrint.Location = New System.Drawing.Point(17, 12)
        Me.lblPrint.Name = "lblPrint"
        Me.lblPrint.Size = New System.Drawing.Size(62, 106)
        Me.lblPrint.TabIndex = 0
        Me.lblPrint.Text = resources.GetString("lblPrint.Text")
        '
        'grpPreview
        '
        Me.grpPreview.Controls.Add(Me.tlpPage)
        Me.grpPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpPreview.Location = New System.Drawing.Point(286, 7)
        Me.grpPreview.Name = "grpPreview"
        Me.grpPreview.Size = New System.Drawing.Size(215, 212)
        Me.grpPreview.TabIndex = 9
        Me.grpPreview.TabStop = False
        Me.grpPreview.Text = "Preview"
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.White
        Me.pnlMain.Controls.Add(Me.grpPreview)
        Me.pnlMain.Controls.Add(Me.btnCancel)
        Me.pnlMain.Controls.Add(Me.grpPaperSize)
        Me.pnlMain.Controls.Add(Me.grpOrientation)
        Me.pnlMain.Controls.Add(Me.grpMargins)
        Me.pnlMain.Controls.Add(Me.btnOk)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(512, 262)
        Me.pnlMain.TabIndex = 10
        '
        'frmParperSetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Honeydew
        Me.ClientSize = New System.Drawing.Size(512, 262)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmParperSetting"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Parper Setting"
        Me.grpPaperSize.ResumeLayout(False)
        Me.grpPaperSize.PerformLayout()
        Me.grpMargins.ResumeLayout(False)
        Me.grpMargins.PerformLayout()
        Me.grpOrientation.ResumeLayout(False)
        Me.tlpPage.ResumeLayout(False)
        Me.grpPreview.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbPaperSize As System.Windows.Forms.ComboBox
    Friend WithEvents grpPaperSize As System.Windows.Forms.GroupBox
    Friend WithEvents grpMargins As System.Windows.Forms.GroupBox
    Friend WithEvents lblBottom As System.Windows.Forms.Label
    Friend WithEvents lblTop As System.Windows.Forms.Label
    Friend WithEvents lblRight As System.Windows.Forms.Label
    Friend WithEvents lblLeft As System.Windows.Forms.Label
    Friend WithEvents lblHeight As System.Windows.Forms.Label
    Friend WithEvents lblWidth As System.Windows.Forms.Label
    Friend WithEvents txtBottom As System.Windows.Forms.TextBox
    Friend WithEvents txtRight As System.Windows.Forms.TextBox
    Friend WithEvents txtTop As System.Windows.Forms.TextBox
    Friend WithEvents txtLeft As System.Windows.Forms.TextBox
    Friend WithEvents txtWidth As System.Windows.Forms.TextBox
    Friend WithEvents txtHeight As System.Windows.Forms.TextBox
    Friend WithEvents rbPortrait As System.Windows.Forms.RadioButton
    Friend WithEvents rbLandscape As System.Windows.Forms.RadioButton
    Friend WithEvents grpOrientation As System.Windows.Forms.GroupBox
    Friend WithEvents tlpPage As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblPrint As System.Windows.Forms.Label
    Friend WithEvents grpPreview As System.Windows.Forms.GroupBox
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
End Class

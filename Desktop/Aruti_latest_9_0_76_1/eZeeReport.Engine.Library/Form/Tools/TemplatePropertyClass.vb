Imports System
Imports System.Collections.Specialized
Imports System.ComponentModel

'Interface for the class to show in propertygrid 
Public Interface ITemplatePropertyClass
    Property PublicProperties() As TemplatePropertyList
End Interface

Public Class TemplatePropertyList
    Inherits NameObjectCollectionBase

    Public Sub Add(ByVal value As [Object])
        'The key for the object is taken from the object to insert 
        Me.BaseAdd(DirectCast(value, TemplateProperty).Name, value)
    End Sub

    Public Sub Remove(ByVal key As [String])
        Me.BaseRemove(key)
    End Sub

    Public Sub Remove(ByVal index As Integer)
        Me.BaseRemoveAt(index)
    End Sub

    Public Sub Clear()
        Me.BaseClear()
    End Sub

    Default Public Property Item(ByVal key As [String]) As TemplateProperty
        Get
            Return DirectCast((Me.BaseGet(key)), TemplateProperty)
        End Get
        Set(ByVal value As TemplateProperty)
            Me.BaseSet(key, value)
        End Set
    End Property

    Default Public Property Item(ByVal indice As Integer) As TemplateProperty
        Get
            Return DirectCast((Me.BaseGet(indice)), TemplateProperty)
        End Get
        Set(ByVal value As TemplateProperty)
            Me.BaseSet(indice, value)
        End Set
    End Property

End Class

Public Class TemplateProperty
    Private m_name As String = String.Empty
    Private m_category As String = String.Empty
    Private m_DisplayName As String = String.Empty

    'Read Only 
    Private m_name_read As String = String.Empty
    Private m_category_read As String = String.Empty
    Private m_DisplayName_read As String = String.Empty

    Public Sub New()
    End Sub

    Public Sub New(ByVal Name As String, ByVal DisplayName As String, ByVal Category As String)
        m_name = Name
        m_DisplayName = DisplayName
        m_category = Category
    End Sub
    Public Sub New(ByVal Name As String, ByVal DisplayName As String, ByVal Category As String, ByVal lReadOnly As Boolean)
        m_name_read = Name
        m_DisplayName_read = DisplayName
        m_category_read = Category
    End Sub

    Public Property Name() As String
        Get
            Return m_name
        End Get
        Set(ByVal value As String)
            m_name = value
        End Set
    End Property

    Public Property Category() As String
        Get
            Return m_category
        End Get
        Set(ByVal value As String)
            m_category = value
        End Set
    End Property

    Public Property DisplayName() As String
        Get
            Return m_DisplayName
        End Get
        Set(ByVal value As String)
            m_DisplayName = value
        End Set
    End Property
    Public ReadOnly Property Name_read() As String
        Get
            Return m_name_read
        End Get
    End Property

    Public ReadOnly Property Category_read() As String
        Get
            Return m_category_read
        End Get
    End Property

    Public ReadOnly Property DisplayName_read() As String
        Get
            Return m_DisplayName_read
        End Get
    End Property
End Class


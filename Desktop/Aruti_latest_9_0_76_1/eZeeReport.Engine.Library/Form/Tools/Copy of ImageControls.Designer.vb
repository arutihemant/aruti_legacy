﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImageControlCopy
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pbSample = New System.Windows.Forms.PictureBox
        CType(Me.pbSample, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pbSample
        '
        Me.pbSample.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.ezeedemo
        Me.pbSample.Location = New System.Drawing.Point(4, 5)
        Me.pbSample.Name = "pbSample"
        Me.pbSample.Size = New System.Drawing.Size(144, 121)
        Me.pbSample.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbSample.TabIndex = 0
        Me.pbSample.TabStop = False
        Me.pbSample.UseWaitCursor = True
        '
        'ImageControls
        '
        Me.Controls.Add(Me.pbSample)
        Me.Name = "ImageControls"
        Me.Size = New System.Drawing.Size(151, 131)
        Me.UseWaitCursor = True
        CType(Me.pbSample, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pbSample As System.Windows.Forms.PictureBox

End Class
